---
permalink: /introduction/issue
---
# Issue

## Definition

::: definition ISSUE

An issue is a part of the change management and includes

* Change Requests
* Requests for Information/Education
* For future consideration
* Incident

<adimg issue/> is the symbol used throughout the app.<p></p>

:::

## Change Requests

A Change Requests an expressed desire to change / modify an artefact

## Requests for Information/Education

A Requests for Information/Education signals that a user wants to get more information about an artefact.

## For future consideration

An issue of type "For future consideration" indicates the desire that an author of an artefact may consider a change in the future but is not yet urgent to do any immediate changes.

## Incident

An incident declares a more or less severe problem that disturbes or even discontinues normal operations when using an artefact.