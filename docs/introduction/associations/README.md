---
permalink: /introduction/associations
---
# Associations and Mappings

## Introduction

Artefacts in ART-DECOR may be *linked* to each other in order to express a certain relationship between them. Beyond the fact that versions of the same artefact are coupled in a row, there are seven other types that carry a qualified relationship. This concerns Mappings

- between **Dataset Concepts** and **Coded Concepts**,
- between **Choice Lists** and **Value Sets**,
- between **Choice Lists Elements** and **Codes in a Value Set**,
- between **Dataset Concepts** and **Identifiers**,
- between **Scenarios** and **Templates/Profiles** (Representing Templates),
- between **Dataset Concepts** and whole **Templates/Profiles** or parts thereof,
- between **Dataset Concepts** of two different **Datasets**.

::: note

Please note that all Maps / Associations between two artefacts may appear 0 to many times. This means e. g., that a Dataset concept “Color of the Iris” may have zero maps to a Coded Concept, or one for example to SNOMED-CT, or even more like to an OrphaNet Code for Rare Diseases etc.

:::

There is another notion of a relationship between Template / Profiule elements of type code and a Value Set or Coded Concept associated with it. This is typically called a **Binding**.

## Collaborative Separation of Concerns

ART-DECOR as a tools focuses on “collaborative separation of concerns”, i. e. that people with different skills play their role on the way towards a healthcare interoperability specification.

::: example

As an example, a healthcare provider specifies his requirement to allow capturing of data about the “Eye color” and defines a corresponding Dataset Concept in his Dataset. A terminologist analyses and annotates this Concept by adding a Map / Association to a e.g. SNOMED-CT code. The contributions of the  healthcare provider and the terminologist are mutually visible in “their Panels”. Finally, a technical modeller adds a technical representation as a Template or Profile by taking the work of the healthcare provider and terminologist into consideration. Also his work is mutually visible to all other “disciplines”.

:::

Later in time, Release 3 will introduce Concept Maps, that allow to takle all six types of maps / associations. Maps then have their own id, effectiveDate and status etc.

## Dataset Concepts and Coded Concepts

This mapping concerns associations between Dataset Concepts and Terminologies = Coded Concepts from a Code Systems (such as  SNOMED-CT). Dataset Concepts may be coupled with one or more Coded Concept to allow more specificity in what the concept actually means.

<img src="../../img/image-20220629102249767.png" alt="image-20220629102249767" style="zoom:50%;" />

::: example

A Concept Headache can be associatiated with the code `R51` from ICD-10...

<img src="../../img/image-20220629095504875.png" alt="image-20220629095504875" style="zoom:50%;" />

... and also with the corresponding SNOMED-CT code.

<img src="../../img/image-20220629095606283.png" alt="image-20220629095606283" style="zoom:50%;" />

:::

Please keep in mind, that a Coded Concept is comprised of a code and the corresponding system where the code is drawn from as mandatory parts, and optional more parts like a display name or designations.

<img src="../../img/image-20220629100546139.png" alt="image-20220629100546139" style="zoom:50%;" />

::: example

Terminology Maps / Associations are assumed to be done by persons with corresponding skills, separate from building a Dataset. A Dataset Concept was named “Eye color” as a requirement. This “trivial” name is coupled to a Coded Concept from SNOMED-CT 247030006 “Color of Iris”.

<img src="../../img/image-20220629102233805.png" alt="image-20220629102233805" style="zoom:50%;" />

:::

## Mappings between Choice Lists and Value Sets

Dataset may have concepts that have a list of choices as a set of valid values for that concept.

::: example

In a healthcare scenario, a Dataset Concept *Color of the Iris* can have values out of the following choices:

- blue
- brown
- green
- other

:::

This mapping concerns associations between Dataset Concepts with Choice Lists and Value Sets. This association is only available on Dataset Concept Value Domain Datatype = code. The linked Value Set shall have a set of codes that properly reflects the choices (see also [next chapter](#mappings-between-choice-list-elements-and-codes-in-a-value-set)).

<img src="../../img/image-20220629111547641.png" alt="image-20220629111547641" style="zoom:50%;" />

## Mappings between Choice List Elements and Codes in a Value Set

This mapping concerns associations between Dataset Concepts and their Choice Lists Elements with Terminologies = Codes in a specific Value Set. This association is only available on Dataset Concept Value Domain Datatype = code.

<img src="../../img/image-20220629103347766.png" alt="image-20220629103347766" style="zoom:50%;" />

## Mappings between Dataset Concepts and Identifiers

This mapping concerns associations between Dataset Concept (in essence: its Value Domain) with identifiers, such as an OID or URI. This association is only available on Dataset Concept Value Domain Datatype = identifier.

<img src="../../img/image-20220629105543386.png" alt="image-20220629105543386" style="zoom:50%;" />

Please keep in mind, that an Identifier is comprised of an ID and the corresponding system that defines the scope of the ID (where the ID is drawn from) as mandatory parts, and optional more parts like an assigning authority or designations.

<img src="../../img/image-20220629104914841.png" alt="image-20220629104914841" style="zoom:50%;" />

::: example

In a Dutch system, there is the notion of a *Citizen Service Number*, Burger Service Nummer (BSN). A BSN is an identification scheme used by various governmental and public service organizations in the Netherlands to identify citizens.

In a dataset, the *Burger Service Nummer* is associated with the corresponding Identifier for Citizen Service Numbers, i.e. [2.16.840.1.113883.2.4.6.3](http://oid-info.com/get/2.16.840.1.113883.2.4.6.3).

:::

## Mappings between Scenarios and Templates/Profiles

This mapping concerns associations between a specific Transaction of a Scenarios and Templates, so-called Representing Templates. It defines that a whole Scenario Transaction can be technically represented using instances of the Template / Profile definition of the linked Representing Templates.

<img src="../../img/image-20220629112640720.png" alt="image-20220629112640720" style="zoom:50%;" />

## Mappings between Dataset Concepts and whole Templates / Profiles or parts thereof

This mapping concerns associations between Dataset Concepts and whole Templates / Profiles or single elements of a Template/ Profile. This allows to define links between groups or items of Dataset Concepts and their technical representation as Template / Profile elements or smaller Templates / Profiles as a whole.

<img src="../../img/image-20220629112811216.png" alt="image-20220629112811216" style="zoom:50%;" />

## Mappings between Dataset Concepts of two Datasets

This mapping concerns associations between Dataset Concepts of two different Datasets. 

<img src="../../img/image-20220629113547207.png" alt="image-20220629113547207" style="zoom:50%;" />

The speciality is, that the maps are qualified and direction dependent. There may be also Concepts in Dataset A that have no corresponding counterpart in Dataset B.

## Quality of Terminology Mapping and Dataset Concepts

Terminology or Dataset Concepts Mappings may carry a quality indicator following ISO/TR 12300:2014 *Health informatics, Principles of mapping between terminological systems*. In addition, [FHIR Concept Map Relationships](http://hl7.org/fhir/codesystem-concept-map-relationship.html) are supported.

<img src="../../img/image-20230524115946188.png" alt="image-20230524115946188" style="zoom:50%;" />

### relatedTo

The concepts are related to each other, and have at least some overlap in meaning, but the exact relationship is not known.

::: note FHIR

FHIR *related-to* = The concepts are related to each other, but the exact relationship is not known.

:::

### equal 

The definitions of the concepts are exactly the same (i.e. only grammatical differences) and structural implications of meaning are identical or irrelevant (i.e. intentionally identical)

::: note ISO 12300

ISO 12300 *Rating 1* = Equivalence of meaning; lexical, as well as conceptual. 

:::

::: example

Asthma and asthma

ovarian cyst and cyst of ovary

:::

### equivalent

The definitions of the concepts mean the same thing (including when structural implications of meaning are considered) (i.e. extensionally identical)

::: note ISO 12300 / FHIR

ISO 12300 *Rating 2* = Equivalence of meaning, but with synonymy. 

FHIR *equivalent* = The definitions of the concepts mean the same thing.

:::

::: example

ureteric calculus and ureteric stone

gall stones and cholelithiasis

:::

### wider 

The target mapping is wider in meaning than the source concept

::: note note ISO 12300 / FHIR

ISO 12300 *Rating 3* = Source concept is broader and has a less specific meaning than the target concept/term.

FHIR *source-is-broader-than-target* = The source concept is broader in meaning than the target concept.

:::

::: example

obesity and morbid obesity

diabetes and diabetes mellitus type II

:::

### narrower

The target mapping is narrower in meaning than the source concept. The sense in which the mapping is narrower SHALL be described in the comments in this case, and applications should be careful when attempting to use these mappings operationally

::: note note ISO 12300 / FHIR

ISO 12300 *Rating 4* = Source concept is narrower and has a more specific meaning than the target concept/term.

FHIR *source-is-narrower-than-target* = he source concept is narrower in meaning than the target concept.

:::

::: example

feels ugly and self-image finding

acute renal failure syndrome secondary to dehydration and acute renal failure syndrome

:::

### unmatched

There is no match for this concept in the destination concept system.

::: note ISO 12300 / FHIR

ISO 12300 *Rating 5* = No map is possible. No concept was found in the target with some degree of equivalence (as measured by any of the other four ratings).

FHIR *not-related-to* = This is an explicit assertion that the target concept is not related to the source concept.

:::

### Deprecated Relationships

The following relationships are still supported but **deprecated**.

#### subsumes (deprecated)

The target mapping subsumes the meaning of the source concept (e.g. the source is-a target)

#### inexact (deprecated)

The mappings overlap, but both source and target cover additional meaning, or the definitions are imprecise and it is uncertain whether they have the same boundaries to their meaning. The sense in which the mapping is different SHALL be described in the comments in this case, and applications should be careful when attempting to use these mappings operationally.

#### disjoint (deprecated)

This is an explicit assertion that there is no mapping between the source and target concept.

