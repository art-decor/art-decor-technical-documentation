---
permalink: /introduction/terminology
---
# Terminology

## Definition

::: definition TERMINOLOGY

Terminologies associate codes with real world concepts.

<adimg terminology/> is the symbol used throughout the app.

The subsumed artefacts are:

<adimg valueset/> [Value Set](../valueset), <p></p>

<adimg codesystem/> [Code Systems](../codesystem).<p></p>

:::
