---
permalink: /introduction/implementationguide
---
# Implementation Guide

## Definition

::: definition Implementation Guide

An Implementation Guide (IG) is a collection of artefacts such as datasets, terminologies or data format specifications, e.g. CDA templates or FHIR profiles, and is meant to be used primarily by Implementers for an Implementation <adimg implementation/> in an applications.

<adimg implementationguide/> is the symbol used throughout the app.<p></p>

:::

## CDA Product Line IGs <adimg template/>



## FHIR Product Line IGs <adimg profile/>



## Packages <adimg package/>



