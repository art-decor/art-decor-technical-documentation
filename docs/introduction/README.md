---
permalink: /introduction
---
# Welcome to ART-DECOR®

<img src="/topmenu-ad-logo.png" alt="File:Adeg-logo.png" style="float: right; width: 50%;" />

## A short "what is it?"

ART-DECOR® is an open-source tool suite that aims at interoperability solutions in the healthcare sector.

The tool is used in more than 80 projects worldwide, mainly in Europe, the server park houses more than 10,000 artifacts such as CDA templates and value sets from various teams of experts (governance groups), from small and medium-sized interoperability projects to large national and international infrastructure projects, for example in Austria, Switzerland, Poland.

The tool supports the creation and maintenance of HL7 templates, value sets, scenarios and data sets. The tool features cloud-based federated Building Block Repositories (BBR) for Templates and Value Sets. It supports comprehensive collaboration of team members within and between governance groups.

ART-DECOR® allows separation of concerns and different views on a single documentation for different domain experts and common documentation done by multidisciplinary stakeholders of healthcare information exchange.

|            ART-DECOR®<br />Features and Artefacts            | Stakeholders                                                 |
| :----------------------------------------------------------: | ------------------------------------------------------------ |
| <adimg dataset/> <adimg scenario/><br />Dataset and Scenario | Caregivers, Healthcare Providers, Medical Experts and Researchers |
| <adimg terminology/> <adimg identifier/><br />Terminology and Identifier | Terminologists                                               |
|           <adimg rules/><br />Profile and Template           | Analysts, Modellers and Template/Profile Creators            |
| <adimg issue/><br />Issue and Change Management, Publications, Reporting | Project leads                                                |
| <adimg implementation/><br />Implementation Support, Production Support | Vendors and Interface Specialists                            |

You can find more detailed information in our section What is [ART-DECOR®](/nutshell/)?

## Technical Documentation

This is the Technical Documentation for users of ART-DECOR. It is comprised of

- an introduction part (this part) about the tool and the various supported artefacts that help user to understand the principles of ART-DECOR; this page is the landing page of that section,
- a [documentation part](/documentation) that explains the use of the tool, focusing on the respective artefacts, plus additional documentation around the tool, e. g. what datatypes are supported or how to use multiple languages in a project, and
- an [administration part](/administration), intended to be typically read by people only who administer foreign servers with the software. 

## ART-DECOR® Expert Group

The kernel activities around the tool, concept, development and practice is done by the [ART-DECOR Expert Group](/adeg).

<img src="../img/800px-Adeg-logo.png" alt="File:Adeg-logo.png" style="zoom: 33%;" />

## ART-DECOR® Open Tools 

The [ART-DECOR® Open Tools](https://art-decor-open-tools.net) GmbH is the company that handles commercial aspects of the ART-DECOR® tool suite development and thus complements the [ART-DECOR® Expert Group](/adeg) that drives the development.

<img src="../img/800px-Adot-logo.png" alt="File:Adot-logo.png" style="zoom: 33%;" />

## ART-DECOR® Newsletter

::: tip SUBSCRIBE

If you want to stay in touch and get the freshest news from the team or hear about newest releases, features or stories, please subscribe to the [ART-DECOR Newsletter](http://seu2.cleverreach.com/f/250987-247353/) <Badge text="external" type="tip"/>.

:::

See here our [Privacy Policy](https://art-decor.org/privacy-policy/).

See here our [Newsletter Archive](/documentation/newsletters).

