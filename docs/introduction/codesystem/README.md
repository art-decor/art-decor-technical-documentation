---
permalink: /introduction/codesystem
---
# Code system

## Definition

::: definition CODE SYSTEM

A code system is a set of codes in a specific context.

<adimg codesystem/> is the symbol used throughout the app.<p></p>

:::

# General Introduction

## What is a Code System?

A code system is a resource that is maintained by individuals and/or organizations, typically has a specific goal or purpose and is published and/or updated at periodic intervals. Its purpose is to declare a collection of codes or identifiers that represent classes, categories, or individuals that are used for reporting, organizing, and/or reasoning about knowledge in some discipline, specialty, or domain. 

