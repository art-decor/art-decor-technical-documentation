---
permalink: /introduction/codesystem
---
# Code system

## Definition

::: definition CODE SYSTEM

A code system is a set of codes in a specific context.

<adimg codesystem/> is the symbol used throughout the app.

:::

# General Introduction

## What is a Code System?

A code system is a resource that is maintained by individuals and/or organizations, typically has a specific goal or purpose and is published and/or updated at periodic intervals. Its purpose is to declare a collection of codes or identifiers that represent classes, categories, or individuals that are used for reporting, organizing, and/or reasoning about knowledge in some discipline, specialty, or domain. 

The following definitions make use of the [HL7 V3 Core Principles](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#coreP_Coded) glossary from chapter *Coded Model Elements and their Vocabularies* and explains the term *Code System* and associated terms with intuitive examples.

Let us start with the introduction of the term Concept.

### Concept

A *Concept* is a unitary mental representation of a real or abstract thing – an atomic unit of thought. 



potatoe - murphy | Kartoffel - Erdapfel  (kein Gemüse Solanacea Tomate, Aubergine)

Tomato - Love Apple (French), wolf's peach (An old and fanciful name for the tomato), pomme d'amour (French), Lycopersicon (Latin), patient friendly terms

**pome** fruits, such as apples, pears and quince

=> a fleshy fruit (apple or pear or related fruits) having seed chambers and an outer fleshy part.



<img src="../../img/image-20220424100752341.png" alt="image-20220424100752341" style="zoom: 10%; float: right" />

Photo by [Marek Studzinski](https://unsplash.com/@jccards?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/apple-fruit?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)



CODE

A concept representation intended for use when representing a concept in a computable manner. For example, passing into a decision support tool or for use in data exchange. Some code systems use human-readable mnemonics for these.

DESIGNATION

Human consumable representation of the concept; may or may not be a string of characters (could be multimedia); generally subject to language variants.



It should be unique in a given *Code System*. A concept may have synonyms in terms of representation and it may be a singleton, or may be constructed of other concepts (i.e. post-coordinated concepts).

*Concept*s, as abstract, language- and context-independent representations of meaning, are important for the design and interpretation of HL7 V3 models. They constitute the smallest semantic entities on which HL7 V3 models are built. The authors and the readers of a model use concepts and their relationships to build and understand the models; these are what matter to the human user of HL7 V3 models. The rest of the vocabulary machinery exists to permit software manipulation of these units of thought. 

### Code System

A *Code System* is a managed collection of *Concept Representations*, including codes, but sometimes more complex sets of rules and references, optionally including additional *Concept Representations* playing various roles including identifiers of the concepts, designations, etc. *Code System*s are often described as collections of uniquely identifiable concepts with associated representations, designations, associations, and meanings. Examples of *Code System*s include ICD-9 CM, SNOMED CT, LOINC, and CPT. To meet the requirements of a *Code System* as defined by HL7, a given concept identifier must resolve to one and only one meaning within the *Code System*; i.e. *Code Systems* used in HL7 must not have homonymy. In the HL7 Terminology Model, a *Code System* is represented by the *Code System* class.

Although *Code System*s may be differentially referred to as terminologies, vocabularies, or coding schemes, HL7 considers all such collections ‘*Code System*s’ for use in Version 3 as described in this document. At a minimum, *Code System*s have the following attributes:

- An identifier that uniquely identifies the *Code System*. For HL7 conformant model instances, this SHALL be in the form of a [UID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-UID). This UID will take one of three forms: an ISO [OID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-OID), a [UUID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-UUID) or an HL7 [RUID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-RUID). If the code system is identified by an OID, this OID SHALL be registered in the HL7 OID registry, unless the code system is a [local code system](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-local_code_system), in which case it MAY be registered in the HL7 OID Registry. The HL7 OID Registry may be found at [http://www.hl7.org/oid/index.cfm](http://www.hl7.org/oid/index.cfm?ref=common)
- A description consisting of prose that describes the *Code System*, and may include the *Code System* uses, maintenance strategy, intent and other information of interest.
- Administrative information proper to the *Code System*, independent of any specific version of the *Code System*, such as ownership, source URL, and copyright information.

The *Concept Representations* in a *Code System* may also be augmented with additional text, annotations, references and other resources that serve to further identify and clarify what a concept is. A *Code System* also may contain assertions about relationships that exist between concepts in the *Code System*.

A *Code System* is typically created for a particular purpose and may include finite collections, such as concepts that represent individual countries, colors, or states. *Code System*s may also represent broad and complex collections of concepts, e.g., SNOMED-CT, ICD, LOINC, and CPT. Where possible, HL7 modelers faced with a requirement for a coded concept will reference an existing *Code System*. Some of these *Code System*s are replicated within the HL7 standard repository for stability or convenience, while others are documented as references. HL7 will only create a new *Code System* when an appropriate existing *Code System* is not available for use in HL7. Such is the case with Class Codes, which are defined and maintained by the HL7 organization. There are also cases where an otherwise appropriate external resource is not available due to licensing or other restrictions.

**Concept Representation**
A *Concept Representation* is a vocabulary object that enables the manipulation of a *Concept* in HL7. A *Concept Representation* exists in some form that is computable, and can be used in HL7 models and specifications. *Concept Representations* can take on a number of different roles in the structure and processing of vocabulary in HL7; these roles and functions are described below.

**Concept Identifier**
A *Concept Identifier* is a *[Concept Representation](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-concept_representation)* that may be published by the *[Code System](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-code_system)* author, and unambiguously represents the concept internally within the context of that *Code System*. Such an object used for identification, when combined with the unique identifier of the *Code System* itself (a machine-processable unique string), provides a globally unique and language-independent identification for the particular concept. This globally unique identification can be used in transactions and data records that span both space and time. In some cases, multiple synonymous identifiers may be present for the same concept.

**Code**
A *Code* is a *[Concept Representation](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-concept_representation)* published by the author of a *[Code System](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-code_system)* as part of the *Code System*. It is the preferred unique identifier for that concept in that *Code System* for the purpose of communication (preferred wire-format identifier), and is used in the 'code' property of an HL7 coded data type. Codes are sometimes meaningless identifiers, and sometimes they are mnemonics that imply the represented concept to a human reader. In *Code Systems* that contain more than one *Concept Identifier*, the one that should be used in HL7 as the *Code* should be explicitly declared as part of the HL7 Code System registration process ([see § 4](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#coreP_Identifying_Elements) above).

The meaning of a Code within a particular *Code System* entity is valid only within that *Code System*. 

::: example

For example, each table having enumerated codes in the HL7 Version 2.x standards represents a different *Code System*, since codes are sometimes used in different *Code System*s to have different meanings. 

* One example is the code "M" in the v2.x table 0001 Administrative Sex which signifies "Male," whereas the code "M" in the in the v2.x table 0002 Marital Status signifies "Married". 

* Another example is the code "1609-7" which signifies the Native American tribe "Sioux" in the Race & Ethnicity – CDC *Code System*(2.16.840.1.113883.6.238), and the code "1609-7" which signifies "Prolactin^1.5H post dose insulin IV" in the LOINC *Code System* (2.16.840.1.113883.6.1). 

:::

Codes do not have explicit semantics without their *Code System*s, and cannot be referenced without identifying the code system in which they are published.

**Designation**
A *Designation* is a *[Concept Representation](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-concept_representation)* that may be published by the *[Code System](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-code_system)* author, and is a language symbol for a concept that is intended to convey the concept meaning to a human being. A *Designation* may also be known as an appellation, symbol, or term. A *Designation* is typically used to populate the 'displayName' property of an HL7 coded data type.



The following table may help to clarify the differences in meanings and uses of these *Concept Representations.*

| Name        | Definition                                                   | Primary HL7 Use                                              | Examples                                                     |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Code        | A concept representation intended for use when representing a concept in a computable manner. For example, passing into a decision support tool or for use in data exchange. Some code systems use human-readable mnemonics for these. | Generally carried ‘on the wire’ in instances of coded attributes of models passed between or shared by different systems. | **HL7 AdministrativeGender:**  F**LOINC:**  6690-2**SNOMED CT:**  DE-10116  X100J  233607000  53084003:246075003=9861002   (all these codes represent the same SNOMED CT concept; the first is an IHTSDO Legacy Code, the second is an IHTSDO Read Code, the third is an IHTSDO Concept ID, and the last is an IHTSDO post-coordinated expression)**UCUM:**  L  mg/L |
| Concept Id  | A concept representation that is unique within the code system and that is used internally by the code system when referencing concepts. | Many code systems use the same value for Concept Code and Concept Identifier, therefore the Concept Identifier may be used in CD.code; however, it is often used as a component of maps and ontologies outside of HL7 interoperability standards. Some Code Systems have more than one Concept Identifier for a concept. | **HL7 AdministrativeGender:**  F**LOINC:**  6690-2**SNOMED CT:**  233607000**UCUM:**  m  g  L |
| Designation | Human consumable representation of the concept; may or may not be a string of characters (could be multimedia); generally subject to language variants. | Human language representation of the concept. A concept has 1..m of these, and they represent the set of candidates for the CD.displayName (v3) or CWE.Text/CWE.Alternate Text (v2). Sometimes used in CWE.Original Text depending upon the application. Often language-dependent, i.e. a set of English designations, a set of French designations, etc. | **HL7 AdministrativeGender:**  F  Female**LOINC:**  Leukocytes [#/volume] in Blood by Automated Count  WBC #Bld Auto  Leucociti**SNOMED CT:**  Pneumococcal pneumonia (disorder)  Bacterial pneumonia (disorder) {Causative agent (attribute) Streptococcus pneumoniae (organism)}**UCUM:**  Litre  mg/L  L  milligram/Litre  gram/Litre |

Code Systems

**Code System**
[Glossary](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-code_system): A *Code System* is a managed collection of *Concept Representations*, including codes, but sometimes more complex sets of rules and references, optionally including additional *Concept Representations* playing various roles including identifiers of the concepts, designations, etc. *Code System*s are often described as collections of uniquely identifiable concepts with associated representations, designations, associations, and meanings. Examples of *Code System*s include ICD-9 CM, SNOMED CT, LOINC, and CPT. To meet the requirements of a *Code System* as defined by HL7, a given concept identifier must resolve to one and only one meaning within the *Code System*; i.e. *Code Systems* used in HL7 must not have homonymy. In the HL7 Terminology Model, a *Code System* is represented by the *Code System* class.

Although *Code System*s may be differentially referred to as terminologies, vocabularies, or coding schemes, HL7 considers all such collections ‘*Code System*s’ for use in Version 3 as described in this document. At a minimum, *Code System*s have the following attributes:

- An identifier that uniquely identifies the *Code System*. For HL7 conformant model instances, this SHALL be in the form of a [UID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-UID). This UID will take one of three forms: an ISO [OID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-OID), a [UUID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-UUID) or an HL7 [RUID](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/datatypes_r2/datatypes_r2.html#dt-RUID). If the code system is identified by an OID, this OID SHALL be registered in the HL7 OID registry, unless the code system is a [local code system](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#gl-local_code_system), in which case it MAY be registered in the HL7 OID Registry. The HL7 OID Registry may be found at [http://www.hl7.org/oid/index.cfm](http://www.hl7.org/oid/index.cfm?ref=common)
- A description consisting of prose that describes the *Code System*, and may include the *Code System* uses, maintenance strategy, intent and other information of interest.
- Administrative information proper to the *Code System*, independent of any specific version of the *Code System*, such as ownership, source URL, and copyright information.

The *Concept Representations* in a *Code System* may also be augmented with additional text, annotations, references and other resources that serve to further identify and clarify what a concept is. A *Code System* also may contain assertions about relationships that exist between concepts in the *Code System*.

A *Code System* is typically created for a particular purpose and may include finite collections, such as concepts that represent individual countries, colors, or states. *Code System*s may also represent broad and complex collections of concepts, e.g., SNOMED-CT, ICD, LOINC, and CPT. Where possible, HL7 modelers faced with a requirement for a coded concept will reference an existing *Code System*. Some of these *Code System*s are replicated within the HL7 standard repository for stability or convenience, while others are documented as references. HL7 will only create a new *Code System* when an appropriate existing *Code System* is not available for use in HL7. Such is the case with Class Codes, which are defined and maintained by the HL7 organization. There are also cases where an otherwise appropriate external resource is not available due to licensing or other restrictions.

 5.1.2.1Post-Coordination in Code Systems

*Code System*s may provide a capability for the composition of complex concepts consisting of two or more existing concepts and formal relationships between them, using a defined syntax. A complex concept which has been assigned a unique concept identifier or code and is published in the *Code System* is referred to as "pre-coordinated". A representation of a complex concept that is created externally to the *Code System* (i.e., by an outside entity or user) is called a *post-coordinated expression*. [[2](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#fn2)] Note also that it is possible (although discouraged) for a post-coordinated expression to be constructed that represents a concept which already exists and is published in the *Code System*. Some commonly used code systems in health care IT, such as SNOMED-CT and UCUM, provide a formal mechanism for *post-coordination*. The mechanism (syntax) that permits this construction of new concept representations is referred to as a *grammar for post-coordination*.

A simple example to illustrate post-coordination is a case where there exists a concept code for the anatomical object 'arm', a concept code for the laterality identifier 'left', and a code used in post coordinated expressions for 'has laterality'. Although the code system may not have any code for 'left arm', it can be represented in the code system with the three codes, e.g. 'arm' - 'has laterality' - 'left'. HL7 permits the use of such expressions in the coded data types for representation in model instances. Another example is the concept "excision of pituitary gland" which may be pre-coordinated in the code system with a unique identifier and a designation of “hypophysectomy”. An alternative definition for this concept can be obtained by post-coordinating codes for "brain excision" and "pituitary gland," without adding a reusable designation to the source system. Post-coordination allows us to derive complex composite terms by combining the concepts they are derived from, which reduces the number of concepts in the source system. Post-coordination requires the explicit resolution of complex questions of context and semantic precedence (e.g., the preposition "of" in this example).

 5.1.2.2Code System Versions

*Code System*s evolve over time. Changes occur because of corrections and clarifications, because the understanding of the entities being modeled evolves (e.g., new genes and proteins are discovered), because the entities being modeled change (e.g., new countries emerge; old countries are absorbed), or because the assessment of the relevance of particular entities within the knowledge resource change (e.g., the addition of new parent-child relationships to existing codes in SNOMED-CT). Depending upon how well the *Code System* adheres to good vocabulary practices, the changes in meaning could be significant. Therefore it can be important to know which version of a given *Code System* was used in the creation of HL7 model instances, the recording of coded data, or in some cases the creation of the HL7 models.

The HL7 model depends on the meaning of a specific concept identifier remaining constant over time, independent of the particular version of the knowledge resource. In cases where the knowledge resource itself doesn’t enforce this (e.g. older versions of ICD-9-CM, where codes were retired and subsequently re-used to represent something different), then the version of the *Code System* in which the code is associated with the desired meaning must be included in the HL7 data type carrying the instance of the *Code*. Where codes do not change meaning over time (or where the meaning change is sufficiently subtle that any such meaning changes are deemed by the user as immaterial for the purpose for which the code is being carried), the version of the *Code System* is not required to be noted with the *Code* and the *Code System*. Some code systems have changes that are significant over time, and for these a new code system has been designated rather than a new version of the same code system. This is the case, for example, with the German diagnostic classification codes: ICD10GM2009 is NOT a new version of ICD10GM2008, but a completely new code system, with its own independent versioning.

*Code System*s employ a variety of mechanisms to signify a particular version; these are defined by the author and/or distributor of the *Code System*. Most use a number, or set of numbers, others may use alphanumeric strings or dates. There exists no standard representation of version identifiers that has been adopted by authors of *Code System*s. Any version identifier that can be represented in an HL7 string data type can be used in HL7. When a code system is registered in the HL7 OID Registry so that conformance may be tested and/or measured, the registration should include precise and detailed information on how code system versions are defined and identified. The registration information should be explicit enough to ensure that independent applications recording the code system version used to capture a particular data element will be recognized by other applications with whom they exchange information. Considerations include factors like use of upper-case vs. lower-case characters, the presence and types of separators, use of leading characters, order of components, and potentially even character set. For example "02.5" would not match with "2-5"; "2001/03/05" would not match with "05-03-2001", etc. The registration should indicate enough information about versioning and the syntax of the version string so as to make the machine machine processing of the version string a reasonable expectation.

The machinery for *Value Set*s (see below) and for conformance must operate over multiple code systems that use these various different means to specify their versions. In order for this to operate properly across these different mechanisms, all HL7 internal machinery uses the effective date for a particular code system version when doing such processing. In order for this to work correctly, for externally maintained terminologies that have named or numbered releases, a table must be maintained that shows the modification dates for the named or numbered releases. For externally maintained terminologies that maintain modification dates for each individual code change, no additional information is needed.

 5.1.3Value Sets

A *Value Set* represents a uniquely identifiable set of valid concept identifiers, where any concept identifier in a coded element can be tested to determine whether it is a member of the *Value Set* at a specific point in time. A concept identifier in a *Value Set*may be a single concept code or a post-coordinated expression of a combination of codes.

*Value Set*s exist to constrain the permissible content for a particular use, e.g. for use in analysis, display to a user, etc. For HL7, value sets constrain the permissible content for a coded element in an HL7 information model or data type specification. Thus, at implementation time, a coded element must be associated with a list of codes that represent the possible concepts that can be represented in that element at the time of use. *Value Set*s cannot have null content, and must contain at least one *Concept Representation*. [[3](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#fn3)] Identical codes from different *Code System*s are allowed because they can be disambiguated by identifying the *Code System* they come from.

Ideally, a given Concept in a *Value Set* should be represented only by a single code. However, in unusual circumstances, a given concept in a *Value Set* can have more than one code (e.g. where a different case is used to signify the same concept, as 'l' and 'L' in UCUM for 'litre'); it is generally rare that *Code System*s from which *Value Sets* are drawn contain multiple codes for the same concept.

*Value Set* complexity may range from a simple flat list of concept codes drawn from a single *Code System* to an unbounded hierarchical set of implicit post-coordinated expressions drawn from multiple *Code System*s. A *Value Set* may include concepts directly defined in code systems, and may also include concepts defined with post coordinated expressions (using those code systems that support post coordination).

A sub-*Value Set* is a sub-set of a parent *Value Set* (the superset). A sub-*Value Set* is generally created as part of the successive constraining process of HL7 V3 development. 

Because the collection of codes in a *Value Set* may be unbounded, and may be dependent upon the time the collection is used, a *Value Set* is only persisted as its *Value Set Definition*, which is a machine-processable set of 1 or more formalisms that permit a specific collection of coded concepts at a given point in time to be reliably reproduced. The collection of codes that is generated from the *Value Set Definition* are called the *Value Set Expansion*. Whether or not the generated collection, the *Value Set Expansion*, is also persisted is an implementation choice. *Value Set Definition*s permit the expanded collection of *Code*s to change over time with no change to the definition, if the user so desires. *Value Set Definition*s can also be revised over time to reflect changes to underlying code systems, improved understanding of the requirements for the *Value Set*, etc. However, changes to a *Value Set Definition* must ensure that the *Value Set Expansion* remains consistent with the use of the *Value Set*; i.e. changes to a *Value Set Definition* should not cause issues with specifications or implementations that are already using the *Value Set* and that may not make reference to the specific version of the definition. Even with this caveat, such changes should still be approached with great care, as changes in value set expansions may have adverse effects on some applications using the value set.

A *Value Set* optionally has a description, but this is not intended to describe the semantics of the *Value Set*; a *Value Set* has no intrinsic semantics separate from its *Value Set Expansion*. The description should capture its intended use, which is needed for ensuring integrity for its use in models across future changes.

HL7 International defines *Value Set*s as part of its vocabulary maintenance procedures, and the definitions of these are incorporated within the published HL7 vocabulary artifacts; these are commonly called *HL7 Value Sets* or *Internal Value Sets*. Other organizations also define *Value Set*s for their own use in HL7 models, and these are referred to (in HL7) as *External Value Sets*. In most circumstances, HL7 International only defines and maintains *Value Set*s that are bound in *Binding Realms* (see *Bindings*) controlled by HL7 International or are used in specifications published by HL7 International.

 5.1.3.1Value Set Definition Methods

Two approaches can be used to define the contents of a *Value Set*:

- *Extensional Definition*: Explicitly enumerating each of the Value Set concepts.
- *Intensional Definition*: Defining an algorithm that, when executed by a machine (or interpreted by a human being), yields such a set of elements.

An *Extensional Definition* is an enumeration of all of the concepts within the *Value Set*. *Value Set*s defined by extension are composed of explicitly enumerated sets of concept representations (with the *Code System* in which they are valid). The simplest case is when the *Value Set* consists of only one code.

An *Intensional Definition* is a set of rules that can be expanded (ideally computationally) to an exact list of concept representations at a particular point in time. While the construction rules can potentially be quite elaborate, HL7 has identified a small set of rules as explicit formalisms that appear to be useful in most circumstances where HL7 models are used. In order to share *Value Set Definitions* between systems if needed, HL7 strongly recommends that the HL7-created definition formalisms be used whenever possible.

If any of the definition statements in the collection making up the *Value Set Definition* is intensional, then the entire *Value Set* is considered to be intensional.

Some examples of the HL7-created *Value Set Definition* formalisms available to specify *Intensional Definitions* are:

- All active unique identifiers from a given coding system (e.g. "All codes from ISO3166-1 Country Codes")
- All unique identifiers that participate in a specified relationship with a given concept in a coding system, which may or may not include the specified code itself (e.g. "All LOINC codes having a SYSTEM of BLD")
- The transitive subset of a given code along a given relationship type, including or excluding the code itself (e.g. "All subtypes of 112283007 Escherichia coli in SNOMED CT" including the head code)
- All codes matching a particular Regular Expression
  [[4](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#fn4)]
- A *nested Value Set* definition in which a *Value Set* entry references another *Value Set* (a child *Value Set*). There is no preset limit to the level of nesting allowed within *Value Set*s. *Value Set*s cannot contain themselves, or any of their ancestors (i.e., they cannot be defined recursively). A *Value Set* whose definition includes *nested Value Set*s is always considered intensionally defined. (e.g. "Include the Value Set of Category A NIAID agents and the Value Set of Category B NIAID agents")
- An explicit set of enumerated codes from a coding system (as in an *Extensional Definition* above) (e.g. "US, CA, UK, DE, JP codes from ISO3166-1 2-character country codes")
- Unions, intersections and exclusions of any of the above.

Note that these are examples of some *Intensional Definition* functions. Many others are possible, and HL7's formalisms will be extended over time as the marketplace requires. Those used in support of HL7 vocabulary that is part of the HL7 ballot publications may be found in the MIF (Model Interchange Format) definition. Note that definitions that are not part of the current HL7 meta-model definition are local extensions, and are not fully HL7 compliant.

The *Intensional Definition* must be specific enough that it is always possible at a point in time (within specific versions of the *Code System*s referenced) to determine whether a given value (including post coordinated collections of codes) is a member of the *Value Set*. For example, an *Intensional Value Set* definition might be defined as, “All SNOMED CT concepts that are specializations of the SNOMED CT concept ‘Diabetes Mellitus.’”

A common method for specifying an *Intensional Definition* is to identify a concept (often referred to as a root concept or head code) that has descendants, indicating that the *Value Set Expansion* contains all of the descendants of the identified concept. The *Intensional Definition* may also specify whether the tree of the concepts to be included in the expanded *Value Set* includes or excludes this root concept. If this root concept at the top of a tree of concepts is to be included in the expanded set, the *Intensional Definition* is considered *Specializable*. If it is explicitly excluded, and is identified just as a referent for the included tree of subordinate concepts, the *Intensional Definition* is termed *Abstract*.

Being *Abstract* or *Specializable* indicates that for an intensionally defined *Value Set* whose definition indicates including a hierarchical tree of concepts from a single root concept, the root concept of the tree may not be populated in the model element to which the *Value Set* is bound. The root concept may also be thought of as 'non-selectable', i.e. the root concept will not be included in the *Value Set Expansion*. While the terms 'abstract' and 'specializable' have been used historically in HL7, and many existing tools display the *Value Set* in this way; these are simply convenient labels. Note that two different *Value Set*s may have what appear to be identical definitions, but one is abstract and the other specializable. These are two distinct *Value Set*s and two distinct definitions, as they will produce value set expansions that differ by the root concept. A *Value Set Definition* may be made up of more than one definition statement of various kinds.

 5.1.3.2Unique Meaning Rule

HL7 International recommends that, whenever possible, a *Value Set* be drawn from a single *Code System*. This is not always practical, however, and when a *Value Set Definition* includes references to more than one *Code System*, designers should never incorporate two identical concepts from two different *Code System*s in the same *Value Set Definition* in order to ensure that the set does not contain more than one globally unique identifier that denotes the same concept. Care must be taken to ensure that every concept represented in the *Value Set* has only one possible globally unique identifier. However, designers may choose to use multiple concepts that are very similar as long as there has been consideration of the impact, and how these may be differentiated by users of application systems.

For example, both CPT and LOINC have codes that represent Hematocrit, meaning that there are two possible globally unique identifiers with approximately the same meaning: 2.16.840.1.113883.6.1#4544-3 (for LOINC) and 2.16.840.1.113883.6.12#85014 (for CPT-4). If both of these are possible choices in a *Value Set*, data encoded with one code may be missed when searching using the other code, and interoperability in general may suffer. Further, dividing semantic stewardship for the *Value Set* can introduce semantic traps: the different source system owners may define their concepts with differences that escape one user's notice, but have a significant effect on another user's interpretation. In the above example, the CPT code specifies a test order, but does not require that the test be filled by a specific method: it refers to any Hematocrit. The LOINC code, on the other hand, specifies the automated count method, specifically excluding packed cell volume tests. Equating these codes may be permissible in some contexts, but would be incorrect in others.

Extra care must be taken to assure that overlapping references do not appear in intensionally defined *Value Set*s, especially as there is no easy way to automatically determine when this situation exists. Again, the easiest way to avoid this is to avoid the use of multiple *Code System*s in a single *Value Set* wherever possible.

 5.1.3.3Value Set Expansion

To obtain a list of enumerated concepts, *Value Set*s must be *expanded*. This means that the *Value Set Definition* must be converted to a list of concept representations at a point in time. This list usually consists of *codes* so that the *Value Set Expansion*may be used in populating or validating communicated model instances, but may alternatively be a list of *designations*. While this is straightforward for extensional *Value Set Definition*s, an intensional *Value Set Definition* must be resolved to a *Value Set Expansion* by processing the rules contained in the *Value Set Definition*. This process should record the exact set of definitional rules used to resolve the *Value Set* using the specified definition version (see [below](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#coreP_Coded_Properties-value-sets-versioning)). *Value Set Expansion* can be done as early as the point of *Value Set* definition or as late as run time. For statistical, legal, and analytical purposes, good practice dictates that a system which captures a coded value should be capable of reconstructing the *Value Set Expansion* that was in effect at the time a given code was selected.

A *Value Set Definition* does not prescribe the structure of a post-coordinated expression in a *Value Set Expansion*, as there is often more than one way to specify a specific concept that is legal within a post-coordination grammar. Also multiple valid *Value Set Definition*s could result in the identical *Value Set Expansion*. For intensional *Value Set*s, the set of concepts contained in the expansion will generally change when the definition is changed (a new version of the *Value Set Definition*), but also may change with the identical version of the definition if the underlying code systems change, and the changes are part of the *Value Set Expansion*. This can be controlled if the definition statement refers to specific code system versions, thereby prohibiting the expansion from changing when the code system changes with a new version release.

Some intensionally defined value sets, particularly those that permit post-coordinated expressions, cannot be expanded in the usual sense because their expansions are infinite (or at least so large as to make enumerating all possible values impractical). For example, the UCUM post-coordination grammar allows units to be combined together by multiplication, such as m, m2, m3, etc. The grammar places no upper limit on what multiplication is possible, so m57 and m2341688 are also legal (though highly unlikely) codes. A *value set* whose definition includes "all codes from UCUM" could thus never be fully enumerated (expanded). In these circumstances, implementations must instead use the algorithms that can process the formalisms in the *Value Set Definition* to evaluate a specified code to see if it meets the constraints of the *Value Set* rather than fully expanding the *Value Set* and performing a simple look-up on the resulting list.

 5.1.3.4Value Set Versioning

A *Value Set Definition* can change over time. New codes may be added to or removed from an *Extensional Value Set* definition, and/or the rules used to construct an intensionally defined *Value Set* may be changed. When a *Value Set Definition* changes, it should be done in a way that ensures both the old and new versions are available for comparison, and for the use of models that explicitly reference the old version.

There are multiple strategies for tracking *Value Set* versions. Two of the most common are:

1. to increment the version number each time a change is made to the *Value Set Definition*
2. to track modification dates for each change to the *Value Set Definition*.

In HL7 standards, *Value Set* versions are determined by effective date (the date at which the *Value Set* version became effective), and not by available date (the date the *Value Set* version was made available within an organization) or by a version number. This policy has the following implications:

1. For enumerated *Value Set*s maintained by HL7, the activation date and deactivation date for individual codes in the *Value Set* must be maintained as part of the *Value Set*. 
2. For intensionally defined *Value Set*s, the activation date and superseded date must be recorded (tracked) each time the logic of the definition is changed.

A *Value Set Version* thus contains a specific *Value Set Definition*, and may also contain descriptive and administrative information, as well as data to assist in curation.

The set of codes comprising the expansion of an intensionally defined *Value Set* at a point in time may change at different points in time, even using the same definition (e.g. when the underlying *Code System* changes in certain ways). These different expansions are not different versions of the *Value Set*; versioning pertains to modifications of the *Value Set Definition*. The curation needs of persisted *Value Set Expansion*s are outside the scope of this specification, as there are performance, architecture, and synchronization issues surrounding persistence of *Value Set Expansions*.

 5.1.3.5Value Set Immutability

At the time a *Value Set* is defined, the business Use Case or information model design may require that the definition of the *Value Set* never change. In order to support this, a *Value Set* has a property called "immutability" which is a Boolean and refers to the *Value Set Definition*. If TRUE, the *Value Set Definition* may not be changed in the future. A new *Value Set* must be created. (There can be no new versions of the *Value Set*). If FALSE, the *Value Set* may be changed, and the versioning mechanism will permit traceability of such changes. Note that an immutable *intensional* definition may still result in an expansion that changes over time if the definition does not explicitly identify the versions of the code systems that it references, thus an isImmutable property set to TRUE will not necessarily make a *Value Set* expansion unchangeable over time. Therefore, to completely freeze a *Value Set* expansion, it must have the isImmutable property set to TRUE and the definition must either be *Extensional* or *Intensional* where reference is only to specific versions of the underlying code systems.

 5.1.4Concept Domains

An HL7 *Concept Domain* is a named category of like concepts (a semantic type) that is specified in the *Vocabulary Declaration* of an attribute in a information model or property in a data type, whose data types are coded or potentially coded, and may be used in a *Context Binding*. *Concept Domain*s exist to constrain the intent of the coded element while deferring the binding of the element to a specific set of codes until later in the specification development process. Thus, *Concept Domain*s are independent of any specific vocabulary or *Code System*. *Concept Domain*s are hierarchical in nature, and each hierarchy represents a constraint path from a broader to a narrower semantic category. In HL7's base models – the RIM and the Abstract Data Types specification – all coded elements are tied to these abstract definitions of the allowed types of concepts.

The deferral of the association between a coded element and its allowed coded values is useful because it may not be possible to achieve consensus on the *Value Set* to be used within a model at the level at which the model is being developed. For example, it may be possible to gain international consensus on messages for submitting insurance claims for health care services. However, gaining international consensus on a single set of billing codes is unlikely any time soon.

*Concept Domain*s are universal in nature (independent of any *Binding Realm*), so the name for a *Concept Domain* should never contain any reference to a specific *Binding Realm*. *Concept Domain*s are also present to permit binding to more than one specific terminology (in general), so the names also should not contain a reference to a specific coding system. *Concept Domain*s are registered with HL7 International: they are proposed as part of the HL7 standards development process and are approved by the RIM harmonization process. Both processes are described in the HL7 Development Framework (HDF).

A *Concept Domain* is documented by specifying a name and a narrative definition. In addition, at least three concept examples that represent possible values for the attribute or data type property are required to illustrate the semantic space. [[5](http://www.hl7.org/documentcenter/public/standards/V3/core_principles/infrastructure/coreprinciples/v3modelcoreprinciples.html#fn5)] The examples should represent concepts that characterize the intent and purpose of the *Concept Domain*. This can be accomplished in one of the following ways:

- Including three example concepts as part of the narrative definition;
- *Context Binding* the *Concept Domain* to a *Value Set* that contains at least three example concepts in the context of the Example *Binding Realm*; or
- *Context Binding* the *Concept Domain* with a *Value Set* that contains a set of concepts which completely cover the intended meaning of the domain, using either the Universal or Representative *Binding Realm* as a context.

 5.1.4.1Examples of Concept Domains

The *Concept Domain* named 'HumanLanguage' carries the description, "Codes for the representation of the names of human languages". The set of concept identifiers that represent different human languages can be drawn from different *Code System*s, depending on which *Binding Realm* or sub-*Binding Realm* is creating the message. For example, the United States *Binding Realm* may choose to use a *Value Set* that includes concept identifiers for various Native American languages, while New Zealand may find such a *Value Set* inappropriate.

 5.1.4.2Sub-Domains

One *Concept Domain* may be defined to be "sub-domain" of another. This means that the intended meaning and reference of the sub-domain is intended to be narrower than the meaning of the parent. For example, there is a domain ObservationMethod, with a sub-domain of GeneticObservationMethod. This is not intended to be an ontological assertion; its primary purpose is to indicate that all of the coded identifiers in a *Value Set* that is associated with the sub-domain should also be valid identifiers for the parent domain, though the reverse may not be true.

 5.1.5Binding Realms

A *Binding Realm* refers to a named interoperability conformance space, meaning that all information models within a particular *Binding Realm* share the same conformance bindings. In non-technical terms, it can be considered a dialect where speakers use the semantics of the language but agree to use certain terms that are specific to their community; it is a context of use for terminology in HL7 models. *Binding Realm*s may be defined by jurisdictional boundaries and are often HL7 Affiliates, or sub-national jurisdictional entities within the Affiliate purview. *Binding Realm*s may also be based on other factors such as type of patient (e.g. human vs. veterinary, pediatric vs. geriatric), type of medicine (e.g. dentistry vs. psychiatry), etc..

The *Binding Realm* is in effect for each section of the instance where it is declared. For example a message or document might declare one *Binding Realm* for the 'root' of the instance, and may declare a different *Binding Realm* for some fragment within the instance. All model instances must declare a particular *Binding Realm* (or sub-*Binding Realm*) based on the context used to determine the terminologies for that instance or instance fragment. A *Binding Realm* is used to provide and manage the bindings of *Value Set*s to reflect rules within the interoperability conformance space.

Each *Binding Realm* has a unique code and is under the stewardship of either HL7 International or an HL7 Affiliate. For example, the *Binding Realm* Germany has a code of DE, and the steward is HL7 Germany. The set of all *Binding Realm* codes is maintained by HL7 International. In order to enable conformance, the Binding Realm, declared in the RIM in InfrastructureRoot.realmCode, is in effect for each section of the instance where it is declared. In the interest of maximizing interoperability, interoperability spaces should be as large as practical: *Binding Realm*s are preferred to be large-grained. As an extreme, if each hospital department had its own *Binding Realm*, chances for semantic interoperability would be very low.

*Binding Realm*s can be defined as a poly-hierarchy where broad *Binding Realm*s might be sub-divided into narrower, more precise *Binding Realm*s. For example, a *Binding Realm* of "Canada Human Dental" might be a sub-*Binding Realm* of both "Canada Dental" and "Canada Human", which would in turn be sub-realms of "Canada" which is itself a sub-realm of Universal. This hierarchical structure reflects the relative containment of the interoperability spaces being defined and allows for consensus on different types of codes at different levels of the interoperability environment. The hierarchical relationship between sub-*Binding Realm*s is determined by the Affiliate responsible for those *Binding Realm*s.

## Introduction to the use of Code Systems in ART-DECOR® Release 3

## Code Systems supported by ART-DECOR® Release 3

