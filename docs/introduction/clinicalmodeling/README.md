---
permalink: /introduction/clinicalmodeling
---
# Clinical Modeling Core Principles

::: danger

This page is un der construction 

:::

## Introduction

## Stakeholders

- Healthcare Providers
- Terminologists
- Analysts, Modelers
- Vendors, Interface Specialists
- Research

## Ten threshold concept of Basic Clinical Modeling

- You are not alone! – Teamwork
- Start talk talkative! – Data elements basics
- Divide and conquer! – Grouping
- Describe the kind of data! – Data types
- Guide to reality! – Examples, examples, examples
- Tell me why! – Evidence
- Foster proper answers! – Operationalization
- Profit from other art! – Re-usability and inheritance
- Document your expectation! – Cardinality and more
- Cycle through life! – Status and Versioning

Work based on a papers and standard work: 

- Team competencies and educational threshold concepts for clinical information modelling. Scott, Philip; Heitmann, Kai. Decision Support Systems and Education. ed. / John Mantas; Zdenko Sonicki; Mihaela Crişan-Vida; Kristina Fišter; Maria Hägglund; Aikaterini Kolokathi; Mira Hercigonja-Szekeres. IOS Press, 2018. p. 252-256 (Studies in Health Technology and Informatics; Vol. 255). 
  - https://researchportal.port.ac.uk/portal/files/11936801/Team_competencies_and_educational_threshold_concepts.pdf
  - http://ebooks.iospress.nl/publication/50513

- International Organization for Standardization (ISO), Technical Committee ISO/TC 215: Health informatics — Clinical information models — Characteristics, structures and requirements. ISO 13972:2022(E) 

## You are not alone! – Teamwork

Active participation of clinicians necessary but requires (communication, soft, …) skills Collaborative environment with multi-stakeholder communication Healthcare providers must learn how to communicate well with non-clinical experts and add Process / content analysts and terminologists Perform team-building activities 

## Start talk talkative! – Data elements basics

- The data elements shall have a short name that allows other to gather what this item means.
- This needs to be simple text in natural language, not acronyms or abbreviations.
  - Examples: Date of birth, Body weight, Type of specimen, Anatomic site, Specimen Collection Time
- Data elements shall always include a description that precisely explains in additional words what the subject of the item is.
- Examples
  - The date and time the specimen was collected
  - The reason why the immunization was refused
  - The administrative gender of a person
- Introduce an identifier (short number for example) for each data element, at least unique within the data set, eventually globally unique; this makes it much easier later to refer to the data element in further discussions.
- Agreement about naming conventions so that you are consistent

## Divide and conquer! – Grouping

- Grouping essential
- Not a natural approach for many healthcare providers
- Problem can be reinforced by using inappropriate tools
- Navigation
- The “tree” can be expanded to see its components.
- Good tools intuitively support this

## Describe the kind of data! – Data types

- Data set is a definition of elements that will be populated with real values
- Datatypes: What is the expected nature of the data

### Count

Countable (non-monetary) quantities

- 7, 11, 298
- Used for countable types such as: number of pregnancies, steps (taken by a physiotherapy patient), number of cigarettes smoked in a day

### Code

A code for a real-world thing; a code is a system of valid symbols that substitute for specified concepts, usually defined by a formal reference to a terminology or ontology, but may also be defined by the provision of text. Dates and Times – represents an absolute point in time specified to the day or additionally the time. The partial form is used for approximate dates.

### Dates and Times

Represents an absolute point in time specified to the day or additionally the time. The partial form is used for approximate dates.

### Text/String

May contain any amount of legal characters arranged as words or sentences. Formatting may be included (text) or not (string).

### Quantity

A measurement comprising a decimal number of arbitrary precision and a unit. There are some "special" quantities used in healthcare: for time durations Duration, for monetary amounts Currency.

- 87 kg, 1.83 cm, 10 mg/mL

### Duration

A quantity representing a period of time with respect to a notional point in time, which is not specified. A sign may be used to indicate the duration is “backwards” in time rather than forwards.

### Identifier

Type for representing identifiers of real-world entities. “Pure” identifiers are meaningless, but that is not usually the intent

- Example: “F564738573” (identifier), German Patient Identifier (context)

### Ratio

unlike its usage in statistics, always a concrete ratio of two quantity measurements. Some apparent “ratios” are not this data type because the ratio is in the unit not the value:

- beats/minute (Heart rate), µmol/L (Serum creatinine).

### Boolean

only “true” or “false” is possible

- Instead, this is often typed as a coded element with, e.g., “Yes” / “No” etc.
- Covers also “don’t know”

### Ordinal

Rankings and scores where there is

- a) implied ordering,
- b) no implication that the distance between each value is constant, and
- c) the total number of values is finite.

Although the term ‘ordinal’ in mathematics means natural numbers only, here any integer is allowed, since negative and zero values are often used for values around a neutral point. Examples

- -3, -2, -1, 0, 1, 2, 3 – reflex response values (neurology)
- 0, 1, 2,… – APGAR score values, to quickly summarize the health of newborn children
- 1, 2, 3, 4,... – ASA classification, a score defined by the American Society of Anesthesiologists for assessing the fitness of patients before surgery
- I, II, III, IV, ... – Tanner scale, a scale of physical development in children, adolescents and adults.

### Further Remarks

- Some of the data types add additional needs to be defined for a data element
- A data element typed as code may require a choice list of concepts you want to allow or see when it comes to real data
- Some terminologies such as SNOMED-CT already have a set of terms (along with codes) for specific purposes
- Choice lists may be found as so-called value sets, containing the choices along with their coded representation for proper implementation in software and *reliable communication.
- Example: data element “Reason for Refusal” in an immunization data set, following choices:
  - immunity
  - medical precaution
  - out of stock
  - patient objection
  - philosophical objection
  - religious objection
  - vaccine efficacy concerns
  - vaccine safety concerns
- Other simple examples of already existing value sets might be the
  - ABO and Rhesus Blood Groups
  - Administrative Gender of a Person
  - Professional Specialties
  - Laboratory Tests

### Ranges

Measurements may define possible ranges, e.g., a Pulse rate has a value in expected “normal range” 60 to 100 or may never be lower than 0 or more than 350 just to detect “illegal” values

### Length

Text may have an expressed/expected minimum or maximum length

### Precision and Scale

Quantities and time stamps may have their (expected) Precision/Scale as an additional property Precision is the number of digits in a number Scale is the number of digits to the right of the decimal point. 

- Example, body height in centimeters should have a Precision of 3 and a scale of 2, e.g., 1.89 cm

The same applies to dates and times.

- You might expect the creation date of a document to be precise to the second (thus year, month, day, hour minute and second being specified as real data)...
- but for a “Date of surgery” in a past medical history you may allow only year, or year and month.

### Units

Quantities also shall define an associated allowed Unit or set of units

- terminologist can help here too: they know systems of predefined units to tell software builders what standardized units look like
- While standardized units are very important for the communication between systems e.g., 10*9/L as the “system” unit for Platelets, using a coding called UCUM), the display of the units should always fit the usual way clinicians are used to seeing them, like x 109/L Platelets

## Guide to reality! – Examples, examples, examples

- If a data element called “dose quantity” in a vaccination record data set is defined, consider adding examples like “20 mL” or “200 I.U.”
- Good tools allow for context examples and comprehensive examples anywhere

## Tell me why! – Evidence

- When defining a data set, a use case / rationale for the collection of the defined set of data overall and each data element individually shall be documented

## Foster proper answers! – Operationalization

- When defining data elements, think about how to get proper values for the item
- There may be use cases where the circumstances of measurement make the data more comparable (a series over time), so you might want to define the details of measurement or the coding system specified for usage
- Perhaps a diagnosis in a discharge scenario has as a legal obligation to be determined as a free text but also as a code drawn from ICD-10, then your operationalization is “use ICD-10 for coding”

## Profit from other art! – Re-usability and inheritance

- When defining a data set it seems likely that parts or components of the data sets may have already been defined elsewhere
- Re-usability is one of the most important features of (clinical) modeling: it saves time if you simply can take over definitions of others (which should be easy by optimal tooling support).
- In case you are not 100% satisfied you still can consider taking the “artwork” of others, clone (inherit) it and start a refinement process to fit your needs; this is still better than starting from scratch and prevents unnecessary re-thinking of already well-thought structures, avoids errors and omissions.

## Document your expectation! – Cardinality and more

- There will be the need to eventually assign another property to a data element, that is to determine whether it is: optional or required, repeatable or not
- This is referred to as cardinality and typically expressed as n..m
  - with n being 0 for optional data elements or 1 for required ones,
  - and m being 1 for non-repeatable items or 2, 3, etc. for reflecting the number of allowed repetitions
  - A special symbol * is used to express that there is actually no upper limit of repetitions if the item, so n..* means “n to many”.
- Examples
  - The item whether a person is pregnant or not might have the cardinality of 0..1

### Condition

You also might consider making it 1..1 under the Condition that the person is female and your scenario requires information about pregnancy, for example patient safety reasons (prescription scenario, perhaps)

### Cardinality and Conformance

#### Mandatory M

- The attribute is mandatory, i.e. a valid value shall be provided and no null value is allowed. The minimum cardinality is at least 1. This also implies that if the sender has no valid value for such an attribute, the message cannot be sent.
- A sender must support elements with “mandatory” conformance, and a receiver must understand these elements.
- Please use mandatory elements with caution as overly-strict requirements may result in not implementable templates or in situations that are not aligned with practice.

#### Required R

- The item is required, i.e. a valid value should be provided or if missing a null value is allowed if its minimum cardinality is 1 (“empty element”), or may be omitted if its minimum cardinality is zero (“missing element”).
- In messages, the element must be communicated if its minimum cardinality is one. In the case where the element is not mandatory, it may be communicated with a null value. Note that any element declared to be "Mandatory" must also be "Required" and have a minimum cardinality of one. If the minimum cardinality is zero, and the element is "Required", conforming applications need not send the element if data does not exist. In some governance groups this is referred to as “Required but may be empty”.
- For required elements, conforming applications must demonstrate their ability to provide and communicate not null values. Receiving applications must demonstrate their ability to receive and process (e.g., store, display to users) not null values for required elements.
- A sender must support elements with “required” conformance, and a receiver must understand these elements.

#### Optional O

- The item is truly optional, i.e. a valid value may be provided or if missing may be omitted.
- It is indicated as “O” in the template item table, a may be considered as a shorthand for an unspecified conformance with a minimum cardinality of zero.

#### Not permitted NP

- The item is not allowed, i.e. the number of allowable occurrences is 0. It is indicated as “NP” in the template item table.

#### Conditional C (typically with a condition table)

- This item has an associated condition predicate and may depend on the co-occurrence of other elements or properties of the instance or situations.

## Cycle through life! – Status and Versioning

- The lifecycle of a definition should always be imagined as two-dimensional: a Status and a Version.
- An item has an initial Status, such as “draft” when you start defining it. It may mature over time to a status like “final”, is used and once no longer needed or replaced by a newer and better definition it might migrate to the status of “deprecated” or “retired”.
- This leads to a new Version of the definition: Versioning retains the original definition (also because it is still in use), “clones” the origin and adds the changes to the new version

### Evolution of models and specifications

### Status

- Main axis
- More possible/desirable
- Transitions
- Status of Versions
- Status „machine“

_____

ADDITIONAL TEXT TO MERGE

Healthcare interoperability depends upon sound semantic models to support safe and reliable exchange of information. We argue that clinical information modelling requires a collaborative team of healthcare professionals, process and content analysts and terminologists and that ‘separation of concerns’ is unhelpful. We present six fundamental concepts that participants must understand to collaborate meaningfully in technology-agnostic information modelling.

## Introduction

Health interoperability is about people far more than it is about technology. The purpose of health interoperability is to improve communication between clinicians and with patients, yet often there is inadequate clinician, and hardly ever patient, involvement in defining the requirements. The requirements definition part of interoperability design is referred to commonly as information modelling. Information modelling involves defining sets of data items for given clinical scenarios, their inter-relationships, constraints and vocabulary requirements. This paper presents a set of general principles derived from practical experience of information modelling projects over many years.

We propose (1) that there is a set of fundamental concepts that clinicians need to understand to contribute productively to information modelling projects. These concepts are about information structures and modelling methods and are technology-agnostic. These are “threshold concepts”: ‘portals to troublesome knowledge’ [[1](#_ENREF_1)]. We also propose that there is (2) a core set of distinct roles that must be present in the team to address all the necessary aspects of an information model that truly represents the real world.

## Roles and definitions

We have found that a clinical information modelling team needs to contain (1) *healthcare providers* (doctors, nurses, therapists, pharmacists, other care professionals as necessary to the requirement under consideration); (2) *process and content analysts* with suitable experience of healthcare terminology and operational workflow; (3) *terminologists* expert in the vocabularies used for formal representation of semantic concepts.

## Ten threshold concepts

### You are not alone! – Teamwork

Active participation of clinicians is a necessary but not sufficient condition for successful clinical information modelling. Data sets defined solely by healthcare providers may function well enough to communicate with other healthcare providers. However, that is not sufficient in a collaborative environment with multi-stakeholder communication.

Therefore, the first step to improve such a data set is to include process and content analysts and a terminologist as soon as practicable in a cooperative definition process. The healthcare providers obviously concentrate on the clinical content, but must learn how to communicate well with non-clinical experts. This requires overcoming a commonly observed condescending attitude towards non-clinicians, demonstrating behaviour that “educates” the rest of the team. Another benefit of having process and content analysts and terminologists on board at an early stage is to raise questions that improve the clarity to others outside the team, such as future implementers of software.

There can be initial anxiety in these multidisciplinary teams. Healthcare providers obviously lead the content definition, but analysts and terminologists should be self-confident enough to believe that they are making substantial contributions and to behave accordingly. Healthcare providers need to welcome and encourage that.

The team should perform team-building activities that beyond the immediate task focus and help to overcome the usual way of doing things. Those “bonding” actions can be [[2](#_ENREF_2)] a meeting outside the usual setting, a dinner or other social event.

### Start talk talkative! – Data elements basics

When discussing the clinical content, consider the following aspects:

- The data elements shall have a **short name** that allows other to gather what this item means. This needs to be simple text in natural language, not acronyms or abbreviations. Examples: Date of birth, Body weight, Type of specimen, Anatomic site, Collection Time

- Data elements shall always include a **description** that precisely explains in additional words what the subject of the item is. Examples:

  - The date and time the specimen was collected

  - The reason why the immunization was refused

  - The administrative gender of a person

- Introduce an **identifier** (short number for example) for each data element, at least unique within the data set, eventually globally unique; this makes it much easier later to refer to the data element in further discussions.

- Agreement about naming conventions so that you are consistent.


### Divide and conquer! – Grouping

Proper data sets contain grouping. Initial draft data sets are often simple lists of data elements. We have observed that grouping and summarizing data items in hierarchies is for some reasons not a natural approach for many healthcare providers, despite their familiarity with these concepts in chemistry and medicine. This problem can be reinforced by using inappropriate tools like spreadsheets that naturally produce flat lists.

To illustrate the enhanced clarity from grouping data, think about the example of a person’s “name” comprised of title, family name and a given name, along with other demographic properties. Rather than defining it as a flat list {Title, Family name, Given name, Date of birth, Gender}, compose the “name” as a group:

- Name

  - Title

  - Family name

  - Given name

- Date of birth

- Gender


This grouping clarifies that “name” has several components. It might seem superfluous, but a long flat list can easily become longer. Seeing only the top-level elements helps to understand and navigate the whole data set. The “tree” can be expanded to see the elements inside the group (denoted by ICON). Good tools intuitively support this notation:

± Name

- Date of birth

- Gender


We have seen “list explosions” due to not using grouping. As an example, let us assume, that in a discharge letter there can be specified up to three diagnoses. We have seen:

- Diagnosis Text 1, Diagnosis Code 1

- Diagnosis Text 2, Diagnosis Code 2

- Diagnosis Text 3, Diagnosis Code 3


Grouping makes it readable and closer to actual implementation as a repeatable unit:

- Diagnosis --- (group is repeatable up to three times)
  - Text
  - Code

Finally, we would like to make the statement that a data set should be seen as a hierarchical glossary[[PS1\]](#_msocom_1) .

### Describe the kind of data! – Data types

The data set is a definition of elements that will populated with real values. What is the expected *nature* of the data? Obviously a “date of birth” is a date like 1976-02-09 (or some other format) and a body weight of a patient is something like 87 kg. In some tools the lists of data types tend to introduce terms that are typically used in programming languages and not really self-explanatory to healthcare providers.

Let us summarize the most common data types (for healthcare provider driven and defined data elements) to pick from:

**Count** – Countable (non-monetary) quantities like 7, 11, 298. Used for countable types such as: number of pregnancies, steps (taken by a physiotherapy patient), number of cigarettes smoked in a day.

**Code** – a code for a real-world thing; a code is a system of valid symbols that substitute for specified concepts e.g., alpha, numeric, symbols and/or combinations, usually defined by a formal reference to a terminology or ontology, but may also be defined by the provision of text.

**Dates and Times** – represents an absolute point in time, as measured on the Gregorian calendar, and specified to the day or additionally the time. Used for recording dates in real world time. The partial form is used for approximate dates. For more detail about timing aspects of clinical modelling, consult the draft ISO document [[3](#_ENREF_3)] for further information.

**Text/String** – may contain any amount of legal characters arranged as words or sentences. Formatting may be included (text) or not (string).

**Quantity** – a measurement like 87 kg, 1.83 cm, 10 mg/mL; note that this is always a combination of a decimal number (of arbitrary precision) and a unit; this type represents "scientific" quantities, i.e. quantities expressed as a magnitude and units. There are some "special" quantities used in healthcare: for time durations *Duration* , for monetary amounts *Currency*.

**Duration** – a quantity representing a period of time with respect to a notional point in time, which is not specified. A sign may be used to indicate the duration is “backwards” in time rather than forwards.

**Identifier** – Type for representing identifiers of real-world entities. Examples: driver’s license number, social security number, prescription id, order id. An identifier is never a simple number or a string, but the context is always necessary. Example: “F564738573” (identifier), German Patient Identifier (context). “Pure” identifiers are meaningless, usually not the intent.

The following data types are sometimes used but less common:

**Ratio** – unlike its usage in statistics, always a concrete ratio of two quantity measurements like 20 mg / 100 mL. Some apparent “ratios” are not this data type because the ratio is in the unit not the value: 80 beats/minute (Heart rate), 60 µmol/L (Serum creatinine).

**Boolean** – only “true” or “false” is possible, this is often typed as a coded element with, e.g., “Yes” / “No” / “Don't know[[PS2\]](#_msocom_2) ” etc.

**Ordinal** – rankings and scores, such as for pain or Apgar values, where there is a) implied ordering, b) no implication that the distance between each value is constant, and c) the total number of values is finite. Although the term ‘ordinal’ in mathematics means natural numbers only, here any integer is allowed, since negative and zero values are often used for values around a neutral point. 

::: example

 -3, -2, -1, 0, 1, 2, 3 – reflex response values (neurology)

0, 1, 2,… – APGAR score values, to quickly summarize the health of newborn children

1, 2, 3, 4,... – ASA classification, a score defined by the American Society of Anesthesiologists for assessing the fitness of patients before surgery

I, II, III, IV, ... – Tanner scale, a scale of physical development in children, adolescents and adults.

:::

Decimal and currency data types are almost never seen in clinical data set definitions.

Some of the data types add additional needs to be defined for a data element. A data element typed as code may require a choice list of concepts you want to actually allow or see when it comes to real data. As an example, if you specify the data element “Reason for Refusal” in an immunization data set, then you might expect an answer out of the following choices:

- immunity

- medical precaution

- out of stock

- patient objection

- philosophical objection

- religious objection

- vaccine efficacy concerns

- vaccine safety concerns


This is another good example of where a terminologist might help. Some terminologies such as SNOMED-CT [[4](#_ENREF_4)] already have a set of terms (along with codes) for specific purposes. Choice lists such the one above may be found as so-called **value sets**, containing the choices along with their coded representation for proper implementation in software and reliable communication. Other simple examples of already existing value sets might be the ABO&Rhesus Blood Groups, Administrative Gender of a Person, Professional Specialties, Laboratory Tests, and so forth.

Measurements may define possible **Ranges**, e.g., a Pulse rate has a value in expected “normal range” 60 to 100 or may never be lower than 0 or more than 350 just to detect “illegal” values. Text may have an expressed/expected minimum or maximum length.

Quantities and time stamps may have their (expected) **Precision/Scale** as an additional property. Precision is the number of digits in a number. Scale is the number of digits to the right of the decimal point in a number. As an example, body height in centimeters should have a Precision of 3 and a scale of 2, e.g., 1.89 cm. The same applies to dates and time stamps. You might expect the creation date of a document to be precise to the second (thus year, month, day, hour minute and second being specified as real data), but for a “Date of surgery” in a past medical history you may allow only year, or year and month.

Quantities also shall define an associated allow **Unit** or set of units, e.g., the body weight may allow units such as gram or kilogram. Note that a terminologist can help here too: they know systems of predefined units to tell software builders what standardized units look like. While standardized units are very important for the communication between systems (e.g., 10*9/L as the “system” unit for Platelets, using a coding called UCUM [[5](#_ENREF_5)]), the display of the units however should always fit the usual way clinicians are used to see units (in the example before something like x 109/L Platelets).

### Guide to reality! – Examples, examples, examples

In order to demonstrate (and document) the expectation of future data collection conformant to the definition in a data set, it is very recommended to add examples of real data. If a data element called “dose quantity” in a vaccination record data set is defined, consider adding examples like “20 mL” or “200 I.U.” to illustrate what is expected and to educate everyone to also provide examples, examples.

### Tell me why! – Evidence

When defining a data set, an overall use case for the collection of the defined set of data shall be documented along with the data set. Reasons for collecting particular data also applies to almost every single data element. It is good practice to document why a data element is defined. This is sometimes referred to a rationale or evidence.

### Foster proper answers! – Operationalization

When defining data elements, think about how to get proper values for the item. Assume, for example, a stand-alone data element “body weight”. There may be use cases where the circumstances of measurement – whether the person wears clothes, whether the measurement happens in the morning or in the evening – do not play an important role. But if you are interested in a particular measurement, perhaps to make the body weights more comparable (a series over time), than you might want to define the way of getting the result. An operationalization could be “always measured in the morning, no clothes”.

Perhaps a diagnosis in a discharge scenario has as a legal obligation to be determined as a free text but also as a code drawn from ICD-10 [[6](#_ENREF_6)], then your operationalization is “use ICD-10 for coding”.

### Profit from other art! – Re-usability and inheritance

When defining a data set it seems likely that parts or components of the data sets may have already been defined elsewhere. For example, the set of “diagnoses” or what makes a “vaccine” seen in earlier examples is probably already available for re-use.

Re-usability is one of the most important features of (clinical) modeling. It saves time if you simply can take over definitions of others (which should be easy by optimal tooling support). In case you are not 100% satisfied you still can consider taking the “artwork” of others, clone (inherit) it and start a refinement process to fit your needs. This is still better than starting from scratch and saves a lot of time, prevents unnecessary re-thinking of already well-thought structures, avoids errors and omissions.

### Document your expectation! – Cardinality and more

Many data set definitions we have seen implicitly have a certain scenario in mind. For example, a data set defining a “laboratory test result” is a particular use case in the laboratory domain. A “laboratory test order” would be an expected other scenario in this healthcare domain.

There will be the need to eventually assign another property to a data element, that is to determine whether it is: optional or required, repeatable or not. This is referred to as cardinality and typically expressed as n..m, with n being 0 for optional data elements or 1 for required ones, and m being 1 for non-repeatable items or 2, 3, etc. for reflecting the number of allowed repetitions. A special symbol * is used to express that there is actually no upper limit of repetitions if the item.

In our “diagnosis” example above, we allow up to three diagnoses in the discharge scenario. The cardinality would be noted as 0..3 if diagnosis can be blank, or 1..3 if you must have at least one diagnosis.

The item whether a person is pregnant or not might have the cardinality of 0..1. You also might consider making it 1..1 under the **Condition** that the person is female and your scenario requires information about pregnancy, for example patient safety reasons (prescription scenario, perhaps). A condition shall be documented under operationalization (see above).

### Cycle through life! – Status and Versioning

The lifecycle of a definition should always be imagined as two-dimensional: a Status and a Version.

An item has an initial **Status**, such as “draft” when you start defining it. It may mature over time to a status like “final”, is used and once no longer needed or replaced by a newer and better definition it might migrate to the status of “deprecated” or “retired”.

Example: consider a group of data called “Estimated Delivery” which contains an item “Estimated Delivery Date” of type “date” (data type, see above), the actual expected date of delivery of the pregnant women, and an item “Determined on” which reflects the date when the EDD was documented.

- Estimated Delivery --- Version 1

  - Estimated Delivery Date (date)

  - Determined on (date)


This construct starts with status “draft” and finally to “final”. While the definition is used in real situations it seems necessary to also add the method of estimation, given as a choice list (data type: code)

This leads to a new **Version** of the definition. Versioning retains the original definition (also because it is still in use), “clones” the origin and adds the changes to the new version. So, the overall situation is that you have a concept defined earlier (see above), and a newer version now containing the method of estimation:

- Estimated Delivery --- new Version 2

  - Estimated Delivery Date (date)

  - Determined on (date)

  - Method of estimation (code)
    *Choice of:*
    - *last ovulation*
    - *last conception*
    - *last menstrual period*
    - *quickening*
    - *ultrasound*

Note that this version also has a status on its own, starting maybe also as a “draft” version and the matures or gets endorsed promoting to “final”. The original version 1 status is likely to go to “deprecated” as it no longer should be used.

This is also another situation where *terminologist* come into play. They would assign a code to the concept of “Estimated Delivery”, perhaps LOINC code 11778-8 “Delivery date Estimated” [[7](#_ENREF_7)]. This identifies the concept uniquely and a code is also needed for the communication between systems.

## Conclusions

We recognize the limitation that this paper is a form of anecdotal ‘folk wisdom’ and welcome feedback on the validity and utility of these concepts and recommendations. Space constraints have not permitted us to address important related issues about tooling, project management, training, visualization and so forth. Clinical information modelling is difficult but need not be an arcane mystery. We hope these suggestions are useful.

## References

[1]   Meyer JH, Land R. Threshold concepts and troublesome knowledge (2): Epistemological considerations and a conceptual framework for teaching and learning. Higher Education. 2005;49(3):373-388.

[2]   Egetto AB. 5 Effective Tips To Team Building. 2017 [cited 7 Nov 2017]. Available from: http://go.challengize.com/5-effective-tips-team-building

[3]   ISO. ISO/DIS 12381 Health informatics -- Time standards for healthcare specific problems. 2017 [cited 7 Nov 2017]. Available from: https://[www.iso.org/standard/74356.html](http://www.iso.org/standard/74356.html)

[4]   SNOMED International. SNOMED CT The Global Language of Healthcare. 2017 [cited 7 Nov 2017]. Available from: https://[www.snomed.org/snomed-ct/](http://www.snomed.org/snomed-ct/)

[5]   Schadow G, McDonald C. The Unified Code for Units of Measure 2014 [cited 7 Nov 2017]. Available from: http://unitsofmeasure.org/ucum.html

[6]   World Health Organization. ICD-10 online versions. 2017 [cited 7 Nov 2017]. Available from: http://www.who.int/classifications/icd/icdonlineversions/en/

[7]   Regenstrief Institute. The international standard for identifying health measurements, observations, and documents. 2017 [cited 7 Nov 2017]. Available from: https://loinc.org/
