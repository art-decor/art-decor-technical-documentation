---
permalink: /introduction/interoperability
---
# Interoperability

## Goal: "Interoperability Specifications"

An Interoperability Specifications collects requirements and definitions from various stakeholders in order to achive interoperability between people, organizations or systems.

::: definition INTEROPERABILITY

Interoperability is the ability to of one or more people, organizations or systems to exchange information, to understand it and to re-use it.

:::

There are different ways to break down the different aspects of interoperability into layers. The following paragraphs describe the general aspects behind the ART-DECOR strategie and policy.

Looking at the dfinition above, the exchange aspect leads to the first and lowest layer of Interoperability: the **structural or syntactic interoperability**. Understanding exchanged information has to do with using the same language, not only in terms of human languages but also regarding the use of terms, words and their meaning. This aspect leads to the second layer of Interoperability: the **semantic interoperability**.

### Structural Interoperability

The structural or syntactic aspect refers to describing the **exact format of the information** to be stored or exchanged in terms of sequence of items, grammar and overall format.

### Semantic Interoperability

Semantic interoperability ensures that the **precise meaning of exchanged data and information** is preserved and understood throughout exchanges between parties, in other words ‘what is sent has a common understanding and is understood by the receiver’.

### Organizational Interoperability

This refers to the way in which public administrations **align their business processes, responsibilities and expectations** to achieve commonly agreed and mutually beneficial goals. In practice, organisational interoperability means documenting and integrating or aligning business processes and relevant information exchanged [[ecjo](https://joinup.ec.europa.eu/collection/nifo-national-interoperability-framework-observatory/glossary/term/organisational-interoperability)].

### Legal Interoperability

Legal interoperability ensures that organizations operating under different legal frameworks, policies and strategies are able to work together [[ecjl](https://joinup.ec.europa.eu/collection/nifo-national-interoperability-framework-observatory/glossary/term/legal-interoperability)].

### Achieving Structural and Semantic Interoperability

<img src="../../img/Wheelsv.png" alt="Wheelsv" style="zoom:20%; float:right;" />ART-DECOR clearly focuses on core aspects of Interoperability, i.e. the structural and semantic aspects and offers a collaboration platform for stakeholders involved in these areas. In particular this includes 

* **Medical domain expertise** in a general and broad sense, brought in by caregivers covering patient care, public health and research through defining requirements of storage, exchange and analysis of data and information and describing real-world medical terms with sufficient precision,
* **Terminology expertise** to allow annotations of real-world terms and concepts with unique codes to enrich human language and allow more precision in dealing with real-world medical terms,
* **Technical expertise** that compiles the medical requirements regarding storage, exchange and analysis of data and information into implementable artefacts such as data formats and exchange interfaces,
* **Implementation expertise** to use the interoperability specification and build software in order to support healthcare providers through interoperable storage, exachange and analysis of data and information through healthcare applications,
* **Project Leads** who coordinate and drive the development of interoperable solutions.

ART-DECOR uses consistent color schemes for the stakeholder groups for better orientation. The five gears on the right symbolize these five areas and the cooperation of the stakeholders.

### The core topics of ART-DECOR as a collaboration platform

The five areas mentioned above are the core topics, ART-DECOR supports through methods for creating, maintaining and publishing artefacts authored by the stakeholder groups above. ART-DECOR acts as an artefact repository, all artefatcs are collected in a "project", that circumscribes a specific set of use cases and aims on published Interoperability Specifications.

The tool supports most of the life cycle parts of the genesis of interoperability specifications: documenting requirements, looking at use cases, transactions and actors in scenarios, annotating concepts with proper terminology, creating data formats and profiles for storage and exchange of healthcare data, support implementation, testing and data validation to enhance data quality. Only production is not directly related to the authoring tool ART-DECOR.

![image-20220701144234738](../../img/image-20220701144234738.png)

