---
permalink: /introduction/valueset
---
# Value set

## Definition

::: definition VALUE SET

A value set is a set of codes drawn from one or more [code systems](../codesystem).

<adimg valueset/> is the symbol used throughout the app.<p></p>

:::

# General Introduction

## What is a Value Set?

Value Sets are sets of codes, drawn from one or more code systems, intended for use in coded data elements in data instances, as defined by a particular conformance rule – such as an Template, Extension or Profile.



## Value Set Authoring Workflow

# Introduction

A value set in ART-DECOR is part of the terminology section. A value set 

1. explicitly include coded concepts from one or more code systems (extensional value sets), exposing it in a (pseudo)hierarchical list, or
2. defines instructions of how to create a value set by allowing to include or exclude or filter codes (intensional value set).

Defining an extensional value sets is the natural “legacy” and a simple way to describe value sets. It offers a complete list of all coded concepts. This is the most often used value set definition in ART-DECOR 2.

Intensional value sets contain the instructions only. In order to come to a similar construct as offers by type 1, the instructions of the value set definitions need to be “executed” by following the instructions. The result can be recorded as a so-called expansion set. Expansion sets are of temporary nature.

The rough structure of a value set is therefore

- value set **meta data** such as id, effectiveDate, versionLabel, statusCode and some more 
- a section that describes the **composition** of the value set that is either
  - a set of coded concepts from code systems to be included/excluded in the value set, or exclusively a filter comprised of a property defined by the underlying code system, an operator such as “all children” and a value which is a code or other matching criteria
  - a value set reference to a value set that is completely included in the value set
- a section that carries the volatile **expansion** of a value set.

# Comparison with ART-DECOR 2

The used format in ART-DECOR for value sets is comprised of

- value set **meta data** such as id, effectiveDate, versionLabel, statusCode and some more 
- a section that describes the composition of the value set by allowing
  - an explicit list of coded concepts to be included as a **conceptList**
  - an **include** value set instruction that allows to compile a new value set out of existing ones
  - an instruction to include all codes from a specific code system by specifying a **complete code system**, which especially made sense for very large code systems.

# The new ART-DECOR 3 value set format

# Value Set Authoring workflow details

There are two ways to require a new value set for a project.

- In the Terminology Panel, a user decides to add a new value set to the list. He pushes the “Add” button” and gets the new value set meta data dialog.
- In the Terminology Browser, a user selects single codes or groups of codes and put them into his “shopping cart”. He is then asked to specify the new value set meta data. 

## New value set meta data dialog

There is a dialog that comes up when a new value set is requested, that allows to enter the following items, organized in rows.

The following table summarizes the items in this row of the dialog. Items with this symbol 💡 are essential initial items.

| **Element**        | **Description**                                              | **Datatype**            |
| ------------------ | ------------------------------------------------------------ | ----------------------- |
| **id**💡            | Defines the globally unique identifier for this value set and may be used to reference it.**Initially populated**: with the first free value set in the project with BaseId for value sets; this is done by the backend. Alternatively upon creating a new value set, an id field may be offered toi be populated by the user. This id is then checked in the backend for uniqueness and consistency.**Editable**: as an OID | Oid                     |
| **displayName**💡   | Display Name**Editable**: string, not empty                  | NonEmptyString          |
| **statusCode**💡    | Status code of the value set**Initially populated**: set to "new"**Editable**: may be changed to draft | ItemStatusCodeLifeCycle |
| **effectiveDate**💡 | Identifies the effective date of the version of the object**Initially populated**: time stamp of current time**Editable**: a date time picker may be offered to change that date | TimeStampNoTimezone     |
| **versionLabel**   | A human readable version number or version label for convenient human rendition; not used by an application**Editable**: short string, may be empty | ShortDescriptiveName    |

Please refer to the **Editable form fields** part of the Dialogs Design Panel to identify the right Editable components:

- EditableString
- EditableShortDescriptiveName
- EditableItemStatusCode

The second part of the dialog handles the following input

| **Element**             | **Description**                                              | **Datatype**        |
| ----------------------- | ------------------------------------------------------------ | ------------------- |
| **name**                | Business Name**Editable**: string, not empty                 | BasicId             |
| **canonicalUri**💡       | Defines the canonical uri for this value set and may be used to reference it.**Initially populated**: empty**Editable**: URI confomant string, may be empty | anyURI              |
| **expirationDate**      | Identifies the expiration date of the version of the object**Initially populated**: empty**Editable**: a date time picker may be offered to add a date | TimeStampNoTimezone |
| **officialReleaseDate** | Identifies the official release date of the version of the object**Initially populated**: empty**Editable**: a date time picker may be offered to add a date | TimeStampNoTimezone |

Please refer to the **Editable form fields** part of the Dialogs Design Panel to identify the right Editable components:

- Short Formal Name
- URI
- EditableDateTime

The third part of the dialog handles the description for the value set only

| **Element** | **Description**              | **Datatype**               |
| ----------- | ---------------------------- | -------------------------- |
| **desc**💡   | Description of the value set | FreeFormMarkupWithLanguage |

Please refer to the **Editable form fields** part of the Dialogs Design Panel to identify the right Editable components:

- EditableTranslationEditor

The fourth part of the dialog handles the authorities, purpose and copyright information.

| **Element**             | **Description**                                              | **Datatype**               |
| ----------------------- | ------------------------------------------------------------ | -------------------------- |
| **publishingAuthority** | The authoritative body who has reviewed the Value set for (clinical) accuracy and relevance, and authorized it for publication. Registries may require this element to be valued. | AuthorityType              |
| **endorsingAuthority**  | A list of bodies who have reviewed the Value set for (clinical) accuracy and relevance, and endorsed it for use | AuthorityType              |
| **purpose**             | A statement about the purpose of the value set               | FreeFormMarkupWithLanguage |
| **copyright**           | A copyright statement relating to the value set and/or its contents. | CopyrightText              |

Please refer to the **Editable form fields** part of the Dialogs Design Panel to identify the right Editable components:

- EditableTranslationEditor
- Look for Authority type in recent System Admin dialogs, it’s comprised of addrLine, id and name and may required a sub dialog or even a new editable component

Support for editing existing valueset:

- completecodesystem (manual entry: concept, exception, inclusion, exclusion). completecodesystem must be editable in the browser, and in the *terminology/valuesets* panel. completecodesystem is part of the composition. In the composition-editor: add a button to add completecodesystems. Note that completecodesystems can also be project codesystems. Composition-editor now has, *remove (*change this into *action),* change this into speeddial selector.
- If the composition is empty (in the empty line): option to add single code, add completecodesystems. Is a Speeddial.
- If the composition has content: (in the line per entry) add single code above, add single below, add completecodesystems above, add completecodesystems below, toggle exception, remove. Under column *action*.
- conceptlist
  - manual entry
  - link to browser in context of this valueset with already populated composition. Browser needs to know the context of the valueset. Browser will provide the search codesystem-context by pre-selecting: a) If there is 1 codesystem: that codesystem from the valueset 2) If there are more codesystems: no context
    - editing the valueset directly lands in the browser to add (and delete) concepts
    - so you can delete codes (also directly in the valuesets panel). Note: deletion of parents causes the children concepts to be deleted also.
    - so you can add codes (by linking to the browser and providing the context of the valueset)
    - add a manual code by hand
    - make sure exceptions can be viewed (now missing in the front-end, is already present in backend). Exceptions are marked as: hollow ball status icon. 
    - add code as exception: can be toggled under the displayname as a checkbox. Concept can move to exception. Exception can move to concept. 
- Both should be available from the valueset panel
- Ordinals: currently only viewable, not editable. When they are present, show column *Ordinal.* If not present, dont show column.
- Only in the  *terminology/valuesets* panel: Designations/benamingen should be editable (same as in ART-DECOR 2).
  - synonym, abbreviation, preferred can be multiple
  - allow maximum 1 FSN
- In browser: When adding codes as extensional, the composition should show the level/type for these codes. 
- In browser: limit the options for adding codes to: (note: we probably will re-add other options. Reason is that the frontend might retrieve 80.000 concepts, and complixity dealing with this).
  - extensional - selected code only.
  - intensional - code and all children
  - intensional - all children
- In terminology/valuesets and browser: Level and type: Must be shown as indented tree, based on level/type (already present in terminology/valuesets)
- Level/type: is only editable by the user in the *terminology/valuesets* panel. Is editable by manually entering level (0, 1, ..) and type (L, S, A, D). Type is a dropdown.
- Browser, composition editor: Level and type, display name, codesystem: are all editable.
- When viewing codes in the browser: Clearer show difference between Relationship type and Relationship destination. Relationship type is *not* clickable. INTERPRETS. Relationship destination is clickable. GENETIC TEST
- Browser: Don't offer the plus signs, when not in edit, or when there is no new valueset created.
- Metadata editing: Valuetse-id and description should be changable.

**Examples**

- As examples:
  - demo5 (vital sign result has include to another valueset. Use case is in valueset *VitalSignResult* include a valuset  id="2.16.840.1.113883.3.1937.99.60.5.11.7" name="HeartRateByMethod")
  - demo5 (exceptions: Example is added to demo5- project in valueset BreathScore: code *OTH* as exception)
- Ordinals, see for example:
  - https://art-decor3.test-nictiz.nl/art-decor/decor-valuesets--zib2020bbr-?id=2.16.840.1.113883.2.4.3.11.60.40.2.12.16.1&effectiveDate=2020-09-01T00:00:00&language=en-US
  - Example is added to demo5- project in valueset BreathScore
- Type D for complete codesystems
  - Example is added to demo5- project in valueset BreathScore
