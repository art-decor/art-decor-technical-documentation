---
permalink: /introduction/project
---
# Project

## Definition

::: definition PROJECT

A DECOR project aims on creating, maintaining and publishing interoperability specifications. It embraces all artefacts for a specific purpose or use case.

<adimg project/> is the symbol used throughout the app.<p></p>

:::

## Project Information

In essence, a DECOR project aims on creating, maintaining and publishing interoperability specifications. Typically it defines a specific set of use cases to be covered and carries a collection of artefacts to reach that goal.

::: example

Examples of a DECOR project include

* a project named *Laboratory Result Reports* aims on interoperability specification for result reports in the laboratory domain
* a  project named *Vaccination Record* summarizes all functional and technical aspects in order to build a vaccination registry with individual records or the vaccination information parts of an electronic health record 

:::

A project embraces all artefacts to reach the defined goal and is comprised of project information, datasets, scenarios, terminologies and rules. Change management is supported by submitting issues, publications of the specifications can be triggered and published at any point in time.

## Datasets <adimg dataset floatright/>

A dataset collects the functional aspects of the medical domain, i.e. what items are needed to be stored, that are part of a record, that are needed to be communicated or are subject of analysis. You can imagine a DECOR dataset as a *hierarchical glossary of terms* for a specific medical domain or use case.

## Scenarios <adimg scenario floatright/>

A scenario reflects a use case to be supported and summarizes process aspects in terms of transactions and the involved actors. For that purpose it defines a subset of the dataset items, enrich them with additional properties and bind them to technical representations.

## Terminologies <adimg terminology floatright/>

Terminologies allow enriching medical terms with codes so that they get more precise and are better machine-processable for optimzed exchange and analysis. Codes in a specific context are collected in code systems, while value sets are subsets of codes from one or more code systems.

## Rules <adimg rules floatright/>

The technical representation of an item in terms of data formats that are exchanged or used for storage are summarized under "rules" as it typically uses standard framework formats and further constrain them until the fit the use case. Smaller sets of rules for data items are collected to larger sets, so that at the end a use case is optimally supported when using the resulting technical representation.

## Issue Management <adimg issue floatright/>

All artefacts so far may evolve over time, taking errors, enhancements or even substantial changes into account. In ART-DECOR this requirement is supported by an artefact-aware issue management that allows optimzed change documentation and management for stakeholder support.

## Publications <adimg publication floatright/>

Thinking in cycles of specifications, the set of artefatcs, compiled in Interoperability Specifications are periodically released, i.e. frozen and published as a certain version. As part of change documentation and management, publications and intermediate archive and backup procedures are supported by ART-DECOR.