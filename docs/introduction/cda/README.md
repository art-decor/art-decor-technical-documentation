---
permalink: /introduction/cda
---
# HL7 CDA Core Principles

## Introduction

This implementation guide specifies the *Core Principles* of using the [*Clinical Document Architecture*](https://www.hl7.org/implement/standards/product_brief.cfm?product_id=7). Aspects of the use of these Core Principles in ART-DECOR® are also covered. 

### Scope

This guide focuses on  [*Data Types Release 1*](https://www.hl7.org/implement/standards/product_brief.cfm?product_id=358). Meanwhile an ISO specification ISO 21090 is out, also known as Data Type Release 2. The latter is subject to another implementation guide that will be published when ISO data types are used.

### Focus of this document: HL7 Version 3 Data Types for CDA

The following chapters of this document will focus on the description of the data types and associated material. It will highlight the essentials in four chapters, all focusing on the Norwegian situation.

- **Common aspects:** explains common aspects of use of CDA and XML
- **Data types:** gives an overview and detailed specification of all data types used in CDA
- **Identification schemes:** explains major aspects of identification and the use in CDA
- **Use of vocabulary:** explains major aspects of vocabulary use in CDA.

### Background

In many (European) countries, implementation guides for the use of data types in HL7 V3 are published. They take into account country-specific constraints on the data types.

### Legend of symbols

In this document several symbols are used.

::: note

Note

:::

::: example

Example

:::

::: constraint

constraint

:::

## Common aspects

### Use of XML

HL7 Version 3 CDA uses the Extensible Markup Language (XML) as the exchange format. It is assumed that the reader is familiar with common XML aspects

- [World Wide Web Consortium. Extensible Markup Language, 1.0, 2nd Edition](http://www.w3.org/TR/REC-xml )
- World Wide Web Consortium. XML Schema [Part 0](http://www.w3.org/TR/xmlschema-0/), [Part 1](http://www.w3.org/TR/xmlschema-1/), [Part 3](http://www.w3.org/TR/xmlschema-2/). 

However, some constraints are made regarding the use of CDA.

#### Character Set

::: constraint

The character encoding of any CDA document shall be “UTF-8”. 

:::

::: example

```xml
<?xml version="1.0" encoding="utf-8"?>
```

:::

### CDA document structure

The XML namespace for CDA Release 2 documents is `urn:hl7-org:v3`. This must be correctly mentioned in every XML instance. 

CDA XML documents starts with the root element *ClinicalDocument*.

::: example

```xml
<ClinicalDocument xmlns="urn:hl7-org:v3" xmlns:voc="urn:hl7-org:v3/voc"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <!-- CDA Header --> 
          ...
    <!-- CDA Body -->
    <component>
        <structuredBody>
           ...
        </structuredBody>
    </component>

</ClinicalDocument>
```

:::

## Data types

### Introduction

The following chapter describes the typical data types used in CDA implementation guides. For further information please refer to the HL7 V3 standard and its two parts concerning data types:

- Abstract Data type definition [HL7 Version 3 Abstract Data type definition](https://www.hl7.org/implement/standards/product_brief.cfm?product_id=264)
- XML ITS for data types [XML ITS for data types](https://www.hl7.org/documentcenter/public_temp_682D4D16-1C23-BA17-0C789BA52BEDB68E/wg/inm/datatypes-its-xml20050714.htm)

Data types are properties of model class attributes and the basic building blocks of information in a CDA XML instance. For example, a model attribute named “time” has as its data type a time stamp `TS` (see Figure).

<img src="../../img/image-20220721131826563.png" alt="image-20220721131826563" style="zoom:50%;" />

*CDA structure (left) and the data type specification (right)*

### XML representation of data types

Actual data is mostly carried in HL7 Version 3 by populating XML attributes and not as element content. Exceptions are names of persons and organizations as well as addresses.

The XML elements have names like the model attributes. For example, a model attribute “id” has a corresponding XML element named `<id…>`. The model determines the sequence of elements and their hierarchy. The data type itself determines the XML attributes within the start tag.

The notation here is as follows:

| Attribute                                    | DT        | Conf                      | Description         |
| :------------------------------------------- | :-------- | :------------------------ | :------------------ |
| (name of the model attribute or XML element) | Data type | Conformance, i.e. O, R, M | Textual description |

XML attributes (data type properties) are denoted as *@attribute* in order to distinguish their names from XML elements.

Example: in the example model above, the data type of the `id` attribute's is `II` (Instance Identifier). The corresponding XML attributes for that data type include `@root` and `@extension`.

::: example

```XML
<id   root="..."   extension="..." />
```

:::

A description of the data types and their corresponding XML representation is contained in subsequent paragraphs of this guide.

#### Exceptions using XML element content

For most data types, the actual data is conveyed in XML attributes. However, there are some exceptions. For the data types binary (BIN), Encapsulated Data (ED), Entity Name (EN), Person Name (PN), Organization Name (ON), Trivial Name (TN), Address (AD) and character string data (ST), information is carried as element content.

Example: the components of an address `<addr>` are represented by sub elements with the data as element content (in the example shown in black).

::: example

```XML
<addr>
    <streetAddressLine>Thormøhlens gate 12</streetAddressLine>
    <postalCode>5006</postalCode>
    <city>Bergen</city>
</addr>
```

:::

#### Combined data types

There are a number of so-called combined data types. For example, an interval with a lower and upper limit and a ratio that has a nominator and a denominator value.

In combined data types the substructure (e.g. `<low> <high>` in an interval) are represented as child elements of the relevant data type element.

::: example

```XML
<effectiveTime>
    <low value="20110701"/>
    <high value="20110729"/>
</effectiveTime>
```

:::

#### Conformance information

The conformance indicators distinguish kinds of conformance information described shortly here.

| Conformance | Description                                                  |
| ----------- | ------------------------------------------------------------ |
| **O**       | An optional item may be omitted if no data is present, i.e. no corresponding XML attribute or element is sent (marked as “O”) |
| **R**       | For required items, lower cardinality is always zero or 1<br /><br />Elements must be present and populated with data if present in the sending system. If no data is available (yet), a null flavor (see below) indicates and possibly classifies the missing information (marked as R). <br />Attribute flagged as required must be present in the instance and populated. |
| **M**       | For mandatory elements data must be sent and no missing values are allowed, i.e. if the sending system does not have information for mandatory elements the XML instance cannot be created and sent (marked as M) |
| **NP**      | No data may be present at all (not permitted)                |
| **C**       | Conditional conformance; typically a conformance table shows the circumstances (conditions) derived from information in the instance and the corresponding cardinalities and conformance statements |

### Missing values (null flavor)

For every data type it is possible to specify a missing value including a kind of reason why data is missing. Instead of specifying any attribute of the corresponding element, a `@nullFlavor` attribute expresses the omission of data. Note that missing data is not allowed for mandatory elements (see above).

::: example

```XML
<value nullFlavor="OTH"/>

<code nullFlavor="UNK"/>

<booleanInd nullFlavor="NI"/>
```

:::

The codes for the `@nullFlavor` attribute are drawn from the following hierarchical code system. It is a Coded Simple (cs), which means it has no code system (attribute `@codeSystem`) specified.

| Code                  | Print Name              | Definition / Description                                     |
| :-------------------- | :---------------------- | :----------------------------------------------------------- |
| <tree level="0"/>NI   | No Information          | No information whatsoever can be inferred from this exceptional value. This is the most general exceptional value. It is also the default exceptional value. |
| <tree level="1"/>OTH  | other                   | The actual value is not an element in the value domain of a variable. (e.g., concept not provided by required code system). |
| <tree level="2"/>NINF | negative infinity       | Negative infinity of numbers.                                |
| <tree level="2"/>PINF | positive infinity       | Positive infinity of numbers.                                |
| <tree level="2"/>UNK  | unknown                 | A proper value is applicable, but not known.                 |
| <tree level="2"/>ASKU | asked but unknown       | Information was sought but not found (e.g., patient was asked but didn't know) |
| <tree level="3"/>NAV  | temporarily unavailable | Information is not available at this time but it is expected that it will be available later. |
| <tree level="3"/>NASK | not asked               | This information has not been sought (e.g., patient was not asked) |
| <tree level="3"/>TRC  | trace                   | The content is greater than zero, but too small to be quantified. |
| <tree level="1"/>MSK  | masked                  | There is information on this item available but it has not been provided by the sender due to security, privacy or other reasons. There may be an alternate mechanism for gaining access to this information.Note: using this null flavour does provide information that may be a breach of confidentiality, even though no detail data is provided. Its primary purpose is for those circumstances where it is necessary to inform the receiver that the information does exist without providing any detail. |
| <tree level="1"/>NA   | not applicable          | No proper value is applicable in this context (e.g., last menstrual period for a male). |
| <tree level="0"/>NP   | not present             | Value is not present in a message. This is only defined in messages, never in application data! All values not present in the message must be replaced by the applicable default, or no-information (NI) as the default of all defaults. |

### ANY as the generic data type

This abstract data type is the basis for all other data types. Not a single value within a CDA document has the effective data type ANY, but any data type within HL7v3 is a specialization of ANY. This also means that any other data type inherits all properties of ANY – see [Missing values (null flavor)](#data-types).

The ANY type is occasionally found in HL7 models when it comes to clinical finding etc. where – at model time – the actual data type of the value is not known. At instantiation time however, the actual data type *is* known and ANY is always replaced by a particular data type in an instance.

One example is the observation value (child element value in an observation), where in the model the data type of value cannot be anticipated. When instantiation is taking place, the data type is specified by the XML instance attribute `@xsi:type`. This information can be used by the receiver to validate and process the value.

Attributes of an element with this data type are

| Core Attribute  |  DT  | Conf | Description                                                  |
| :-------------- | :--: | :--: | :----------------------------------------------------------- |
| **@nullFlavor** |  CS  |  –   | Classification of the missing value, see [Missing values (null flavor)](#data-types). |

::: constraint dtr1-1-ANY

If there is a `@nullFlavor`, there shall be no other attribute or element, unless it's `nullFlavor="OTH"`.

In that case `@codeSystem` or `originalText` (see “code” data types) may have a value.

:::

::: example

```xml
<!-- A value of data type ANY in the model, instantiated as CE -->
<value xsi:type="CE" code="N11.9" codeSystem="2.16.840.1.113883.6.3"/>

<!-- A value of data type ANY in the model, instantiated as PQ -->
<value xsi:type="PQ" value="12" unit="mL"/>

<!-- A value of data type ANY in the model, instantiated as ED -->
<value xsi:type="ED">this is text</value>
```

:::

### Boolean (BL)

Elements of this data type refer to Boolean logic and have an attribute *@value*.

| Core Attribute |  DT  | Conf | Description                 |
| :------------- | :--: | :--: | :-------------------------- |
| **@value**     |  BL  |  –   | Valued as “true” or “false” |

::: example

```xml
<negationInd value="false"/>

<deceasedInd value="true"/>
```

:::

In some situations, the BL data does not apply to an XML element, but an attribute. In that case, the attribute carries the Boolean value directly.

::: example

```xml
<support contextConductionInd ="false">
   ...
</support>
```

:::

### Instance Identifier (II)

This data type identifies instances of classes like observations or entities (persons, organizations). Attributes are

| Core Attributes |  DT  | Conf | Description              |
| :-------------- | :--: | :--: | :----------------------- |
| **@extension**  |  ST  |  O   | Identification           |
| **@root**       | OID  |  R   | Source of identification |

::: example

```xml
<id extension="13234453645" root="2.16.840.1.113883.2.6.15.3.427.1"/>
```

:::

The `@root` attribute is of type OID (object identifier), a unique identifier of the issuing authority or system. For example, a patient with a patient number `13234453645` created by a specific healthcare provider organization is uniquely identified by the number itself in `@extension` and the OID of the organization or the sending system, in the example `2.16.840.1.113883.2.6.15.3.427.1` in `@root`.

In some cases, a single OID identifies an object uniquely. In that situation, only `@root` is populated and `@extension` is omitted.

::: constraint

All conformance statements applicable to this data type can be found at the the [ART-DECOR table of V3 data types](../documentation/datatypes/).

:::

### Encapsulated Data (ED)

Elements of this data type carry multimedia content. The actual multimedia content is represented as element content. There are two situations for the use of ED: 

- Inline: The full data is transmitted in the type. This form is mainly used for text.
- By reference: a thumbnail is provided which links to the full data by reference.

In an instance, the data type is normally determined by an `xsi:type` specification and some attributes.

| Core Attributes     |  DT  | Conf | Description                                                  |
| :------------------ | :--: | :--: | :----------------------------------------------------------- |
| **@mediaType**      |  CS  |  O   | MIME media type classification. At time of writing this specification, the following MIME types are allowed:<br />text/plain (plain text)<br />text/html (text with html markup)<br />image/jpeg (JPEG image)<br />application/pdf (a PDF, portable document format)<br />application/hl7-sda+xml (CDA documents). |
| **@representation** |  CS  |  O   | Representation of the multimedia content. At time of writing this specification, the following representations are allowed:<br />TXT (text, this is the default)<br />B64 base 64 encoded data, for example for embedded CDA documents). |

::: example

```xml
<text xsi:type="ED" mediaType="text/plain">
    Patient treated sucessfully
</text>

<text mediaType="application/hl7-sda+xml" representation="B64">PD94bWw..etc..==</text>
```

:::

To include referenced data the two sub elements `thumbnail` and `reference` are used.

| Core Elements |  DT  | Card | Conf | Description                  |
| :------------ | :--: | :--: | :--: | :--------------------------- |
| **reference** | TEL  | 0..* |  O   | Link (URI) to full data      |
| **thumbnail** |  ED  | 0..* |  O   | Thumbnail of referenced data |

::: example

```xml
<value xsi:type="ED" mediaType="image/png" representation="B64">
    <reference value="http://radiology.bergen.no/xrays/128s.png"/>
    <thumbnail mediaType="image/jpeg" representation="B64"> MNY...83== </thumbnail>
</value>
```

:::

### Strings (ST)

This data type is a specialization of the ED type where `@mediaType` is fixed `text/plain` and `@representation` is fixed `TXT`. It allows conveying a character string as **element content**. This data type is used in other data types it like the `@extension` in instance identifiers, or address and person name data type components.

::: example

```xml
<lotNumber> 143GT6DT </lotNumber>
```

:::

### Concept Descriptor (CD)

This data type specifies a concept by means of a code and its code system (except for situations where no code is available).

A couple of specializations of data type CD exist, which differ in the main characteristics (properties) of the data type that may or may not be supported.

| DataType               |  CD  |  CE  |  CV  |  CO  |  CS  |
| :--------------------- | :--: | :--: | :--: | :--: | :--: |
| Property ↓             |      |      |      |      |      |
| **@code**              |  X   |  X   |  X   |  X   |  X   |
| **@codeSystem**        |  X   |  X   |  X   |  X   |      |
| **@codeSystemVersion** |  X   |  X   |  X   |  X   |      |
| **@displayName**       |  X   |  X   |  X   |  X   |      |
| **originalText**       |  X   |  X   |  X   |  X   |      |
| **translation**        |  X   |  X   |      |      |      |
| **qualifier**          |  X   |      |      |      |      |

Because the CE variant is most used in practice, the general explanation of the properties will be given with de description of CE (see below).

The only attribute that is unique to CD is briefly explained here.

| Core Attribute |  DT  | Conf | Description                           |
| :------------- | :--: | :--: | :------------------------------------ |
| **@qualifier** |  CR  |  O   | An additional qualifier for the code. |

Complex terminologies like SNOMED CT can be used only using data type CD.

### Coded with Equivalents (CE)

With elements of this data type, a concept by means of a code and its coding system (except for situations where no code is available) can be specified. It optionally contains one or more equivalents using other code systems.

It may contain the following attributes:

| Core Attributes        |  DT  | Conf | Description                                                  |
| :--------------------- | :--: | :--: | :----------------------------------------------------------- |
| **@code**              |  ST  |  R   | The actual code                                              |
| **@codeSystem**        | OID  |  R   | required to identify the code system were the code is drawn from. This is an OID (object identifier, see [Object Identifiers](http://localhost:8082/introduction/cda/#identification-mechanisms)) |
| **@displayName**       |  ST  |  O   | Optional textual representation of code. A display name may be specified representing the plain text of the code. The information of this attribute may not be used by the receiving system for interpretation, but only for display purposes. The display name may never be transmitted without a code and must have the same meaning as the code. |
| **@codeSystemName**    |  ST  |  O   | A textual representation of the name of the code system      |
| **@codeSystemVersion** |  ST  |  O   | A textual representation of the version of the code system   |

::: example

```xml
<administrativeGenderCode code="F" codeSystem="2.16.840.1.113883.5.1"/>

<code code="AU" codeSystem="2.16.840.1.113883.5.33"/>

<storageCode code="AC" codeSystem="2.16.840.1.113883.5.34" displayName="active"/>

<code code="A45.0" displayName="Somatisierungsstörung" codeSystem="1.2.276.0.76.5.409" codeSystemName="ICD-10 GM 2012"/>
```

:::

There are some child elements of codes that can be used to express what the “original text” was that was used for the coding or to indicate that there are “translations” to other code systems than the primary code.

| Core Element     |  DT  | Card | Conf | Description                                                  |
| :--------------- | :--: | :--: | :--: | :----------------------------------------------------------- |
| **originalText** |  ST  | 0..* |  O   | Textual representation of the concept, actually a description which was used to create the actual code |

No semantics may be assumed other than showing the `@displayName` to a user to clarify the code. A display name cannot occur without its code and must carry the same meaning.

In case no code is known, it is allowed to specify a `@nullFlavor` for the code element and to give information about the (not yet coded) concept in original text. However, this should be an exception.

If other codes than the primary code need to be conveyed adding a translation child element can be used.

::: example

```xml
<code code="K12.0" codeSystem="2.16.840.1.113883.6.3" codeSystemName="ICD10" displayName="Recurrent oral aphthae">
    <originalText>minor aphthous stomatitis</originalText>
</code>
```

:::

| Core Element    |  DT  | Card | Conf | Description         |
| :-------------- | :--: | :--: | :--: | :------------------ |
| **translation** |  CD  | 0..* |  O   | Alternative code(s) |

A `translation` element is also of type CD and shall have a code and a code system identified. It shall point to the same concept as the primary code. It is allowed that a translation conveys less conceptual details than the original code.

::: example

```xml
<code code="K12.0" codeSystem="2.16.840.1.113883.6.3" codeSystemName="ICD10" displayName="Recurrent oral aphthae">
     <translation code="528.2" codeSystem="2.16.840.1.113883.6.2" codeSystemName="ICD9" displayName="Oral aphthae"/>
</code>
```

:::

### Coded Value (CV)

This is actually a specialization of CE (see above), with the exception that neither `originalText` nor `translation` child element are allowed.

### Coded Simple Value (CS)

This is actually a specialization of CE (see above), with no code system attribute and no original text or translation child element allowed. It specifies a concept by means of a code from a *predefined* code system (table) where the code is drawn from. This data type is usually applied in the context of fixed predefined HL7 tables.

::: example

```xml
<statusCode code="completed"/>
```

:::

In this example `statusCode` is bound in the underlying model to the specific code system *ActStatus*. Because it is predefined by means of the vocabulary binding, `@codeSystem` is omitted.

::: constraint

All conformance statements applicable to this data type can be found at the the [ART-DECOR table of V3 data types](../documentation/datatypes/).

:::

### Telecommunication Address (TEL)

Elements of this type have the following attributes.

| Core Attributes |  DT  | Conf | Description                                                  |
| :-------------- | :--: | :--: | :----------------------------------------------------------- |
| **@value**      |  ST  |  R   | carries strings that represent telecommunication contact information |
| **@use**        |  CS  |  O   | Represents the type of the telecommunication contact         |

The actual data is preceded by the scheme (refer to Internet Standard [RFC 1738 Uniform Resource Locators (URL)](https://tools.ietf.org/html/rfc1738).

Examples:

- telephone `tel:`
- fax `fax:`
- email `mailto:`

It must follow the rules defined in Internet [RFC 2806 URLs for Telephone Calls](https://tools.ietf.org/html/rfc2806).

The following codes will be used in the “use” attribute: 

- `H` – Home 
- `WP` – Work 
- `MC` can be used (in addition to `H` or `WP`) to denote that this is a mobile phone. 

::: example

```xml
<telecom value="tel:55354257" /> 

<telecom use="H" value="tel:55354257" /> 

<telecom use="H" value="fax:56542558" /> 

<telecom value="tel:+41.34.21.4765342"/>

<telecom value="mailto:opr@opr-sem.no"/>

<telecom use="WP MC" value="tel:97555786" />
```

:::

It is possible to express the useable period of a telecommunication contact. A child element `useablePeriod` is used as a time interval to indicate this period.

| Core Element      |   DT   | Card | Conf | Description                                     |
| :---------------- | :----: | :--: | :--: | :---------------------------------------------- |
| **useablePeriod** | IVL_TS | 0..* |  O   | validity range of the telecommunication contact |

::: example

```xml
<telecom value="tel:55354257">
    <useablePeriod>
        <high value="20111201"/>
    </useablePeriod>
</telecom>
```

:::

### Postal Address (AD)

Elements of this data type have a substructure in order to represent a structured address specification. In this implementation guide only a selection of all possible child elements is allowed, the original HL7 specification is richer.

| Core Elements         |  DT  | Card | Conf | Description                                           |
| :-------------------- | :--: | :--: | :--: | :---------------------------------------------------- |
| **streetAddressLine** |  ST  | 0..* |  O   | Street address, includes street name and house number |
| **city**              |  ST  | 0..* |  O   | City                                                  |
| **postalCode**        |  ST  | 0..* |  O   | Postal code                                           |
| **country**           |  ST  | 0..* |  O   | Country                                               |

::: note

The sequence of the sub elements is country specific, i.e. it has to follow “common” rules how to write an address. For example, an UK address and a German address have different sequences of address representation.

:::

A `@use` attribute can also be specified.

| Core Attribute |  DT  | Conf | Description                                          |
| :------------- | :--: | :--: | :--------------------------------------------------- |
| **@use**       |  CS  |  O   | Represents the type of the telecommunication contact |

::: example

```xml
<addr use="WP">
    <streetAddressLine>Anna-Louisa-Karsch-Str. 2</streetAddressLine>
    <postalCode>10178</postalCode>
    <city>Berlin</city>
</addr>
```

:::

::: example

```xml
<addr use="H WP">
    <streetAddressLine>Tatungasse 34</streetAddressLine>
    <postalCode>10825</postalCode>
    <city>Berlin</city>
</addr>
```

:::

Also, a country may be specified using the element `country`.

### Person Name (PN)

Elements of this data type do also have a substructure to represent the parts of a person name, like family or given name and prefixes.

::: example

```xml
<name>
    <given>Franz</given>
    <given>Ingrun</given>
    <family>Nordmann</family>
</name>
```

:::

Previous or birth or maiden names are supported using the `validTime` element. `validTime` (with child elements low and high, see data type [IVL_TS](#interval-of-time-ivl-ts)) specifies the validity range of the name.

| Core Element  |   DT   | Card | Conf | Description                |
| :------------ | :----: | :--: | :--: | :------------------------- |
| **validTime** | IVL_TS | 0..* |  O   | validity range of the name |

::: example

```xml
<!-- Birthname, used/valid up to 1-1-2000 -->
<name>
    <given>Jan</given>
    <given>Olaf</given>
    <family>Hendrikson</family>
    <validTime>
        <high value="20000101"/>
    </validTime>
</name>
```

:::

::: note

The name attribute is repeating, i.e. multiple (types of) names could be sent. It is up to the receiver (based on the value of the `@use` attribute, and `validTime`) to decide what name it wants to use or import in its database.

:::

### Organisation Name (ON)

Elements of this data type represent a name of an organization.

::: example

```
<name> Charité Berlin </name>
```

:::

The optional `validTime` element (with child elements low and high, see data type [IVL_TS](#interval-of-time-ivl-ts)) may be used to specify the validity range of the name of the organization.

| Core Element  |   DT   | Card | Conf | Description                                    |
| :------------ | :----: | :--: | :--: | :--------------------------------------------- |
| **validTime** | IVL_TS | 0..* |  O   | validity range of the name of the organization |

::: example

```xml
<name>The old hospital
    <validTime> 
        <high value="20111231235959"/>
    </validTime>
</name>
```

:::

::: note

The use of `validTime` yields XML mixed content which should be avoided if possible.

:::

### Integer Number (INT)

Elements of this data type are used to specify integer numbers and have one attribute.

| Core Attribute |  DT  | Conf | Description   |
| :------------- | :--: | :--: | :------------ |
| **@value**     | int  |  R   | Integer value |

::: example

```xml
<sequenceNumber value="24"/>
```

:::

### Physical Quantities (PQ)

Elements of this data type are physical quantities, i.e. a measurable quantity (or countable) value from the physical world. This is different from just a “number”, because there is a (natural) unit specified in which the measurable quantity is expressed. Examples:

- an amount of blood 20 ml (sample volume)
- a cut with a length of 10 cm (result observation)
- a dose of 200 milligrams per administration (dosing)
- delivery of 60 pieces of a tablet (amount supplied).

Elements of this data type have the following attributes.

| Core Attributes |  DT  | Conf | Description              |
| :-------------- | :--: | :--: | :----------------------- |
| **@value**      | real |  R   | value of the measurement |
| **@unit**       |  CS  |  R   | unit of measurement      |

In the unit of measurement unit is expressed following the *Unified Code for Units of Measure* (UCUM) specification. This specification is a collection of atomic units, prefixes and a corresponding grammar for valid combinations to build all-natural units of measurement. For a complete description of possible units, see the [*Unified Code for Units of Measure*](http://unitsofmeasure.org/) specification.

If a value is not countable or if measurable, for example expressed in milligrams or liters, then this unit is a valid UCUM unit like `mg` or `L`. If a value is countable, for example the number of tablets, then this attribute contains `1`.

In HL7 it is required that all PQ data type units are valid UCUM units.

::: example

```xml
<value value="165" unit="cm"/>

<value value="81.1" unit="kg"/>

<value value="120" unit="mm[Hg]"/>

<value value="5" unit="1"/>
```

:::

### Point in Time (TS)

Elements of this data type have the attribute

| Core Attribute |  DT  | Conf | Description   |
| :------------- | :--: | :--: | :------------ |
| **@value**     |  TS  |  R   | Point in time |

and specify points in time in the format `YYYYMMDDhhmmss±ZZZZ`, where 

- `YYYY` is the four-digit year, 
- `MM` the two-digit month, 
- `DD` the two-digit day, 
- `hh` the two-digit hour,
- `mm` the two-digits minute and
- `ss` the two-digit second.

Optional a time zone `ZZZZ`, the four-digit time alignment with a preceding `-` or a `+` can be specified. If time zone is omitted, local time is assumed.

The point in time may be right truncated in respect to the precision of the information.

::: example

```xml
<effectiveTime value="20140412"/>

<effectiveTime value="201409091245"/>

<effectiveTime value="1996"/>

<birthTime value="19540714"/>

<effectiveTime value="20140412+0100"/>
```

:::

### Intervals (IVL)

Intervals are defined only for data types with an ordinal or interval scale type. There are several possibilities to determine an interval (see Figure 2), for example to specify

- a lower `<low>` and an upper boundary `<high>` (a),
- a lower `<low>` boundary and width `<width>` (b)
- an upper boundary and width `<width>` (c) or
- the middle of the interval `<center>` and a width `<width>` (d).

<img src="../../img/image-20220821173152487.png" alt="image-20220821173152487" style="zoom:67%;" />

In addition, there is sometimes the need to specify incomplete intervals (open intervals). For example, an interval with only an upper limit to indicate a maximum dose or a start date and time only to express “since…”.

::: example

To indicate an interval, only the following combinations are permitted.

(1) A lower and an upper boundary may not be identical, and their accuracy has to be equal.

Correct examples:

- Interval from 5 to 6 
- Interval from 20110607 until 20110612

Incorrect examples:

- Interval from 5 to 6.0 
- Interval from 20110101 to 2011

(2) A lower boundary and a width

(3) A lower limit boundary

(4) A upper limit boundary

(5) Only the middle of the interval

(6) Only the width of an interval

:::

Accordingly, four child elements are available.

| Core Element |  DT   | Card | Conf | Description           |
| :----------- | :---: | :--: | :--: | :-------------------- |
| **low**      | *(T)* | 0..1 |  C   | Lower boundary        |
| **high**     | *(T)* | 0..1 |  C   | Upper boundary        |
| **center**   | *(T)* | 0..1 |  C   | Center of an interval |
| **width**    |  PQ   | 0..1 |  C   | Width of an interval  |

Where *(T)* is defined in this specification as one of the data types “TS” or “PQ”.

### Interval of time (IVL_TS)

Elements of this data type represent time intervals. There are four possible child elements.

| Core Elements |  DT  | Card | Conf | Description                      |
| :------------ | :--: | :--: | :--: | :------------------------------- |
| **low**       |  TS  | 0..1 |  C   | Lower boundary (start date/time) |
| **high**      |  TS  | 0..1 |  C   | Upper boundary (end date/time)   |
| **center**    |  TS  | 0..1 |  C   | Center of an interval            |
| **width**     |  PQ  | 0..1 |  C   | Width of an interval             |

Normally, a lower and an upper boundary is given, both are points in time. Also, it is possible to specify open intervals like “since June 14, 1999” (left boundary given only) or “until September 24, 2006” (right boundary given).

::: example

```xml
<effectiveTime>
    <low value="20110507"/>
    <high value="20110909"/>
</effectiveTime>

<effectiveTime>
    <low value="20110507"/>
</effectiveTime>

<value>
    <low value="20110101"/>
    <width value="120" unit="d"/>
</value>

<value>
    <center value="20110101"/>
</value>
```

:::

::: note

It must be noted that intervals of time stamps in HL7 are equally treated as other interval types (e.g. integers). The statement, one may take 1–3 tablets (interval of integers), for example, says that you are allowed to take no more than three tablets, not 3½ or 3.5 tablets. On the other hand, when treat the words “until 12 June 2011” regarding the upper interval limit differently, particularly we assume that the entire day 12 June is still included in this time interval.

Thus, in human perception time intervals are handled differently compared to such as number intervals. This is a situation may lead to inconsistencies in standardized communication. As a conclusion, an upper limit “12 June 2011” means that only the very first moment of the 12 June is included in the interval, but not, for example, the 12 June 13:00 hours.

Interpretation does not depend on the accuracy of the time: 

- “after 2004” means “from January 1, 2005 00:00:00.000”,
- “after 20041201” means “from 2 December 2004 00:00:00.000 hours”,
- and “after 200412011200” means “from 1 December 2004 12:01:00.000”.

As a result, you have to carefully consider the specification and implemen-tation of time intervals upper boundaries. In order to prevent mis-understandings intervals are always specified with the date and time for an upper limit that would cover the whole day. Accordingly, “201106122359” means 12 June 2011 throughout the day.

```xml
<effectiveTime>
    <low value="201106122359"/>
</effectiveTime>
```

Because of the equal treatment of time intervals this is applied also to the lower limit boundaries of time intervals as “0000”. This means for example that “since 9 June 2011” is expressed as “201106090000”.

:::

The `<width>` element is of data type PQ (physical quantity). The value obviously shall relate to a period of time and it is required to include a “time” unit. These are the units shown in the list below.

::: constraint

For interval of time stamps the `@unit` of a `low` or `high` child element shall use units

- `us` (microseconds),
- `ms` (milliseconds),
- `s` (seconds),
- `min` (minute),
- `h` (hours), 
- `d` (day),
- `wk` (week),
- `mo` (month) or
- `a` (year).

:::

### Interval of physical quantities (IVL_PQ)

Elements of this data type are used to indicate intervals of physical quantities (see also [Physical Quantities (PQ)](#physical-quantities-pq)). In general, an upper and a lower boundary is specified. It is also possible to use `center` instead of `low` and `high`.

| Core Elements |  DT  | Card | Conf | Description                                                |
| :------------ | :--: | :--: | :--: | :--------------------------------------------------------- |
| **low**       |  TS  | 0..1 |  C   | the lower limit of the interval (i.e. the minimum amount). |
| **high**      |  TS  | 0..1 |  C   | the upper limit of the interval (i.e. the maximum amount). |
| **center**    |  TS  | 0..1 |  C   | Center of the interval                                     |

The `center` child element is used when there is no interval, but an exact value. Alternative would be to demote IVL_PQ to data type PQ and populate the element’s `@value` attribute, but this is more difficult to process in XML. That is why the use of the `center` child element is deprecated.

::: constraint

All conformance statements applicable to this data type can be found at the the [ART-DECOR table of V3 data types](/documentation/datatypes/).

:::

::: example

```xml
<referenceRange>
    <low value="7.5" unit="mmol/l"/>
    <high value="10" unit="mmol/l"/>
</referenceRange>

<doseQuantity>
    <center value="9" unit="1"/>
</doseQuantity>
```

:::

### Ratio of quantities (RTO_QTY_QTY)

Elements of this data type specify ratios and have two child elements, a numerator and a denominator. The underlying QTY (Quantity) data type is an abstract data type and in a XML instance it has to be replaced by a specific data type using the `@xsi:type` instruction (see below).

In this specification only “PQ” or “INT” are allowed for the demotion of both `numerator` and `denominator`. The relevant child elements are:

| Core Elements   |  DT  | Card | Conf | Description                                |
| :-------------- | :--: | :--: | :--: | :----------------------------------------- |
| **numerator**   | (T)  | 1..1 |  R   | The amount of the numerator of the ratio   |
| **denominator** | (T)  | 1..1 |  R   | The amount of the denominator of the ratio |

If a model attribute already is a RTO_QTY_QTY, you need to specify (demote) the OTY abstract data type for both `numerator` and `denominator` by using an `@xsi:type` instruction. In some cases where the model attribute is of type “ANY”, also the root element shall carry an `@xsi:type` RTO_QTY_QTY instruction.

::: example

```xml
<maxDoseQuantity>
    <numerator xsi:type="PQ" value="6"/>
    <denominator xsi:type="PQ" value="1" unit="d"/>
</maxDoseQuantity>

<maxDoseQuantity>
    <numerator xsi:type="PQ" value="400" unit="mg"/>
        <denominator xsi:type="PQ" value="1" unit="d"/>
</maxDoseQuantity>

<maxDoseQuantity>
    <numerator xsi:type="PQ" value="5"/>
    <denominator xsi:type="PQ" value="1" unit="h"/>
</maxDoseQuantity>

<value xsi:type="RTO_QTY_QTY">
    <numerator xsi:type="INT" value="1"/>
    <denominator xsi:type="INT" value="10000"/>
</value>
```

:::

## Identification mechanisms

### Object Identifiers (OIDs)

Standardized exchange of information using messages or documents also deals with unique identifications of objects and concepts, especially in environments where sender and receiver do not “know” each other necessarily.

It is important to understand the difference between identification of instances (ids) and code concepts (classifications). Within the context of HL7 interactions OIDs are quite often used.

An id points to a specific instance of an object or identification scheme (a particular methodology for the identification of a class of objects), for example a specific person as the patient or a physician, a specific lab observation or an X-ray photo.

In contrast to instance identifiers, a classification identifies a specific concept. The type of patient (inpatient, outpatient), the type of doctor (an anesthesiologist), the codes for gender or a lab observation (blood cell count) may serve as examples.

OIDs (object identifiers) are worldwide unique identifiers for objects. They are used in HL7 to specify identifications and classifications. An object in this sense is persistent, well-defined information, definitions or specifications and is represented with ids or codes.

Commonly used OIDs (i.e. used for a purpose that is wider in scope than the regional healthcare organization; used in communications between healthcare regions) can be found in OIDs registries such as

- [www.oid-info.com](http://www.oid-info.com/), 
- [www.hl7.org](http://www.hl7.org/), 
- [HL7 OID registry in the UK](http://www.hl7.org.uk/version3group/oids.asp), 
- [OID registry Germany](https://www.dimdi.de/dynamic/de/klassifikationen/weitere-klassifikationen-und-standards/oid/).

::: note

All OIDs with an element of `999` or `9999` are for documentation purposes only. They will be replaced by permanent OIDs once these have been assigned/found/applied for.

:::

OID (Object Identifiers) are unique identifiers for any kind of objects. They are defined in Rec. ITU-T X.660 | ISO/IEC 9834-1. This identification system for objects and concepts makes reliable electronic information exchange possible. Administration and Registration is regulated by a set of rules, described in the corresponding chapter of this document.

Every artefact in ART-DECOR has an Object Identifier (OID). There are recommendations for the management and the exchange of Object Identifiers, partially defined in [ISO TS 13582](https://www.iso.org/standard/69652.html).

*HL7 International* has created a type ontology for the OIDs in the HL7 registry to make it easier for the user community to search for OIDs they may be looking for and can be found at hl7.org.

From a policy perspective it is common practice in several healthcare related OID registries (HL7 International at [http://www.hl7.org](http://www.hl7.org/), OID Registry Germany at [http://www.dimdi.de](http://www.dimdi.de/), OID registry Switzerland at [http://oid.refdata.ch](http://oid.refdata.ch/), and OID registry Austria at https://www.gesundheit.gv.at/OID_Frontend) to distinguish at least between the following types of OIDs:

| Arc  | Description                       |
| :--: | :-------------------------------- |
|  .3  | organizational bodies and groups  |
|  .4  | identifier systems and namespaces |
|  .5  | code systems                      |
|  .7  | document artefacts                |
|  .9  | conformance profiles              |
| .10  | templates                         |
| .11  | value sets                        |
| .19  | examples                          |
| .99  | experimental                      |

::: example

Given that “your” root OID is `1.2.3.4.5`, the first value set of your governance group is then to be `1.2.3.4.5.11.1`, with `.11` as the value set “branch” and the last `.1` as the first artefact of that kind.

:::

In **ART-DECOR** a project typically is set into the `.77` branch of a governance group. When setting up the project, the OID for that project also determines the artefact OIDs using throughout the ART-DECOR project. 

Additional [branches are used in ART-DECOR](../documentation/artefacts/#artefact-types) to distinguish between different OID types. In ART-DECOR a newly created project uses these OID branches automatically.

### Instance identification

Instance identification is done with the id element of a class. Its type is II (instance identifier) and described in the data type section above. For example, a patient number can be represented as

- `2.16.578.1.34.1.5432.1`, the root-OID of the issuing authority, for example a system/application in a hospital, and
- `67543242` as the `@extension`, representing the patient number within the system defined by the `@root` attribute.

### Coding scheme identification

Coding schemes have to be uniquely identified using OIDs as well. All coding tables used in this implementation guide show the corresponding OID that has to be specified in the `@codeSystem` attribute of coded elements. Both attributes `@code` and `@codeSystem` together allows a correct interpretation of concept and code.

### List of identification schemes

The following are OIDs for commonly used identification schemes.

| OID               | Identification System Name | Description               |
| :---------------- | :------------------------- | :------------------------ |
| 1.2.276.0.76.4.8  | KV-Nummer                  | Krankenversichertennummer |
| 1.2.276.0.76.4.17 | BSNR                       | Betriebsstättennummer     |
| 1.2.276.0.76.4.16 | LANR                       | lebenslange Arztnummer    |

### List of code systems

The following are OIDs for code systems.

| OID                    | Code System Name            | Description                                                  |        Issuer         |
| :--------------------- | :-------------------------- | :----------------------------------------------------------- | :-------------------: |
| 2.16.840.1.113883.6.1  | LOINC                       | Logical Observation Identifiers Names and Codes (LOINC)      | Regenstrief Institute |
| 2.16.840.1.113883.6.96 | SNOMED CT                   | Systemized Nomenclature in Medicine Reference Terminology (SNOMED CT) | SNOMED International  |
| 1.0.3166.1.2           | ISO 3166-1 (second edition) | Country codes as per ISO 3166-1:2007                         |          ISO          |

## Vocabulary

### Code Systems

Within HL7, a *Code System* is defined as a collection of codes with associated designations and meanings. Examples of code systems include ICD-10, SNOMED CT, and LOINC. 

To meet the requirements of a code system as defined by HL7, a given code must resolve to one and only one meaning within the code system. Given this definition, each code table in the HL7 Version 3 standard represents a different code system since codes are sometimes used in different tables to have different meanings. 

For example, `M` in the administrative gender table (*AdministrativeGender*, OID `2.16.840.1.113883.5.1`) means “male“, while `M` in the marital status table (*MaritalStatus*, OID `2.16.840.1.113883.5.2`) means “married”.

In this specification, code system tables are not presented in detail.

### Value Sets

A *Value Set* represents a uniquely identifiable set of valid concept representations, where any concept representation can be tested to determine whether or not it is a member of the value set.

Value set complexity may range from a simple flat list of concept codes drawn from a single code system, to an unbounded hierarchical set of possibly post-coordinated expressions drawn from multiple code systems.

Value sets exist to constrain the content for a coded element in an HL7 static model or data type property. Value sets cannot have null content and must contain at least one concept representation where any given concept is generally (but not required to be) represented by only a single code within the Value Set.

![image-20220821113643315](../../img/image-20220821113643315.png)

*Example presentation of the value set “MaritalStatus”*

::: note

Please note that value set OIDs never appear in a CDA Release 2 document. They are used for constraint expressions, for example in implementation guides.In order to send a specific code drawn from a value set, the corresponding code system has to be conveyed.An example, in the implementation specification the constraint on the model element *maritalStatusCode* is expressed as “to be drawn from value set 2.16.840.1.113883.5.2 *MaritalStatus*”.

To actually send a marital status of “M” (married) that is shown in the value set figure above, the corresponding HL7 CDA instance would say:

```xml
<maritalStatusCode code="M" codeSystem="2.16.840.1.113883.5.2"/>
```

:::

In this specification, value set tables are not presented in detail.

#### Binding strength

There is an optional vocabulary strength for data types that support that feature (coded concepts). Examples include CD, CE, CV and CO. Values are required, extensible, preferred and example. Default is required.

Please note: the formerly supported values CNE (Coded No Extensibility) and CWE (Coded With Extensibility) in ART-DECOR are deprecated as of December 2017. Specification CNE is handled as required and CWE is handled as extensible.

|  Strength  | Description                                                  |
| :--------: | :----------------------------------------------------------- |
|  required  | Required/CNE. Coded with no exceptions; this element SHALL be from the specified value set |
| extensible | Extensible/CWE. Coded with Exceptions; this element SHALL be from the specified value set if any of the codes within the value set can apply to the concept being communicated. If the value set does not cover the concept (based on human review), alternate codings (or, data type allowing, text) may be included instead. |
| preferred  | Preferred. Instances are encouraged to draw from the specified codes for interoperability purposes but are not required to do so to be considered conformant. |
|  example   | Example. Instances are not expected or even encouraged to draw from the specified value set. The value set merely provides examples of the types of concepts intended to be included. |

Mismatches with required lead to validation errors (SHALL logic). Mismatches with extensible lead to validation warnings (SHOULD logic). Mismatches with preferred lead to validation information messages (MAY logic). Mismatches with example don't have any validation consequences.