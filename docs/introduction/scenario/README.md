---
permalink: /introduction/scenario
---
# Scenario

## Definition

::: definition SCENARIO

A Scenario is a definition of a process for information capturing or exchange. It is comprised of one or more Transaction Groups that contains one or more Transactions with Actors involved and with items from a source dataset to reflect the content of a Transaction.

<adimg scenario/> is the symbol used throughout the app.

The subsumed artefacts are:

<adimg transaction/> transactions, 

and transaction of specific type

* *initial*, <adimg transactioninitial/>,
* *back* <adimg transactionback/> and
* *stationary* <adimg transactionstationary/>.

:::

Scenarios and datasets are the area of the healthcare providers: they know what data they want to see, what data they want to exchange. Scenarios are about the dynamic aspect of data: when information is exchanged and between what actors.

Content wise, Scenarios formally define healthcare situations or events in terms of concepts. These concepts are specified in (and can be selected from) existing datasets. Such a dataset can be specifically defined for a given healthcare domain, such as for instance, laboratory or perinatology.

## Scenarios

Scenarios are a container for Transaction Groups which are container for Transactions and will typically represent a health care use case, e.g. a Medication Prescription or a Radiology Examination. Scenarios/Transactions provide the usage context for starting information capturing or exchange. 

## Transaction Group and Transaction Leaf

Transaction Groups are containers for Transactions (Transaction Leaves). There are two categories of Transaction Leaves.

### Category 1: *Inital* and *Back* Transactions

This is characterized as a pair of *initial* and *back* Transactions used in the messaging paradigm where an initial information transfer evokes a response message in terms of an acknowledgement or in a query/response environment. This Transaction typically has a sender and a receiver interacting.

<img src="../../img/image-20220701162755603.png" alt="image-20220701162755603" style="zoom:50%;" />

### Category 2: *Stationary* Transactions

The "stationary" transaction that is a self-referencing Transaction for example to create a document. This interaction typically has a "sender" only, that is more a creator of the document. Sending and receiving aspects are not part of this category of transaction and handled separately.

<img src="../../img/image-20220701162810130.png" alt="image-20220701162810130" style="zoom:50%;" />

### Sequence Diagrams

ART-DECOR shows this kind of Transaction also in a graphical way at the Transaction Group visualization, summarizing all Transaction in the Group as Sequence Diagrams.

<img src="../../img/image-20220701163409264.png" alt="image-20220701163409264" style="zoom: 50%;" />

Transactions represent a (sub)set of concepts from a dataset and add cardinality, conformance and possible conditions to them so they reflect the use case demands appropriately. A transaction typically has a reference to a representing template, that is the technical representation of the transaction and its underlying concepts.

















<img src="../../img/image-20220701160733622.png" alt="image-20220701160733622" style="zoom:50%;" />

<img src="../../img/image-20220701160749631.png" alt="image-20220701160749631" style="zoom:50%;" />

## Transactions as FHIR Questionnaire

ART-DECOR supports serializing any transaction as a FHIR Questionnaire, see Questionnaire.
