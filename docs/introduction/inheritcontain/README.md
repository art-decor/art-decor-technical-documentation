---
permalink: /introduction/inheritcontain
---
# Inheritance / Containment

## Inherited Concepts

Inheritance is a „copy of a design“. Upon creation of an inherited concept, a structural copy is created from another location into the current data set (left), containing references to each and every concept in the source (right).

<img src="../../img/image-20220701125552318.png" alt="image-20220701125552318" style="zoom:50%;" />

If a concept in a dataset is inherited from another dataset, the status icon is actually a chain symbol for items and a outline folder for groups in the respective color.

In this example the item *Blutzucker* is a genuine project item while *Atemfrequenz* is a inherited item from another dataset, so it carries the appropriate symbol.

![image-20210929172318060](../../img/image-20210929172318060-2928999.png)

In order to be able to edit an inherited concept, you must *de-inherit* the concept first.

In the example above you can easily do that through a special DE-INHERIT button with a un-link symbol in the Dataset Concept Detail Top Bar. After this action the concept is copied to your project with all properties including a relationship indicator to the formerly inherited dataset concept.

In a hierarchical tree, usually also all parent concepts need to be de-inherited. In this example the desire is to alter *Symptomdauer* that is part of the inherited group *Beschwerden bei Vorstellung*.

![image-20210929170207538](../../img/image-20210929170207538-2927728.png)

The reason for de-inheriting the group first before you can do anything with its children is, that you are potentially altering the semantics of a concept and therefore potentially also the semantics of all parent concepts. Therefore you need to de-inherit your target concept and all parents to indicate to anybody, that there is a possible change in the meaning of that group/block or concept. De-inherit all parent concepts as well first, i.e. here the group *Beschwerden bei Vorstellung*.

After that you can de-inherit the desired concept *Symptomdauer* in the same way and edit it.

![image-20210929170236537](../../img/image-20210929170236537-2927758.png)

After having de-inherited the group you might now also add or change items in the group, as an example to add another element to the de-inherit group, called *Erheber*.

![image-20210929170255286](../../img/image-20210929170255286-2927776.png)

After this action, group *Beschwerden bei Vorstellung* has a normal group symbol and a changed definition because *Erheber* has been added. *Beschwerden bei Vorstellung (Freitext)* is obviously left untouched so far (still has the chain symbol).

## Containment Concepts

Containment is a true reference (à la “sym links” in other contexts) of a Concept Group. Upon creation of a concept, this is a transparent link to an original concept at another location, denoted with the "share all" icon. Containment is only possible with groups.

<img src="../../img/image-20220701125149297.png" alt="image-20220701125149297" style="zoom:50%;" />

It is possible to transition a containment relationship into a true inheritance relationship. The original group is turned into a set of inherited concepts. An inherited group is denoted by the open folder outline symbol, inherited items by the chain symbol.

<img src="../../img/image-20220701125710065.png" alt="image-20220701125710065" style="zoom:50%;" />

It is also possible to transition a containment relationship into a true copy. It causes also a relationship indication to the original ("concept comes from this original concept"). For that purpose the formal inheritance indicator is replaced by an informal relationship indicator.

<img src="../../img/image-20220701125903855.png" alt="image-20220701125903855" style="zoom:50%;" />