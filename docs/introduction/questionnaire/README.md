---
permalink: /introduction/questionnaire
---
# Questionnaire

## Definition

::: definition QUESTIONNAIRE

A Questionnaire is a kind of electronic form to be filled in with collection of questions to be answered by a user.

<adimg questionnaire/> is the symbol used throughout the app.<p></p>

The subsumed artefacts are:

<adimg questionnaireresponse/> a stored Questionnaire Response<p></p>

<adimg questionnaireedit/> an edited Questionnaire (in contrast to a genuinely derived and unchanged Questionnaire)<p></p>

:::
