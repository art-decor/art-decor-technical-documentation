---
permalink: /introduction/rules
---
# Rules

## Definition

::: definition RULES

Rules is a summarizing term in ART-DECOR for any set of constraints upon an underlying specification such as the

- Clinical Document Archtecture (CDA) or
- Fast Healthcare Interoperability Resources (FHIR).

<adimg rules/> is the symbol used throughout the app.<p></p>

:::
