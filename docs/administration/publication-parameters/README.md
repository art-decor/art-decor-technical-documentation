---
permalink: /administration/publication-parameters
---
# DECOR-Publication-Parameters

The basic setup of a publication has parameters documented in the *decor-parameters.xml* file. A complete setup example is shown below.

The parameters are divided into several (virtual) section for the area of parametrizing

- General switches 
- ISO schematron generation
- Human readable publication

The following sections explain the switches and parameters, default parameters are shown in **bold**.

## General switches

### inDevelopment

Create artifacts in timestamped directories (**inDevelopment0**) or create artifacts in pfx-runtime-develop / pfx-html-develop (inDevelopment1) while we are just publishing/testing intermediate results

```
<inDevelopment1/>
```

### defaultLanguage

Override /decor/project/@defaultLanguage with a specific language. Value ALL activates all languages

```
<defaultLanguage>nl-NL<defaultLanguage>
```

### artdecordeeplinkprefix

URI for ART-DECOR, e.g. http://art-decor.org/art-decor/. MUST end with a /. The value is used to build the URI to the live project for example to get to full issue details. If you do not set it here, we will try decor/@deeplinkprefix which gets set through project compilation, if that has no value, and you do not supply it directly to DECOR2schematron (e.g. through the ADRAM service), the value is empty and links will not work.

```
<artdecordeeplinkprefix>http://x.y.z</artdecordeeplinkprefix>
```

### logLevel

Determines the log level of the DECOR to schematron transformation.

- The INFO level designates informational messages that highlight the progress of the application at coarse-grained level.
- The ALL has the lowest possible rank and is intended to turn on all logging.
- The DEBUG Level designates fine-grained informational events that are most useful to debug an application.
- The WARN level designates potentially harmful situations.
- The ERROR level designates error events that might still allow the application to continue running.
- The FATAL level designates very severe error events that will presumably lead the application to abort.
- The OFF level has the highest possible rank and is intended to turn off logging.

## ISO schematron generation

### switchCreateSchematron

Create Schematron with **switchCreateSchematron1** or don't create switchCreateSchematron0

```
<switchCreateSchematron1/>
```

### switchCreateSchematronWithWrapperIncludes

Create Schematron with HL7v3 messaging wrappers includes switchCreateSchematronWithWrapperIncludes1 or don't create with **switchCreateSchematronWithWrapperIncludes0**

```
<switchCreateSchematronWithWrapperIncludes1/>
```

### switchCreateSchematronClosed

Create closed qualification quality schematrons switchCreateSchematronClosed1 or respect whatever is actually configured **switchCreateSchematronClosed0**

```
<switchCreateSchematronClosed1/>
```

### switchCreateSchematronWithWarningsOnOpen

This switch if on (switchCreateSchematronWithWarningsOnOpen1) causes the schematron to contain warnings on encountered instance parts that were not defined. While legal from the perspective of open templates, you may still want to be warned when this occurs during testing/qualification. Default is **switchCreateSchematronWithWarningsOnOpen0**.

```
<switchCreateSchematronWithWarningsOnOpen1/>
```

### switchCreateSchematronWithExplicitIncludes

Create Schematrons with includes only for those that are actually part of the template representingTemplate/@ref and/or representingTemplate/@flexibility switchCreateSchematronWithExplicitIncludes1 or all **switchCreateSchematronWithExplicitIncludes0**

```
<switchCreateSchematronWithExplicitIncludes1/>
```

### switchCreateDatatypeChecks

If switchCreateDatatypeChecks1 all XML elements with a datatype are created with an extends in Schematron. Normally left active. Defaults to **switchCreateDatatypeChecks0**.

```
<switchCreateDatatypeChecks1/>
```

### bindingBehavior

Switch bindingBehavior determines the binding behavior for the schematron engine and influences schematron generation:

- *freeze*: The schematron engine generates code for dynamically bound value sets by treating the most recent version at the time of generation as static (freeze the most recent version and treat it as if it was declared static). **freeze** is the default.
- *preserve*: The schematron engine generates code for dynamically bound value sets by using the actual most recent version at the time of validation and therefore create true value set look-up code to retrieve the recent version from the ART-DECOR terminology service (preserve the real most recent version and treat it as such)

```
<bindingBehavior valueSets="freeze"/>
```

## Human readable publication

### switchCreateDocHTML

Create HTML documentation switchCreateDocHTML1 or don't create **switchCreateDocHTML0**

```
<switchCreateDocHTML1/>
```

### switchCreateDocSVG (deprecated)

Create SVG images where applicable switchCreateDocSVG1 or don't create **switchCreateDocSVG0**. This switch has been declared deprecated and SVGs are always created regardless of this switch.

### switchCreateDocPDF

If the conversion process should create the PDF based documentation the element `<switchCreateDocPDF1 include="...">` should be present; creating PDF is skipped with **switchCreateDocPDF0**.

The attribute @include of this element should contain letters 

- d datasets
- s scenarios
- n identifiers
- t terminologies
- r rules, and/or 
- i for issues

to be included in the PDF. If omitted, the default **dsntri** (all) is assumed.

```
<switchCreateDocPDF1 include="dsntri"/>
```

### switchCreateDocDocbook (deprecated)

Create DocBook documentation switchCreateDocDocbook1 or don't create **switchCreateDocDocbook0**

```
<switchCreateDocDocbook0/>
```

### useLocalAssets

Use local assets dir ../assets (useLocalAssets1) instead of online version (**useLocalAssets0**) on http://art-decor.org/ADAR/rv/assetsvxx/ (xx depends on DECOR2Schematron.xsl)

```
<useLocalAssets1/>
```

### useLocalLogos

Use local logos dir ../pfx-logos (useLocalLogos1) instead of online version (**useLocalLogos0**) on /decor/project/reference + /pfx-logos/

```
<useLocalLogos1/>
```

### useCustomLogo

Add a logo to the top right on HTML pages, optionally with a URL (useCustomLogo1) or don't add (**useCustomLogo0**). Logo @src is mandatory and may be relative local path or full URL. The logo may have URL in @href as a link to a website.

```
<useCustomLogo0 src="../pfx-logos/nictiz.jpg" href="http://pfx.art-decor.org"/>
```

### useLatestDecorVersion

If set useLatestDecorVersion1, decor2html will get overview results using RetrieveTransaction for the most recent version in project-decor.xml. If absent or **useLatestDecorVersion0**, current RetrieveTransaction results will be used.

```
<useLatestDecorVersion1/>
```

### createDefaultInstancesForRepresentingTemplates

Creates default pseudo XML instances in XML and html for representingTemplates in HTML folder createDefaultInstancesForRepresentingTemplates1 or not **createDefaultInstancesForRepresentingTemplates0**.

```
<createDefaultInstancesForRepresentingTemplates0/>
```

### useCustomRetrieve

Customizations to be used in RetrieveTransaction. May contain attribute @hidecolums, which contains columns to be hidden when calling RetrieveTransaction (defaults to "45ghi").

```
<useCustomRetrieve1 hidecolums="45"/>
```

### switchCreateTreeTableHtml

Conversion process should create HTML tables (e.g. templates) the traditional way switchCreateTreeTableHtml0 or **switchCreateTreeTableHtml1** should create HTML tables (e.g. templates) with a binding on treetable.js which enables folding of element. 

This option is not recommended for wiki and in fact overridden (ignored) if you do wiki publication through [ADAWIB](https://art-decor.org/mediawiki/index.php?title=ADAWIB).

If you use switchCreateTreeTableHtml1 in a publication through [ADRAM](https://art-decor.org/mediawiki/index.php?title=ADRAM) together with option switchCreateDocPDF1, a second HTML generation process is done through the conversion in order to allow HTML with folding of elements enabled and a proper PDF rendition.