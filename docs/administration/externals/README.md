---
permalink: /administration/externals
---
# External Services

ART-DECOR comes with several useful External Services that run under Linux, typically as PHP scripts for a couple of special tasks.  Many of the features formerly useful in ART-DECOR Release 2 are now already built-in ART-DECOR Release 3. 

The following  External Services may still be useful.

- ART-DECOR External Services: [ADPING](ADPING.md)
