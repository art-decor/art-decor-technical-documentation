---
permalink: /administration/adping
---
# ART-DECOR PING Service (ADPING)

## What is it?

The ART-DECOR PING Service informs administrators of an ART-DECOR server whether he is up or down by means of a status email about up/down changes of the server.

## Installation on ART-DECOR Server Level

At the ART-DECOR server at Linux user level, logged in as root:

Go to the hosting directory for ART-DECOR scripts, for example `/opt`

Create a new directory there called art-decor-ping by using

```sh
mkdir art-decor-ping
```

Change user of the directory to be root by using

```sh
chown root:root art-decor-ping
```

Change the access rights to read-write-execute for user root only by using

```sh
chmod 700 art-decor-ping
```

Get the script from the repository.

You should see the following files

```sh
art-decor-ping.php
art-decor-ping.sh
config.php
```

Edit `config.php` and fill in the variables `$url1` and `$recipients` appropriately, especially the parts marked in yellow:

```php
<?php
// the URL to the main page of the art-decor server to ping
$url1 = "http://URL-TO-YOUR-ART-DECOR-SERVER-LANDING-PAGE";
// recipients to inform on status changes, separated by comma
$recipients = "reply.not.possible@art-decor.org";
?>
```

Replace `http://URL-TO-YOUR-ART-DECOR-SERVER-LANDING-PAGE` with the appropriate URL to the main page of your ART-DECOR (this shall not be http://art-decor.org/art-decor/home).

Replace `reply.not.possible@art-decor.org`with the appropriate recipient email addresses, a single one or multiple, separated by comma.

Test it at the command line by using

```sh
sh art-decor-ping.sh
```

The “status” file should appear representing the latest status of the server. It is used internally by the script only to keep record of the status. If status changes, an email is sent. 

Add a crontab for the PING, e.g. every ten minutes over the hour would look like:

| Crontab                                                      |
| ------------------------------------------------------------ |
| 0-59/10   *   *   *   *   /opt/art-decor-ping/art-decor-ping.sh |

 
