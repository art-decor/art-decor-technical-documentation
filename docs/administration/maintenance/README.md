---
permalink: /administration/maintenance
---
# Server Maintenance Periods

The Servers part of our Cluster are undergoing regular maintenance, i. e. copy and extract of the database and check and repairs.  The following tables show the mainteance periods for our user related Cluster mebers.

| Server Cluster Member           | Maintenance Time (CET / CEST) |                                                              |
| ------------------------------- | ----------------------------- | ------------------------------------------------------------ |
| MAIN (art-decor.org)            | 1:00am until 1:45am           | <img src="../../img/image-clock-01000145.png" alt="image-20230731113918125" style="zoom:10%;" /> |
| DEVELOP                         | 1:00am until 1:45am           | <img src="../../img/image-clock-01000145.png" alt="image-20230731113918125" style="zoom:10%;" /> |
| ACCEPTANCE                      | 1:00am until 1:45am           | <img src="../../img/image-clock-01000145.png" alt="image-20230731113918125" style="zoom:10%;" /> |
| Customer Server Cluster Members | 1:00am until 1:45am           | <img src="../../img/image-clock-01000145.png" alt="image-20230731113918125" style="zoom:10%;" /> |
| NEWBIE / Sandboxes              | 5:00am until 5:30am           | <img src="../../img/image-clock-05000530.png" alt="image-20230731114041587" style="zoom:10%;" /> |

During these times access to the server may be very limited or even restricted.

For live updates on uptimes and downtimes and urgent announcements visit our [Live on Air](/administration/liveonair) page.
