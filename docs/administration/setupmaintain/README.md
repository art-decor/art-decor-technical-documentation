---
permalink: /administration/setupmaintain/
---
# Setup and Maintenance Support

Depending on what you want to establish on your (local) server you need follow a few steps for installation. You can install a server with ART-DECOR Release 3 only, a server with both ART-DECOR Release 3 and Release 2 (a so-called duplex server) or you can run installations to create a ART-DECOR Release 2 only server.

::: warning

Please note that all ART-DECOR Release 2 components reach **End of Service/Support** (EOSL) by the **end of 2023**. 

:::

## ART-DECOR Release 3 only

::: note

This installation is recommended.

:::

1. Install **Database Backend**, following the ART-DECOR eXist-DB database [backend installer](/administration/setupmaintain/backend/) scripts.
2. Install **VUE Frontend and proxy ART-DECOR Release 3**, folllwing the ART-DECOR [Vue Frontend and http-proxy](/administration/setupmaintain/frontend3/) installation instructions.
3. Look after the  [**eXist scheduled jobs**](/administration/setupmaintain/scheduler/) required or recommended for ART-DECOR and configure the scheduled jobs.

## ART-DECOR Release 3 and Release 2

::: note

This installation establishes a hybrid or duplex server. Please note the **End of Service/Support** (EOSL) note for Release 2 components above.

:::

1. Install **Database Backend**, following the ART-DECOR eXist-DB database [backend installer](/administration/setupmaintain/backend/) scripts.
2. Install **VUE Frontend and proxy ART-DECOR Release 3**, folllwing the ART-DECOR [Vue Frontend and http-proxy](/administration/setupmaintain/frontend3/) installation instructions.
3. Install **Orbeon/Tomcat Frontend ART-DECOR Release 2**, following the ART-DECOR Release 2 [Orbeon and Tomcat](/administration/setupmaintain/frontend2/) installation instructions.
4. Look after the  [**eXist scheduled jobs**](/administration/setupmaintain/scheduler/) required or recommended for ART-DECOR and configure the scheduled jobs.

## ART-DECOR Release 2

::: warning

Please note the **End of Service/Support** (EOSL) note for Release 2 components above.

:::

1. Install **Database Backend**, following the ART-DECOR eXist-DB database [backend installer](/administration/setupmaintain/backend/) scripts.
2. Install **Orbeon/Tomcat Frontend ART-DECOR Release 2**, following the ART-DECOR Release 2 [Orbeon and Tomcat](/administration/setupmaintain/frontend2/) installation instructions.
3. Look after the  [**eXist scheduled jobs**](/administration/setupmaintain/scheduler/) required or recommended for ART-DECOR and configure the scheduled jobs.

## User Data Migration

If you need to convert "older" exist-db data into a newer, not binary-compatible version, follow our [Migration Instructions](/release2/migration/)

## CRON trigger

If needed, please consult detailed information about the [CRON trigger](/administration/setupmaintain/cron/) methodology used by ART-DECOR.

## Database Recovery

If the database gets corrupted you may want to try **Database Recovery** using the eXist-DB [Recovery Methods](/administration/setupmaintain/recovery/).
