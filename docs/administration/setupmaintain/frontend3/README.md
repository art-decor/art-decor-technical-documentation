---
permalink: /administration/setupmaintain/frontend3/
---
# Install VUE frontend for ART-DECOR�

To perform the following instructions you need to have root ssh access to your server. Alternatively, you can use the sudo command accordingly.

::: note

Since ART-DECOR Release 3.5, the **VUE frontend** always has to run together with **ART-DECOR HTTP Proxy**. Therefore this installation describes how to install both.

:::

## Preliminaries

Along with the typical unix command you also need to check / install the following commands. Use the unix package management system like *yum* (Yellow-Dog Updater Modified) or *dnf* (Dandified *yum*) or *rpm* to install the commands.

```sh
> git --version
git version 1.8.3.1
# optionally install git

> npm -v
8.15.0
# optionally install npm
```

At the time of writing this documentation the required versions (or higher) are shown. Please check.

## Place the install script and settings file

We adivice to place ART-DECOR related stuff in `/opt`. You may choose for another location but our instructions refer to that directory.

First check if the `art-decor-vue` directory with vue installation scripts does not already exist:

```sh
ls -al /opt/art-decor-vue
```
Expected for a new installation: no such file or directory.

Continue with the installation: change directory:

```sh
cd /opt/
```

Download the scripts and settings files from the repository.

```
https://repository.art-decor.org/installers/art-decor-vue.tar.gz
```

You can use typical download file command line tools, for example (replace "..." by the URL above):

```
curl -O ...
wget ...
```

Fyi: The original files are located in https://bitbucket.org/art-decor/art-decor-external-tools/src/master/art-decor-vue/.

Unpack the .gz file:

```
tar zxvf art-decor-vue.tar.gz
```

Expected file and folder setup:

```
/opt/art-decor-vue/vue_pull.sh
/opt/art-decor-vue/vue_patch_env.sh
/opt/art-decor-vue/proxy_pull.sh
/opt/art-decor-vue/  ....more files...
```

Change access rights for the scripts.

```sh
cd /opt/art-decor-vue/
chmod +x ./vue_pull.sh
chmod +x ./vue_patch_env.sh
chmod +x ./proxy_pull.sh
chmod +x ./proxy_systemd_deploy.sh
```

Fyi: The default configuration will create a git folder: /opt/art-decor-vue/art-decor 
With these default settings_vue:
```
# where is our git local working copy located on this server
GIT_LOCAL_WORKING_COPY_DIR=/opt/art-decor-vue/art-decor
```

## Typical location for VUE frontend

Configure the location for the vue frontend files in the file: settings_vue, configuration parameter: VUE_PUBLIC_DIR_NGINX

The typical location for the vue frontend files are in the respective directory for the website in a subdirectory ad. As an example if your website http files are located in `/xxx/yyy/httpdocs` the typical directory for the VUE frontend files is `/xxx/yyy/httpdocs/ad`.

## Install VUE frontend

Get the VUE frontend from the repo and install it.

If you install it on a local machine (laptop etc.) use

```sh
./vue_pull.sh
```

On servers run the script as shown here

```sh
./vue_pull.sh --local
```

## Install ART-DECOR HTTP Proxy

Get the ART-DECOR HTTP Proxy from the repo and install it.

```sh
./proxy_pull.sh
```

This action also starts the ART-DECOR HTTP Proxy at port 3000.

### Additional first time installation actions

When installing the ART-DECOR HTTP Proxy for the first time, please consider to run the systemd installation as well. This creates a systemd script with `proxy_systemd_deploy.sh`, named: **`art-decor-http-proxy.service`**

```
./proxy_systemd_deploy.sh
```

With this you can check, start and stop the ART-DECOR HTTP Proxy service.

```
systemctl status art-decor-http-proxy.service
systemctl start art-decor-http-proxy.service
systemctl stop art-decor-http-proxy.service
```

::: note

The ART-DECOR HTTP Proxy service is started / restarted also upon updating it using the normal installer/updater. Only if there are updates in the repository, rebuild and stop/start is performed.

```
./proxy_pull.sh
```

:::

## NGINX settings

### Existing NGNX

Your nginx settings shall be adapted. Consider adding the following `location` additions.

```nginx
# AD3 routes
# rewrite /exist/apps/api to proxy
location /exist/apps/api {
	proxy_connect_timeout       900;
	proxy_send_timeout          900;
	proxy_read_timeout          900;
	send_timeout                900;
	proxy_pass http://localhost:3000;
}
# end of location /exist

# rewrite proxies
location /socket.io {
	proxy_connect_timeout       900;
	proxy_send_timeout          900;
	proxy_read_timeout          900;
	send_timeout                900;
	proxy_pass http://localhost:3000;
	proxy_http_version 1.1;
	proxy_set_header Upgrade $http_upgrade;
	proxy_set_header Connection "upgrade";
}
location /image-proxy {
	proxy_connect_timeout       900;
	proxy_send_timeout          900;
	proxy_read_timeout          900;
	send_timeout                900;
	proxy_pass http://localhost:3000;
}
# end proxies

# AD2 routes
# rewrite /art-decor
location /art-decor {
	# 20171011 timeouts
	proxy_connect_timeout       900;
	proxy_send_timeout          900;
	proxy_read_timeout          900;
	send_timeout                900;
	proxy_pass http://localhost:8080/art-decor;
}
# end of location /art-decor

# rewrite /ada
location /ada {
	proxy_pass http://localhost:8877/exist/ada;
}
# end of location /ada

# rewrite /art
location /art {
	proxy_pass http://localhost:8877/exist/art;
}
# end of location /art

# rewrite /decor
location /decor/ {
	# 20170726 timeouts
	proxy_connect_timeout       900;
	proxy_send_timeout          900;
	proxy_read_timeout          900;
	send_timeout                900;
	proxy_pass http://localhost:8877/exist/decor/;
}
# end of location /decor

# rewrite /fhir
location /fhir/ {
	proxy_pass http://localhost:8877/exist/fhir/;
}
# end of location /fhir

# rewrite /xis
location /xis {
	proxy_pass http://localhost:8877/exist/xis;
}
# end of location /xis

# rewrite /systemtasks
location /systemtasks {
	proxy_pass http://localhost:8877/exist/systemtasks;
}
# end of location /systemtasks

# rewrite /temple
location /temple {
	proxy_pass http://localhost:8877/exist/apps/temple;
}
# end of location /temple

# terminology route
# rewrite /terminology
location /terminology {
	proxy_pass http://localhost:8877/exist/terminology;
}
# end of location /terminology
```

If you encounter difficulties in reaching the eXist-DB Monitor, please consider adding the following `location` statement to your nginx setting.

```nginx
location /exist/status {
	proxy_pass http://localhost:8877/exist/status;
}
```

### Setting up NGINX from scratch

If you need to set up NGINX config from scratch, please add the following config file `artdecor.conf` for ART-DECOR, typically in the `conf.d` subdirectory of your NGINX installation.

```nginx
server {
  # all requests to port 80 are redirected to SSL, see below
  listen 80;
  listen [::]:80;
  server_name _;
  root /path/to/your/html;
  index index.html;
  location / {
    return 301 https://$host$request_uri;
  }
}

server {
  # set up SSL conncetion with proper certificates
  listen 443 ssl http2;
  listen       [::]:443 ssl http2;
  server_name  _;
  root /path/to/your/html;
  index index.html;

  client_max_body_size 134217728;

  # SSL certificates
  ssl_certificate "/path/to/certificate/fullchain.pem";
  ssl_certificate_key "/path/to/certificate/privatekey.pem";
  ssl_session_cache shared:SSL:1m;
  ssl_session_timeout  10m;
  ssl_ciphers PROFILE=SYSTEM;
  ssl_prefer_server_ciphers on;

  # - - - add here all location statements from the above listing - - - 
  # AD3 routes
  # rewrite /exist/apps/api to proxy
  # location /exist/apps/api {.....}

  # add custom error pages (optional)
  error_page 404 /artdecor_404.html;
  location = /artdecor_404.html {
    root /path/to/your/html;
    internal;
  }
  error_page 500 502 503 504 /artdecor_50x.html;
  location = /artdecor_50x.html {
    root /path/to/your/html;
    internal;
  }

}
```

## Hint for running the temple app in non-nginx environments

If you are working locally e.g. a laptop and/or do not have an nginx with the above rules running, you need to tweak the template service call in the frontend to behave properly.

In your local frontend directory were you build your app using npm there is also a file called `.env`. Edit this file and adapt the line with `VUE_APP_EXIST_ORIGIN` to look like the one shown here.

```js
############ Other settings ############
# Origin of the eXist-db server.
# This is used for direct links to (for example) Temple.
VUE_APP_EXIST_ORIGIN=http://localhost:8877/exist/apps
```

## BOT spam and DoS protection

You might consider to add the following nginx rejection statements to prevent bots from spamming your server or even causing a denial of service (DoS).

```nginx
if ($http_user_agent ~* (2ip\ bot|360Spider|404checker|404enemy|80legs|Abonti|Aboundex|Aboundexbot|Acunetix|ADmantX|adscanner|AdsTxtCrawlerTP|AfD-Verbotsverfahren|AHC|AIBOT|AiHitBot|Aipbot|Alexibot|ALittle\ Client|Alligator|AllSubmitter|AlphaBot|Amazonbot|Anarchie|Anarchy|Anarchy99|Ankit|Anthill|anthropic-ai|Apache-HttpClient|Apexoo|archive.org_bot|arquivo-web-crawler|arquivo.pt|Aspiegel|ASPSeek|Asterias|Atomseobot|Attach|autoemailspider|Awario|AwarioBot|AwarioRssBot|AwarioSmartBot|BackDoorBot|Backlink-Ceck|backlink-check|BacklinkCrawler|BackStreet|BackWeb|Badass|Bandit|Barkrowler|BatchFTP|Battleztar\ Bazinga|BBBike|BDCbot|BDFetch|BetaBot|bidswitchbot|Bigfoot|Bitacle|Black\ Hole|Blackboard|BlackWidow|BLEXBot|Blow|BlowFish|Boardreader|Bolt|BotALot|Brandprotect|Brandwatch|Buck|Buddy|BuiltBotTough|BuiltWith|Bullseye|BunnySlippers|BuzzSumo|Bytedance|Bytespider|cah.io.community|Calculon|CATExplorador|CazoodleBot|CCBot|Cegbfeieh|CensysInspect|CF-UC|ChatGPT-User|check1.exe|CheeseBot|CherryPicker|CheTeam|ChinaClaw|Chlooe|Citoid|ClaudeBot|Claritybot|clark-crawler|Cliqzbot|Cloud\ mapping|coccocbot|Cocolyzebot|CODE87|Cogentbot|cognitiveseo|cohere-ai|Collector|com.plumanalytics|comscore|Copier|CopyRightCheck|Copyscape|Cosmos|Craftbot|crawl.sogou.com|crawler.feedback|crawler4j|Crawling\ at\ Home\ Project|CrazyWebCrawler|Crescent|CriteoBot|CrunchBot|CSHttp|Curious|Custo|CyotekWebCopy|DatabaseDriverMysqli|DataCha0s|dataforseo.com|DataForSeoBot|dataforseobot|DBLBot|demandbase-bot|Demon|Deusu|Devil|Digincore|DigitalPebble|DIIbot|Dirbuster|dirbuster|Disco|Discobot|Discoverybot|Dispatch|DittoSpyder|DnBCrawler-Analytics|DnyzBot|DomainAppender|DomainCrawler|Domains\ Project|DomainSigmaCrawler|domainsproject.org|DomainStatsBot|DomCopBot|Download\ Wonder|Dragonfly|Drip|DSearch|DTS\ Agent|EasyDL|Ebingbong|eCatch|ECCP/1.0|Ecxi|EirGrabber|EMail\ Siphon|EMail\ Wolf|EroCrawler|evc-batch|Evil|Exabot|Expanse|Express\ WebPictures|ExtLinksBot|Extractor|ExtractorPro|Extreme\ Picture\ Finder|EyeNetIE|Ezooms|FacebookBot|facebookscraper|FDM|FemtosearchBot|FHscan|Fimap|Firefox/7.0|FlashGet|Flunky|Foobot|Freeuploader|FriendlyCrawler|FrontPage|Fuzz|FyberSpider|Fyrebot|G-i-g-a-b-o-t|Gabanzabot|GalaxyBot|Genieo|GermCrawler|Getintent|GetRight|GetWeb|Gigabot|Go-Ahead-Got-It|Go-http-client|Go!Zilla|gopher|Gotit|GoZilla|GPTBot|Grabber|GrabNet|Grafula|GrapeFX|GrapeshotCrawler|GridBot|GT::WWW|Haansoft|HaosouSpider|harvest|Harvest|Havij|HEADMasterSEO|Heritrix|heritrix|Hloader|HMView|HonoluluBot|HTMLparser|HTTP::Lite|HTTrack|Humanlinks|HybridBot|Iblog|Id-search|IDBot|IDBTE4M|IlseBot|Image\ Fetch|Image\ Sucker|imagesift.com|ImagesiftBot|IndeedBot|Indy\ Library|InfoNaviRobot|InfoTekies|instabid|Intelliseek|InterGET|Internet\ Ninja|InternetSeer|internetVista\ monitor|IonCrawl|ips-agent|Iria|IRLbot|isitwp.com|Iskanie|IstellaBot|iubenda-radar|JamesBOT|Java|Jbrofuzz|JennyBot|JetCar|Jetty|JikeSpider|JOC\ Web\ Spider|Joomla|Jorgee|JustView|Jyxobot|Kenjin\ Spider|Keybot\ Translation-Search-Machine|Keyword\ Density|Kinza|Kozmosbot|Lanshanbot|Larbin|Leap|LeechFTP|LeechGet|LexiBot|Lftp|LibWeb|Libwhisker|LieBaoFast|Lightspeedsystems|Likse|Linkbot|linkdexbot|LinkextractorPro|LinkpadBot|LinkScan|LinksManager|LinkWalker|LinqiaMetadataDownloaderBot|LinqiaRSSBot|LinqiaScrapeBot|Lipperhey|Lipperhey\ Spider|Litemage_walker|Lmspider|LNSpiderguy|Ltx71|lwp-request|lwp-trivial|LWP::Simple|Mag-Net|Magnet|magpie-crawler|Mail.RU_Bot|Majestic-SEO|Majestic\ SEO|Majestic12|MarkMonitor|MarkWatch|Mass\ Downloader|Masscan|Mata\ Hari|MauiBot|Mb2345Browser|MBCrawler|MeanPath\ Bot|Meanpathbot|meanpathbot|Mediatoolkitbot|mediawords|MegaIndex.ru|Metauri|MFC_Tear_Sample|MicroMessenger|Microsoft\ Data\ Access|Microsoft\ URL\ Control|MIDown\ tool|MIIxpc|Minefield|Mister\ PiX|MJ12bot|MJ12Bot|Moblie\ Safari|Mojeek|Mojolicious|MolokaiBot|Morfeus\ Fucking\ Scanner|Mozlila|MQQBrowser|Mr.4x3|MSFrontPage|MSIECrawler|Msrabot|MTRobot|muhstik-scan)) {
	return 444;
}
if ($http_user_agent ~* (Musobot|Name\ Intelligence|Nameprotect|Navroad|NearSite|Needle|Nessus|Net\ Vampire|NetAnts|Netcraft|netEstate\ NE\ Crawler|NetLyzer|NetMechanic|NetSpider|Nettrack|Netvibes|NetZIP|newspaper|NextGenSearchBot|Nibbler|NICErsPRO|Niki-bot|Nikto|NimbleCrawler|Nimbostratus|Ninja|Nmap|NPbot|Nuclei|Nutch|Octopus|Offline\ Explorer|Offline\ Navigator|omgili|OnCrawl|Openfind|OpenLinkProfiler|OpenVAS|Openvas|OrangeBot|OrangeSpider|OutclicksBot|OutfoxBot|Page\ Analyzer|page\ scorer|PageAnalyzer|PageGrabber|PageScorer|PageThing.com|Pandalytics|pangolin|Panscient|Papa\ Foto|Pavuk|pcBrowser|PECL::HTTP|PeoplePal|Petalbot|PHPCrawl|Pi-Monster|Picscout|Picsearch|PictureFinder|Piepmatz|Pimonster|Pixray|PleaseCrawl|plumanalytics|Pockey|POE-Component-Client-HTTP|polaris\ version|probe-image-size|Probethenet|ProPowerBot|ProWebWalker|Proximic|Psbot|Pu_iN|Pump|PxBroker|PyCurl|python-requests|Python-urllib|QueryN\ Metasearch|Quick-Crawler|Rainbot|RainBot|RankActive|RankActiveLinkBot|RankFlex|RankingBot|RankingBot2|Rankivabot|RankurBot|Re-re|RealDownload|Reaper|RebelMouse|Recorder|RedesScrapy|ReGet|RepoMonkey|Ripper|ripz|RocketCrawler|RSSingBot|s1z.ru|SalesIntelligent|satoristudio.net|SBIder|scalaj-http|scan.lol|ScanAlert|Scanbot|ScoutJet|Scrapy|Screaming|ScreenerBot|ScrepyBot|Searchestate|SearchmetricsBot|Seekport|SeekportBot|SemanticJuice|SentiBot|SenutoBot|seobility|SeobilityBot|seocompany.store|SEOkicks|SEOkicks-Robot|SEOlyticsCrawler|SEOprofiler|seoscanners|SeoSiteCheckup|seostar|SEOstats|serpstatbot|sexsearcher|SeznamBot|Shodan|Siphon|SISTRIX|Site\ Sucker|Sitebeam|sitechecker.pro|SiteCheckerBotCrawler|SiteExplorer|Siteimprove|SiteLockSpider|siteripz|SiteSnagger|SiteSucker|Sitevigil|SlySearch|SmartDownload|SMTBot|Snake|Snapbot|Snoopy|SocialRankIOBot|Sociscraper|Sogou\ web\ spider|sogouspider|Sosospider|Sottopop|sp_auditbot|SpaceBison|Spammen|SpankBot|Spanner|Spbot|Spider_Bot|Spinn3r|spinner|SputnikBot|spyfu|Sqlmap|Sqlworm|Sqworm|Steeler|Stripper|Sucker|Sucuri|SuperBot|SuperHTTP|Surfbot|SurveyBot|Suzuran|Swiftbot|sysscan|Szukacz|T0PHackTeam|T8Abot|tAkeOut|Teleport|TeleportPro|Telesoft|Telesphoreo|Telesphorep|The\ Intraformant|TheNomad|thesis|Thumbor|TightTwatBot|Timpibot|tiny|TinyTestBot|Titan|Toata|Toweyabot|Tracemyfile|Trendiction|trendiction.com|trendiction.de|Trendictionbot|True_Robot|Turingos|Turnitin|TurnitinBot|TwengaBot|Twice|Typhoeus|ubermetrics-technologies.com|UnisterBot|Upflow|URLy.Warning|URLy\ Warning|V-BOT|Vacuum|Vagabondo|VB\ Project|VCI|VelenPublicWebCrawler|VeriCiteCrawler|VidibleScraper|Virusdie|VoidEYE|Voil|Voltron|voyagerx.com|Wallpapers|Wallpapers/3.0|WallpapersHD|WASALive-Bot|WBSearchBot|Web\ Auto|Web\ Collage|Web\ Enhancer|Web\ Fetch|Web\ Fuck|Web\ Pix|Web\ Sauger|Web\ Sucker|Webalta|WebAuto|WebBandit|WebCollage|WebCopier|WEBDAV|WebEnhancer|WebFetch|WebFuck|webgains-bot|WebGo\ IS|WebImageCollector|WebLeacher|WebmasterWorldForumBot|webmeup-crawler|WebPix|webpros.com|webprosbot|WebReaper|WebSauger|Webshag|Website\ Quester|WebsiteExtractor|WebsiteQuester|Webster|WebStripper|WebSucker|WebWhacker|WebZIP|WeSEE|Whack|Whacker|Whatweb|Who.is\ Bot|Widow|WinHTTrack|WiseGuys\ Robot|WISENutbot|Wonderbot|Woobot|Wotbox|wp_is_mobile|Wprecon|WPScan|WWW-Collector-E|WWW-Mechanize|WWW::Mechanize|WWWOFFLE|x09Mozilla|x22Mozilla|Xaldon_WebSpider|Xaldon\ WebSpider|Xenu|xpymep1.exe|YoudaoBot|Zade|Zauba|zauba.io|Zermelo|Zeus|zgrab|Zitebot|ZmEu|ZoomBot|ZoominfoBot|ZumBot|ZyBorg)) {
	return 444;
}
```

<konec/>
