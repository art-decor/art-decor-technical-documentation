---
permalink: /administration/setupmaintain/backend/
---
# Installer for eXist-db for ART-DECOR®

To perform the following instructions you need to have root ssh access to your server. Alternatively, you can use the sudo command accordingly.

::: note

The following instructions are valid for CentOS Linux. The database runs on other Linux systems, too. The commands need to be adapted accordingly, e. g. the package management system `yum` (*Yellowdog Updater, Modified*) is not existent on all Linux variants. 

:::

## Stop eXist-db and create a backup

If a previous version of eXist-db is running on your system, stop that eXist-db instance first and create a backup.

see our [Updating an ART-DECOR Release](/administration/releaseupdate/#initial-backup-of-the-full-installation) page.

Note: from that page you should only perform the following action:

- Initial Backup of the full installation

## Place the binary eXist-db installer

We adivice to place ART-DECOR related stuff in `/opt`. You may choose for another location but our instructions refer to that directory.

First create the `art-decor-linux` directory and subdiretcories if they do not exist.

```sh
mkdir -p /opt/art-decor-linux/tooling/packages
```

Change directory to `packages`.

```sh
cd /opt/art-decor-linux/tooling/packages
```

Download the eXist-db installer files into directory  `packages` from the repository.

```
https://repository.art-decor.org/installers/art-decor-exist-db-installer.tar.gz
```

You can use typical download file command line tools, for example (replace "..." by the URL above):

```
curl -O ...
wget ...
```

The original files are located in https://bitbucket.org/art-decor/art-decor-external-tools/src/master/art-decor-exist-db-installer.

You now should have a `README.md,` a `scripts` directory and a directory denoting the version of the eXist-db installer, e.g. `6.2.0`.

## Install script and settings file

Make sure the `scripts` directory contains the `install_exist.sh` script and a `settings` file.

```sh
cd /opt/art-decor-linux/tooling/scripts
```

 Make sure the script is executable, e.g. by issuing 

```sh
chmod +x install_exist.sh 
```

## Java openJDK

Update / install the JAVA openJDK version.

```sh
yum update java-11-openjdk
yum install java-11-openjdk
```

Something similar to this output should be shown.

```sh
> java --version
openjdk 11.0.16.1 2022-08-12 LTS
OpenJDK Runtime Environment (Red_Hat-11.0.16.1.1-1.el7_9) (build 11.0.16.1+1-LTS)
OpenJDK 64-Bit Server VM (Red_Hat-11.0.16.1.1-1.el7_9) (build 11.0.16.1+1-LTS, mixed mode, sharing)
```

```sh
> ls /usr/lib/jvm
java-11-openjdk-11.0.16.1.1-1.el7_9.x86_64  jre  jre-11  jre-11-openjdk  jre-11-openjdk-11.0.16.1.1-1.el7_9.x86_64  jre-openjdk
```

## Run the installer script

### Check settings in file settings

Edit the `settings` file residing in the `scripts` directory and modify it.

Example output above for the java lib was: `java-11-openjdk-11.0.16.1.1-1.el7_9.x86_64`, thus change the following line in the `settings` accordingly to your output.

```sh
java_location=/usr/lib/jvm/java-11-openjdk-11.0.16.1.1-1.el7_9.x86_64/
```

Check memory settings and place them in settings

```sh
exist_maxmem=8192
exist_cachemem=1024
```

This can be changed if your server is "bigger", e.g. for an 32 G memory you may want to switch to

- `exist_maxmem=10240` :: exist-db max mem = wrapper.java.maxmemory
- `exist_cachemem=1280` :: exist-db cache

Hints for further tuning in `/etc/conf.xml` of the eXist-db directory when eXist is installed

- 2560 : conf.xml: db-connection cacheSize = 1/3 of exist-db max mem
- 1280 : conf.xml: db-connection collectionCache = 1/6 of exist-db max mem

Also look at http://exist-db.org/exist/apps/doc/tuning.xml?field=all&id=D3.7 for the `JAVA_OPTS`.

Check whether the installer location is correct. It could be just the folder `packages` (as assumed above) or a folder with a version, e.g. `6.2.0`.

```sh
# location where tooling is located
installer_location=../packages
```

### Run the installer

Choose your db admin password (`{ENTERYOURPASSHERE}` is the placeholder here)

```sh
./install_exist.sh -p /usr/local/exist_atp --password {ENTERYOURPASSHERE}
```

### Clean the password from the history

```sh
history | grep password
history -d <number>
```

## Backup of raw install (without ART-DECOR)

Create a backup of an empty exist-db

```sh
mkdir -p /opt/art-decor-linux/tooling/backups
cd /opt/art-decor-linux/tooling/backups
```

If you have older eXist-db versions installed under /usr/local/ that you do not wish to backup, move them first.

Backup your fresh installation.

```sh
VERSIONSHORT=6.2.0
tar zcvf exist_without_artdecor_${VERSIONSHORT}_$(date '+%Y%m%d%H%M').tgz /usr/local/exist_atp_*
```

Please check whether the path to the installation (`/usr/local/exist_atp_*` in the example above) is correct.

## Check and start exist_db

Maybe you have to reload your systemctl daemon first

```sh
systemctl daemon-reload
```

The check and start eXist-db

```sh
systemctl status eXist-db.service
systemctl start eXist-db.service
```

And maybe check again whether the service is running.

```
systemctl status eXist-db.service
● eXist-db.service - eXist-db 6.2.0 Server
   Loaded: loaded (/etc/systemd/system/eXist-db.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2022-07-04 11:08:52 CEST; 4s ago
 Main PID: 1288412 (java)
    Tasks: 90 (limit: 822516)
   Memory: 469.7M
   CGroup: /system.slice/eXist-db.service
           └─1288412 /usr/bin/java -Xmx8g -Dorg.exist.db-connection.cacheSize=2700M -Xms128m -Dfile.encoding=UTF-8 -Dlog4j.configurationFile=/usr/local/exist_atp/etc/log4j2.xml -Dexist.home=/usr/local/exist_>

Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,828 [main] INFO  (JettyStart.java [run]:287) - -----------------------------------------------------
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,828 [main] INFO  (JettyStart.java [run]:288) - Server has started, listening on:
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,828 [main] INFO  (JettyStart.java [run]:290) - http://62.138.179.44:8877/
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,829 [main] INFO  (JettyStart.java [run]:290) - https://62.138.179.44:8477/
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,829 [main] INFO  (JettyStart.java [run]:293) - Configured contexts:
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,829 [main] INFO  (JettyStart.java [run]:299) - /exist (eXist XML Database)
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,831 [main] INFO  (JettyStart.java [run]:315) - /exist/iprange (IPrange filter)
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,831 [main] INFO  (JettyStart.java [run]:299) - / (eXist-db portal)
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,832 [main] INFO  (JettyStart.java [run]:315) - /iprange (IPrange filter)
Jul 04 11:08:55 art-decor-open-tools.net startup.sh[1288412]: 04 Jul 2022 11:08:55,832 [main] INFO  (JettyStart.java [run]:323) - -----------------------------------------------------

```

The service file is `/etc/systemd/system/eXist-db.service`.

## Further Installations

To continue with the further installation use the eXist Package Manager.

::: note

If you have a browser available on the system where the installation just took place, simply contact `localhost:8877`.

If the installation was done on a distant server, you probably need to etstablish a SSH tunnel, that tunnels a port on your local machine to the `localhost:8877` address and port on the distant server. An example

```
ssh -c {cipher} -f {user}@{distant-server-address} -L {local-port}:127.0.0.1:8877 -N
```

Please find more [information here](https://linuxize.com/post/how-to-setup-ssh-tunneling/).

You might also need to trim your firewall configuration to allow access.

:::

After logging in as the admin, you select the eXist Package Manager that shows up as follows.

<img src="../../../img/image-20221015202642337.png" alt="image-20221015202642337" style="zoom:67%;" />

::: note PREREQUISITES

This step depends on what repository branch you want to follow. There is a **stable** and a **development** path.

- To use the **stable** ART-DECOR Release 3 backend use
  `https://repository.art-decor.org/stable3`,
- to use the **development** ART-DECOR Release 3 backend use
  `https://repository.art-decor.org/development3`.

Unless you are interested in getting and using the most recent develeopment code we recommend to use the **stable** (master) branch.

:::

### Update eXist Apps

Update eXist Apps, e.g. exide etc. from the default repository, specified by default in the file `/db/apps/packageservice/configuration.xml`. For eXist this is

- `http://exist-db.org/exist/apps/public-repo`

...thus in the config file

```xml
<repository active="true" default="true">http://exist-db.org/exist/apps/public-repo</repository>
```

You **must install** from eXist:

- JWT - Create, read and verify JSON Web Tokens (JWT) in exist-db
- roaster - OpenAPI Router

Please check also if the 

- Shared Resources: jquery, dojo, ace, templating, utilities

... package is listed and install it, if it is missing.

### Install ART-DECOR backend packages

To install the ART-DECOR backend, change the repository in file `/db/apps/packageservice/configuration.xml` to any of the above mentioned paths, depending on stable or development.

| ART-DECOR Release 3<br/>backend branch | URL to use                                      |
| -------------------------------------- | ----------------------------------------------- |
| **Stable**                             | `https://repository.art-decor.org/stable3`      |
| **Development**                        | `https://repository.art-decor.org/development3` |

Change the repository element to what you desire, e.g. for the stable branch use

```xml
<repository active="true" default="true">https://repository.art-decor.org/stable3</repository>
```

...and comment out the eXist repository, so that there is only one active repository.

Then install the following packages from the repository in exactly this sequence:

1. DECOR core files
2. ADA - ART DECOR Applications
3. Advanced Requirement Tooling
4. ART-DECOR API
5. ART-DECOR System Services
6. DECOR example files

If you are going to use Terminology Packages install at least

- Terminology Applications

... and desired Terminology Data packages, their names start with *Terminology Data –*.

If problems occur at startup regarding a missing EXPath Cryptographic library, please upload / install

- eXist-db EXPath Cryptographic library that can best be optained [here](https://github.com/eXist-db/expath-crypto-module/releases).

Optional are (for later):

- DECOR services 
- FHIR 4.0.1 (R4) Server *(takes a while)*
- OID Registry - HL7 International *(takes a while)*
- Temple (ART-DECOR XML Editor)

## Setting up the eXist scheduler

There are a couple of scheduled jobs that are required and some that you might consider to add to your installation. To this topic we dedicated a [seperate documentation](/administration/setupmaintain/scheduler/) that we strongly recommend to follow, install the required scheduled jobs and check whether you need the recommended ones.

## Initial Backup of the full installation

Stop the service

```sh
systemctl stop eXist-db.service
```

...and then backup the new full installation with the ART-DECOR packages installed

```sh
cd /opt/art-decor-linux/tooling/backups
VERSIONSHORT=6.0.1
tar zcvf exist_with_artdecor_${VERSIONSHORT}_$(date '+%Y%m%d%H%M').tgz /usr/local/exist_atp_*
```

Start the system again and check whether it is running properly

```sh
systemctl start eXist-db.service
...
systemctl status eXist-db.service
```

This concludes the database installation.

## Troubleshooting 

Some pointers for troubleshooting the eXist-db installation.

```sh
# check out the status of the eXist-db process
sudo ps aux |grep -i exist
# check out the status of the port that eXist-db is running on
sudo netstat -anp |grep :8877
```

Besides the systemctl logging above, there are also specific eXist-db logging files.

```sh
sudo updatedb
sudo locate exist.log
sudo tail -f /usr/local/exist_atp_${VERSIONSHORT}/logs/*.log
# or just tail: exist.log
```

## Useful tools while troubleshooting

### monex
Monex provides monitoring for eXist-db.  
Please find more [information here on exist-db.org](https://exist-db.org/exist/apps/doc/indexing.xml).  
Please find more [information here on github.com](https://github.com/eXist-db/monex).  

Monex can be used to:  
- look at index usage  
- find longer running actions. See actions that have been done in the past  
- stop actions  
- report server status and information

### xst
xst is a modern command line interface for eXist-db.  
Please find more [information here on github.com](https://github.com/eXist-db/xst)  

Quick installation:  
```
sudo su -l root
npm install --global @existdb/xst
```

Usage:  
Use a configuration file like: [.xstrc here](https://github.com/eXist-db/xst/blob/main/spec/fixtures/.xstrc)  

Usage examples:  
```
xst --help  
xst --config .xstrc2 upload --help
xst --config .xstrc2 upload demo1-decor.xml /db/apps/decor/data/examples/demo1/
xst --config .xstrc2 edit --editor nano /db/apps/packageservice/configuration.xml  
xst info  (report server status and information)  
xst ls /db --recursive --glob "*.css" |grep xml  
xst ls /db --long --sizesort --no-color  
xst ls /-tree --depth 2 --long  
```  

Please read the full documention to see more examples for running xqueries and other useful pointers.

<konec/>
