---
permalink: /administration/scheduler
---
# Scheduler

## Basics

ART-DECOR uses the eXist database functionalities for scheduling tasks. Information about the scheduler module can be found [here](https://exist-db.org/exist/apps/doc/scheduler). It uses the [Quartz Cron Trigger](http://www.quartz-scheduler.org/documentation/quartz-2.3.0/tutorials/crontrigger.html).

## Job Classes

We are using **User** Jobs. User jobs are a general class of job, authored by a user of the system. They can be scheduled for execution either periodically or as a one-off. A user job can also be configured to execute concurrently with another instance of itself, should schedules overlap due to execution time, or be mutually exclusive. As we have two special types of users for the ART-DECOR Release 3 API, api-user and api-admin, we have two classes to User Jobs:

- periodical jobs, i. e. tasks that repeatedly are executed with a period of time in between the executions 
- scheduled jobs that are executed upon request, once per request.

ART-DECOR uses / proposes to use several scheduled jobs for

- Processing Queues
- Refreshers
- Notifications
- Backup, Export and Consistency checks.

For futher information please refer to the corresponding [scheduler setup](/administration/setupmaintain/scheduler/).

For ART-DECOR maintained servers there is a backup strategy described in our internal Knowledge Base documents.
