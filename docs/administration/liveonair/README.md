---
permalink: /administration/liveonair
---
# Live on Air

## Uptimes

Up- and downtimes of our servers are recorded. On this page below you can see the actual status of our server park members.

## Urgent News Anouncements

ART-DECOR may show Urgent News banners on main screens and panels where information, warnings and announcements are shown. If you want to read more details about Urgent News Anouncements, please refer to our [documentation](/documentation/principles/#urgent-news).

##  MAIN/NEW 

<adimg server/><live/>

Current Newsbanner 

<NewsBanner server="HIGH"/>

The system is [![Better Uptime Badge](https://betteruptime.com/status-badges/v1/monitor/iq2d.svg)](https://betteruptime.com/?utm_source=status_badge)

## DEVELOP

<adimg server/><live/>

Current Newsbanner

<NewsBanner server="DEVELOP"/>

The system is [![Better Uptime Badge](https://betteruptime.com/status-badges/v1/monitor/dcyb.svg)](https://betteruptime.com/?utm_source=status_badge)

## NEWBIE

<adimg server/><live/>

Current Newsbanner 

<NewsBanner server="NEWBIE"/>

The system is [![Better Uptime Badge](https://betteruptime.com/status-badges/v1/monitor/gbgz.svg)](https://betteruptime.com/?utm_source=status_badge)

## ACCEPTANCE

<adimg server/><live/>

Current Newsbanner 

<NewsBanner server="ACCEPTANCE"/>

The system is [![Better Uptime Badge](https://betteruptime.com/status-badges/v1/monitor/i971.svg)](https://betteruptime.com/?utm_source=status_badge)

## AUX terminology / repository

<adimg server/><live/>

Current Newsbanner 

<NewsBanner server="AUX"/>

The system is [![Better Uptime Badge](https://betteruptime.com/status-badges/v1/monitor/dcyb.svg)](https://betteruptime.com/?utm_source=status_badge)

## art-decor-open-tools.net
<adimg server/><live/>

The system is [![Better Uptime Badge](https://betteruptime.com/status-badges/v1/monitor/dsau.svg)](https://betteruptime.com/?utm_source=status_badge)

