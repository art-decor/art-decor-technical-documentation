---

---


A complete description is part of the internal documentation, repeated here.



##### ART-DECOR® – Release 3.6

##### Release notes 2 December 2022

## 



## Hoovering History

### Rationale

When a lot of work is done by the project authors, artifact history can be very lengthy. On servers where this is the case, *hoovering* the history files is appropriate. Hoovering essentially means to gzip so far unhoovered artifact files and replace the history items by a skeleton that looks like the following example.

```
 <history 
   date="2022-11-07T00:39:51.903+02:00"
   authorid="kai" author="Kai Heitmann"
   id="86f43b6f-8986-4a9a-ad0b-43a95fad3090"
   intention="version"
   artifactId="2.16.840.1.113883.2.6.60.3.4.13"
   artifactEffectiveDate="2019-12-05T08:17:05"
   artifactStatusCode="draft"
   hoovered="true"
   bag="cd6dcacb-76c2-43cf-adca-cb76c253cf8d"
 />
```

The artifact body in the history item is omitted, instead two extra attributes are presented:

- hoovered – always true, to indicate that the history item is hoovered (in the hoover bag, compressed)
- bag – the id of the hoover bag in subfolder hooverbag, the gzipped file is named bag id + suffix .xml.gz

### XQuery

The action is perfomed by an xquery in db system tasks called hoover.xquery. It has the following parameters.

- project – a project prefix parameter; if not specified, all projects in the database are processed
- action – the action to be performed, one of the following
  - list_history – list statistics about history of project or all projects
  - hoover_history – hoover (clean up) history of project or all projects
- secret – a secret, actually only something to prevent use by accident
- username and password as login credentials, to allow the actions to be performed

If no action is specified, nothing happens and a succesful result is returned. Similar messages are returned on invalid credentials.

### Results

Hoovering large histories brings down re-index time dramatically by approximatedly factor 10 and improves the overall database performance.

As an example: a large set approximately 19.400 history items uses 91s of re-index time of the history folder. The hoover time is ca. 33 minutes, a re-index *after* hoovering takes only 3s, as gzip files aren't indexed at all.

### Set up and config

The external hoover service has two parts:

- the hoover.xquery in /db/systemtasks/modules
- the external scripts residing in folder art-decor-hoover-history on the server's file system containing art-decor-hoover-history.php, config.php and the log file subfolder logs.

The config file needs to be adapted to the local needs.

```
 <?php
 /*
 ADHOO art-decor hoover history / releases
        config file
 */
 
 // the URL to the hoover xquery in db systemtasks
 $url = "http://localhost:..../exist/apps/systemtasks/modules/hoover.xquery" ;
 
 /*
        list of projects that are in scope of hoovers, others on server 
        are not in scope and will not be processed.
        array of strings
        each of the projects will have a log file in the logs folder
        when processed
*/
$PROJECTS_IN_HOOVERSCOPE = array (
        'demo5-',
        'demo3-',
        'aktin-'
);
 
 // exist parameters and secret data for the automatic hoover
 $data = array (
        // exist parameters
        // the user name of the sigbot user in exist-db, 
        // having the dba right and thus the license to hoover
        'user' => 'xxxxxxx',
        // the secret password of that user
        'password' => 'xxxxxxx',
        // the hoover system password, fixed
        'secret' => '61fgs756.s9'
 );
 
 ?>
```

Note that the secret is called secret but actually only shall prevent calling it by accident without parameters.

After setting up the config file the script art-decor-hoover-history.php can be tested and on success be added to a cron tab for regular hoovering.

# Future plans

In the future we will offer diff functionality (see https://art-decor.atlassian.net/browse/AD30-96). To offer enhanced diffs for historic items also gzipped content might be needed to be considered part of a diff (thus unzip, get and diff).

Background

Ticket [AD30-348: artifact History HandlingDONE](https://art-decor.atlassian.net/browse/AD30-348) describes the creation of a “hoover” mechanism to properly handle artifact history. Fact is, that some projects have an enormous amount of detailed history files. The “hoover” activity allows to zip parts of the history and just leaving a skeleton in the index to be displayed in the History Panel.

The mechanism described in the ticket above is due to be part of the API and being called by the normal **ART-DECOR Release 3 Scheduler**.

## Solution

For that purpose the following steps have been undertaken: 

1. The **backend logic** baked of the formerly created extra xquery under system tasks, called `hoover.xquery` is moved into the corresponding brand-new API library module `scheduled-hoover-sth.xql`.  This file represents now the whole *hoover something* logic.
2. The extra xquery file under system tasks, called `hoover.xquery` is deleted.
3. The access rights are being adapted in `permissions-lib.xqm` to be processed properly in the post install phase of the API.
4. The runtime of the script (time-out limit) is 60 minutes as of now.
5. For a proper display in the **frontend** of which items are already hoovered, the history-lib.xqm in the API adds to return also the `@hoovered`attribute.  The frontend has been adapted to show hoovered history items (with a sandglass):

### Artifact History Hoovering

This eXist scheduled job is <optional/> to be configured and should be considered when large and active projects has a big set of history items.

::: note WHAT IT DOES

The history of versionable artifacts of a project is stored completely in the exist db, comprised of a reference wrapper (used for display in the front-end) and the changed content itself as “body”.

Some projects have an enormous amount of detailed history files that all reside in the database, fully indexed. That is not a desirable situation.

The API library has a “hoover” mechanism implemented. The “hoover” activity allows to zip parts of the history and just leaving a skeleton in the index to be displayed in the History Panel.

:::

The scheduled job has three parameters

- **threshold**: total size of a project history in MB when hoover shall take place; the value shall be between 10 and 500 MB,
- **project**: the project(s), expressed as prefixes, for which the hoovering shall take place, either `"*ALL*"` which processes all folders in the history folder (see $setlib:strDecorHistory), or it is a single project (ending with “`-`”), or a list of projects such as `"demo5- demo3- prsb03-"`, all separated by blanks; empty is not allowed,
- **action**: the actual action to be taken, either list-history or hoover-history.

An example in `etc/conf.xml`

```xml
<!--    
    Run “hoover” mechanism
    This will tidy-up the project history folders and zip older entries
-->
<job type="user" name="scheduled-hoover-sth" xquery="/db/apps/api/modules/library/scheduled-hoover-sth.xql"
  cron-trigger="0 0 6 2 * ?" unschedule-on-exception="true">
  <parameter name="threshold" value="50"/>
  <parameter name="project" value="demo5- demo3- prsb03-"/>
  <parameter name="action" value="hoover-history"/>
</job>
```

The typical cron trigger is `cron-trigger="0 0 6 2 * ?` which means *every 2nd day of a month at 6:00 in the morning.*

::: note

Hoovering – as in real live :-) – can be a **strenuous and time-consuming action**. Don’t do this during normal operation hours or running at a time where it interferes with backup procedures, for example.

:::
