---
permalink: /administration/tuning
---

# Tuning your eXist database

## Memory setting

Optimzing the memory settings for your eXist database can enhance performance and query response speed enormously. Here are some hints to find the right settings and where to set. This chapter is mainly focusing on Linux environments, but it may be adopted to other Operating Systems accordingly.

### Determine your available memory

On LINUX servers you can detemine the amount to memory with this command

```sh
cat /proc/meminfo

MemTotal:    32664636 kB
MemFree:     312888 kB
MemAvailable:  12868008 kB
```

In the example above, the server has around 32 GB of memory in total. Depending on the typical available memory you might consider following the following rules of thumb.

### eXist and the Java VM

The eXist database is a Java application. Java (Java Virtual Machine, JVM) always limits the maximum amount of memory available to a process. So eXist-db will not automatically use all of the available memory on your machine.

There are two important parameters for the JVM:

- **Xmx** = maximum memory allocation pool for a Java Virtual Machine (JVM)
- **Xms** = initial memory allocation pool

On the other hand there are a few memory settings in eXist db settings, typically residing in `conf.xml` in the `etc/` directory. In short:

**cacheSize**

The maximum amount of memory to use for database page buffers. 

Each database file has an associated page buffer for B+-tree an data pages. However, the memory specified via cacheSize is shared between all page buffers. It represents an absolute maximum, which would be occupied if all page buffers were completely full.

The cacheSize should typically not be more than half of the size of the JVM heap size (set by the JVM -Xmx parameter). It can be larger if you have a large-memory JVM (usually a 64bit JVM).

**checkMaxCacheSize**

Specifies whether eXist should check the max cache size on startup and reduce it if it is too large. This value should be set to true unless you know what you are doing! Setting this value to false may cause memory issues which may lead to database corruptions, since it disables the automated max cache size checks! You have been warned! ;-)

**collectionCache**

Maximum amount of memory (in megabytes) to use for collection caches.

Memory calculation is just approximate. If your collections are very different in size, it might be possible that the actual amount of memory used exceeds the specified limit. You should thus be careful with this setting.

As mentioned these parameters are set in the `conf.xml` file in the `etc/` directory.

For example

```xml
<db-connection cacheSize="1024M" collectionCache="256M" ...
```

### A fine Default Setting

So to summarize, a reasonable setting for the Java JVM memory to start with would be

```sh
-Xmx8192m -Xms512m
```

For *Unix/Linux* systems, this is done in the `.sh` files, for instance `$EXIST_HOME/bin/startup.sh`, from the`JAVA_OPTS` evironment variable. You can override those variables globally in your own shell.

You might also consider putting the `JAVA_OPTS` environment variable in `/etc/systemd/system/eXist-db.service` if you have a Linux service enabled.

```sh
[Unit]
Description=eXist-db 6.2.0 Server
Documentation=
After=syslog.target

[Service]
Type=simple
User=existdb
Group=existdb
Environment='JAVA_OPTS=-Xmx=8192m -Xms=1024m'
ExecStart=/usr/local/exist_atp/bin/startup.sh

[Install]
WantedBy=multi-user.target
```

This default setting would suggest to set the `conf.xml` settings for `db-connection` settings to

```xml
<db-connection cacheSize="3280M" collectionCache="1640M" ...
```

The db-connection `cacheSize` is typically around 1/3 of the maxmimum available JVM memory, and the `collectionCache` is around 1/6 of the maxmimum available JVM memory.

### A Table full of Hints

The following table gives hints on memory and parameter settings depending on the maximum memory allocation pool for a Java Virtual Machine (JVM) you can afford.

| Max Memory Java VM<br />parameter -Xmx | Initial  Memory Allocation JVM<br />parameter -Xms | db-connection<br />cacheSize<br />= 1/3 max memory | db-connection<br />collectionCache<br />= 1/6 max memory | cacheSize<br />= 1/8 max memory |
| -------------------------------------: | -------------------------------------------------: | -------------------------------------------------: | -------------------------------------------------------: | ------------------------------: |
|                                   8192 |                                               1024 |                                               3280 |                                                     1640 |                      Maarten??? |
|                                  12288 |                                               1024 |                                               4920 |                                                     2460 |                      Maarten??? |
|                                  16384 |                                               1024 |                                               6560 |                                                     3280 |                      Maarten??? |
|                                  20480 |                                               1024 |                                               8200 |                                                     4100 |                      Maarten??? |
|                                  24576 |                                               1024 |                                               9840 |                                                     4920 |                      Maarten??? |
|                                  28672 |                                               1024 |                                              11480 |                                                     5740 |                      Maarten??? |
|                                  32768 |                                               1024 |                                              13120 |                                                     6560 |                      Maarten??? |

Legenda: *Xmx* = maximum memory allocation pool for a Java Virtual Machine (JVM), *Xms* = initial memory allocation pool

The *cacheSize* should not be more than half of the size of the JVM heap size (set by the JVM `-Xmx` parameter). If the JVM heap is less than 512 megabyte, the cacheSize should even be smaller, e.g. 1/3.

## Setting for the database connection pool

Settings for the database connection pool:

**min**: minimum number of connections to keep alive.

**max**: maximum number of connections allowed.

**sync-period**: defines how often the database will flush its internal buffers to disk. The sync thread will interrupt normal database operation after the specified number of milliseconds and write all dirty pages to disk.

**wait-before-shutdown**: defines how long the database instance will wait for running operations to complete before it forces a shutdown. Forcing a shutdown may leave the db in an unclean state and may trigger a recovery run on restart.

Setting wait-before-shutdown="-1" means that the server will wait for all threads to return, no matter how long it takes. No thread will be killed.    

This is the section in the `conf.xml` file in the `etc/` directory to paramterize the database connection pool.

```xml
<pool min="1" max="20" sync-period="120000" wait-before-shutdown="120000"/>
```

## Lucene and Range Index

If you are using the lucene index or the new range index, you should consider increasing the `@buffer` setting in the corresponding index module configurations in `conf.xml` as well. `@buffer` defines the amount of memory (in megabytes) Lucene or Range Index will use for buffering index entries before they are written to disk. The default is 32.

```xml
<module id="lucene-index" buffer="256" class="org.exist.indexing.lucene.LuceneIndex" />
<module id="range-index" buffer="256" class="org.exist.indexing.range.RangeIndex"/> 
```











Maarten's hints:

24 of 32 GB calculator

8192 tomcat

10240 exist-db max met = wrapper.java.maxmemory

1280 exist-db cache = max mem / 8

2560 conf.xml db-connection cache size = 1/3 van existed max mem

1280 conf.xml db-connection collectionCache = 1/6 van existdb max men





## Some background info

Wolfgang Meier from eXist Solutions GmbH summarized it this way:

> Giving more memory to eXist will not necessarily improve response times. "Bad" queries may consume lots of RAM, but the better your queries are optimized, the less RAM they need: most of the heavy processing will be done using index lookups and the optimizer will try to reduce the size of the node sets to be passed around. Caching memory thus has to be large enough to hold the most relevant index pages. If this is already the case, increasing the caching space will not improve performance anymore. On the other hand, a too small cacheSize of collectionCache will result in a recognizable bottleneck. For example, a batch upload of resources or creating a backup can take several hours (instead of e.g. minutes) if @collectionCache is too small.

> If most of your queries are optimized to use indexes, 8 GB RAM for eXist does usually give you enough room to handle the occasional high load. Ideally you could run some load tests to see what the maximum memory use actually is. For @cacheSize, I rarely have to go beyond 512m. The setting for @collectionCache depends on the number of collections and documents in the database. If you have tens or hundreds of thousands of collections, you may have to increase it up to 768M or more. As I said above, you will recognize a sudden breakdown in performance during uploads or backups if the collectionCache becomes too small.