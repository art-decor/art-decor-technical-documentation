---
permalink: /administration/migration
---
# Migration Support

A project so far published under ART-DECOR Release 2 only and that wants to undergo a migration to hybrid/duplex or single ART-DECOR Release 3 environments can follow the path of our so-called **Smooth Migration**.

## Smooth Migration

Smooth Migration allows a gentle transition for all users to a hybrid/duplex or single ART-DECOR Release 3 environments.

### Project File “inmigration”

For additional assistance to users, a new optional project file can be introduced in any arbitrary project folder. The migration file shall be named `{project-prefix}-inmigration.xml` residing in the project folder, e.g. `demo5-inmigration.xml`.

The file contains information regarding the migration:

- an indication whether there is a **new hybrid / duplex environment** with AD2 and AD3 both using the same database or a **single AD3 environment** with no AD2 option for entry,
- a URL to offer a redirect to the new environment, AD2 and/or AD3 and 
- a corresponding label  AD2 and/or AD3.

The structure of the project file called `{project-prefix}-inmigration.xml` is as follows

```xml
<inmigration type="duplex|single3" redirect2="..." label2="..." redirect3="..." label3="..." />
```

Where

- @type is an indication whether there is a **new hybrid / duplex environment** with AD2 and AD3 both using the same database (type=duplex) or a **single AD3 environment** with no AD2 option for entry (type=single3).
- a conditional URL in `redirect2` with an optional label to be displayed for the URL in `label2` to offer a redirect link to the new environment for ART-DECOR Release 2, `redirect2` required and displayed only when type=duplex
- a mandatory URL `redirect3` with an optional label to be displayed for the URL in `label3` to offer a redirect to the new environment for ART-DECOR Release 3
- `label2` / `label3` default to the URL in `redirect2` / `redirect3`.

This is an example in-migration file

```xml
<inmigration
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="http://art-decor.org/ADAR/rv/DECOR-inmigration.xsd"
  type="duplex"
  redirect2="https://new.art-decor.org/art-decor/decor-project--projectprefix-"
  label2="The Project (Release 2)"
  redirect3="https://new.art-decor.org/ad/#/projectprefix-/project/overview"
  label3="The Project (Release 3)"
/>
```

### Old Project read-only

In order to disallow editing in the original AD2 project, all users in the original AD2 project are disabled for working with the project in edit mode.

### Message

Wherever a user is approaching the original AD2 project, a *Smooth Migration Message* appears on the top (a la Urgent News, but just for this project) that says:

When a user enters the old project, in case of type `duplex`

::: tip MESSAGE

This project is undergoing a migration to the new ART-DECOR Release 3 environment. Also the ART-DECOR Release 2 (“classic”) environment has moved. You are encouraged to follow one of the two links below, to continue your work. Preferably use the ART-DECOR Release 3 Entrance link, use your known credentials and directly work on the project with a better user experience and performance.

ART-DECOR Release 2 Classic Entrance go to {label2} {redirect2}

ART-DECOR Release 3 Entrance {label3} {redirect3}

:::

In case of single3

::: tip MESSAGE

This project is undergoing a migration to the new ART-DECOR Release 3 environment. The project has moved, at the location here the project is now read-only. Please follow the link below, use your new credentials and directly work on the project at the new location with a better user experience and performance.

ART-DECOR Release 3 Entrance {label3} {redirect3}

:::

The following example shows such a message on the old original AD2 project, appearing there and inviting users to directly go to the new environment.

<img src="../../img/image-20220926142550857.png" alt="image-20220926142550857" style="zoom:67%;" />
