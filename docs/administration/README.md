---
permalink: /administration
---
# System Administration

## Overview

The System Administration Panel shows statistics about the server and allows access to functions that affects the server.

![image-20230117152935707](../img/image-20230117152935707.png)

## Projects

The Project panel allows to add new projects to the server. The panel shows the list of existing projects, along with its ID, prefix, the date/time when the project was last modified, the time since last modified in days, whether the project is experimental or production and what the default language is.

![image-20231108165722012](../img/image-20231108165722012.png)

The panel offers a + button to add a new project and a search field to search for project name or prefix.

## DECOR locks

## Server Management

The Server Management Panel allows access to functions that in one way or another affects server behavior for all users.

### Update codesystem index

This function only pertains to ART-DECOR 2 [ClaML based terminologies](https://en.wikipedia.org/wiki/Classification_Markup_Language). It does not impact anything related to ART-DECOR >= 3 centralized terminology services.

**What does it do?**

It creates an index file of all available ClaML code systems on the server which is used in searching code systems in ART-DECOR 2. If a code system is not listed in this index file, it will not be included in search results.

The database resource it affects is `/db/apps/terminology/claml/classification-index.xml`

**When do I need this?**

After installing a new/updated ART-DECOR 2 code system like ICD-10 or ICPC or if you sense that not all expected results are returned when searching.

**What impact should I expect?**

Running this function should not take more than a few seconds.

### Fix ART permissions

This function seems to pertains to ART-DECOR Release 2, but it has some useful effects for ART-DECOR Release 3 and upper, too.

**What does it do?**

- Fix permissions for collection `/db/apps/art` and `art-data` so the right user (groups) can read/write/execute
- Synchronize the eXist-db users with `/db/apps/art-data/user-info/{users}.xml` to make sure all users are listed with their groups, full name, organization and email
- Fix permissions for collections `/db/apps/decor/data` (e. g. all projects), `/history`, `/releases`, `/scheduled-tasks`, and `/tmp` so the right user (groups) can read/write/execute

**When do I need this?**

Use of this function may help if a user experiences permission issues in creating/deleting runtime builds, or other areas using ART-DECOR. Due to the improved permission model of ART-DECOR Release 3, this should not be an issue anymore.

### Clean up DECOR

**What does it do?**

- Clean up `/db/apps/decor/tmp` so only the latest copy per project is left
- Clean up `/db/apps/decor/releases/*/development` so only the latest 3 compilations per project are left

**When do I need this?**

Use of this function may help if a user experiences permission issues in creating/deleting runtime builds, or other areas in the ART-DECOR 2 interface. Due to the improved permission model of ART-DECOR 3, this should not be an issue. However you may still want to periodically cleanup superfluous runtime versions and temporary files.

**What impact should I expect?**

How long this function takes really depends on the number of runtime environments and the number of temporary files to cleanup. It might be a multi minute process during which your server will appear frozen. 

::: warning

Preferably do not run this feature in office hours.

::: 

### Fix terminology permissions

This function pertains to ART-DECOR 2 terminology applications and authoring of ART-DECOR >= 3 CADTS terminologies.

**What does it do?**

- Fix permissions for collection `/db/apps/terminology` so the right user (groups) can read/write/execute
- Fix permissions for collection `/db/apps/terminology-data/codesystem-authoring-data` , `/valueset-authoring-data`, and `/conceptmap-authoring-data` so the right user (groups) can read/write/execute

**When do I need this?**

When users run into permission issues. Due to the improved permission model of ART-DECOR 3, this should not be an issue.

**What impact should I expect?**

Running this function should not take more than a few seconds.

### Fix FHIR permissions

This function only pertains to servers that have one or more FHIR server packages installed.

**What does it do?**

For all installed FHIR servers:

- Fix permissions for collection `/db/apps/fhir/*` so the right user (groups) can read/write/execute
- Fix permissions for collection `/db/apps/fhir-data` so the right user (groups) can read/write/execute
- Refresh the Conformance (DSTU2) and/or CapabilityStatement resources to reflect current installed state of affairs
- Remove all temporary files older than 7 days, e.g. Bundles of results that were stored for paging purposes.

**When do I need this?**

If you feel the FHIR temporary files collection (`/db/apps/fhir-data/*/_snapshot`) is rather full. If you feel certain terminologies are not reflected properly in the CapabilityStatement.

**What impact should I expect?**

How long this function takes really depends on the number of temporary files to cleanup. It might be a multi minute process during which your server will appear frozen.

::: warning

Preferably do not run this feature in office hours.

:::

### Refresh all OID Lookups

This function only pertains to servers that have one or more [OID](https://en.wikipedia.org/wiki/Object_identifier) registries installed. Searching OIDs during various lookups is not done on the OID registry resources directly but, for performance reasons, on lookup resources derived from them. 

**What does it do?**

For all installed OID registries:

- Recreate the lookup resource if one does not exist, or if it is older than the registry

**When do I need this?**

If the OID Registry tools package has been updated and has new capabilites in indexing/creating lookup resources. If you've updated a registry manually and you need the changes reflected in the lookup resource.

**What impact should I expect?**

How long this function takes really depends on the number and size of the OID registries. It usually takes about a minute during which your server should be operating normally.

### Refresh cache

This function pertains to servers that call on one or more building block repositories (BBRs) from another ART-DECOR server. Building block repositories are basically projects that have the marker `repository=true`. It retrieves these projects from those other servers and caches them on your server for performance reasons.

::: note

The cache is typically refreshed by ART-DECOR®'s scheduled *automatic caching mechanism*.

:::

**What does it do?**

Retrieves trusted BBRs as specified in /db/apps/art-data/server-info.xml. Retrieves BBRs as specified in projects on this server. Recursively retrieves any BBRs that BBRs depend on.

**When do I need this?**

If your server tells you the cache is *withered*. See [Cache freshness](/documentation/landing/#server-summary).

**What impact should I expect?**

How long this function takes really depends on the number of BBRs to retrieve and bandwidth of the hosting server. It usually takes a few minutes during which your server should be operating normally.

### Realtime Updates

This function initiates realtime updates for all ART-DECOR® clients on the server manually, e.g. the *server information*. This could be useful after the admin installed a new terminology package, for example.

::: note

Clients on the server are typically refreshed by ART-DECOR®'s *http-proxy*.

:::

## Users

The User Panel allows to create and maintain ART-DECOR users, i. e.  eXist-db users for the database plus additional ART-DECOR related information.

<img src="../img/image-20230119142252977.png" alt="image-20230119142252977" style="zoom:67%;" />

The list shows the display name of the user and his database user id, the status of the account and the groups, the user belongs to. Clicking on the row expand button shows more details about the user and gives the following options.

### Edit the user information

You can edit most of the settings for the user, especially his display name, organization, description, email and group assignments. The user id in the database cannot be changed.

<img src="../img/image-20230119142817112.png" alt="image-20230119142817112" style="zoom:67%;" />

### Re-assigning new password

You can re-assign a new password directly and manually. You need to inform the user about his new password.

<img src="../img/image-20230119143041299.png" alt="image-20230119143041299" style="zoom:67%;" />

### Automatic user password reset

You can set a new password for the user. ART-DECOR automatically notifies the user about the new password.

<img src="../img/image-20230119143131602.png" alt="image-20230119143131602" style="zoom:80%;" />

## ADAWIB

(follows)

## News

(follows)

## ART-Settings

(follows)

## Governance Groups

(follows)

## OID-Registry

(follows)
