---
permalink: /administration/frontendfeatures/
---

# Frontend Features

## Disable frontend functionality

As administrator of an ART-DECOR environment (server) you might want to be able to switch the functionality (read/write) on and off for datasets, scenarios etc. so that it can be switched on / off during a migration, for example.

For that purpose you might want to edit the database file that resides in `/db/apps/art-data` called `server-functions.xml`. It allows you to specify the behavior of the frontend and comprises by default of the following XML structure and data.

```xml
<server-functions>
  <!-- CRUD for DECOR. If something is false, then you cannot override with a child -->
    <decor read="true" write="true">
        <project read="true" write="true">
            <overview read="true" write="true" />
            <authors read="true" write="true" />
            <publications read="true" write="true" />
            <implementation-guides read="true" write="true" />
            <history read="true" write="true" />
            <identifiers read="true" write="true" />
            <locks read="true" write="true" />
            <my-community read="true" write="true" />
            <governance-groups read="true" write="true" />
            <ada read="true" write="true" />
            <development read="true" write="true" />
            <project-index read="true" />
        </project>

        <datasets read="true" write="true">
            <dataset read="true" write="true"/>

            <concept write="true" />
        </datasets>

        <scenarios read="true" write="true">
            <scenarios read="true" write="true"/>
            <actors read="true" write="true"/>
            <questionnaires read="true" write="true"/>

            <transaction write="true">
                <representingTemplate write="true"/>
            </transaction>
        </scenarios>


        <terminology read="true" write="true">
            <valuesets read="true" write="true"/>
            <codesystems read="true" write="true"/>
            <mappings read="true" write="true"/>
            <identifiers read="true" write="true"/>
            <browser read="true" write="true"/>
            <concept-maps read="true" write="true"/>
        </terminology>

        <rules read="true" write="true">
            <templates read="true" write="true"/>
            <profiles read="true" write="true"/>
            <associations read="true" write="true"/>
            <identifiers read="true" write="true"/>
        </rules>

        <issues read="true" write="true">
            <issues read="true" write="true"/>
            <labels read="true" write="true"/>
        </issues>
    </decor>

    <design read="true" />
</server-functions>
```

::: note
If a value for any of the attributes is missing, "false" is assumed.

If an element is not present at all, "true" is assumed for both @read and @write.
:::

As an example, if you set up an ART-DECOR server which only supports codesystem/terminology browsing, in which there are no set projects and only borrowing rights are required, this would be enough:

```xml
<server-functions>
    <decor read="true" write="true">
        <project read="false"/>
        <datasets read="false"/>
        <scenarios read="false"/>

        <terminology read="true">
            <valuesets read="false"/>
            <codesystems read="false"/>
            <mappings read="false"/>
            <identifiers read="false"/>
            <browser read="true"/>
        </terminology>

        <rules read="false"/>
        <issues read="false"/>
    </decor>

    <design read="false"/>
</server-functions>
```

The minimal content of the file should always be the following structure. It results in a fully available frontend.

```xml
<server-functions>
    <decor read="true" write="true"/>
</server-functions>
```
