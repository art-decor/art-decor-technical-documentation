---
permalink: /administration/dochelper
---
# Documentation Helpers

## Tip

```markdown
::: tip
This is a tip
:::
```

::: tip
This is a tip
:::

## Note

```markdown
::: note
This is a note
:::
```

::: note
This is a note
:::

## Warning

```markdown
::: warning
This is a warning
:::
```

::: warning
This is a warning
:::

## Danger

```markdown
::: danger
This is a dangerous warning
:::
```

::: danger
This is a dangerous warning
:::

## Details

```markdown
::: details
This is a details block, which does not work in IE / Edge
:::
```

::: details
This is a details block, which does not work in IE / Edge
:::

## Definition

```markdown
::: definition
This contains a definition of something
:::
```

::: definition
This contains a definition of something
:::

## Example

```markdown
::: example
An example, also available as examples
:::
```

::: example
An example, also available as examples
:::

## Constraint

```markdown
::: constraint
This contains a constraint definition
:::
```

::: constraint
This contains a constraint definition
:::

## Wise Owl

```markdown
::: owl
A wise text, customizable and inspired by wisdom
:::
```

::: owl
A wise text, customizable and inspired by wisdom
:::

## Under Construction

```markdown
::: underconstruction
This page is under construction, this says
:::
```

::: underconstruction
This page is under construction, this says
:::

## Badges and Dots

### Abandoned, New, Planned

This feature is no longer supported <abandoned/>

This feature is no longer new <new/>

This feature is no longer planned <planned/>

This information is live data <live/>

```xml
<abandoned/>
<new/>
<planned/>
<live/>
```

### Flags

Only valid in Germany <flag country="DE"/> or in the united Kingdom <flag country="GB"/> or USA <flag country="US"/> only

```xml
<flag country="DE"/>
```

### Errror, Warnings and Info Dots in Reports

Report gives <error/> errors, <warning/> warnings and <info/> infos.

```xml
<error/>
<warning/>
<info/>
```

### Required and Recommended

Flags things as <required/> or <recommended/>.

```xml
<required/>
<recommended/>
```

### Reference Dots

Pictures and graphics may contain numbers like <ref1/> or <ref2/> that can be referenced in the text.

```xml
<ref1/>
<ref2/>
```

