---
permalink: /administration/releaseupdate/
---

# Updating an ART-DECOR® Release

ART-DECOR publishes new releases every once in a while, see our [ART-DECOR® Releases](/documentation/releasenotes/) page. Here are the instructions to update from an existing installation to a new release. The update always contains two steps: 

- the backend update and 
- the frontend update.

## Updating the backend

### Initial Backup of the full installation

First make a backup of the full installation.

Stop the service

```sh
systemctl stop eXist-db.service
```

...and then backup the new full installation with the ART-DECOR packages installed

```sh
cd /opt/art-decor-linux/tooling/backups
VERSIONSHORT=6.0.1
tar zcvf exist_with_artdecor_${VERSIONSHORT}_$(date '+%Y%m%d%H%M').tgz /usr/local/exist_atp_*
```

Start the system again and check whether it is running properly

```sh
systemctl start eXist-db.service
...
systemctl status eXist-db.service
```

### eXist and ART-DECOR Package Updates

For the installation use the eXist Package Manager as [described here](/administration/setupmaintain/backend/#further-installations).

Update the eXist packages first. For this action, make sure that the default repository, specified by default in the file `/db/apps/packageservice/configuration.xml` is

- `http://exist-db.org/exist/apps/public-repo`

...thus in the config file

```xml
<repository active="true" default="true">http://exist-db.org/exist/apps/public-repo</repository>
```

The next step is to update the ART-DECOR backend. Change the repository in file `/db/apps/packageservice/configuration.xml` to any of the above mentioned paths, depending on stable or development.

| ART-DECOR Release 3<br/>backend branch | URL to use                                      |
| -------------------------------------- | ----------------------------------------------- |
| **Stable**                             | `https://repository.art-decor.org/stable3`      |
| **Development**                        | `https://repository.art-decor.org/development3` |

Change the repository element to what you desire, e.g. for the stable branch use

```xml
<repository active="true" default="true">https://repository.art-decor.org/stable3</repository>
```

...and comment out the eXist repository, so that there is only one active repository.

::: warning

Please deinstall the ART-DECOR API **before** you install the recent version. This step is important as the scheduled tasks would prevent the install process to be finalized.

:::

::: warning

If you install the new release on **Terminology Package** please put that update at the end. The index definition for data packages has been updated from Terminology Package v2.1.22 on. Installation includes re-index and **can take a significant amount of time (even hours)** depending on how many packages you have installed and the hardware you are running on.

:::

::: note UPDATE

In the package manager all packages ready for an update are shown. Update them step by step, taking the warnings mentioned above into account.

:::

### Final Backup of the full installation

Optionally you can backup the full installation again, as described above.

## Updating the frontend

If you followed our advice to place ART-DECOR related stuff in `/opt` go to the directory

```sh
cd /opt/art-decor-vue
```

... otherwise to the one you've chosen during the fron end installation process.

Get the **ART-DECOR VUE Frontend** from the repo and install it.

If you install it on a local machine (laptop etc.) use

```sh
./vue_pull.sh
```

On servers run the script as shown here

```sh
./vue_pull.sh --local
```

The second step is to update the **ART-DECOR HTTP Proxy** from the repo.

```sh
./proxy_pull.sh
```

This action also starts the ART-DECOR HTTP Proxy at port 3000.

## Updating external documentation and tools

Optionally you can run updates for ART-DECOR external documentation and tools, such as:

- ADRAM