---
permalink: /konec
---
# Konec and Krtek

On installation and procedure descriptions in ths documentation you might find a mole at the end with the word konec. Here is a bit of explanation for our curious users.

<img src="../img/krtek.png" style="zoom:33%;" />



The **Mole**, in the Czech language also *krtek*, *krteček*, is an animated character in a series of cartoons created by Czech animator Zdeněk Miler (1921–2011). In the original Czech version of the cartoon, the mole is called *krtek*, the Czech word for mole. 

The premiere of the first short film with Mole took place at the Venice Film Festival in 1957. Since its inception, the cartoon has gained enormous popularity in many countries.

The 63 episodes ended with an "The End" counterpart in the Czech language: *konec* (pronounced *con-edge*).
