---
permalink: /licenses
---

# ART-DECOR® – Licenses

::: note Trademark

ART-DECOR® is a registered trademark. 

:::

## Overview ART-DECOR license model

The license model for ART-DECOR® covers the following areas

* DECOR Methodology
* DECOR software components 
* ART (the user interface)

## Licenses DECOR

### DECOR methodology

The DECOR methodology is licensed under following license:

- Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported

### DECOR Software

The DECOR software (xsl, php, java etc.) is distributed with the following license

* GNU General Public License (GPL), version 3.
* A few exceptions are made for some software components, see License Apache 2.0

A short summary of the GPL rights and conditions with which DECOR software is distributed (not a complete listing):

1. Attribution: copyright notice should be displayed.
2. Source code: source code should be accessible for the public.
3. Copyleft modified work: offers the right to distribute copies and modified versions of a work and requiring that the same rights be preserved in modified versions of the work.
4. Copyleft for entire work: You must license the entire work, as a whole, under the GPL License to anyone who comes into possession of a copy.
5. See disclaimer below

For the full license text, see: http://www.gnu.org/licenses/gpl.html

## Licenses ART

### ART software

The ART software is distributed with the following license:| 

* GNU Lesser General Public License, (LGPL), version 2.1+

A short summary of the LGPL rights and conditions with which ART software is distributed (not a complete listing):
1. Attribution: copyright notice should be displayed.
2. Source code: source code should be accessible for the public.
3. Copyleft modified work: offers the right to distribute copies and modified versions of a work and requiring that the same rights be preserved in modified versions of the work.

For the full license text, see: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html

## License Documentation

The documentation is distributed with the following license: CC BY 4.0 Licensed, Copyright © 2009-<currentYear/> ART-DECOR Open Tools GmbH (ADOT)

## License Apache 2.0

The following software components of the DECOR software part uses the Apache 2.0 license:

* core / DECOR-DTr1.xsl

| License | |
|-----|-----|
| **YOU CAN** | |
| Commercial Use    | …use the software for commercial purposes. |
| Modify            | …modify the software and create derivatives. |
| Distribute        | …distribute original or modified (derivative) works. |
| Sublicense        | …grant/extend a license to the software. |
| Private Use       | …use/modify software freely without distributing it. |
| Use Patent Claims | …practice patent claims of contributors to the code. |
| Place Warranty    | …place warranty on the software licensed |
| **YOU CANNOT** | |
| Hold Liable       | …charge the software/license owner for damages. |
| Use Trademark     | …use contributors' names, trademarks or logos. |
| **YOU MUST** | |
| Include Copyright | …retain the original copyright. |
| Include License   | …inlcude the full text of license in modified software. |
| State Changes     | …state significant changes made to software. |
| Include Notice    | …you must include that NOTICE when you distribute if the library has a "NOTICE" file with attribution notes. You may append to this NOTICE file. |

For the full license text, see: https://www.apache.org/licenses/LICENSE-2.0

## Copyright

Please see our [Copyright note](/copyright/).

## Third party terms of use

### SNOMED Clinical Terms® (SNOMED CT®)

This material includes SNOMED Clinical Terms® (SNOMED CT®) which is used by permission of the International Health Terminology Standards Development Organisation (IHTSDO). All rights reserved. SNOMED CT®, was originally created by The College of American Pathologists. “SNOMED” and “SNOMED CT” are registered trademarks of the IHTSDO.

### Logical Observation Identifiers Names and Codes LOINC

This material contains content from LOINC® (http://loinc.org). The LOINC table, LOINC codes, and LOINC panels and forms file are copyright © 1995-2013, Regenstrief Institute, Inc. and the Logical Observation Identifiers Names and Codes (LOINC) Committee and available at no cost under the license at http://loinc.org/terms-of-use.

## Disclaimer

You expressly acknowledge and agree to use this software and documentation under the following terms:

1. This software, such as XML-stylesheets and XML-schematron scripts, are meant to be used as demonstration software, reference implementation or test software.

2. You agree to use and maintain this software and documentation under your sole responsibility.

3. Liability claims against the authors relating to damages of any kind or damage caused by the use of the software or through the use of any inaccurate and incomplete software or documentation are excluded on principle, unless there is proof of gross negligence or intent on the part of the authors.

4. This software and documentation is provided “as is”. The authors do not pretend to cover all situations and exceptions. The authors are not obligated to supply maintenance or updates. The authors are not obligated to supply instructions, documentation or help with installation of software. 

5. Intellectual property of contained software and documentation remains with the authors. When reusing the software or documentation as part of an entire work, you may not charge third parties a fee or impose additional licenses for this part of software or documentation.
