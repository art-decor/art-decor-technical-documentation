---
permalink: /release2/
---

# ART-DECOR® Release 2

ART-DECOR Release 2 has been first released 2010. Since 2020 Release 3 is in development and this new environment will replace Release 2 in the future.

Until end of 2023, ART-DECOR Release 2 is in maintenance only mode which means that there will be no new features introduced. From 2024 on, ART-DECOR Release 2 will be phased out completely in all ADOT maintained projects and there is the strong recommendation to do this also for self-hosted projects.

For good old times, here are a few topics that might be of interest.

- [Installation of Orbeon for ART-DECOR Release 2](/administration/setupmaintain/frontend2/)
- [Migrating eXist-db 2.2 to Release 3](/release2/migration/)
- [Special Features](/release2/specials)
