---
permalink: /release2/specials/
---

# Special Features

This page describes some special features for ART-DECOR Release 2.

## ART-DECOR Applications aka ADA

This chapter contains the documentation of ART DECOR Applications (ADA), for those who want to start building and deploying ADA apps. ADA apps are fully functional applications, generated from DECOR datasets, transactions and valueSets. 

ADA is an ideal tool for:
* rapid prototyping, to show care providers what the DECOR specs functionally mean;
* iterative improvement of DECOR specs, where feedback on the ADA app is used to improve the DECOR specs;
* creating realistic examples, since care providers themselves can provide those using ADA,
* to make examples in a simplified XML format, to be used as the basis for conversion to HL7 or other formats.

ADA is not:
* secure, so never use real patient data in ADA;
* a complete application (i.e. there is no database with patient data from which to start, one tests only transactions);
* a UI with the capabilities of a tailor-made interface - the focus is on functional completeness, not necessarily the optimal workflow for a real-life scenario.

### Installing ADA

To get started with the examples, you'll need ADA Core checked out to a local directory. Either get it from SourceForge, or install the ADA package in eXist and copy that to disk. 

Start with this layout. Installing the ADA package will take care of most of this (not my-project, but it does the rest):

- ada
  - core (for ada/core)
- ada-data
  - projects
    - my-project (we'll use demo1 in this Guide)
      - definitions
      - new
      - schemas
      - views

After downloading ADA, ada-data/projects/empty contains the layout to get you started.

### ADA Application

ADA starts with a DECOR release, as made in the ART project form. Make one (or refer to an existing one), and retrieve the URI to it through:

```
http://localhost:8877/decor/services/ProjectIndex
```

(Throughout this Guide, we'll use localhost:8877 for the host - use another if you don't run a local instance on port 8877).
Search for your project, and get an URL like this one:

```
http://localhost:8877/decor/services/RetrieveTransaction?id=2.16.840.1.113883.3.1937.99.62.3.4.2&language=en-US&version=2014-05-12T14:02:55&format=xml
```

With RetrieveTransaction, a decor/transaction is 'enhanced' with all the goodies the dataset and terminology provide. It is:
* a decor transaction
* with the tree view from the dataset
* all concept adornments (names, valueDomains, you name it) from the dataset
* for every valueDomain of type 'code', the relevant valueSet pulled in
* and all this filtered for just one particular language so you don't need to worry about stuff you don't need to worry about.

### ADA Definitions

Now make an ADA definition file in /projects/my-project/definitions and call it demoapp-ada.xml. Here's an overview of the structure.

::: note

If you want a generated start point for your project, or this demo project, please go to `/decor/services/RetrieveProject?prefix=demo1-` on your server, see also [here](https://wiki.art-decor.org/index.php?title=URIs#RetrieveProject). Select your project, select your version (if any), and using mode 'ada-definition' you may generate a definition as outlined below.

:::

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ada xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xsi:noNamespaceSchemaLocation="../../../core/ada.xsd">
    <project prefix="demo1-" language="en-US" 
        versionDate="...">
        <release baseUri="http://localhost:8877/decor/services/RetrieveTransaction"/>
    </project>
    <applications>
        <application version="1">
            <views>
                <view id="1" type="crud" target="xforms" 
                    transactionId="..." 
                    transactionEffectiveDate="...">
                    <name>My Form</name>
                    <concepts include="all"/>
                </view>
                <view id="2" type="index" target="xforms" 
                    transactionId="..." 
                    transactionEffectiveDate="...">
                    <name>My Index</name>
                    <indexOf ref="1"/>
                    <concepts include="only"/>
                </view>
            </views>
        </application>
    </applications>
</ada>
```

The root is called /ada, below are:

#### project

```xml
<project prefix="demo1-" language="en-US" versionDate="2014-05-12T14:02:55">
     <release baseUri="http://localhost:8877/decor/services/RetrieveTransaction"/>
</project>
```

Change the following:
* set `project/@prefix` to your project prefix (for documentation only)
* set `project/@language` to the language from the RetrieveTransaction link
* set `project/@versionDate` to the date from the RetrieveTransaction link (leaving it empty will retrieve the 'live' version of the transaction)

ADA retrieves all necessary information from the RetrieveTransaction service. Make sure this is OK (like, all names for the required language are provided, valueSets are present in the concepts etc.). If not all is present - ADA is a really good way to test the completeness of your DECOR specs. Just generate an ADA app, run it and see what's missing.

#### application

Next comes a bit of housekeeping. 

```xml
<applications>
    <application version="1">
        <params>
            ...
        </params>
        <views>
            <view ...>
            </view>
        </views>
    </application>
</applications>
```

Everything except the `application/@version` this is just a way to get you to where it really happens: the view. An ADA view is - basically - an enhanced version of the RetrieveTransaction output, just like RetrieveTransaction itself is an enhanced version of a DECOR transaction. The ADA view allows you to add in UI necessities:
* widgets 
* tabs
* links between views (such as an index which points to detail views)
* and more, and still more to come.

#### params

Sometimes you want to globally override certain behavior. Using a parameter you may override/extend certain behavior in creating the release file. You may define parameters at almost any level, but the parameter that is 'closest' to the item it needs to affect, counts. At present these options, depicted with their recommended settings, are supported here:

```xml
    <param name="overrideDatatypeDateTimeAsString" value="true"/>
    <param name="overrideDatatypeTextAsString" value="true"/>
    <param name="overrideContainsAsIdentifier" value="false"/>
```

* `overrideDatatypeDateTimeAsString` - Using this parameter you can override `valueDomain/@type='datetime'`, `valueDomain/@type='date'` as string. This supports forms that need parametrized dates which are not valid dates in itself. The only other way to do this is by listing and overriding each concept separately. Date or dateTime depends on the original valueDomain and properties in the dataset. Supported formats for parametrized dates in general are:
** T = today
** T-10D = ten days ago. Also support for M (months), or Y (years)
** T+10D = ten days from today. Also support for M (months), or Y (years)
** T-10D{hh:mm:ss} = ten days ago at exactly hh:mm:ss. No need to state seconds
** T+10D{hh:mm:ss} = ten days from today at exactly hh:mm:ss. No need to state seconds
** T{hh:mm:ss} = today at exactly hh:mm:ss. No need to state seconds
** Stating a regular date as YYYY-MM-DD or dateTime as YYYY-MM-DDThh:mm:ss is also possible
* `overrideDatatypeTextAsString` - Using this parameter you can override `valueDomain/@type='text'` as string. This supports forms that do not want rich text as value for the concept values. The only other way to do this is by listing and overriding each concept separately using valueDomainType='string'.
* `overrideContainsAsIdentifier` - Using this parameter you can override concepts with a contains element, that do not have a valueDomain and are not marked for expansion somewhere to be an item with `valueDomain/@type='identifier'`. This supports referencing identified 'objects' that live somewhere else although undefined where , like health care provider or patient. The only other way to do this is by listing and overriding each concept separately.

#### view

Most of the times, you'll want two views:
* a CRUD form for creating and editing data based on your transaction
* an index form which lists and links to the created records.

The ADA view is where it all happens. Views need:
* a `view/@type` (we'll use 'crud' and 'index' in this walkthrough)
* a `view/@target` (for which we'll explore 'xforms' - html is another obvious candidate)
* a `view/@transactionId` and `view/@transactionEffectiveDate` (this of course identifies the corresponding DECOR transaction)
* an optional attribute `view/@addComments`. If 'true', group headers will have a 'comment' input control where the user can describe the group, particularly useful when ADA is used to make test cases.
* an optional attribute `view/@errorSummary` (default 'true') which supresses the Orbeon error-summary. In large forms the error-summary may lead to runtime errors. Set to 'false' to suppress the Orbeon error-summary.
* an optional attribute `view/@language` which overrides the `project/@language`.
* a `view/name` (which is shown in the browser)
* one or more `view/concepts` or (only for indexes) `view/indexOf/concepts`. concepts come in two flavors:
  * `concepts include='all'` (All concepts in the DECOR transaction are shown. No concept children are needed, unless you want special processing for those.)
  * `concepts include='only'` (No concepts in the DECOR transaction are shown, except the children of `<concepts>`.)
  * `concept/@ref` points to a DECOR concept from the DECOR transaction to override properties of and/or add concepts to as first in group or
  * `concept/@preceding-ref` points to a DECOR concept from the DECOR transaction to add contained concepts before or
  * `concept/@following-ref` points to a DECOR concept from the DECOR transaction to add contained concepts after
  * `concept/@widget` is used for UI specials (In this example, we'll create two tabs - of course, for a tabbed view, you might want to choose concepts groups on the same depth in the DECOR dataset tree.)
  * `concept/@adaId` is used to offer an `@id` on the concept in the ADA format. This @id is expected to behave like an [xml:id](https://www.w3.org/TR/xml-id/) / [xs:ID](https://www.w3.org/TR/xmlschema11-2/#ID). This `concept/@id` may subsequently be referred to by a `concept[@datatype = 'reference']/@value`
  * `concept/@adaIdAndConceptRef` Same as @adaId, and additionally the release file creation process (ada2release.xsl) will assume that you may want refer to this group from other instances in the same context. To support this, a new dataset concept is injected in creating the release file with a contains relationship to the adaId concept. The name will be concept name + 'Ref'. If you need to refer to this concept from other places in your dataset then your dataset needs to have concepts with a contains relationship to this concept where relevant.
    * `concept/@adaIdAndConceptRef` = true, overrides `@adaId` = false but generally you should NOT use both.

Note that if you are defining extra concepts, then these must be fully populated comparable to concepts in a compiled dataset.

::: note

How the referencing dropdown for concepts internally or externally works:
* **Internal**: while you are working in the form, each reference keeps track for concepts that match the definition of the ADA release, normally based on `@conceptId`, that also have an @id and presents those under Internal references. If you do see any: you don't have the right object set with an @id to work with
* **External**: upon opening the form in edit mode, it loads the list of all other instances in the same project that have concepts with an `@id`. Just like with internal instances, only the correct type is listed under External References. If you do see any: you don't have the right object set with an @id to work with. External references are of the form `[instance/@id]#[concept/@id]`

Dutch example with "*Interne verwijzingen*" and "*Externe verwijzingen*"
![image-20231108235959-form-reference-dropdown](../../img/image-20231108235959-form-reference-dropdown.jpg)

:::

This view will constitute the main CRUD view to edit or create instances of this particular transaction. The transaction below is a 'measurement form', where a patient can periodically submit weight measurements to a registry (the example is meant to show all main DECOR constructs, and is admittedly somewhat artificial). For CRUD forms, only 'xforms' is currently a valid target environment.

```xml
<view id="1" type="crud" target="xforms"
  transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2"
  transactionEffectiveDate="2012-09-05T16:59:35">
    <name>Measurement Form</name>
    <concepts include="all">
        <concept ref="2.16.840.1.113883.3.1937.99.62.3.2.1" widget="collapse" 
          initial="open"  minimumMultiplicity="1"/>
        <concept ref="2.16.840.1.113883.3.1937.99.62.3.2.6" widget="collapse"/>
        <concept ref="2.16.840.1.113883.3.1937.99.62.3.2.3" widget="radio"/>
        <concept ref="2.16.840.1.113883.3.1937.99.62.3.2.5" widget="radio"
          default="4"/>
        <concept notPresentWhen="../weight_gain/@value='false'"
          ref="2.16.840.1.113883.3.1937.99.62.3.2.18"/>
    </concepts>
</view>
```

For index views the following options exist for specifying concepts that need to be in the index page:
* view has `@transactionId` and `<concepts>` has no `@transactionId`
  * Meaning: consider child concepts under `<concepts>` for `@transactionId`

```xml
<view id="1" type="index" target="xforms" transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2"
    transactionEffectiveDate="2012-09-05T16:59:35">
    <name>Measurement Form</name>
    <concepts include="only">...</concepts>
</view>
```

* view has no `@transactionId` and `<concepts>` has `@transactionId` + `@transactionEffectiveDate`
  * Meaning: consider child concepts under `<concepts>` for `@transactionId` + `@transactionEffectiveDate`

```xml
<view id="1" type="index" target="xforms">
    <name>Measurement Form</name>
    <concepts include="only" transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2"
        transactionEffectiveDate="2012-09-05T16:59:35">...</concepts>
</view>
```

* `view` and `concepts` both have no @transactionId
* Meaning: consider child concepts under `<concepts>` for all transactions in definition

```xml
<view id="1" type="index" target="xforms">
    <name>Measurement Form</name>
    <concepts include="only">...</concepts>
</view>
```

* `view` has no `@transactionId`, there no `view/concepts`, `indexOf` contains `<concepts>`
  * Meaning: consider child concepts under `<concepts>` for the transaction that indexOf points to

```xml
<view id="1" type="index" target="xforms">
    <name>Measurement Form</name>
    <indexOf ref="2">
        <concepts include="only">...</concepts>
    </indexOf>
</view>
<view id="2" type="crud"  target="xforms" ... 
    transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2" 
    transactionEffectiveDate="2012-09-05T16:59:35">
    ...
</view>
```

Views can define which widgets are used. They can also override DECOR attributes such as `minimumMultiplicity` in this example. If an attribute occurs in both the DECOR and ADA definitions, the ADA one will take precedence. This may be necessary when UI behaviour is not the same as the definition for messaging.

We'll make a second view, since we'll need an index of all weight measurements for lookups and edits. We'll list the concepts we want shown in the index (name and date of measurement, here). For indexes, 'xforms' is a valid target environment.

Take care: 
* use a unique name for the index, not the same name as the main form
* indexes need an "indexOf" element which points to the `@id` of the form being indexed

```xml
<view id="2" type="index" target="xforms"
    transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2" 
    transactionEffectiveDate="2012-09-05T16:59:35">
    <name>Measurement Index</name>
    <indexOf ref="1"/>
    <concepts include="only">
        <concept ref="2.16.840.1.113883.3.1937.99.62.3.2.4"/>
        <concept ref="2.16.840.1.113883.3.1937.99.62.3.2.2"/>
    </concepts>
</view>
```

#### concept children

Children from a concept in a view are copied as-is. This allows for:
* The insertion of new concepts which are not in the dataset. Examples are: comments, displayNames etc. which are not required in the dataset but provide extra information in this view. Concept children should look like the XML output from RetrieveTransaction, with unique id's and names and shortNames which are unique in the context (i.e. no homonymous siblings). Provide at least: `@id`, `@minimumMultiplicity`, `@maximumMultiplicity`, @type, `implementation/@shortName`, `name`, `valueDomain/@type`. 
* Pieces of code, provided one uses a code generator which picks those up (the standard XForm code generator for ADA doesn't).

```xml
<concept ref="2.16.840.1.113883.3.1937.99.62.3.2.1">
    <concept id="test-01" minimumMultiplicity="1" maximumMultiplicity="1" type="item">
        <implementation shortName="test"/>
        <name>Test</name>
        <valueDomain type="string"/>
    </concept>
</concept>
<concept ref="2.16.840.1.113883.3.1937.99.62.3.2.6">
    <script>function(x, y) ....</script>
</concept>
</view>
```

#### UI attributes for concepts

Concepts may need extra data which is not part of the DECOR definition. but is solely there for the UI logic. These can be added to the concepts in the ADA definition.

| UI Attribute  | Example  | Description  |
|---|---|---|
| default |default="4" |Provides a default value which is used instead of an empty value for newly added concepts. For codes this should be @localId when concepts have a valueSet, or else valueDomain/conceptList/concept/name.
|notPresentWhen |notPresentWhen="../weight_gain/@value='false'" |Will not show the concept if the mentioned condition is true. The condition must be an XPath expression relative to the current concept in the ADA XML format.
|widget | |See below.
|initial |initial='open'  initial='closed' |Initial state for collapsible widgets. 'closed' is the default.
|skip |skip="true", skip="1" |This concept is skipped in the UI. The schema isn't changed, and the xml element corresponding to the skipped item is inserted in the ADA XML. Use with caution, since skipped concepts cannot be added, changed or removed in the UI. Skipping items is not recommended, since it may lead to invalid ADA XML data. Main use: skip superfluous group levels.
|order |order='1', order='2', order='99' |Determines the order in which the concepts are shown in the UI. All concepts siblings must have an order number for this feature to work.
|adaId |adaId="true", adaId="false" (default value) |Adds the option of a concept/@id in ADA instances that behaves like an [xml:id](https://www.w3.org/TR/xml-id/) / [xs:ID](https://www.w3.org/TR/xmlschema11-2/#ID)

#### widgets

These are the currently supported widgets for each target platform.


| Widget | Target | Description |
|---|---|---|
|tab |XForms |Will generate a tab for this concept (group). Use only on top level. When widget is not 'tab', groups will be collapsible. |
|radio |XForms |Will generate radio buttons for codes instead of a drop-down list. |

#### Specifying UI attributes through ART DECOR

There's also a convenient route to specify UI attributes for ADA through ART. ART supports communities. In any project, in the project window, choose 'MyCommunity' and make a new community with name 'ada'. (DisplayName can be anything you want). You will need to give 'guest' access to the ADA community, otherwise ADA will not be able to retrieve the ADA community info from the ada2release stylesheet. Then use the 'Definitions' tab to add the necessary UI attributes, such as 'widget' and 'initial'. Now use this community to add UI attributes to concepts.

![image-20231108235959-ada-community](../../img/image-20231108235959-ada-community.jpg)

This provides a simple route to enter attributes without accessing the underlying XML files.

Definitions will take precedence in the following order:
1. Attributes defined in the ADA definition file
1. Attributes defined in myCommunity ada
1. Attributes defined in the DECOR file

So it is possible to override what's defined in DECOR through an ada community, and to override that again in the ADA definition file. Note: ada communities will work on dataset level, whereas the ADA definition works on transaction level, thus allowing finer-grained control.

### Making an ADA release

The next step is to execute some of the ADA stylesheets. Use ada2release.xsl (See "[Generating an ADA App](#generating-an-ada-app)") to transform the ADA definitions to a release:

```
demoapp-ada.xml -> [ada2release.xsl] -> demoapp-ada-release.xml
```

This process pulls in all the information from RetrieveTransaction and combines it with the UI logic from ADA. 

```xml
<view id="1" type="crud" target="xforms"
  transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2" 
  transactionEffectiveDate="2012-09-05T16:59:35">
    <name>Measurement Form</name>
    <implementation shortName="measurement_form"/>
    <dataset id="2.16.840.1.113883.3.1937.99.62.3.1.1" 
      effectiveDate="2012-05-30T11:32:36" statusCode="draft"
      transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2" 
      transactionEffectiveDate="2012-09-05T16:59:35" 
      shortName="measurement_message">
      <concept minimumMultiplicity="0" maximumMultiplicity="*" 
        conformance="" isMandatory="false" 
        id="2.16.840.1.113883.3.1937.99.62.3.2.1"
        statusCode="draft" effectiveDate="2012-05-30T11:32:36" 
        type="group" widget="tab">
        <name language="en-US">Measurement</name>
        <desc language="en-US">Measurement of body weight on a specific date</desc>
        <implementation shortName="measurement" 
          xpath="/hl7:registrationProcess/hl7:organizer"/>
```

It still looks a lot like an ADA definition, with:
* a `<dataset>` element where `<concepts>` was
* implementation elements with
* a @shortName, which is used for (amongst others) XML element names
* an @xpath expression which shows where this concept resides in a HL7 document (only if all templates and templateAssociations are present).
* all other information from dataset/concept and transaction/concept
* plus the information which was added in the ADA definition, such as @widget

### Generating an ADA App

The next step is convert demoapp-ada-release.xml to the necessary schemas, forms and other resources. Use transformation scenarios in a tool like oXygenXML

Get the xsls from your server. E.g. https://decor.nictiz.nl/ada/core/release2newxml.xsl.

* Create/apply "XML transformation with XSLT"
  * "ADA-1 generate release" with XML URL ${currentFileURL} -> [ada2release.xsl] -> create ADA release file from initial ADA file
  * "ADA-2 generate new" with XML URL ${currentFileURL} -> [release2newxml.xsl] -> create empty XML from ADA release file
  * "ADA-3 generate schema" with XML URL ${currentFileURL} -> [release2schema.xsl] -> create XML Schema from ADA release file
  * "ADA-4 generate xforms" with XML URL ${currentFileURL} -> [release2xform.xsl]  -> create XForms from ADA release file
  * *ADA-5 and ADA-6 no longer apply*
  * "ADA-7 generate package files" with XML URL ${currentFileURL} -> [release2package.xsl]  -> create eXist-db install details, e.g. expath-pkg.xml and repo.xml,from ADA release file
    * Parameter `authorString` e.g. Nictiz
    * Parameter `authorWebsiteString` e.g. https://www.nictiz.nl 

#### The ADA XML format

The first transform generates an empty ADA XML instance:
  ada/projects/demoapp/new/measurement_message.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!--New XML generator, 2014-05-13T13:17:04.038+02:00-->
<measurement_message id="new" app="demoapp"
  transactionRef="2.16.840.1.113883.3.1937.99.62.3.4.2" 
  transactionEffectiveDate="2012-09-05T16:59:35"
  versionDate="2014-05-13T13:16:14" 
  prefix="demoapp-" language="en-US">
   <measurement conceptId="2.16.840.1.113883.3.1937.99.62.3.2.1">
      <weight conceptId="2.16.840.1.113883.3.1937.99.62.3.2.3" value="" unit="kg"/>
      <weight_gain conceptId="2.16.840.1.113883.3.1937.99.62.3.2.13" value="false"/>
      <weight_gain_data conceptId="2.16.840.1.113883.3.1937.99.62.3.2.18">
         <amount_of_weight_gain conceptId="2.16.840.1.113883.3.1937.99.62.3.2.19" value="" unit="gram"/>
         <cause_of_weight_gain conceptId="2.16.840.1.113883.3.1937.99.62.3.2.20" value=""/>
      </weight_gain_data>
      <datetime conceptId="2.16.840.1.113883.3.1937.99.62.3.2.2" value=""/>
      <measured_by conceptId="2.16.840.1.113883.3.1937.99.62.3.2.5" value="1"/>
      <remarks conceptId="2.16.840.1.113883.3.1937.99.62.3.2.17" value=""/>
   </measurement>
   <person conceptId="2.16.840.1.113883.3.1937.99.62.3.2.6">
      <name conceptId="2.16.840.1.113883.3.1937.99.62.3.2.4" value=""/>
      <bsn conceptId="2.16.840.1.113883.3.1937.99.62.3.2.7" value=""/>
      <date_of_birth conceptId="2.16.840.1.113883.3.1937.99.62.3.2.8" value=""/>
      <length conceptId="2.16.840.1.113883.3.1937.99.62.3.2.15" value="" unit="m"/>
   </person>
</measurement_message>
```

The ADA XML is a very straightforward XML format. It's design principles are readability, conciseness and following the dataset as closely as possible. It has:
* an XML element name taken from @shortName (since shortName discards spaces and most special characters, and is all lowercase, DECOR concept names must yield unique shortNames for ADA to work)
* the tree structure of the dataset, making it easy to navigate
* an optional conceptId (it may be absent since it can be calculated from the ADA release info, if needed)
* @value attribute containing the entered value or the indexed localId for a code. @value might be left out for coded concepts as the value would then be in code, and could be unknown in which case @nullFlavor should be present
* @unit for quantity units, @value holds the value 
* @root for identifier systems, @value holds the identifier
* @nullFlavor for unknown @value. Supports any valid NullFlavor, e.g. UNK, MSK, NI. @nullFlavor should not co-exist with coded data. In this case @code will hold the @nullFlavor and @codeSystem points to the NullFlavor code system.
* @code, @codeSystem, @codeSystemName, @codeSystemVersion, @preferred, @originalText attribute for coded concepts. @originalText is expected when NullFlavor is OTH.
* @id supporting a value of type xml:id / xs:ID useful for referring to this concept
* @datatype supporting any [DECOR DataSetValueType](https://wiki.art-decor.org/index.php?title=DECOR-dataset). This is relevant when the dataset concept has the indeterminate value 'complex'. In addition to the DECOR datatypes, there is 'reference' which allows a @value to refer to a concept/@id in the same instance.

Valid combinations of attributes in ADA XML elements (@conceptId and @id are always valid):
* @value
* @value + @unit
* @value + @root 
* @code + @codeSystem, optionally @value and/or @displayName, @preferred and/or @originalText
* @nullFlavor, optionally with @unit or @root, never with @code
* @datatype (only for complex concepts) with any of the above combinations

Retrieval of lists with more than one resource are bundled in FHIR style. Bundle is not in the FHIR namespace since it is not 100% compliant. This may change in the future.

```xml
<Bundle>
  <type value="searchset"/>
  <entry>
    <resource>
      <measurement_message ...>
      ...
      </measurement_message>
    </resource>
  </entry>
  <entry>
    <resource>
      <measurement_message ...>
      ...
      </measurement_message>
    </resource>
  </entry>
</Bundle>
```

#### The ADA JSON format

ADA also supports JSON, both as output and input format.

```json
{"measurement_message": {
    "app": "demoapp",
    "transactionRef": "2.16.840.1.113883.3.1937.99.62.3.4.2",
    "transactionEffectiveDate": "2012-09-05T16:59:35",
    "versionDate": "",
    "prefix": "demoapp-",
    "language": "en-US",
    "title": "POC case 2",
    "id": "fc912512-7d4c-4cd6-a9c4-4912a23ecb77",
    "measurement": [{
        "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.1",
        "weight": [{
            "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.3",
            "value": 70,
            "unit": "kg",
            "datatype": "quantity"
        }],
        "weight_gain": [],
        "weight_gain_data": [],
        "length": [],
        "datetime": [],
        "measured_by": [{
            "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.5",
            "value": "1",
            "code": "P",
            "codeSystem": "2.16.840.1.113883.3.1937.99.62.3.5.1",
            "displayName": "Patiënt",
            "datatype": "code"
        }],
        "comments": []
    }],
    "person": [{
        "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.6",
        "name": [{
            "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.4",
            "value": "Marc",
            "datatype": "complex"
        }],
        "bsn": [{
            "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.7",
            "value": "12341234",
            "datatype": "identifier"
        }],
        "date_of_birth": [],
        "number_of_children": []
    }]
}}
```

The serialization rules are simple (and quite like the JSON format of FHIR):
* The transaction element in serialized as a {name, value} pair.
* All elements become JSON arrays. Of course 0..1 and 1..1 concepts items  needn't be arrays, but a single approach allows applications to treat all concepts alike. Also, if a new version of the ADA app changes a concept to 1..*, old JSON data files will still interoperate.
* All attributes become {name, value} pairs. Booleans, counts, decimal and quantity become literals (i.e. 6, true, false), others become strings (i.e. "Marc").
* The datatype is serialized as well.

#### The ADA RESTful interface

ADA has a RESTful interface, also inspired by FHIR. The RESTful interface operates on /db/apps/ada, not /db/apps/ada-data. See the controller.xql in /db/apps/ada for details. The following methods are supported:
* GET [base]/[app]/[id] reads a resource(where id is the uuid of the ADA XML)
* GET [base]/[app]/new reads a new (empty) resource 
* GET [base]/[app] returns a list of all resources for this app (in a `<Bundle>`)
* POST [base]/[app]/ will save the POSTed data. POST data should be an ADA XML file. If the uuid exists, it will be updated (if the user has access rights), if not, it will be created. (TODO: provide PUT interface.)
* DELETE [base]/[app]/[id] will remove the resource.

An example is:
http://localhost:8877/ada/projects/demoapp/3deb5f5f-3898-4c7c-94b9-cfe53c8e6d73
where:
* [base] = http://localhost:8877/ada/projects
* [app] = demoapp
* [id] = 3deb5f5f-3898-4c7c-94b9-cfe53c8e6d73

Also the following parameters are allowed:


| Parameter | Values  | Description  |
|format |json, xml, html |This overrides the Accept header. Permitted values are json, xml, html, and the corresponding media types (i.e. application/json etc.)
|from |an @id |This retrieves the data with @id from the data collection, or if not available there, from the new collection and uses the 'from' data as prototype instead of the standard 'new' XML. This can be useful if for instance optional groups should be absent by default, or to prefill fields with default values. Create a default record using the UI, and use that record in the 'from' clause.
|summary |true, false |If true, returns only summary data, which are the concepts which are defined in views of type 'index'.

#### ADA XML Schema

There are now two schemas:
  ada/projects/demoapp/schemas/measurement_message_draft.xsd
  ada/projects/demoapp/schemas/measurement_message.xsd

Both schemas validate the ADA XML format for measurement_message. The 'draft' version allows incomplete data to be stored in the database, i.e. when someone is filling out a complex form and wants to store it for later completion. A draft schema will allow @value and @unit to be absent on all nodes, even when mandatory in the transaction. The other schema checks all constraints from the dataset and scenario. A couple of 'ada-...' schemas are generated too. These are wrappers which validate the ADA XML as stored in the ADA data store (See [The ADA XML Data Store](#The_ADA_XML_data_store)).

#### ADA Views and Modules

Our two views are stored in:
  ada/projects/demoapp/views/measurement_index.xhtml
  ada/projects/demoapp/views/measurement_form.xhtml
We'll see those in action soon.

### Deploying an ADA App

* Run `ant` in your ADA App folder to create a sub directory `build` containing the xar for installation, or zip the contents of the ADA App folder as [folder name].zip
** Do not zip the app folder itself as that will create a top level folder inside the zip 
* Go to https://[your server]/ada/modules/index-admin.xquery?language=nl-NL
** Note that whatever the selected project is in index-admin is irrelevant to what you are installing. What you are installing may be a new version of the app or a completely new app
* Select the zip or xar file using the Select button next to the label *Upload new ADA project version*
* If successful, it will briefly say so and after five seconds return you to the index.admin page
* If this fails, it will tell you what's wrong so you may fix that

If `conf.xml` contains an element `<log/>` all requests and POST data will be logged to ada-data/log.

### Running the ADA App

Now point your browser to http://localhost:8877/ada/modules/index.xquery (or where your eXist lives). You may need to log into eXist, and should see something like:

![image-20231108235959-ada-index](../../img/image-20231108235959-ada-index.jpg)
with at least the demoapp included. Click demoapp to view
  http://localhost:8877/ada/projects/demoapp/modules/index.xquery
in action:
![image-20231108235959-measurement-index-empty](../../img/image-20231108235959-measurement-index-empty.jpg)
Not spectacular yet. Click 'New' to see:
  http://localhost:8080/art-decor/ada/demoapp/views/measurement_form.xhtml?id=new
This link tells ADA to use the measurement_form for a new (empty) instance. Note the red exclamation marks for data which is not yet valid. If you see nothing, or too little, you're probably not logged into ART. ADA uses the ART login for the time being for XForms. At the right top is a 'Login' link.
![image-20231108235959-measurement-form-empty](../../img/image-20231108235959-measurement-form-empty.jpg)
Fill out the form. You can add a second (or third, etc.) measurement with the '+ Measurement' button.
![image-20231108235959-measurement-form-tab1](../../img/image-20231108235959-measurement-form-tab1.jpg)
Don't forget the nice-looking tab2. This is where the ADA widgets came into action to create tabs.
![image-20231108235959-measurement-form-tab2](../../img/image-20231108235959-measurement-form-tab2.jpg)
Finally, save the data and return to the demoapp index:
![image-20231108235959-measurement-index-full](../../img/image-20231108235959-measurement-index-full.jpg)
We now see the two columns we defined for the index ('Datetime' and 'Name'). By default, ADA provides the username of the creator and the last update time.

```xml
<a name="The_ADA_XML_data_store"></a>
```

### The ADA XML data store

The ADA XML is stored in eXist-db in:
  apps/ada-data/db/demoapp/491e3a76-bf50-4e21-ac5a-67d93d498aae.xml
or any other uuid your XQuery favors at the moment. In this file, you'll find:
```xml
<adaxml>
    <meta status="new" created-by="marc" last-update-by="marc" 
      creation-date="2014-05-12T16:47:48.789+02:00" 
      last-update-date="2014-05-12T16:47:48.789+02:00"/>
    <data>
        <measurement_message app="demoapp" 
        ...
          <weight conceptId="2.16.840.1.113883.3.1937.99.62.3.2.3" 
            value="86" unit="kg"/>
          ...
          <remarks conceptId="2.16.840.1.113883.3.1937.99.62.3.2.17"/>
          ...
        </measurement_message>
    </data>
</adaxml>
```
The actual ADA XML is wrapped in an adaxml element, with:
* a `<meta>` element, with
  * @status ('new', but could be 'sent' or 'failed' etc.)
  * @created-by
  * @last-update-by
  * @creation-date
  * @last-update-date
* a `<data>` element, with the actual ADA XML message, note:
  * values and units are provided
  * concepts without values have no @value, ADA will add it upon retrieval for the XForm.
  The ADA XML storage format allows for data conversion. I.e. next to adaxml/data a HL7 transformer could create an adaxml/hl7 element with the HL7 message, etc. But that's for later.

In the eXist database, you will see:
- ada (this contains ADA backend functionality common to all ADA apps)
- ada-data 
  - backup
    - {my project} (with the data backup from the last package install)
  - db 
    - {my project} (with the actual data)
  - log (for logging)
  - projects
    - {my project} (with ADA app-specific functionality)

### ADA packages

ADA can generate what's needed to build a package with release2package.xsl. You will still need to build the package yourself, either with something like ANT, or with eXide's "Download app" utility. If ADA is installed with a package, ADA will create a backup of existing data in the backup collection.

### Troubleshooting ADA

When ADA contains data, and a new app is generated and deployed, many things can go wrong. The older data may not validate against the newer schema. There are two troubeshooting tools in ada.
  ada/projects/demoapp/modules/index-admin.xquery?app={app}
It is recommended to run index-admin always after deploying a new version of an app. It will show the status of the ADA XML in database, and through the 'Validate' Link, offer an detailed analysis if there are problems. It is also possible to inspect various REST interfaces, as well as XML and JSON. The index-admin also contains a 'Coverage' item. This checks how often concepts from the transaction's definition are used in ADA XML data. This is mainly useful when ADA is used to supply test data and test completeness needs to be established.

The other tool is the logger. Insert `</log>` in `conf.xml` in ada-data, and all calls and POSTed data are logged in ada-data/log. The `<log/>` value can be set at runtime. Turning the logging on and off requires dba rights on eXist.

All these functions can be accessed through the Dashboard:
  ada/projects/demoapp/modules/dashboard.xquery

## CodeSystem Add and Edit

### Introduction

There is no Codesystem Editor in ART-DECOR Release 2 (there is one in ART-DECOR Release 3). But despite of a missing Editor in ART-DECOR 2, you can easily use a (nerdy) workaround by facilitating Temple, the XML-aware multipurpose editor for technical experts.

If you go to the CodeSystem Tab in ART-DECOR 2, you get a list of already referenced or created CodeSystems.

Example: in the demo1 Project, there is a CodeSystem called Measured by.

![image-20220218104801927](../../img/image-20220218104801927.png)

Once a CodeSystem is selected its contents is shown on the right side.

### Editing existing non-final CodeSystems

A non-final CodeSystem can be edited by using the Temple XML-aware multipurpose editor.

![image-20220218104015288](../../img/image-20220218104015288.png)

If you want to edit a final CodeSystem, you are asked whether you want to create a new version (or an adaptation) or open it in read-only mode.

![image-20220218105521377](../../img/image-20220218105521377.png)

When opening Temple the CodeSystem is shown in its XML representation.

![image-20220218105702221](../../img/image-20220218105702221.png)

Make you changes and hit the SAVE button to save it to the Project. You can also VALIDATE the content to check before SAVE. Any save action also does validation before storing the artefact.

### Creating new CodeSystems

From Temple *build 2.3.6* on, a new Button *New codeSystem* is introduced in Temple's interface. New CodeSystems can be created by just calling the Temple Editor with this URL, as an example here for Project `demo5-`

```
{server}exist/apps/temple/views/temple.html?prefix=demo5- 
```

There is a Start Temple button planned to appear in ART-DECOR 2.

Once opened a new Code System in Temple, Copy&Paste the content below into the Temple Editor and press SAVE to create a New CodeSystem. You can change the content first but be sure that the id ends up with ***.new*** to instruct ART-DECOR to assign a new id from the Project to the new CodeSystem.

```xml
<terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <!-- Create an @id derived from a baseId to create a new codeSystem -->
    <codeSystem id="2.16.840.1.113883.3.1937.99.60.5.5.new" name="Test1" displayName="Test 1"
      effectiveDate="2022-02-18T00:00:00" statusCode="draft">
      <desc language="de-DE">This is a Test Codesystem</desc>
      <conceptList>
        <codedConcept code="A" level="0" type="L" statusCode="active">
          <designation displayName="A-Code" language="en-US" type="preferred"/>
        </codedConcept>
      </conceptList>
  </codeSystem>
</terminology>
```

A Coded Concept is the main part of a Codesystem in ART-DECOR. A Coded Concept has

- the `code` itself
- a `statusCode`, typically set to "active"
- a `level` indication
- a `type` indication, can be set to "L" leaf, "S" specializable, "A" abstract and "D" deprecated. "D" deprecated shall always be in concordance with statusCode
- a set of sub-elements `designation` with a `displayName`, `language` indicator and `type` ("preferred" or "synonym").

```XML
<codedConcept code="A" level="0" type="L" statusCode="active">
  <designation displayName="A-Code" language="en-US" type="preferred"/>
</codedConcept>
```

## DECOR Services

Identification of DECOR objects with URIs.

### Introduction

In the earlier days of ART-DECOR we developed a couple of services for direct retrieval of some of the DECOR objects like value sets, templates, datasets and transactions. This grew into a rather extensive set of services, some of which have wide spread usage, while others tend to have a low key presence. Specifically RetrieveValueSet, RetrieveTemplate, RetrieveDataSet and RetrieveTransaction are integrated deeply into the ART-DECOR 2 interface, ART-DECOR publications, and ART-DECOR Applications aka ADA. RetrieveTransaction, and RetrieveQuestionnaire are also known to be in use by various vendors for post processing outside of the scope of ART-DECOR.

#### URI logic

All query parameters have the same meaning across the different services

| Item | Description |
|---|---|
| server | http://art-decor.org/decor/services |
| separator | / |
| action | [ProjectIndex](#ProjectIndex), [RetrieveDataSet](#RetrieveDataSet), [RetrieveTransaction](#RetrieveTransaction), [RetrieveConcept](#RetrieveConcept), [RetrieveValueSet](#RetrieveValueSet), [RetrieveOID](#RetrieveOID), [RetrieveTemplate](#RetrieveTemplate) , [DataSetIndex](#DataSetIndex), [TransactionIndex](#TransactionIndex), [ValuesetIndex](#ValuesetIndex), [OIDIndex](#OIDIndex), [TerminologyReport](#TerminologyReport), [GetImage](#GetImage), [RetrieveIssue](#RetrieveIssue), [IssueIndex](#IssueIndex), [ProjectLogo](#ProjectLogo), [ValidateCode](#ValidateCode), [Template2Example](#Template2Example), [RetrieveQuestionnaire](#RetrieveQuestionnaire) *[RetrieveProject](#RetrieveProject)*<br/>The service *RetrieveProject* is not a public service because it is only relevant for users who access these services via the database directly in order to produce a publication. |
| query part and query parameters separator | ? |

| Query parameter | Description |
|---|---|
| id | The id of the concept, value set, template or dataset. For instance an OID like: 123 or 123.123.123
| code | The code of the concept.
| codeSystem | The codesystem of the concept. For instance an OID like: 123 or 123.123.123
| assigningAuthority | The responsibleAuthority/code/@code of the OID's returned must match.
| effectiveDate | The date of the object that id points to as YYYY-MM-DDTHH:mm:ss (2011-12-31T00:00:00). If no date is entered, all versions will be returned. If you use the key word 'dynamic' the latest/current version is returned.
| language | Language qualifier for contents, for instance language=en-US. When no language is entered, the content default language is used. Use language=* for "all languages"
| ui | User Interface language. When no language is entered, the server default language is used. By default English should be available.
| format | The desired return format, for instance format=xml (or: html, csv, pdf). When no format parameter is entered, the default return format is HTML. The HTML and PDF formats have no further specification and are not meant to be automatically parsed.
| name | The name of value set or template. The value set name (valueSet/@name) or OID-name (id/@name) or template name (template/@name) must be an exact match.
| prefix | The project prefix (/decor/project/@prefix) must be an exact match.
| mode | The return type. For instance 'mode=verbatim' returns the project as-is. 'mode=compiled' will resolve external references so the result is self-contained. Please note that 'compiled' is meant to be used for HTML- and Schematrongeneration. The returntype 'compiled' is unsuitable for other uses.
| download | If download=true, will trigger a download instead of showing output in the browser.
| hidecolumns | Which columns should be hidden in the returned view. For instance 'hidecolumns=53' will hide columns 3 and 5. After 9, continue in hexadecimal (a, b, c, etc.). Is only applicable for HTML views. See [Hidecolumns mapping](#Hidecolumns_mapping).

### Examples

- https://art-decor.org/decor/services/RetrieveValueSet?id=1.2.40.0.34.10.65&effectiveDate=2015-09-01T00:00:00&prefix=elga-&format=xml&language=de-DE<br/>Will return a value set with a specific date in the return format XML
- https://art-decor.org/decor/services/RetrieveValueSet?id=1.2.40.0.34.10.65&effectiveDate=2013-01-10T00:00:00&prefix=elga-&format=csv<br/>Will return a value set with a specific date in the return format CSV
- https://art-decor.org/decor/services/RetrieveValueSet?id=1.2.40.0.34.10.65&prefix=elga-&format=csv<br/>Will return the latest/current version of a value set in the return format CSV
- https://art-decor.org/decor/services/RetrieveValueSet?id=1.2.40.0.34.10.65&prefix=elga-<br/>Will return all versions of a value set in the default return format (HTML)
- https://art-decor.org/decor/services/RetrieveValueSet?id=1.2.40.0.34.10.65&effectiveDate=2015-09-01T00:00:00&prefix=elga-&format=json&language=de-DE<br/>Will return a value set with a specific date in the in the return format JSON
- https://art-decor.org/decor/services/RetrieveValueSet?id=1.2.40.0.34.10.65&effectiveDate=2015-09-01T00:00:00&prefix=elga-&format=sql&language=de-DE<br/>Will return a value set with a specific date in the in the return format SQL
- https://art-decor.org/decor/services/RetrieveValueSet?id=1.2.40.0.34.10.65&language=en-US<br/>{for future use} will return all versions of a valueset in the default return format (HTML) in American English
- https://art-decor.org/decor/services/RetrieveCode?code=M&codeSystem=2.16.840.1.113883.5.1<br/>{for future use} will return the concept 'Male' of HL7 AdministrativeGender
- https://art-decor.org/decor/services/ValueSetIndex<br/>Will return a HTML table with formats and versions for all valuesets in DECOR projects, with links to the valuesets.
- https://art-decor.org/decor/services/DataSetIndex<br/>will return a HTML table with formats and versions for all datasets in DECOR projects, with links to the datasets.
- https://art-decor.org/decor/services/GetImage?prefix=demo-<br/>will return two SVG diagrams for each transactiongroup in the demo DECOR project. One diagram shows the functional perspective and the other shows the technical perspective.
- https://art-decor.org/decor/services/GetImage?prefix=demo-&id=2.16.840.1.113883.2.4.3.46.99.3.4.1<br/>will return two SVG diagrams for a specific transactiongroup with id '2.16.840.1.113883.2.4.3.46.99.3.4.1' in the demo DECOR project. One diagram shows the functional perspective and the other shows the technical perspective.
- https://art-decor.org/decor/services/OIDIndex<br/>will return a HTML table with an overview for all OIDs from the OID-registry, with links to a detailed view for each OID.
- https://art-decor.org/decor/services/OIDIndex?id=1.0.3166.1.2.2<br/>will return a HTML table with an overview for the OID with id '1.0.3166.1.2.2' from the OID-registry, with a link to a detailed view for this OID.
- https://art-decor.org/decor/services/OIDIndex?id=1.0.3166.1.2.2&language=de-DE<br/>will return a HTML table with an overview for the OID with id '1.0.3166.1.2.2' from the OID-registry, with a link to a detailed view for this OID in German.
- https://art-decor.org/decor/services/RetrieveOID?id=1.0.3166.1.2.2&format=html<br/>will return a HTML table with a detailed view for the OID with id '1.0.3166.1.2.2' from the OID-registry, with links to a XML view for this OID.
- https://art-decor.org/decor/services/RetrieveOID?id=1.0.3166.1.2.2&format=html&language=de-DE<br/>will return a HTML table with a detailed view in German for the OID with id '1.0.3166.1.2.2' from the OID-registry, with links to a XML view for this OID.
- https://art-decor.org/decor/services/TransactionIndex?format=html&language=en-US<br/>will return a HTML table with all transactions in American English
- https://art-decor.org/decor/services/TransactionIndex?prefix=demo3-&format=html&language=en-US<br/>will return a HTML table with all transactions from the DECOR project demo3- in American English
- https://art-decor.org/decor/services/RetrieveTransaction?id=2.16.840.1.113883.3.1937.99.60.3.4.2&format=html&language=en-US<br/>will return a HTML table for the transaction with id 2.16.840.1.113883.3.1937.99.60.3.4.2 (Electrocardiogram Report) in American English
- https://art-decor.org/decor/services/RetrieveTemplate?id=2.16.840.1.113883.3.1937.99.60.3.10.3001&prefix=demo3-&format=xml<br/>will return a original template in raw format with id 2.16.840.1.113883.3.1937.99.60.3.10.3001 (Electrocardiogram Report) in project demo3
- https://art-decor.org/decor/services/Template2XSL?id=2.16.840.1.113883.3.1937.99.60.3.10.3001&prefix=demo3-&format=html<br/>will return a HTML overview of generated XSLTs based on template id 2.16.840.1.113883.3.1937.99.60.3.10.3001 in project demo3
- https://art-decor.org/decor/services/Template2XSL?id=2.16.840.1.113883.3.1937.99.60.3.10.3001&prefix=demo3-&format=xsl<br/>will return the generated XSLT for template 2.16.840.1.113883.3.1937.99.60.3.10.3001 in project demo3
- https://art-decor.org/decor/services/RetrieveConcept?conceptId=2.16.840.1.113883.3.1937.99.62.3.2.3&conceptEffectiveDate=2011-01-28T00:00:00&language=en-US will return info for the concept Weight inside the dataset of the demo1- project
- https://art-decor.org/decor/services/Template2Example?id=2.16.840.1.113883.3.1937.99.62.3.10.11&effectiveDate=2012-03-11T00:00:00&prefix=demo1- will build an example based on the template id and effectiveDate supplied stoppage at references to other templates (@contains and include)
- https://art-decor.org/decor/services/Template2Example?id=2.16.840.1.113883.3.1937.99.62.3.10.11&effectiveDate=2012-03-11T00:00:00&prefix=demo1-&doRecursive=true will build an example based on the template id and effectiveDate supplied traversing all references to other templates (until recursion is found or no more references are left to follow)
- https://art-decor.org/decor/services/RetrieveQuestionnaire?id=2.16.840.1.113883.3.1937.99.62.3.4.2&effectiveDate=2012-09-05T16:59:35&language=en-US&ui=nl-NL&format=html will render this transaction through [https://lhcforms.nlm.nih.gov LHCForms]
|}

### Implementation status of the query parameters

Each query parameter marked with ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) is implemented at this moment.
Each query parameter marked with *N/A* is not applicable, i.e. not implemented.

| Service Name | Parameter | Description | Status |
|---|---|---|---|
| <span id="ProjectIndex">ProjectIndex</span> | format | Return format. Options: 'html' (default), 'xml'. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
|| prefix | Project prefix - if empty and format is html does all repositories. If empty and format is xml does all project metadata | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
|| version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
|| language | Content language - format ll-CC. Recommended with param version. Any '*' without quotes for 'any language' | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
|| ui | User Interface language - format ll-CC when format is HTML | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
|| view | Options: 'd' Limits index to datasets, 't' limits view to transactions, 'v' limits index to value sets, 'r' limits index to templates (rules). | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="DataSetIndex"> DataSetIndex </span> | | Shortcut to Projectindex?view=d | |
| <span id="TransactionIndex">TransactionIndex</span> | | Shortcut to Projectindex?view=t | |
| <span id="ValueSetIndex">ValueSetIndex</span> | | Shortcut to Projectindex?view=v | |
| <span id="TemplateIndex">TemplateIndex</span> | | Shortcut to Projectindex?view=r | |
| <span id="OIDIndex">OIDIndex</span> | prefix | Registry prefix - if empty does all registries. Note: was parameter `registry` (still exists but deprecated) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | Content language - format ll-CC. Recommended with param version. Any '*' without quotes for 'any language' |optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | ui | User Interface language - format ll-CC when format is HTML |optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | OID to filter list on | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | name | OID name to filter list on | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | assigningAuthority | OID assigning authority to filter list on | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | OID effective date to filter list on | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveConcept">RetrieveConcept</span> | | Shortcut to RetrieveTransaction where param `id` is optional may be either a dataset or a transaction, and param `conceptId` shall have a value. If param `id` does not have a value, then the dataset that holds the concept is assumed. See RetrieveTransaction for other parameters | |
| <span id="RetrieveDataSet">RetrieveDataset</span> | | Shortcut to RetrieveTransaction where param `id` shall be a dataset. See RetrieveTransaction for other parameters | |
| <span id="RetrieveTransaction">RetrieveTransaction</span> | format | Return format. Options: 'html' (default), 'xml', 'hlist' (hierarchical list) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | prefix | Project prefix. A dataset/transaction/concept is assumed to be in exactly one project, so param `prefix` is not necessary | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | Content language - format ll-CC. Recommended with param version. Any '*' without quotes for 'any language' | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) 
| | ui | User Interface language - format ll-CC when format is HTML | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | Dataset/Transaction id - format is OID. Param `id` and/or `conceptId` is required. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) 
| | effectiveDate | Dataset/Transaction effectiveDate -  yyyy-mm-ttThh:mm:ss. Only with param `id`. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | conceptId | Concept id - format is OID. Param `id` and/or `conceptId` is required. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | conceptEffectiveDate | Concept effectiveDate -  yyyy-mm-ttThh:mm:ss. Only with param `conceptId`. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | community | Community prefix. This parameter may repeat. Can only include communities with guest access enabled | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | unfiltered | Options 'true' or 'false' (default). If true, shows any deprecated/cancelled/rejected concepts that may be in the dataset. If absent or 'false', those are hidden. Is always 'true' i.e. irrelevant for transactions. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | collapsed | Options 'true' (default) or 'false'. If true, groups start collapsed. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | draggable | Options 'true' (default) or 'false'. If false, dragging of columns is disabled. Since draggable columns can't be selected with the mouse, useful if one wants to copy data. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | download | Options 'true' (default) or 'false'. Triggers a download. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | hidecolumns | Alphanumeric indicator of columns that should be hidden upon launch. Default set is ''. Options: see below | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveConceptDiagram"> RetrieveConceptDiagram </span> | format | Return format. Options: 'svg' (default) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | Project release language - format ll-CC. Recommended with param version. Also UI language with format HTML | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | datasetId | Format is OID. Need param transactionId or datasetId | conditional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | datasetEffectiveDate | Format is yyyy-mm-ddThh:mm:ss. Only relevant with param `datasetId` | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | transactionId | Format is OID. Need param transactionId or datasetId | conditional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | transactionEffectiveDate | Format is yyyy-mm-ddThh:mm:ss. Only relevant with param `transactionId` | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | Concept id. Format is OID. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | Concept effective date. Format is yyyy-mm-ddThh:mm:ss. Only relevant with param `id` | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | interactive | Options 'true' (default) or 'false'. Triggers the generation of javascript (onClick) that allows interactive graphics and that should be set to false if the crated sag is about to be uploaded into a CMS where interactive graphics are not allowed. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveValueSet">RetrieveValueSet</span> | format | Return format. Options: 'html' (default), 'xml' (ART-DECOR), 'csv', 'svs' (IHE SVS 2) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | inline | Relevant only with format=html. Options 'true' and 'false' (default). Omits HTML header info. Useful for inclusion of HTML in other pages. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | prefix | Project prefix. Doesn't return anything without prefix. | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | Project release language - format ll-CC. Recommended with param version. Also UI language with format HTML | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | Value set id - format is OID. Only with param prefix. When omitted checks param name | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | name | Value set name - format is string. Only with param prefix. Only checked when id is omitted. When both id and name are omitted checks param ref | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | ref | Value set id or name - format is OID or string. Only with param prefix. Only checked when both id and name are omitted. When id, name and ref are omitted all project value sets are returned. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | Value set effectiveDate -  yyyy-mm-ttThh:mm:ss. Only with param `id`, `name`, or `ref`. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveTemplate">RetrieveTemplate</span> | format | Return format. Options: 'html' (default), 'xml' (pure raw template (versions) wrapped in &lt;rules/&gt; element , 'expandedxml' (expanded template (versions) wrapped in &lt;rules/&gt; element, expands includes, associations etc.), 'xmlnowrapper' (pure first matching raw template) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | inline | Relevant only with format=html. Options 'true' and 'false' (default). Omits HTML header info. Useful for inclusion of HTML in other pages. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | prefix | Project prefix | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | Project release language - format ll-CC. Recommended with param version. Also UI language with format HTML | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | Template id - format is OID. Only with param prefix | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | Template effectiveDate -  yyyy-mm-ttThh:mm:ss. Only with param `id`. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) 
| <span id="RetrieveTemplateDiagram">RetrieveTemplateDiagram</span> | format | Return format. Options: 'svg' (default), 'xml' (mostly useful for debug), 'hgraph' (hierarchical graph) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | prefix | Project prefix | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | *not implemented*
| | language | Project release language - format ll-CC. Recommended with param version. Also UI language with format HTML | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | Template id - format is OID. Only with param prefix | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | Template effectiveDate -  yyyy-mm-ttThh:mm:ss. Only with param `id`. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="Template2Example">Template2Example</span> | prefix | Project prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | Template id - format is OID. Only with param prefix, and when no template is POSTed to the service | conditional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | Template effectiveDate -  yyyy-mm-ttThh:mm:ss. Only with param `id`. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | elementId | Template element @id - Will trigger the example to be generated from that element onwards | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | serialized | boolean. Default true when template is POSTed. Default false otherwise. Returns content of `<example/>` as serialized XML if true, or as XML otherwise | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | selectedOnly | boolean. Default true when template is POSTed. Default false otherwise. Process only particles marked @selected="" on the incoming template. Imported when POSTed fvrom the visual template-editor where some particles may have been unselected | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | recursive | boolean. Default false. Process only particles in current template is false and do not proces @contains/include, or recursively follow all references to other templates until that runs into a loop or no more references are left to follow | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveOID">RetrieveOID</span> | format | Return format. Options: 'html' (default), 'xml' | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | prefix | Registry prefix - if empty does all registries. Note: was parameter `registry` (still exists but deprecated) | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | UI language with param format=html - format ll-CC. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | OID | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | name | OID name/description | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | statusCode | OID status code | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveIssue">RetrieveIssue</span> | | Currently returns XML only and does not have an interface | |
| | id | OID of the issue | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="IssueIndex">IssueIndex</span> | | Currently returns XML only and does not have an interface | |
| | prefix | Project prefix. Note: prefix was added in 1.6.11 to align with the other services. Parameter project remains supported. | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | searchString | List of terms that results should match in issue/@displayName or any desc element. Results all if empty | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | type | List of issue/@type values that results should have. Returns all if empty | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | priority | List of issue/@priority values that results should have. Returns all if empty | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | statusCode | List of issue most recent status code values that results should have. Returns all if empty | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | assignedTo | List of project/author/@id values that the latest assigned person of the issue should match. Returns all if empty | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | labels | List of issue most recent label code values that results should have. Returns all if empty | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | sort | Singular value to sort on. Supported are 'issue' (display name),'priority','type','status','date','assigned-to','label'. There's no default. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | max | Maximum number of results with minimum 1. Default is 75 | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="ProjectLogo">ProjectLogo</span><br/>*(since DECOR services 1.6.6)* | prefix | Project prefix. | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | logo | Project logo. If not provided will return the project/reference/@logo or if even that is empty will return the server default logo. You may specify any logo name such as a copyright logo. Any logo is expected to come from the collection prefix-logos which is also the place where you upload logos through the ART project form. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | mode | Options: '' (default - retrieves the requested logo), 'list' (lists available logos) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveProject">*RetrieveProject*</span> | format | Return format. Options: 'html' (default when param `prefix` is omitted), 'xml' (default in all other cases) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | prefix | Project prefix - if empty shows HTML form that guides you. | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | Project release language - format ll-CC. Recommended with param version. Also UI language with format HTML | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | mode | Options: 'verbatim' (default - raw project file), 'compiled' (resolves references and inherits, adds id info, suitable for publication purposes), 'test' (undocumented), 'cachemeta' (undocumented, internal use only), 'ada-definition' (creates an [[ADA_Documentation|ADA Definition file]] suitable as starting point for creating ADA Releases.) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | force | Options: 'true', 'false' (default). Relevant only when mode is 'compiled' and on the live version. If true and working on the live version i.e. not on a release, forces recompile if one should already exist. If false or working on a release, compiles when needed or returns the compiled release | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | ignoreFilter | Options: 'true' (default), 'false'. Relevant only when mode is 'compiled' and on the live version. Use current compile filter settings if any (false) or ignore any filter settings and do a full compile (true). | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | download | Options: 'true', 'false' (default) | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="ValidateCode">ValidateCode</span><br/>*(since DECOR services 1.8.27)* | prefix | Project prefix. | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | ValueSet id - format is OID. | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | ValueSet effectiveDate -  yyyy-mm-ttThh:mm:ss. Only with param `id`. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | code | code to check | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | codeSystem | codeSystem to check | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| <span id="RetrieveQuestionnaire">RetrieveQuestionnaire</span> | format | Return format. Options: 'json', 'html', 'xml' (default). | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | id | Content transaction id to convert | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | effectiveDate | Content transaction effectiveDate to convert.yyyy-mm-ttThh:mm:ss. Only with param `id` | required ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | version | Project release version - format yyyy-mm-ttThh:mm:ss. Only with param prefix | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | language | Content language - format ll-CC. Recommended with param version. Any '*' without quotes for 'any language' | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| | fhirLinkItemStyle | If set to "id" linkItem value is the id, e.g. "2.16.840.1.113883.3.1937.99.62.3.2.2--2011-01-28T00:00:00", if set to "oid" the OID is shown only, e.g. "2.16.840.1.113883.3.1937.99.62.3.2.2", otherwise the id display name is shown if available, if not and the default is "id. | optional ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)

### Implementation status of the return formats

Each format marked with ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) is implemented at this moment.

| | Retrieve Dataset | Retrieve Transaction | Retrieve ValueSet | Retrieve Code | Retrieve OID | Retrieve Concept | Retrieve Project | Retrieve Template |
|---|---|---|---|---|---|---|---|
| XML | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| CSV | no | no | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | no | no | no | no | no
| HTML | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | no | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg)
| SQL | no | no | ![image-20231108235959-conformance](../../img/image-20231108235959-conformance.svg) | no | no | no | no | no
| PDF | for future use | possibly | possibly | no | no | no | no | no

All *Index resources are only returned in HTML format.

<a name="Hidecolumns_mapping"></a>

### Hidecolumns mapping

The query parameter hidecolumns can contain numbers and letters (the order is not significant) which represent the columns in the returned view, with the following mapping:


| Column to hide | hidecolumn contains |
|---|---|
| Name | N/A
| ID | 2
| Mandatory | 3
| Conformance (transaction) | 4
| Cardinality (transaction) | 5
| Max (transaction) | 6 
| Cardinality/conformance/datatype column (convenience single column) (transaction) | 0
| Datatype | 7
| Unit | 8
| Example | 9
| Codes | a
| Description | b
| Source | c
| Rationale | d
| Operationalization | e
| Comment | f
| Condition (transaction) | g
| Status column |  h
| Community column | i
| Terminology column | j
| Value set column | k
| Type column (group / item) | l
| Parent column | m
| Inherit column | n
| Mapping | o
| Context | p

### Error handling

When a resource does not exist (either the queried format does not exist, or the resource does not exist in that language, etc) a HTTP 404 error is returned.

### FHIR URIs

For FHIR URIs, see: [FHIR_URIs](https://wiki.art-decor.org/index.php?title=FHIR_URIs)
