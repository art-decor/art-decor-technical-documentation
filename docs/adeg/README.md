---
pageClass: custom-page-class
permalink: /adeg/
---

# Who we are!

## Founders

### Kai Heitmann <img src="../img/100px-Kai.jpg" alt="Kai Heitmann" style="float:right;" />
DECOR is developed by **Dr Kai Heitmann** who works as an healthcare IT consultant in Europe with the main focus on Germany and the Netherlands. He is mainly active in the fields of standardization in healthcare communication, especially the HL7 (Health Level Seven) family of standards like CDA and FHIR.

Kai is an acknowledged expert in Healthcare Information Technology; he has advised the Ministries of Health of some European countries, as well as large trade associations in Germany and the Netherlands, on aspects of e-Health.

### Gerrit Boers <img src="../img/100px-Gerrit.jpg" alt="Gerrit Boers" style="float:right;" />
ART is developed by **Gerrit Boers** who is a Netherlands-based health care consultant. His expertise in terminology applications is a major contribution to ART-DECOR's terminology services and browsers.



## ART-DECOR® Open Tools

Kai and Gerrit are the CEOs of [ART-DECOR® Open Tools](https://art-decor-open-tools.net). Both are also part of the ART-DECOR® Expert Group ADEG.

## ART-DECOR Expert Group

Since the start of the original development of ART-DECOR back in 2008, the ART-DECOR Expert Group is continuously growing. People with different skills joined the group to take care of specific topics, give advice or actually help build ART-DECOR. 

The ART-DECOR Expert Group is comprised of various experts.

<img src="../img/100px-Alexander-2246110.jpg" alt="Alexander Henket" style="float:right" />**Alexander Henket** is a consultant at Nictiz with more than15 years of experience in the creation and implementation of various standards and based in The Netherlands. He is an active member of the HL7 community both national and international. 
<br clear="all"/>

<img src="../img/100px-Maarten.jpg" alt="Maarten Ligtvoet" style="float:right;" />**Maarten Ligtvoet** is an architect at Nictiz who specializes in network infrastructure, audits of health information systems, open standards, authentication & security and based in The Netherlands.
<br clear="all"/>

<img src="../img/100px-Christopher.jpg" alt="Christopher Groß" style="float:right;" />**Christopher Groß** is a Germany-based Full Stack Developer with a focus on new UI frameworks, especially Vue and Vuetify, the technological frontend corner stone of ART-DECOR 3. 
<br clear="all"/>

<img src="../img/image-20221117151005075.png" alt="image-20221117151005075" style="zoom:50%; float:right;" />**Rogier Wiertz** is based in Sweden and is a Full Stack Web Developer and mainly contributes to our ART-DECOR 3 frontend. 

<br clear="all"/>

<img src="../img/100px-Patrick.jpg" alt="Patrick Werner" style="float:right;" />**Patrick Werner** is a FHIR focused health IT expert and hapi FHIR java library contributor who focuses on Questionnaires, Conformance and FHIR Validation. He is based in Germany and works for the MOLIT Institut Heilbronn and is CEO of the echinos UG.
<br clear="all"/>

<img src="../img/100px-Marc-2245453.jpg" alt="Marc de Graauw" style="float:right;" />**Marc de Graauw** is an independent consultant who specializes in digital information exchange, health & ICT, information architecture, security, semantics & Web Services and based in The Netherlands. 

<br clear="all"/>

<img src="../img/100px-Laurence-20210921194211830-2246133-2246158.jpg" alt="Laurence Strasser" style="float:right;" />**Laurence Strasser** studied Medical Computer Science in Vienna and worked with ART-DECOR for his Bachelor’s and Master’s thesis. During these projects he also gained profound experience with HL7 CDA, HL7 FHIR and other standards. He is based in Austria. 

<br clear="all"/>

<img src="../img/100px-Frank-2245872.jpg" alt="Dr. Frank Oemig" style="float:right;" />**Dr Frank Oemig** works for Deutsche Telekom Healthcare and Security Solutions GmbH and focuses on Conformance and Testing  as well as on Java applications around ART-DECOR. He is based in Germany. 
<br clear="all"/>

<img src="../img/100px-Mathias.jpg" alt="Mathias Aschhoff" style="float:right;" />**Mathias Aschhoff** works for The Competence Centre for Telematics in HealthCare (ZTG) and is a HL7 CDA and JAVA expert focusing on the intelligent diff strategies for ART-DECOR artefacts such as like value sets and templates. He is based in Germany. 

<br clear="all"/>

**Jolissa Latour** is based in the Netherlands and is a frontend developer at Nictiz, she contributes to our ART-DECOR 3 frontend. 

<br clear="all"/>

<img src="../img/100px-AnneMarie.jpg" alt="Anne-Marie van den Boogaard" style="float:right;" />**Anne-Marie van den Boogaard** is based in the Netherlands and is a backend developer and information analyst. She works at Nictiz and contributes to our ART-DECOR 3 backend. 

<br clear="all"/>

Part of the team are also two experts from [eXist Solutions GmbH](http://existsolutions.com/), **Juri Leino**, Tech Lead, and **Jörn Turner**, Java Enterprise Solutions Expert, bringing in their thorough expertise in the eXist database, the backend hart of ART-DECOR since version 1.

## ART-DECOR contributors

We are lucky to have many persons contributing to ART or DECOR to make it better or to request new features. This is a list of a few of them:

- André Achterberg, AA ICT Consultancy
- Mathias Aschhoff, ZTG GmbH, Bochum
- Jos Baptist, Nictiz (Terminology)
- Sebastian Bojanowski, iEHR.eu
- Abderrazek Boufahja, IHE Europe Development, Gazelle Team
- Dr. Dominik Brammen, University of Magdeburg, Germany
- Giorgio Cangioli, HL7 International Foundation
- Didi Davis, Director, Testing Programs, The Sequoia Project®
- Heike Dewenter, University of Applied Science Niederrhein, Germany
- Oliver Egger, agile health data information systems
- André Huisman, MedicalPHIT
- Claartje Hülsman, Nictiz
- Maarten Jansonius, Tieto
- Tom de Jong, Nova Pro Consultancy/VZVZ
- François Macary, PHAST
- Hans Mekenkamp, MedicalPHIT
- Tilman Mueller, Topicus
- Lisa Nelson
- Eric Poiseau, IHE Europe
- Roman Radomski, iEHR.eu
- Stefan Sabutsch, ELGA
- Tony Schaller, medshare GmbH
- Tessa van Stijn, Nictiz
- Michael Tan, Nictiz
- Carsten Thiell, QiN
- Prof. Dr. Sylvia Thun, University of Applied Science Niederrhein, Germany (Terminology)
- Arianne van de Wetering, Nictiz
- Jens Villadsen, Principal Solutions Architect at DXC