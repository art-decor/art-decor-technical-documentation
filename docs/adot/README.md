---
permalink: /adot
---
# What is ART-DECOR® Open Tools?

ART-DECOR® Open Tools has been established as a company with limited liability  (German GmbH). The company handles commercial aspects of the ART-DECOR® tool suite development and thus complements the ART-DECOR® Expert Group that drives the development. 

For more details, please visit the website of [ART-DECOR®](https://art-decor.org/).

## Support Plans

ART-DECOR® Open Tools offers and handles support plans for both, ART-DECOR® hosted servers and for foreign hosted servers. For information, please visit the website of [ART-DECOR® Open Tools](https://art-decor.org/about-open-tools/).

## Project hosting

We offer free project hosting on ART-DECOR (without support). Feel free to Contact us if you are interested. If you are interested in running and hosting your own ART-DECOR® servers with support, contact ART-DECOR® Open Tools.
