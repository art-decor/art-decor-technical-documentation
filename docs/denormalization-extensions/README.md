---
permalink: /denormalization-extensions
---
# Denormalization Extensions

## Introduction

For maximum of speed and performance when searching through large sets of data, ART-DECOR knows a set of extensions to normalized structures that empowers search engines. Examples are

- `browsableCodeSystem` and the associated `codedConcept` typed *codedConceptBrowsable*.

## Methodology

The Extensions are all marked approriately in the XSD schema (DECOR.xsd) so that each definition identifies itself as a denormalized attribute. This is done by the W3C XSD schema construct `xs:annotation`, with a nested `xs:appinfo` element.

Example

```XML
<xs:annotation>
  <xs:appinfo source="https://docs.art-decor.org/denormalization-extensions">
    <extension>LOINC</extension>
  </xs:appinfo>
</xs:annotation>
```

This essentially mean an additional application information, here info about the ART-DECOR denormalization extensions. The URL in `@source` links to this page. The `extension` element points to the Extension Type Code, its presence denotes the element and all subsequent elements as denormalization extensions.

## List of Extension Types

| Extension Type Code | Description                                                  | Since |
| ------------------- | ------------------------------------------------------------ | ----- |
| LOINC               | An extension used to denormalize LOINC additional properties for quick search; can be retransformed into Code System properties | 2018  |
| SNOMED              | An extension used to denormalize SNOMED-CT additional properties for quick search; can be re-transformed into Code System properties | 2018  |
| SPED                | This denotes a speed denormalization extension, most often used, can safely be ignored upon transformed into a normal Code System | 2018  |

