---
permalink: /features
---
# The ART-DECOR® Feature Ensemble

ART-DECOR as a collaboration platform for experts from medical, terminological and technical domains is aiming on creation and maintenance of **Datasets** with data element descriptions, use case **Scenarios**, **Terminologies** and data formats such as HL7 Templates or FHIR Profiles, summarized in the tools as **Rules**. ART-DECOR allows to iteratively improve recorded data definitions, e.g. with change management through **Issue** tickets. The artefacts can be linked together with input from various experts with different background knowledge: caregivers, terminologist, modellers, analysts and interface/communication specialists. Mature artefatcs can be re-used from official **Repositories**, finally and in return, mature artefacts from projects can be published in Repositories. **Publications** formally freezes a certain status of the set of artefacts, summarizing the status of the specification.

![image-20220629192114242](../img/image-20220629192114242.png)

All artefacts in ART-DECOR are logically embraced in so-called Projects (see below). A Project typically addresses a set of Use Cases.

## Caregivers  <adimg dataset/> <adimg scenario/>

<img src="../img/image-20220629191213784.png" alt="image-20220629191213784" style="zoom:50%; float:right;" />For caregivers, the tools mainly registers data elements and their collections as data sets. It allows the documentation of concepts, optionally arranged in groups. For Concept Items data types, properties such as allowable value ranges, identifications, codes, rationales, operationalizations, examples and business rules etc. can be specified. Publicly available concepts can be re-used by inheritance.

In addition, use case specific Scenario's can be documented, summarized in Transactions, utilizing to Actors. A Transaction also reports dataset elements to be used in the specific transaction, assign cardinalities, conformance and conditions. Also, Questionnaires can be derived from Transactions, ART-DECOR Release 3 allows Questionnaire Maintenance and Prototyping Questionnaires and their Responses.

## Terminologists <adimg terminology/> <adimg identifier/>
<img src="../img/image-20220629191256212.png" alt="image-20220629191256212" style="zoom:50%; float: right;" />From the base requirements the caregivers documented, terminologist can add the appropriate Codes, associate caregiver concepts with Coded Concepts and create and maintain Value Sets and propriertary Code Systems to be used.

Additionally, Concept Maps / Associations can be created, e.g. from Dataset Elements to Coded Concepts or Choice Lists to Value Sets and Choice Lists Elements to Value Sets member codes.

Typically also identifiers, namespaces, URIs and OIDs are assigned. ART-DECOR also offers an OID-registry that allows to hold OIDs and URIs for artefacts inside or outside a project.

## Informaticians <adimg rules/> <adimg template/> <adimg profile/>
<img src="../img/image-20220629191654589.png" alt="image-20220629191654589" style="zoom:50%; float: right;" />Modelers, analysts and interface / communication specialists add the technical representations as data formats, e.g. as HL7 CDA Templates, HL7 FHIR profiles or IHE profiles. They can superimpose additional Conditions or Constraints to Structures or design extensions to be included in the resulting specifications.

During that design phase and also in the following implementation and production phases the tool allows comprehensive validation and testing.

The overall goal is an Implementation Guide that is part of (cyclic) Publications.

## Project leads <adimg issue/>
<img src="../img/image-20220629191828549.png" alt="image-20220629191828549" style="zoom:50%; float: right;" />During development and production phases stakeholders and team members of the governance group are supported by an artefact-aware issue and reporting management that enables effective change management of the artefacts. Proper documentation of all issues, questions or suggestions including their assignements to team members and trackings, help to keep track of all mutations of the specifications.

A semi-automatic summary method offers all changes recorded as issues in the phase of creating Release Notes to be taken over.

## Vendors and Interface Specialists <adimg implementation/> <adimg publication/>
<img src="../img/image-20220629191923243.png" alt="image-20220629191923243" style="zoom:50%; float: right;" />Once a specification is ready, a Publication is triggered that formally freezes a certain status of the set of artefacts, summarizing the status of the specification. Typical media used so far are Mediawiki, Confluence or WordPress environments, PDF or HTML publications or Implementation Guide packages.

Vendors and Interface Specialists can start Implementation. For that purpose ART-DECOR offers support to understand, implement, validate and test the build using the tool-site built-in validation mechanisms or download the validation packages to do testing in local environments. 

## A Strong Asset: Re-Use through Repositories <adimg repository/> <adimg cache/>

<img src="../img/image-20220629191941092.png" alt="image-20220629191941092" style="zoom:50%; float: right;" />Project Authors or Governance Groups may decide that mature artefatcs may be re-used by others from official **Repositories**. In fact Project Authors finally and in return, may published their own mature artefacts in a publicly available Repository.

For performance reasons, repositories are cached on a server. The caching mechanism (in ART-DECOR Release 2) approaches each and every registered repository in the projects and periodically updates its cache upon changes. In ART-DECOR Release 3 the ART-DECOR's Cloud Server does this job, maintains changes of the sources properly and exposes changes only to the caching mechanism of any server. 

## Project <adimg project/>

As already mentioned, all artefacts in ART-DECOR are logically embraced in so-called Projects. A Project is managed by Project Authors who are maintaining the specifications and run th project management. A Project typically addresses a set of Use Cases.

<img src="../img/image-20220709103410694.png" alt="image-20220709103410694" style="zoom:50%;" />

A Project may belong to one or more Governance Groups.

## Governance Groups <adimg governancegroups/>

Designers of datasets, scenarios, templates, value sets and other artifacts often create their designs within the context of a community. Often these communities are involved with standards creation, like HL7 and IHE, but they don’t have to be. 

In ART-DECOR, these communities is called a *Governance Group*. A Governance Group establishes the rules and best practices associated with their creation and management of artifacts in ART-DECOR. The group establishes practices that are used to manage the intellectual property rights associated with the artifacts they create and use, and provide for governance functions. Some Governance Groups develop artifacts with the intent to register and share their designs. Others intent to maintain their template definitions as private works. 

A Governance Group is the custodian of a set of artifacts and can act in any of the three (stereotype) roles: Creator, Adopter, and Adapter. They use ART-DECOR as a Repository to hold the artifacts designs it creates and uses, residing in one or more Projects.

<img src="../img/image-20220709104013151.png" alt="image-20220709104013151" style="zoom: 40%;" />

It maintains the designs it develops and uses, and tracks their status and relationships. It set the policies and procedures it will follow when using, adding, and addressing the life cycle of each design under its governance.

::: note

A Governance Group can be a legal entity but it can also be a community of several entities with a commen set of rules aiming on creation and management of artifacts.

::: 

There are several views and dashboards (see example) available for Governance Groups to fulfill their needs.

<img src="../img/image-20230202103409826.png" alt="image-20230202103409826" style="zoom:67%;" />
