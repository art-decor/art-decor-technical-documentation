---
permalink: /copyright
---

# ART-DECOR® – Copyright

::: note Trademark

ART-DECOR® is a registered trademark. 

:::

| Copyright |                                                              |                                                            |
| --------- | ------------------------------------------------------------ | ---------------------------------------------------------- |
| 2013-2022 | ART-DECOR Expert Group<br />art-decor.org<br />... [more](https://docs.art-decor.org/whoweare/#founders) | <img src="../img/800px-Adeg-logo.png" style="zoom:30%;" /> |
| 2016-2022 | ART-DECOR Open Tools<br />art-decor-open-tools.net <br />... [more](https://docs.art-decor.org/adot/) | <img src="../img/800px-Adot-logo.png" style="zoom:30%;" /> |

::: note ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE

Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH<br/>
see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

This file is part of the ART-DECOR® tools suite.

ART-DECOR® is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ART-DECOR® is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

:::
