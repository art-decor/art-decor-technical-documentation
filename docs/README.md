---
home: true
heroImage: /topmenu-ad-logo.png
heroText: ART-DECOR®
permalink: /
tagline: Technical Documentation
actions:
  - text: Introductions →
    link: /introduction/
    type: secondary
  - text: User Manual →
    link: /documentation/
    type: primary
features:
- title: Introduction
  details: Information and backgrounds about the open-source collaboration tool suite and the various supported artefacts.
  link: /introduction/
- title: Documentation
  details: Detailed description on how to use ART-DECOR® and its features and artefacts.
  link: /documentation/
- title: Administration
  details: Information for you if are in charge of administering ART-DECOR® or you need to manage a server.
  link: /administration/
footerHtml: true
footer: CC BY 4.0 Licensed | Copyright © 2009-2024 <a href="https://art-decor-open-tools.net">ART-DECOR Open Tools GmbH (ADOT)</a>
---
::: note Welcome to ART-DECOR®

ART-DECOR® – the open-source tool suite that aims at interoperability solutions in the healthcare sector and that fosters collaboration between all stakeholders involved in the process of **making real interoperabiltity come true**.

This is the Technical Documentation Version <version />
:::
