---
permalink: /documentation/issue/
---

# Issue

There are two panels that are related to [Issues](/introduction/issue):

- The [Issue Panel](#the-issue-panel)
- The [Label Panel](#the-label-panel)

Users with sufficient permissions can [create new Issues](#create-a-new-issue) from every panel of the Project.

## The Issue Panel

![image-20220624152043219](../../img/image-20220624152043219.png)

The Issue Panel lists all issues that are created for the project <ref1/>.

The search results can be filtered by configuring the search settings <ref2/>.

Clicking on the Issue title <ref3/> will navigate to a page with detailed information on the Issue.

### Issue Details

The Issue Details page offers more details on the issue such as the title, status, priority, type <ref1/> and the tracking of comments <ref2/>.

![image-20220624152506009](../../img/image-20220624152506009.png)

The Concerns Chip <ref3/>opens up a dialog with more details on the artefact the issue is related to. 

<img src="../../img/image-20230224152104975.png" alt="image-20230224152104975" style="zoom:30%;" />

The Subscribe switch <ref4/> (above) can be set to get notifications about the issue in scope.

The Issue Details are editable. For example, Labels can be specified. Also additional trackings (events) and assignments to team members can be arranged <ref5/>.

::: tip
The Issue Panel provides a complete overview of the Issues in a project. Issues are also accessible from the **Issues section** of the artifact that they are associated with.
:::

Regarding the concerned artefacts you may also add additional artefacts where the issue is related to by clicking on the + button. You first have to choose which kind of artefatc you want to add, then a dialog with search options appears to offer addding a specific artefact.

<img src="../../img/image-20230224152210604.png" alt="image-20230224152210604" style="zoom:33%;" />

You can delete a concern relationship clicking on the x in the chip.

<img src="../../img/image-20230224152047055.png" alt="image-20230224152047055" style="zoom:40%;" />

## The Label Panel

The Label Panel is used to manage Issue labels.

![image-20220624152746795](../../img/image-20220624152746795.png)

The Speed Dial <ref1/>can be used to edit or delete a Label. Labels are colored tags to classifiy an issue for better organization, triage grouping etc. 

The ADD button <ref3/> offers a dialog to create a new Label. It offers the following dialog to set a name, a short code and a description for the label and to use a specific color. A preview of the label is also presented.

<img src="../../img/image-20220624152939504.png" alt="image-20220624152939504" style="zoom:67%;" />

Labels can be assigned to Issues on the [Issue Details](#issue-details) page.

## Create a new Issue

Issues can be created from any panel in a project. Authorized users are offered a NEW ISSUE button. <ref1/> This button will open up a dialog to create the issue.

![image-20220624153238276](../../img/image-20220624153238276.png)

The following dialog is presented so the the user can complete the issue/ticket. As a minimum you need to enter a title and a description.

<img src="../../img/image-20220624153605825.png" alt="image-20220624153605825" style="zoom: 67%;" />

::: note

The context of the Issue is automatically preserved <ref1/> when you used one of the artefact Panel buttons to raise an issue. The context here is the artefact the Issue is related to.

:::

You can also use the Issue Panel to simply create an issue without a referenced artefact, maybe add a context later.

## Maintaining context of an Issue

When adding an issue through a artefact Panel, the context, i. e. the artefact where the Issue is related to, is automatically added, (see above). You can add or delete context information of an issue also afterwards.

The *Concerns* statement in the Issue Detail view below states already assigned artefacts (artefact pills). If you are a Project Author, you see a little + behing the *Concerns* label . 

![image-20221011161813393](../../img/image-20221011161813393.png)

Clicking on the + you get a list of artefact types to choose from.

![image-20221011161936960](../../img/image-20221011161936960.png)

Once selected, the list of artefacts from the project appear and one or more items can be selected to be express they are associated with the issue.

![image-20221011162054504](../../img/image-20221011162054504.png)
