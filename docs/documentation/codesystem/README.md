---
permalink: /documentation/codesystem/
---

# Code System

## Code System Panel

The Code System Panel offers the list of all Code Systems of the Project, sorted alphabetically.
The icon indicates wether a Code System is in-project (colored dot) or a reference from a Building Block Repository (chain symbol).

![codesystem-tree-1](../../img/codesystem-tree-1.png)

Once a Code System is selected, the details of the Code System (metadata and the concept list) are shown.

Project authors can edit the metadata directly. The concept list is edited with the [Code System Editor](#code-system-editor).

<img src="../../img/image-20220623173254816.png" alt="image-20220623173254816" style="zoom:50%;" />

The Version Stepper <ref1/> allows to select a different version of the Code System.

Additional meta data becomes available by clicking the green Show More arrow <ref2/>.

Use the search bar to search coded concepts in the concept list <ref3/>. It's possible to search by code or designation. For exact search, use a colon as a prefix, e.g. `:ZA`. 

The concept list <ref4/> shows all concepts, preferred terms, synonyms etc.

The Usage and Issues tab <ref5/>can be expanded to load the corresponding information.

## Code System Editor

Project authors can access the Code System Editor by clicking the EDIT button in the header of the concept list.

The Code System Editor contains two main sections:

- Concept list
- Property definitions

### Concept list

This section is used to add or edit coded concepts in the concept list.

The hierarchical tree on the left can be used to select a coded concept.
Once a concept is selected, the details of the concept are shown on the right. If applicable, parent and child concepts are also displayed here.

![image-20220623173959537](../../img/image-20220623173959537.png)

The menu button <ref1/> allows adding new coded concepts to the list.

Buttons are available <ref2/> to move the concept among its sibblings and to manage parent relationships.

The Designations tab can be expanded <ref3/> to add or edit designations.

The Properties tab can be expanded to add or edit properties to the concept <ref4/>. This is only available if there are [Property definitions](#property-definitions) defined in the Code System.

### Property Definitions

This section is used to manage property definitions.

![image-20220623174247514](../../img/image-20220623174247514.png)New property definitions can be added <ref1/>.

Definitions that are used by a coded concept are not editable and can´t be removed <ref2/>.
