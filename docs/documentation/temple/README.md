---
permalink: /documentation/temple/
---

# Temple

Temple is an XML-based editor for templates. Temple is fast and well suited for those who are confomfortable with the XML syntax for e.g. Templates, Value Sets and Code Systems. Temple is aware of names and id's of Concepts, Value Sets and Templates and supports autocompletion of those. Temple has a built-in inspector (Ctrl+S, see below) to quickly inspect references.

## Starting Temple

Temple may be opened from the Template viewer with the Temple button (an iconic temple :-). If opened, the Template being viewed is opened. 

::: note

Temple is not an Orbeon XForms application, but eXist + HTML. This means that you need to be logged in to eXist to use Temple. Temple (or more correctly, your browser) may ask credentials when opening Temple.

:::

::: warning

Our colleagues in Austria found out that Temple does not seem to work properly with Microsoft's Edge browser. It seems that Edge is not supporting the extra login in Temple. 

Please consider using another browser.

:::

## The Editor

When opening a Template, the corresponding templateAssociation is automatically retrieved. If it does not exist, it is created in Temple (though not saved yet). The attributes `@templateId` and `@effectiveDate` are not shown on templateAssociation. On saving, Temple will copy them from the template. (If you add them manually, they will be overwritten on save.)

If you need a new `template/@id`, simply type in *new*, and Temple will create a legal `template/@id` when saving:

```xml
<template id="new" ...
```

You can (manually) enter a new `@effectiveDate`, or choose from the drop-down. In the drop-down, 'now' translates to the time Temple was opened (i.e. *2014-01-21T13:16:08*) and *today* translates to the start of today (i.e. *2014-01-21T00:00:00*). 

The following rules apply when saving a template:

- If `@id` and `@effectiveDate` exist, template and templateAssociation are overwritten.
- If `@id` exists, but `@effectiveDate` not, a new template is created (in effect, a new version of the template).
- If `@id = 'new'`, a new template is created. An `@effectiveDate` must be supplied by the user.
- If templateAssociation does not exist, it is created (maybe empty, if no concepts are provided).

When saving, Temple will validate against the DECOR schema. If there are errors, those will be displayed. This will land you on an error page, so go back in the browser to edit further. It's also possible to validatte without saving. If the template is valid, after saving you will stay on the edited template page, thus allowing to Save and continue working. Only when creating a template with `@id='new'`, you will be redirected to an empty page, and will have to navigate to the new template from the User Interface (where you might need to refresh the page you were on).

Temple will add names to:

- `templateAssociation/concept/@ref`
- `include/@ref`
- `@contains`
- `@valueSet`

when those are referenced by id. The names are discarded again when saving.

The editor supports 'Find' and 'Replace' with the default [CodeMirror shortcuts](https://codemirror.net/demo/search.html). Make sure the cursor is in the Code area, otherwise the browser behavior will overrule the shortcuts.

::: note CodeMirror shortcuts

- Ctrl-F / Cmd-F

  Start searching

- Ctrl-G / Cmd-G

  Find next

- Shift-Ctrl-G / Shift-Cmd-G

  Find previous

- Shift-Ctrl-F / Cmd-Option-F

  Replace

- Shift-Ctrl-R / Shift-Cmd-Option-F

  Replace all

- Alt-F

  Persistent search (dialog doesn't autoclose, enter to find next, Shift-Enter to find previous)

- Alt-G

  Jump to line

:::

## The inspector

When the cursor is in on any construct containing the name or id of a Template, a Value Set or Concept, press Ctrl-I to inspect it. Temple will look for an `effectiveDate` or flexibility on the same line of code and use that, and else will use *dynamic*. For instance, put the cursor inside the id in:

```xml
 <element name="hl7:id" datatype="II.NL.BSN" id="2.16.840.1.113883.3.1937.99.62.3.11.7.1">
```

and press Ctrl-S to inspect the referenced concept(s). Within the inspector one can also inspect further, for instance to inspect the parent of a concept or an included template. Searchable id's are color-coded. When inspecting something on `id` (and `effectiveDate`) a global search in the DECOR repository is done. When searching on name, a local search in current project is done.

The inspector has a search box which gets all artefacts (concepts, transactions, valueSets and templates) containing a search string. 

::: note Supported Searches

Supported searches are:

- 'address' : Search for all artefacts containing 'address' in current project.
- 'all:address' or '*:address': Search for artefacts containing 'address' in current project and BBR's referenced in current project.
- 'demo1:address' : Search for all artefacts containing 'address' in DECOR 'demo1-' project (if available on seerver). This allows searching in other projects than current project.
- 'loinc:amoxi serum' : Search for LOINC concepts containing 'amoxi' AND 'serum'. Returns <code .../> elements.
- 'loinc:18-2' : Search for LOINC concept on id. Returns concept details.
- 'sct:myocard infarct' : Search for Snomed CT concepts containing 'myocard' AND 'infarct'. Returns <code .../> elements.
- 'sct:22298006' : Search for Snomed CT concept on id. Returns concept details.
- sct and loinc search will display an error on servers where LOINC or Snomed are not installed.

:::

## Shortcuts

While editing a template, temple supports the following shortcuts:

- The attribute *cc* on **element** offers a shortcut to adding the most common combinations of @minimumMultiplicity, @maximumMultiplicity, @conformance and @isMandatory. Example:
  *cc*="**1..1 M**" expands into *minimumMultiplicity*="**1**" *maximumMultiplicity*="**1**" *isMandatory*="**true**"
- The attribute *as* on **attribute** offers a shortcut to adding the most common HL7 V3 attributes like classCode and moodCode as name/value pair. Example:
  *as*="**classCode**" expands into *name*="**classCode**" *value*=""
