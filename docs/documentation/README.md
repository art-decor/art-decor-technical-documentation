---
permalink: /documentation
---

# Documentation

This section explains the use of the tool (user manual), focusing on the respective artefacts, plus additional documentation around the tool, e. g. what datatypes are supported or how to use multiple languages in a project.

## Getting Started

- [Principles of Use](principles)
- [Starting with ART-DECOR](starter)
- [The Landing Page](landing)

## Artefacts

- [Project](project)
- [Dataset](dataset)
- [Scenario](scenario)
- [Questionnaire](questionnaire)
- [Terminology](terminology)
- [Value set](valueset)
- [Code System](codesystem)
- [Template](template)
- [Profile](profile)
- [Issue](issue)

## Features

- [Datatypes](datatypes)
- [Multiple Languages](languages)
- [FHIR](fhir)
- [LOINC Panels](loinc-panels)

## Lists

- [Artefact Types](artefacts)
- [ART-DECOR® – Releases](releasenotes)
- [Publications about ART-DECOR®](papers)

## Newsletter

- [Newsletters ART-DECOR®](newsletters)