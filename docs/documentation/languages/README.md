---
permalink: /documentation/languages/
---
# Multiple Languages

## Introduction

ART-DECOR distinguishes two types of multi language support

- the user interface language, i.e. in what language the Menus, Panels, Labels, Buttons etc. appear
- the project language, i.e. in what language a project author collects content to name concepts, terminologies, templates, profiles etc. or in what language issues are submitted.

::: tip PLEASE NOTE

Do not mix Project Content Languages with User Interface Languages.

:::

## User Interface Language

Users, regardless whether logged in or not, can choose for one of the supported official ART-DECOR **User Interface Languages**:

- American English (en-US)
- German (de-DE)
- Dutch (nl-NL)
- French (fr-FR)
- Italian (it-IT)
- Polish (pl-PL)

The choice can be made in the Top Menu Strip with the rightmost flag select.

![image-20211001155828892](../../img/image-20211001155828892-3096709.png)

Switching the language immediately changes the User Interface Language.

::: tip DEFAULT LANGUAGE

Users that are not logged in will get the language of their browser/navigator used to approach the site. If this is not set, the server default language (a server setting made by the server administrators) is used but the user may switch to another language for the session.

:::

If a user is logged in he can make use of the user preferences and select his preferred User Interface Language in his settings (“Preferred language”):

<img src="../../img/image-20230623185752952.png" alt="image-20211001155852350" style="zoom:50%;" />

His settings are always used when the user logs in, however he also may temporarily switch the User Interface Language for the session.

## Project Language

In ART-DECOR, multilingual projects are supported. The project language determines in what language a project author collects content to name concepts, terminologies, templates, profiles etc. or in what language issues are (typically) submitted.

A Project Administrator can determine all supported languages of a single project in the Project Overview Panel under Properties and “Project Languages”.

![image-20211001155933178](../../img/image-20211001155933178.png)

In addition, the so-called “Default Langauge” can be determined, i.e. the language the acts as the default and preferred language for the project.

If a project has more than one supported content languages, the flag next to the name of the project in the Top Menu Strip may be used to select another Project Language to be displayed for names etc.

![image-20211001160003203](../../img/image-20211001160003203-3096804.png)

If a new artefact is created, e.g. a dataset, only the supported project languages are offered to be populated.

<img src="../../img/image-20211001160022895-3096824.png" alt="image-20211001160022895" style="zoom:75%;" />

If you want to support additional languages in a project the project administrator has to add this extra language in to the project language in properties in the Project Overview Panel. 

## Fall-back for missing content

It may happen that a project has supported languages but not all artefacts have proper names or descriptions in the respective languages. 

::: example

A project supports nl-NL, en-US and de-DE. The default language is nl-NL.

:::

If the selected language to be displayed for the project content is not populated the frontend falls back to 

- the default language as this shall be the language in which all artefacts shall have proper names or descriptions
- English (en-US).

::: example

In the example above, if the selected project language to be displayed is de-DE and there is an artefact to redisplayed that has no proper name is de-DE, the front end tries to show the name in nl-NL and if that fails in en-US. 

:::

This mechanism is in place to avoid in most cases that a artefact such a Navigation Tree or a Name or Description will not show anything.
