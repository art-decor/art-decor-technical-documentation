---
permalink: /documentation/dataset
---

# Dataset

## Dataset Panel
The Dataset Panel offers a list of datasets. A Dataset is a list of (hierarchical) Concepts.

![image-20220701103125611](../../img/image-20220701103125611.png)

When opening the Dataset Panel, the Navigation Card on the left <ref1/> shows the list of Datasets of the Project. 

## Choosing a Dataset
Clicking on a Dataset in the the Navigation Card immediately switches the Navigation tree to show details of the selected Dataset where you can find all the concepts.

![image-20220701103427919](../../img/image-20220701103427919.png)

## Adding an empty Dataset<new/>
<img src="../../img/image-20220701104035535.png" alt="image-20220701104035535" style="zoom:67%;" />

By clicking on the menu <ref1/> you can choose the option to add a new Dataset or to clone a Dataset.

In the Search bar <ref2/> you can search for a Dataset in the tree. This makes especially sense if your Project has many Datasets.

You can also filter out a status code for the Dataset by clicking on the filter button.

![image-20220701104133601](../../img/image-20220701104133601.png)

## Concept Detail Panel

After choosing a Dataset, the concept Detail Panel opens where you can find the list of Concepts for that specific Dataset. You can also search for a concept with the search bar or filter out concepts based on their status.

![image-20220701104233458](../../img/image-20220701104233458.png)

By clicking on a Concept the details of that Concept are shown on the right where you can find the metadata and further details.

![image-20220701104333724](../../img/image-20220701104333724.png)

### Editing Concepts

Editing Concept details is done in two ways. 

By clicking on the *Show more metadata button* <ref1/>you can see more metadata. For metadata you are able to directly edit the corresponding fields.

To edit the details of the Concept you select the EDIT button <ref2/>.

<img src="../../img/image-20220701104716257.png" alt="image-20220701104716257" style="zoom: 50%;" />

When switched to edit-mode, the concepts gets into the focus of the screen, everything around the Concept Details is grayed out <ref3/>. There are multi-lingual fields <ref4/> to be added: Operationalization, Source, Rationale, Comment. Also Properties may be added, essentially a entity-value pair providing additional classifications for your dataset concept.

For items, the Value Domain <ref5/> allows to specify a datatype for the concept. If a Concept has a coded datatype you may also add Choices to the Choice List listed at <ref6/>.  The ADD button offers a dialog to add a singular Choice Concept with all details. To just add a quick list of terms use the QUICK ADD button. Choices maybe refined later through the edit menu per row of choice <ref7/>.

<img src="../../img/image-20220705105942314.png" alt="image-20220705105942314" style="zoom:67%;" />

Value Set Associations are displayed here only <ref8/>. To manage Associations to have to go to the approriate *Mapping / Associations* Panel in the Terminology Menu.

If a Dataset Concept is of type code, has no choice list but one or more Value Set Associations, the Concepts in <ref7/> are populated with the Value Set codes due to lack of a choice list. If only one Value Set Associations exist, it is preselected and the content is directly shown. If there is more than one Value Set Association, you can select one (check box) to be shown.

<img src="../../img/image-20220928190923537.png" alt="image-20220928190923537" style="zoom:70%;" /> 

If the multiple Value Set Associations and multiple Value Set Concept Associations, all assocations are combined in the view.

<img src="../../img/image-20220928191334487.png" alt="image-20220928191334487" style="zoom:67%;" />

Value Domains may have various Properties, the availability depends on the datatype. For example, a date may have a precision. See [datatype-facets](../../introduction/dataset/#datatype-facets) in the introduction on datasets for more details. 

<img src="../../img/image-20220701124041648.png" alt="image-20220701124041648" style="zoom:67%;" />

A Dataset Concept may also get Keywords being assigned to it to be better found with specialized terms in searches.

![image-20220701124051791](../../img/image-20220701124051791.png)

### Moving Concepts

You can also change the sequence or nesting of concepts. This is done through the Navigation Tree where you find a MOVE button <ref1/>.

<img src="../../img/image-20220701105218458.png" alt="image-20220701105218458" style="zoom: 50%;" />

A new window will pop up where you can edit the tree structure by drag and drop concepts within the hierarchy. The SAVE button confirms your moves.

<img src="../../img/image-20220701105420494.png" alt="image-20220701105420494" style="zoom:67%;" />


## Adding a Concept

### Inserting Concepts

At the right side of each concept in the Navigation Tree a little PLUS button offers a small menu to insert new concepts. 

![image-20220701105715334](../../img/image-20220701105715334-6665835.png)

To add a new concept option are offered to choose from on how to insert the new Concept. The new Concept (group or item) may be added before or after an existing one, when the selected Concept is a group the new Concept may also be inserted also into that group.

![image-20220701105842865](../../img/image-20220701105842865.png)

### New Concept's details

When adding a Concept a dialog is offered to assign a name to the Concept <ref1/>.

![image-20220701115753139](../../img/image-20220701115753139.png)

#### Inheritance / Containment

As re-use is a focus topic of ART-DECOR you can also click in the search icon <ref2/>after entering a name and search for similar concepts in opther projects to optionally use them (**inheritance / containment**) instead of creating a possible duplicate in your project. The list of similar concepts is offered in the *Related Concepts* table after search. Your search can be filtered either by the usual Status code or Project <ref3/>.

![image-20220701115724611](../../img/image-20220701115724611.png)

The menu next to each found Related Concept offers the chain symbol that allows to specify **Inheritance** of the concept. When the selected concept is a group, also the **Containment** option is also offered. To learn more about Inheritance and Containment of Concepts see our [introduction here](/introduction/inheritancecontainment), if you want to deal with Inheritance and Containment, refer to the [section here](#concept-inheritance-and-containment).

![image-20220701120958450](../../img/image-20220701120958450.png)

[Et voilà](https://de.pons.com/übersetzung/französisch-englisch/et+voilà): a new inherited or contained concept has been added to the dataset.

::: warning

You cannot inherit from datasets residing in experimental projects. Project flagged as *experimental* are always invisible to other projects regarding data set elements inheritance.

:::

#### New (own) Concept

If creating a new Concept, click on the NEXT button then to add more details, especially a description <ref4/>. Eventually click on ADD <ref5/> to add the new concept to the dataset.

![image-20220701120134649](../../img/image-20220701120134649.png) 

[Et voilà](https://de.pons.com/übersetzung/französisch-englisch/et+voilà): a new concept has been created and added to the dataset.

## Concept inheritance and containment

Inherited concepts apear with a colored chain symbol <ref1/>. If you choose to alter an inherited concept, you first have to de-inherit the concept <ref2/>.

<img src="../../img/image-20220701122111623.png" alt="image-20220701122111623" style="zoom:50%;" />

### Dealing with Inherited Concepts

Inheritance is a „copy of a design“. Upon creation of an inherited concept, a structural copy is created from another location into the current data set (left), containing references to each and every concept in the source (right).

<img src="../../img/image-20220701125552318.png" alt="image-20220701125552318" style="zoom:50%;" />

If a concept in a dataset is inherited from another dataset, the status icon is actually a chain symbol for items and a outline folder for groups in the respective color.

In this example the item *Blutzucker* is a genuine project item while *Atemfrequenz* is a inherited item from another dataset, so it carries the appropriate symbol.

![image-20210929172318060](../../img/image-20210929172318060-2928999.png)

This is also shown in the Detail view of the concept, see last row in this example.

![image-20210929170154069](../../img/image-20210929170154069-2927715.png)

In order to be able to edit an inherited concept, you must *de-inherit* the concept first.

In the example above you can easily do that through a special DE-INHERIT button with a un-link symbol in the Dataset Concept Detail Top Bar. After this action the concept is copied to your project with all properties including a relationship indicator to the formerly inherited dataset concept.

In a hierarchical tree, usually also all parent concepts need to be de-inherited. In this example the desire is to alter *Symptomdauer* that is part of the inherited group *Beschwerden bei Vorstellung*.

![image-20210929170207538](../../img/image-20210929170207538-2927728.png)

The reason for de-inheriting the group first before you can do anything with its children is, that you are potentially altering the semantics of a concept and therefore potentially also the semantics of all parent concepts. Therefore you need to de-inherit your target concept and all parents to indicate to anybody, that there is a possible change in the meaning of that group/block or concept. De-inherit all parent concepts as well first, i.e. here the group *Beschwerden bei Vorstellung*. To de-inherit a concept, a special DE-INHERIT button with a un-link symbol appears in the Dataset Concept Detail Top Bar as demonstrated above already.

![image-20210929170222380](../../img/image-20210929170222380-2927744.png)

After that you can de-inherit the desired concept *Symptomdauer* in the same way and edit it.

![image-20210929170236537](../../img/image-20210929170236537-2927758.png)

After having de-inherited the group you might now also add or change items in the group, as an example to add another element to the de-inherit group, called *Erheber*.

![image-20210929170255286](../../img/image-20210929170255286-2927776.png)

After this action, group *Beschwerden bei Vorstellung* has a normal group symbol and a changed definition because *Erheber* has been added. *Beschwerden bei Vorstellung (Freitext)* is obviously left untouched so far (still has the chain symbol).

### Dealing with Containment

Containment is a true reference (à la “sym links” in other contexts) of a Concept Group. Upon creation of a concept, this is a transparent link to an original concept at another location, denoted with the "share all" icon. Containment is only possible with groups.

<img src="../../img/image-20220701125149297.png" alt="image-20220701125149297" style="zoom:50%;" />

It is possible to transition a containment relationship into a true inheritance relationship. For that purpose the DE-INHERIT / DE-CONTAIN button is offered. The original group is turned into a set of inherited concepts. An inherited group is denoted by the open folder outline symbol, inherited items by the chain symbol.

<img src="../../img/image-20220701125710065.png" alt="image-20220701125710065" style="zoom:50%;" />

It is also possible to transition a containment relationship into a true copy. It causes also a relationship indication to the original ("concept comes from this original concept"). For that purpose the formal inheritance indicator is replaced by an informal relationship indicator.

<img src="../../img/image-20220701125903855.png" alt="image-20220701125903855" style="zoom:50%;" />
