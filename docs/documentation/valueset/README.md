---
permalink: /documentation/valueset/
---

# Value set

The Panel offers the list of all Value Sets of the Project, sorted alphabetically and indicating whether they are in-project Value Sets (plain colored icons) or references from a Building Block Repository BBR (chain symbol).

![image-20220126161021364](../../img/image-20220126161021364.png)

## Concept Lists (Compositions)

Once a value set is chosen from the Navigation Tree, the right card shows the details of the Value Set (metadata) including the Concept List.

![image-20220126120314693](../../img/image-20220126120314693.png)

Concept Lists are **composition of a concrete set of codes** (_extensional_ Value Set, like in the example above) or contain **instructions** basically of inclusion and exclusion of codes (_intensional_ Value Sets), that are typically drawn from a hierarchical code system. Intensional Value Set definitions are not expanded but shown as the definition itself, e.g. _Include_.

![image-20220126122154735](../../img/image-20220126122154735.png)

The example above is a small intensional Value Set that includes a single SNOMED-CT code with the instruction to expand this on demand to all codes hierarchically under this code. The expansion is done on demand at "runtime" and leads to an Expansion Set. Intensional Value Set definitions are not expanded by default but shown as the definition itself, e.g. _Include_.

Mixes of concrete lists of codes and instruction are also possible.

![image-20220126161157393](../../img/image-20220126161157393.png)

## Designations

Value Set Codes may have designations with types and in different languages. A designation adds extra information to the display name.

### Types of a designations

From a clinical terminology viewpoint, a type of a designation is a selection of one of the following choices. Some definitions and examples have been drawn from Henriksson A, Conway M, Duneld M, Chapman WW. Identifying synonymy between SNOMED clinical terms of varying length using distributional analysis of electronic health records. AMIA Annu Symp Proc. 2013 Nov 16;2013:600-9. PMID: 24551362; PMCID: PMC3900203, modified.

#### Preferred Term

A Preferred Term is a common *preferred* or acceptable phrase or word used by clinicians to name a concept in a language context. In contrast to the *Fully Specified Name*, the Preferred Term is not necessarily unique. It can be a Synonym or preferred name for a different Concept.

There is exactly one Preferred Term in a given language.

::: example Examples from SNOMED-CT

Clinical Context: Myocardial Infarction

<badge text="Myocardial infarction" blue/> is the Preferred Term

Clinical Context: Depression

<badge text="Depressive disorder" blue/> is the Preferred Term

:::

#### Fully Specified Name

A Fully Specified Name is an unambiguous string used to name a concept.

::: example Examples from SNOMED-CT

Clinical Context: Myocardial Infarction

<badge text="Myocardial infarction (disorder)" blue/>  is the Fully Specified Name

Clinical Context: Depression

<badge text="Depressive disorder (disorder)" blue/> is the Fully Specified Name

:::

#### Synonym

A Synonym that can be used as an acceptable alternative to the PreferredTterm. A concept can have zero or more synonyms in a given language.

::: example Examples from SNOMED-CT

Clinical Context: Myocardial Infarction

Common synonyms: <badge text="Myocardial infarct" blue/> <badge text="Heart attack" blue/> <badge text="Infarction of heart" blue/> <badge text="Cardiac infarction" blue/> <badge text="MI - myocardial infarction" blue/>

Clinical Context: Depression

Common synonyms: <badge text="Depressed" blue/> <badge text="Depressive illness" blue/> <badge text="Depression" blue/> <badge text="Mood disorder of depressed type" blue/> <badge text="Depressive episode" blue/>

:::

#### Abbreviation

An Abbreviation is a shortened form of a word or phrase used in place of the whole word or phrase. This includes acronyms as a word or name consisting of parts of the full name's words. Please use Abbreviation with care as they have the potential for misunderstandings.

::: example Examples from SNOMED-CT

Clinical Context: Myocardial Infarction

Common synonyms: <badge text="MI" blue/>

Clinical Context: Bone fracture

Common synonyms: <badge text="FRX" blue/> <badge text="FX" blue/> <badge text="#" blue/>

Clinical Context: Computerized Tomography

Common synonyms: <badge text="CT" blue/> <badge text="MRI" blue/>

:::

::: warning

Abbreviations may need context to properly interpret them.

Example: <badge text="MI" blue/> is *Myocardial infarction* but also *Mitral insufficiency* or *Mental institution*. Beyond the healthcare topic it yould even mean *Military Intelligence*.

:::

#### Descriptions

You may add descriptions, one per given language.

#### A visual example

The above-mentioned Henriksson A, Conway M, Duneld M, Chapman WW published a figure about the relationship of terms (and types of designations) to a clinical context. This is an adapted figure to show this based on the *Myocardial infarction* example.



<img src="../../img/image-20231122165408933.png" alt="image-20231122165408933" style="zoom:40%;" />

### Designations in Value Set Panel and Terminology Browser (view)

The designations for the selected **project language** are shown if present. This can be seen in the value set panel, but also in the Terminology Browser. You may switch visibility on/off, so, if it would pollute the view too much, you could toggle designation's visibility.

<img src="../../img/image-20231122120542394.png" alt="image-20231122120542394" style="zoom:60%;" />

### Terminology Browser as Editor

When editing a Value Set (which is done as a Composition in the Terminology Browser) you will see designations for the selected project language only when looking at the Composition as a whole. However, if there are more designations this is indicated by a "+n" addition next to designation.

<img src="../../img/image-20231122122710901.png" alt="image-20231122122710901" style="zoom:70%;" />

If you *edit* a specific concept in the Terminology Browser, you would see the designations for *all* the languages that are available.

<img src="../../img/image-20231122122750336.png" alt="image-20231122122750336" style="zoom:67%;" />

## Value Set Authoring

A Project Author in ART-DECOR can create new Value Sets and modify or clone already existing ones.

### Creating and editing a Value Set

#### Extensional Value Sets

Editors of a project may create new Value Sets through the Value Set form or clone an existing Value Set. By choosing ADD from the Panel menu you can add a new Value Set or clone an existing Value Set.

![2022-06-24_11-25-52.png](../../img/2022-06-24_11-25-52.png)

The Terminology Browser is not only used for browsing. For the purpose of creating or editing Value Sets, the author is redirected from the Terminology Panel to the Terminology Browser. In addition to the search field (and possible search results), the existing Composition is shown at the bottom. You may need to scroll on larger search result lists to see the Composition Card. When adding a new Value Set the Composition is initially empty, of course.

![image-20220126120254484](../../img/image-20220126120254484.png)

The Value Set Composition can be completed by adding terms that have been found, or by adding single coded items. When adding an item to the Value Set Composition, the author get the opportunity to add new codes from his search, or individual codes, and edit the Value Set Composition.

![image-20220126121454467](../../img/image-20220126121454467.png)

##### Adding a code manually

In some cases it might be useful to manually add a code to a Value Set. To do so, click the '+'-icon under the actions menu (three dots).

![Actions menu for adding a code manually](../../img/add-manual-code-01.png)

A dialog is offered to fill in the details of the code.

![Dialog for adding a code manually](../../img/add-manual-code-02.png)

**Select codesystem for context** can be used as a convenient way to fill in the "Code System" field.

When all required fields are populated, the code can be added to the Value Set.

#### Searching in Project specific code systems only

To add a code from a proprietary code system of your own project one can select “Current project” as the selected code system to use for search. 

<img src="../../img/image-20240105161337412.png" alt="image-20240105161337412" style="zoom:67%;" />

Once entered a term or code for search the own project code systems are searched and results are displayed as usual, i.e. as for any other code system.

<img src="../../img/image-20240105161418000.png" alt="image-20240105161418000" style="zoom:67%;" />

::: note

Please note that searching in proprietary project code systems is only supported for code systems that are stored as part of the Centralized ART-DECOR Terminology Services (CADTS). Usually this happens automaticall whenever a project code system is edited and saved.

:::

#### Intensional Value Sets

This example shows the inclusion of an **intensional Value Set** definition from SNOMED-CT, specifying all descendents of the entered code.

<img src="../../img/image-20220126122115868.png" alt="image-20220126122115868" style="zoom:67%;" />

Please also note, that editing a code of a composition also offers a variety of other options, including entering types of designations such as

- Preferred Terms
- Synonyms
- Abbreviations
- Fully Specified Names and
- Descriptions.

The intensional operators typically are _is-a_ (the specified code and all children codes, see the left part of the following figure), or _decendent-of_, which means the children only. The following figure illustrates these two options.

 <img src="../../img/image-20220624144354572.png" alt="image-20220624144354572" style="zoom:33%;" />

Next to inclusion also exclusion is possible which makes sense of a part of an already included tree shall not be part of the expansion. One can include complete Code Systems or Value Sets from the project. This example shows the inclusion of an intensional Value Set definition, specifying to include a whole Value Set from the project. The reference to a Code System or a Value Set used in intensional expressions can be done dynamically (always the most recent version), or fixed for a specific version.

# <img src="../../img/image-20220126121742994.png" alt="image-20220126121742994" style="zoom:67%;" />

You can expand intensional value sets if they are CADTS aware. If so, the Composition in the Value Set Panel shows the *Expansion Set* button.

<img src="../../img/image-20221122184107782.png" alt="image-20221122184107782" style="zoom:67%;" />

Clicking on the button opens a dialog that first presents the count of items in the Expansion Set. This is usually quick, e. g. 300,000 codes counting members of the Expansion Set takes around 2-3 seconds.

<img src="../../img/image-20221122184504724.png" alt="image-20221122184504724" style="zoom:67%;" />

The Expansion Set can also be inspected, showing only 100 codes. Later releases will allow to store Expansion Sets.

<img src="../../img/image-20221122184551990.png" alt="image-20221122184551990" style="zoom:67%;" />

It is important to note that concepts in an expansion set do not have any specific order. If the order of the concepts in the set is important, e.g. with answer options for questionnaires, an extensional definition should be used. In the case of LOINC answer lists the preferred method is to use the LOINC panel import function as this will import the required value sets in the order specified in the answer list and also contain score values defined in the answers.

Please note that if an intensional definition contains a construct that has inactive codes this might lead to wrong counts or expansion sets. It is indicated if detected as follows.

<img src="../../img/image-20221122184616994.png" alt="image-20221122184616994" style="zoom:67%;" />

**Intensional Value Sets** also support Complete Codesystems with filter options. A Codesystem can be included completely (only the codesystem need to be mentioned) or may be included with a filter that selects only certain codes from the system.

The include *Complete Codesystems with filter* dialog has the following required components:

- a **mandatory Codesystem** from the project with optional flexibility
- one or more optional filters, each with a mandatory **property**, an **operator** and a **value**.

There are some **Filter Examples** to demonstrate the following situations.

::: example Hierarchical code system filter

Consider a hierarchical code system H (without any extra properties) with the following hierarchy. 

<img src="../../img/image-20230620113539423.png" alt="image-20230620113539423" style="zoom:40%;" />

You can now create an intensional value set that shall only include the third level codes (from A-1-1 and A-1-2 etc.) by selecting the complete code system with all codes were CHILD exist false (implicit property filter).

<img src="../../img/image-20230620114025479.png" alt="image-20230620114025479" style="zoom:60%;" />

This is displayed in the Value Set definitions as follows.

<img src="../../img/image-20230620113610669.png" alt="image-20230620113610669" style="zoom:40%;" />

In the above example the expansion set will include concepts A-1-1, A-1-2, B-1-1 and B-1-2 only.

<img src="../../img/image-20230620113637761.png" alt="image-20230620113637761" style="zoom:40%;" />

:::

::: example Code System with Filter by Property

A proprietary code system C is defined and a concept property X is defined.

Code System C

- has property X type string
- concept a, property X='A'
- concept b, property X='B'
- concept c, property X='A'
- concept d, property X='C'

You can now create an intensional value set with

- … the complete code system C, expansion set will include concepts a, b, c and d
- … the complete code system C with all codes were property X equal 'A', expansion set will include concepts a, and c only

This is rendered as follows...

<img src="../../img/image-20230620114144698.png" alt="image-20230620114144698" style="zoom:60%;" />

...and this is the resulting Expansion Set. 

<img src="../../img/image-20230620113716084.png" alt="image-20230620113716084" style="zoom:40%;" />

:::

::: example LOINC example Filter by Property

All LOINC codes were property METHOD = 'Manual count' (explicit property filters)

:::

::: note Internal Format Note

With this feature, the DECOR storage format refrains from using completeCodeSystem tags in Value Set definitions. This has been fixed in updated versions of the API, ART, DECOR-core, DECOR-services, FHIR 1.0, FHIR 3.0, FHIR 4.0, and Temple.

:::

Value Set Expansion for Intensional Value Sets works with SNOMED and all ART-DECOR Code Systems. A future release will allow *storage and download* of Expansion Sets.

#### Exceptions
Coded concepts (like for instance nullFlavors in HL7v3/FHIR) can also be added to the Value Set as exceptions rather then regular coded concepts. Exceptions can used to indicate that the data is missing, sometimes possibly including a reason for the absence of the data.

#### Postcoordination Expression

It is also possible to add post-coordinated expressions using SNOMED. When adding or editing an item in a value set, there is also the tab "Postcoordination" offered that gives a dialog with options to enter Level and Type, a Root / focus concept and a set of Attribute-Value codes drawn from SNOMED. In the Root / focus concept and Attribute-Value code fields you can enter search terms that are looked up in the SNOMED code system (given one is installed).

<img src="../../img/image-20230731114816415.png" alt="image-20230731114816415" style="zoom:70%;" />

The post-coordinated expression is also checked against SNOMED.

<img src="../../img/image-20230731115252551.png" alt="image-20230731115252551" style="zoom:70%;" />

The item appears in the value set as something like in this example.

<img src="../../img/image-20230731115843142.png" alt="image-20230731115843142" style="zoom:77%;" />

::: note SNO-POEM

We are working hard on a SNOMED post-coordinated expressions editor based on SNOMED's [Expression Template Language](https://confluence.ihtsdotools.org/display/DOCSTS/6.1.+Expression+Template+Language) (ETL). The methodology is to derive Questionnaires from official (and self-crafted) ETL expressions and render them as a form that is subsequently populated by a user. The SNOMED CT expression template is converted (once) for that purpose to a Questionnaire that is stored in a Building Block Repository and offered to the user through the post-coordination editor (POCE). 

The resulting Questionnaire is part of our new **SNO-POEM** concept. In future Releases we will publish SNO-POEM as a proper  alternative to hand-crafted SNOMED post-coordinated expressions.

:::

### Cloning a Value Set

The Panel menu also offers the option to clone an existing Value Set, e.g. for versioning purposes. After you choose the clone option a dialog opens. In the Value Set bar you can search and select the Value Set you want to clone.

![2022-06-24_11-44-30.png](../../img/2022-06-24_11-44-30.png)

#### Keep ID's

The 'Keep IDs' switch is **active** by default. A new version of the Value Set will be created. The generated Value Set has the same ID. Effective date will be set to 'now'.

Cloning a Value Set with the switch **deactivated** will generate a Value Set with a new id (based on the base id for Value Sets). Name, display name and other [Meta Data](/documentation/principles/#top-core-meta-data) can be edited afterwards.
