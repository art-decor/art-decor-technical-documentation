---
permalink: /documentation/repositories/
---

# Repositories

As part of the overall migrations to new Versions and Releases, ART-DECOR software and data for the backend (e. g. the eXist database) and for Release 2 is typically organized its **Repositories** and is installed using the eXist-db Package Manager. The default repository URL points to the eXist-db public repository, this needs to be changed to point to the public repository residing at repository.art-decor.org.

::: note

For most of our users updates and upgrades on our servers happen without further notice. Normally, our users simply use ART-DECOR or one of our servers art-decor.org, develop-art-decor.org, acceptance.art-decor.org or – for the starters and learners – on newbie.art-decor.org. We simple take care of properly installed software on our servers. 

If you use the tools on *your own servers or local machines* hosted by you and want to be up-to-date at any time, please have a look at this info chapter.

:::

## repository.art-decor.org – the place to be!

We have **stable** Release 2 and Release 3 master software availble for those you want stability and thoroughly tested and accepted software packages.

For all development work regarding Release 3 a **development** path for those you want to profit from the most recent nightly builds of the tool. All of the three repositories are organzied and located at **repository.art-decor.org**.

::: note

The formerly used repositories at decor.nictiz.nl are no longer available.

:::

### ART-DECOR Release 3

To use the **stable** ART-DECOR Release 3 backend use:

https://repository.art-decor.org/stable3

To use the **development** ART-DECOR Release 3 backend use:

https://repository.art-decor.org/development3

### ART-DECOR Release 2

For those who only want to use the backend **repositories for Release 2**, use:

https://repository.art-decor.org/stable2

### Additional information

Unless you are interested in getting and using the most recent develeopment code we recommend to use the **stable** (master) branch.

We documented all of that in chapter [Setup and Maintenance Support](https://docs.art-decor.org/administration/setupmaintain/), esspecially in the [repository change chapter](https://docs.art-decor.org/administration/setupmaintain/backend/#install-art-decor-backend) for the backend.

## terminology.art-decor.org

All terminology packages are described at our terminology.art-decor.org Repository. As Terminology Packages for Release 2 and for Release 3 are different, the "Terminology Data" Packages are residing in the respective **stable2** or **stable3** / **development3** repositories and are installed from there.
