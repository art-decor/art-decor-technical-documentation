---
permalink: /documentation/papers/
---
# Publications about ART-DECOR®

## Videos

### 2022

#### ART-DECOR® YouTube Video Channel

-   **ART DECOR Release 3 Top 10 Features** – A short intro video about ART DECOR Release 3 and the Top 10 Features, April 2022 [view](https://www.youtube.com/watch?v=c1hNXCXZjxA&t=83s)
-   **ART-DECOR® Release 3 New Terminology Browser** – A short intro to ART-DECOR®'s Release 3 New Terminology Browser, Januar 2022 [view](https://www.youtube.com/watch?v=X4naVY5sDDE)

## Presentations and Papers

### 2024

Vorisek, C.N., Klopfenstein, S.A.I., Löbe, M. *et al.* Towards an Interoperability Landscape for a National Research Data Infrastructure for Personal Health Data. *Sci Data* **11**, 772 (2024). https://rdcu.be/dPyAm

### 2023

New Release Updates Open-Source Tools Suite: **ART-DECOR® Release 3 – Next Generation Tooling** . HL7 International Newsletter Summer 2023, p 22ff. [Download PDF](HL7_NEWS_20230620.pdf)

### 2022

- de Ridder S, Beliën JAM. The iCRFGenerator: Generate interoperable CRFs, Amsterdam UMC. [Presentation](icrf_generator_slides_20220915.pdf) 15. September 2022

- de Ridder S and Beliën JAM. The iCRF Generator: Generating interoperable electronic case report forms using online codebooks [version 2; peer review: 2 approved, 1 approved with reservations]. *F1000Research* 2020, **9**:81 (https://doi.org/10.12688/f1000research.21576.2)

- Rinaldi E, Stellmach C. Establishing semantic interoperability across cohorts at European level – [Behind the Paper](https://healthcommunity.nature.com/posts/establishing-semantic-interoperability-across-cohorts-at-european-level)

- Rinaldi, E., Stellmach, C., Rajkumar, N.M.R. *et al.* Harmonization and standardization of data for a pan-European cohort on SARS- CoV-2 pandemic. *npj Digit. Med.* **5**, 75 (2022). https://doi.org/10.1038/s41746-022-00620-x

- Sabutsch S, Frohner M, Kleinoscheg G, Klostermann A, Svec N, Rainer-Sablatnig S, Tanjga N. ELGA Outpatient Clinic Report and ELGA Telehealth Note: Two HL7-CDA®-Based Modular Electronic Documents. Stud Health Technol Inform. 2022 May 16;293:73-78. doi: 10.3233/SHTI220350. PMID: 35592963. [Download PDF](https://doi.org/10.3233/SHTI220350)

### 2021

- Heitmann K. ART-DECOR® Release 3. HL7-Mitteilungen Nr. 45/2021, 14-19 [Download PDF](https://hl7.de/wp-content/uploads/hl7m452021.pdf)
- Wiedekopf J, Drenkhahn C, Ulrich H, Kock-Schoppenhauer AK, Ingenerf J. Providing ART-DECOR ValueSets via FHIR Terminology Servers - A Technical Report. Stud Health Technol Inform. 2021 Sep 21;283:127-135. doi: 10.3233/SHTI210550. PMID: 34545828. [Download PDF](https://pubmed.ncbi.nlm.nih.gov/34545828/)

### 2020

- Sass, J., Bartschke, A., Lehne, M. et al. The German Corona Consensus Dataset (GECCO): a standardized dataset for COVID-19 research in university medicine and beyond. BMC Med Inform Decis Mak 20, 341. 2020 December 21. [Download PDF](https://doi.org/10.1186/s12911-020-01374-w)
- Kankova B, Duftschmid G. Reusing Data of an EU-Wide EHR System for Clinical Trials: A Capabilities Analysis. Stud Health Technol Inform. 2020 Jun 23;271:17-22. doi: 10.3233/SHTI200069. PMID: 32578536. [Download PDF](https://pubmed.ncbi.nlm.nih.gov/32578536/)
- Bild R, Bialke M, Buckow K, Ganslandt T, Ihrig K, Jahns R, Merzweiler A, Roschka S, Schreiweis B, Stäubert S, Zenker S, Prasser F. Towards a comprehensive and interoperable representation of consent-based data usage permissions in the German medical informatics initiative. BMC Med Inform Decis Mak. 2020 Jun 5;20(1):103. doi: 10.1186/s12911-020-01138-6. PMID: 32503529; PMCID: PMC7275462. [Download PDF](https://pubmed.ncbi.nlm.nih.gov/32503529/)
- de Ridder S, Beliën JAM. The iCRF Generator: Generating interoperable electronic case report forms using online codebooks. F1000Res. 2020 Feb 4;9:81. doi: 10.12688/f1000research.21576.2. PMID: 32566137; PMCID: PMC7291075. [Download PDF](https://pubmed.ncbi.nlm.nih.gov/32566137/)
- ART-DECOR Expert Group: ART-DECOR® 3.0 New Features and Feelings - Tool Update and Roadmap [Download PDF](https://art-decor.org/mediawiki/images/e/e0/Artdecor30-update-202007-web.pdf)

### 2019

- Henket A, Heitmann K. Update on the FHIR® Roadmap ART-DECOR® Tooling, September 2019 Annual Plenary & Working Group Meeting, HL7 International [Download PDF](https://art-decor.org/mediawiki/index.php?title=Special:Upload&wpDestFile=ART-DECOR_FHIR_Roadmap_201909.pdf)
- Augustinov G, Duftschmid G. Can the Austrian Nation-Wide EHR System Support the Recruitment of Trial Patients? Stud Health Technol Inform. 2019;259:87-90. PMID: 30923279. [Abstract](https://pubmed.ncbi.nlm.nih.gov/30923279/) [Download PDF](https://pdfs.semanticscholar.org/da3e/c08c692812a678182ec004e217f29c0828a5.pdf)
- Ott S, Rinner C, Duftschmid G. Expressing Patient Selection Criteria Based on HL7 V3 Templates Within the Open-Source Tool ART-DECOR. Stud Health Technol Inform. 2019;260:226-233. PMID: 31118342. [Abstract](https://pubmed.ncbi.nlm.nih.gov/31118342/) [Download PDF](https://www.researchgate.net/publication/333354346_Expressing_Patient_Selection_Criteria_Based_on_HL7_V3_Templates_Within_the_Open-Source_Tool_ART-DECOR)

### 2018

- Scott PJ, Heitmann KU: Team Competencies and Educational Threshold Concepts for Clinical Information Modelling, Stud Health Technol Inform. 2018;255:252-256. [Download PDF](https://ebooks.iospress.nl/publication/50513)
- Heitmann KU , Cangioli G, Melgara M, Chronaki C: Interoperability Assets for Patient Summary Components: A Gap Analysis; Stud Health Technol Inform. 2018;247:700-704. [Download PDF](https://ebooks.iospress.nl/publication/48882)

### 2017

- Pantazoglou E, Thoma M, van de Sand L, Dewenter H, Thun S. Standardizing the Transmission of Genomic-Related Medical Data for the Optimization of Diagnostics and Research in the Field of Oncological Diseases. European Journal for Biomedical Informatics. EJBI – Volume 13 (2017), Issue 1. [Download PDF](https://www.ejbi.org/abstract/standardizing-the-transmission-of-genomicrelated-medical-datarnfor-the-optimization-of-diagnostics-and-research-in-the-f-3807.html)
- Laurence Strasser, Torsten Loos, Christoph Rinner: Adapting ELGA templates in a local ART-DECOR instance at the Medical University of Vienna. Center for Medical Statistics, Informatics and Intelligent Systems, Medical University of Vienna, IHIC 2017 [Download PDF](http://ihic.info/2017/ihic2017_paper_4.pdf)
- Dr Kai U. Heitmann: Getting closer to 2.0: ART-DECOR® 1.8+ published. HL7 Europe Newsletter 7/2017, p44-45 [Download HL7 Newsletter 7/2017](http://www.hl7.eu/download/eun-07-2016.pdf)
- Brammen D, Dewenter H, Heitmann KU, Thiemann V, Majeed RW, Walcher F, Röhrig R, Thun S. Mapping Equivalence of German Emergency Department Medical Record Concepts with SNOMED CT After Implementation with HL7 CDA. Stud Health Technol Inform. 2017;243:175-179 [Download PDF](https://pubmed.ncbi.nlm.nih.gov/28883195/)

### 2016

- Dr K U. Heitmann: ART-DECOR® Introduction and Demo to the IHE Cardiology Technical Committee. December 8, 2016 [Download PDF](https://art-decor.org/mediawiki/images/5/58/Ihecard-artdecor-kheitmann.pdf)
- A Henket, K. Heitmann: The FHIR® Roadmap ART-DECOR® Tooling. FHIR Developer Days 2016, Amsterdam [Download PDF](https://art-decor.org/mediawiki/images/3/34/ART-DECOR-FHIR-roadmap-112016.pdf)
- Success with ART-DECOR in British Columbia – Pilot project uses ART-DECOR to capture and manage CDA Templates. Patrick E. Loyd, Co-Chair, HL7 Orders & Observations Work Group and Lorraine Constable, Co-Chair, HL7 Orders & Observations Work Group, Vice Chair Architecture Board, and Chair Standards Governance Board, with coordination from the BC Team. [Download HL7 Newletter 9/2016 PDF](http://www.hl7.org/documentcenter/public/newsletters/HL7_NEWS_20160913.pdf)
- Dr Kai U. Heitmann: CDA Introduction. Tutorial held at the International HL7 Interoperability Conference IHIC 12 June 2016, Genoa, Italy [Download PDF](https://art-decor.org/mediawiki/images/1/17/Ihic-cda-kheitmann-web.pdf)
- Dr Kai U. Heitmann: ART-DECOR® - Introduction and Demos. Tutorial held at the International HL7 Interoperability Conference IHIC, 12 June 2016, Genoa, Italy [Download PDF](https://art-decor.org/mediawiki/images/d/dd/Ihic-artdecor-kheitmann-web.pdf)
- Giorgio Cangioli: How ART-DECOR® supports the Handover of the Patient Summary Specifications to the Connecting Europe Facility. HL7 Europe Newsletter 6/2016, p21-26 [Download HL7 Newsletter 6/2016](http://www.hl7.eu/download/eun-06-2016.pdf)
- Nikos Kyriakoulakos, Aram Balian: A Case Study for Hemodialysis Patient Referral based on HL7 CDA R2 using ART-DECOR®. HL7 Europe Newsletter 6/2016, p32-35 [Download HL7 Newsletter 6/2016](http://www.hl7.eu/download/eun-06-2016.pdf)
- Dr Kai U. Heitmann, FHL7: ART-DECOR Templates for the Impatient: C-CDA 2.1 and Temple. HL7 Newsletter January 2016 [Download PDF](https://art-decor.org/mediawiki/images/4/4d/Hl7newsletter201601.pdf)

### 2015

- Kai Heitmann (DE): CDA Introduction, held @ART-­DECOR Developer Day at IHIC 2015, 9th February 2015, Prague, Czech Republic [Download PDF](https://art-decor.org/mediawiki/images/b/b5/T1-ART-DECOR-devday-tutorial1-96.pdf)
- Kai Heitmann (DE): ART-­DECOR Basics and User Perspective, held @ART-­DECOR Developer Day at IHIC 2015, 9th February 2015, Prague, Czech Republic [Download PDF](https://art-decor.org/mediawiki/images/8/86/T2-ART-DECOR-devday-tutorial2-96.pdf)
- Kai Heitmann (DE): ART-DECOR for Specification Developers, held @ART-­DECOR Developer Day at IHIC 2015, 9th February 2015, Prague, Czech Republic [Download PDF](https://art-decor.org/mediawiki/images/7/7b/T3-ART-DECOR-devday-tutorial3-96.pdf)
- Kai Heitmann (DE): ART-DECOR for Software Developers/Implementers, held @ART-­DECOR Developer Day at IHIC 2015, 9th February 2015, Prague, Czech Republic [Download PDF](https://art-decor.org/mediawiki/images/1/14/T4-ART-DECOR-devday-tutorial4-96.pdf)
- Abderrazek Boufahja (IHE Europe, FR): IHE Gazelle ObjectsChecker : Concepts, Benefits, Demonstration and Access, held @ART-­DECOR Developer Day at IHIC 2015, 9th February 2015, Prague, Czech Republic [Download PDF](https://art-decor.org/mediawiki/images/a/a2/T5-ART-DECOR-devday-presentation5-96.pdf)
- Maarten Ligtvoet (Nictiz, NL): ART-DECOR for software implementers: validation, held @ART-­DECOR Developer Day at IHIC 2015, 9th February 2015, Prague, Czech Republic [Download PDF](https://art-decor.org/mediawiki/images/7/76/T6-ART-DECOR-devday-presentation6-96.pdf)

### 2014

- Justin Fyfe: Demonstration of the Sherpas toolkit for representing DECOR based CDA templates in C#. Recorded at the HL7 WGM Phoenix (US), May 2014. [Watch video at youtube](https://www.youtube.com/watch?v=p5oasVIQaNE)
- Kai Heitmann: ART-DECOR Creating Templates with Prototypes and Building Block Repositories. Held at the HL7 WGM Phoenix (US), May 2014. [Download PDF](ART-DECOR-tooling-kheitmann-web-20140507.pdf)

### 2013

- Marc de Graauw: ART DECOR: an XML framework for medical metadata, Held at XML Amsterdam 2013 [Download PDF](https://art-decor.org/mediawiki/images/4/4e/ART_DECOR_-_XML_Amsterdam_2013.pdf)
- Kai Heitmann: Representing (CDA Section and Entry Level) Templates in DECOR. Held 2012-04-05 Templates Working Group. [Download PDF](https://art-decor.org/mediawiki/images/6/6e/Kh-templates-presentation-20120405-2.pdf)

## Articles in Papers

- [HL7 Mitteilungen 32/2013](http://art-decor.org/downloads/downloads/HL7_Nr_32_2013_low_art_decor.pdf) 2013-04-09 (de-DE)
- [HL7 Europe News #4](http://www.hl7.eu/download/eun-04-v2014.pdf) 2014-May (en-EN)
- [HL7 International Newsletter](http://www.hl7.org/documentcenter/public/newsletters/HL7_NEWS_20140908.pdf) September 2014
- [Using ART DECOR for Code Generation](http://www.marcdegraauw.com/2015/02/20/using-art-decor-for-code-generation/) February 2015
- [Code Generation Strategies for ART DECOR](http://www.marcdegraauw.com/2015/02/27/code-generation-strategies-for-art-decor/) February 2015

## Citations

- Kai U. Heitmann: ART-DECOR: Brücke zwischen Experten aus Medizin und IT. HL7-Mitteilungen Nr. 32/2013, 12-18
- Kai U. Heitmann, Gerrit Boers, Alexander Henket, Maarten Ligtvoet, Marc de Graauw: ART-DECOR: a tool bridging the chasm between clinicians and health IT. HL7 Europe News 4/2014, p24-30
- Kai U. Heitmann: ART-DECOR: An Open-Source Tool Bridging the Chasm Between Clinicians and Health IT. HL7 International News, September 2014, 1-4
- Roman Radomski, Sebastian Bojanowski: Lessons Learned from the Transition of the Polish National Draft Implementation Guide for HL7 CDA Standard into DECOR Format. Proceedings 15th International HL7 Interoperability Conference (IHIC 2015), 45
- Sebastian Bojanowski, Roman Radomski: DECOR Driven Framework for Rapid Development of HL7 CDA Document Editor Components of EHR Systems. Proceedings 15th International HL7 Interoperability Conference (IHIC 2015), 46
- ART-DECOR: Advanced Requirement Tooling and Data Elements, Codes, OIDs and Rules, art-decor.org
- Philippe Finet, Bernard Gibaud, Olivier Dameron, Régine Le Bouquin Jeannès: Relevance of health level 7 clinical document architecture and integrating the healthcare enterprise cross-enterprise document sharing profile for managing chronic wounds in a telemedicine context. Healthc Technol Lett. 2016 Mar; 3(1): 22–26.
- Gunter Haroske, Thomas Schrader: A reference model based interface terminology for generic observations in Anatomic Pathology Structured Reports Diagn Pathol. 2014; 9(Suppl 1): S4.
