- ---
  permalink: /documentation/project/
  ---

# Project

The Project Menu offers the following areas:

- an **[Overview](#overview)** of the selected project
- **[Version Information](#version-information)** containing information about releases and publications of the project information)
- **[History](#history)** of artefacts of the project
- a list of **[Identifiers](#identifiers)** used in the project
- Forms with ADA (not yet implemented)
- a **[Development](#development)** area to check your project for consistency, e.g. before publications, or to create validation scripts and methods.

If you are logged in and an author of the project, the following additional areas are offered:

- a list of **[Authors](#authors)** contributing to and associated with the project.
- a panel to see and maintain **[DECOR locks](#decor-locks)** on artefacts
- entry to the **[MyCommunity](#mycommunity)** area offering community specific information
- the section about **[Governance Groups](#governance-groups)** the project belongs to

## Overview

![image-20220726192149227](../../img/image-20220726192149227.png)

In this Panel <ref1/> you get an overview about the project, its Statistics <ref2/>, Properties and Settings  <ref3/>. There are also cards for Copyright Information <ref4/>, Scheduled Project Tasks <ref5/> and defined Services <ref6/>. The overview is completed by the list of referenced Repositories <ref7/> and Namespaces used <ref8/>. 

Depending on whether you are a logged-in project author or not, additional options appear.

### Statistics Cards

Below Project information, description and icon you see the Statistics Cards <ref2/> that are also clickable for direct access to [Datasets](https://docs.art-decor.org/introduction/dataset/), [Scenarios](https://docs.art-decor.org/introduction/scenario/), [Terminology](https://docs.art-decor.org/introduction/terminology/), [Templates](https://docs.art-decor.org/introduction/template/), [Issues](https://docs.art-decor.org/introduction/issue/) and [Publications](https://docs.art-decor.org/introduction/publication/).

### Properties Card

In the Properties Card <ref3/>, Identifiers, Languages, Switches and Publication Locations are shown. Clicking on a tab opens it, clicking again on an open tab collapses the Property Card tabs.

**Identifiers** gives you an overview of the project prefix, id and last modified date.

![image-20220726175452839](../../img/image-20220726175452839.png)

In the **Project Languages** tab you can edit the name of the current project in one or more different languages or add new languages to the project. You also can see and switch which language is the so-called default language. There are comprehensive explanations in this area.

![image-20220726175645618](../../img/image-20220726175645618.png)

The  **Switches** tab allows to make the project private or public, to declare that the project contains reusable content, to declare the project as experimental/test or not and to switch the notifier on or off. There are comprehensive explanations in this area.

![image-20220726175803319](../../img/image-20220726175803319.png)

The **Publication Location** tab gives you the option to edit the location of the publication or to delete the location. It also allows you to overview the publications-related external processes, the Release and Archive Manager (ADRAM) and the Automated Wiki Bot (ADAWIB), if configured and parametrized for this project.

![image-20220726180034964](../../img/image-20220726180034964.png)

### Copyright Card

Copyright <ref4/> provides an overview of the Copyright Information for the current project. It is also possible to add a new Copyright or to maintain the existing ones.

<img src="../../img/image-20220726180538211.png" alt="image-20220726180538211" style="zoom:80%;" />

### Scheduled Project Tasks Card

Scheduled Project Task <ref5/> provides a list of actual running tasks for the project and their status, e.g. a publication request. If a task has not been completed it is also possible to delete the task. 

<img src="../../img/image-20220726180844615.png" alt="image-20220726180844615" style="zoom:80%;" />

### Services Card

Services <ref6/> contains a list of services defined for the project along with the specified format. You can add new or maintain existing ones.

<img src="../../img/img-20231102154607.png" alt="img-20231102154607" style="zoom:80%;" />

### Repositories Card

Repositories <ref7/> gives you an overview of the repository reference used for the project with repository prefix, url and type. To add a new repository reference to the project simply use the ADD button. The ADD dialog offers also a search for a specific repository.

It is not possible to un-reference (delete) a repository reference.

<img src="../../img/image-20220726181451690.png" alt="image-20220726181451690" style="zoom:80%;" />

::: note

Please be aware that BBR URLs are identifiers rather than real visitable URLs. They shall always have `http://` as the scheme.

:::

### Namespaces Card

Namespaces provides an overview of which namespaces are used in the current project. To declare a current namespace as default you can select it in the default column.

<img src="../../img/image-20220726181803612.png" alt="image-20220726181803612" style="zoom:80%;" />

You can also add a new namespace by clicking the ADD-button.

::: warning

Please be cautious and sure that you know what you are doing when maintaining namespaces. Unthoughtful actions on namespaces may even make the project unavailable to you or may cause errors on validation.

:::

### License Management Card

ART-DECOR® is a **promotor of open standards** and fosters to make specifications findable, accessible, and optimize their re-use.

If interoperability specifications and especially content for building block repositories shall be truly open source, project authors/responsibles **need to license it** so that others are free to use, change, and distribute the specifications.

Specification of **License information** for DECOR Projects is available and appears on a special License Management Card. In the Project Overview Panel, right after the Copyright Card the new License Card appears.

<img src="../../img/image-20230620213221524.png" alt="image-20230620213221524" style="zoom:70%;" />

As a Project Author you can assign a license to the project as a whole. When clicking on the ADD + button in the License Card a dialog is opened that offers a list of supported licenses.

<img src="../../img/image-20230620213406155.png" alt="image-20230620213406155" style="zoom:60%;" /> 

The list is using an [SPDX license Identifiers](https://spdx.org/licenses/), or a special case 'not-open-source'. Once saved the license information can be edited or deleted.

<img src="../../img/image-20230620213913896.png" alt="image-20230620213913896" style="zoom:67%;" />

A project can have at most one license. We in the team discuss whether we suggest having a default license for new projects in the future.

## Authors

The Authors panel is only available if you are logged-in project author. It provides an overview of all authors of the project, their name and whether their account is active or not. More details like email address, subscriptions and expiration date can be seen when expanding the row. It is also possible to add a new author the current project via the + button.

![image-20220726182752533](../../img/image-20220726182752533.png)

## Publications

The Publications panel provides the list of existing publications and gives you the option to add a new publication to the current project.

![image-20220726183148847](../../img/image-20220726183148847.png)

The status of a publication or publication request is shown in the status column.

<img src="../../img/image-20220726183906233.png" alt="image-20220726183906233" style="zoom:50%;" />

An **Intermediate Version** acts as a kind of backup of the current state of the project. For this purpose the project is historicized as a whole, this is shown with a green hollow diskette symbol <ref1/>. 

For **Releases**, option 1 is to only save the project, as a "freeze". This saved-only release is not published elsewhere but can act as a starting point for further releases of the project and the corresponding release notes. The saved-only release is denoted with a green diskette symbol <ref2/>.

If a Release is properly processed and **published at its Publication Location**, the Status if the release is indicated (in the example "active"), and the green diskette symbol says it is stored, the bookshelf symbol says it is published at its Publication Location <ref3/>. The Status of the Publication can be changed according to the Release status workflow.

Once processed in the first phase (compilation of all project artefacts needed), the corresponding icon is a clock that the external continuation process (ADRAM) <ref4/>.

#### Triggering a Release

**Preparation**

Preparation of the publication is important. Once published you may give the result a status cancelled or deprecated but there's no undo. Here's a checklist of things to consider:

- Run [Terminology Report](../terminology/terminologyreport/) and fix issues
- Run [Check DECOR](#check-decor) and fix issues
  - Also look at the warnings like missing translations in multilingual projects
- Check status for all artefacts i.e. datasets, scenarios, transactions, templates, terminology
- Check contents of publication filter if all relevant transactions are present.

**Optional extra steps**
- Run a [runtime compile](#compile-a-development-version) and validate instances against the updated specification
- Run a development compile and generate html and runtime environment against the compilation
  - Follow the steps below and check the 'development' option below the release label.

Now that you are ready: Click on the + button in the Publications Panel to initiate a new

- Intermediate Version
- Release without a Publication
- Release with Publication.

A new window with three tabs will open where you can set the options for the Publication.

In the **General** Tab you can choose for any of the three types mentioned above, either as an Intermediate Version (historicized project)...

<img src="../../img/image-20221215105508442.png" alt="image-20221215105508442" style="zoom: 50%;" />

... or as a Release <ref1/> with or without Publication Request. Set the Publication Request to on <ref2/> if the Release shall be published at the pre-defined Publication Location.

<img src="../../img/image-20221215105852398.png" alt="image-20221215105852398" style="zoom: 50%;" />

Choose a previous Release (if any) on which to base the Release Notes for this publication <ref3/>.

:::note

If you want a Release to be officially published at its pre-defined Publication Location, you must switch the corresponding switch <ref2/> to "on". It is disabled, if there is no pre-defined Publication Location set yet. Refer to the [Project Properties](#properties-card) to learn how to parametrize it.

:::

The remainder of the dialog allows you to add or refine more details for the Release. 

<img src="../../img/image-20221215110238639.png" alt="image-20221215110238639" style="zoom:50%;" />

Add Release Notes as text <ref1/>.

If a Release shall be created, assign a Release Label <ref2/>. 

You can decide to have a development / test release only <ref3/> instead of a proper publication. 

Choose the languages of the Publication <ref4/>. 

In the **Parameters** tab you can switch specific parameters on or off. Information buttons provide more information about the parameter.

<img src="../../img/image-20220726190356811.png" alt="image-20220726190356811" style="zoom:67%;" />

You can also add filters to the Publication based on one or more Scenarios via the **Filter** tab. Add a filter using the + button. A list of all possible Transactions in the project is shown. Choose one or more Transactions that shall act as a filter. It is required to give a label to the filter.

<img src="../../img/image-20220726190754942.png" alt="image-20220726190754942" style="zoom:67%;" />

A defined filter is then offered to be used for the Publication ("Select filter") in the Filter tab. Click to select the filter for the publication.

<img src="../../img/image-20220726190841693.png" alt="image-20220726190841693" style="zoom:67%;" />

::: tip

In fact a filtered publication is a Scenario/Transaction or use case based publication including only those artefacts actually belonging to the Scenario/Transaction. This is especially useful for larger project that cover multiple use cases.

:::

#### Process of Release Publication Requests

If a Release is triggered it is shown initially as a waiting-in-queue. 

<img src="../../img/image-20220726185054288.png" alt="image-20220726185054288" style="zoom:50%;" />

During the process the progress is shown.

<img src="../../img/image-20220726185132945.png" alt="image-20220726185132945" style="zoom:50%;" />

Once processed in the first phase (compilation of all project artefacts needed), the corresponding icon is a clock. The hover text tells that in this state it is waiting for the external continuation process (ADRAM).

<img src="../../img/image-20220726184844401.png" alt="image-20220726184844401" style="zoom:50%;" />

Once it is taken over by ADRAM, the progress of the second phase (the actual transformation of the release and its publication) is denoted by a gondola, along with progress information.

<img src="../../img/image-20220726185401392.png" alt="image-20220726185401392" style="zoom:50%;" />

If a release publication process fails it is displayed as an error.

<img src="../../img/image-20220726185224853.png" alt="image-20220726185224853" style="zoom:50%;" />

::: note

A number of checks can be done to improve the quality of your project before you create and publish a release. To check the quality of terminology content contained in the project you can perform a [Terminology Report](terminologyreport/).

:::

## History

The History panel provides you an overview of all edit activities of current project. It is also possible to search for a specific activity. You must be a logged-in project author to see this panel.

![image-20220726191708519](../../img/image-20220726191708519.png)

## Identifiers

The Identifiers panel provides an overview of all Identifiers used in the current project. The are divided into two sections:

- **Base Ids**
- **Other Ids**.

### Base Ids

Base Ids, shown in the lower part of the Identifier Panel, represent the set of root identifiers (OIDs) for ART-DECOR artefacts used by the project as default assignments for new Ids and for display purposes.

![image-20220726212740650](../../img/image-20220726212740650.png)

A Base Id has an OID (root), type of artefact and a so-called prefix that is used for short names for ids. It is possible to add and maintain the ids, or make a certain Base Id the default.

<img src="../../img/image-20220726225600120.png" alt="image-20220726225600120" style="zoom:67%;" />

::: example

If you create a new Code Systems where you don't have an own identification for, ART-DECOR assigns an id based on the Base Ids for Code Systems (see above). According to the recommendations of the ISO standard *ISO/TS 13582:2015(en) Health informatics — Sharing of OID registry information*, the new Code System for example gets the first free id based on the root ending on `.5` concatenated with the first free last digit of the already existing Code Systems in the project.

:::

### Other Ids

Other Ids are external Ids such as terminology Ids (OIDs or URIs) for LOINC and SNOMED etc. or for identification schemes such as the Dutch National Citizen Identifier (BSN). It is possible to add and maintain an id, add a designation or associate a URI.

![image-20220726212400441](../../img/image-20220726212400441.png)

Base Ids

## DECOR locks

In the DECOR locks panel you have an overview of all locks of the current project. You can see which type the lock is, the name, prefix and how many days it has is locked. It is also possible to delete a lock, either one-by-one or for a whole holder of locks.

![2022-06-27_11-22-22.png](../../img/2022-06-27_11-22-22.png)

## MyCommunity

In this panel you have direct access to the MyCommunity board.

![image-20220728140338126](../../img/image-20220728140338126.png)

## Governance Groups

This panel allows you to create and maintain Governance Groups.

![image-20220728140506576](../../img/image-20220728140506576.png)

## Development

The Development Panel offers various features for authors of a project during developers of a specification. The features include:

- to **check** the underlying internal specification file, called DECOR Project file.
- to **compile a development version** based on the current status of the Project
- to **manage** ART-DECOR **artefacts** of the Project
- to create and maintain Terminology Reports of the Project
- to create and maintain Generic and Sanity Reports of the Project.

The features are arranged with tabs, initially the CHECK DECOR tab is selected.

 ![image-20220728143116746](../../img/image-20220728143116746.png)

### Check DECOR
The CHECK DECOR tab gives you the option to validate the project, in essence the underlying internal specification file (DECOR file). If there has been a previous check you can see when the  results.

<img src="../../img/image-20220728144744756.png" alt="image-20220728144744756"  />

#### Define Project Checks

To validate the project you click on the CHECK DECOR button and a new window will open where you can configure optional checks and other settings. When you click on the Information icons you can see what each configuration means in detail. Once you've selected the configuration option you need, you can then click on the NEXT button.

<img src="../../img/image-20220728143633945.png" alt="image-20220728143633945" style="zoom:67%;" />

In the next step you can select or specify filters on which transaction(s) you wish to perform the check. To add a filter click on the + button and a new window will open where you have the option to filter a specific transaction to validate. If you don't need to filter out you can directly click on the NEXT button.

Adding a label to the selected transaction is required, this is the name of the filter the selection. 

<img src="../../img/image-20220728143915217.png" alt="image-20220728143915217" style="zoom:67%;" />

You can then click on save and the filter will be added to the DECOR CHECK where they can be selected/de-selected for the actual check.

<img src="../../img/image-20220728144010415.png" alt="image-20220728144010415" style="zoom:67%;" />

By clicking on the NEXT button you come to the last part of the DECOR CHECK dialog where you see an summary of the configuration, filter and compiled settings. 

<img src="../../img/image-20220728144115985.png" alt="image-20220728144115985" style="zoom:67%;" />

You can then click on CHECK DECOR. As soon as you start the CHECK DECOR and it's ready the current window will automatically refresh.

#### Results of a Check
The results of the DECOR check is done through schema and schematron validation and are listed below where you have an overview of the different part of the validation.

<img src="../../img/image-20220728144533675.png" alt="image-20220728144533675" style="zoom:67%;" />

It is also possible to filter out the results of the check / validation using the filter icon.

<img src="../../img/image-20220728144556109.png" alt="image-20220728144556109" style="zoom: 67%;" />

### Compile a development version
You can Compile a Development Version of your project which essentially compiles your project in current state with a number of options:

- create schematrons (open or close)
- create explicit includes (an Information button tells more about this option)
- select one or more Transactions from your project to set the focus on the selected ones.

<img src="../../img/image-20220728145143486.png" alt="image-20220728145143486" style="zoom:67%;" />

The overall result is shown after processing in the list. Historic compiles are store also but marked automatically as deprecated if they are the third or higher generation of compilations.

<img src="../../img/image-20220728145721364.png" alt="image-20220728145721364" style="zoom:67%;" />

You can also delete or download an existing schematron package or do a live validation of an XML instance against the compiled schematron packages. 

<img src="../../img/image-20220728145807039.png" alt="image-20220728145807039" style="zoom: 67%;" />

#### Live Validation of Instances

You can validate XML instances against your development packages directly in the tool. Choose first a compiled environment from the list.

A dialog opens and offers you to **Upload** the instance to be validated and to select a Transaction from the list. Then you click on VALIDATE.

<img src="../../img/image-20220728150339332.png" alt="image-20220728150339332" style="zoom:80%;" />

The results of the validation (schema and schematron from the development environment) are shown in the **Report** tab.

<img src="../../img/image-20220728150602344.png" alt="image-20220728150602344" style="zoom:67%;" />

In case the creation or use of the validation scripts (SVRL) fails, an error message is displayed accordingly instead of the schematron validation result.

<img src="../../img/image-20240930180539372.png" alt="image-20240930180539372" style="zoom:40%;" />

### Artefact management

Under this tab options are listed to **manage** ART-DECOR **artefacts** of the Project, namely Adding Ids to Template element and attribute definition and Artifact Identifier Management.

<img src="../../img/image-20240730192224608.png" alt="image-20240730192224608" style="zoom:67%;" />

#### Adding IDs to Template element and attribute definition

If this project is a Building Block Repository it is recommended that all Template/Profile element and attribute definitions carry an internal id attribute. Otherwise this may lead to problems for others to re-use Templates/Profiles as prototypes. Therefore is is possible to add internal id's to all Template elements and attributes in the project. This is rarely done manually as ART-DECOR tries to assign internal id's to all Template elements and attributes when maintaining Templates.

#### Artifact Identifier Management

In edge cases you may want to change the ID of an existing artifact. This should be done only in the development phase of an artifact. The Artifact Identifier Management dialog offers you options to select an artifact and change its ID.

You first choose the type of artifact you want to change using the popup menu at the button.

<img src="../../img/image-20240730192418276.png" alt="image-20240730192418276" style="zoom:67%;" />

Dependent on the chosen type the opening dialog offers all items on a left sided navigation card. When selection an item, the dialog to the right shows all options for the ID change. 

<img src="../../img/image-20240730192657260.png" alt="image-20240730192657260" style="zoom:67%;" />

The top card shows some item details, the bottom card allows to change the ID and lists all affected artifacts with some details. In case the ID is already exsting the item is shown are saving the change is disabled.

<img src="../../img/image-20240730193207354.png" alt="image-20240730193207354" style="zoom:60%;" />

You may add the New ID for the change.

::: note

OID pattern is `[0-2](\.(0|[1-9][0-9]*))*`. An OID identifies exactly one object and SHALL NOT be reused. OIDs of three nodes or less are always reserved for a purpose. See [https://oid-info.com](https://oid-info.com/) or the hosting servers' OID index when in doubt.

:::

When the new ID is ok from the admins perspective the save button changes the item's ID.

::: note

When changing the ID, ART-DECOR does not automagically change all dependencies. Changing IDs is on users own responsibility and possible reference changes that were listed in the affected item list need to be changed manually.

:::

#### Terminology Reports of the Project

You can trigger and maintain Terminology Reports of the Project.

It is possible to initiate a Terminology Report to check the terminologies used in your project. The report detects whether a coded concept in a value set has an illegal display name or is deprecated and should no longer be used. Usually a Terminology Report check will be perfomed before creating a publication, as part of preflighting publication and quality checks but could of course be run more often. For projects with large or many value sets this can be a lengthy process.

You can trigger a new Terminology Report by clicking on the add (+) button. The request is added to the process queue and handled in the background. Once finished it appears in the list of Terminology Report of the Project.

<img src="../../img/image-20241115201938575.png" alt="image-20241115201938575" style="zoom:67%;" />

#### Generic and Sanity Reports of the Project

You can trigger and maintain Generic and Sanity Reports of the Project. While this is still an experimental but continuously maturing area, you can trigger already various types of reports. Stay tuned to learn more in the near future.

<img src="../../img/image-20241115202501288.png" alt="image-20241115202501288" style="zoom:67%;" />

As of now the following report is available:

**Scenario Primary-Secondary Dataset Sources (PSD)** shows origin of scenario concepts in the inheritance chain through datasets. The following report is a rendition of such a report. 

![image-20241115202554133](../../img/image-20241115202554133.png)

### Project Index

The Project Index allows you to inspect summaries of all ART-DECOR artefacts of your Project. Use the artefact tabs to flip between the different kinds of artefacts. Each tab also offers a variety of download options in different formats through the colored pills.

![image-20220728151301581](../../img/image-20220728151301581.png) 

As usual green pills are used for Care provider oriented artefacts, blue pills for Terminologist and red pills for Data Format Specialists and Technicans.

![image-20220728151400568](../../img/image-20220728151400568.png)



