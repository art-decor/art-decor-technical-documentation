---
permalink: /documentation/releasenotes/release31
---
# ART-DECOR® – Release 3.1

Release notes 23 August 2021

## Introduction

ART-DECOR® is an open source tool suite that aims at interoperability solutions in the healthcare sector. 

This year October 1st, it’s ART-DECOR’s 12th birthday. Reason enough to take a short look back in history and to look forward on what we call **ART-DECOR Release 3**. And the future starts now.

ART-DECOR® Version 3 will be published step by step. After the very first Release 3.0 in July that has been started as a Canary' Release, now Release 3.1 (beta) is out and brings along many new features and improvements in the coming releases.

| ***Canary Release**                                          |
| ------------------------------------------------------------ |
| ... is a release for Early Birds who volunteer to test it and runs on build.art-decor.org (international) and art-decor3.test-nictiz.nl (Netherlands focus) |

To summarize the topics of this and future planned ART-DECOR Releases 3

- New UI / UX
- Microservices
- Better Scalability, higher Reliability
- Extended Documentation Platforms
- Centralized ART-DECOR® Terminology Services (CADTS)
- More FHIR Support (Repository, Profiling)

## Overview

**Platform upgrade**

A new user interface with an improved user experience (UX); a new framework (Vue) replaces the Orbeon® platform. The eXist® database (exist-db.org) remains as the well-founded backend; an update to the latest version has been done to improve performance and stability.

**New functionality**

Bug resolutions related to platform upgrade has been performed.

## **Release 3.1 for Canaries and Early Birds** 

If you want to be part of the first wave visitors of Releases of ART-DECOR, please let us know. You may stray around on build.art-decor.org without a password to just see what the new interface looks like. If you want to test the first feature, please let us know ([e-mail us](mailto:support@art-decor.org)) and we assign a username and password to you and point to practical projects.

In this Release 3.1, the Project Panel (part 2), Datasets (part 2), Scenario’s (part 1), Terminology, Identifiers, OIDs and first Concept Maps are available to investigate. In addition, there are intense preparations underway to allow a transition and migration phase later in August, first for the Dutch community, later in September for everybody, to have both, Release 2 (Orbeon-based) and the new Release 3 software, in use for production. It is the plan to completely migrate all functionalities in Release 2 to Release 3, if approriate, and to close the use ofthe old  Release 2 software soon. 

![600 pixels wide image](../../../img/roadmap31.png)](htt

Please find a short journey through the landing page and subsequent Panels in the new Release. Release 3 first covers to see a Landing page where you can choose a project to inspect or work with.

![600 pixels wide image](../../../img/landingpage30.png)

Choose a project and start your endeavors. Please provide feedback if you like through the provided link.

![600 pixels wide image](../../../img/choose30.png)Once a project is chosen, see the Project Overview and continue from there.

![600 pixels wide image](../../../img/projectoverview30.png)

You can go tho the main subject of this release: datasets with the navigation tree on the left and details shown on the right. Just try the new look-and-feel with our example datasets or a selection of "real" datasets coming from different projects.

![600 pixels wide image](../../../img/datasetdetails30.png)

The Scenario Panel offers the scenarios in the usual hierarchical way, the Transaction Groups (next figure), if possible with Actor interaction diagrams and the first version of the Transaction Leaf Panel (figure after next figure) with the details.

![600 pixels wide image](../../../img/scenario-tr-gr.png)



![600 pixels wide image](../../../img/scenario-tr-l.png)

## Resolved in Release 3.1

The following taks have been resolved. Please note: *AD-XXX* is the reference to the Nictiz ART-DECOR ticket register. AD30-XXX is the reference to the ADEG (ART-Decor Expert Group) ticket register.

- New Landing Page, Project Overview Page and Datasets Page with the navigation tree on the left and details shown on the right. 
- Database upgrade (to version 5.3.0). [AD-124; AD30-29, AD30-32, AD30-33, AD30-66, AD30-37, AD30-153]
- New functionality: add new data set in project [AD-149; AD30-105]
- New functionality: add SNOMED CT code with terminology link [AD-147; AD30-244]
- New functionality: add metadata (copyright) in dedicated fields [AD-168; AD-30-243]
- Bug: eXist-db XSLT with namespaces [AD-34]

### Known errors/missing functionality

As Release 3.1 is the beta version of ART-DECOR version 3 there are known errors and compared to ART-DECOR version 2 there is missing functionality. The following main areas (main menu items) are still under development and therefore considered missing functionality:

- Scenarios and Transactions
- Profiles

The other areas are available in Release 3.1 and the specific known errors and missing functionality for these areas are listed. Corresponding issues are registered in the issue register (AD-XXX reference is given) and the resolution can be followed in Jira.

## Resolution details

### Landing page

New landing page where you can choose a project to inspect or work with.

![img](../../../img/clip_image002-0707781.jpg)

**Known errors**

- Language needs to be set again on each page [AD-190]

### Project overview

![img](../../../img/clip_image003.jpg)

You can go to different areas with the navigation tree on the left and details shown on the right.

**Known errors**

- URL not editable or clickable (Publication Location) [AD-191]

**Missing functionality**

- View publications [AD-192]

The so-called Project Panel (Overview, Authors, Identifiers, Release List, History), Issues and Datasets are available to investigate.

| ![img](../../../img/clip_image004-0707781.jpg) | ![img](../../../img/clip_image005.jpg) |
| ---------------------------------------------- | -------------------------------------- |

### Datasets

![img](../../../img/clip_image006.jpg)

**Known errors**

- Dataset names for which there is no translation available show empty [AD-193]
- Edit button not working [AD30-260], [AD-194]

**Missing functionality**

- Concept ID (DETAILS) [AD-195]
- used/used by (data set items) [AD-196]
- Value set relation (data set concepts) [AD-197]
- Nesting (concept lists) [AD-198]
- No value set (inherited items) [AD-199]

### Terminology

#### Value Sets

![img](../../../img/clip_image007.jpg)

**Missing functionality**

- All values are shown (no distinction for ʻdeprecated valuesʼ), not all full/explanation names are shown (just code) [AD-200]

#### Terminology Identifiers

 ![img](../../../img/clip_image008.jpg)      

#### Terminology Mappings

![img](../../../img/clip_image009.jpg)

***Missing functionality***

- Browser [AD-201]

### Issues

#### Issues List

### ![img](../../../img/clip_image010.jpg)      

#### Labels

![img](../../../img/clip_image011.jpg) 

### Join the Journey

A recent survey among users and interested parties of the ART-DECOR tool suite unveiled that they want to use the platform more than in the past. They are curious about the new user experience and look forward to seeing new features.

The team will do its best to deploy good and efficient software. Please feel free to join the new paths of our journey towards more interoperability in healthcare.

There will be follow-up information on the next releases soon.

## Outlook next release: ART-DECOR® ‒ Release 3.2

The current development is on:

- Resolution known errors and missing functionality in Release 3.1
- Analysis and resolution of new identified bugs in Release 3.1
- Performance improvement (building on improvements from platform upgrade)

## Stay tuned

As you can see: more on that to come. Thank you for your patience. Stay tuned.
