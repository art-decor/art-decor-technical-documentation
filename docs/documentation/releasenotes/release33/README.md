---
permalink: /documentation/releasenotes/release33
---
# ART-DECOR® – Release 3.3

Release notes 31 January 2022

## Introduction

The ART-DECOR® tool suite is migrated from the current Release 2 to the new Release 3 family. This development started in 2020 and was successfully continued in 2021 and beyond. 

Recently, Release 3.3 was published that brings along many new features and improvements. This is the Release Note with more details.

## General Remarks

Release 3 introduced our new user interface with an improved user experience (UX) utilizing a new framework (Vue and Vuetify) that replaces the Orbeon® platform (XForms framework) and Tomcat (the so far used Open-Source Implementation of technologies such as Java Servlet, JavaServer Pages, Java Expression Language and WebSocket). 

The eXist® database (exist-db.org) remains the well-founded backend. Here, an update to the latest version has been done to improve performance and stability. A Database upgrade to version 5.3.x has been done, with major improvements on stability, performance, and caching. The log4j problem has also been addressed with the upgrade to database version 5.3.1.

All in all, we made good progress on the migration, taking many opportunities for functional improvements. The major remaining topics in our "waiting room" are FHIR Profiles and Questionnaires as well as renewed and enhanced options for Implementation Guides and publications. The conceptual work has been done, and we are confident that Release 3.4 and 3.5 these areas will successfully be added to the tool suite.

For beginners we added our new duplex server *newbie.art-decor.org* that hosts one database and both ART-DECOR Releases 2 and 3. This duplex production environment is set up for serving beginners, students and participants of workshops and for training.

## New functionalities and fixes in Release 3.3 at a glance

The following sections focus on the various artefacts and shows the new functionalities (compared to ART-DECOR Release 3.2, compared to ART-DECOR Release 2 shown **in bold**).

::: note

The numbers shown are the ART-DECOR Expert Group ticket identifiers.

:::

**Navigation Panels**

- Navigation Panels that offer hierarchical trees such as Datasets, Scenarios and Templates now got an **expand button** to directly expand the whole tree for overview [AD30-456].

**Projects** (fix)

- A few fixes have been applied in the creation process of new Projects.

**Datasets** (mostly fixes)

- Clicking on inherited links showed only basic information from the inherited concept. This has been extended to show more details of an artefact (e.g., an inherited concept) [AD30-457].
- Relations for inherited and contained Dataset Concept were editable where they should not be editable. This has now been corrected so that these relations cannot be edited anymore [AD30-487].
- Containment descriptions were editable where they should not be. This has now been corrected so that these descriptions cannot be edited anymore.
- Dataset clone operation did not properly assign concept identifiers [AD30-485].
- Dataset new operation got a fix on an issue in the creation of a Dataset in the Navigation Tree [AD-362].

**Scenarios and Transactions**

- Scenarios, Transactions, including Representing Templates and Scenario Actors are now editable.
- Clicking on Representing Template and Source Dataset showed only basic information from the related concept. This has been be extended to show more details of an artefact (e.g., template or dataset) [AD30-457].

**Terminology**

- The **Terminology Browser** with quick cross code system browsing has been enhanced in order to support Value Set Edits, in contrast to simple code system browsing.
- The Value Set Panel already allowed for navigation and view details of the Value Sets. Now, creation and editing of Value Sets has been added, with lots of improvements and an **optimized User Experience** [AD30-521, AD30-522, AD30-523, AD30-524, AD30-525, AD30-526, AD30-528, AD30-529]
- While the metadata of the Value Set can be directly edited/changed in the Panel (micro edits), a Concept List got an EDIT button to allow the edit. Once clicked the user is **redirected to the Terminology Browser** with the Composition. The user can search and add concepts, delete single concepts, and add single concepts to the Composition
- Adding a single concept to a Value Set now includes Complete Code System, Include Value Set, Single Coded Concept Inclusions and Exclusions with operators and Exceptions [AD30-530, AD30-532, AD30-541]
- The Create/Edit **Composition Item Dialog** for Value Sets has been enhanced to cover all functionalities [AD30-550].
- The Terminology Panel for Value Sets shows now all metadata available [AD30-551].
- [Owls](https://docs.art-decor.org/documentation/principles/#owl) have been added to the Metadata dialog (Composition) when creating new Value Sets, steps have been improved  [AD30-552, AD30-525].
- The Terminology Browser no longer has a "Search" column in **Language Selector Dialog** (not needed anymore) [AD30-549].
- The Terminology Panel for Value Sets now shows the loader as in other Panels [AD30-542].

**Rules (formerly Templates)**

- This area covers view, add, and manage constraint rules, here: templates, especially for HL7 v3 and document specifications based on the Clinical Document Architecture CDA.
- All metadata for Templates can be micro-edited now, the view has been improved  [AD30-491]
- All items of a Template Design Body are now displayed [AD30-61, AD30-504].
- Names of included or contained Templates are shown [AD30-500]
- The Template Relationship, Usage and Example Area has been improved [AD30-501]
- Navigation, grouped by type, and details including almost all items of the Template Design Body
- Note: the name change of this area has been performed to address FHIR Profiles in one of the next Releases.

**Issues** (fix)

- The Issue New Event Dialog Button shows a description field, but if text is entered there the NEW EVENT button was not enabled. This has been fixed [AD30-554].

**Repositories**

- The DECOR cache for repository projects in AD2 was either refreshed manually through server functions or was refreshed periodically by an ART-DECOR External Script called ADRESH (ART-DECOR Refresher service).
- While triggering refresh manually is also present in AD3 through Server Admin Functions / Server Management, a new automatic and period refreshing mechanism has been implemented in AD3 as an internally scheduled process [AD30-514].

## Incoming features on further Releases

ART-DECOR Release 3 can be seen as a horizontal move of all ART-DECOR Release 2 features, plus adding new features immediately if appropriate or later as planned. Compared to Release 2, ART-DECOR Release 3 still must migrate some of the functionality.

The following main areas (main menu items) are still under development and therefore incoming functionality in the following Releases:

Scenarios

- Options to ADD or CLONE a Scenarios / Transactions is under intense testing and will be released with a planned intermittent Release 3.3.1

Terminology (read-only in 3.3)

- Edit and Maintain Code Systems
- Edit and Maintain Terminology Mappings

Rules

- Templates (read-only in 3.3)
- Concepts will been improved for editing a Template Design Body, introducing a Template **Structure Context** Menu  [AD30-494], a Template **Property Context** Menu  [AD30-493], in-column edits  [AD30-495], an **Undo-Function** for the Template Design  [AD30-510].

Questionnaires

- Questionnaire (QQ) and Questionnaire Response (QR) as defined in the FHIR standard plus an ART-DECOR wrapper to allow consistent handling like all versional artefacts will be introduced from Release 3.4 on.
- Questionnaires Enhanced (QE) will also be introduced, starting with Release 3.4: once Questionnaires has been edited/enhanced by the user with for instance: layout instructions, conditions, it migrates its status to an Questionnaires Enhanced (QE) in order to be distinguished from genuinely generated Questionnaires in ART-DECOR.

We also zoom in on the areas in detail which are ready to be used to provide you with more details about the area.

## Resolutions details for Scenarios, Terminology, Rules

### Navigation Panels

Navigation Panels that offer hierarchical trees such as Datasets, Scenarios and Templates now got an expand button to directly expand or collapse the whole tree for overview.

![image-20220126124201066](../../../img/image-20220126124201066.png)

### Scenarios, Transactions and Questionnaires

The areas in Scenarios are *Scenarios* with Transactions, *Actors* and *Questionnaires*.

#### Scenario and Transaction Panel

The Panel offers all Scenarios and nested Transaction Groups, and Transaction Leaves appear in the Navigation Tree. Transaction Groups displays a sequence diagram with actors.

The right detail area varies depending on the items chosen in the navigation. Transaction groups typically show Sequence Diagrams (in new style with actors.,

![image-20220126155522063](../../../img/image-20220126155522063.png)

The Transaction Leaves show their Representing Template along with details of the Transaction Concepts such as a view on the corresponding Dataset Concept, Cardinalities, Conditions etc.

#### Edit Representing Template

A Transaction Leaf containing a Representing Template, that documents, which Dataset Concepts are linked to a Transaction Leaf. This has been fully implemented now.

The Representing Template in view mode shows the selected Concepts in condensed format.

![image-20220126155814937](../../../img/image-20220126155814937.png)

Release 3.3 makes the edit mode available. When starting the edit, the condensed view is replaced by a more detailed view of the Representing Template Tree with edit options.

![image-20220126155929752](../../../img/image-20220126155929752.png)

The edit options include management of

- selected from underlying Dataset or not
- Cardinality
- Conformance
- Conditions.

All items of the underlying Dataset are shown in this view and items are marked whether they are selected or not. In the example below, *Datetime* and *Comments* are part of the Scenario Transaction / Representing Template, *Length* and *Measured by* are not.

![image-20220126160125842](../../../img/image-20220126160125842.png)

If the user decides to include a concept in the Scenario Transaction / Representing Template, a pop-up offers all typical Cardinality and Conformance combinations from which the user may choose and may edit Cardinality and Conformance also later.

![image-20220126160021077](../../../img/image-20220126160021077.png)

#### Incoming functionality

Incoming functionality for Scenarios:

- Cloning Scenarios, Transactions is under intense testing and will be released with a planned intermittent Release 3.3.1 

- The fields *trigger* and *condition* will be in a consistent layout, the change of type of transaction will be possible [AD30-569], planned to be coming with intermittent Release 3.3.1
- Transforming Scenarios into Questionnaires, being visible in the Questionnaire Panel; Edit Questionnaires, Questionnaires Prototype Cycle including Questionnaires Response.

### Terminology

The areas in Terminology are *Value Sets*, *Identifiers*, *Mappings*, *Code Systems* and *Terminology Browser*.

The Terminology Panel is now fully ready for use, virtually all parts are available. We will zoom in on the specific areas to inform you about the missing functionality.

#### Value Sets

The Panel offers the list of all Value Sets of the Project, sorted alphabetically and indicating whether they are in-project Value Sets (plain colored icons) or references from a Building Block Repository BBR (chain symbol).

![image-20220126161021364](../../../img/image-20220126161021364.png)

Once a value set is chosen on the left, the right Card shows the details of the Value Set (metadata) including the Concept List.

![image-20220126120314693](../../../img/image-20220126120314693.png)

Concept Lists are now fully featured to show extensional and intensional Value Set definitions (Compositions).

![image-20220126161157393](../../../img/image-20220126161157393.png)

Intensional Value Set definitions are not expanded but shown as the definition itself, e.g. *Include*.

![image-20220126122154735](../../../img/image-20220126122154735.png)

In a later Release, the option to view (and download) the Expansion Set will be included.

Our Terminology Browser is not only used for browsing. If a user is the author of a project in ART-DECOR, he can create new and modify already existing valuesets. For that purpose, he is redirected from the Terminology Panel to the Terminology Browser. 

![image-20220126120254484](../../../img/image-20220126120254484.png)

The Value Set Composition can be completed by adding terms that have been found, or by adding single coded items. 

![image-20220126121454467](../../../img/image-20220126121454467.png)

When adding an item to the Value Set Composition, the author get the opportunity to add new codes from his search, or individual codes, and edit the Value Set Composition. 

This example shows the inclusion of an intensional Value Set definition from SNOMED-CT, specifying all descendents of code entered code. 

![image-20220126122115868](../../../img/image-20220126122115868.png)

This example shows the inclusion of an intensional Value Set definition, specifying to include a whole Value Set from the project. Includes can be done dynamically (always the most recent version), or fixed for a specific version. 

![image-20220126121742994](../../../img/image-20220126121742994.png)

#### Terminology Mappings

The Mappings Panel allows first creations of terminology mappings. Add and Edit Mapping is currently disabled, only viewing is available. This will be extended in the next release.

#### Incoming functionality

Incoming functionality for Value Sets:

- Add the option to view (and download) the Expansion Set of a Value Sets that contain intensional definitions.
- Add and Edit Terminology Mappings, the relationships between Dataset Concepts and Coded Concepts and Value Sets, and Concept Lists of a Dataset Concepts with distinct Coded Concepts of an associated Value Set. 
- The Mappings Panel will allow creation of and maintain terminology mappings. It is planned to publish the feature in an intermittent Release 3.3.1.

### Rules

The areas in Rules are *Templates*, *Profiles*, *Associations* and *Identifiers*.

#### Templates

The Panel offers the list of all Templates of the Project, grouped by their type as usual. The Templates are group-wise sorted alphabetically and indicate whether they are in- project templates (plain colored icons) or references from a Building Block Repository BBR (chain symbol). Once a Template is chosen on the left, the right card shows the details of the templates including the Template Design Body.

![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image016.png)

The Template Design Body is now feature complete regarding viewing, including the link to Dataset Concepts that are associated with Templates as a whole or distinct Template Elements.

![image-20220126162647754](../../../img/image-20220126162647754.png)

Template to Dataset Concepts associations are shows as the usual green pills (see also [Artefact Reference Details](https://docs.art-decor.org/documentation/principles/#artefact-reference-details)).

#### Incoming functionality

Incoming functionality for Rules:

- Next releases will allow to edit all properties of the Template Design Body properly [AD30-61]
- FHIR Profile Panel is in development [AD30-425], an editor is planned for a later release [AD30-429]
- Association is in development [AD30-434]
- Identifiers is in development [AD30-472], functionality is already present in the Project Index, tabs *Templates and Profiles*

## Outlook on what comes next: ART-DECOR® – Release 3.4

The current development is heading towards a soon next Release, where the following topics are on:

- Option to view Value Set Expansions when the Value Set contains intensional instructions such as include or exclude operations
- Detailed views on Templates in the Rules section, first Structural and Property Edits (Releases 3.4) and FHIR profiling (Releases 3.5 and 3.6)
- Like the new automatic and period refreshing mechanism for DECOR cache, there is also work underway to allow the same concept for a new extended automatic notification functionality.
- The database eXist version 6 has recently been released and is under investigation by the team for being the base for future ART-DECOR Releases.
- A note on terminology: we are re-organizing ART-DECOR in how terminology works internally. The API reflects the progress in this area. The sections codeSystem and valueSet support DECOR representations, while the section terminology represents what has been dubbed **Centralized ART-DECOR Terminology Services** or **CADTS**. CADTS currently powers the Browser frontend.

As you can see: more on that to come. Thank you for your patience. Stay tuned.
