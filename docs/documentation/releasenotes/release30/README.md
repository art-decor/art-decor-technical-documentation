---
permalink: /documentation/releasenotes/release30
---
# ART-DECOR® – Release 3.0

Release notes 16 July 2021

## Introduction

The ART-DECOR® tool suite is migrated from the current Release 2 to the new Release 3 family. This development started in 2020 and was successfully continued in 2021.

ART-DECOR® Version 3 will be published step by step. As the first of the family of Release 3 publications, Release 3.0 was recently published that brings along first new features and improvements. This start is done as a so-called Canary* Release.

| ***Canary Release**                                          |
| :----------------------------------------------------------- |
| ... is a release for Early Birds who volunteer to test it and runs on build.art-decor.org (international) and art-decor3.test-nictiz.nl (Netherlands focus) |

**Frontend: User Experience**

Release 3 introduces a new user interface with an improved user experience (UX) utilizing a new framework (Vue and Vuetify) that replaces the Orbeon® platform.

## New functionalities in Release 3.0 at a glance

To summarize the topics of this and future planned ART-DECOR Releases 3

- New UI / UX
- Microservices
- Better Scalability, higher Reliability
- Extended Documentation Platforms
- Centralized ART-DECOR® Terminology Services (CADTS)
- More FHIR Support (Repository, Profiling)

Special Topics regarding the platform upgrade in Release 3.0

- A new Token System has been created together with our partners from [exist Solutions GmbH](https://www.existsolutions.com), its introduction leads to a new security architecture for ART-DECOR with the propectus of even more fine-grained role-based access to resources
- A Project Landing Page with first features is available
- Datasets (part 1) with first features
- The Issues Panel is completely functional 

## Outlook on what comes next: ART-DECOR® – Release 3.1

The current development is heading towards a soon Release 3.1, where the following topics are on:

- Completing the new Landing Page, Project Overview Page 
- Completing Datasets Page with the navigation tree on the left and details shown on the right
- Upgrade of the underlying eXist database for more stability and performance

## Stay tuned

As you can see: more on that to come. Thank you for your patience. Stay tuned.
