---
permalink: /documentation/releasenotes/release386
---
# ART-DECOR® – Release 3.8.6

Release notes 13 November 2024

## Introduction

We are happy to announce **Release 3.8.6**, another minor release with a number of additions, smaller new features and several improvements. 19 tasks were performed, partially based on client requests. Due to major activities in the area of user feedback and controlled user interviews, collected test findings from various international sources and our own improved testing environment, this release also addressed 16 bugs that were fixed.

Thanks to all testers and people who submitted improvement suggestions! We are lucky to have you with us.

In total, 35 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8.6 focuses on the following major topics:

- enhancements in the area of [→](#dataset) **Datasets** and [→](#scenario) **Scenarios**,
- better  [→](#fhir-logical-model-export) **FHIR Logical Model Export** from datasets and scenarios, 
- enhancements in the area of  [→](#project-testing-and-validation) **Project, Testing and Validation**,
- enhancements in the area of [→](#templates) **Templates**,

- new features and corrections to the [→](#questionnaires) **Questionnaires Management** Panel,
- enhancements in the area of [→](#terminology) **Terminology**,
- enhancements to  [→](#publications) **Publications**.
- enhancements in the area of  [→](#software-development-tools-features) **Software Development / Tools features**,

In addition, we work closely with our eXist partners to investigate and deploy the new **eXist Database Release v6.3.0** soon.

## Areas of New Features and Improvements

### Dataset

#### Better handling of cancelled or rejected concepts on de-contain or de-inherit

When executing the action to de-contain or de-inherit concepts, cancelled or rejected concepts were also included in this action. This led to performance problems on large amounts of cancelled or rejected concepts. When de-contain or de-inherit now, cancelled or rejected concepts are no longer included in this action [AD30-1731]. Performance was also already enhanced in Release 3.8.5 when applying these functions to large datasets [AD30-1626].

#### Inherited containment indication

Inherited containment items in a dataset were showing up as empty groups. 

<img src="../../../img/image-20241118111943807.png" alt="image-20241118111943807" style="zoom:77%;" />

While this is not entirely wrong, a better way of displaying inherited containment items was indicated. They are now shown in a more appropriate way [AD30-1732] using an outline icon as usual for inherited items.

<img src="../../../img/image-20241118112008410.png" alt="image-20241118112008410" style="zoom:77%;" />

#### Further Achievements in this Release for Datasets

- Frontend allowed creating a concept association on the transaction level only, this has been fixed [AD30-1388].
- A bug was fixed where a concept association in a transaction gave an error [AD30-1669].

### Scenario

#### Further Achievements in this Release for Scenarios

- Problems with loading performance [AD30-1760] and display [AD30-1701] of the Scenario Transaction Leaf Metadata Panel were fixed.
- An intended Transaction Copyright update failed, which was fixed [AD30-1761].

### FHIR Logical Model Export

ART-DECOR has the ability to export DECOR Datasets or Transactions as [FHIR Logical Models](http://hl7.org/fhir/structuredefinition.html) since a few years. In this release (and the following one) some enhancements have been applied to the export function, namely the display of correct cardinalities in StructureDefinition export for Datasets and Transactions [AD30-1442].

#### Dataset or Transaction

It is possible to get FHIR Logical Model representation for Datasets or Transactions from any DECOR project. As an example, for the demo7- dataset the export can be triggered using the Project Index tabs for Datasets or Transactions and the respective FHIR download pill button (in JSON or XML format).

<img src="../../../img/image-20241118104912239.png" alt="image-20241118104912239" style="zoom:67%;" />

 The corresponding  API (getting the call would be demo7 Vaccination Certificates Document dataset in JSON format) would be

```
https://art-decor.org/fhir/4.0/demo7-/StructureDefinition/2.16.840.1.113883.3.1937.99.60.7.1.1--20131010000000?_format=json
```

::: note

Please note that a Dataset in DECOR has no cardinalties or conformance information. As it is illegal to omit cardinalities in FHIR Logical Models, all cardinalities are set to 0..* in case of a Dataset export. All further conformance constraints are omitted.

A transaction does have cardinalties / conformance information and this is adequately transformed into a FHIR Logical Model.

:::

**Examples**

A dataset has just the structure and no cardinalities / conformance information.

<img src="../../../img/image-20241012200909466.png" alt="image-20241012200909466" style="zoom:77%;" />

As stated above the resulting FHIR Logical Model export has no conformance information and all cardinalities are set to 0..*.

<img src="../../../img/image-20241012200958544.png" alt="image-20241012200958544" style="zoom:67%;" />

A transaction however may have cardinalities / conformance information.

<img src="../../../img/image-20241012200933992.png" alt="image-20241012200933992" style="zoom:67%;" />

They are translated accordingly to FHIR Logical Model constructs.

<img src="../../../img/image-20241012201018121.png" alt="image-20241012201018121" style="zoom:67%;" />

There will be some more enhancements for the FHIR Logical Model Export that will be part of Release 3.8.7.

### Project, Testing and Validation

#### Development Compile Runtime Version Panel

The **Development Compile Runtime Version Panel** got a better layout and new functionalities [AD30-1711]. The Panel frontend and the corresponding backend functions were adapted accordingly.

Compile tasks and the compilation results were already shown together but were not merged visually. They are now merged and displayed as one item. *Successful* compilations are still displayed as usual. *Failed* tasks result in compilations that have the status ‘failed’ and the process and error texts of the corresponding task shall be merged into the compile entry once available.

<img src="../../../img/image-20241118110420417.png" alt="image-20241118110420417" style="zoom: 33%;" />

The available error messages were not displayed at all. Especially the “last survivor message” from the xslt processes behind the scenes in the backend did not bubble up at the frontend to help the user to identify the problem. This has been corrected.

  <img src="../../../img/image-20241118110452833.png" alt="image-20241118110452833" style="zoom:33%;" />

The list showed tasks and compiled items all in one list. Tasks now have a special layout to indicate them as such. A task item shall have a “quick erase” indication ('x' button to the right) in the actions column to quickly kill a task in the panel too (and not just only in the Project overview panel).

<img src="../../../img/image-20241118110606565.png" alt="image-20241118110606565" style="zoom:33%;" />

Retired compilations shall be easily deletable with no action menu but rather with just a delete button under actions because it is the only action.

<img src="../../../img/image-20241118110657185.png" alt="image-20241118110657185" style="zoom:33%;" />

#### Validation Panel

The layout of **Validation Panel** has also been enhanced [AD30-1716]. The Validate button is shown also on failed compilations, which was not desirable. Only the delete and the download button are now available.

### Templates

#### Possible list of attributes when designing a template

Clients pointed out that the addition of a new attribute when designing a template used to be easier in ART-DECOR Release 2. There, when adding a new attribute, there is a list of attributes associated with the datatype of the selected element.

![image-20241118113003617](../../../img/image-20241118113003617.png)

For example, here is what we have when we select “new attribute” on a code in V2 : a list of attributes and an option "other" if the attribute that has to be added does not come from this list. Besides with each attribute selected, the datatype is automatically registered to stay consistent with CDA schema.

This feature has been re-introduced and now offers a possible list of attributes when designing a template [AD30-1707].

#### Further Achievements in this Release for Templates

- Template association fails: Parameter project is required [AD30-1608].
- Backend error on Template association remove [AD30-1686].
- Revise link function for specialized or adapted templates [AD30-1708].
- In the API package the function *templ:getTemplateList()* from the "old" ART package was used. For further decoupling ART and API, the old code was integrated into the API template code, leaving the ART code as-is [AD30-1498]. The *templ:getTemplateList()* is only used in the *tmapi:putTemplateStatus* and is now replaced by the *tmapi:getTemplateList()*.

### Questionnaires

#### Allow option "hidden" for Questionnaire items

Questionnaire items are typically visible when rendered. In some cases it is necessary (for example for extra meta information for Data extraction) to hide specific items from being rendered. The Questionnaire Editor now allows to add the option 'hidden' (boolean switch) [AD30-1609, AD30-1697].

#### LHC forms and Terminology Server

Our Questionnaire rendition engine **LHC forms** now gets information about a coded FHIR Questionnaire item to connect to a possibly specified Terminology Server [AD30-1673].

**Background**

LHC Forms supports the use of answerValueSet statements in items. However, when no terminology server element is stated in a FHIR Questionnaire item, no list is obtained or displayed as single coded answers (choice selection is empty for type-ins). This is expected behavior.

When a terminology server is stated in the FHIR Questionnaire item, LHC Forms now approaches the terminology server with an API call and whatever is returned as a FHIR value set is then offered as a coded option list whenever an answerValueSet is part of an item in a (converted) FHIR Questionnaire,

LHC Forms prepends with the local server uri and asks the API for a ValueSet with `$expand` operation and `url` parameter.

```
http://{server}/fhir/4.0/ValueSet/$expand?url={url}
```

with `{url}` stated as the generated or explicitly stated url of the value set, such as in the following example:

```
http://{server}/exist/apps/fhir/ValueSet/2.16.840.1.113883.3.1937.99.60.10.11.10--20220610000000
```

#### Further Achievements in this Release for Questionnaires

- Questionnaires now have FHIR support for underlining and strikethrough in item text parts [AD30-1710].
- No valueDomain properties were transformed in a Questionnaire, this has been fixed [AD30-1718].

### Terminology

#### Further Achievements in this Release for Terminology

- A cleanup code was performed for LOINC multiple answer lists when all ART-DECOR installs have LOINC version 2.76.1 or higher installed [AD30-1331].
- A problem was fixed when expanding complete code systems from the project where no codes were shown [AD30-1431].
- Some minor issues with the concept-properties code system are fixed now [AD30-1563].
- An update to the ART-DECOR Release 2 SNOMED was applied to display concepts to handle NL annotations [AD30-1712].
- Value set 'shopping' did not use selected preferred language, which has been fixed [AD30-1726].
- Terminology report fails to display under certain circumstances; this has been corrected [AD30-1743].

### Publications

A bug where Publications suffered from missing comments has been fixed [AD30-1752].

### Software Development / Tools features

#### Further Achievements in this Release for Software / Tools

- Schematron generation failed for vocabulary/@valueSet with inclusions and deprecated codes [AD30-1723].
- Purpose field was displayed too large in meta data, fixed [AD30-1730].
- The Usage indication was missing some templateAssociations and/or questionnaireAssociations, this has been corrected [AD30-1735].
- Handle edit to "O" – optional correctly [AD30-1759].
- Uppercase username did not work when changing email [AD30-1675].
- A Password validation bug was fixed [AD30-1689].
- A MyCommunity that was just created was missing prototype on edit, this was fixed [AD30-1698].
- FHIR  download did not pass the correct context to the FHIR server for BBR  references; this has been fixed [AD30-1739].
- Acceptance test release 3.8.6 was performed [AD30-1721].

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset and Questionnaire editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org/) team. Dataset and Scenario enhancements are also coming from the [Professional Record Standards Body](https://theprsb.org/) in the UK – PRSB.

Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl/). Thanks to Gunter Haroske working for the [Deutsche Gesellschaft für Pathologie / German Association of Pathology](https://www.pathologie-dgp.de/) pointing to shortcomings and missing features in our Questionnaire functions and to the Dutch [Uitkomstgerichte Zorg](https://www.uitkomstgerichtezorg.nl/) Program for contributions in the same area. 

Special thanks also to the [mio42 GmbH](https://mio42.de/) team, [Agence du Numérique en Santé](https://esante.gouv.fr/) (ANS) and our partners at [Kereval](https://www.kereval.com/home/) / [IHE Europe](https://www.ihe-europe.net/) for testing, feature suggestions and bug fix requests.

Please don't hesitate to write us with suggestions for improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.7 quickly, planned to be published end of December 2024. One of the major aspects are more features and enhancements for the Questionnaire Management. As you can see: more on that to come. Thank you for your patience. Stay tuned.
