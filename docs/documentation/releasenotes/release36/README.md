---
permalink: /documentation/releasenotes/release36
---
# ART-DECOR® – Release 3.6

Release notes 2 December 2022

## Introduction

We are happy to announce **Release 3.6** today. The published version addresses new features and improvements, during development a big amount of tickets were handled. This is the Release Note with more details.

## General Remarks

Release 3.6 focuses on the following major topics.

- New **Implementation Guide** Panel with authoring capability
- **Code System Maintenance** completed
- Expansion Sets of **Intensional Value Sets** (part 2: any code system)
- Further improvements of the **Centralized ART-DECOR Terminology Services** (CADTS)
- ART-DECOR **Server Cluster Migrations**

and a longer list of further enhancements and fixes.

## Major Topics

### Implementation Guides

The new Implementation Guide Feature [AD30-438, AD30-820] is introduced that allows authoring Implementation Guides (IG) that includes various artifacts such Value Sets, Code Systems, Templates and Profiles. Submission of crafted IGs to a Publication Process is subject of part 2 in a further release.

An Implementation Panel [AD30-827] shows all Implementation Guides created and maintained for the Project and offers metadata editing [AD30-828]. The left part shows the Navigation list with all IGs of the project. 

<img src="../../../img/image-20221206135418855.png" alt="image-20221206135418855" style="zoom:67%;" />

Once an IG is selected you get the pages tree shown.

<img src="../../../img/image-20221206135641077.png" alt="image-20221206135641077" style="zoom:67%;" />

In the page hierarchy (on the left) here is always one Index Page with any arbitray number of sub-pages. The right part shows the list of artefacts (resources) that can be referenced in the Implementation Guide. Once clicked on a page in the Navigation part the page content is shown on the right.

<img src="../../../img/image-20221206135821628.png" alt="image-20221206135821628" style="zoom:67%;" />

The IG Page Editor makes use of our multilingual Text Editor for the hierarchy of pages and drag and drop capabilities to add resources from the Project/Scenario [AD30-829]. The Page Editor has a head bar with the title of the page and the IG name <ref1/>, a set of buttons to edit or delete a page or – when editing a page – the usal save and cancel options <ref2/>. The title of the page can be changed directly <ref3/>. The text area <ref5/> has a tool bar on top <ref4/> when editing that can be used to layout the text with the usual options.

<img src="../../../img/image-20221206140043949.png" alt="image-20221206140043949" style="zoom:67%;" />

**Further Enhancements and Fixes to Implementation Guides**

- XSD Schema [AD30-824] and API additions were performed [AD30-825] 
- the Backend Logic was enhanced [AD30-821]
- Added DECOR var / constant in settings [AD30-822]
- Added new DECOR baseIds [AD30-823]
- Editor Refinements, e.g. sticky tool bars [AD30-826]
- Added a Scrollbar in Implementation Guide Panel [AD30-964]
- Editor now shows all (filtered) Resources of the project [AD30-830]
- Solved a problem when server GETs Implementation Guide [AD30-839]

### Code System Maintenance Enhancements

We enhanced the set of features of our **Code System Maintenance**. Code Systems can be created and maintained, our new Code System editor supports lists, hierarchies, or networks of codes. Code Systems Maintenance now allows editing Network of Coded Concepts [AD30-688].

The Concept List section is used to add or edit coded concepts in the concept list. The hierarchical tree on the left can be used to select a coded concept. Once a concept is selected, the details of the concept are shown on the right. If applicable, parent and child concepts are also displayed here.

![image-20220623173959537](../../../img/image-20220623173959537.png)

The menu button <ref1/> allows adding new coded concepts to the list. Buttons are available <ref2/> to move the concept among its sibblings and to manage parent relationships. The Designations tab can be expanded <ref3/> to add or edit designations. The Properties tab can be expanded to add or edit properties to the concept <ref4/>. This is only available if there are Property Definitions defined in the Code System. For further information please refer to our documentation about the [Code System Editor](https://docs.art-decor.org/documentation/codesystem/).

**Further Enhancements and Fixes to Code System Maintenance**

- Code System ref is now possible and displayed properly [AD30-862]
- A problem was solved with Edit/Patch Metadata shortcomings [AD30-690]
- Allow for status code 'experimental' in Coded Concepts of Code Systems [AD30-859]
- A problem was solved with missing documentation and unexpected behavior in the code system API [AD30-230]
- A HTTP 500 error was fixed when retrieving list of Code Systems [AD30-871]
- Searching for codes in project code system has been fixed [AD30-885]
- A problem was solved when adding code system failed and a variable was not set [AD30-887]
- Code System ref: backend [AD30-957]
- Show draft codes in DECOR Code Systems [AD30-909]

### Value Set Expansion Sets Enhancements

Visualization of **Expansion Sets of Intensional Value Sets** has been enhanced [AD30-702] and now works for intensional Value Sets with SNOMED and all ART-DECOR Code Systems [AD30-747]. Next release will allow *storage and download* of Expansion Sets.

![image-20220623174846393](../../../img/image-20220623174846393.png)

An intensional Value Sets contains instructions to derive the actual code list instead of the code list itself. As an example the Value Set in this example states an INCLUDE of the *Blood group A* code <ref1/> and also any children that may exist in the code hierarchy. If an intensional Value Set can be expanded into an Expansion Set, an appropriate button appears <ref2/>. Clicking on it offers you a dialog with the number of codes in the Expansion Set as a "preview", and the option to see the actual Expansion Set (restricted number of codes). 

<img src="../../../img/image-20220623175406356.png" alt="image-20220623175406356" style="zoom:50%;" />

In a subsequent release Expansion Sets can also be stored and maintained.

**Further Enhancements and Fixes to Value Set Expansion Sets**

- Value Set Expansion hint added on deprecated concepts [AD30-849]
- Also Project Code Systems can now be handled CADTS-wise [AD30-853]
- On expand request now a conversion of project CS into CADTS happens [AD30-881]
- Support of more intensional expressions [AD30-946]

### General Centralized ART-DECOR Terminology Services (CADTS) Enhancements

We added further enhancements to the Centralized ART-DECOR Terminology Services CADTS [AD30-344].

- Code validation added on Search Input Field Terminology Browser [AD30-768]
- Review of HL7 Terminology Package as a source for our ad2bbr Repo and CADTS packages [AD30-855]

- No longer Inconsistent display of deprecated (SNOMED) concepts [AD30-763]
- Terminology browser support for multilingual SNOMED added [AD30-804]
- Decoupled server-functions.xml and route names and check xml structure and make it the default template for server-functions [AD30-846]
- A problem was solved where creation of a Value Set based on a complete Code System was not possible [AD30-886]
- Allow creation of a new Value Set directly from the terminology associations panel [AD30-895]
- The Terminology Report has been migrated to CADTS functions [AD30-910]
- Usage on Value Set is now shown properly [AD30-934]
- Value Set editor got a feature to adding code at a specific location in Value Set [AD30-942] or move code location within Value Set [AD30-944]
- Added default level/type when adding code to Value Set [AD30-943]
- Delete/add complete Code System in a Value Set error was fixed [AD30-974]
- A problem was solved where adding code in Value Set showed a pre-populated value [AD30-915]
- A problem was solved where Code System ref NullFlavor and LOINC not found in demo1 [AD30-936]
- Added display of Code System Properties in Terminology Browser [AD30-892]

### Server Cluster Migrations

The long awaited and long planned ART-DECOR Server Cluster Migration finally took place at the beginning of November. The old main server hosting many projects was running since 2014 and performance and capacity needed to be improved [AD30-655].

*ART-DECOR Open Tools* now holds a cluster of seven servers hosting various services and/or projects in the ART-DECOR ecosystem. The new main servers [AD30-654] offers more performance, new auxilliary servers hold large capacities of disk space for all publications or offer specific services for other server cluster members to distribute and load-balance all activities and duties. Some servers have been established as so-called *duplex* servers featuring an ART-DECOR Release 2 **and** ART-DECOR Release 3 entrance to the same database for smooth transition.

Migration scripts for the import of data from the old database have been crafted [AD30-922]. The [documentation](https://docs.art-decor.org/administration/setupmaintain/) got a major update on all installation and migration instructions.

All repositories have moved from the former Nictiz hoster to the new ART-DECOR Repository [AD30-921]. There is a [spearate newsletter](http://newsletter.art-decor.org/m/13972851/) on the repository migration.

A new surveillance system has been introduced. The backup system has been enhanced to a cloud backup system for all server cluster members with a separate surveillance.

::: note

**We apologize for any inconveniences that the migration beginning of November has caused. Planned for a few hours of downtime eventually three days of shortcommings happened, mainly caused by Domain Name System (DNS) problems.** 

If you still have any issues with your server or on any of our servers, please let us know. Especially separate ART-DECOR Release 2 server need an update/patch and we were happy to conduct those on several systems or help users out to get the newest upgrade.

Thank you for your patience.

:::

All installation scripts now end with the Czech word "konec" and a [little mole](https://docs.art-decor.org/konec/), for a bit fun after hard word :-)

## Further Enhancements and Fixes

The following sections focus on the various artefacts and shows the new functionalities (compared to ART-DECOR Release 3.5.1, compared to ART-DECOR Release 2 shown **in bold**).

::: note

The numbers shown are the ART-DECOR Expert Group ticket identifiers.

::: 

**Associations**

- Transactions Terminology associations can now be viewed, created and deleted [AD30-577, AD30-635, AD30-636]
- A problem has been solved when Terminology association value set lists were lagging [AD30-891]
- Added association options for Dataset Concepts with Terminology [AD30-59]
- Added association options for Dataset Concepts to Profile Components [AD30-221]
- Changing Terminology Associations and Concept Map Routes [AD30-834]

**Datasets**

- The Expandable section *History* has been added [AD30-297]
- Show and edit dataset concept synonyms [AD30-906]
- A problem has been solved with a server error when getting concept with associations=true [AD30-961]
- Dataset filter + original only switch when adding new concepts [AD30-877]

**Issues**

- Show "No template (ref) found" on clicking template version [AD30-866]
- Transaction concept conformance conditional got ability to specify conditions [AD30-869]
- Can now edit issue trackings/assignments [AD30-874]
- Now can add link to objects [AD30-882]
- Now can add concerns data element to issue, a problem was solved where call did not return data [AD30-939]
- Solved a problem where new ids on issues, scenarios, transactions and were actors incorrect [AD30-863]
- Solved a problem where creating an issue on a reference that does not resolve / yields no object [AD30-864]
- Solved a problem where Dataset concept usage renders the wrong cardinalities [AD30-867]
- Solved a problem where Dataset concept rendering of templateAssociation appears unfinished [AD30-868]
- Terminology association to concept no longer fails to connect to search results [AD30-870]
- Got rid of large unused cardinality/conformance column for Dataset usage (in transactions) [AD30-872]
- Changed weird display on status change in issue [AD30-873]

**Projects**

- Project History Panel with Filters and enhanced Search [AD30-840]
- Scheduled Tas got a fix on pattern when deleting a planned scheduled Task [AD30-717]
- Unexpected response from GET / project/{prefix}/$check solved [AD30-682]
- Problems validating Instance in HL7 Italy project solved [AD30-794]
- Getting project history now returns the latest activities [AD30-861]
- Adding copyright logo is expected in the backend and now checked in frontend [AD30-962]

**Publications [AD30-69]**

- Publication management for project admins (backend) has been revissed [AD30-117]
- Allow trigger Project Publications/Versions [AD30-185]
- Issue a warning on Value Sets and Code System Publications [AD30-854]
- Changed output in publications where HTML publication says 'partial' when filters are off [AD30-506]
- Show also usage on value sets in publication [AD30-656]
- Reduced the number of clicks to see the release notes for publications [AD30-905]

**Questionnaires**

- Made Questionnaire metadata editable [AD30-193]
- Made Copyright editable [AD30-243]
- Added missing Choices in rendered Questionnaire [AD30-770]
- Solved duplicate UUIDs in questionnaire list [AD30-968]

**Scenarios/Transactions**

- Added a loading indicator in Scenario Transaction detail view [AD30-925]
- Solved a Scenario edit bug: oops upon concept de-select from transaction [AD30-916]
- Solved a problem when changes were not directly visible after editing condition [AD30-972]
- Scenario Details are no longer editable for non-project authors [AD30-860]
- Associated issue count added on Scenarios and Transactions [AD30-875]
- Scenario / transaction details view now shows Issue Associations [AD30-876]
- Transaction: conformance/cardinality for conditions renders now correctly [AD30-935]
- A bug in scenarioDetailsCard watcher was fixed [AD30-967]
- A Deep link to concept in transaction was added [AD30-914]

**Server Admin Functions**

- EDIT buttons now shown/hidden consistently [AD30-767]
- Adding new user via api (playground) adds username to groups and sets primary group [AD30-167]

**User Experience, general User Interface, Software Tools**

- Deep Links were added throughout ART-DECOR 3 [AD30-370]
- A problem has been solved where FHIR artefact elements contain too many newlines [AD30-368]
- Fxied a problem with Repo Cache and multiple Templates [AD30-518]
- Don't show performer of History items to not-logged in user (backend) [AD30-779] [AD30-780]
- History API now handles Data set concepts, Transactions, other parts of history [AD30-795]
- Show 'unavailable' message when navigating to pages with read=false [AD30-844]
- Allow easier switching between projects [AD30-893]
- Show Mouse over for full dataset name [AD30-901]
- Added documentation link to Services Card on Home [AD30-918]
- Updated UUID for Frontend [AD30-949]
- Enhanced Change password feature (backend) [AD30-954]
- Investigated Refresh token feature (backend) [AD30-955]
- Triaged several styling suggestions from customers [AD30-973]
- Investigated user suggestion to add more feedback when changing password [AD30-952]
- History Backend to allow direct calls for history of a specific artefact [AD30-841]
- Scroll bar added for Scenario and Code System Tree [AD30-850]
- The TipTap Toolbar in the editor has been made sticky [AD30-851]
- Added inline code option to TipTap [AD30-852]
- TipTap Editor inline code icon has been changed [AD30-929]
- Customers feedback helped where different lists on usage on datasets were not in sync [AD30-958]
- Added internal featurest o allow *Hoovering* Artefact History Handling to reduce real XML in the database for historic items [AD30-348] 

**Rules** (Templates)

- Customers feedback: Choice description and list is no longer missing in Templates [AD30-857]
- Customers feedback: Removing a choice in a template no longer lets loose ends happen after save [AD30-933]
- Adding Value Set Binding has now an ADD button [AD30-948]
- Customers feedback: Options in Templates for manually adding predicate / slicing added [AD30-951]
- Copy/clone existing template now works properly [AD30-969]
- Copy template example now works properly [AD30-708]
- User feedback: enhancement suggestions when comparing AD3 vs AD2 Template viewer [AD30-856]

**Realtime updates**

Our Realtime updates techniques makes working together easier and safer, as everyone working simultaneously in the same area of artefacts in a project gets informed about changes other team members just did and in fact sees the changes as an update. We made some enhancements and additions to this feature.

- Realtime updates for Implementation Guide panel added [AD30-923]
- Realtime updates in conjunction with the AdArtefactDetailsCard [AD30-706]
- Realtime updates for Project index added [AD30-734]
- Realtime Updates for sections in Admin panel added [AD30-845]

**Prefetching**

Artefacts re-used and re-visited often by the user now are prefetched from the API (and updated realtime on changes) so that lists or details visualisation got a performance boost.

- Pre-fetching data from projects [AD30-911]
- Fetch Implementation Guide list once, then sync [AD30-927]

**Terminology Package Updates**

- LOINC 2.73 terminology package was added to terminology repositories [AD30-843]
- Most recent version of SNOMED was added to terminology repositories [AD30-890]
- Lab system *Nomenclature for Properties and Units (NPU)* Code System was added
- The document classes list Klinische Dokumentenklassen-Liste (KDL) Code System was added

## Incoming features with next ART-DECOR® Releases

ART-DECOR Release 3 is a horizontal move of all Classic Release 2 features, plus adding new features immediately if appropriate or later as planned. **This migration is now complete.** We focus on new features and functionalities.

The following main areas are under current development and are seen as incoming functionality in the following Releases:

**Implementation Guide**

- The Implementation Guide Feature [AD30-438, AD30-820] was so far covering crafting and maintenance of IGs in a project. Ongoing work allows to use project artefacts in IGs that are renedered later appropriately. Major work is done to offer rendition in one of the next releases. It will allow the submission of an IG with text and resources to our extended publication process and get rendered HTML publications at specified publication locations. In addition, the [FHIR IG Publisher](https://confluence.hl7.org/display/FHIR/IG+Publisher+Documentation) will be offered as one of the render machinery, e.g. for CDA- or FHIR-based IGs. 

**Terminology**

- Store and maintain **Value Set Expansions** when the Value Set containing intensional instructions such as include or exclude operations will be offered.
- Edit and Maintain **Terminology Mappings** type 3: Choice List – Concept Association
- **Concept Maps** (mappings bewteen dataset concepts and between coded concepts, e.g. in value sets)
- A **Terminology Report** was already featured in Release 2 checking terminologies used in projects and detecting e.g. out-dated codes or improper display names. In Release 3.7 the new Terminology Report is offered with new features and support for user requests.

**Questionnaires**

- **Questionnaire** (QQ) and **Questionnaire Response** (QR) as defined in the FHIR standard plus an ART-DECOR wrapper to allow consistent handling like all versionable artefacts will be further enhanced from Release 3.6 on.
- An Questionnaire **Editor** for the  Items is in development and will be released with 3.7+, a new member of the Expert Group dedicates work on the [ART-DECOR Implementation Guide on Questionnaires](https://ig.art-decor.pub/art-decor-questionnaires/StructureDefinition-ArtDecorQuestionnaire.html), visualization and creation/maintenance of Questionnaires. 
- Questionnaires Enhanced (QE) was introduced, starting with Release 3.4.1: once Questionnaires have been edited/enhanced by the user with for instance: layout instructions, conditions, it migrates its status to an Questionnaires Enhanced (QE) in order to be distinguished from genuinely generated Questionnaires in ART-DECOR.

**FHIR Profiling**

- First **FHIR Profiling** will be features starting with Release 3.8.

**System Architecture**

- **GIT integration** as a natural extension of the ART-DECOR Building Block Repository concept will be subject of one of the next releases, collaborations have been established to quickly bring this topic to first functionality.

## Outlook

The current development is heading towards a soon next Release, planned to be published in during January 2023, where the aformentioned new features will be addressed along with enhancements and bug fixes on Release 3.6. 

As you can see: more on that to come. Thank you for your patience. Stay tuned.
