---
permalink: /documentation/releasenotes/release383
---
# ART-DECOR® – Release 3.8.3

Release notes 2 June 2024

## Introduction

Today we proudly announce **Release 3.8.3**, another minor release with a number of additions, smaller new features and several improvements. 33 tasks were performed, partially based on client requests. Due to major activities in the area of user feedback and controlled user interviews, collected test findings from various international sources and our own improved testing environment, this release also addressed 26 bugs that were fixed. Thanks to all testers and people who submitted improvement suggestions! We are lucky to have you with us.

In total, 60 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8.3 focuses on the following major topics:

- new features and corrections to the [→](#dataset) **Dataset** Panel,
- enhancements for [→](#scenario-transactions) **Scenario / Transactions** for smart cloning of new Scenarios based on new version of an underlying dataset,
- in the area of [→](#terminology) **Terminology** we offer now working with LOINC® panels, also enhancements and corrections were applied,
- new options for the Design Details and corrections to the [→](#templates) **Templates** Panel,
- better options for Expanding large tree views in asynchronous chunks, a more consistent download of artifacts and Tree View vertical resize options in the area of  [→](#software-development-tools-features) **Software Development / Tools features**
- enhancements to the [→](#questionnaires) **Questionnaires Management**, [→](#concept-map) **Concept Maps**, [→](#tsting-and-validation) **Testing and Validation** and [→](#publications) **Publications**.

## Areas of New Features and Improvements

### Dataset

The missing support for **Dataset Properties Management** has been added [AD30-1345, AD30-1402]. 

<img src="../../../img/image-20240531145144892.png" alt="image-20240531145144892" style="zoom: 67%;" />

Dataset Properties can now be added, edited and deleted.

<img src="../../../img/image-20240531145129560.png" alt="image-20240531145129560" style="zoom: 67%;" />

The expanded state of Accordion selections for Relationships, Usage, History and the metadata is now stored **per artefact** and have been **made permanent** for the user session [AD30-1488]

<img src="../../../img/image-20240531145247033.png" alt="image-20240531145247033" style="zoom:67%;" />

When creating a new Dataset Concept, possible candidates for inheritance were shown with a status icon (dot), but no status text was offered. A tooltip has been added to show the status as text when **hovering over the status dot** [AD30-1503].

<img src="../../../img/image-20240531145414687.png" alt="image-20240531145414687" style="zoom:67%;" />

**Further Achievements in this Release for Datasets**

- The problem that dataset/concept deinherit of containment did not work under certain circumstances has been fixed [AD30-1487]
- Usability was improved when moving items in a dataset [AD30-1491]

### Scenario / Transactions

#### **Smart Cloning of a Scenario**

We added an essential feature when working with multiple versions of Datasets and Scenarios. When cloning a Transaction leaf of a Scenario the new Scenario is basically empty as it loses its Source Dataset. We now introduced the Cloning a Transaction leaf of a Scenario based on a new version of the underlying dataset  [AD30-1273, AD30-1465, AD30-1466]. Below we have included some background we recommend you read.

**Background:** a Source Dataset provides a set of Concepts (with an id and effectiveDate) that are referenced in a Transaction Leaf.

<img src="../../../img/image-20231109-183852.png" alt="image-20231109-183852" style="zoom:40%;" />

Creating a new version of the Dataset may typically let all IDs be the same per concept (persistent IDs), while the effective dates move on to the cloning date (the new version date).

<img src="../../../img/image-20231109-183949.png" alt="image-20231109-183949" style="zoom:40%;" />

**Problem:** Cloning a Transaction that is based on Version 1 of the dataset loses all referenced concepts (is basically empty) as it loses its Source Dataset

<img src="../../../img/image-20231109-183929.png" alt="image-20231109-183929" style="zoom:40%;" />

**Happier with Smart Cloning:** With the pre-requisite that the IDs are kept for the concepts in Dataset version1 and the new Dataset v2, cloning a Transaction based on Dataset v1 may include the option to take over all concepts in Transaction v1 that are also present in Dataset v2 and populate the newly cloned Transaction with references to the concepts in Dataset v2. So the cloned Transaction is now based on the Concepts of Dataset v2 that where already present in Transaction v2 based on Dataset v1.

<img src="../../../img/image-20231109-183909.png" alt="image-20231109-183909" style="zoom:40%;" />

An example: assume that there is a Scenario that has just been cloned from an existing one. Looking at the Scenario metadata we see that the underlying dataset is still the *Estimated Delivery Date as of 2021-12-10* dataset, the old version.

<img src="../../../img/image-20240601101934753.png" alt="image-20240601101934753" style="zoom:37%;" />

When clicking on the pencil to edit the Source Dataset you see a dialog that shows all dataset in the project. We still see that the *Estimated Delivery Date v1* dataset is chosen, but we want to re-assign the underlying dataset to the new existing version 2 of the *Estimated Delivery Date* dataset.

<img src="../../../img/image-20240601101956361.png" alt="image-20240601101956361" style="zoom:50%;" />

When selecting the new *Estimated Delivery Date v2* dataset the right side of the dialog opens up to summarize what you are going to do: the wise owl days that the concepts in the current transaction leaf are based on the displayed original dataset and that you can "smart clone" the concepts in the transaction leaf referring to the selected new dataset. It shows old and prospective new dataset and explains the options to either just **re-assign the transaction to another dataset** and lose all references of the concepts in the transaction (so you can start building the concepts from scratch) or to **smart clone concepts of this transaction based on another (newer) dataset**. 

<img src="../../../img/image-20240601102015219.png" alt="image-20240601102015219" style="zoom:50%;" />

For the latter option there is a button SMART CLONING (PREFLIGHT) to start a pre-flight to see the statistics of the resulting concepts in the transaction, i.e. a list of concepts that could not be transformed in to the new dataset.

<img src="../../../img/image-20240601102029771.png" alt="image-20240601102029771" style="zoom:43%;" />

<img src="../../../img/image-20240601102056231.png" alt="image-20240601102056231" style="zoom:50%;" />

Once you decide what to do you can either "smart clone" the transaction, or cancel the preflight and just re-assign to start from scratch with selecting concepts in the transaction. 

#### More inherit and containment information on Scenario Details

The Scenario detail view now offers inherit and containment information as in datasets [AD30-1534].

<img src="../../../img/image-20240531150723332.png" alt="image-20240531150723332" style="zoom: 67%;" />

**Further Achievements in this Release for Scenario / Transactions**

- An error has been fixed in the Transaction Editor where removing a conditional concept leads to error on leftover conditions [AD30-1404]
- A Visual discrepancy of cardinality has been adjusted [AD30-1457]
- The Scenario editor now makes better use of display space when in fullscreen [AD30-1502]

### Terminology

#### Working with LOINC® panels

LOINC panels are collector terms linked to a set of discrete child elements. Examples are a laboratory battery of tests, forms or assessments related to health completed by patients and/or providers.

To facilitate the use of LOINC panels in ART-DECOR® projects a new function has been added to import panels as dataset concepts complete with all associated value sets (answer lists in LOINC) [AD30-1434]. In addition to the panel items and value sets terminology associations will be created linking the dataset concepts to the original LOINC concepts and the concept lists to the imported value sets. 

The import function is located in the dataset tree menu:

<img src="../../../img/concept-inherit.png" alt="concept-inherit.png" style="zoom:67%;" />

Just as with creating a new concept in a dataset, a context for insert can be selected. If no concept is selected the panel will be imported after the last concept in the dataset. Selecting the ‘Import LOINC panel’ function will open the terminology browser:

<img src="../../../img/import-loinc-browser.png" alt="import-loinc-browser.png" style="zoom:67%;" />

The search context is set to ‘LOINC’. Use the browser to search for, or navigate to the panel to import. The selected panel and the context will be shown in the ‘Import LOINC panel’ part at the bottom of the page. Only panels can be imported this way, the class of the LOINC concept must begin with ‘PANEL’

<img src="../../../img/import-loinc-panel-selected.png" alt="import-loinc-panel-selected.png" style="zoom:67%;" />

Clicking the ‘Save’ button will open a dialog asking for confirmation of the import:

<img src="../../../img/confirm-import.png" alt="confirm-import.png" style="zoom:67%;" />

The dialog shows the number of concepts, value sets and terminology associations that will be created. Clicking the ‘Save’ button will perform the import and return focus to the dataset. Just like other concepts the properties can be edited and translations can be added.

<img src="../../../img/panel-in-dataset.png" alt="panel-in-dataset.png" style="zoom:67%;" />

If additional panels are imported into the project only new concepts and value sets will be imported, concepts already in the dataset will be inherited:

<img src="../../../img/concept-inherit.png" alt="concept-inherit.png" style="zoom:67%;" />

Value sets are imported into the project to allow for editing of properties, especially translation of display names. The order of items in the concept list is by sequence number as specified in the LOINC answer list. Ordinals (score) are included with the concept list items.

<img src="../../../img/imported-valueset.png" alt="imported-valueset.png" style="zoom:67%;" />

The Terminology Report function can be used to check if any imported items are affected by changes in LOINC if a new version of LOINC is released. 

**Further Achievements in this Release for Terminology**

- We now offer support for display and filter of SNOMED refsets [AD30-1235]
- We now offer option for downloads values sets in various formats [AD30-1323]
- Missing function to select codes from your custom code system and include them in a ValueSet were added [AD30-1366]
- A LOINC BBR for value sets was created (but not used) [AD30-1377]
- We now offer support for creation of valuesets that include score/ordinals as supplied by LOINC [AD30-1287]
- We determined which terminology packages we support in AD3, please refer to [Terminology Repository](https://terminology.art-decor.org) [AD30-1433]
- A generated info file with information about our supported third-party terminology packages is now generated from the  terminology packages and used in the frontend [AD30-1464]
- Terminologyreport did not render under certain circumstances, this has been fixed [AD30-1480]
- CWE and CNE as options were removed from the dropdown in terminology association editor [AD30-1481]
- Correct conceptAssociations on valueset usage call  [AD30-1507]
- FHIR CodeSystem.count is now not present for content not-present [AD30-1510]
- Terminology report fails when multiple versions of a dataset/concept exist, this has been fixed [AD30-1511]

### Templates

An option has been introduced in the top left button list that allows toggling display of tags/labels in Template Design [AD30-1452].

<img src="../../../img/image-20240601101326254.png" alt="image-20240601101326254" style="zoom: 40%;" />

**Further Achievements in this Release for Templates**

- The button to use the XML Editor Temple is now visible on Template Editor [AD30-1407]
- Template association now shows which template it is related to [AD30-1408]
- A faulty feature was corrected where the Template association panel acts as dataset editor for scenarios [AD30-1409]
- Template now shows all template associations, after it showed only one per element/attribute [AD30-1418]
- Editing assert in template editor 'edit' button is now 'save' [AD30-1435]
- Template associations ref now has item/object to drag into associations panel [AD30-1436]

### Software Development / Tools features

**Expanding large tree views in asynchronous chunks** 

When opening (expanding) large trees in the tree view, this can be very time consuming until the expansion is seen and a performance killer, too. They are now displayed in asynchronous chunks when expanding  [AD30-1383].

Instead of expanding all items in the tree at once, we could do it asynchronous. So open the first 50 items beginning from top and open the rest delayed (~50ms) in chunks. That could solve that issue with large trees, so we should investigate that further.

**Artifacts download**

The option to download artifacts in various formats such as DECOR format, FHIR exports etc, is now consistently set up, similar to the Project Index pills [AD30-1371].

<img src="../../../img/image-20240226145622305.png" alt="image-20240226145622305" style="zoom:70%;" />

**Tree and Table view vertical resize option**

The Tree and Table Views in cards now have a vertical resize option [AD30-1517].

<img src="../../../img/image-20240601112013750.png" alt="image-20240601112013750" style="zoom:50%;" />

**Further Achievements in this Release for Software Development / Tools features**

- The *Profile* API has been added to backend as a framework [AD30-1204]
- We now offer consistent option for downloads artifacts in various formats [AD30-1371]
- FHIR Servers canonicalUri generation git some enhancements [AD30-1399]
- Package updates have been applied and node version compatibility has been checked [AD30-1403]
- ESLint enhancements regarding vue/multi-word-component-names [AD30-1446], no-v-text-v-html-on-component [AD30-1449], array-callback-return [AD30-1451]
- The Text editing tools overlapping actual text in certain situations, this has been corrected [AD30-1496]
- The original project index calls (non-html) have been replaced by their new API calls [AD30-1500]
- The download calls have been deplaced to use their new API calls  [AD30-1501]
- ART 2.2.13 on develoment and production differ, this has been further investigated [AD30-1516]
- Tree View vertical resize option [AD30-1517]
- GET locks should be 200 with empty list instead of 404 [AD30-1518]
- API pre-install decor group admin member [AD30-1544]

### Questionnaires

* FHIR ordinal in questionnaire has now the correct datatype [AD30-1542]
* The Questionnaire API now offers POST calls to store Questionnaire Responses [AD30-584]
* Allow to change order item in Questionnaires Enhanced [AD30-1482]

### Concept Map

- Concept maps panels offered edits for non-authors which led to errors when saving; this has been fixed [AD30-1397]

### Testing and Validation

- When uploading an instance for validation within the ART-DECOR testing area (development) , an instance loading indicator is now showed [AD30-1444]

### Publications

- The Publication Site software (part of our Release and Archive Manager ADRAM) has been enhanced so that in a Publications, a group with only obsolete items is completely shown in the Archive section [AD30-1473]
- A Nictiz publication had html markup characters in release notes, this has been corrected [AD30-1476]
- Project release notes now get parsing before added to project [AD30-1522]
- Missing version date information in some release document have been added back [AD30-1532]

### Strategic Questions and Answers

The "old" DECOR service area used in ART-DECOR Release 2 is sunsetted for ART-DECOR Release 3. The API has been enriched to cover all functionalities such as

- backend retrievals, now called extractions [AD30-1272]
- exports in different formats such CSV, SQL, SVS for codesystem and valueset [AD30-1275]
- exports in SVG/html formats for dataset/transaction/template [AD30-1346]
- the functions set getExpanded Valueset/Codesystem/Template has been made available in API [AD30-1400]

### eXist Database Patch Preparations for v6.2.0 (and newer)

We realized in some projects that – under certain circumstances – the **database index** of a project folder dropped accidently while editing with the ART-DECOR tool with the effect that the project was no longer on the server. After a re-index of that folder the project was back for access. We realized that first with our User Management where similar problems occurred.

We created a set of test runs on our pre-production development server ATIS and finally found the problem reproduceable. With our partners from [eXist Solutions](https://www.existsolutions.com) we continued the analysis of the problem when editing files and checked if index got lost [AD30-1485].

It turned out that a **patch for the underlying eXist database software** and some general tweaks were needed. The fixes have been applied to the ATIS server and were further tested. In Release 3.8.4 – which is likely to be published in July – this database patches and tweaks will be made available to foreign hosted servers and will be installed on all our Customer Cluster Servers including our main and development servers. Customers please contact us for getting support for the patch.  

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset and Questionnaire editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org) team. Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl). Thanks to Gunter Haroske working for the [Deutsche Gesellschaft für Pathologie / German Association of Pathology](https://www.pathologie-dgp.de) pointing to shortcomings and missing features in our Questionnaire functions and to the Dutch [Uitkomstgerichte Zorg](https://www.uitkomstgerichtezorg.nl) Program for contributions in the same area. Special thanks also to the [mio42 GmbH](https://mio42.de) team for feature suggestions and bug fix requests.

Please don't hesitate to write us with suggestions for improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.4 quickly, planned to be published in July 2024. One of the major aspects are our new Artifact ID Management, more features for the Questionnaire Management and the Profile Panel and the aforementioned patch for the underlying eXist database software. As you can see: more on that to come. Thank you for your patience. Stay tuned.
