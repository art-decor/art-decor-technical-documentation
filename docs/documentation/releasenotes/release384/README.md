---
permalink: /documentation/releasenotes/release384
---
# ART-DECOR® – Release 3.8.4

Release notes 26 July 2024

## Introduction

We are happy to announce **Release 3.8.4**, another minor release with a number of additions, smaller new features and several improvements. 27 tasks were performed, partially based on client requests. Due to major activities in the area of user feedback and controlled user interviews, collected test findings from various international sources and our own improved testing environment, this release also addressed 10 bugs that were fixed. Thanks to all testers and people who submitted improvement suggestions! We are lucky to have you with us.

In total, 37 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8.4 focuses on the following major topics:

- the new [→](#artifact-identifier-management) **Artifact Identifier Management** for Project administrators,
- in the area of [→](#terminology) **Terminology** we display properties for ORPHAcode in our Browser and we added HPO as a Third-Party code system for search, also enhancements and corrections were applied,
- new features and corrections to the [→](#questionnaires) **Questionnaires Management** Panel,
- enhancements for [→](#scenario-transactions) **Scenario / Transactions** for smart cloning of new Scenarios based on new version of an underlying dataset,
- enhancements in the area of  [→](#software-development-tools-features) **Software Development / Tools features**,
- enhancements to  [→](#tsting-and-validation) **Testing and Validation**.

In addition, the announced eXist Database Patch is now available for v6.2.0 (and newer).

## Areas of New Features and Improvements

### Artifact Identifier Management

We added support for changing Ids of artifacts by adding our **Artifact Identifier Management** [AD30-1391, AD30-1417, AD30-1509]. The already existing part for adding ids to Template element and attribute definition was reachable through the Project Development Menu option. This is now accompanied by this new ID Management feature.

<img src="../../../img/image-20240730192224608.png" alt="image-20240730192224608" style="zoom:67%;" />

In edge cases you may want to change the ID of an existing artifact. This should be done only in the development phase of an artifact. The Artifact Identifier Management dialog offers you options to select an artifact and change its ID.

You first choose the type of artifact you want to change using the popup menu at the button.

<img src="../../../img/image-20240730192418276.png" alt="image-20240730192418276" style="zoom:67%;" />

Dependent on the chosen type the opening dialog offers all items on a left sided navigation card. When selection an item, the dialog to the right shows all options for the ID change. 

<img src="../../../img/image-20240730192657260.png" alt="image-20240730192657260" style="zoom:67%;" />

The top card shows some item details, the bottom card allows to change the ID and lists all affected artifacts with some details. In case the ID is already exsting the item is shown are saving the change is disabled.

<img src="../../../img/image-20240730193207354.png" alt="image-20240730193207354" style="zoom:60%;" />

You may add the New ID for the change.

::: note

OID pattern is `[0-2](\.(0|[1-9][0-9]*))*`. An OID identifies exactly one object and SHALL NOT be reused. OIDs of three nodes or less are always reserved for a purpose. See [https://oid-info.com](https://oid-info.com/) or the hosting servers' OID index when in doubt.

:::

When the new ID is ok from the admins perspective the save button changes the item's ID.

::: note

When changing the ID, ART-DECOR does not automagically change all dependencies. Changing IDs is on users own responsibility and possible reference changes that were listed in the affected item list need to be changed manually.

:::

**Further Achievements in this Release for Project**

- The Artifact Hoover History Handling was completely re-designed to now be part of a scheduled tasks rather than an external script action [AD30-1621]. For more information about "hoovering" projects see our [ART-DECOR® Hoover](https://docs.art-decor.org/administration/hoover/) information and documentation section.
- The mycommunity call was removed from the Project API [AD30-1495].

### Terminology

#### ORPHAcode

We added enhancements for the display of properties such as provided Concept Maps for ORPHAcode in Terminology Browser [AD30-1585].

<img src="../../../img/image-20240730194045736.png" alt="image-20240730194045736" style="zoom:67%;" />

#### Human Phenotype Ontology (HPO)

We did not yet support the Human Phenotype Ontology (HPO). We added this is a Third-Party supported Code System [AD30-1586] and also show properties such as provided Concept Maps.

<img src="../../../img/image-20240730194330398.png" alt="image-20240730194330398" style="zoom:67%;" />

#### Working with LOINC® panels: Answer list order and intensional value sets

When intensional value sets are expanded the order of the concepts is determined by the way they appear in the database. This means that LOINC answer lists may not be expanded in the correct order as specified by LOINC. If the order of concepts in a value set is important this value set should by defined extensional.

When using the LOINC panel import function all value sets for answer lists will be created as extensional sets and will be in the correct order.

**Further Achievements in this Release for Terminology**

- Switch from ART API call to `getValueSetById` to API `getValueSetById` caused failure when getting value set association information [AD30-1617]; this has been fixed.
- Add Status Dot to associated Value Sets [AD30-1619].

### Questionnaires

The Questionnaire item text XHTML markup handling was enhanced [AD30-1605]. In ART-DECOR, Dataset and Scenario Concept names and descriptions can contain markup. The mechanism to transform a Scenario into an ART-DECOR Questionnaire (and possibly transfer it into a FHIR Questionnaire) was enhanced to allow this feature throughout the process.

<img src="../../../img/image-20240730195051671.png" alt="image-20240730195051671" style="zoom:60%;" />

**Further Achievements in this Release for Questionnaires**

- Fixed a technical issue in editing questionnaires [AD30-1543]
- Fixed a problem in editing questionnaires where ordinals were not saved [AD30-1549]
- Fixed a problem in saving questionnaires when id definition for oid is missing [AD30-1601]
- Added support for binding value sets to questionnaire question responses [AD30-1558]
- Added support for Questionnaire/QuestionnaireResponse history [AD30-1591]
- Fixed an issue in http headers upon download of a Questionnaire Response [AD30-1580]

### Scenario / Transactions

#### **Smart Cloning of a Scenario**

When using the [new SMART clone feature](https://docs.art-decor.org/documentation/releasenotes/release383/#scenario-transactions) to re-assign a dataset to a transaction the process now only looks after concepts with status draft or final or new  [AD30-1613].

### Software Development / Tools features

- Expanding large tree views in asynchronous chunks [AD30-1383]
- The password generation was optimized [AD30-1455]
- A problem with FHIR artifact download links was fixed [AD30-1493]
- All fonts used in the app have moved from Content Delivery Network (CDN) to self hosted [AD30-1567]
- When creating a new project it is created always with description and email address user [AD30-1569, AD30-1570]
- The art-data/userinfo/guest.xml accessibility was fixed for more security [AD30-1573]
- The API call dataset/transaction extract got the download=true option  [AD30-1578]
- The ART-DECOR appversion and buildNumber was added to the meta tag to any HTML header for optimized surveillance purposes [AD30-1579]
- Operationalization was missing in getFullDatasetTree [AD30-1604]
- The API called a function in error which was fixed [AD30-1618]
- Unwanted node rendering of all "property" nodes is now prevented [AD30-1628]
- Acceptance Tests for Release 3.8.4 were performed [AD30-1630]
- Temple edit now allows forms up to a maximum length of 200,000 bytes [AD30-1581]
- NICTIZ's Dataset Birth Care 3.2 produced an out-of-memory condition [AD30-1551]
- The myCommunity API list calls were adapted [AD30-1574]

### Testing and Validation

Missing icons on Validation Panel were added back [AD30-1599], the Validation Severity is now shown with icons as expected.

<img src="../../../img/image-20240730201044919.png" alt="image-20240730201044919" style="zoom:67%;" />

When validation compilation errors occur during the validation script compilation or validation process this is now shown in the Project Scheduled Task area and in the "Compile Development Version" tab of the Project Development Panel [AD30-1600].

### eXist Database Patch available for v6.2.0 (and newer)

We noted in some projects that – under certain circumstances – the **database index** of a project folder dropped accidently while editing with the ART-DECOR tool with the effect that the project was no longer accessible on the server. After a re-index of that folder the project was back for access. We first noted this in our User Management where similar problems occurred.

We created a set of test runs on our pre-production development server ATIS and finally found the problem reproduceable. With our partners from [eXist Solutions](https://www.existsolutions.com) we continued the analysis of the problem when editing files and checked if index got lost [AD30-1485].

We created a **patch for the underlying eXist database software** [AD30-1588] and applied this database patches and tweaks on all our Customer Cluster Servers including our main and development servers. Customers who use ART-DECOR on "foreign" hosted servers, please contact us for getting support for the patch.

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset and Questionnaire editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org) team. Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl). Thanks to Gunter Haroske working for the [Deutsche Gesellschaft für Pathologie / German Association of Pathology](https://www.pathologie-dgp.de) pointing to shortcomings and missing features in our Questionnaire functions and to the Dutch [Uitkomstgerichte Zorg](https://www.uitkomstgerichtezorg.nl) Program for contributions in the same area. Special thanks also to the [mio42 GmbH](https://mio42.de) team for feature suggestions and bug fix requests.

Please don't hesitate to write us with suggestions for improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.5 quickly, planned to be published at the beginning of September 2024. One of the major aspects are more features and enhancements for the Questionnaire Management. As you can see: more on that to come. Thank you for your patience. Stay tuned.
