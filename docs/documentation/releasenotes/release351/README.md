---
permalink: /documentation/releasenotes/release351
---
# ART-DECOR® – Release 3.5.1

Release notes 11 August 2022

## Introduction

We are happy to announce the intermediate **Release 3.5.1**. Next to some major enhancements we arranged a large amount of smaller additions and fixes. Here are more details.

## General Remarks

Release 3.5.1 focuses on the following major topics.

- **Template Associations Panel** allows create, update and delete of associations with simple drag-and-drop actions.
- Substantial **API performance enhancements**
- **Broadcasting realtime updates** for most of our panels on changes when using ART-DECOR with multiple concurrent users
- **Associations** between Choice Lists Elements and Codes in a Value Set
- **Identifier Associations** for Dataset Concepts and Transactions
- Options to add Value Set and Template **References from foreign Repositories**
- Enhanced **Search options** for navigation trees

Furthermore, we made good progress on **Questionnaires** as well as renewed and enhanced options for **Implementation Guides** and **Publications**. We are confident that in upcoming Releases these areas will successfully be added to the tool suite.

## Major Topics

### Template Associations Panel

**Template Associations Panel** allows now ready for list, create, update and delete of associations with simple drag-and-drop actions. It offers a new look-and-feel to create links between a logical concept in a dataset or scenario and a technical artefact such as HL7 Templates or Template components.

<img src="../../../img/image-20220623152425972.png" alt="image-20220623152425972" style="zoom:50%;" />

In principle, the Panel allows you to select a Dataset or Scenario <ref1/>, the content is shown in the left Navigation Bar <ref2/>. Please note that concepts that have an association with a Template or Template Element are in bold face.

The right Navigation Bar <ref3/> shows all Templates of the Project, as usual grouped by type. Both the left and the right Navigation Bar can be expanded with one click.

The area denoted by <ref4/> will carry the details of the selected Dataset / Scenario concept, the Template and of course the actual mappings/associations. For example if you select *Person*––*Date of birth* on the left Dataset details, the middle area shows the following details.

<img src="../../../img/image-20220623153026886.png" alt="image-20220623153026886" style="zoom:67%;" />

The top middle area shows some details of the selected concept. The middle part shows the Template were the mapping/association points to. Finally, the lower part depicts the mapping/association between the selected concept and the Template/Template element.

When clicking on a Template in the Templates Navigation Bar,...

<img src="../../../img/image-20220623161012075.png" alt="image-20220623161012075" style="zoom:50%;" />

...it morphs the Navigation Bar (showing all Templates) to display the selected Template Element hierarchy.

An association can be created when a concept and a Template or Template Element is selected. You simply take the part of the Template, drag it into the middle area and drop it there. 

<img src="../../../img/image-20220623160745185.png" alt="image-20220623160745185" style="zoom:50%;" />

The Template Association is then created and displayed as follows in the lower middle card. It can be deleted by simply click on the X button in the arrow.

<img src="../../../img/image-20220623160805561.png" alt="image-20220623160805561" style="zoom:67%;" />

### Substantial API performance enhancements

The performance of our API has been tremendously improved. Our backend, the eXist database ([https://exist-db.org](https://exist-db.org/)) is an integral part of the ART-DECOR® tool suite. Add-on *Roaster*, the Open API Router for eXist from the TEI Publisher Project Team, routes all REST requests to the corresponding XQueries. 

![image-20220817115811546](../../../img/image-20220817115811546-0730292.png)

Recently, we re-arranged the architecture of our Open API *Roaster* software component (see [repository](https://exist-db.org/exist/apps/public-repo/packages/roaster)) and tremendously speeded up the performance. 

We [dedicated a BLOG to this topic](https://art-decor.org/roast-the-deficit/) as it might be of interest for other database users [AD30-803].

<img src="../../../img/image-20220817115917204.png" alt="image-20220817115917204" style="zoom:67%;" />

*Photo by [Nicole Baster](https://unsplash.com/@nicolebaster?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/speed-up?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)*

In addition, certain API call race conditions affecting all panels have been eliminated [AD30-701].

### Associations between Choice Lists Elements and Codes in a Value Set

Creating and maintaining associations between Choice Lists Elements of a coded Dataset Concept and the Codes in a Value Set is now possible [AD30-699]. This feature is offered through the Terminology Menu item "Associations" and allows to associate a Choice list element like *blue*, *brown*, *green* etc. in the example below with Coded Concepts within a Value Set like the SNOMED Code *301952009 Blue iris (finding)*.

<img src="../../../img/image-20220818184733989.png" alt="image-20220818184733989" style="zoom:50%;" />

In the Panel you first choose a at the left site a Dataset or Scenario where you want to add Associations to. 

<img src="../../../img/image-20220818185811775.png" alt="image-20220818185811775" style="zoom:50%;" />

The right site shows the details of the Concept as usual <ref1/>, in addition it allows project authors to add Coded Concepts for the Datatset item itself <ref2/>. If the Dataset Concept is coded, it typically gets a Value Set association <ref3/> that defines the expected or allowed values for the codes. When a Value Set association exist you then can create and maintain associations through the Associations Editor <ref4/>.

<img src="../../../img/image-20220818191140347.png" alt="image-20220818191140347" style="zoom:50%;" />

The Association Editor offers a selector for a Value Set <ref1/> where the codes are residing for the associations. The list of codes in the value set is display to the right <ref2/>, the left part summarizes all choice list elements that can get associated with a code <ref3/>. If you only want to see those items that so far have no associations, use the switch toggle to show/hide items without mappings only <ref4/>.

<img src="../../../img/image-20220818191337649.png" alt="image-20220818191337649" style="zoom:50%;" />

To acutally create the Association simply drag the correct Code from the right Value Set over the corresponding Choice List Item a drop the code there.

<img src="../../../img/image-20220818191841690.png" alt="image-20220818191841690" style="zoom:60%;" />

After this drag and drop action the association is store immediately and displayed in the left list appropriately. Associations can be deleted in the left list by opening the association list expandable and click on the trashcan.

<img src="../../../img/image-20220818192024373.png" alt="image-20220818192024373" style="zoom:80%;" />

### Broadcasting realtime updates

With Release 3.5 we began to establish HTTP Proxy WebSocket techniques for all areas where changes might be of interest for concurrent users.

The **ART-DECOR HTTP Proxy** consists of two parts, the HTTP Proxy and the WebSocket Server. Any request that is sent to the proxy server will be passed to the ART-DECOR API. The proxy server will return the response exactly as is.

The WebSocket techniques makes also working together easier and safer, as everyone working simultaneously in the same area of artefacts in a project gets informed about changes other team members just did and in fact sees the changes as an update. For example if one team member changes the name of a concept...

![image-20220623165326817](../../../img/image-20220623165326817.png)

...other team members in the same Panel gets informed with a snackbar notification and the concepts is also updated fro them.

![image-20220623165313839](../../../img/image-20220623165313839.png)

Broadcasting realtime updates have now been completed for most of our panels on changes when using ART-DECOR with multiple concurrent users:

- Realtime updates for Projects [AD30-731]
- Realtime updates Project Authors [AD30-732]
- Realtime updates Project Identifiers [AD30-782]
- Realtime updates for the Dataset Panel [AD30-684]
- Realtime updates for the Scenario Panel [AD30-705]
- Realtime updates for Actors panel [AD30-723]
- Realtime updates Labels panel [AD30-737]
- Realtime updates for Value Sets [AD30-677]
- Realtime update of Value Set (not most recent version) no longer fails [AD30-727]
- Realtime updates for Mappings Panel [AD30-725]
- Realtime updates for the Code System panel [AD30-689]
- Realtime updates for Terminology identifiers panel [AD30-724]
- Realtime updates for the Templates Panel has been added [AD30-678]
- Realtime updates News items [AD30-679]
- Realtime updates Governance groups panel [AD30-733]
- WebSockets: Adding/deleting an actor now refresh the graphic on the transaction group [AD30-721]
- Realtime updates for DECOR-locks panel [AD30-783]
- Realtime updates for panels: texts and translations [AD30-720]
- Refactoring listeners for realtime updates [AD30-726]
- Support event broadcasting for updating project authors established [AD30-771]
- Prevent logging syntax errors for http responses with no content [AD30-772]
- Prevent broadcasting 'undefined' event [AD30-774]
- Allow broadcasting of server/governancegroup and server/settings [AD30-786]

### Enhanced Search options for navigation trees

The Search field in Navigation Cards / Tree Views now takes usual terms but also OIDs to find items in the tree. This works either with a complete OID or only the last digit of the OID.

![image-20220818115747881](../../../img/image-20220818115747881.png)

### Identifier Associations

**Identifier Associations** for Dataset Concepts and Transactions can now be added.

### References from foreign Repositories

Options have been implemented to add and delete Value Set and Template **References from foreign Repositories**.

## Further Enhancements and Fixes

Edit and Maintain Terminology Mappings type 3: Choice List – Concept Association

**Project**

- Author Panel in Menu and Edit options are now more consistent [AD30-766]
- The notifier when a project author signals whether he want to subscribe to all issues are now displayed correctly [AD30-760]
- For a set of clients a "Smooth Migration Support" has been established so users can see that a project is in migration and where to go to [AD30-819]
- An Unlock Panel has been established to list and remove locks projects wide [AD30-112]

**Datasets**

- It is now possible to search on id/oid for dataset concepts with the treeview search field [AD30-807]
- Identifier associations are now rendered in the context of the concept/valueDomain [AD30-775]
- The correct button for de-containment in Dataset Panel is now used [AD30-787]
- Dataset list details have been made more consistent across different views [AD30-790]
- Dataset name who are too long in treeview to display now get a tooltip/hover to show the whole name [AD30-455]

**Scenarios/Transactions**

- Scenario/transaction/representingTemplate/context are now present [AD30-715]
- Sequence Diagram is now always shown in Scenario Panel [AD30-788]
- A transaction / Actor change is now stored properly [AD30-718]

**Terminology**

- A "new reference button" for Value Sets and Code System Panel has been added [AD30-791]
- ValueSet copyright statement now returned properly [AD30-704]
- Better support for Template Elements of type Code [AD30-749]
- Code System Maintain: Add/Edit Code Description [AD30-691]
- New Code Systems can be created or existing ones be cloned and added to a project [AD30-693]
- Codes can be added to the end of a Code System [AD30-798]
- Insert into label for inserting Coded Concepts [AD30-799]
- Cloning reference Code Systems now possible [AD30-800]
- More info on deprecated concepts in SNOMED CADTS browser is now displayed [AD30-686]
- Added code systems to terminology identifiers panel [AD30-738]

**Templates / Profiles**

- Template Reference can now be deleted [AD30-816]
- Problems with Template attributes when creating examples have been fixed [AD30-805]
- The Rules Identifier Panel has been added  [AD30-784] [AD30-472]
- All selected on Template Prototype Selection and Search Throttle/Debounce now in use [AD30-743]
- Edit Value Set Binding possible now [AD30-745]
- Cloning template elements possible now [AD30-746]
- Illegal flexibility upon Template store has been fixed [AD30-750]
- Adding Template Associations of Template Fragments now possible [AD30-761]
- Adding attributes to Templates without a Namespace now possible [AD30-796]
- Bug when adding a Template Reference has been fixed [AD30-813]
- Problem fixed when starting to add a Template Reference to a project that has no templates so far [AD30-814]

**Issues**

- The German term "Vorgang" was chosen for issue over "Problem/Frage" [AD30-837]

**Associations**

- Identifier associations for dataset concepts can now be added and maintained [AD30-578]
- Identifier associations for transactions can now be added and maintained [AD30-579]
- The difference between terminology and identifier associations from dataset versus transaction are now better layouted [AD30-669]
- Mappings panel now not causes duplicate http requests [AD30-728]
- Mappings panel transactions: concepts now reloaded when transaction is changed [AD30-729]

**Software Development / Tooling**

- The TipTap editor now also allows Blockquote, Citations and H4 headings, the toolbar has been re-arranged [AD30-555]
- Relationship section enhancements [AD30-811]
- Add new concept dialog - enhancements and bugfixes [AD30-812]
- Alignment of front page server statistics [AD30-818]
- Backend for History of a Project got an algorithmic refurbish [AD30-847]
- WebSockets: scenarios: Inserted new group, triggers a tree collapse [AD30-722]
- A CSS Deprecation Warning has been suppressed during build [AD30-676]
- Unwanted API calls in dataset panel has been suppressed [AD30-700]

**Server Administration Tasks**

- New users are now added to the project with their email address [AD30-759]
- New user names are now autofiltered, capital letters as start and trailing numbers 0-9 are now allowed [AD30-778]
- Front page cache status now is updated after cache refresh [AD30-817]

### Publications

Code Systems are now added to the list of normal Terminology Publications [AD30-815].

A Warning is shown on Value Sets and Code System Publications that published terminologies are a snapshot in time only [AD30-854].

![image-20220817113452432](../../../img/image-20220817113452432.png)

### Documentation Enhancements

We update our user manual / user interface documentation part at our [Documentation Site](docs.art-decor.org). The following areas have ondegone larger additions and enhancements.

- [Project](https://docs.art-decor.org/documentation/project/) [AD30-751]
- [Dataset](https://docs.art-decor.org/documentation/datatset/) [AD30-739]
- [Scenarios](https://docs.art-decor.org/documentation/scenario/) [AD30-740]
- [Terminology](https://docs.art-decor.org/documentation/terminology/) [AD30-753]
- [Value Sets](https://docs.art-decor.org/documentation/valueset/) [AD30-741]
- [Code System](https://docs.art-decor.org/documentation/codesystem/) [AD30-754]
- [Template](https://docs.art-decor.org/documentation/template/) [AD30-755]
- [Issue](https://docs.art-decor.org/documentation/issue/) [AD30-756].

## Incoming features on further Releases

ART-DECOR Release 3 is also a horizontal move of all "Classic" Release 2 features, plus adding new features immediately if appropriate or later as planned. This migration is now almost complete. We focus on new features and functionalities.

The following main areas are still under development and are seen as incoming functionality in the following Releases:

Terminology

- Expansion Sets of Intensional Value Sets for CADTS-aware Code Systems in ART-DECOR. This already works for SNOMED-only Value Sets and will be extended to be useable for intensional Value Sets that make use of any Code System, especially Project proprietary ones.
- Expansion Sets can be stored/presisted after expansion and appear as sub items of the source Value Set.

Questionnaires

- Questionnaire (QQ) and Questionnaire Response (QR) as defined in the FHIR standard plus an ART-DECOR wrapper to allow consistent handling like all versionable artefacts will be further enhanced from a subsequent Release on.
- An editor for the Questionnaire Items is in development and will be released with 3.6+, a new member of the Expert Group dedicates work on visualization and creation/maintenance of Questionnaires. 
- Questionnaires Enhanced (QE) will also be introduced, starting with Release 3.4.1: once Questionnaires have been edited/enhanced by the user with for instance: layout instructions, conditions, it migrates its status to a Questionnaires Enhanced (QE) in order to be distinguished from genuinely generated Questionnaires in ART-DECOR.

Rules

- The work on the FHIR Profile Editor has started.

System Architecture

- GIT integration as a natural extension of the ART-DECOR Building Block Repository concept will be subject of one of the next releases, collaborations have been established to quickly bring this topic to first functionality.

## Outlook on what we expect in next ART-DECOR® Releases

The current development is heading towards a soon next Release, where the following topics are on:

- The new Implementation Guide Feature [AD30-438, AD30-820] will be introduced that allows writing and rendering Implementation Guides (IG). An Implementation Panel [AD30-827] shows all IGs created and maintained for the Project and offers metadata editing [AD30-828]. The IG Guide Editor is developed and tested that make use of our multilingual Text Editor for the hierarchy of pages and drag and drop capabilities to add resources from the Project/Scenario [AD30-829]. This feature is far developed and the first phase of features is to be expected part of the next major release.
- View, store and maintain **Value Set Expansions** when the Value Set contains intensional instructions such as include or exclude operations
- **Questionnaire** (QQ) and **Questionnaire Response** (QR) Management and a Questionnaire **Editor**
- First **FHIR profiling** (Release 3.7 and further) will be offered.
- Smaller Enhancements and Bug Fixes on Release 3.5.1

As you can see: more on that to come. Thank you for your patience. Stay tuned.
