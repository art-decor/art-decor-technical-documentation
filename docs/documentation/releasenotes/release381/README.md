---
permalink: /documentation/releasenotes/release381
---
# ART-DECOR® – Release 3.8.1

Release notes 17 December 2023

## Introduction

We are happy to announce **Release 3.8.1**. A number of additions, new smaller features and improvements, documented in around 30 tasks were performed in this minor release and 25 bugs were fixed. During development more than 60 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8.1 focuses on the following major topics:

- Additions and corrections to the [→](#dataset) **Dataset** Panel,
- Enhancements to the [→](#questionnaires) **Questionnaires Management**,
- Enhancements to the [→](#implementation-guides) **Implementation Guide** Panel, e. g. the ability to reference to a resources from the resource list of the IG
- Additions to the [→](#concept-maps) **Concept Map** Panel
- Enhancements for [→](#scenario-transactions) **Scenario / Transactions**, especially in the area of concept with Textual and/or Structural Conditions
- Enhancements and corrections in the area of [→](#terminology) **Terminology**, the [→](#templates) **Templates** Panel, regarding [→](#governance-groups) **Governance Groups**, concerning [→](#software-development-tools-features) **Software Development / Tools features** and the [→](#server-admin-panel) **Server Admin Panel**.

## Major Topics

### Dataset

In the New Concept dialog the buttons all are now moved to a more expected and logical place, the bottom right area of the dialog [AD30-1206].

<img src="../../../img/image-20231218105536897.png" alt="image-20231218105536897" style="zoom:67%;" />

**Further Achievements in Release 3.8.1 for Datasets**

- The Property *timeStampPrecision* of Dataset Concepts was not editable, this has been fixed [AD30-208]
- A problem was fixed where examples weren't shown properly [AD30-1175]
- A bug was fixed where the Dataset tree was empty when inserting new group after inherit and cancel action [AD30-1300].
- A problem was fixed where the status for the demo1- project publication could not be set [AD-30-1179].

### Questionnaires

**Questionnaires** in ART-DECOR are versionable Objects that define forms with groups and enterable item fields that can subsequently be rendered as forms and populated with data.

The Questionnaire Panel was completed [AD30-203] and Questionnaires were added to appear in publications [AD30-1025], also transaction/enablewhen is now support in HTML publications [AD30-1146].

When rendering a Questionnaire either with LHC Forms...

<img src="../../../img/image-20231218110955258.png" alt="image-20231218110955258" style="zoom:50%;" />

... there was no option to save the Questionnaire Response. A Questionnaire Response can now be saved [AD30-1048]. The saved Questionnaire Response is automagically associated with its underlying Questionnaire.

**Further Achievements in Release 3.8.1 for Questionnaires**

- An issue was fixed when performing a Transform transaction to questionnaire [AD30-1167]
- Under circumstances, an enableWhen function constantly disappeared from an item after saving. This error was fixed that appeared only when using the enableWhen functionality [AD30-1256].
- The Project Index Panel was enhanced. The Project Index panel now shows list tabs for DATASETS, TRANSACTIONS, VALUE SETS, CODE SYSTEMS, TEMPLATES, QUESTIONNAIRES and CONCEPT MAPS [AD30-1297].
- For a specific pathology project the user reported the error message *not facet-valid with respect to maxLength '80' for type 'ShortDescriptiveName*. This was fixed permanently by aligned ALL instances of displayName to use the same definition of NonEmptyString which imposes a minimum but no maximum [AD30-1291].

We documented what ART-DECOR supports with regards to **FHIR Questionnaires** in our [Implementation Guide](https://ig.art-decor.pub/art-decor-questionnaires/).

### Implementation Guides

In Release 3.8 we added new features to the Implementation Guide Feature. When editing an Implementation Guide page it is now allowed to add and change resources (such as Value Sets and Templates) as link or object [AD30-880] and the TipTap feature was enhanced to allow images being uploaded and embedded [AD30-848]. 

![image-20231023155816207](../../../img/image-20231023155816207.png)

The UI got two extra buttons in the Tip Tap menu when editing a page for adding images (left button) and artefacts (right button).

![image-20231023155952408](../../../img/image-20231023155952408.png)

This feature has been enhanced [AD30-880]. The following figure shows a visualization of three types as so-called *Artifact Pills*.

<img src="../../../img/image-20231218110124219.png" alt="image-20231218110124219" style="zoom:60%;" />

When a user is in edit mode of an IG text page, he is now able

- adding a reference to a resources from the resource list of the IG
- editing a reference to a resources from the resource list of the IG
- delete a reference.

The Artifact Pills act as pointer to the resource definition when not in edit mode.

**Further Achievements in Release 3.8.1 for Implementation Guides**

- Unwanted icon changes upon IG page save have been fixed [AD30-1231]

### Concept Maps

We introduced the artifact filter [AD30-1265] based on status as for many other artifacts, too. 

<img src="../../../img/image-20231218104617249.png" alt="image-20231218104617249" style="zoom:53%;" />

Now you can use the usual filter to intentionally not show artifacts with a certain status. 

<img src="../../../img/image-20231218104536594.png" alt="image-20231218104536594" style="zoom:30%;" />

**Further Achievements in Release 3.8.1 for Concept Maps**

- RetrieveConceptMap missed correct target value set information which is now corrected [AD30-1283]
- Cloning of a Concept Map is now possible [AD30-1288]
- Project Index Panel now also shows CONCEPT MAP tab [AD30-1315]
- Realtime updates for Concept Maps were introduced [AD30-1293]

### Scenario / Transactions

The mechanism to support structured conditions in Condition Predicate Tables as an extension to the Scenario/Transaction dialog for Conditions has been completed. A concept may have **Textual** and/or **Structural** Conditions and they are saved [AD30-1143]. Structural Conditions are also promoted when converting a Scenario / Transaction to a Questionnaire [AD30-1144] and exposed also with DECOR services [AD30-1145].

<img src="../../../img/image-20231218112027400.png" alt="image-20231218112027400" style="zoom:67%;" />

**Further Achievements in Release 3.8.1 for Scenario / Transaction**

- Several bugs have been fixed that appeared when cloning a Scenario / Transaction [AD30-1188] or creating a Transaction [AD30-1269]
- A bug was fixed that appeared when saving a Scenario with terminologyAssociation and then lost the terminologyAssociation [AD30-1313]

### Terminology

When manually adding/editing codes in Value Set Maintenance (Composition), a validation against the terminology report can be triggered to see if codes are deprecated, or not present in the used Code System [AD30-960].

<img src="../../../img/image-20231218115403147.png" alt="image-20231218115403147" style="zoom:60%;" />

In the Code System Editor the field description wasn't displayed properly when editing or showing a code. This has been fixed [AD30-1257].

<img src="../../../img/image-20231218115653157.png" alt="image-20231218115653157" style="zoom:27%;" />

The Value Set Panel now shows preferred terms in the code list as expected [AD30-1286].

<img src="../../../img/image-20231218120318636.png" alt="image-20231218120318636" style="zoom:67%;" />

**Further Achievements in Release 3.8.1 for Terminology**

- An Exception for GET fhir/1.0/{project}/ValueSet/{id}--{version} "Argument 2: evaluates to an empty sequence, but an empty argument is not allowed here" was fixed [AD30-1267].
- An error was fixed that when adding a new item to a Value Set, and an error occurred, a cryptic error message was displayed to user [AD30-1278].
- A problem was fixed for DECOR-services *RetrieveValueSet*, *RetrieveCodeSystem*, *RetrieveConceptMap* that produced incorrect xml/json links containing the placeholder DEEP-LINK-PREFIX-FAILURE [AD30-1279].
- The ART-DECOR Release 2 (outdated) ValueSet editor could not handle completeCodeSystem as include which has been fixed [AD30-1284]
- Creating Value Sets with completeCodeSystem and/or other codes is now possible [AD30-1289]
- During creation of a Value Set the id was not properly checked and was temporarily commented out until this has been fixed; The Value Set gets a default id from the project anyway [AD30-1306]
- When adding codes to a Value Set manually, users can fix the order of codes in the composition by drag-dropping codes in the order they want; a problem has been fixed were initially added codes were in the wrong order [AD30-1311]

### Governance Groups

There is now a Governance Group READ-ONLY landing page [AD30-1266] that can easily be reached from the ART-DECOR Tool Home / Landing Page in the Service Card (link at the bottom).

<img src="../../../img/image-20231218114823296.png" alt="image-20231218114823296" style="zoom:67%;" /> 

In addition the Governance Group panel (the read-only one and the System Admin one) now shows DETAILS - DASHBOARD - PROJECTS - VALUE SETS - CODE SYSTEMS - CONCEPT MAPS - TEMPLATES - QUESTIONNAIRES [AD30-1296].

### Software Development / Tools features

The User Information and Preferences file has now been split into separate files per user [AD30-1247]. This avoids losing index on this heavily used file or its parent folder. The restructuring is done upon installation of Release 3.8.1 ART or API packages. 

In addition, we performed some enhancements and bugfixed on the general Tool / App appearance.

- A detected wrong permissions handling on user-data file upon creation have been fixed [AD30-1294].
- All Fullscreen cards were aligned to be the same [AD30-1203]
- Moving a concept locked the dataset on *id* only, instead of *id* + *effectiveDate*, this has been fixed [AD30-1253]
- Better line break rules are now used in Text Fields [AD30-1271]
- Confusing naming of broadcast paths are now avoided [AD30-1276]
- DECOR-services uses now local XSL Stylesheets for output, not those from `assets.art-decor.org` [AD30-1280]
- DECOR2html links do not upgrade to https for localhost [AD30-1281]
- FullDatasetTree enhanced ValueSets now contains the missing parts of the definition [AD30-1282]
- For a new project, the first `author.id` is now 1 instead of 0 [AD30-1301]
- DECOR Locks can now be seen or deleted by "non-admin" project authors [AD30-1209]
- URNs such as `urn:ihe:iti:xds-b:2007` are now allowed to be used when adding namespace prefixes to the project [AD30-1248]

### Templates

There have been some enhancements to the Templates Panel

- Realtime updates Template associations panel are now working [AD30-735]
- The Template's item description in `Template.item.desc` was not rendered, this has been fixed [AD30-1186]
- The temple button in Template Panel was outside of screen [AD30-1262]
- Validation support (independent from XIS package) backend is established [AD30-1171]

### Server Admin Panel

The Server Admin Panel got several enhancements and bug fixed. After identifying problems like

- Multiple API calls when making changes
- Vuex store is mutated directly
- When a PATCH call fails, the UI shows the wrong data in the inputs
- Translation labels missing
- Template in .vue file is hard to read because of a lot of (unnecessary) logic
- Panel takes up more space than needed
- The default value of the ART server logo href is hardcoded and still http

... all of the above have been fixed or got an enhancement [AD30-1239]. As a System Admin one now can see the list of projects in the Projects Panel along with a search field [AD30-1258].

In ART-DECOR Release 2 we had a function called “Fix ART permissions” that could be triggered by the System Administrator to do essentially a few things, all together.

1. Fix permissions for collection `/db/apps/art` and `art-data` so the right user (groups) can read/write/execute
2. Synchronize the eXist-db users with `/db/apps/art-data/user-info.xml`
   - Makes sure all users are listed with their groups, full name, organization and email
3. Fix permissions for collections `/db/apps/decor/data`, `/history`, `/releases`, `/scheduled-tasks`, and `/tmp` so the right user (groups) can read/write/execute
4. Clean up `/db/apps/decor/tmp` so only the latest copy per project is left
5. Clean up `/db/apps/decor/releases/*/development` so only the latest 3 compilations per project are left

In ART-DECOR Release 3, this function resides in the Server Management Panel. Originally it was designed in exactly the same (somewhat overloaded) way but is now split into two tasks [AD30-1270].

- The **Fix ART permission** function now executes the permission functions, shown in item 1, 2 and 3 above,
- The new **Clean Up DECOR** function performs all clean-up functions, mentioned as items 4 and 5 in the list above.

<img src="../../../img/image-20231218113756607.png" alt="image-20231218113756607" style="zoom:67%;" />

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset and Questionnaire editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org) team. Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl). Thanks to Gunter Haroske working for the [Deutsche Gesellschaft für Pathologie / German Association of Pathology](https://www.pathologie-dgp.de) pointing to some shortcomings of one of our Questionnaire functions. 

A great review was done by and discussed with the [mio42 GmbH team](https://mio42.de) with a couple of enhancement suggestions and bug fix requests, most of them flowing into the upcoming minor and major releases.

Please don't hesitate to write improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.2 soon, planned to be published in late January 2024. As you can see: more on that to come. Thank you for your patience. Stay tuned.
