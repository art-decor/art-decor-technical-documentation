---
permalink: /documentation/releasenotes/release382
---
# ART-DECOR® – Release 3.8.2

Release notes 25 February 2024

## Introduction

We are happy to announce **Release 3.8.2**. Again, we decided to have a minor release with a number of additions, smaller new features and several improvements. We documented 28 tasks that were performed. Due to major activities in the area of user feedback and controlled user interviews, collected test findings from various international sources and our own improved testing environment, this release also addressed 52 bugs that were fixed. Thanks to all testers and people who submitted improvement suggestions! We are lucky to have you with us.

In total, 80 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8.2 focuses on the following major topics:

- new features and corrections to the [→](#dataset) **Dataset** Panel,
- enhancements and corrections in the area of [→](#terminology) **Terminology** including the our new Terminology website, 
- new options for the Design Details and corrections to the [→](#templates) **Templates** Panel,

- enhancements to the [→](#questionnaires) **Questionnaires Management**,
- enhancements for [→](#scenario-transactions) **Scenario / Transactions** for more shortcuts while designing a transaction,
- enhancements to the [→](#project) **Project** Copyright card and Author lists,
- better options for a consistent download of artifacts as well as more Keyboard Shortcuts for Multilingual text fields in the area of  [→](#software-development-tools-features) **Software Development / Tools features**
- additions to the [→](#concept-map) **Concept Map** Panel
- enhancements regarding [→](#governance-groups) **Governance Groups**, and the [→](#server-admin-panel) **Server Admin Panel**.

## Areas of New Features and Improvements

### Dataset

When creating a new Dataset Concept, the dialog now allows to change the type of the Concept (item or group), regardless of what has been pre-selected by the Tree View button  [AD30-1345].

<img src="../../../img/image-20240226141113566.png" alt="image-20240226141113566" style="zoom:67%;" />

When creating a new Dataset Concept, the datatype can now be selected during the creation process. In addition there is a preset of the description of a new Dataset Concept [AD30-1344], while we still recommend to refine the description to reflect the meaning for all stakeholder groups and interested parties.

<img src="../../../img/image-20240226141311578.png" alt="image-20240226141311578" style="zoom:67%;" />

**Further Achievements in this Release for Datasets**

- In datasets now all synonyms for all languages are shown, not just only for the selected language [AD30-940]
- An option is now available to add a first dataset concept to an empty dataset [AD30-1062]
- An empty information card is now hidden when there is no copyright information [AD30-1340]
- The property settings for defaults on string data elements with longer texts are now readable [AD30-1343]
- When searching for related dataset concepts a slash "/" is now recognized [AD30-1361]
- The readability of some dataset concept properties is now better [AD30-1364]
- Search behavior for keywords is now corrected where also "empty" elements were reported [AD30-1374]
- Creating a new dataset concept of type "code" now includes an (empty) conceptlist [AD30-1453]
- Conformity and Cardinality is now shown in "Usage" field in Dataset Panel for some reported elements [AD30-1365]

### Terminology

Our new website [terminology.art-decor.org](https://terminology.art-decor.org) is now up and running with the first Third Party Terminologies described. We established and tested the pipelines for the creation of our Terminology Packages, upcon successful processing the updated packages are available through the repository upload (see documentation [here](https://docs.art-decor.org/administration/setupmaintain/backend/#further-installations)).

<img src="../../../img/image-20240223202155476.png" alt="image-20240223202155476" style="zoom:67%;" />

*Figure: New Landing Page of terminology.art-decor.org.*

Next to SNOMED CT and LOINC, the automated Third Party Terminology Packages we support are [AD30-913]:

- ATC - Anatomical Therapeutic Chemical (ATC) – CADTS
- NPU - Nomenclature for Properties and Units (NPU) – CADTS
- ORPHAcode - Orphanet rare disease nomenclature (ORPHAcode) – CADTS
- RadLex - RadLex Radiology Lexicon – CADTS
- HL7 Terminologies

<img src="../../../img/image-20240223202833075.png" alt="image-20240223202833075" style="zoom:50%;" />

*Figure: First list of available Third Party Terminologies with automated processing.*

Value Set Expansion Sets that have been saved are displayed in the list of Usage since Release 3.8. The list opens when the users clicks on the Usage List card.

<img src="../../../img/image-20240226142435465.png" alt="image-20240226142435465" style="zoom:35%;" />

There is now also a hint how many Value Set Expansion Sets are stored directly at the Concept List card [AD30-1353].

<img src="../../../img/image-20240226142412255.png" alt="image-20240226142412255" style="zoom:35%;" />

**Further Achievements in this Release for Terminology**

- Add Option to delete stored Value Set Expansion Sets (frontend) [AD30-1226]
- Add Option to delete stored Value Set Expansion Sets (backend) [AD30-1227]
- ValueSet expansion is broken for multi code system collections like HL7 [AD30-1232]
- SNOMED refsets: display and filter [AD30-1235]
- Getting from scenario concept value set association to the value set [AD30-1260]
- Creation of value set does not properly check id [AD30-1304, AD30-1305]
- Valueset editor, exclude shows wrong display name [AD30-1309]
- Valueset editor: compose/expand error [AD30-1310]
- Valuesets: cloning referenced VS is allowed by frontend, but gives backend error [AD30-1312]
- Temple/AD2: VS allows include own valueset (backend) [AD30-1316]
- Usage on valueset: show concept version label [AD30-1329]
- Terminology expansion: bug The actual cardinality for parameter $compose [AD30-1330]
- Terminology browser (AD3) not showing details for some SNOMED codes [AD30-1341]
- Value set create dialog breaks after step 2 [AD30-1355]
- Fullscreen mode in code system editor not yet optimized [AD30-1385]
- Create codesystem ref to SNOMED yields error [AD30-1387]
- Terminology association to valueset does not save [AD30-1428]

### Templates

ART-DECOR Release 3 has ever since the concept of a more tidied-up user interface with a focus set of information per artifact rather than showing also non-focus information at once. For the Template Panel, there we collected user requests to allow a denser view when displaying the Template Design [AD30-1406] like shown here.

<img src="../../../img/image-20240226142649223.png" alt="image-20240226142649223" style="zoom:40%;" />

The options have been explored and implemented [AD30-1321, AD30-1410]. There is now a fourth toggle switch in the row of buttons in the Template Design Card...

<img src="../../../img/image-20240226142709259.png" alt="image-20240226142709259" style="zoom:40%;" />

... that toggles between the dense and normal view.

<img src="../../../img/image-20240226142731778.png" alt="image-20240226142731778" style="zoom:40%;" />

**Further Achievements in this Release for Templates**

- Concept association is not displayed in AD3 Template view [AD30-1337]
- Template valueset binding switch version not saving [AD30-1415]
- Left - Right scrollbar disappears on very large templates [AD30-1416]
- Value Set Bindings only for coded Template Elements [AD30-1419]

### Questionnaires

A Questionnaire Metadata option "Code (Questionnaire)" now allows to add Concept Associations for the Questionnaire  [AD30-1423].

<img src="../../../img/image-20240226144608257.png" alt="image-20240226144608257" style="zoom:60%;" />

**Further Achievements in this Release for Questionnaires**

* Gather testprojects as input [AD30-196]
* Create FHIR questionnaire based on test projects [AD30-197]
* Edit existing questionnaires [AD30-201]
* Questionnaire response issues [AD30-1250]
* Add Relationship section to the Questionnaire panel [AD30-1392]
* Questionnaire PATCH issue (path /item) [AD30-1437]
* Questionnaire render bug: LHC form does not render with 404 [AD30-1454]

We documented what ART-DECOR supports with regards to **FHIR Questionnaires** in our [Implementation Guide](https://ig.art-decor.pub/art-decor-questionnaires/), that has recently been updated.

### Scenario / Transactions

In the Scenario Transaction Editor the Conformance shortcuts "NP" and "C" were missing in the dropdown. These and a few more options have been added as shortcuts [AD30-1339].

<img src="../../../img/image-20240226144937172.png" alt="image-20240226144937172" style="zoom:50%;" />

**Further Achievements in this Release for Scenario / Transactions**

- Breadcrumb in Scenarios  [AD30-511]
- Scenario: user wants option to open link to template [AD30-1322]
- Concepts in scenario/transaction with conditionals don't update correctly [AD30-1332]
- Transaction, add source-dataset does not update the representingTemplate [AD30-1335]
- Expansion of data tree in scenario-panel not possible if there is more than 1 root element [AD30-1363]
- Correct interactive dialog for transaction diagram [AD30-1373]

### Project

The Project Author List view has been enhanced [AD30-1384] to easily identify administrators by a specific icon.

<img src="../../../img/image-20240226145422017.png" alt="image-20240226145422017" style="zoom:60%;" />

Displaying the Project Copyright has been refined, addrLine type is no longer required [AD30-1405]

<img src="../../../img/image-20240226145346875.png" alt="image-20240226145346875" style="zoom:60%;" />

**Further Achievements in this Release for Projects**

- Frontend project/description edit should check for allowed author [AD30-1201]
- Copyright/fax is not returned as fax in AD3 [AD30-1302]
- Project Index Questionnaire links are also red, not blue [AD30-1372]
- A bug has been fixed where a project description, attempted to be edited by authors who were not a project author, failed upon save; they now have no options to edit the project description [AD30-1201]

### Software Development / Tools features

The option to download downloads artifacts in various formats such as DECOR format, FHIR exports etc, is now consistently set up, similar to the Project Index pills [AD30-1371].

<img src="../../../img/image-20240226145622305.png" alt="image-20240226145622305" style="zoom:70%;" />

The OID Registry Panel with list and search functionality is now available to System Administrators [AD30-1320].

<img src="../../../img/image-20240226145807270.png" alt="image-20240226145807270" style="zoom:50%;" />

**TipTap Keyboard Shortcuts** 

Multilingual text fields have many formatting options that are now also available as Keyboard Shortcuts using our editor TipTap [AD30-930]. This is the complete list of ART-DECOR's keyboard hacks for multilingual text fields.

Legenda: `⌘` Command, `⇧` Shift, `⌥` Option / Alt, `⌃` Control / Ctrl

| Command                       | Windows/Linux      | macOS       |
| :---------------------------- | :----------------- | :---------- |
| ***Editing***                 |                    |             |
| Copy                          | `Ctrl` `C`         | `⌘` `C`     |
| Cut                           | `Ctrl` `X`         | `⌘` `X`     |
| Paste                         | `Ctrl` `V`         | `⌘` `V`     |
| Undo                          | `Ctrl` `Z`         | `⌘` `Z`     |
| ***Inline Text Style***       |                    |             |
| Bold                          | `Ctrl` `B`         | `⌘` `B`     |
| Italicize                     | `Ctrl` `I`         | `⌘` `I`     |
| Underline                     | `Ctrl` `U`         | `⌘` `U`     |
| Strikethrough                 | `Ctrl` `Shift` `X` | `⌘` `⇧` `X` |
| Code                          | `Ctrl` `E`         | `⌘` `E`     |
| Subscript                     | `Ctrl` `,`         | `⌃` `,`     |
| Superscript                   | `Ctrl` `.`         | `⌃` `.`     |
| ***Headings and Paragraphs*** |                    |             |
| Apply normal text style       | `Ctrl` `Alt` `0`   | `⌘` `⌥ `0   |
| Apply heading style 1         | `Ctrl` `Alt` `1`   | `⌘` `⌥` `1` |
| Apply heading style 2         | `Ctrl` `Alt` `2`   | `⌘` `⌥` `2` |
| Apply heading style 3         | `Ctrl` `Alt` `3`   | `⌘` `⌥` `3` |
| Apply heading style 4         | `Ctrl` `Alt` `4`   | `⌘` `⌥` `4` |
| Apply heading style 5         | `Ctrl` `Alt` `5`   | `⌘` `⌥` `5` |
| Apply heading style 6         | `Ctrl` `Alt` `6`   | `⌘` `⌥` `6` |

**Further Achievements in this Release for Software Development / Tools features**

- Copy Paste between Description fields now work as expected  [AD30-1357]
- Package updates for the ART-DECOR http-proxy [AD30-1375]
- AD2: CSV export of values list now also works in Chrome [AD30-285]
- A problem with Certificate lists is solved [AD30-999]
- Only installed FHIR services are now offered in Project Overview [AD30-1164]
- Fetching BBR from http:// gives no longer gives an internal error  [AD30-1319]
- RetrieveDataset now gives the desired release [AD30-1336]
- Better alignment of Logo, App Name and Version on Top Menu [AD30-1338]
- Changed symbol for HEADer in TipTap [AD30-1358]
- Explained why click on version in navigation bar resets selection [AD30-1362]
- Missing scroll function for large overlay menus is added [AD30-1381]
- Background in scrollable overlay is now scaling [AD30-1382]
- FHIR API call now matches function signature in utillib [AD30-1443]

### Concept Map

The Concept Maps Panel now shows the usual [Version Stepper](https://docs.art-decor.org/documentation/principles/#version-stepper) in the Details Area, when a Concept Mps has multiple versions [AD30-1292].

<img src="../../../img/image-20240226150114301.png" alt="image-20240226150114301" style="zoom:60%;" />

**Further Achievements in this Release for Concept Maps**

- Realtime updates for concept maps [AD30-1293]
- Terminology Identifiers: Concept maps [AD30-1327]

### Governance Groups

- Routes in Governance Group Panel incorrect [AD30-1314]

### Server Admin Panel

- Perform update in ART-DECOR API updates other user [AD30-173]
- Inconsistent required minimum password length (8 vs 10 characters) [AD30-1360]

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset and Questionnaire editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org) team. Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl). Thanks to Gunter Haroske working for the [Deutsche Gesellschaft für Pathologie / German Association of Pathology](https://www.pathologie-dgp.de) pointing to shortcomings and missing features in our Questionnaire functions and to the Dutch [Uitkomstgerichte Zorg](https://www.uitkomstgerichtezorg.nl) Program for contributions in the same area. Special thanks also to the [mio42 GmbH](https://mio42.de) team for feature suggestions and bug fix requests.

Please don't hesitate to write improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.3 soon, planned to be published in late April 2024. As you can see: more on that to come. Thank you for your patience. Stay tuned.
