---
permalink: /documentation/releasenotes/release38
---
# ART-DECOR® – Release 3.8

Release notes 20 October 2023

## Introduction

We are happy to announce **Release 3.8**. Originally, we wanted to publish a sub-release, e. g. 3.7.1 or so, but the number of additions, new features and improvements, documented in 39 tasks and around 25 bugs that were fixed, made us comfortable to name the new step a major release. During development more than 64 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8 focuses on the following major topics:

- Additions to the **Questionnaires** Panel [→](#questionnaires) which features viewing, maintaining and rendering Questionnaires derived from Project Scenarios or created from scratch
- Enhancements to the **Value Set Expansion Mechanism** [→](#value-set-expansion-mechanism), e.g. feature to persist an Expansion Set
- Some new features have been added along with big fixes to the **Templates** Panel [→](#templates)
- Additions to the **Implementation Guide** [→](#implementation-guides) Panel, e.g. options to embed images in the text and also to reference artefacts from the project
- Additions to the **Concept Map** [→](#concept-maps) Panel (which features in phase 1 Coded Concept Mapping between codes of two Value Sets) regarding Publications, Project Index and Usage display
- Several enhancements for better **User Experience** [→](#user-experience), ease creation and maintenance of artefacts

and a longer list of further enhancements and fixes [→](#further-enhancements-and-fixes).

## Major Topics

### Questionnaires

**Questionnaires** in ART-DECOR are versionable Objects that define forms with groups and enterable item fields that can subsequently be rendered as forms and populated with data.

In Release 3.8 we added new features to the Questionnaire management

- proper display Questionnaire Responses as child items in the Navigation Card [AD30-1250, AD30-1251]
- DOWNLOAD option for FHIR export and RENDER option for using LHC Forms to display Questionnaires or Questionnaire Response [AD30-1251]
- a better workflow integration of Questionnaires [AD30-1083, AD30-1125]
- option to add conditional statements ("enable when") in both the original Scenario and the Questionnaire [AD30-565, AD30-1101, AD30-1124, AD30-1228]
- consider Questionnaires as implementable artefacts (data formats) and move the menu item from the Scenario menu to the Rules menu [AD30-1208]
- enhancements in the export of ART-DECOR Questionnaires as FHIR Questionnaires [AD30-1157, AD30-1158, AD30-1163]
- enhancements in the rendition of Questionnaires with a rendering engine [AD30-604, AD30-73]

We documented what ART-DECOR supports with regards to **FHIR Questionnaires** in our [Implementation Guide](https://ig.art-decor.pub/art-decor-questionnaires/).

**Questionnaires Panel**

All Questionnaires of a Project are shown as usual in a Navigation Card to the left of the Questionnaires Panel (from Release 3.8 on, under menu item *Rules*). All Questionnaires are listed alphabetically. Associated Questionnaire Responses are shown as child items of the Questionnaire.

<img src="../../../img/image-20231023145510502.png" alt="image-20231023145510502" style="zoom:80%;" />

Please note that a Responses to a Questionnaire are intended to be prototyping-only in ART-DECOR. Questionnaire Responses are shown in the Navigation Card as child items of the underlying Questionnaire.

<img src="../../../img/image-20231023145435071.png" alt="image-20231023145435071" style="zoom:90%;" /> 

**Questionnaires Details Card**

The Details Card on the right shows the usual metadata for versionabel objects and a table reflecting the hierarchical list of the Items of the Questionnaire Definition. The list can be expanded or collapsed. 

<img src="../../../img/image-20230210181930612.png" alt="image-20230210181930612" style="zoom:67%;" />

Groups are shown with a folder icon, items are arranged with an icon indicating their Questionnaire item type.

In the Details Card there are two green buttons DOWNLOAD and RENDER available, as long as there is a FHIR server configured for the project.

<img src="../../../img/image-20231023145646370.png" alt="image-20231023145646370" style="zoom:80%;" />

With DOWNLOAD you get a dialog to specify the FHIR Version configured for the project (e.g. "r4") and the output wire format (XML or JSON). Once specified the Questionnaire is converted to a FHIR Questionnaire export using the parameters.

<img src="../../../img/image-20231023145802475.png" alt="image-20231023145802475" style="zoom:67%;" />

Clicking on RENDER renders the Questionnaire in LHC Forms (see [here](https://lhvforms.nlm.nih.gov) for more details).

<img src="../../../img/image-20231023150134321.png" alt="image-20231023150134321" style="zoom:80%;" />

Please note that clicking on RENDER on a Questionnaire Response typically renders the original Questionnaire and populates all given answers of the Response into the Questionnaire fields.

There is now the option to add **conditional statements**, a so-called "enable when" behavior, that allows to define a condition under which a Questionnaire item or group is shown or not. 

Structured conditional statement for a **conditional element** have three components

- a response item / field in the questionnaire that can carry a response (value), e.g. a quantity or a yes/no field 
- an operator, e. g. *equal*, *not equal*, *greater than*, etc.
- a typed comparison value such as yes, or 80 kg etc., which is of the same datatype as the response item.

The creation of a structured conditional statement for, let's say the *Weight gain data* group in the questionnaire shown below, starts with the selection of the response item / field in the set. The idea is to only show this group and its child items when the question *Weight gain* is answered with "Yes" (true).

<img src="../../../img/image-20231007114004641.png" alt="image-20231007114004641" style="zoom:33%;" />

Thus assume that we choose *Weight gain* in this example which yields a boolean ("yes", "no") as the response item / field in the questionnaire.

<img src="../../../img/image-20231007114032196.png" alt="image-20231007114032196" style="zoom:50%;" />

Then the Conditions table shows your selection, yet not further specified regarding the operator or the comparison value.

<img src="../../../img/image-20231007115002652.png" alt="image-20231007115002652" style="zoom:40%;" />

You can then add an operator...

<img src="../../../img/image-20231007114353748.png" alt="image-20231007114353748" style="zoom:33%;" />

... and a comparison value, offered in a list of possible types such as *Boolean*, *Date*, *Date and Time*, *String*, *Coding* etc.

<img src="../../../img/image-20231007114121651.png" alt="image-20231007114121651" style="zoom:33%;" />

Finally the Condition is created with all three parts. A rendition of such an "enable when" construct means that this group is shown only, when the answer to the Weight gained question is "Yes" (true).

<img src="../../../img/image-20231007114139168.png" alt="image-20231007114139168" style="zoom:40%;" />

**Workflow integration of Questionnaires**

A Template is an implementable artefact (data formats). We have proven features in that area that allow associations of a **Scenario** with a **Representing Template**, i. e. the Template definition that represents the functional content defined in the Scenario. Also we offer the ability to associate specific Tempates and Template Elements with a distinct functional content element adding **Template Associations**.

We consider Questionnaires as implementable artefacts (data formats) and similar to the Template features we added options to to the same links and ties between a Scenario and a Questionnaire. There is now an option to specify an additional **Representing Questionnaire** for a Scenario. The associations of a Questionnaire item / fields or group with the Scenario is automagically added, as a Questionnaire is typically derived from the Scenario.

<img src="../../../img/image-20231007121413758.png" alt="image-20231007121413758" style="zoom:67%;" />

In the example above, a **Scenario / Transaction** *Summary Vital Signs*, based on a Source Dataset, has a **Representing Template**, in this case a CDA Template definition, and a **Representing Questionnaire** *Summary Vital Signs*. 

::: note

Please note that this option is essential to prevent a somewhat uncontrolled growth of Questionnaires that have no associations with an underlying Scenario/Dataset, thus a functional (logical) model.

We see the dataset as an important **semantic anchor** for Implementable Specifications. Scenarios and derived Questionnaires have ties back into the dataset as the technology-independent functional (logical) model, carrying all sematic annotations and associations created to the dataset elements.

<img src="../../../img/image-20231007121751882.png" alt="image-20231007121751882" style="zoom:40%;" />

Also other Implementable Specifications such as Templates or Profiles (FHIR) can have associations with the originating dataset elements.

<img src="../../../img/image-20231007121824095.png" alt="image-20231007121824095" style="zoom:40%;" />

In this triangle, semantic context is provided at any point in time.

<img src="../../../img/image-20231007121837451.png" alt="image-20231007121837451" style="zoom:40%;" />

:::

**Further Achievements for Questionnaires**

- New scenarios are no longer created following the Questionnaires section in the underlying DECOR file [AD30-1139]
- Wrong behavior was fixed when deleting a representing questionnaire [AD30-1225]

### Value Set Expansion Mechanism

In Release 3.8 we added new features to the Value Set Expansion mechanism

- Added an option for adding post-coordination codes to valuesets [AD30-897]
- Added an option for adding new SCT codes to valuesets [AD30-898]
- Value Set Expansion Mechanism was extended to persist an Expansion Set [AD30-1056]
- Expansion Set Store button and Value Set Panel Navigation Tree enhancements [AD30-1057]
- New Complete Code System Filters were added [AD30-1123]

Furthermore there is an announcement of our new SNO-POEM mentioned below.

**Postcoordination Expression**

It is now possible to add post-coordinated expressions using SNOMED. When adding or editing an item in a value set, there is also the tab "Postcoordination" that gives a dialog with options to enter Level and Type, a Root / focus concept and a set of Attribute-Value codes drawn from SNOMED. In the Root / focus concept and Attribute-Value code fields you can enter search terms that are looked up in the SNOMED code system (given one is installed).

<img src="../../../img/image-20230731114816415.6a68c6f7.png" alt="image-20230731114816415" style="zoom:67%;" />

The post-coordinated expression is also checked against SNOMED.

<img src="../../../img/image-20230731115252551.486cccd5.png" alt="image-20230731115252551" style="zoom:67%;" />

The item appears in the value set as something like in this example.

<img src="../../../img/image-20230731115843142.728d7975.png" alt="image-20230731115843142" style="zoom:67%;" />

**Announcement: SNO-POEM**

We are working hard on a SNOMED post-coordinated expressions editor based on SNOMED's [Expression Template Language](https://confluence.ihtsdotools.org/display/DOCSTS/6.1.+Expression+Template+Language) (ETL). The methodology is to derive Questionnaires from official (and self-crafted) ETL expressions and render them as a form that is subsequently populated by a user. The SNOMED CT expression template is converted (once) for that purpose to a Questionnaire that is stored in a Building Block Repository and offered to the user through the post-coordination editor (POCE). 

The resulting Questionnaire is part of our new **SNO-POEM** concept. In future Releases we will publish SNO-POEM as a proper  alternative to hand-crafted SNOMED post-coordinated expressions.

**Persisting Value Set Expansion Sets**

Intensional Value Sets offer an EXPANSION SET button that primarily displays the resulting Expansion Set in two steps.

<img src="../../../img/image-20231023153121678.png" alt="image-20231023153121678" style="zoom:67%;" />

The Value Set Expansion Set dialog appears and first calculates the number of members (codes) in the Expansion Set, without displaying the distinct codes. 

<img src="../../../img/image-20231023153350938.png" alt="image-20231023153350938" style="zoom:67%;" />

The user can click on the EXPAND + button to see all codes in the set. The list of member codes is displayed in the usual way. 

<img src="../../../img/image-20231023153412190.png" alt="image-20231023153412190" style="zoom:67%;" />

Next to the CANCEL option for this dialog, a SAVE button is also offered to save the Value Set Expansion Set. Once saved (signaled by a snackbar alert), the persisted Expansion Set can be seen with its ID under the *Usage* tab of the corresponding original Value Set. 

<img src="../../../img/image-20231023153609206.png" alt="image-20231023153609206" style="zoom:67%;" />

::: note Technical Note

The Expansion Set could be downloaded through the API, e.g. by calling `/valuesetexpansion/{id}`. 

:::

**Complete Code System Filters**

The list of our Complete Code System Filter Operators is now including `equal` and `exist`.

| Filter-<br />Operator | Description                                                  |
| --------------------- | ------------------------------------------------------------ |
| `equal`               | the specified property of the code is equal to the provided value |
| `exist`               | the specified property of the code does exist/does not exist (provided value must be true or false), implemented so far for the default intrinsic standard properties `child` and `parent` of a code |

Additional Complete Code System Filter operators such as `is-a`, `descendent-of`, `is-not-a`, `regex` and `in` will be added in future Releases.

**Further Achievements for Expansion Sets in Value Sets**

- Documentation for the Terminology Report was completed [AD30-1109]
- DECOR Schema additions for Expansion Sets in Value Sets [AD30-1055]
- An option was added to delete one or more associations [AD30-1090]
- Value Set flexibility is now taken into account when selecting a code system for context [AD30-1108]
- A Server error upon valueset expansion was fixed [AD30-1224]

### Templates

In Release 3.8 we added new features to our Templates maintenance system.

- An option was added to be able to add multiple fixed values for attributes [AD30-1097]
- Default sort and adding version label to the *Usage* tab [AD30-1103]
- Allow sorting template tables by ID [AD30-1120]

**Further Achievements for Templates**

- In Template mapping the Template version was not shown, this has been fixed [AD30-953]
- Some Code system editor bugs has been fixed [AD30-986]
- Choice items in template editor are now displayed completely [AD30-1000]
- Multiple constraints are displayed properly now [AD30-1113]
- A Problem with choices in Templates and multiplicity display was fixed [AD30-1127]
- Umlauts now get properly displayed when validating instances containing Umlauts (thanks to ELGA team for reporting it) [AD30-1154]
- A function threw an error when evaluation missing artefacts [AD30-1223]
- A warning is now thrown when CDA schema is not available for validation [AD30-1229]

### Implementation Guides

In Release 3.8 we added new features to the Implementation Guide Feature. When editing an Implementation Guide page it is now allowed adding and changing resources (such as Value Sets and Templates) as link or object [AD30-880] and the TipTap feature was enhanced to allow images being uploaded and embedded [AD30-848]. 

![image-20231023155816207](../../../img/image-20231023155816207.png)

The UI got two extra buttons in the Tip Tap menu when editing a page for adding images (left button) and artefacts (right button).

![image-20231023155952408](../../../img/image-20231023155952408.png)

### Concept Maps

**Concept Maps** in ART-DECOR are versionable Objects that are a documentation of the a match-making process of Coded Concepts from two Value Sets [AD30-831]

The **Concept Map Panel** is reachable through the Terminology menu *Concept Maps*.

<img src="../../../img/image-20230620094213986.png" alt="image-20230620094213986" style="zoom:35%;" />

In Release 3.8 we added new features to our Concept Maps maintenance

- Include Concept Map association for value sets in HTML publication [AD30-1023]
- Include Concept Maps to Compilations/Publications [AD30-1024]
- The usage of a Concept Map is now added to the corresponding Code Systems in the *Usage* tab [AD30-1050]
- It is now allowed adding a comment to a Concept Map [AD30-1135]
- Concept Maps are now also part of the Project Index [AD30-1142]

### User Experience

From Release 3.7 on we started introducing Fullscreen buttons in several Panels to switch from overview to a focused view, e.g. when editing artefacts. In Release 3.8 we added a Dialog Fullscreen Expand function to the Scenario/Transactions Editor [AD30-1067] and to the Template Editor [AD30-1141].

Switching between Fullscreen (see right-most icon) ...

<img src="../../../img/image-20231023163059635.png" alt="image-20231023163059635" style="zoom:80%;" />

... and exiting Fullscreen (see below) is offered in the Top Menu row of the Editors.

<img src="../../../img/image-20231023163144692.png" alt="image-20231023163144692" style="zoom:80%;" />

## Further Enhancements and Fixes

The following sections focus on the various artefacts and show new functionalities, enhancements and bugfixes.

::: note

The numbers shown are the ART-DECOR Expert Group (ADEG) ticket identifiers.

::: 

**FHIR Export Options**

- The defaults with FHIR version/format dialog has been changed [AD30-1160]
- Render Button uses the "default" FHIR server endpoint for transformation now [AD30-1161]

**Datasets**

- It is now possible to (re-)edit a comment field in dataset concepts. This is the only location where a comment field can have multiple items per language (normally it's just one comment per language). The UI now supports adding multiple comments per language and renders inherited comments also. A PATCH payload for comments (remove old comments, add new comments) was also added [AD30-1199]
- The undesired filter on Quantity properties entry field has been fixed [AD30-1207]
- Support for viewing the coding strength for valueset associations in the dataset form is now added [AD30-1181]

**Scenarios/Transaction**

A few enhancements for better creation / maintenance have been added [AD30-1174]

- **No Actors, no action**
  If no actors are defined you can not add a new scenario. Instead of this you will get and owl dialog that you have to create an actor first.
- **Initial ADD problem**
  After creating a scenario the treeview was going crazy because of non-valid uuid. To fix this we are using the scenario id instead.
- **EDIT enhancement**
  If a scenario has no source dataset the edit button for representing templates triggers an owl dialog "please add dataset first".
- **Render Button enhancement**
  The eye icon on representing templates only shows up if representing templates are available.
- **Empty dataset after initial add change**
  Fix reload issue after adding source dataset and open representing template edit mode after.
- **Group not selected with child items**
  Improved handling of parent items on representing templates editor if no selection is defined.
- **Parent group always with children**
  Added new options for select only the group with "0..1 R" or "1..1 R".

**Documentation**

- Release note links are now checked in backend packages [AD30-1129]

**Projects**

- After adding a project-author you now can see what you've added without reloading the project [AD30-317]
- Confirmation on changing project settings & improve i-button layout has been added [AD30-1168]
- Show hint to user on how to unlock when he found a locked object (thanks to the BIH @ Charité team for asking for this feature) [AD30-1197]

**Server Admin Functions**

- Allow to sort Governance Group tables by ID [AD30-1096]
- A new button was added in the New User dialog to shortcut set default groups [AD30-1149]
- The Governance Groups Panel can now be sorted by ID [AD30-1156]
- URLs were fixed in the Admin project panel, pointing now to the correct overview URL for projects [AD30-1185]
- A Problem in project with "more than one @code attribute" was fixed [AD30-1212]

**User Experience, general User Interface, Software Tools**

- Context is now provided when switching Panels [AD30-889]
- `desc` elements that have no `@language` attributes are now handles correctly [AD30-1073]
- Copy to clipboard of full artefact ID is now offered [AD30-1088]
- In ART-DECOR Release 2, copy/paste of absolute links inside text are made relative [AD30-1134]
- Version Label change no longer leads to an error [AD30-1136]

**Demo Project fixes**

- Examples demo9- contained illegal code with whitespace which has been fixed [AD30-1243]
- Various issues in demo11- have been fixed [AD30-1244]

**System Architecture**

- Triangular import in apps/API was removed [AD30-276]
- Larger improvements on codeSystem and valueSet list performance [AD30-1162]
- Serialize node seems not to work in eXist db 6.1.0 which has been fixed [AD30-1027]

::: note A BIG thank you!

We are always thankful for any comments and suggestions. Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org) team. Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl). 

Please don't hesitate to write improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Incoming features with next ART-DECOR® Releases

ART-DECOR Release 3 is focuses on new features and functionalities according to the roadmap. The following main areas are under current development and are seen as incoming functionality in the following Releases:

**Terminology**

- Additional Complete Code System Filter operators such as `is-a`, `descendent-of`, `is-not-a`, `regex` and `in` will be added in future Releases.
- We are working on SNO-POEM (SNOMED Post-coordination Editor and Management) to offer an enhanced possibility for adding post-coordinated expressions. Collaborations are ongoing regarding this project with [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl), [Integraal Kankercentrum Nederland (IKNL)](https://iknl.nl) and about to be established with the [Institute of Medical Informatics, University of Lübeck](https://www.imi.uni-luebeck.de/en/institute.html).

**Implementation Guide**

- The Implementation Guide Feature will offer triggering a publication with rendition in HTML. It will allow the submission of an Implementation Guide with text and resources to our extended publication process and get rendered HTML publications at specified publication locations. In addition, the [FHIR IG Publisher](https://confluence.hl7.org/display/FHIR/IG+Publisher+Documentation) will be offered as one of the rendering machinery, e.g. for CDA- or FHIR-based Implementation Guides. 

**Questionnaires**

- The existing **Questionnaire Editor** for the Items will get options for editing answer lists. Visualization and creation/maintenance of Questionnaires and Responses will further be developed.

**FHIR Profiles and Profiling**

- Dealing with **FHIR Profiles** starts with Release 3.9 in phases. First phase feature is already in testing phase offering visualization of **Profiles in a Project**. The second feature is under development and will allow to add **Profile Associations**, similar to Template Associations. Finally **FHIR Profiling** will be another added feature utilizing the principles of the well-known CDA Template Editor.

**System Architecture**

- **GIT integration** as a natural extension of the ART-DECOR **Building Block Repository** concept will be subject of one of the next releases, collaborations have been established to quickly bring this topic to first functionality.

## Outlook

Current development is heading towards a next Release soon, planned to be published in December 2023, where the aforementioned new features will be addressed along with enhancements and bug fixes on Release 3.9. 

As you can see: more on that to come. Thank you for your patience. Stay tuned.
