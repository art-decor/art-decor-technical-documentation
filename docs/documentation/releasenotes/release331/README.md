---
permalink: /documentation/releasenotes/release331
---
# ART-DECOR® – Release 3.3.1

Release notes 21 February 2022

## Introduction

The ART-DECOR® tool suite is migrated from Release 2 to the new Release 3 family. This development started in 2020 and was successfully continued in 2021 and beyond. 

Recently, Release 3.3.1 was published that brings along many new features and improvements. This is the Release Note with more details.

## General Remarks

We made good progress on the migration, taking many opportunities for functional improvements. The major remaining topics in our "waiting room" are FHIR Profiles and Questionnaires as well as renewed and enhanced options for Implementation Guides and publications. The conceptual work has been done, and we are confident that Release 3.4 and 3.5 these areas will successfully be added to the tool suite.

For beginners we started using our new duplex server *newbie.art-decor.org* that hosts one database and both ART-DECOR Releases 2 and 3. This duplex production environment is set up for serving beginners, students and participants of workshops and for training.

| ![image-20220221103822453](../../../img/image-20220221103822453.png) |
| ------------------------------------------------------------ |



Furthermore, our new Central Terminology Package Server at *terminology.art-decor.org* is set-up and we started  testing. Our plans include to go live by the end of March 2022.

| ![image-20220221103848717](../../../img/image-20220221103848717.png) |
| ------------------------------------------------------------ |

## New functionalities and fixes in Release 3.3.1 at a glance

The following sections focus on the various artefacts and shows the new functionalities (compared to ART-DECOR Release 3.3, compared to ART-DECOR Release 2 shown **in bold**).

::: note

The numbers shown are the ART-DECOR Expert Group ticket identifiers.

:::

**Scenarios and Transactions**

- Scenarios, Transactions, including Representing Templates and Scenario Actors are editable and can now be cloned on Scenarios, Transactions Group or Transactions Leaf level [AD30-34, AD30-387].
- The metadata fields condition, trigger for Scenarios, condition for Transactions Groups, and trigger for Transactions Leaves can now be maintained [AD30-453].

**Terminology**

- Terminology Associations have been improved, all types can now be maintained with the exception of Choice List – Concept Association Types [AD30-224, AD30-233].

**Rules**

- Viewing of the Template Design Body has been improved [AD30-503].

**General improvements**

- Most versionable artefacts (e. g. dataset, scenarios, transactions) now have a last modified date indication `lastModifiedDate` [AD30-466].
- More explicit handling of artefact descriptions [AD30-477].

## Incoming features on further Releases

ART-DECOR Release 3 can be seen as a horizontal move of all ART-DECOR Release 2 features, plus adding new features immediately if appropriate or later as planned. Compared to Release 2, ART-DECOR Release 3 still must migrate some of the functionality.

The following main areas (main menu items) are still under development and therefore incoming functionality in the following Releases:

Terminology (read-only in 3.3.1)

- Edit and Maintain Code Systems
- Edit and Maintain Terminology Mappings type 3: Choice List – Concept Association

Rules / Templates (read-only in 3.3.1)

- This area covers view, add, and manage constraint rules, here: templates, especially for HL7 v3 and document specifications based on the Clinical Document Architecture CDA.
- Editing a Template Design Body is almost complete and is planned to be published with Release 3.4. The new Editing functions offer three manners of editing a Template Design Body, introducing a Template **Structure Context** Menu  [AD30-494], a Template **Property Context** Menu  [AD30-493], in-column edits  [AD30-495], an **Undo-Function** for the Template Design  [AD30-510].

- The Template API is about to be finalized for Template Associations to allow next to list also create, update and delete [AD30-545].
- The Template API introduced update of Templates [AD30-479], Data types allows in the context of Templates are returned [AD30-562].

Questionnaires

- Questionnaire (QQ) and Questionnaire Response (QR) as defined in the FHIR standard plus an ART-DECOR wrapper to allow consistent handling like all versional artefacts will be introduced from Release 3.4 on.
- Questionnaires Enhanced (QE) will also be introduced, starting with Release 3.4: once Questionnaires has been edited/enhanced by the user with for instance: layout instructions, conditions, it migrates its status to an Questionnaires Enhanced (QE) in order to be distinguished from genuinely generated Questionnaires in ART-DECOR.

Periodic and Scheduled Tasks Managament

- New scheduler mechanics are in place that allows to do background periodic or scheduled tasks.

We also zoom in on the areas in detail which are ready to be used to provide you with more details about the area.

## Resolutions details for Scenarios, Rules and Periodic / Scheduled Tasks Managament

### Adding and cloning scenarios and transactions

With Release 3.3.1 adding a new scenario and cloning of a scenario or transaction is now available.

![image-20220126154429092](../../../img/image-20220126154429092.png)

Next to the search field in the Navigation Tree Card there is a mini menu that offers options to ADD or CLONE a scenario. When adding a dialog asks for all necesssary and optional data for the new Scenario/Transaction.

<img src="../../../img/image-20220126154605204.png" alt="image-20220126154605204" style="zoom:67%;" />

New Transactions can be created before or after a selected existing Transaction.

![image-20220126154413252](../../../img/image-20220126154413252.png)

### Periodic and Scheduled Tasks Managament

ART-DECOR uses the eXist database functionalities for scheduling tasks. We have two classes to scheduling tasks:

- periodical jobs, i. e. tasks that repeatedly are executed with a period of time in between the executions 
- scheduled jobs that are executed upon request, once per request.

With Release 3.3.1 we use the following types of scheduling tasks

- Project related requests, requests such as compilation
- DECOR cache refresh
- Periodic notifications, to scan/process for periodic notifications,  e.g. notifications on changed issues and release
- Scheduled notifications, to scan/process for scheduled notifications, e.g. (new) users to be notified about the username, password (reset) and projects
- Scheduled backups

## Outlook on what comes next: ART-DECOR® – Release 3.4

The current development is heading towards a soon next Release, where the following topics are on:

- Option to view Value Set Expansions when the Value Set contains intensional instructions such as include or exclude operations
- For Templates in the Rules section, In-column, Structural and Property Edits are available (Releases 3.4) and first FHIR profiling (Releases 3.5 and 3.6) is offered.
- The database eXist version 6 has recently been released and is under investigation by the team for being the base for future ART-DECOR Releases.
- A note on terminology: we are re-organizing ART-DECOR in how terminology works internally. The API reflects the progress in this area. The sections codeSystem and valueSet support DECOR representations, while the section terminology represents what has been dubbed **Centralized ART-DECOR Terminology Services** or **CADTS**. CADTS currently powers the Browser frontend.

As you can see: more on that to come. Thank you for your patience. Stay tuned.



