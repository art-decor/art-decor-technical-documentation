---
permalink: /documentation/releasenotes/
---
# ART-DECOR® – Releases

The ART-DECOR® tool suite is migrated from the current Release 2 to the new Release 3 family. This development started in 2020 and was successfully continued in subsequent years. 

The migration is complete since Quarter 1 of 2023, new features are developed in parallel to satisfy a growing community that creates and maintaines interoperability specifications.

This is the comprehensive list of Release Notes (newest first) of the ART-DECOR® Release 3 family.

## Release 3.8.8

Release 3.8.8 has been published in January 2025. Please find the Customer [Release Notes here](release388/).

## Release 3.8.7

Release 3.8.7 has been published in December 2024. Please find the Customer [Release Notes here](release387/).

## Release 3.8.6

Release 3.8.6 has been published in November 2024. Please find the Customer [Release Notes here](release386/).

## Release 3.8.5

Release 3.8.5 has been published in September 2024. Please find the Customer [Release Notes here](release385/).

## Release 3.8.4

Release 3.8.4 has been published in July 2024. Please find the Customer [Release Notes here](release384/).

## Release 3.8.3

Release 3.8.3 has been published in June 2024. Please find the Customer [Release Notes here](release383/).

## Release 3.8.2

Release 3.8.2 has been published in February 2024. Please find the Customer [Release Notes here](release382/).

## Release 3.8.1

Release 3.8.1 has been published in December 2023. Please find the Customer [Release Notes here](release381/).

## Release 3.8

Release 3.8 has been published in October 2023. Please find the Customer [Release Notes here](release38/).

## Release 3.7

Release 3.7 has been published in May 2023. Please find the Customer [Release Notes here](release37/).

## Release 3.6

Release 3.6 has been published in December 2022. Please find the Customer [Release Notes here](release36/).

## Release 3.5.1

Intermediate Release 3.5.1 has been published in August 2022. Please find the Customer [Release Notes here](release351/).

## Release 3.5

Release 3.5 has been published in June 2022. Please find the Customer [Release Notes here](release35/).

## Release 3.4

Release 3.4 has been published in April 2022. Please find the Customer [Release Notes here](release34/).

## Release 3.3.1

Intermediate Release 3.3.1 has been published in February 2022. Please find the Customer [Release Notes here](release331/).

## Release 3.3

Release 3.3 has been published in January 2022. Please find the Customer [Release Notes here](release33/).

## Release 3.2

Release 3.2 has been published in December 2021. Please find the Customer [Release Notes here](release32/).

## Release 3.1

Release 3.1 has been published in August 2021. Please find the Customer [Release Notes here](release31/).

## Release 3.0

Release 3.0 has been published in July 2021. Please find the Customer [Release Notes here](release30/).
