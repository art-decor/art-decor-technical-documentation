---
permalink: /documentation/releasenotes/release32
---
# ART-DECOR® – Release 3.2

Release notes 17 December 2021

## Introduction

The ART-DECOR® tool suite is migrated from the current Release 2 to the new Release 3 family. This development started in 2020 and was successfully continued in 2021. 

Recently, Release 3.2 was published that brings along many new features and improvements. This is the Release Note with more details.

## General Remarks

**Frontend: User Experience**

Release 3 introduces a new user interface with an improved user experience (UX) utilizing a new framework (Vue and Vuetify) that replaces the Orbeon® platform.

**Backend: Database**

The eXist® database (exist-db.org) remains the well-founded backend. Here, an update to the latest version has been done to improve performance and stability. A Database upgrade to version 5.3.x has been done, with major improvements on stability, performance and caching. The log4j problem has also addressed recently.

## New functionalities in Release 3.2 at a glance

**Landing Page (Home)**

A new Landing Page is presented when approaching the tool. It allows to select a project; also, various information is presented.

**Project Overview and Panels**

The Project Overview got a new Landing Page, and new Panels are introduced for Datasets, Scenarios, Terminologies, and Templates with the Navigation Tree on the left and details shown on the right. In addition, the new rapid Terminology Browser has been fully integrated.

The following sections focus on the various artefacts and shows the new functionalities (compared to ART-DECOR Release 3.1, compared to ART-DECOR Release 2 shown **in bold**)

**Project**

-  add metadata (copyright) in dedicated field
- manage Repositories
- manage Project Services
- manage name spaces
- manage Project Languages
- edit Publication Locations
- manage Project Identifiers
- manage Governance Groups the Project is associated with
- use Project Development panel with options to check project, compile development environments and manage artefacts
- view the Project Index, an overview of all artefacts of the project **in a new layout**.

**Datasets**

- **add new or clone Dataset in project**
- manage Inheritance and Containment of Dataset Concepts.

**Scenarios and Transactions**

- view Scenarios and Transactions, including Representing Templates, corresponding Actors and the compilation of Concepts in Transaction Leaves.

**Terminology**

- view Value Sets, navigation and details
- view Code Systems, navigation and details
- view Terminology Identifiers List
- use **Terminology Browser with quick cross code system browsing**.

**Rules (formerly Templates)**

- add and manage constraint rules, here: templates especially for HL7 v3 and document specifications based on the Clinical Document Architecture CDA, navigation, grouped by type, and details including almost all items of the Template Design Body
- Note: the name change of this area has been performed to address FHIR Profiles in one of the next Releases.

**General enhancements**

- In certain situations, our **wise owl** may appear and give you hints or instructions on how to continue or what to do. The example below is the owl telling you to select a project first, before you can browse it.
  
  <img src="../../../img/owl.png" alt="owl" style="zoom:55%;" />
  
- There are **Breadcrumbs** in most Panels visible at the top to make orientation easier and to allow deep links, copyable to your clipboard using the Paper Clip icon.
  
  <img src="../../../img/clip_image003.png" alt="img" style="zoom:40%;" />

## Incoming features on further Releases

ART-DECOR Release 3 can be seen as a horizontal move of all ART-DECOR Release 2 features, plus adding new features immediately if appropriate or later as planned. Compared to Release 2, ART-DECOR Release 3 still has to migrate some of the functionality.

The following main areas (main menu items) are still under development and therefore incoming functionality in the following Releases:

Scenarios and Transactions (read-only in 3.2)

-  the Representing Template documents, which Dataset Concepts are linked to a Transaction and enriches it with additional properties

Terminology (read-only in 3.2)

- Value Sets
- Code Systems
- Mappings

Rules

- Templates (read-only in 3.2)

The other areas are available in Release 3.2. Specific missing functionality for these areas are listed on the following pages. 

Note: the numbers shown are the ART-DECOR Expert Group ticket identifiers.

We also zoom in on the areas in detail which are ready to be used to provide you with more details about the area.

## Resolutions details for Project, Datasets, Scenarios, Terminology, Rules, Issues

### Landing page (Home)

New landing page where you can choose a project to inspect or work with. 

![img](../../../img/clip_image004.jpg)

Once a project is selected, the menu on the left let you choose specific Project areas: *Project*, *Datasets*, *Scenarios*, *Terminology*, *Rules*, *Issues*.

![clip_image003](../../../img/clip_image003.jpg)

### Project

The Project Panel is ready to be used. The areas in Project are *Overview, Authors, Publications, History, Identifiers, DECOR Locks, MyCommunity, Governance Groups, ADA, Development* and *Project Index*.

### Datasets

Datasets is ready for use. To see the Datasets, click on the submenu *Datasets*. The Datasets for that Project will be shown in the Navigation Tree View.

<img src="../../../img/clip_image005.png" alt="img" style="zoom:40%;" />

When you select a Dataset in the Navigation Tree view you can see the details on the right.

![img](../../../img/clip_image006.png)

Please note, that there are Breadcrumbs on top of the Panels to make orientation easier.

Inherited Concepts can be “enlarged” by clicking on the green pill to see details of the original Concept, lazy loaded, ticket [AD30-457].

<img src="../../../img/clip_image007.png" alt="Ein Bild, das Text enthält.  Automatisch generierte Beschreibung" style="zoom:50%;" />

### Scenarios, Transactions and Questionnaires

The areas in Scenarios are *Scenarios* with Transactions, *Actors* and *Questionnaires*.

The Panel offers all Scenarios and nested Transaction Groups and Transaction Leaves in the Navigation Tree. The right detail area varies depending on the items chosen in the navigation. Transaction groups typically show Sequence Diagrams (in new style), the Transaction Leaves show their Representing Template along with details of the Transaction Concepts such as a view on the corresponding Dataset Concept, Cardinalities, Conditions etc.

![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image008.png)

Questionnaires are already present as a draft preview Navigation Tree mock-up, functional in Release 3.4 first.

<img src="../../../img/clip_image009.png" alt="Ein Bild, das Text enthält.  Automatisch generierte Beschreibung" style="zoom:50%;" />

Note: Questionnaires are viewable/downloadable from the Project Index.

Incoming functionality for scenarios:

- Adding a new scenario and cloning of a scenario is not available; this will be extended in the next release, ticket [AD30-387]

- Scenario, transactions and representing template are read-only in Release 3.2; the Representing Template documents, which Dataset Concepts are linked to a Transaction Leaf and will be implemented in the next release, ticket [AD30-453]
- Transforming Scenarios into Questionnaires, being visible in the Questionnaire Panel; Edit Questionnaires, Questionnaires Prototype Cycle including Questionnaires Response.

### Terminology

The areas in Terminology are *Value Sets*, *Identifiers*, *Mappings*, *Code Systems* and *Browser*.

The Terminology Panel is not yet fully ready for use, but most parts are available read-only. We will zoom in on the specific areas to inform you about the missing functionality.

#### Value Sets

The Panel offers the list of all Value Sets of the Project, sorted alphabetically and indicating whether they are in-project Value Sets (plain colored icons) or references from a Building Block Repository BBR (chain symbol). Once a value set is chosen on the left, the right Card shows the details of the Value Set including the Concept List.

![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image010.png)

Incoming functionality for Value Sets:

- Add and Edit Value Sets is currently disabled, only viewing is available. This will be extended in the next release, ticket [AD30-393].

#### Terminology Identifiers List

A sortable list of all terminology identifiers of the project is available.

![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image011.png)

#### Terminology Mappings

The Mappings Panel allows first creations of terminology mappings.

| ![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image012.jpg) |
| ------------------------------------------------------------ |

Incoming functionality for Terminology, Mappings:

- Add and Edit Mapping is currently disabled, only viewing is available. This will be extended in the next release, ticket [AD30-233].

#### Code Systems

The list of Code Systems used in the project is viewable through the Code System Panel.

![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image013.png)

#### Terminology Browser

The Terminology Browser with quick cross Code System browsing is available.

![img](../../../img/clip_image014.png)

The Browser will be also used for adding and editing Value Sets in the next release (3.4).

![img](../../../img/clip_image015.png)

### Rules

The areas in Rules are *Templates*, *Profiles*, *Associations* and *Identifiers*.

#### Templates

The Panel offers the list of all Templates of the Project, grouped by their type as usual. The Templates are group-wise sorted alphabetically and indicate whether they are in- project templates (plain colored icons) or references from a Building Block Repository BBR (chain symbol). Once a Template is chosen on the left, the right card shows the details of the templates including the Template Design Body.

![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image016.png)

Incoming functionality for Rules:

- Template Panel (in Rules) is read-only for now. This will be extended in the next release, ticket [AD30-36]
- Template Design Body is yet only almost feature complete regarding viewing; next release will show all properties properly [AD30-61]
- FHIR Profile Panel is in development [AD30-425], an editor is planned for a later release [AD30-429]
- Association is in development [AD30-434]
- Identifiers is in development [AD30-472], functionality is already present in the Project Index, tabs *Templates and Profiles*

### Issues

The areas in issues are *Issues* and *Labels.*

#### Issues Panel

Issues are listed (paginated) and can be filtered by various switches and filters.

![img](../../../img/clip_image017.png)

#### Issue Labels

Issue Labels can be added and maintained including assigning colors and descriptions to the labels. Labels can be associated with issues for optimal change management and maintenance.

![img](../../../img/clip_image018.png)

## A word on the Backend API

ART-DECOR 3 supports a powerful, modern RESTful API backend. The ART-DECOR frontend leverages this API, but scripting or third-party add-ons are also made possible.

The ART-DECOR 3 API is documented using OpenAPI 3 (see specification: https://www.openapis.org and https://swagger.io/specification/) and presented through Swagger (see also: https://swagger.io).

![Ein Bild, das Text enthält.  Automatisch generierte Beschreibung](../../../img/clip_image019.png)

Privileged actions need authentication using an access token. Any ART-DECOR server has the Swagger UI interface at the following location: [base]/exist/apps/api/. This interface allows inspection and testing of any actions within the limitations of your account where applicable.

All actions support input and output examples, textual documentation and schemas. All actions that support JSON input/output, also support XML input/output.

The API is organized in sections: Project, Scenarios, Transactions, Value Sets, Code Systems, Issues, and more.

## Outlook on what comes next: ART-DECOR® – Release 3.3

The current development is heading towards a soon Release 3.3, where the following topics are on:

- Adding and cloning of a Scenario/Transaction
- Adding and maintaining Representing Templates in Transactions
- Adding and maintaining Value Sets in Terminology
- Detailed views on Templates in the Rules section, first Structural and Property Edits (Release 3.4) and FHIR profiling (Releases 3.5 and 3.6)
- A note on terminology: we are re-organizing ART-DECOR in how terminology works internally. The API reflects the progress in this area. The sections codeSystem and valueSet support DECOR representations, while the section terminology represents what has been dubbed **Centralized ART-DECOR Terminology Services** or **CADTS**. CADTS currently powers the Browser frontend.

As you can see: more on that to come. Thank you for your patience. Stay tuned.
