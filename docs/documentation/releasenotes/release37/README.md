---
permalink: /documentation/releasenotes/release37
---
# ART-DECOR® – Release 3.7

Release notes 30 May 2023

## Introduction

We are happy to announce **Release 3.7**.  It took longer than we expected but this published version addresses many new features and improvements, documented in 54 tasks, and fixes around 30 bugs. During development more than 80 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.7 focuses on the following major topics:

- New **Questionnaires** Panel [→](#questionnaires) which features viewing, maintaining and rendering Questionnaires derived from Project Scenarios or created from Scratch
- New **Concept Map** [→](#concept-maps) Panel which features (in phase 1) Coded Concept Mapping between codes of two Value Sets
- The re-newed **Terminology Report** [→](#terminology-report), known from ART-DECOR Release 2 but now with many new features
- Expansion Sets of **Intensional Value Sets** [→](#value-set-expansion-sets-enhancements) (phase 3: complete code systems with filters)
- There are some **Enhancements** for **Terminology Browser** [→](#enhancements-for-terminology-browser)
- **License Management** [→](#license-management-for-projects) for Projects
- Additions to the **Implementation Guide** [→](#implementation-guides) Panel
- Further improvements of the **Centralized ART-DECOR Terminology Services** (CADTS) [→](#general-centralized-art-decor-terminology-services-cadts-enhancements)

and a longer list of further enhancements and fixes [→](#further-enhancements-and-fixes).

We also added some **Installation Hints** [→](#installation-hints) for foreign maintanced servers to give some advice to the server maintainer.

## Major Topics

### Questionnaires

**Questionnaires** in ART-DECOR are versionable Objects that define forms with groups and enterable item fields that can subsequently be rendered as forms and populated with data.

In Release 3.7 we introduce a new Questionnaire Panel with functionalities

- to add, edit and maintain Questionnaires [AD30-421, AD30-422, AD30-657]
- to transform Questionnaires from a Project Scenario (as a scheduled task) [AD30-624]
- export Questionnaires e. g. as FHIR Questionnaires [AD30-72]
- render Questionnaires with a rendering engine [AD30-604, AD30-73]

To document what ART-DECOR is supporting with regards to **FHIR Questionnaires**, we created an [Implementation Guide](https://ig.art-decor.pub/art-decor-questionnaires/) to define all features. Release 3.7 introduces features up to priority 2.

- A Questionnaires Panel with Navigation Card and Detail Area to show the details of a Questionnaire definition
- An Questionnaire Item Editor, in this phase
  - supports building a tree of Questionnair items 
  - supports properties such as id, type, text, cardinalities (repeats, required)
  - supporting FHIR Extensions to Questionnaires
    - [Extension: itemControl (core)](https://hl7.org/fhir/R4/extension-questionnaire-itemcontrol.html)
    - [Extension: Rendering Style (core)](http://hl7.org/fhir/R4/extension-rendering-style.html)
    - [Extension: minValue (core)](http://hl7.org/fhir/R4/extension-minvalue.html)
    - [Extension: maxValue (core)](https://hl7.org/fhir/R4/extension-maxvalue.html)
  - Note: not in priority 2 is answerList and enableWhen

**Questionnaires Panel**

All Questionnaires of a Project are shown as usual in a Navigation Card to the left of the Questionnaires Panel (under menu item *Scenarios*). All Questionnaires are listed alphabetically, associated Questionnaire Responses are shown as child items of the Questionnaire.

<img src="../../../img/image-20230210181455928.png" alt="image-20230210181455928" style="zoom:80%;" />

The Tree can be filtered, completely expanded or collapsed and the menu item offers to create a new Questionnaires (starting from scratch), or to Transform an already defined Project Transaction into a Questionnaire.

<img src="../../../img/image-20230210181412785.png" alt="image-20230210181412785" style="zoom:80%;" />

::: note

If your project has not defined any FHIR services (see Project Overview - Services) then the following dialog invites you to defined such a FHIR service for your project.

<img src="../../../img/image-20230210181425556.png" alt="image-20230210181425556" style="zoom:80%;" />

:::

**Questionnaires Details Card**

The Details Card on the right shows the usual metadata for versionabel objects and a table reflecting the hierarchical list of the Items of the Questionnaire Definition. The list can be expanded or collapsed. 

<img src="../../../img/image-20230210181930612.png" alt="image-20230210181930612" style="zoom:67%;" />

Groups are shown with a folder icon, items are arranged with an icon indicating their Questionnaire item type.

**Questionnaire Item Types**

The first level distinguished between:

| **Item Type** | **Icon**            | **Explanation**                                              |
| :------------ | :------------------ | :----------------------------------------------------------- |
| Group         | <adimg     folder/> | An item with no direct answer but should have at least one child item. |
| Display       | <adimg     label/>  | Text for display that will not capture an answer or have child items |
| Question      | multiple            | An item that defines a specific answer to be captured, and which may have child items. The answer provided in the QuestionnaireResponse should be of the defined datatype. |

A Question can express expectations of the Question Types.

| Item Type   | Icon                    | **Explanation**                                              |
| :---------- | ----------------------- | :----------------------------------------------------------- |
| Boolean     | <adimg     boolean/>    | Question with a yes/no answer                                |
| Decimal     | <adimg     decimal/>    | Question with is a real number answer                        |
| Integer     | <adimg     integer/>    | Question with an integer answer                              |
| Date        | <adimg     date/>       | Question with a date answer                                  |
| Date Time   | <adimg     datetime/>   | Question with a date and time answer                         |
| Time        | <adimg     time/>       | Question with a time (hour:minute:second) answer independent of date |
| String      | <adimg     string/>     | Question with a short (few words to short sentence) free-text entry answer |
| Text        | <adimg     text/>       | Question with a long (potentially multi-paragraph) free-text entry answer |
| Url         | <adimg     url/>        | Question with a URL (website, FTP site, etc.) answer         |
| Choice      | <adimg     choice/>     | Question with a Coding drawn from a list of possible answers (specified in either the answerOption property, or via the valueset referenced in the answerValueSet property) as an answer |
| Open Choice | <adimg     openchoice/> | Answer is a Coding drawn from a list of possible answers (as with the choice type) or a free-text entry in a string |
| Attachment  | <adimg     attachment/> | Question with binary content such as an image, PDF, etc. as an answer |
| Reference   | <adimg     reference/>  | Question with a reference to another resource (practitioner, organization, etc.) as an answer |
| Quantity    | <adimg     quantity/>   | Question with a combination of a numeric value and unit, potentially with a comparator (<, >, etc.) as an answer. |

**Export/Download and Form Rendering**

In the top bar there are three buttons offered, the usual raise-an-issue button, and especially for Questionnaires the Download and the Render button. 

<img src="../../../img/image-20230620145425084.png" alt="image-20230620145425084" style="zoom:60%;" />

Clicking on the download button allows you to **export** the ART-DECOR Questionnaire as a **FHIR Questionnaire**. It opens a dialog that shows the available FHIR endpoints (version) of the Project and a format switch.

<img src="../../../img/image-20230620145709908.png" alt="image-20230620145709908" style="zoom:60%;" />

The Render button opens a new window that shows the rendered form. It uses the [LHC Forms](https://lhcforms.nlm.nih.gov) rendering machine from the Nation Library of Medicine. Later you can choose between two different rendering machines.

<img src="../../../img/image-20230620145902029.png" alt="image-20230620145902029" style="zoom:67%;" />

**Further Achievements for Questionnaires**

- Adjust frontend logic for patching questionnaire(responses) [AD30-1047]
- Fetch Questionnaire list only once, then sync [AD30-926]
- Metadata to be supported by the Questionnaire designer [AD30-959]
- Questionnaire Export uses wrong rendering extension [AD30-950]
- Questionnaire Panel Edit  [AD30-422]
- Questionnaire Panel Read [AD30-421]
- Questionnaire part III: edit body [AD30-657]
- Questionnaire part IV: Render design body [AD30-604]
- Questionnaire part V: Export [AD30-72]
- Questionnaire part VI: Rendering the Form [AD30-73]
- Questionnaire patch meta data fixes [AD30-1054]
- Questionnaire transform frontend [AD30-581]
- Realtime updates for Questionnaire panel [AD30-924]
- Scheduled Project Request from Questionnaire Transform Trigger [AD30-624]
- Use FHIR service for downloading questionnaire body [AD30-989]

### Concept Maps

**Concept Maps** in ART-DECOR are versionable Objects that are a documentation of the a match-making process of Coded Concepts from two Value Sets. The schematic overview shows two Value Sets and the relationship between a Coded Concept from Value Set A (the source) to B (the target). Mappings are *one way relationships*, from the source to the target system. While in many cases, the reverse mappings are valid, it is not a rule and cannot be assumed to be the case. 

![image-20230605102243766](../../../img/image-20230605102243766.png)

There is also the option (or requirement) to qualify the **Relationship**. In the example case it might be simple as the codes can be considered top be *equivalent*, but there are other mappings where relationships that are not only equivalent.

In essence we distinguish between four major types (in FHIR), ART-DECOR essentially follows the ISO Standard *ISO/TR 12300:2014 Health informatics,Principles of mapping between terminological systems* that knows a few more.

<img src="../../../img/image-20230524115946188.41ecf5aa.png" alt="image-20230524115946188" style="zoom: 50%;" />

The following table shows the general Relationship Types with definitions and its counterparts in ISO and FHIR. For the ART-DECOR Concept Map Relationship Types are comprised of only those types printed in **<span style="color: green">green bold</span>**.

| **Relationship (ART**‑**DECOR)**                 | **Definition**                                               | **ISO&#8199;Definition**                                     | **FHIR&#8199;Definition**                                    |
| :----------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| **<span style="color: green">equal</span>**      | The definitions of the concepts are exactly the same (i.e. only grammatical differences) and structural implications of meaning are identical or irrelevant (i.e. intentionally identical) | 1 = Equivalence of meaning; lexical, as well as conceptual. For example, asthma and asthma; ovarian cyst and cyst of ovary. | see equivalent                                               |
| **<span style="color: green">equivalent</span>** | The definitions of the concepts mean the same thing (including when structural implications of meaning are considered) (i.e. extensionally identical) | 2 = Equivalence of meaning, but with synonymy. For example, ureteric calculus and ureteric stone; gall stone sand cholelithiasis | equivalent = The definitions of the concepts mean the same thing |
| **<span style="color: green">relatedto</span>**  | The concepts are related to each other, and have at least some overlap in meaning, but the exact relationship is not known |                                                              | related-to = The concepts are related to each other, but the exact relationship is not known |
| **<span style="color: green">wider</span>**      | Source concept is broader in meaning than the target concept | 3 = Source concept is broader and has a less specific meaning than the target concept/term. For example, obesityand morbid obesity; diabetes anddiabetes mellitus type II. | source-is-broader-than-target= The source concept is broader in meaning than the target concept |
| subsumes                                         | deprecated – The target concept subsumes the meaning of the source concept (e.g. the source is-a target) |                                                              |                                                              |
| **<span style="color: green">narrower</span>**   | The source concept is narrower in meaning than the target concept. The sense in which the mapping is narrower SHALL be described in the comments in this case, and applications should be careful when attempting to use these mappings operationally. | 4 = Source concept is narrower and has a more specific meaning than the targetconcept/term. For example, feels ugly andself-image finding; acute renal failuresyndrome secondary to dehydration andacute renal failure syndrome. | source-is-narrower-than-target= The source concept is narrower in meaning than the target concept. |
| specializes                                      | deprecated – The target concept specializes the meaning of the source concept (e.g. the target is-a source) |                                                              |                                                              |
| inexact                                          | deprecated – The mappings overlap, but both source and target cover additional meaning, or the definitions are imprecise and it is uncertain whether they have the same boundaries to their meaning. The sense in which the mapping is different SHALL be described in the comments in this case, and applications should be careful when attempting to use these mappings operationally. |                                                              |                                                              |
| **<span style="color: green">unmatched</span>**  | There is no match for this concept in the destination concept system | 5 = No map is possible. No concept was found in the target with some degree ofequivalence (as measured by any of theother four ratings). | not-related-to = This is an explicit assertion that the target concept is not related to the source concept |

For further information about Concept Map Relationship Types and Quality of Terminology Mapping see our [documentation](https://docs.art-decor.org/introduction/associations/#quality-of-terminology-mapping-and-dataset-concepts).

In Release 3.7, **Coded Concept Mapping** between codes contained and defined by two different Value Sets are supported (Phase 1). Further releases will allow Concept Mapping between codes from any code system, and between dataset elements (non-coded Concept Map).

For this feature, additions for Frontend [AD30-832] and Backend [AD30-835, AD30-836, AD30-1022] were introduced and an **Coded Concept Map Editor** is offered [AD30-833].

A new **Concept Map Panel** is now reachable through the Terminology menu *Concept Maps*.

<img src="../../../img/image-20230620094213986.png" alt="image-20230620094213986" style="zoom:35%;" />

The Panel has the usual left part with the list of Concept Maps in the Project and a right part that shows the details once a map has been selected.

<img src="../../../img/image-20230620103653141.png" alt="image-20230620103653141" style="zoom:60%;" />

In the detail area you can see the meta data with the speciality here that Source and Target Value Set are shown. 

<img src="../../../img/image-20230620094951329.png" alt="image-20230620094951329" style="zoom:60%;" />

Typically, the Value Sets in the mapping are part of the Project (either as source or in-scope as a reference to a repository). If neither is the case, the detail area indicates that with an error message.

<img src="../../../img/image-20230620095156339.png" alt="image-20230620095156339" style="zoom:80%;" />

The Concept Map **details** are grouped by source-target codesystem pairs as expected (heading are the blue rows in the example below). The table shows Source code and display name, Target code and display name and the Relationship Type between the two codes in the middle.

<img src="../../../img/image-20230620095338318.png" alt="image-20230620095338318" style="zoom:67%;" />

Editing the Concept Map shows the Editor where you can see the Source Value Set with its codes to the left and the Target Value set with codes to the right. In the middle all Maps are shown.

<img src="../../../img/image-20230620100034018.png" alt="image-20230620100034018" style="zoom:67%;" />

Selecting a Source code automatically shows either the corresponding exsting mapping and advices what the mapping person can do.

<img src="../../../img/image-20230620100246378.png" alt="image-20230620100246378" style="zoom:67%;" />

If a mapping exists, the corresponding code is highlighted in the Target value set. You can create an unmatched concept map relation without adding a concept from the target mapping by clicking on "Set Unmatched". Select a code from the Target and drag the concept from the there to establish a Source → Target mapping.

<img src="../../../img/image-20230620103554629.png" alt="image-20230620103554629" style="zoom:67%;" />

To qualify the Relationship Type a small dialog offers you the type choices (see above). Click "OK" and the concept mapping is added to the list. New mappings appear in green, deleted ones in red. The list got tidied-up after save. 

<img src="../../../img/image-20230620100615770.png" alt="image-20230620100615770" style="zoom:50%;" />

The existing Relationship Type can be changed by clicking in the blue circle. A dialog offers all types, and additionally you can delete the mapping or set it to unmatched. In the upcoming next release you also can add and edit comments for a mapping.

<img src="../../../img/image-20230620101938427.png" alt="image-20230620101938427" style="zoom:70%;" />

You can unselect an existing map by clicking on the Target Pill in the concept mapping list, or delete an existing mapping by clicking on the Source Pill in the concept mapping list. The latter can also be done by the Relationship Type Dialog (see above).

<img src="../../../img/image-20230620100959408.png" alt="image-20230620100959408" style="zoom:70%;" />

To get a better overview of the existing concept mapping list, you can hide all already assigned mappings, leaving only unmatched codes in Source and Target list.

<img src="../../../img/image-20230620101245872.png" alt="image-20230620101245872" style="zoom:70%;" />

### Terminology Report

Terminology plays an important role in the definition of standards in healthcare. Keeping track of referenced terminology by hand is a time consuming task and also prone to errors, this is where the Terminology Report function comes in [AD30-628]. 

::: note Thanks!

This function was already existent in ART-DECOR Release 2 but is now re-engineerd to optimally use the Centralized ART-DECOR Terminology Services (CADTS). The improvement from Terminology Report Release 2 to Release 3 was also accompanied and made possible by qualified comments from [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl), [mio42](https://mio42.de) in Germany, and [Integraal Kankercentrum Nederland (IKNL)](https://iknl.nl). Thanks for all your contributions.

:::

The Terminology Report function scans a project and reports issues related to terminology (code systems) referenced in the project. This is especially useful to check if updates to code systems have consequences for a specific project.

The terminology report function will walk through datasets, transactions and Value Sets and check the following items in the project:

Datasets:

- Concept terminology associations
- Concept concept-list Value Set associations
- Concept concept-list-item Value Set item associations

Transactions:

- Concept terminology associations

Value Sets:

- Value Set items

::: note

Only items with status code ‘final’ and ‘draft’ items will be checked.

::: 

The report does not categorize messages as errors, warnings or informational messages but allows for filtering on individual message types.

It is possible to initiate a Terminology Report to check the terminologies used in a project. The report detects whether a coded concept in a Value Set has an illegal display name or is deprecated and should no longer be used.

Usually a Terminology Report check will be perfomed before creating a publication, as part of Preflighting publication and quality checks but could of course be run more often.

For projects with large Value Sets this can be a lengthy process. Our documentation shows [How to perform a Terminology Check](https://docs.art-decor.org/documentation/terminology/terminologyreport/).

### Value Set Expansion Sets Enhancements

**Intensional Value Sets** has been enhanced to support Complete Codesystems with filter options [AD30-947, AD30-1091, AD30-1092]. A Codesystem can be included completely (only the codesystem need to be mentioned) or may be included with a filter that selects only certain codes from the system.

The include *Complete Codesystems with filter* dialog has the following required components:

- a **mandatory Codesystem** from the project with optional flexibility
- one or more optional filters, each with a mandatory **property**, an **operator** and a **value**.

There are some **Filter Examples** to demonstrate the following situations.

::: example Hierarchical code system filter

Consider a hierarchical code system H (without any extra properties) with the following hierarchy. 

<img src="../../../img/image-20230620113539423.png" alt="image-20230620113539423" style="zoom:40%;" />

You can now create an intensional value set that shall only include the third level codes (from A-1-1 and A-1-2 etc.) by selecting the complete code system with all codes were CHILD exist false (implicit property filter).

<img src="../../../img/image-20230620114025479.png" alt="image-20230620114025479" style="zoom:60%;" />

This is displayed in the Value Set definitions as follows.

<img src="../../../img/image-20230620113610669.png" alt="image-20230620113610669" style="zoom:40%;" />

In the above example the expansion set will include concepts A-1-1, A-1-2, B-1-1 and B-1-2 only.

<img src="../../../img/image-20230620113637761.png" alt="image-20230620113637761" style="zoom:40%;" />

:::

::: example Code System with Filter by Property

A proprietary code system C is defined and a concept property X is defined.

Code System C

- has property X type string
- concept a, property X='A'
- concept b, property X='B'
- concept c, property X='A'
- concept d, property X='C'

You can now create an intensional value set with

- … the complete code system C, expansion set will include concepts a, b, c and d
- … the complete code system C with all codes were property X equal 'A', expansion set will include concepts a, and c only

This is rendered as follows...

<img src="../../../img/image-20230620114144698.png" alt="image-20230620114144698" style="zoom:60%;" />

...and this is the resulting Expansion Set. 

<img src="../../../img/image-20230620113716084.png" alt="image-20230620113716084" style="zoom:40%;" />

:::

::: example LOINC example Filter by Property

All LOINC codes were property METHOD = 'Manual count' (explicit property filters)

:::

With this feature, the DECOR storage format refrains from using completeCodeSystem tags in Value Set definitions [AD30-990]. This has been fixed in updated versions of the API, ART, DECOR-core, DECOR-services, FHIR 1.0, FHIR 3.0, FHIR 4.0, and Temple.

Value Set Expansion for Intensional Value Sets works with SNOMED and all ART-DECOR Code Systems [AD30-747]. Next release will allow *storage and download* of Expansion Sets.

**Further Enhancements and Fixes to Value Set Expansion Sets**

- Adaptation of the expansion set logic in the terminology API [AD30-971]
- Add save logic to the API for saving value sets with complete code system filters [AD30-970]
- Associations: possibility for adding postcoordinated expressions [AD30-244]
- A problem was solved where Code System update leads to an error message on deprecated codes [AD30-1019]
- Fixed a problem where an Expansion Set was not working [AD30-1098]

### Enhancements for Terminology Browser

There have been made some Enhancements for Terminology Browser regarding LOINC [AD30-1087]. A new filter option appears when LOINC is selected to be the only Codesystem to be searched in and offers the following dialog:

<img src="../../../img/image-20230620120653185.png" alt="image-20230620120653185" style="zoom:33%;" />

With this one can filter a search on LOINC concepts to display

- all types of LOINC codes,
- original LOINC codes,
- LOINC PART codes (LP), or
- Answer lists codes (LL)  only. 

On LOINC codes and additional content in the LOINC distribution, i. e. LOINC Parts and Part hierarchies (LP) as well as LOINC Answers (LA) and Answer Lists (LL) visit the [documentation](https://loinc.org/kb/users-guide/additional-content-in-the-loinc-distribution/) at loinc.org.

**Further Enhancements and Fixes to the Terminology Browser**

- Enhancements of the Terminology Browser Layout [AD30-1011].

### License Management for Projects

ART-DECOR® is a **promotor of open standards** and fosters to make specifications findable, accessible, and optimize their re-use.

If interoperability specifications and especially content for building block repositories shall be truly open source, project authors/responsibles **need to license it** so that others are free to use, change, and distribute the specifications.

We introduced **License information** for DECOR Projects [AD30-1004, AD30-1038, AD30-1039, AD30-1040]. In the Project Overview Panel, right after the Copyright Card the new License Card appears.

<img src="../../../img/image-20230620213221524.png" alt="image-20230620213221524" style="zoom:70%;" />

As a Project Author you can assign a license to the project as a whole. When clicking on the ADD + button in the License Card a dialog is opened that offers a list of supported licenses.

<img src="../../../img/image-20230620213406155.png" alt="image-20230620213406155" style="zoom:60%;" /> 

The list is using an [SPDX license Identifiers](https://spdx.org/licenses/), or a special case 'not-open-source'. Once saved the license information can be edited or deleted.

<img src="../../../img/image-20230620213913896.png" alt="image-20230620213913896" style="zoom:67%;" />

A project can have at most one license. We discuss [AD30-1107] whether we suggest having a default license for new projects in the future.

### Implementation Guides

The Implementation Guide Feature [AD30-438, AD30-820] allows authoring Implementation Guides (IG) that includes various artifacts such Value Sets, Code Systems, Templates and Profiles. Submission of crafted IGs to a Publication Process (ART-DECOR Release and Archive Manager ADRAM) is subject of a further release. This will also include an optional export for submission to be processed by the **FHIR IG Publisher**.

**Further Enhancements and Fixes to Implementation Guides**

- Implementation Guide Refresh does not re-use the filters set on initial ADD [AD30-1060]
- Resource part in Implementation Guide does not properly contain url and ident attributes [AD30-1059]
- Support issues for implementation guides [AD30-995]
- Support issues for implementation guides (backend) [AD30-996]

### General Centralized ART-DECOR Terminology Services (CADTS) Enhancements

We added further enhancements to the Centralized ART-DECOR Terminology Services CADTS [AD30-344] to support codesystem LOINC better.

## Further Enhancements and Fixes

The following sections focus on the various artifacts and shows new functionalities, enhancements and bugfixes.

::: note

The numbers shown are the ART-DECOR Expert Group (ADEG) ticket identifiers.

::: 

**Terminology Associations**

- Droped level and type in browser as they are not relevant [AD30-982]
- Added an option for equivalence when adding first association [AD30-984]
- Added equivalence and delete triggers error [AD30-983]
- Known codesystems of the project are now shown in a list [AD30-985]
- More clearness on Concept Association versus Terminology associations [AD30-981]
- Avoid "Concept not found" [AD30-1063]

**Datasets**

- Discussion about adding Dataset datatype ratio and currency lead to add information to the documentation but no changes where executed or planned [AD30-1065]
- A call to the API was added to get full dataset for diagrams [AD30-1046]
- Wrong API call for patching concept associations was corrected [AD30-1028]

**Issues**

- Dataset Concept was missing for selecting issue concern types [AD30-1043]
- A Return-to-issue-list button was added at the issue detail view [AD30-1044]

**Projects**

- A link was added to project and the icon for experimental was changed [AD30-1012]
- Author Panel crashed on empty display names for users [AD30-1007]
- License information in DECOR Projects was added, see also the corresponding section earlier in this Release Note [AD30-1004]

**Publications**

- Default setting for Publication Request on Publications was added [AD30-1002]

**Scenarios/Transactions**

- Erasing text in context dialog for Scenario Transactions is now possible [AD30-1112]
- A Scenario Cardinality Problem was solved [AD30-1080]
- Solved a problem with "Message Payload Too Large" when trying to save Scenario Transaction Leaf [AD30-1081]
- More details are shown on data elements in scenario viewer [AD30-945]

**Terminology**

- Added CANCEL button on New Value Set creation [AD30-1111]
- Fixed that completeCodesystem as 'include' doesn't accept flexibility [AD30-1110]
- Valueset display name now shows code display name instead of display of codesystem [AD30-1089]

**Server Admin Functions**

- Added a  Server Scheduler Overview Panel to System Administration [AD30-1015]

**User Experience, general User Interface, Software Tools**

- Tree View filter by status code now works again [AD30-1116]
- Catching the HTTP 404 on scenarios was added [AD30-1001]
- Proxy misbehavior since PATCH upgrade was corrected [AD30-1051]
- A problem with Transactions getting java:org.exist.xquery.XPathException was solved [AD30-975]
- "No data" is now shown in TreeView Panel if there is nothing to display [AD30-1014]
- Tree View filter by status code now works again properly [AD30-1116]
- Treeview Panel does not correctly handle keywords [AD30-1066]
- Allow to copy to clipboard of full artefact ID [AD30-1088]
- Better version number for Development Environment  [AD30-1042]
- CORS restrictions with FHIR service was solved [AD30-988]
- An immediate blur was added in EditableDropdown component [AD30-991]
- Version Labels in Navigation Trees are now properly displayed, clipped item/group names are now correctly clipped based on their space they would need rather their pure number of characters [AD30-992]

**Rules** (Templates)

- Added a link to BBR within referenced templates [AD30-1071]
- Bad alignment of markers in lists in the Template Panel was corrected [AD30-1106]
- Constraints on choices are now properly displayed [AD30-1104]
- Constraints in unexpected format are now displayed correctly [AD30-998]
- Enhancements of the Template Element Predicate EDIT dialog were added [AD30-1075]
- It appeared that in Rules only one of two existing constraints in the same language was displayed, as per discussion we corrected that behavior because only one constraint per language is allowed [AD30-1113]
- A missing option to add Binding Strength was added [AD30-1006]
- Constraints are now rendered more prominently in Templates [AD30-1121]
- Template element predicates are now shown properly [AD30-1074]

**Terminology Package Updates**

- Renamed the AD3 style package terminology to contain 'CADTS' [AD30-976]
- Drop support for iso-9999 terminology data [AD30-977]

::: note A BIG thank you!

We are always thankful for any comments and suggestions. Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from ELGA (Austria) and HL7 Italy / EY. Especially in the terminology and questionnaire sector we got serious feedback from Nictiz. 

:::

## Incoming features with next ART-DECOR® Releases

ART-DECOR Release 3 is focuses on new features and functionalities according to the roadmap. The following main areas are under current development and are seen as incoming functionality in the following Releases:

**Implementation Guide**

- The Implementation Guide Feature [AD30-438, AD30-820] was so far covering crafting and maintenance of Implementation Guides in a project. Ongoing work allows to use project artefacts in Implementation Guides that are rendered later appropriately. Major work is done to offer rendition in one of the next releases. It will allow the submission of an Implementation Guide with text and resources to our extended publication process and get rendered HTML publications at specified publication locations. In addition, the [FHIR IG Publisher](https://confluence.hl7.org/display/FHIR/IG+Publisher+Documentation) will be offered as one of the rendering machinery, e.g. for CDA- or FHIR-based Implementation Guides. 

**Terminology**

- Store and maintain **Value Set Expansions** when the Value Set containing intensional instructions such as include or exclude operations will be offered.
- Edit and Maintain **Terminology Mappings** type 3: Choice List – Concept Association
- We are working on SNO-POEM (SNOMED Post-coordination Editor and Management) to offer an enhanced possibility for adding post-coordinated expressions [AD30-244, ...]. Collaborations are ongoing regarding this project with [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl), [Integraal Kankercentrum Nederland (IKNL)](https://iknl.nl) and about to be established with the [Institute of Medical Informatics, University of Lübeck](https://www.imi.uni-luebeck.de/en/institute.html).

**Questionnaires**

- The existing **Questionnaire Editor** for the Items will get options for editing answer lists and especially enable-when condition. Visualization and creation/maintenance of Questionnaires and Responses will further be developed.
- Questionnaires Enhanced (QE) was introduced, starting with Release 3.4.1. Once a Questionnaire has been edited/enhanced by the user with for instance, layout instructions, conditions, etc., it migrates its status to an **Questionnaires Enhanced (QE)** in order to be distinguished from genuinely generated Questionnaires in ART-DECOR.

**FHIR Profiles and Profiling**

- Dealing with **FHIR Profiles** starts with Release 3.8 in phases. First phase feature will be visualization of **Profiles in a Project**, second feature will allow to add **Profile Associations**, similar to Template Associations. Finally **FHIR Profiling** will be another added feature utilizing the principles of the well-known CDA Template Editor.

**System Architecture**

- **GIT integration** as a natural extension of the ART-DECOR **Building Block Repository** concept will be subject of one of the next releases, collaborations have been established to quickly bring this topic to first functionality.

## Outlook

The current development is heading towards a soon next Release, planned to be published in summer 2023, where the aformentioned new features will be addressed along with enhancements and bug fixes on Release 3.7. 

As you can see: more on that to come. Thank you for your patience. Stay tuned.

## Installation Hints

If you install this new release on your own server/computer please note that the API 3.6.0 needs to be uninstalled first and then the new API 3.7.0 needs to be installed freshly. Without this action there maybe access denied errors when logging in. 

https://docs.art-decor.org/administration/releaseupdate/

::: note

The index definition for data packages has been updated in the Terminology Package v2.1.22. Installation includes re-index and **can take a significant amount of time (even hours)** depending on how many packages you have installed and the hardware you are running on.

:::
