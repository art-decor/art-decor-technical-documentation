---
permalink: /documentation/releasenotes/release34
---
# ART-DECOR® – Release 3.4

Release notes 14 April 2022

## Introduction

The ART-DECOR® tool suite is migrated from Release 2 to the new Release 3 family. This development started in 2020 and was successfully continued in 2021 and beyond. 

Recently, Release 3.4 was published that brings along new features and improvements. This is the Release Note with more details.

## General Remarks

Release 3.4 focuses on the finalization of the **Template Editor**.

Furthermore, we made good progress on the remainder of the migration, taking many opportunities for functional improvements. The major remaining topics in our "waiting room" are FHIR Profiles and Questionnaires as well as renewed and enhanced options for Implementation Guides and publications. The conceptual work has been done, and we are confident that in upcoming Releases these areas will successfully be added to the tool suite.

## New functionalities and fixes in Release 3.4 at a glance

The following sections focus on the various artefacts and shows the new functionalities (compared to ART-DECOR Release 3.3.1, compared to ART-DECOR Release 2 shown **in bold**).

::: note

The numbers shown are the ART-DECOR Expert Group ticket identifiers.

:::

**Rules** (Templates/Profiles)

- This area covers view, add, and manage constraint rules, here: templates, especially for HL7 v3 and document specifications based on the Clinical Document Architecture (CDA).
- Editing a Template Design Body is now complete and published with Release 3.4. An EDIT button puts the Design Body into Edit Mode and our new functions offer three manners of editing a Template Design Body, introducing 
  - a Template **Structure Context** Menu [AD30-494], 
  - a Template **Property Context** Menu [AD30-493], 
  - **In-column** edits [AD30-495], 
- Furthermore, a new **Undo-Function** for the Template Design [AD30-510] is implemented.
- The Template API is finalized for Template Associations to allow next to list also create, update and delete [AD30-545].
- The Template API introduced update of Templates [AD30-479], Data types allows in the context of Templates are returned [AD30-562].

**Terminology**

- The Browser now offers a combined search field, uses debounce/throttle techniques to call upon user input and has an owl to inform about search strategies and options [AD30-650]. 

**Database**

- The database eXist version 6 has recently been [released](https://exist-db.org/exist/apps/wiki/blogs/eXist/eXistdb601) and is now successively  the base for (new and migrated) ART-DECOR installations.

## Incoming features on further Releases

ART-DECOR Release 3 can be seen as a horizontal move of all ART-DECOR Release 2 features, plus adding new features immediately if appropriate or later as planned. Compared to Release 2, ART-DECOR Release 3 still has to migrate a few remaining functionalities, but focus is now more on adding new features.

The following main areas (main menu items) are still under development and are seen as incoming functionality in the following Releases:

Terminology (read-only in 3.4)

- Edit and Maintain Code Systems: a new editor has been designed and implemented, it is  tested right now
- Edit and Maintain Terminology Mappings type 3: Choice List – Concept Association

Questionnaires

- Questionnaire (QQ) and Questionnaire Response (QR) as defined in the FHIR standard plus an ART-DECOR wrapper to allow consistent handling like all versionable artefacts will be introduced from Release 3.4.1 on.
- An editor for the Questionnaire Items is in development and will be released with 3.4.+ 
- Questionnaires Enhanced (QE) will also be introduced, starting with Release 3.4.1: once Questionnaires have been edited/enhanced by the user with for instance: layout instructions, conditions, it migrates its status to an Questionnaires Enhanced (QE) in order to be distinguished from genuinely generated Questionnaires in ART-DECOR.

Rules

- A new Template Associations Panel will be introduced to allow – next to see a listing – also creation, update and deletion of associations [AD30-545]. It will offer drag-and-drop creation of associations.

## Solution details for the Template Editor, Terminology and the Database

### Tree View on Templates

All templates of a Project are shown as usual in a Navigation Card to the left of the Templates Panel. All Templates are arranged by their type, such as *CDA Document Level Type*, *CDA Header Level type* etc.

![image-20220412105738279](../../../img/image-20220412105738279.png)

Template Reference, i. e. definitions drawn from another Building Block Repository for re-use, are denoted appropriately with a link symbol.

![image-20220412110136261](../../../img/image-20220412110136261.png)

The Tree can be filtered, completely expanded or collapsed and the menu item offers to create a new Template (using Prototypes or from scratch), to establish a new Reference to a foreign Template, or to clone an existing Template in the Project.

![image-20220412105825159](../../../img/image-20220412105825159.png)

When adding a Template based on a Prototype, a search dialog offers all reachable Template Definitions to be used as the starting Protype for the new Template Design.

![image-20220413103620721](../../../img/image-20220413103620721.png)

### Template Metadata

Template Metadata appears on the right detail area of the Template Panel. Next to the description, the usual version data is shown such as id, display name, status, version and label. Context of the id is shown and any Top Level Reference Label that is used throughout the design. A classification is displayed, and the new Tag features allows Template Designers to classify their Template with tags. 

![image-20220412110638443](../../../img/image-20220412110638443.png)

Most of these items are directly editable (microscervices).

Also Usage, Relationships and Examples are shown here.

![image-20220412110800311](../../../img/image-20220412110800311.png)

### Editing the Template Design Body

The Template Design Body is shown at the bottom of the right side. You can exapand or collapse the definition tree.

![image-20220412110925788](../../../img/image-20220412110925788.png)

For Project Authors an Edit button appears that switches the Template Design Body into the edit mode. 

![image-20220413103814516](../../../img/image-20220413103814516.png)

There is a SAVE and a CANCEL button<ref1/>.

Once editing the Design Body you may go back and forth in edit history using the Undo button<ref2/>.

A line shows the Template Elements, Attributes, Includes, Choices, etc. Next to the name, the **Structure Context Menu** is shown<ref3/> (details see below) that allows *structural* changes such as adding items above, into or under existing items, deleting, selecting and deselecting items, depending on the context. This menu is followed line-wise by the datatype, cardinaity and conformance (see "In-column edits" below). 

The Description and Constraint Column<ref4/>contains further descriptions and shows the constraints defined for the line item.

Finally, a Property Context Menu<ref5/>offers options to edit the *properties* of the corresponding line item.

Please note, that the underlying Template (Model, Prototype) may have additional items that are not selected in the current Design. Nonetheless the unselected items from the underlying model are grayed-out and may be selected and the edited, selected items may be unselected. In the example below, the frist `templateId` is selected and refined, the second one is unselected (thus grayed-out).

![image-20220412113230015](../../../img/image-20220412113230015.png)

The new editing functions, offering three manners of editing a Template Design Body, introducing Structural Editing, Property Editing and In-column Editing, are described now in more detail.

#### Template Structure Context Menu

The Structure Context Menu is used to alter the structure of the Template Design. Depending on the context it offers options to add new Elements or Attributes (above, into or behind existing items), to clone a line item, insert includes or choices, select and deselect an item and deal with schematron instructions such as let, assert and report.

<img src="../../../img/image-20220412115706849.png" alt="image-20220412115706849" style="zoom:50%;" />

Please note that, once an include is added for example, its properties are changed using the Property Context Menu.

#### Template Property Context Menu

The Property Context Menu is used to alter properties of the corresponsing line item in the Template Design. The options are context and content sensitive thus may vary from line item to line item. As an example, if you are editing the properties of an element, you add, alter or delete a datatype, add or edit the containment or examples etc. The list for an attribute is shorter, e.g. you cannot add a single example for an attribute only.

<img src="../../../img/image-20220412115818489.png" alt="image-20220412115818489" style="zoom:50%;" />

As an example, of the usage of the Property Context Menu consider you have an element `code` defined in your template where you want to add an Example. The definition of the element looks like this.

![image-20220412124850322](../../../img/image-20220412124850322.png)

If you go to the Property Context Menu and use "Insert Example", you get the folllowing dialog.

<img src="../../../img/image-20220412125345517.png" alt="image-20220412125345517" style="zoom:40%;" />

This let you add an example to the list manually, delete an example from the list, or ask the Example Wizzard to generate an example. Once added you can save it and it appears immediately as a property of your element.

![image-20220412125123189](../../../img/image-20220412125123189.png)

#### In-column edits

The columns datatype, cardinality and conformance allow direct editing in the respective column. 

<img src="../../../img/image-20220412125452524.png" alt="image-20220412125452524" style="zoom:50%;" />

The datatype is context and content sensitive and offers all options to change a datatype of an item<ref1/>. In the example, A CD (Concept Descriptor datatype) may be refined to any of the datatypes in the list<ref2/>. The "wisdom" for this list comes from the underlying datatype specification(s). 

Also the cardinality may be changed in-column<ref3/>. The typical situations are refelcted as a choice, e.g. `0..1`, `1..*` etc., but you may edit the cardinality also component-wise to define "unusual" cardinalties.

<img src="../../../img/image-20220413104517536.png" alt="image-20220413104517536" style="zoom:50%;" />

The conformance can also be editing In-column<ref4/>.

#### Undo-Function

A new Undo-Function has been added for the Template Design. Once editing the Design Body you may go back and forth in edit history of the current edit session using the Undo buttons in the top buttons row.

![image-20220412125554094](../../../img/image-20220412125554094.png)

There are three steps stored in the buffer, so you can undo work up to three steps. If there is no edit history, the buttons are disabled.

### Terminology

The Browser now offers a combined search field, where you can switch between searching a term or searching a code (here were two fields in the Releases before).

<img src="../../../img/image-20220412125828832.png" alt="image-20220412125828832" style="zoom:50%;" /> 

The search field now uses *debounce/throttle techniques* to initiate the search upon user input. An owl is placed right to the search field to inform about search strategies and options. This is what the owl says:

<img src="../../../img/image-20220412130035075.png" alt="image-20220412130035075" style="zoom:70%;" />

### Database

The database eXist version 6 has recently been [released](https://exist-db.org/exist/apps/wiki/blogs/eXist/eXistdb601) and is now the base for (new and migrated) ART-DECOR installations.

## Outlook on what comes next: ART-DECOR® – Release 3.4.1

The current development is heading towards a soon next Release, where the following topics are on:

- Edit and Maintain **Code Systems**
- Edit and Maintain **Terminology Mappings** type 3: Choice List – Concept Association
- Option to view **Value Set Expansions** when the Value Set contains intensional instructions such as include or exclude operations
- A new **Template Associations Panel** will be introduced to allow – next to list – also create, update and delete. It will offer drag-and-drop creation of associations.
- **Questionnaire** (QQ) and **Questionnaire Response** (QR) Management and a Questionnaire **Editor**
- First **FHIR profiling** (Releases 3.5 and 3.6) will be offered.
- A note on terminology: we are re-organizing ART-DECOR in how terminology works internally. The API reflects the progress in this area. The sections codeSystem and valueSet support DECOR representations, while the section terminology represents what has been dubbed **Centralized ART-DECOR Terminology Services** or **CADTS**. CADTS currently powers the Browser frontend.

As you can see: more on that to come. Thank you for your patience. Stay tuned.
