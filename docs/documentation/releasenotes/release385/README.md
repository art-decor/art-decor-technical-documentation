---
permalink: /documentation/releasenotes/release385
---
# ART-DECOR® – Release 3.8.5

Release notes 17 September 2024

## Introduction

We are happy to announce **Release 3.8.5**, another minor release with a number of additions, smaller new features and several improvements. 29 tasks were performed, partially based on client requests. Due to major activities in the area of user feedback and controlled user interviews, collected test findings from various international sources and our own improved testing environment, this release also addressed 18 bugs that were fixed.

Thanks to all testers and people who submitted improvement suggestions! We are lucky to have you with us.

In total, 47 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8.5 focuses on the following major topics:

- new features and corrections to the [→](#questionnaires) **Questionnaires Management** Panel,
- enhancements in the area of [→](#terminology) **Terminology**,
- enhancements in the area of [→](#datasets) **Datasets**,
- enhancements in the area of  [→](#software-development-tools-features) **Software Development / Tools features**,
- enhancements to  [→](#testing-and-validation) **Testing and Validation**.

In addition, the announced eXist Database Patch is now available for v6.2.0 (and newer).

## Areas of New Features and Improvements

### Questionnaires

We added feature to add/edit **Copyright and Publishing Authority** Information to an ART-DECOR Scenario and ART_DECOR Questionnaire (that thus also appears in automatically transformed FHIR Questionnaires) [AD30-1620]. Frontend [AD30-1633] and backend [AD30-1632] have been enhanced to offer this feature.

There is now the complete "inheirit" cascade in place so that 

- If Copyright or Publishing Authority information is defined on the **Transaction Leaf** level, it is used to populate the Questionnaire Copyright and Publishing Authority Information
- otherwise if Copyright or Publishing Authority information is defined on the **Transaction Group** level, it is used
- otherwise if Copyright or Publishing Authority information is defined on the **Scenario** level, it is used
- otherwise use the **Project** Copyright or Publishing Authority information - if present - to populate the Questionnaire Copyright and Publishing Authority Information.

Copyright or Publishing Authority information does not have to be defined on the various levels. Only when the user defines this on one of the levels, or on project level, the algorithm takes this into account.

 <img src="../../../img/image-20240919174843723.png" alt="image-20240919174843723" style="zoom:67%;" />

We elaborated the different aspects of a Questionnaire that is derived from a Transaction and regarding the transformation of  **Dataset / Transaction Elements** [AD30-1379]. This is how these items are reflected in automatically translated Questionnaires.

| **Dataset / Transaction Element**                            | **Example**                                                  | **If automatically translated into ART-DECOR Questionnaire** | If converted to FHIR Questionnaires                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Name                                                         | Pulse Regularity                                             | Questionnaire item text                                      | item.text                                                    |
| Operationalization if its content **does not contain a question mark** = is considered as help text or instruction | Place the tips of your first and second finger on the inside of the patient's wrist. Press gently against the pulse. Take your time to note any irregularities in strength or rhythm. If the pulse is regular and strong, **measure the pulse for 30 seconds**. | adding the original concept operationalization as Questionnaire item operationalization | converted as a subitem with item.text and itemControl helpButton |
| Operationalization if its content **does contain a question mark** = is considered as the real question | How regular is your pulse?                                   | item.text<br />The original concept name is added as Questionnaire item synonyms | item.text                                                    |
| Description                                                  | Pulse rhythm regularity                                      | -not at all-                                                 |                                                              |
| Comment                                                      | any text                                                     | -not at all-                                                 |                                                              |

Questionnaires now show **answersOptions** with their types [AD30-1378] and display and allow adding a single answer value set reference [AD30-1640]

<img src="../../../img/image-20240919174603000.png" alt="image-20240919174603000" style="zoom:70%;" />

<img src="../../../img/image-20240919175909963.png" alt="image-20240919175909963" style="zoom:70%;" />

An issue was handled that came up in the Dutch UZ project [AD30-1641]. In FHIR Questionnaires `item.text` is string (not markdown). In case the original question text contains markup the conversion now adds a known FHIR extension that reflects the XHTML as markdown. The item.text is un-HTMLified. 

This is an example in XML.

```xml
<text value="What was the method of estimation?">
  <extension url="http://hl7.org/fhir/StructureDefinition/rendering-markdown">
    <valueMarkdown value="What was the **method of estimation**?"/>
  </extension>
</text>
```

**Further Achievements in this Release for Questionnaires**

- The documentation on Questionnaires Management has been enhanced [AD30-1483]
- A bug was fixed where no terminology association was utilzed in creation questionnaire [AD30-1494]
- FHIR Questionnaire.date is now ART-DECOR's lastModifiedDate [AD30-1631] 
- Usage of operationalization from concepts for Questionnaire item text [AD30-1642]
- The LHCForms rendering machine is now connected to the TerminologyServer [AD30-1673]
- The LHCForms machine has been updated [AD30-1679]

### Terminology

We already do offer API endpoints to **import** (POST) **Terminology** items such as Value Sets, Code Systems and Concept Maps into an existing project using the DECOR format. Before this release we did not have any option to use a **FHIR endpoint** (such as “4.0”) and POST an FHIR Terminology item such as Value Sets, Code Systems and Concept Maps into the system.

The original request comes from the Austrian ELGA and their TerminoloGit where ART-DECOR shall be the source of truth for the Terminology but some terminologies are defined externally and delivered in FHIR (4) format and need an import option into a DECOR project.

We now offer a FHIR API endpoint option to import FHIR Terminology (value sets) with operations [AD30-1552]. The new operation has the URL prototype

```
 POST {FHIR_URL}/4.0/demo1-/ValueSet/$toDecor
```

...where the body of the POST shall contain a JSON FHIR valueset. The new operation then does the following actions:

- validate the incoming FHIR valueset against schema and schematron,
- check if the valueset already exists in the project (by name),
- transform the the valueset to DECOR format,
- validate the result against DECOR schema and schematron and
- store the valueset into the project
- return a HTML header "201 created" as a return value.

We tested the new feature extensively and will further test with ELGA and other interested parties.

**Further Achievements in this Release for Terminology**

- CADTS Code System conversion has effective date problems with project owned code systems [AD30-1629]
- The use of a wrong URL used for Terminology Identifiers Panel has been corrected [AD30-1656]
- The Terminology Report was extend with UCUM essence check [AD30-1611]
- The Terminology Report was extend with conceptlist vs valueset size check [AD30-1612]
- When importing LOINC panels, the "into" option now accepts only concept groups, not concept item [AD30-1635]
- The menu items for the Terminology menu have a new order [AD30-1643]

### Dataset

We tackled performance issues with de-contain or inherit with large datasets [AD30-1626] by replacing single database operations with bulk operations.

MyCommunity support is now available in ART-DECOR Release 3 [AD30-1342].

### Software Development / Tools features

We re-introduced grouping of versions in Identifier Listings [AD30-1677] that was present already in ART-DECOR Release 2. For example in the example below, If a Value Set has multiple versions, it is once named with OID and name etc., and all versions are then subsequently listed in a nested way.

<img src="../../../img/image-20240919192159254.png" alt="image-20240919192159254" style="zoom:80%;" />

**Further Achievements in this Release for Software Development / Tools features**

- We stopped using vue-mermaid-string and now use the [Mermaid package](https://mermaid.js.org) directly [AD30-1445].
- A problem was observed that no output was shown in Project's Author panel when a name starts with a space; this has been fixed [AD30-1638]
- Publications in Dutch should use "Versie" as label for @effectiveDate to be consistent with AD3 [AD30-1658]
- Release 3.8.5 Acceptance testing was done and documented [AD30-1696]
- Corrections of French language labels have been performed (thanks to [Agence du Numérique en Santé (ANS)](https://gnius.esante.gouv.fr/en/players/player-profiles/agence-du-numerique-en-sante-ans) and [Kereval](https://www.kereval.com/home/) teams) [AD30-1705]. Some of the automatic and semi-automatic translations in fact make us laugh. For example the English term *date* was robot-translated happily into *rendez-vous amoureux* :-). 
- For system-wide Urgent News the language was not updated upon switching, this is fixed now [AD30-1649]
- Vue installation procedure is changed for patching the `env` file [AD30-1693]
- HTTP Proxy installation procedure is changed for patching the `env` file [AD30-1694]
- Our internal documentation regarding Frontend developer workflow READMEs including releases and Wiki testscenario has been elaborated [AD30-1577]

### Publications

The ART-DECOR Release and Archive Manager process (ADRAM) did not safely communicate and document errors when a publication build process failed. This resulted in a stuck scheduled tasks queue. This problem has been fixed now [AD30-1684].

We identfied an enhancment for Project compilations that should better compile once for all language and use that as basis for other languages. This process and performance enhancement has been implemented [AD30-1700].

When creating a publication one may choose the languages to compile for. By default this is done for all supported languages but there was an error for handling publication's project languages other than default. This is now corrected [AD30-1699].

### Testing and Validation

Schematron  predicate generation does now no longer take datatype `SC` into account [AD30-1666]

### Project

- A search field has been added in the Project Authors Panel [AD30-1592]
- MyCommunity support in ART-DECOR Release 3 was completed  [AD30-1180]
- An error was fixed when DECOR check was run: attribute classcode does not match [AD30-1467]
- Intermediate version now shows in th Project Index [AD30-1556]
- In ADA now easier referencing of dataset concepts is allowed [AD30-1653]
- An  upload function for new ADA project packages has been added [AD30-1545]
- Adding the `xsi:` Namespace did not work, this has been fixed [AD30-1671]
- Development releases failed for demo1 and demo5 on Server `acceptance` , this has been investigated and fixed[AD30-1695]
- The new Artifact Manager emited an HTML 404 under certain circumstances, this has been fixed [AD30-1667]
- Identifiers panel does now properly switch projects [AD30-1676]

### Templates

There was an unwanted duplication of item labels in template design observed when editing/saving a template that has a top level item reference label. This has been fixed [AD30-1704].

Creating Template Associations is now possible when template design root is a choice [AD30-1414].

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset and Questionnaire editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org) team. Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl). Thanks to Gunter Haroske working for the [Deutsche Gesellschaft für Pathologie / German Association of Pathology](https://www.pathologie-dgp.de) pointing to shortcomings and missing features in our Questionnaire functions and to the Dutch [Uitkomstgerichte Zorg](https://www.uitkomstgerichtezorg.nl) Program for contributions in the same area. Special thanks also to the [mio42 GmbH](https://mio42.de) team for feature suggestions and bug fix requests.

Please don't hesitate to write us with suggestions for improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.6 quickly, planned to be published end of October 2024. One of the major aspects are more features and enhancements for the Questionnaire Management. As you can see: more on that to come. Thank you for your patience. Stay tuned.
