---
permalink: /documentation/releasenotes/release35
---
# ART-DECOR® – Release 3.5

Release notes 17 June 2022

## Introduction

In 2020, the migration of the ART-DECOR® tool suite from Release 2 to the new Release 3 family was started. Meanwhile we are happy to announce **Release 3.5** today. 

Almost all features and functionality from the "Classic" Release 2 is now migrated into Release 3. The recently published version also addresses new features and improvements. This is the Release Note with more details.

## General Remarks

Release 3.5 focuses on the following major topics.

- Template Associations Panel
- Code System Maintenance
- Expansion Sets of Intensional Value Sets (part 1: SNOMED only)
- Further improvements of the Centralized ART-DECOR Terminology Services (CADTS)
- Web sockets
- Scheduled tasks on project and on server level 
- Recursive Status Change of hierarchically arranged artefacts
- Representing Templates in a Scenario/Transaction

Furthermore, we made good progress on **Questionnaires** as well as renewed and enhanced options for Implementation Guides and publications and FHIR Profiles. The conceptual work has been done, and we are confident that in upcoming Releases these areas will successfully be added to the tool suite.

## New functionalities and fixes in Release 3.5 at a glance

The following sections focus on the various artefacts and shows the new functionalities (compared to ART-DECOR Release 3.4, compared to ART-DECOR Release 2 shown **in bold**).

::: note

The numbers shown are the ART-DECOR Expert Group ticket identifiers.

:::

**Rules** (Templates/Profiles)

- The introduction of the **Template Associations Panel offers a new look-and-feel** to create links between a logical concept in a dataset or scenario and a technical artefact such as HL7 Templates or Template components [AD30-544, AD30-702]. The new Panel is offering read operations on all existing associations, in a subsequent release it allows – next to see a listing – also creation, update and deletion of associations [AD30-545] by simple drag-and-drop creation of associations.
- There is now a COPY button for Template Example editing [AD30-713].

**Terminology**

- We concluded the set of features of our **new Code System Maintenance**. Code Systems can now be maintained (soon also: add new ones, clone existing), our new Code System editor supports lists, hierarchies or networks of codes. [AD30-590, AD30-648, AD30-649, AD30-668, AD30-694, AD30-695].
- Visualization of **Expansion Sets of Intensional Value Sets** have been introduced [AD30-702]. This now works for intensional Value Sets with SNOMED codes only, but it will be extended to work for all ART-DECOR Code Systems. Thanks to our **Centralized ART-DECOR Terminology Services (CADTS)** extending intensional Value takes a blink of an eye only. Next step is also to allow *storage and download* of Expansion Sets.
- After a bug fix now the Value Set Copyright Statement is displayed correctly [AD30-704].
- The list of children of a coded concept is now always displayed completely in the Terminology Browser [AD30-683].

**Scenarios/Transactions**

- Transaction panel now shows transaction properties instead of dataset concept properties [AD30-670].
- Finally, proper Icons are shown for Templates that are Representing Templates for a Scenario/Transaction.

**System Architecture**

- Important for collaborative work is our support for **Web sockets** [AD30-677, AD30-681, AD30-684, AD30-705]. It makes working together easier, as everyone working simultaneously in the same area of artefacts in a project gets informed about changes other team members just did.

::: note

If you **install ART-DECOR locally**, you need to also install and configure our additional software *art-decor-http-proxy* from Releasae 3.5 on.

:::

- **Scheduled tasks** per project and per server have been introduced and extended [AD30-663, AD30-667]. Notification of a new user to the server or summaries of issues are managed now with this set of feature. In addition, project compilation, creation of validation or publication packages and checks of the project is scheduled work now, either user triggered or initiated by an administration process.

**General**

- Release 3.5 allows Recursive Status Change of hierarchically arranged artefacts, a well-known feature taken over from Release 2 [AD30-641].

**Dataset**

- The **Dataset Panel backend behavior** has been improved for better performance on Navigation Tree viewing [AD30-680].

**Project**

- For existing publications: editing the release label is now possible [AD30-646].

## Incoming features on further Releases

ART-DECOR Release 3 is a horizontal move of all "Classic" Release 2 features, plus adding new features immediately if appropriate or later as planned. This migration is now almost complete. We focus on new features and functionalities.

The following main areas are still under development and are seen as incoming functionality in the following Releases:

Terminology

- Expansion Sets of Intensional Value Sets for CADTS-aware Code Systems in ART-DECOR, with lower performance for all Code Systems.
- Edit and Maintain Terminology Mappings type 3: Choice List – Concept Association
- New Code Systems can be created or existing ones be cloned and added to a project [AD30-693]
- Code System Identifiers will be added to the listing in the Terminology Identifiers Panel

Questionnaires

- Questionnaire (QQ) and Questionnaire Response (QR) as defined in the FHIR standard plus an ART-DECOR wrapper to allow consistent handling like all versionable artefacts will be further enhanced from a subsequent Release on.
- An editor for the Questionnaire Items is in development and will be released with 3.6+, a new member of the Expert Group dedicates work on visualization and creation/maintenance of Questionnaires. 
- Questionnaires Enhanced (QE) will also be introduced, starting with Release 3.4.1: once Questionnaires have been edited/enhanced by the user with for instance: layout instructions, conditions, it migrates its status to a Questionnaires Enhanced (QE) in order to be distinguished from genuinely generated Questionnaires in ART-DECOR.

Rules

- The Template Associations Panel allow creation, update and deletion of associations [AD30-545] by drag-and-drop actions.
- The work on the FHIR Profile Editor has started.

System Architecture

- Websocket Functionality (see above) for more Panels in place.
- GIT integration as a natural extension of the ART-DECOR Building Block Repository concept will be subject of one of the next releases, collaborations have been established to quickly bring this topic to first functionality.

## Solution details

### Template Associations Panel

The new **Template Associations Panel** offers a new look-and-feel to create links between a logical concept in a dataset or scenario and a technical artefact such as HL7 Templates or Template components. The new Panel is offering read operations on all existing associations, in subrelease 3.5.1 it allows – next to see a listings – also creation, update and deletion of associations by simple drag-and-drop creation of associations.

<img src="../../../img/image-20220623152425972.png" alt="image-20220623152425972" style="zoom:50%;" />

In principle, the Panel allows you to select a Dataset or Scenario <ref1/>, the content is shown in the left Navigation Bar <ref2/>. Please note that concepts that have an association with a Template or Template Element are in bold face.

The right Navigation Bar <ref3/> shows all Templates of the Project, as usual grouped by type. Both the left and the right Navigation Bar can be expanded with one click.

The area denoted by <ref4/> will carry the details of the selected Dataset / Scenario concept, the Template and of course the actual mappings/associations. For example if you select *Person*––*Date of birth* on the left Dataset details, the middel area shows the following details.

<img src="../../../img/image-20220623153026886.png" alt="image-20220623153026886" style="zoom:67%;" />

The top middel area shows some details of the selected concept. The midde part shows the Template were the mapping/association points to. Finally, the lower part depicts the mapping/association between the selected concept and the Template/Template element.

When clicking on a Template in the Templates Navigation Bar,...

<img src="../../../img/image-20220623161012075.png" alt="image-20220623161012075" style="zoom:50%;" />

...it morphs the Navigation Bar (showing all Templates) to display the selected Template Element hierarchy.

An association can be created when a concept and a Template or Template Element is selected. You simply take the part of the Template, drag it into the middle area and drop it there. 

<img src="../../../img/image-20220623160745185.png" alt="image-20220623160745185" style="zoom:50%;" />

The Template Association is then created and displayed as follows in the lower middle card. It can be deleted by simply click on the X button in the arrow.

<img src="../../../img/image-20220623160805561.png" alt="image-20220623160805561" style="zoom:67%;" />

### Code System Maintenance

The **Code System Panel** offers the list of all Code Systems of the Project, sorted alphabetically.
The icon indicates wether a Code System is in-project (colored dot) or a reference from a Building Block Repository (chain symbol).

<img src="../../../img/codesystem-tree-1.png" alt="codesystem-tree-1" style="zoom:80%;" />

Once a Code System is selected, the details of the Code System (metadata and the concept list) are shown. Project authors can edit the metadata directly. The concept list is edited with the [Code System Editor](#code-system-editor).

<img src="../../../img/image-20220623173254816.png" alt="image-20220623173254816" style="zoom:50%;" />

The Version Stepper <ref1/> allows to select a different version of the Code System.

Additional meta data becomes available by clicking the green Show More arrow <ref2/>.

Use the search bar to search coded concepts in the concept list <ref3/>. It's possible to search by code or designation. For exact search, use a colon as a prefix, e.g. `:ZA`. 

The concept list <ref4/> shows all concepts, preferred terms, synonyms etc.

The Usage and Issues tab <ref5/> can be expanded to load the corresponding information.

### Code System Editor

Project authors can access the Code System Editor by clicking the EDIT button in the header of the concept list.

The Code System Editor contains two main sections:

- [Concept List](#concept-list)
- [Property Definitions](#property-definitions)

#### Concept List

This section is used to add or edit coded concepts in the concept list.

The hierarchical tree on the left can be used to select a coded concept.
Once a concept is selected, the details of the concept are shown on the right. If applicable, parent and child concepts are also displayed here.

![image-20220623173959537](../../../img/image-20220623173959537.png)

The menu button <ref1/> allows adding new coded concepts to the list.

Buttons are available <ref2/> to move the concept among its sibblings and to manage parent relationships.

The Designations tab can be expanded <ref3/> to add or edit designations.

The Properties tab can be expanded to add or edit properties to the concept <ref4/>. This is only available if there are [Property Definitions](#property-definitions) defined in the Code System.

#### Property Definitions

This section is used to manage property definitions.

![image-20220623174247514](../../../img/image-20220623174247514.png)New property definitions can be added <ref1/>.

Definitions that are used by a coded concept are not editable and can't be removed <ref2/>.

### Expansion Sets of Intensional Value Sets

Visualization of **Expansion Sets of Intensional Value Sets** have been introduced. This now works for intensional Value Sets with SNOMED codes only, but it will be extended to work for all ART-DECOR Code Systems.

![image-20220623174846393](../../../img/image-20220623174846393.png)

An intesional Value Sets contains instructions to derive the actual code list instead of the code list itself. As an example the Value Set in this example states an INCLUDE of the *Blood group A* code <ref1/> and also any children that may exist in the code hierarchy. If an intensional Value Set can be expanded into an Expansion Set, an appropriate button appears <ref2/>. Clicking on it offers you a dialog with the number of codes in the Expansion Set, and the option to see the Expansion Set (restricted number of codes). 

<img src="../../../img/image-20220623175406356.png" alt="image-20220623175406356" style="zoom:50%;" />

In a subsequent release Expansion Sets can also be stored and maintained.

### Websockets

Beginning with Release 3.5, HTTP Proxy Websocket techniques have been introduced and implemented. 

The **ART-DECOR HTTP Proxy** consists of two parts, the HTTP Proxy and the WebSocket Server. Any request that is sent to the proxy server will be passed to the ART-DECOR API. The proxy server will return the response exactly as is.

The Websocket techniques makes also working together easier and safer, as everyone working simultaneously in the same area of artefacts in a project gets informed about changes other team members just did and in fact sees the changes as an update. For example if one team member changes the name of a concept...

![image-20220623165326817](../../../img/image-20220623165326817.png)

...other team members in the same Panel gets informed with a snackbar notification and the concepts is also updated fro them.

![image-20220623165313839](../../../img/image-20220623165313839.png)

### Scheduled tasks on project and on server level 

Scheduled tasks per project and per server have been introduced and extended. Notification of a new user to the server or summaries of issues are managed now with this set of feature. Notified users gets this kind of emails from the Notifier.

<img src="../../../img/image-20220623164723230.png" alt="image-20220623164723230" style="zoom:45%;" />

In addition, project compilation, creation of  validation or publication packages and checks of the project is scheduled work now, either user triggered or initiated by an administration process. The Panels that triggered the schedule action gets progress indicators of the background processes like in this example for the compilation of a Develepment Version.

<img src="../../../img/image-20220623164856365.png" alt="image-20220623164856365" style="zoom:67%;" />

The Project Overview now also has a Scheduled Tasks Card telling a list of Scheduled Tasks in the background.

<img src="../../../img/image-20220623165028424.png" alt="image-20220623165028424" style="zoom:67%;" />

### Recursive Status Change of hierarchically arranged artefacts

If you change the status of group in hierarchically arranged artefacts such as datasets, scenarios, a new dialog appears to offer you a Recursive Status Change of the group item and all nested artefacts, a well-known feature taken over from Release 2. The dialog shows a switch where you can determine *Recursive* hierarchical status change or not. In addition you may add a *Version Label* and an *Expiration Date*.

<img src="../../../img/image-20220623163900763.png" alt="image-20220623163900763" style="zoom:50%;" />

::: note

A *Recursive* hierarchical status change can possibly not be undone in a satisfactory way.

:::

### Representing Templates in a Scenario/Transaction

A Scenario Transaction that has an association to a Template, a Representing Template, now shows the proper symbol.

<img src="../../../img/image-20220623163639094.png" alt="image-20220623163639094" style="zoom:33%;" />

## Outlook on what we expect in next ART-DECOR® Releases

The current development is heading towards a soon next Release, where the following topics are on:

- Smaller Enhancements and Bug Fixes on Release 3.5
- Edit and Maintain **Terminology Mappings** type 3: Choice List – Concept Association
- View, store and maintain **Value Set Expansions** when the Value Set contains intensional instructions such as include or exclude operations
- **New Code Systems** can be created and existing ones be cloned and added to a project
- **Template Associations Panel** allows create, update and delete of associations with simple drag-and-drop actions.
- **Questionnaire** (QQ) and **Questionnaire Response** (QR) Management and a Questionnaire **Editor**
- First **FHIR profiling** (Release 3.6 and further) will be offered.

As you can see: more on that to come. Thank you for your patience. Stay tuned.
