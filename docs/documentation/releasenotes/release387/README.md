---
permalink: /documentation/releasenotes/release387
---
# ART-DECOR® – Release 3.8.7

Release notes 13 December 2024

## Introduction

We are happy to announce **Release 3.8.7**, another minor release with a number of additions, smaller new features and several improvements. 20 tasks were performed, partially based on client requests. Due to major activities in the area of user feedback and controlled user interviews, collected test findings from various international sources and our own improved testing environment, this release also addressed 15 bugs that were fixed.

Thanks to all testers and people who submitted improvement suggestions! We are lucky to have you with us.

In total, 35 tickets were handled. This is the Release Note with more details.

## General Remarks

As a summary, Release 3.8.7 focuses on the following major topics: enhancements in the area of

- [→](#dataset) **Datasets** and [→](#scenario) **Scenarios**,
- [→](#terminology) **Terminology**,
- [→](#software-development-tools-features) **Software Development / Tools features**,
- [→](#templates) **Templates**,
- [→](#system-adminstrator-tasks) **System Adminstrator Tasks**.

In addition, we work closely with our eXist partners to test and deploy the new **eXist Database Release v6.3.0** soon.

## Areas of New Features and Improvements

### Dataset

#### Further Achievements in this Release for Datasets

- A problem was fixed when one cannot add a dataset description (Save button inactive) [AD30-1727].
- The user preferences for expanded / collapsed lengthy dataset item descriptions is now stored during the user's browser session [AD30-1819].

### Scenario

#### Enhanced options for cardinality-conformance combinations in Scenario Editor

When a concept item in the Scenario Editor is newly selected, a list of pre-defined choices as short-hand notations for the cardinality-conformance combinations are offered. This list lacks of an "optional" choice. We added “0..1 Optional” to this list [AD30-1801].

<img src="../../../img/image-20241212154851029.png" alt="image-20241212154851029" style="zoom:50%;" />

Additionally we added more options to Scenario Editor Cardinality Settings under "OTHER" to be able to express cardinalities such as 1..10 or 0..32 etc.  [AD30-1820].

<img src="../../../img/image-20241212154952154.png" alt="image-20241212154952154" style="zoom:50%;" />

#### Further Achievements in this Release for Scenarios

- An issue was fixed when linking to Template in Scenario URL: when clicked jumps to most recent version instead of linked version [AD30-1724]
- Two problems around the creation process of a Scenario have been fixed, one error that was triggered when managing actors [AD30-1749] and one that was triggered on copyright management [AD30-1750].

### Terminology

A short-hand option was added to create a Value Set Reference when not found yet in Terminology Panel [AD30-1775].

<img src="../../../img/image-20241212160327146.png" alt="image-20241212160327146" style="zoom:50%;" />

Clicking on the "Add reference" link offers the usual "New reference" dialog now.

![image-20241212160341355](../../../img/image-20241212160341355.png)

#### Further Achievements in this Release for Terminology

- Preferred language setting of terminology browser is now retained [AD30-1745].
- A restore of a terminology association on inherited concepts did not show the right information; this has been fixed [AD30-1757].
- When defining a terminology association ‘overwrite’ on transaction level, an option was offered to go back to the dataset associations and all transaction association overwrites were removed upon such a restore action; This was disrupted for Value Sets and re-introduced [AD30-1764].
- A script has been created to show the list of LOINC codes used in projects of a certain governance group (e.g. Nictiz)  [AD30-1768].
- The Terminology report now checks if SNOMED concept is part of SNOMED Core module [AD30-1770].
- Some Value Set links did not include effectiveDate; this has been fixed [AD30-1774].
- An error was fixed where the wrong version of a value set reference was loaded [AD30-1780].
- A copy of the direct link to value sets did not always point to the correct deep link (URL); this has been fixed [AD30-1785].
- Terminology Reports threw an error under certain circumstances in Scenarios; this has been fixed [AD30-1795].
- Some changes have been applied for Dutch display of terminology package [AD30-1802].
- Value Set versionLabel are now visible in dataset view [AD30-1803].

### Software Development / Tools features

#### Copyright and Publishing Authority handling

Publisher and copyright information play a role for example for Questionnaires but also for other artifacts. It is especially the deployed FHIR export where users desire to include Publishing Authority and Copyright information. For that purpose ART-DECOR handles publisher and copyright information on different levels, where the most prominent one is the **Project copyright**. 

<img src="../../../img/image-20241212161251560.png" alt="image-20241212161251560" style="zoom:50%;" />

The next level is the ability to assign copyright to each single dataset, the **Dataset copyright**. We allow to assign publisher and copyright information on **Value Set** and on **Code System** Level.

We now have option to assign Publishing Authority and Copyright information [AD30-1603] including 

- Concept Maps 
- Templates or Profiles and 
- Questionnaires.

In that light some fixed have been perfomred on the AdCopyrightCrud component [AD30-1808].

#### Further Achievements in this Release for Software Development / Tools features

- When creating a value set, the user has the option to assign his own id’s; the UI now performs a better check for sanity on the id [AD30-1390].
- The FHIR 4 server did not handle BBR references correctly; this has been fixed [AD30-1738].
- Care and complete view for project index as a replacement for AD2 links is now offered  [AD30-1747].
- Bugfixes and design question for Copyright and Authorities panel were applied [AD30-1754].
- An acceptance test was performed for release 3.8.7 [AD30-1805].
- The ADA (ART-DECOR Applications) index-admin could now give more focus on "invalid"  instances using filtering [AD30-1771].
- The OID-Registry search max results was increased and the search for symbolic names was enhanced [AD30-1829].

### Templates

#### Better links to documentation

Direct links to the respective data type documentation at [docs.art-decor.org](https://docs.art-decor.org/documentation/datatypes/) are now shown when viewing a Template Design  [AD30-1800].

<img src="../../../img/image-20241212162901387.png" alt="image-20241212162901387" style="zoom:50%;" />

#### Further Achievements in this Release for Templates

- Template assert / report with mixed content is now displayed correctly (backend [AD30-1729] and frontend [AD30-1736]).

### System Adminstrator Tasks

#### Enhanced Scheduled Tasks monitoring

The System Adminstrator interface got a new extended list for viewing Scheduled Tasks [AD30-1691]. This Sysadmin Panel now shows Scheduled Tasks in Projects, invstigated server-wide, and Server Scheduled Tasks. Problematic tasks (e.g. unexpected long-runners or undesired completed tasks) are marked with orange or red warning chips. 

<img src="../../../img/image-20241212162202724.png" alt="image-20241212162202724" style="zoom:40%;" />

#### Further Achievements in this Release for System Adminstrator Tasks

- The permissions for Database Administrators (dba) users have been enhanced and fixed in the backend [AD30-1786].
- Serverfunctions for concept maps have been introduced [AD30-1828].

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Special thanks for the feedback regarding the Template Viewer and Editor to the colleagues from [ELGA (Austria)](https://www.gesundheit.gv.at/gesundheitsleistungen/elga.html) and [HL7 Italy](http://www.hl7italia.it/hl7italia_D7/) / Ernst&Young (EY) and for Dataset and Questionnaire editor to the [Berlin Institute of Health @ Charité](https://www.bihealth.org/) team. Dataset and Scenario enhancements are also coming from the [Professional Record Standards Body](https://theprsb.org/) in the UK – PRSB.

Especially in the terminology and questionnaire sector we got great feedback from the [Dutch ICT Institute in Health Care (Nictiz)](https://nictiz.nl/). Thanks to Gunter Haroske working for the [Deutsche Gesellschaft für Pathologie / German Association of Pathology](https://www.pathologie-dgp.de/) pointing to shortcomings and missing features in our Questionnaire functions and to the Dutch [Uitkomstgerichte Zorg](https://www.uitkomstgerichtezorg.nl/) Program for contributions in the same area. 

Special thanks also to the [mio42 GmbH](https://mio42.de/) team, [Agence du Numérique en Santé](https://esante.gouv.fr/) (ANS) and our partners at [Kereval](https://www.kereval.com/home/) / [IHE Europe](https://www.ihe-europe.net/) for testing, feature suggestions and bug fix requests.

Please don't hesitate to write us with suggestions for improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.8 quickly, planned to be published end of January 2024. One of the major aspects are more features and enhancements for the Questionnaire Management. As you can see: more on that to come. Thank you for your patience. Stay tuned.
