---
permalink: /documentation/releasenotes/release388
---
# ART-DECOR® – Release 3.8.8

Release notes 31 January 2025

## Introduction

We are publishing a **Release 3.8.8**, with only a very few changes compared to the predecessor release. Reason for this is a set of features that were not properly promoted to the 3.8.7 release branch, so we decided to have a small release just with that set of features that were missing.

Thanks to all testers and people who submitted improvement suggestions! We are lucky to have you with us.

## Areas of New Features and Improvements

Release 3.8.8 focuses on the following scenario features. It is a frontend change only, the backend is not affected (but e.g the API also is released now as 3.8.8 with no changes).

### Scenario

#### Enhanced options for cardinality-conformance combinations in Scenario Editor

When a concept item in the Scenario Editor is newly selected, a list of pre-defined choices as short-hand notations for the cardinality-conformance combinations are offered. This list lacks of an "optional" choice. We added “0..1 Optional” to this list [AD30-1801].

<img src="../../../img/image-20241212154851029.png" alt="image-20241212154851029" style="zoom:50%;" />

Additionally we added more options to Scenario Editor Cardinality Settings under "OTHER..." to be able to express cardinalities such as 1..10 or 2..32 etc.  [AD30-1820].

<img src="../../../img/image-20241212154952154.png" alt="image-20241212154952154" style="zoom:50%;" />

## Thank you

::: note A BIG thank you!

We are always thankful for any comments and suggestions. 

Please don't hesitate to write us with suggestions for improvements or feature requests to our email address provided for this purpose: `feedback@art-decor.org`, also easily reachable through the *Services* Card at our ART-DECOR landing page (the link with the bulb :-)).

<img src="../../../img/image-20231023162540457.png" alt="image-20231023162540457" style="zoom:67%;" />

:::

## Outlook

Current development is heading towards a next Minor Release 3.8.9 quickly, planned to be published in March 2024. One of the major aspects are more features and enhancements for the Questionnaire Management. As you can see: more on that to come. Thank you for your patience. Stay tuned.
