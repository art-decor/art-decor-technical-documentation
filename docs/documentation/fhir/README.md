---
permalink: /documentation/fhir/
---

# ART-DECOR and FHIR

## Introduction

This article describes the ART-DECOR® Release 3 Support for HL7® FHIR®. The tool in its recent Release still focuses on creation and maintenance of technology-independent Datasets, dynamic processes in Scenarios and Transactions, terminologies with Value Sets, Code Systems and Concept Maps, and implementable constraint-based data exchange formats such as HL7 Templates for the Clinical Document Architecture (CDA).

ART-DECOR follows and further develop its features to create and maintain interoperability specifications mentioned above. In that sense this is the common objective with the HL7 FHIR standard. Through endpoints FHIR capabilities are offered since a decade now, with Release 3 more and sophisticated FHIR support is continously improved.

![fhirtrailer](../../img/fhirtrailer.png)

## From ART-DECOR Release 2 to Release 3

ART-DECOR Open Tools and its Expert Group spent a lot of efforts in making ART-DECOR Release 3 as complete as possible compared to our “old” Release 2, feature-wise. This task has been concluded. ART-DECOR Release 3 still uses the database as backend with improved performance (see our BLOG *[Roast the Deficit](https://art-decor.org/roast-the-deficit/)*) and a newly designed frontend was added using modern Javascript frameworks. Architectural changes allowed getting higher reliability, performance, and user interactions on concurrent work on artifacts. In November last 2022, we started the transition for our clients by offering – next to the Release 2 “entry” – also a Release 3 entry using the same underling artifact definitions of our clients, in fact using the same database.

Our Expert Group [development team](https://docs.art-decor.org/adeg/) has also grown to speed up things to bring in innovative ideas. Our [new documentation](https://docs.art-decor.org) offers conceptual and technical information and a user manual. 

## FHIR matures, ART-DECOR follows

ART-DECOR has capabilities to support FHIR since a decade now, starting with options to export artifacts into the FHIR format of its various versions through our **FHIR server endpoints**. For more (technical) information on our FHIR server endpoints see [here](https://docs.art-decor.org/documentation/fhirserver/).

**Implementation Guides** were in the scope of the tool ever since. This feature has been extended to a broader variety of formats, incl. publishing using FHIR Implementation Guide tooling . 

With Release 3 more FHIR support was introduced stepwise. Questionnaires Management became an important cornerstone, but also intuitive support of Concept Maps and enhanced terminology features such as Value Set Expansion Sets or Post Coordination Editors for SNOMED CT came into play.

One of our focuses is on **Questionnaires** that are directly derived from Dataset and Scenario definitions or can be created by our Editor in the Questionnaire Management Panel (see also https://art-decor.org/fhir-questionnaires/). As ART-DECOR is ever since a design tool for interoperability specifications, the prototyping of Questionnaires, its design and on-the-fly rendering including entering test data is offered. Once a Questionnaire is ready to publish it goes to production in data capturing applications and other systems. All Questionnaires artifacts can be exported through our **FHIR server endpoints** to FHIR Questionnaires (in a target FHIR version).

Regarding terminology we introduced the **Centralized ART-DECOR Terminology Services** (CADTS). This includes many background alignments of standard and user terminologies used in the tool but more prominently out new cross-**Terminology Browser**. Rapid searching for terms or codes cross terminologies is done at the snap of a finger and the creation of (intensional or extensional) Value Sets is easier using the browser.

The **Value Set** Support including new capabilities to create and maintain Value Sets has been improved. We offer intensional and extensional Value Set creation and maintenance with a speedy option to see the Expansion Sets in case of intensional definitions. 

New is our **Code System** Management that allows authors to craft and maintain (proprietary) Code Systems if necessary. Also, a couple of filter mechanisms are implemented for the creation of subset Value Sets. All Terminology artifacts can be exported through our **FHIR server endpoints** to FHIR terminology.

The **Concept Map** Panel gives you options to map coded concepts from one Value Set to another one with qualifying relationships. They can simply be created by drag and drop, exported through our FHIR server endpoints to FHIR Concept Maps.

As mentioned already, the **Implementation Guide** feature was introduced a while ago and meanwhile we are heading towards new options for Implementation Guides using the tool, e.g. the next iteration of the International Patient Summary (IPS). This feature offers creating text and include graphics with our usual ART-DECOR Release 3 editors. Publishing still includes the established methods but also two new options.

Since years we already have export and API mechanisms for **Naming Systems**, also in FHIR format.

For one of the next major Releases, we are working on our new **FHIR Profile Editor** to create **Structure Definitions**. It will soon offer a profiling option using the tool. It works like the well-known CDA Template Editor in ART-DECOR. This feature will include a link to Project based **Github** repositories to submit artifacts to a repository.

More information can be found in [our documentation](https://docs.art-decor.org/documentation/fhirserver/).

## FHIR R5 and beyond

In 2023, FHIR Release 5 came out that is seen more as a preview on FHIR R6 than a natural follow-up of R4 regarding implementation. Many nations have decided to use R5 as a look-ahead, continue with R4 for most real-world implementations or R4B that is compatible with R4 and closely embracing R5 concepts.

We will continue following the developments of FHIR and the tooling around. Stay tuned.
