---
permalink: /documentation/fhirserver/
---

# FHIR Server

## FHIR Version support / Pre-requisites

As of now, ART-DECOR® support FHIR server endpoints for the following versions:

3.0 (STU 3), with the endpoint

```
https://serveraddress/fhir/3.0/...
```

4.0 (R4), with the endpoint

```
https://serveraddress/fhir/4.0/...
```

The support is provided by separate Packages that need to be installed. Please refer to the FHIR server installation instructions to learn to to set up ART-DECOR a FHIR server.

## Capabilities

- Retrieve DECOR Dataset or Transaction as [FHIR Logical Models](http://hl7.org/fhir/structuredefinition.html)
- Retrieve DECOR Templates in  as [FHIR Structure Definitions](http://hl7.org/fhir/structuredefinition.html)
- Retrieve DECOR Value Sets as [FHIR Value Sets](http://hl7.org/fhir/valueset.html)
- Retrieve DECOR Transaction as  [FHIR Questionnaires](http://hl7.org/fhir/questionnaire.html)
- Retrieve OID Registry info as [FHIR Naming System](http://hl7.org/fhir/namingsystem.html)

## URI Patterns

All calls to ART-DECOR's FHIR features are to be made through FHIR version-aware endpoints.

![image-20230825204317529](../../img/image-20230825204317529.png)

All URIs are using the following construct.

FHIR System URIs are everywhere. They provide business identifiers to conformance resources like StructureDefinition, ValueSet, CodeSystem and Questionnaire. They also serve to link between these resources e.g. when supplying a value set for use in a StructureDefinition.

For objects with a `@canonicalUri` defined, this is assumed to be correct. For objects without a @canonicalUri, it is generated from the `@id` of the artefact. When the id has a uri defined in the old registry, this prevails. When it is not, the generator would come up with urn:oid:[@id].

::: example

```
https://server/fhir/4.0/public/CodeSystem/2.16.840.1.113883.3.1937.777.71.5.9
```

yields URI [fhirCanonicalBase]/[type]/[id]--[effectiveDate], e.g.

```
https://server/fhir/4.0/CodeSystem//2.16.840.1.113883.3.1937.777.71.5.9--20200928123456
```

:::

### Pattern Overview

```
https://serveraddress/fhir/[version]/[channel]/[resource]/[id]
```

### Base URI

```
https://serveraddress/fhir/
```

### Version Identifier

FHIR Version, one of the following

| FHIR Version | Identifer to use in URI |
| ------------ | ----------------------- |
| DSTU 2       | `1.0`                   |
| STU 3        | `3.0`                   |
| R 4          | `4.0`                   |
| R 4B         | `4.3`                   |
| R 5          | `5.0`                   |

### Channel

The following channels are supported

| Description                                                  | Channel Identifier                                           |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Trying to find the requested resource anywhere on the server | `public`                                                     |
| Project prefix limits scope to the requested DECOR project   | project prefix, e.g. `demo1-`                                |
| Project prefix + release version limits scope to the requested DECOR project release | project prefix + release version as digits,<br /> e.g. `demo1-20151105100336` |
| OID Registry name limits scope to the requested OID Registry | OID Registry name, e.g. `hl7org`                             |

### Resource

The [resource] you would like to retrieve, e.g.

- `Bundle`
- `CapabilityStatement`
- `ConceptMap`
- `NamingSystem`
- `Questionnaire`
- `QuestionnaireResponse`
- `StructureDefinition`
- `ValueSet`
- `CodeSystem`

### Identifier

The id for the resource you would like to retrieve. For NamingSystem, just use the OID. For other DECOR objects, use the id--effectiveDate where you strip non-digits from the effectiveDate

## Artifacts and Examples

### Dataset or Transaction

Get the FHIR Logical Models for the object from any live DECOR project that has it. Any Dataset or Transaction will work, in this case the demo1- dataset.

```
https://art-decor.org/fhir/4.0/public/StructureDefinition/2.16.840.1.113883.3.1937.99.62.3.1.1--20120530113236
```

::: note

Please note that a Dataset in DECOR has no cardinalties or conformance information. While it is illegal to omit cardinalities in FHIR Logical Models, all cardinalities are set to 0..* in case of a Dataset export. All further conformance constraints are omitted.

A transaction does have cardinalties / conformance information and this is adequately transformed into a FHIR Logical Model.

:::

**Examples**

A dataset has just the structure an no cardinalities / conformance information.

<img src="../../img/image-20241012200909466.png" alt="image-20241012200909466" style="zoom:77%;" />

As stated above the resulting FHIR Logical Model export has no conformance information and all cardinalities are set to 0..*.

<img src="../../img/image-20241012200958544.png" alt="image-20241012200958544" style="zoom:67%;" />

A transaction however may have cardinalities / conformance information.

<img src="../../img/image-20241012200933992.png" alt="image-20241012200933992" style="zoom:67%;" />

They are translated accordingly to FHIR Logical Model constructs.

![image-20241012201018121](../../img/image-20241012201018121.png)

### Template

Get the Template with id xxxxx and version yyyyy from the live DECOR demo5- project.

```
https://art-decor.org/fhir/4.0/demo5-/StructureDefinition/xxxxx/yyyyy
```

### Value Set

Get Value Set with id 2.16.840.1.113883.3.1937.99.62.3.11.5 and version 2011-07-25T15:22:56 from the live DECOR demo1- project.

```
https://art-decor.org/fhir/4.0/demo5-/ValueSet/2.16.840.1.113883.3.1937.99.62.3.11.5--20110725152256
```

Get Value Set with id 2.16.840.1.113883.3.1937.99.62.3.11.5 and version 2011-07-25T15:22:56 from the DECOR demo1- project release 2015-11-05T10:03:36.

```
https://art-decor.org/fhir/4.0/demo1-20151105100336/ValueSet/2.16.840.1.113883.3.1937.99.62.3.11.5--20110725152256
```

### Questionnaire

Get FHIR Questionnaire derived from Transaction id 2.16.840.1.113883.3.1937.99.60.10.4.2 and version 2022-02-16T00:00:00 from the live DECOR demo10- project.

```
https://art-decor.org/fhir/4.0/demo10-/Questionnaire/2.16.840.1.113883.3.1937.99.60.10.4.2--20220216000000
```

### Naming System

Get the naming system for SNOMED CT from the HL7 OID Registry.

```
https://art-decor.org/fhir/4.0/hl7org/NamingSystem/2.16.840.1.113883.6.96
```
