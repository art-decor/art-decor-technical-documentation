---
permalink: /documentation/starter/
---
# Starting with ART-DECOR

If you start ART-DECOR® the first you see is the Landing Page. The page offers you to choose a project to inspect or work with. More details are documented in the documentation about the [The Landing Page](../../documentation/landing/). 

![image-20211130161900227](../../img/image-20211130161900227.png)

Choose a project by searching or chosing one in the Project Choice Card<ref1/>and start your endeavors. Please consider to provide feedback if you like through the feedback link in the Services Card<ref2/>.

![image-20211130165044978](../../img/image-20211130165044978.png)

You can continue viewing project details and artefacts without being logged in. Of course, you cannot change anything in the project then. If you are logged you see your Welcome Card. If you haven't select any project, our *[wise owl](../../documentation/principles/#owl)* tells you what to do. 

<img src="../../img/image-20211130162143949.png" alt="image-20211130162143949" style="zoom:90%;" />

If you are logged in and a project is selected a set of Quick Links is offered to quickly go to the desired area of your project.

<img src="../../img/image-20211130162200623.png" alt="image-20211130162200623" style="zoom:90%;" />

You are typically sent to the Overview Panel of the project where you can see the name, description, some artefact statistics and many more.

![image-20211130162251250](../../img/image-20211130162251250.png)

Clicking on one of the Statistics Card (examples below) brings you to the respective Artefact Panel. 

![image-20211130163710919](../../img/image-20211130163710919.png)

You can also choose a menu item from the menu bar on the left. The menu bar offers a variety of options to go to. Here is an overview of the menu.

- Project
- Datasets
- Scenarios
- Terminology
- Rules
- Issues
