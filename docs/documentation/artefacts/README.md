---
permalink: /documentation/artefacts/

---

# Artefact Types

| Artefact code | Description                | Recommended<br />OID suffix | Recommended prefix                             |
| :-----------: | :------------------------- | :-------------------------: | :--------------------------------------------- |
| **DS**        | dataset                    | *projectid*.1               | *projectprefix*-dataset-                     |
| **DE**      | dataset concept            | *projectid*.2               | *projectprefix*-dataelement-                 |
| **SC**      | scenario                   | *projectid*.3               | *projectprefix*-scenario-                    |
| **TR**      | transaction in scenario    | *projectid*.4               | *projectprefix*-transaction-                 |
| **CS**      | code system                | *projectid*.5               | *projectprefix*-codesystem-                  |
| **IS**      | issue                      | *projectid*.6               | *projectprefix*-issue-                       |
| **AC**      | actor in scenario          | *projectid*.7               | *projectprefix*-actor-                       |
| **CL**      | concept list in dataset    | *projectid*.8               | *projectprefix*-conceptlist-                 |
| **EL**      | element in template        | *projectid*.9               | *projectprefix*-template-element-            |
| **TM**      | template                   | *projectid*.10              | *projectprefix*-template-                    |
| **VS**      | value set                  | *projectid*.11              | *projectprefix*-valueset-                    |
| **RL**      | rule for instances         | *projectid*.12              | *projectprefix*-rule-intern-                 |
| **SX**      | test/example scenario      | *projectid*.18              | *projectprefix*-test-scenario-               |
| **EX**      | test/example instance      | *projectid*.19              | *projectprefix*-example-instance-            |
| **QX**      | qualification test/example | *projectid*.20              | *projectprefix*-qualification-test-instance- |
| **CM**      | community                  | *projectid*.21              | *projectprefix*-community-                   |
| **TX**      | test/example data element  | *projectid*.22              | *projectprefix*-test-dataelement- |
|  |  | (23) | (intenionally not used) |
| **MP**      | concept map                | *projectid*.24              | *projectprefix*-map-                         |
|  |  | (25) | (intenionally not used) |
| **SD**      | structure definition       | *projectid*.26              | *projectprefix*-structuredefinition-         |
| **QQ**      | questionnaire              | *projectid*.27              | *projectprefix*-questionnaire-               |
| **QR**      | questionnaire response     | *projectid*.28              | *projectprefix*-questionnaire-response-      |
| **IG** | Implementation guide | *projectid.29* | *projectprefix*-ig- |
| **BC** | browsable codesystem | (30) | (not used in a project) |
| **VE**      | value set expansion set                 | (31)       | (not used in a project) |


::: tip PLEASE NOTE

For the recommended prefix, *projectprefix*- should be replaced with the actual project/@prefix.

In the recommended OID *projectid*.x shall be substituted according to the project OID.

:::