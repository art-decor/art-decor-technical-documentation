---
permalink: /documentation/implementationguide
---
# Implementation Guide

## Implementation Guide Panel

The Implementation Guide Panel offers a list of Implementation Guides created for the project. An Implementation Guide (IG) is a collection of artefatcs, definitions, explanations and instructions for Implementations <adimg implementation/> in applications.

<img src="../../img/image-20220915105541338.png" alt="image-20220915105541338" style="zoom:67%;" />

When opening the Implementation Guide Panel, the Navigation Card on the left <ref1/> shows the list of Implementation Guides of the Project (left tab), once a guide is selected, the pages tab shows the hierarchical list of pages of the selected IG.

<adimg implementationguide/> is the symbol used throughout the app.

## Adding a New Implementation Guide

To add a new Implementation Guide use the small menu in the Navigation Card and use the ADD button.

<img src="../../img/image-20220915105850053.png" alt="image-20220915105850053" style="zoom:33%;" />

To create selective Implementation Guides, a Transaction based filter can be defined <ref1/>. 

<img src="../../img/image-20220915110456253.png" alt="image-20220915110456253" style="zoom:50%;" />

To define a Transaction based filter the corresponding dialog shows a list of Transactions that can be selected <ref2/>. A label must be given to name the filter <ref3/>.

<img src="../../img/image-20220915110710332.png" alt="image-20220915110710332" style="zoom:50%;" />

If no filter is set, the Implementation Guide contains an index of all artefacts, if filter is active <ref4/>, only those that are related to the selective filter(s). Clicking on ADD <ref5/> triggers the creation of a blank Implementation Guide. 

<img src="../../img/image-20220915110806382.png" alt="image-20220915110806382" style="zoom:50%;" />

The new Implementation Guide immediately appears in the Implementation Guide List. In the background the selected artifacts are collected from the underlying project. Once compiled they appear in the Implementation Guide Meta Data tab under "Resources".

<img src="../../img/image-20220915110930848.png" alt="image-20220915110930848" style="zoom:50%;" />

## Implementation Guide Meta Data

When creating a new Implementation Guide, all selected artifacts based on one or more Transactions (Scenarios) are collected from the underlying project by a background process. Once compiled they appear in the Implementation Guide Meta Data tab under "Resources" <ref2/>. This might take a minute or two until complete, while other meta data of the Implementation Guide are immediatley available <ref1/>. As usual, a versionable artifact in ART-DECOR (such as an Implementation Guide) have a name/title, a unique id, a status and a Version Date (the date, the IG came into being). The Version Date might be annotated with a human readable Version label (e.g. "v1.0") <ref1/>.

<img src="../../img/image-20220915111148505.png" alt="image-20220915111148505" style="zoom:50%;" />

## Managing Implementation Guide Content

The left side of the Implementation Guide <ref1/> shows the Guides List in one Tab, by clicking on a concrete Implementation Guide you will be directed to the (hierarchical) list of Pages.

There is one "root" Page, initailly called Index Page. When selecting a page the details/contents of that page is shown on the right area of the Panel <ref3/>. You can see the name/title of the page, and a button to the very right <ref3/> that allows you to edit the page contents.

<img src="../../img/image-20220915111819795.png" alt="image-20220915111819795" style="zoom:50%;" />

### Adding New Pages

Hierarchically nested under the "root" page (Index Page initially, but it may be renamed), you can add additional pages or sub pages by using the (+) menu in the Navigation Tree of each page. It allows you to Insert a New Page (sub page) under an exsting one, or a new page before or after an existing one (on the same level).

<img src="../../img/image-20220915111647688.png" alt="image-20220915111647688" style="zoom:50%;" />

### Page Navigation

Once there are many pages that are displayed as a tree in the Navigation Tree on the left of the Panel <ref1/>. There is a Search Fields provided where you can search for title or content of the pages <ref2/>. There is an expand button to expand the whole Navigation Tree at once to see all pages and a Hamburger menu that offers you context specific further action options <ref3/>.

<img src="../../img/image-20220915111429291.png" alt="image-20220915111429291" style="zoom:50%;" />

### Moving Pages around

If you have many pages already defined in your Implementation Guide you can move single pages around in the Tree of pages by using the Hambuger menu item in the Navigation Tree on the right.

<img src="../../img/image-20220915112909562.png" alt="image-20220915112909562" style="zoom:50%;" />

For moving a page, the whole tree is shown and you can simple drag and drop the pages within the tree for any re-arrangement of sequence or hierachy. Please note that there is always a "root" page that cannot be moved.

<img src="../../img/image-20220915113004823.png" alt="image-20220915113004823" style="zoom:50%;" />

### Editing Pages

If you select a page <ref1/> and click in the EDIT button <ref3/> you will see the usual ART-DECOR text editor to edit page contents. Of course this is only possible if you a a project author and may edit artifcats of the project at all.

<img src="../../img/image-20220915111845624.png" alt="image-20220915111845624" style="zoom:50%;" />

During edit of a page SAVE can be clicked to intermediately store the page changes, or SAVE AND CLOSE to also leave the edit mode or an edit session can be cancelled.

![image-20240510164813614](../../img/image-20240510164813614.png)

### Adding Images

You can add images / figures to the page contents by drag and drop of an image or by clicking on the image icon to select an image from your local computer.

 ![image-20240510165444745](../../img/image-20240510165444745.png)

<img src="../../img/image-20240510165750772.png" alt="image-20240510165750772" style="zoom:67%;" />

### Adding Artifact References

While editing a page contents you may add a reference to an artifact into the running text or somehow exposed. To add an artifact reference click on the ADD ARTIFACT REFERENCE icon in the Editor Toolbar.

![image-20240510164953875](../../img/image-20240510164953875.png)

This opens a selection dialog with all artifacts offered as a reference in this Implementation Guide. Zou need to select the type of artifact first and then the offered specific item.

<img src="../../img/image-20240510165153732.png" alt="image-20240510165153732" style="zoom:50%;" /> 

They appear as little icons in the text editor ...

<img src="../../img/image-20240510165229247.png" alt="image-20240510165229247" style="zoom:77%;" />

... and can be changed by clicking on it (a dialog appears).

<img src="../../img/image-20240510165304707.png" alt="image-20240510165304707" style="zoom:67%;" />

## Post-Processing Implementation Guides and Publication

### Triggering a Publication

### Publication Site (old style)

### Publication Site (new style)

### FHIR Implementation Guides Techniques and Support

### Packages

<adimg package/>

