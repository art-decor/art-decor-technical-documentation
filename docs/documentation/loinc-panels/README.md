---
permalink: /documentation/loinc-panels/
---

# Working with LOINC® panels

LOINC® panels are collector terms linked to a set of discrete child elements. Examples are a laboratory battery of tests, forms or assessments related to health completed by patients and/or providers.

To facilitate the use of LOINC panels in ART-DECOR® projects a new function has been added to import panels as dataset concepts complete with all associated value sets (answer lists in LOINC). In addition to the panel items and value sets terminology associations will be created linking the dataset concepts to the original LOINC concepts and the concept lists to the imported value sets. 

The import function is located in the dataset tree menu:

![concept-inherit.png](../../img/concept-inherit.png)

Just as with creating a new concept in a dataset, a context for insert can be selected. If no concept is selected the panel will be imported after the last concept in the dataset. Selecting the ‘Import LOINC panel’ function will open the terminology browser:

![import-loinc-browser.png](../../img/import-loinc-browser.png)

The search context is set to ‘LOINC’. Use the browser to search for, or navigate to the panel to import. The selected panel and the context will be shown in the ‘Import LOINC panel’ part at the bottom of the page. Only panels can be imported this way, the class of the LOINC concept must begin with ‘PANEL’

![import-loinc-panel-selected.png](../../img/import-loinc-panel-selected.png)

Clicking the ‘Save’ button will open a dialog asking for confirmation of the import:

![confirm-import.png](../../img/confirm-import.png)

The dialog shows the number of concepts, value sets and terminology associations that will be created. Clicking the ‘Save’ button will perform the import and return focus to the dataset. Just like other concepts the properties can be edited and translations can be added.

![panel-in-dataset.png](../../img/panel-in-dataset.png)

If additional panels are imported into the project only new concepts and value sets will be imported, concepts already in the dataset will be inherited:

![concept-inherit.png](../../img/concept-inherit.png)

Value sets are imported into the project to allow for editing of properties, especially translation of display names. The order of items in the concept list is by sequence number as specified in the LOINC answer list. Ordinals (score) are included with the concept list items.

![imported-valueset.png](../../img/imported-valueset.png)

The Terminology Report function can be used to check if any imported items are affected by changes in LOINC if a new version of LOINC is released. 

::: note LOINC answer list order and intensional value sets

When intensional value sets are expanded the order of the concepts is determined by the way they appear in the database. This means that LOINC answer lists may not be expanded in the correct order as specified by LOINC. If the order of concepts in a value set is important this value set should by defined extensional.

When using the LOINC panel import function all value sets for answer lists will be created as extensional sets and will be in the correct order.

:::
