---
permalink: /documentation/governancegroups/
---
# Dealing with Governance Groups

## Concept of a Governance Group

Designers of templates, value sets and other artifacts often create their designs within the context of a community. Often these communities are involved with standards creation, like HL7 and IHE, but they don’t have to be. 

A Governance Group establishes the rules and best practices associated with their creation and management of templates, value sets and other artifacts in ART-DECOR. The group establishes practices that are used to manage the intellectual property rights associated with the artifacts they create and use, and provide for governance functions. Some Governance Groups develop artifacts with the intent to register and share their designs. Others intent to maintain their template definitions as private works. 

A Governance Group is the custodian of a set of artifacts and can act in any of the three (stereotype) roles: Creator, Adopter, and Adapter.

They use ART-DECOR as a Repository to hold the artifacts designs it creates and uses. It maintains the designs it develops and uses, and tracks their status and relationships. It set the policies and procedures it will follow when using, adding, and addressing the life cycle of each design under its governance.

## How to host a Governance Group on a server

As a ART-DECOR server admin you may add (=host) a new Governance Group by simply adding the corresponding information. In the menu item "Application" choose "Governance Groups" which leads to the list of Governance Groups hosted so far. A click on the tab "New hosted Governance Group" leads to the following input dialog.

[![Governancegroupnew.jpg](../../img/750px-Governancegroupnew.jpg)](https://art-decor.org/mediawiki/index.php?title=File:Governancegroupnew.jpg)

After completing the input fields and saving it, the new hosted governance group appears in the list under Governance Groups.

## How a Project becomes a member of a Governance Group

A project admin may add his project to one or more Governance Groups. In the project menu click on the "Governance Group" menu item which leads to a list of available Governance Groups for that project.

[![Governancegroupprojectlist.jpg](../../img/750px-Governancegroupprojectlist.jpg)](https://art-decor.org/mediawiki/index.php?title=File:Governancegroupprojectlist.jpg)

Clicking on the plus sign links the project to a Governance Group, clicking on the delete icon unlinks it from the Governance Group.

## ART-DECOR Menu appearance

Once a Governance Group has set up and projects are assigned to it the Governance Group gets a top-level menu item with the name of the group and a sub menu containing all projects belonging to that Governance Group.

Example:

[![Governancegroupmenu.jpg](../../img/Governancegroupmenu.jpg)](https://art-decor.org/mediawiki/index.php?title=File:Governancegroupmenu.jpg)

Projects that are part of one or more Governance Groups appear as a menu item within that Governance Group(s) only and not any more as single top level menu items.

## ART-DECOR Governance Group Summaries

The Governance Group Detail page has also two tabs that summarizes the template and value set artifacts under the respective Governance Group.

Example:

[![Governancegrouptemplatelist.jpg](../../img/750px-Governancegrouptemplatelist.jpg)](https://art-decor.org/mediawiki/index.php?title=File:Governancegrouptemplatelist.jpg)

## Governance Group Homepage

Clicking on the Governance Group menu item on top level he is redirected to the Governance Group homepage. Here the description and a list of associated projects are shown.

Example:

[![Governancegrouphomepage.jpg](../../img/750px-Governancegrouphomepage.jpg)](https://art-decor.org/mediawiki/index.php?title=File:Governancegrouphomepage.jpg)

For future use: Also a list of the overall template and value set id’s is available in this homepage form. Some more support for Governance Groups may be useful as well.



ADMIN STUFF

## DECOR objects created

### hosted-governance-groups.xml

The Governance Group definitions are stored in file `art-data/hosted-governance-groups.xml`. It has the following structure:

```
<governance xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:noNamespaceSchemaLocation="http://art-decor.org/ADAR/rv/DECOR-governance-groups.xsd">
   <group id="..." defaultLanguage="...">
      <name language=""></name>
      <desc language=""></desc>
      <copyright>
         <addrLine>....</addrLine>
      </copyright>
   </group>
</governance>
```

### governance-group-links.xml

The Governance Group links to be established between a project and a specific Governance Group are stored in the project collection in a file named `governance-group-links.xml`. It has the following structure:

```
<governance-group-links projectId="...">
  <partOf governanceId="..."/>
</governance-group-links>
```
