---
permalink: /documentation/datatypes/DTr1_CO.EPSOS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CO.EPSOS Coded Ordinal - Flavor epSOS

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CO.EPSOS** |<flag country="EU"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |


