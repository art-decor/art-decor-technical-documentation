---
permalink: /documentation/datatypes/DTr1_CE.EPSOS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CE.EPSOS Coded with Equivalents - Flavor epSOS

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CE.EPSOS** |<flag country="EU"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |
|`hl7:translation` | 0 .. * | [CD](../DTr1_CD) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CE.EPSOS

Extends Rule(s): [CD.EPSOS](../DTr1_CD.EPSOS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| cannot have qualifier|dtr1‑1‑CE.EPSOS|


