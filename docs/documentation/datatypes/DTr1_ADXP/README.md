---
permalink: /documentation/datatypes/DTr1_ADXP/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# ADXP Address Part


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **ADXP** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@partType` |  optional  | cs|
|`@mediaType` |  optional  | cs|
|`@language` |  optional  | cs|
|`@compression` ( DF \| GZ \| Z \| ZL ) |  optional  | cs|
|`@integrityCheck` |  optional  | bin|
|`@integrityCheckAlgorithm` ( SHA-1 \| SHA-256 ) |  optional  | cs|
|`@representation` ( B64 \| TXT ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:reference` | 0 .. 1 | [TEL](../DTr1_TEL) |
|`hl7:thumbnail` | 0 .. 1 | [thumbnail](../DTr1_thumbnail) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** ADXP

Extends Rule(s): [ST](../DTr1_ST) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-002] `<name/>` (*$value*) SHALL be distinct|dtr1‑1‑ADXP|


