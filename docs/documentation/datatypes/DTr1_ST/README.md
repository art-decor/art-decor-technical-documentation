---
permalink: /documentation/datatypes/DTr1_ST/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# ST Character String


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **ST** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@mediaType` |  optional  | cs|
|`@language` |  optional  | cs|
|`@representation` |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** ST

Extends Rule(s): [ED](../DTr1_ED) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| no reference|dtr1‑3‑ST|
|<error/>| no thumbnail|dtr1‑4‑ST|
|<error/>| @representation SHALL be 'TXT' if present.|dtr1‑5‑ST|
|<error/>| @mediaType SHALL be 'text/plain' if present.|dtr1‑6‑ST|
|<error/>| @compression SHALL NOT be used on ST.|dtr1‑7‑ST|
|<error/>| @integrityCheck SHALL NOT be used on ST.|dtr1‑8‑ST|
|<error/>| @integrityCheckAlgorithm SHALL NOT be used on ST.|dtr1‑9‑ST|


