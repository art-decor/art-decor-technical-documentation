---
permalink: /documentation/datatypes/DTr1_CD/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CD Concept Descriptor


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CD** ||[CO.EPSOS](../DTr1_CO.EPSOS) [CV.EPSOS](../DTr1_CV.EPSOS) [CV.IPS](../DTr1_CV.IPS) [CE.EPSOS](../DTr1_CE.EPSOS) [CE.IPS](../DTr1_CE.IPS) [CD.EPSOS](../DTr1_CD.EPSOS) [CD.IPS](../DTr1_CD.IPS) [CD.SDTC](../DTr1_CD.SDTC) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |
|`hl7:qualifier` | 0 .. * | [CR](../DTr1_CR) |
|`hl7:translation` | 0 .. * | [CD](../DTr1_CD) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CD

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or code and/or originalText|dtr1‑1‑CD|
|<error/>| code requires codeSystem|dtr1‑2‑CD|
|<error/>| codeSystemName only if codeSystem|dtr1‑3‑CD|
|<error/>| codeSystemVersion only if codeSystem|dtr1‑4‑CD|
|<error/>| displayName only if code|dtr1‑5‑CD|
|<error/>| co-occurence violation. Cannot have code and null|dtr1‑6‑CD|
|<error/>| co-occurence violation. Cannot have displayName and null|dtr1‑7‑CD|
|<error/>| no null on translations (Exceptional values (NULL-values) can not be elements of a set.)|dtr1‑10‑CD|
|<error/>| translation code requires codeSystem|dtr1‑11‑CD|
|<error/>| translation codeSystemName only if codeSystem|dtr1‑12‑CD|
|<error/>| translation codeSystemVersion only if codeSystem|dtr1‑13‑CD|
|<error/>| translation displayName only if code|dtr1‑14‑CD|
|<warning/>| it looks like there are formatting instructions in attribute @displayName. Please note that this may lead to interoperability problems.|dtr1‑15‑CD|
|<error/>| originalText/@language SHALL conform to RFC 3066. This usually has format sss?-CC with sss? for language code (ISO-639-1 / ISO-639-2) and optional CC for country code (conform ISO-3166 alpha-2)|dtr1‑20‑CD|

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-002] *$name* (@code&#061;*$code* @codeSystem&#061;*$codeSystem*) SHALL be distinct|dtr1‑18‑CD|
|<error/>| [RIM-002] translations SHALL be distinct|dtr1‑19‑CD|


