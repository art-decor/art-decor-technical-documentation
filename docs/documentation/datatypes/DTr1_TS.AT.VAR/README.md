---
permalink: /documentation/datatypes/DTr1_TS.AT.VAR/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.AT.VAR Time Stamp - Flavor Date

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.AT.VAR** |<flag country="AT"/> ||

*SHALL be precise to the year (YYYY), to the month (YYYYMM), to the day (YYYYMMDD) or precise to the second (YYYYMMDDhhmmss) without milliseconds.*

*SHALL include a time zone if more precise to the day (YYYYMMDDhhmmss[+/-]HHMM)*


## Operationalization
Precise to the year, year and month or date -or- precise to the second and timezone required, no milliseconds allowed
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.AT.VAR

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time if specified SHALL be precise to the year (YYYY), to the month (YYYYMM), to the day (YYYYMMDD) or precise to the second (YYYYMMDDhhmmss) without milliseconds. Found *local-name()* "*@value*"|dtr1‑1‑TS.AT.TZ|
|<error/>| time SHALL include a time zone if more precise to the day (YYYYMMDDhhmmss[+/-]HHMM). Found *local-name()* "*@value*"|dtr1‑2‑TS.AT.TZ|


