---
permalink: /documentation/datatypes/DTr1_IVL_TS.IPS.TZ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# IVL_TS.IPS.TZ Interval of Time Stamp - Flavor IPS IVL TS TZ

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **IVL_TS.IPS.TZ** |UV||

*Interval of Timestamp. SHALL be precise to the day, SHALL include a time zone if more precise than to the day, and SHOULD be precise to the second.*


## Operationalization
Timezone required
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:low` | 0 .. 1 | [IVXB_TS](../DTr1_IVXB_TS) |
|`hl7:center` | 0 .. 1 | [TS](../DTr1_TS) |
|`hl7:width` | 0 .. 1 | [PQ](../DTr1_PQ) |
|`hl7:high` | 0 .. 1 | [IVXB_TS](../DTr1_IVXB_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** IVL_TS.IPS.TZ

Extends Rule(s): [IVL_TS](../DTr1_IVL_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time SHALL be precise to the day|dtr1‑1‑IVL_TS.IPS.TZ|
|<warning/>| time SHOULD be precise to the second|dtr1‑2‑IVL_TS.IPS.TZ|
|<error/>| time SHALL include a time zone if more precise than to the day|dtr1‑3‑IVL_TS.IPS.TZ|
|<error/>| low boundary SHALL be precise to the day|dtr1‑4‑IVL_TS.IPS.TZ|
|<warning/>| low boundary SHOULD be precise to the second|dtr1‑5‑IVL_TS.IPS.TZ|
|<error/>| low boundary SHALL include a time zone if more precise than to the day|dtr1‑6‑IVL_TS.IPS.TZ|
|<error/>| center value SHALL be precise to the day|dtr1‑7‑IVL_TS.IPS.TZ|
|<warning/>| center value SHOULD be precise to the second|dtr1‑8‑IVL_TS.IPS.TZ|
|<error/>| center value SHALL include a time zone if more precise than to the day|dtr1‑9‑IVL_TS.IPS.TZ|
|<error/>| high boundary SHALL be precise to the day|dtr1‑10‑IVL_TS.IPS.TZ|
|<warning/>| high boundary SHOULD be precise to the second|dtr1‑11‑IVL_TS.IPS.TZ|
|<error/>| high boundary SHALL include a time zone if more precise than to the day|dtr1‑12‑IVL_TS.IPS.TZ|


