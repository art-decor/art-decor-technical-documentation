---
permalink: /documentation/datatypes/DTr1_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS Time Stamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS** ||[IVL_TS.EPSOS.TZ](../DTr1_IVL_TS.EPSOS.TZ) [IVL_TS.IPS.TZ](../DTr1_IVL_TS.IPS.TZ) [IVL_TS.EPSOS.TZ.OPT](../DTr1_IVL_TS.EPSOS.TZ.OPT) [IVL_TS.CH.TZ](../DTr1_IVL_TS.CH.TZ) [TS.DATE](../DTr1_TS.DATE) [TS.DATE.FULL](../DTr1_TS.DATE.FULL) [TS.DATE.MIN](../DTr1_TS.DATE.MIN) [TS.DATETIME.MIN](../DTr1_TS.DATETIME.MIN) [TS.DATETIMETZ.MIN](../DTr1_TS.DATETIMETZ.MIN) [TS.EPSOS.TZ](../DTr1_TS.EPSOS.TZ) [TS.IPS.TZ](../DTr1_TS.IPS.TZ) [TS.EPSOS.TZ.OPT](../DTr1_TS.EPSOS.TZ.OPT) [TS.CH.TZ](../DTr1_TS.CH.TZ) [TS.AT.TZ](../DTr1_TS.AT.TZ) [TS.AT.VAR](../DTr1_TS.AT.VAR) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS

Extends Rule(s): [QTY](../DTr1_QTY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value or child element in case of extension|dtr1‑1‑TS|
|<error/>| *local-name()* "*$theTS*" is not a valid timestamp.|dtr1‑2‑TS|


