---
permalink: /documentation/datatypes/DTr1_CO/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CO Coded Ordinal


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CO** ||[CO.EPSOS](../DTr1_CO.EPSOS) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CO

Extends Rule(s): [CV](../DTr1_CV) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| cannot have translation|dtr1‑1‑CO|


