---
permalink: /documentation/datatypes/DTr1_RTO_QTY_QTY/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# RTO_QTY_QTY Ratio Quantity / Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **RTO_QTY_QTY** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@nullFlavor` |  optional  | cs|
|`hl7:numerator` | 1 .. 1 | [QTY](../DTr1_QTY) |
|`hl7:denominator` | 1 .. 1 | [QTY](../DTr1_QTY) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** RTO_QTY_QTY

Extends Rule(s): [QTY](../DTr1_QTY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| numerator and denominator required|dtr1‑1‑RTO_QTY_QTY|
|<error/>| no updateMode on numerator or denominator|dtr1‑2‑RTO_QTY_QTY|
|<error/>| no uncertainty|dtr1‑3‑RTO_QTY_QTY|
|<error/>| The denominator must not be zero.|dtr1‑4‑RTO_QTY_QTY|


