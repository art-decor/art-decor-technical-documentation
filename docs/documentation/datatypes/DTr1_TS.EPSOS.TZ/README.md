---
permalink: /documentation/datatypes/DTr1_TS.EPSOS.TZ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.EPSOS.TZ Time Stamp - Flavor epSOS

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.EPSOS.TZ** |<flag country="EU"/> ||

*SHALL be precise to the day, SHALL include a time zone if more precise than to the day, and SHOULD be precise to the second.*


## Operationalization
Timezone required
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.EPSOS.TZ

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time SHALL be precise to the day|dtr1‑1‑TS.EPSOS.TZ|
|<warning/>| time SHOULD be precise to the second|dtr1‑2‑TS.EPSOS.TZ|
|<error/>| time SHALL include a time zone if more precise than to the day|dtr1‑3‑TS.EPSOS.TZ|


