---
permalink: /documentation/datatypes/DTr1_INT/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# INT Integer


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **INT** ||[INT.NONNEG](../DTr1_INT.NONNEG) [INT.POS](../DTr1_INT.POS) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | int|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** INT

Extends Rule(s): [QTY](../DTr1_QTY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value or child element in case of extension|dtr1‑1‑INT|
|<error/>| no uncertainty|dtr1‑2‑INT|


