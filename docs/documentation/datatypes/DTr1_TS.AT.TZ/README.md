---
permalink: /documentation/datatypes/DTr1_TS.AT.TZ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.AT.TZ Time Stamp - Flavor Date

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.AT.TZ** |<flag country="AT"/> ||

*SHALL be precise to the day (YYYYMMDD) or SHALL be precise to the second (YYYYMMDDhhmmss) without milliseconds.*

*SHALL include a time zone if more precise to the second (YYYYMMDDhhmmss[+/-]HHMM)*


## Operationalization
Precise to the date -or- precise to the second and timezone required, no milliseconds allowed
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.AT.TZ

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time if specified SHALL be precise to the day (YYYYMMDD) or SHALL be precise to the second (YYYYMMDDhhmmss) without milliseconds. Found *local-name()* "*@value*"|dtr1‑1‑TS.AT.TZ|
|<error/>| time SHALL include a time zone if more precise to the second (YYYYMMDDhhmmss[+/-]HHMM). Found *local-name()* "*@value*"|dtr1‑2‑TS.AT.TZ|


