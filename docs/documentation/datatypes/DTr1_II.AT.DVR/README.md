---
permalink: /documentation/datatypes/DTr1_II.AT.DVR/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# II.AT.DVR Instance Identifier - Flavor Austrian DVR

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **II.AT.DVR** |<flag country="AT"/> ||

*II Flavor .AT Austria .DVR Datenverarbeitungsregister-Nummer (DVR) Österreich, fixed OID 1.2.40.0.10.2.0.2.1*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@root` |  optional  | uid|
|`@extension` |  optional  | st|
|`@assigningAuthorityName` |  optional  | st|
|`@displayable` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** II.AT.DVR

Extends Rule(s): [II](../DTr1_II) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| @root SHALL be 1.2.40.0.10.2.0.2.1 if not null|dtr1‑1‑II.AT.DVR|


