---
permalink: /documentation/datatypes/DTr1_PQR/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# PQR Physical Quantity Representation


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **PQR** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** PQR

Extends Rule(s): [CV](../DTr1_CV) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not null and value|dtr1‑1‑PQR|


