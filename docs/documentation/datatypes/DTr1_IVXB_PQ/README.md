---
permalink: /documentation/datatypes/DTr1_IVXB_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# IVXB_PQ Interval Boundary of Physical Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **IVXB_PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@unit` |  optional  | cs|
|`@inclusive` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|
|`hl7:translation` | 0 .. * | [PQR](../DTr1_PQR) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** IVXB_PQ

Extends Rule(s): [PQ](../DTr1_PQ) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not inclusive if null|dtr1‑1‑IVXB_PQ|


