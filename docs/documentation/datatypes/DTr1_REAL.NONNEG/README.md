---
permalink: /documentation/datatypes/DTr1_REAL.NONNEG/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# REAL.NONNEG Real - Flavor Real, non-negative

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **REAL.NONNEG** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** REAL.NONNEG

Extends Rule(s): [REAL](../DTr1_REAL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value &gt;&#061; 0|dtr1‑1‑REAL.NONNEG|


