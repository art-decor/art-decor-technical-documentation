---
permalink: /documentation/datatypes/DTr1_TN/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TN Trivial Name


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TN** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@nullFlavor` |  optional  | cs|
|`validTime` | 0 .. 1 | [IVL_TS](../DTr1_IVL_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TN

Extends Rule(s): [EN](../DTr1_EN) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| trivial names SHALL consist of only one name part without any name part type or qualifier|dtr1‑1‑TN|


