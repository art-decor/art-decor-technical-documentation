---
permalink: /documentation/datatypes/DTr1_II.NL.BIG/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# II.NL.BIG Instance Identifier - Flavor Dutch BIG

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **II.NL.BIG** |<flag country="NL"/> ||

*II Flavor .NL The Netherlands .BIG beroepen in de individuele gezondheidszorg (BIG) Nederland, fixed OID 2.16.528.1.1007.5.1*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@root` |  optional  | oid|
|`@extension` |  optional  | st|
|`@assigningAuthorityName` |  optional  | st|
|`@displayable` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** II.NL.BIG

Extends Rule(s): [II](../DTr1_II) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| BIG-nummer @root MOET 2.16.528.1.1007.5.1 zijn indien niet null. Gevonden: "*@root*"|dtr1‑1‑II.NL.BIG|


