---
permalink: /documentation/datatypes/DTr1_CS.LANG/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CS.LANG Coded Simple Value - Flavor Language Codes

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CS.LANG** |UV||


## Operationalization
The language is compliant to RFC 3066 (Tags for the Identification of Languages ). The format for the language to describe is ss [-CC] where ss stands for the language tag, according to ISO 639-1 and CC for the optional country code, ISO-3166 alpha-2. The resulting language indicator is not case sensitive.
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CS.LANG

Extends Rule(s): [CS](../DTr1_CS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| @code SHALL conform to RFC 3066. This usually has format sss?-CC with sss? for language code (ISO-639-1 / ISO-639-2) and optional CC for country code (conform ISO-3166 alpha-2)|dtr1‑2‑CS.LANG|


