---
permalink: /documentation/datatypes/DTr1_SXCM_MO/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SXCM_MO Set Component of Monetary Amount


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SXCM_MO** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@currency` |  optional  | cs|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** SXCM_MO

Extends Rule(s): [MO](../DTr1_MO) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not operator if null|dtr1‑1‑SXCM_MO|


