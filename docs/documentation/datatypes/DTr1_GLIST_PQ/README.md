---
permalink: /documentation/datatypes/DTr1_GLIST_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# GLIST_PQ Sampled Sequence of Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **GLIST_PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@period` |  optional  | int|
|`@denominator` |  optional  | int|
|`head` | 1 .. 1 | [PQ](../DTr1_PQ) |
|`increment` | 1 .. 1 | [PQ](../DTr1_PQ) |


