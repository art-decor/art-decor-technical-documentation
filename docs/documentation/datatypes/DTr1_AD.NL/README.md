---
permalink: /documentation/datatypes/DTr1_AD.NL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# AD.NL Address - Flavor Dutch Address

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **AD.NL** |<flag country="NL"/> ||

*Implementatiehandleiding HL7v3 Basiscomponenten, v2.2 - Datatype 1.0 AD - Address NL*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@isNotOrdered` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|
|`hl7:delimiter` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:country` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:state` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:county` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:city` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postalCode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumber` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumberNumeric` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:direction` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetName` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameBase` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:additionalLocator` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitID` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:careOf` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:censusTract` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:buildingNumberSuffix` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:carrier` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:addressKey` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:desc` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:useablePeriod` |  | [SXCM_TS](../DTr1_SXCM_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** AD.NL

Extends Rule(s): [AD](../DTr1_AD) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| postcodes onder codeSystem '2.16.840.1.113883.2.4.4.15' moeten voldoen aan '9999AA'|dtr1‑1‑AD.NL|
|<error/>| Nederlandse postcodes in de text-nodes moeten voldoen aan '9999 AA'|dtr1‑2‑AD.NL|
|<error/>| binnen Nederland mogen alleen delimiter, country, county, city, postalCode, houseNumber, buildingNumberSuffix, streetName, additionalLocator, unitID en useablePeriod worden gebruikt. Gevonden '*$illegalAddressPart*'.|dtr1‑3‑AD.NL|
|<error/>| alleen postalCode, country en county kunnen gecodeerd worden. Gevonden '*$illegalCodedAddressPart*'|dtr1‑4‑AD.NL|


