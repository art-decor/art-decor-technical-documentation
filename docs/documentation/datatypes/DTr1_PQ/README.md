---
permalink: /documentation/datatypes/DTr1_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# PQ Physical Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@unit` |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:translation` | 0 .. * | [PQR](../DTr1_PQR) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** PQ

Extends Rule(s): [QTY](../DTr1_QTY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value or child element in case of extension|dtr1‑1‑PQ|

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-002] translations SHALL be distinct|dtr1‑3‑PQ|


