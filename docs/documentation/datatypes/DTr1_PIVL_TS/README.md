---
permalink: /documentation/datatypes/DTr1_PIVL_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# PIVL_TS Periodic Interval of Timezone


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **PIVL_TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@alignment` ( CD \| CH \| CM \| CN \| CS \| CW \| CY \| D \| DM \| DW \| DY \| H \| HD \| J \| M \| MY \| N \| NH \| S \| SN \| W \| WY \| Y ) |  optional  | cs|
|`@institutionSpecified` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|
|`phase` | 0 .. 1 | [IVL_TS](../DTr1_IVL_TS) |
|`period` | 0 .. 1 | [PQ](../DTr1_PQ) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** PIVL_TS

Extends Rule(s): [SXCM_TS](../DTr1_SXCM_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| Datatype PIVL_TS: no updateMode on PIVL attributes|dtr1‑1‑PIVL_TS|
|<error/>| null violation. Cannot have @nullFlavor and @value or child elements, or the other way round|dtr1‑2‑PIVL_TS|
|<error/>| co-occurence violation. Cannot have @value and other child elements, or missing @value and child elements with data, or center element with other elements, or width element         with both low and high elements|dtr1‑3‑PIVL_TS|
|<error/>| period/@value must be rounded to 4 digits or less|dtr1‑5‑PIVL_TS|
|<error/>| width element: no unit without value|dtr1‑6‑PIVL_TS|
|<error/>| width element: no translation|dtr1‑7‑PIVL_TS|


