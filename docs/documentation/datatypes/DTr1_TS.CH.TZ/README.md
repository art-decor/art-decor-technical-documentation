---
permalink: /documentation/datatypes/DTr1_TS.CH.TZ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.CH.TZ Time Stamp - Flavor Date

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.CH.TZ** |<flag country="CH"/> ||

*SHALL be precise to the day, SHALL include a time zone if more precise than to the day*


## Operationalization
Timezone required
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.CH.TZ

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time SHALL include a time zone if more precise than to the day|dtr1‑1‑TS.CH.TZ|


