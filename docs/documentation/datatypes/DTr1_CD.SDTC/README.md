---
permalink: /documentation/datatypes/DTr1_CD.SDTC/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CD.SDTC Concept Descriptor - Flavor Structured Documents Technical Committee

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CD.SDTC** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`@sdtc:valueSet` |  optional  | oid|
|`@sdtc:valueSetVersion` |  optional  | st|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |
|`hl7:qualifier` | 0 .. * | [CR](../DTr1_CR) |
|`hl7:translation` | 0 .. * | [CD](../DTr1_CD) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CD.SDTC

Extends Rule(s): [CD](../DTr1_CD) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| attribute @displayName is required on all codes and translations thereof|dtr1‑1‑CD.SDTC|


