---
permalink: /documentation/datatypes/DTr1_ON/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# ON Organization Name


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **ON** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@nullFlavor` |  optional  | cs|
|`delimiter` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`prefix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`suffix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`validTime` | 0 .. 1 | [IVL_TS](../DTr1_IVL_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** ON

Extends Rule(s): [EN](../DTr1_EN) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| no parts may be person name type particles|dtr1‑1‑ON|
|<error/>| organization names SHALL be element content|dtr1‑2‑ON|


