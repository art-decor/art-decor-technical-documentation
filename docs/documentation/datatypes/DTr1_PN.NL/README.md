---
permalink: /documentation/datatypes/DTr1_PN.NL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# PN.NL Person Name - Flavor Dutch Person Name

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **PN.NL** |<flag country="NL"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@nullFlavor` |  optional  | cs|
|`delimiter` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`family` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`given` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`prefix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`suffix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`validTime` | 0 .. 1 | [IVL_TS](../DTr1_IVL_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** PN.NL

Extends Rule(s): [PN](../DTr1_PN) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| Person names SHALL NOT contain a name part qualified with 'LS' (Legal status for organizations)|dtr1‑1‑PN|
|<error/>| als given/@qualifier 'IN' bevat dan moeten er initialen gescheiden en gevolgd door een punt in het element given staan.|Datatype PN|
|<error/>| given/@qualifier 'IN' en 'CL' gaan niet samen. Initalen zijn geen roepnaam|Datatype PN|
|<error/>| als given/@qualifier 'CL' bevat dan moeten er voornamen in dit element staan. Voornamen hebben geen puntjes.|Datatype PN|
|<warning/>| als er een spatie tussen een voorvoegsel en de bijbehorende achternaam hoort, moet het prefix element met qualifier&#061;VV ook eindigen met een spatie.|Datatype PN|
|<warning/>| voorvoegsels moeten in het prefix element worden gevoerd, tenzij het bronsysteem voorvoegsels en achternaam niet gescheiden vastlegt.|Datatype PN|


