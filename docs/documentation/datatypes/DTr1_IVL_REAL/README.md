---
permalink: /documentation/datatypes/DTr1_IVL_REAL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# IVL_REAL Interval of Real


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **IVL_REAL** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:low` | 0 .. 1 | [IVXB_REAL](../DTr1_IVXB_REAL) |
|`hl7:center` | 0 .. 1 | [REAL](../DTr1_REAL) |
|`hl7:width` | 0 .. 1 | [PQ](../DTr1_PQ) |
|`hl7:high` | 0 .. 1 | [IVXB_REAL](../DTr1_IVXB_REAL) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** IVL_REAL

Extends Rule(s): [SXCM_REAL](../DTr1_SXCM_REAL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null violation. Cannot have @nullFlavor and @value or other child elements|dtr1‑2‑IVL_REAL|
|<error/>| co-occurence violation. Cannot have @value and other child elements|dtr1‑3‑IVL_REAL|
|<error/>| co-occurence violation. Cannot have @value and child elements|dtr1‑4‑2‑IVL_REAL|
|<error/>| co-occurence violation. Cannot have center and other elements|dtr1‑4‑3‑IVL_REAL|
|<error/>| co-occurence violation. Cannot have width and have both low and high elements|dtr1‑4‑4‑IVL_REAL|
|<error/>| no updateMode on IVL attributes|dtr1‑5‑IVL_REAL|
|<error/>| width element: no unit without value|dtr1‑1‑PQR|
|<error/>| width element: no translation|dtr1‑2‑PQR|
|<error/>| low/@value must be lower than or equal to high/@value|dtr1‑7‑IVL_REAL|
|<error/>| low must be lower than or equal to high. Found low boundary PINF (Positive Infinity)|dtr1‑7‑1‑IVL_REAL|
|<error/>| low must be lower than or equal to high. Found high boundary NINF (Negative Infinity)|dtr1‑7‑2‑IVL_REAL|


