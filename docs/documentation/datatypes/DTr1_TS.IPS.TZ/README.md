---
permalink: /documentation/datatypes/DTr1_TS.IPS.TZ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.IPS.TZ Time Stamp - Flavor epSOS

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.IPS.TZ** |UV||

*SHALL be precise to the day, SHALL include a time zone if more precise than to the day, and SHOULD be precise to the second.*


## Operationalization
Timezone required
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.IPS.TZ

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time SHALL be precise to the day|dtr1‑1‑TS.IPS.TZ|
|<warning/>| time SHOULD be precise to the second|dtr1‑2‑TS.IPS.TZ|
|<error/>| time SHALL include a time zone if more precise than to the day|dtr1‑3‑TS.IPS.TZ|


