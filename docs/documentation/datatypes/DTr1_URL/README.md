---
permalink: /documentation/datatypes/DTr1_URL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# URL Universal Resource Locator


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **URL** ||[TEL.AT](../DTr1_TEL.AT) [TEL.EPSOS](../DTr1_TEL.EPSOS) [TEL.IPS](../DTr1_TEL.IPS) [TEL.NL.EXTENDED](../DTr1_TEL.NL.EXTENDED) [TEL.CA.EMAIL](../DTr1_TEL.CA.EMAIL) [TEL.CA.PHONE](../DTr1_TEL.CA.PHONE) [URL.NL.EXTENDED](../DTr1_URL.NL.EXTENDED) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** URL

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| elements of type URL SHALL have a @value attribute.|dtr1‑1‑URL|
|<error/>| @value must be a valid URI, e.g. '*iri-to-uri(@value)*'.|dtr1‑2‑URL|

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-001] *$name* SHALL NOT have nullFlavor, if there are other *$name* elements which are not null|dtr1‑4‑URL|
|<error/>| [RIM-002] *$name* (*$value*) SHALL be distinct|dtr1‑5‑URL|


