---
permalink: /documentation/datatypes/DTr1_EIVL.event/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# EIVL.event Event-Related Interval of Time


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **EIVL.event** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` ( AC \| ACD \| ACM \| ACV \| C \| CD \| CM \| CV \| HS \| IC \| ICD \| ICM \| ICV \| PC \| PCD \| PCM \| PCV \| WAKE ) |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|


