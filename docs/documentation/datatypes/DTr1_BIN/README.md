---
permalink: /documentation/datatypes/DTr1_BIN/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# BIN Binary Data


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **BIN** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@representation` ( B64 \| TXT ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** BIN

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| there must be a nullFlavor, or content must be non-empty|dtr1‑1‑BIN|


