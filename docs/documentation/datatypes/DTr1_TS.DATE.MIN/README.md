---
permalink: /documentation/datatypes/DTr1_TS.DATE.MIN/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.DATE.MIN Time Stamp - Flavor Date

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.DATE.MIN** |UV||

*TS Flavor .DATE.MIN, shall be at least YYYYMMDD*


## Operationalization
Shall be at least YYYYMMDD
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.DATE.MIN

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or date precision of time stamp shall be at least YYYYMMDD.|dtr1‑1‑TS.DATE.MIN|


