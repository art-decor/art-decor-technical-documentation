---
permalink: /documentation/datatypes/DTr1_CR/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CR Concept Role


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CR** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@inverted` |  optional  | [bn](../DTr1_bn) |
|`@nullFlavor` |  optional  | cs|
|`hl7:name` | 0 .. 1 | [CV](../DTr1_CV) |
|`hl7:value` | 0 .. 1 | [CD](../DTr1_CD) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CR

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value|dtr1‑1‑CR|


