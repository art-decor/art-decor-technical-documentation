---
permalink: /documentation/datatypes/DTr1_TS.DATETIMETZ.MIN/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.DATETIMETZ.MIN Time Stamp - Flavor Date

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.DATETIMETZ.MIN** |UV||

*TS Flavor .DATETIMETZ.MIN, shall be at least YYYYMMDDhhmmss[+-]nnnn*


## Operationalization
Shall be at least YYYYMMDDhhmmss[+-]nnnn
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.DATETIMETZ.MIN

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or date precision of time stamp shall be at least YYYYMMDDhhmmss[+-]nnnn where [+-]nnnn is the timezone.|dtr1‑1‑TS.DATETIMETZ.MIN|


