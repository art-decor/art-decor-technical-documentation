---
permalink: /documentation/datatypes/DTr1_AD.CA.BASIC/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# AD.CA.BASIC Address - Flavor Canadian Address - supported address parts (part types) are country, city, state, postalCode, addressLine and up to four delimiter - address parts and delimiter-separated text has each a maximum length of 80 characters

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **AD.CA.BASIC** |<flag country="CA"/> |[AD.CA](../DTr1_AD.CA) |

*AD.CA.BASIC Basic Address (CA constraints)*

*Used to communicate addresses for simple display, mailing and contact purposes. The data type is not generally suitable for registries*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:delimiter` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:country` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:state` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:city` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postalCode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** AD.CA.BASIC

Extends Rule(s): [AD](../DTr1_AD) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| useablePeriod property is not permitted|dtr1‑1‑AD.CA.BASIC|
|<error/>| isNotOrdered property is not permitted|dtr1‑2‑AD.CA.BASIC|
|<error/>| @use is required|dtr1‑3‑AD.CA.BASIC|
|<error/>| @use may have up to three codes and must be drawn from 'H', 'PHYS', 'PST', 'TMP', 'WP', 'CONF', 'DIR'|dtr1‑4‑AD.CA.BASIC|
|<error/>| supported address parts (part types) are country, city, state, postalCode, addressLine and up to four delimiter|dtr1‑5‑AD.CA.BASIC|
|<error/>| address parts and delimiter-separated text has each a maximum length of 80 characters|dtr1‑6‑AD.CA.BASIC|


