---
permalink: /documentation/datatypes/DTr1_hl7nl_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# hl7nl:TS Time Stamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **hl7nl:TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|


