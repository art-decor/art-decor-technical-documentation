---
permalink: /documentation/datatypes/DTr1_TS.DATE.FULL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.DATE.FULL Time Stamp - Flavor Date

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.DATE.FULL** |UV||

*TS Flavor .DATE.FULL, shall contain reference to a particular day, format YYYYMMDD*


## Operationalization
Shall contain reference to a particular day
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.DATE.FULL

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or date precision of time stamp shall be YYYYMMDD.|dtr1‑1‑TS.DATE.FULL|


