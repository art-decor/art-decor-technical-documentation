---
permalink: /documentation/datatypes/DTr1_INT.NONNEG/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# INT.NONNEG Integer - Flavor Interval of Integer, non-negative

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **INT.NONNEG** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | int|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** INT.NONNEG

Extends Rule(s): [INT](../DTr1_INT) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value &gt;&#061; 0|dtr1‑2‑INT.NONNEG|


