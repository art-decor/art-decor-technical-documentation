---
permalink: /documentation/datatypes/DTr1_TEL.NL.EXTENDED/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TEL.NL.EXTENDED Telecommunication Address - Flavor Dutch Extended

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TEL.NL.EXTENDED** |<flag country="NL"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:useablePeriod` | 0 .. * | [SXCM_TS](../DTr1_SXCM_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TEL.NL.EXTENDED

Extends Rule(s): [URL.NL.EXTENDED](../DTr1_URL.NL.EXTENDED) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not null and usablePeriod|dtr1‑1‑TEL.NL.EXTENDED|


