---
permalink: /documentation/datatypes/DTr1_IVL_TS.CH.TZ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# IVL_TS.CH.TZ Interval of Time Stamp - Flavor Timezone required

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **IVL_TS.CH.TZ** |<flag country="CH"/> ||

*Interval of Timestamp. SHALL be precise to the day, SHALL include a time zone if more precise than to the day*


## Operationalization
Timezone required
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:low` | 0 .. 1 | [IVXB_TS](../DTr1_IVXB_TS) |
|`hl7:center` | 0 .. 1 | [TS](../DTr1_TS) |
|`hl7:width` | 0 .. 1 | [PQ](../DTr1_PQ) |
|`hl7:high` | 0 .. 1 | [IVXB_TS](../DTr1_IVXB_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** IVL_TS.CH.TZ

Extends Rule(s): [IVL_TS](../DTr1_IVL_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time value SHALL include a time zone if more precise than to the day|dtr1‑1‑IVL_TS.CH.TZ|
|<error/>| time low value SHALL include a time zone if more precise than to the day|dtr1‑2‑IVL_TS.CH.TZ|
|<error/>| time center value SHALL include a time zone if more precise than to the day|dtr1‑3‑IVL_TS.CH.TZ|
|<error/>| time high value SHALL include a time zone if more precise than to the day|dtr1‑4‑IVL_TS.CH.TZ|


