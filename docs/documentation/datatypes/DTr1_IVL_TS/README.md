---
permalink: /documentation/datatypes/DTr1_IVL_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# IVL_TS Interval of Time Stamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **IVL_TS** ||[IVL_TS.EPSOS.TZ](../DTr1_IVL_TS.EPSOS.TZ) [IVL_TS.IPS.TZ](../DTr1_IVL_TS.IPS.TZ) [IVL_TS.EPSOS.TZ.OPT](../DTr1_IVL_TS.EPSOS.TZ.OPT) [IVL_TS.CH.TZ](../DTr1_IVL_TS.CH.TZ) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:low` | 0 .. 1 | [IVXB_TS](../DTr1_IVXB_TS) |
|`hl7:center` | 0 .. 1 | [TS](../DTr1_TS) |
|`hl7:width` | 0 .. 1 | [PQ](../DTr1_PQ) |
|`hl7:high` | 0 .. 1 | [IVXB_TS](../DTr1_IVXB_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** IVL_TS

Extends Rule(s): [SXCM_TS](../DTr1_SXCM_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null violation. Cannot have @nullFlavor and @value or other child elements|dtr1‑2‑IVL_TS|
|<error/>| co-occurence violation. Cannot have @value and other child elements|dtr1‑3‑IVL_TS|
|<error/>| co-occurence violation. Cannot have @value and child elements|dtr1‑4‑2‑IVL_TS|
|<error/>| co-occurence violation. Cannot have center and other elements|dtr1‑4‑3‑IVL_TS|
|<error/>| co-occurence violation. Cannot have width and have both low and high elements|dtr1‑4‑4‑IVL_TS|
|<error/>| no updateMode on IVL attributes|dtr1‑5‑IVL_TS|
|<error/>| low/@value must not be equal to high/@value|dtr1‑6‑IVL_TS|
|<error/>| width element: no unit without value|dtr1‑1‑PQR|
|<error/>| width element: no translation|dtr1‑2‑PQR|
|<error/>| low must be lower than or equal to high. Found low boundary PINF (Positive Infinity)|dtr1‑7‑1‑IVL_TS|
|<error/>| low must be lower than or equal to high. Found high boundary NINF (Negative Infinity)|dtr1‑7‑2‑IVL_TS|
|<error/>| for width only us (microseconds), ms (milliseconds), s (seconds), min (minute), h (hours), d (day), wk (week), mo (month) or a (year) are allowed|dtr1‑8‑IVL_TS|
|<error/>| *local-name()*/low "*$theTSLow*" is not a valid timestamp.|dtr1‑9‑IVL_TS|
|<error/>| *local-name()*/center "*$theTSCenter*" is not a valid timestamp.|dtr1‑9‑IVL_TS|
|<error/>| *local-name()*/high "*$theTSHigh*" is not a valid timestamp.|dtr1‑9‑IVL_TS|
|<error/>| low/@value (*$theTSLow*) must be before high/@value (*$theTSHigh*)|dtr1‑7‑IVL_TS|


