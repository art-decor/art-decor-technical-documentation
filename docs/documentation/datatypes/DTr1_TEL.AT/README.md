---
permalink: /documentation/datatypes/DTr1_TEL.AT/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TEL.AT Telecommunication Address - Flavor Austria

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TEL.AT** |<flag country="AT"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:useablePeriod` | 0 .. * | [SXCM_TS](../DTr1_SXCM_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TEL.AT


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| elements of type URL SHALL have a @value attribute.|dtr1‑1‑URL|
|<error/>| not null and useablePeriod|dtr1‑1‑TEL|
|<warning/>| ungültiges URL-Schema *$urlScheme*|dtr1‑1‑TEL.AT|
|<warning/>| @use MUSS aus den Werten 'H', 'HP', 'HV', 'WP', 'AS', 'EC', 'MC', 'PG', 'TMP' gewählt werden.|dtr1‑2‑TEL.AT|
|<warning/>| Nur Ziffernzeichen 0 bis 9, ggf. mit vorangehendem + sowie als visuelle Separatorzeichen Bindestrich -, Punkt . oder Klammern () sind erlaubt.|dtr1‑3‑TEL.AT|


