---
permalink: /documentation/datatypes/DTr1_AD/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# AD Address


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **AD** ||[AD.NL](../DTr1_AD.NL) [AD.DE](../DTr1_AD.DE) [AD.EPSOS](../DTr1_AD.EPSOS) [AD.IPS](../DTr1_AD.IPS) [AD.CA.BASIC](../DTr1_AD.CA.BASIC) [AD.CA](../DTr1_AD.CA) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@isNotOrdered` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|
|`hl7:delimiter` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:country` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:state` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:county` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:city` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postalCode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumber` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumberNumeric` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:direction` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetName` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameBase` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:additionalLocator` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitID` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:careOf` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:censusTract` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationArea` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationQualifier` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryMode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryModeIdentifier` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:buildingNumberSuffix` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postBox` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:precinct` | 0 .. * | [ADXP](../DTr1_ADXP) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** AD

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| address shall be null or text only or element only (no mixed content)|dtr1‑1‑AD|
|<error/>| no useablePeriod if null|dtr1‑2‑AD|
|<error/>| updateMode shall not be used on address elements|dtr1‑3‑AD|
|<error/>| useablePeriod/low/@value must have the same precision as useablePeriod/high/@value|dtr1‑5‑AD|
|<error/>| useablePeriod/low/@value must be before useablePeriod/high/@value|dtr1‑6‑AD|
|<error/>| for useablePeriod/width only us (microseconds), ms (milliseconds), s (seconds), min (minute), h (hours), d (day), wk (week), mo (month) or a (year) are allowed|dtr1‑7‑AD|
|<warning/>| empty address particles should not be present.|dtr1‑8‑AD|

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-001] *$name* SHALL NOT have nullFlavor, if there are other *$name* elements which are not null|dtr1‑9‑AD|
|<error/>| [RIM-002] *$name* (*$value*) SHALL be distinct|dtr1‑10‑AD|


