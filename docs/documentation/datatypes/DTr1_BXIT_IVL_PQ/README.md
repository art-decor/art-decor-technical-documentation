---
permalink: /documentation/datatypes/DTr1_BXIT_IVL_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# BXIT_IVL_PQ BAG of Interval of Physical Quantities


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **BXIT_IVL_PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@qty` |  optional  | int|
|`@nullFlavor` |  optional  | cs|


