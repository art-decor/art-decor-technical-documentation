---
permalink: /documentation/datatypes/DTr1_SXCM_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SXCM_PQ Set Component of Physical Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SXCM_PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | int|
|`@unit` |  optional  | cs|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** SXCM_PQ

Extends Rule(s): [PQ](../DTr1_PQ) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not operator if null|dtr1‑1‑SXCM_PQ|


