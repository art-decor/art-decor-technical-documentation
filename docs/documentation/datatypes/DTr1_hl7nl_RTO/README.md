---
permalink: /documentation/datatypes/DTr1_hl7nl_RTO/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# hl7nl:RTO Ratio Quantity / Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **hl7nl:RTO** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@nullFlavor` |  optional  | cs|
|`hl7:numerator` | 1 .. 1 | |
|`hl7:denominator` | 1 .. 1 | |


