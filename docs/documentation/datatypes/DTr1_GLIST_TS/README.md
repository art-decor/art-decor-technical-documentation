---
permalink: /documentation/datatypes/DTr1_GLIST_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# GLIST_TS Sampled Sequence of Timestamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **GLIST_TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@period` |  optional  | int|
|`@denominator` |  optional  | int|
|`head` | 1 .. 1 | [TS](../DTr1_TS) |
|`increment` | 1 .. 1 | [PQ](../DTr1_PQ) |


