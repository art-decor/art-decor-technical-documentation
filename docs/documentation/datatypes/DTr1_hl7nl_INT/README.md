---
permalink: /documentation/datatypes/DTr1_hl7nl_INT/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# hl7nl:INT Integer


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **hl7nl:INT** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | int|
|`@nullFlavor` |  optional  | cs|
|`hl7nl:uncertainRange` | 0 .. 1 | |


