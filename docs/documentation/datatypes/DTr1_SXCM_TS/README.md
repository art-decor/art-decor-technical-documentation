---
permalink: /documentation/datatypes/DTr1_SXCM_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SXCM_TS Set Component of Time Stamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SXCM_TS** ||[IVL_TS.EPSOS.TZ](../DTr1_IVL_TS.EPSOS.TZ) [IVL_TS.IPS.TZ](../DTr1_IVL_TS.IPS.TZ) [IVL_TS.EPSOS.TZ.OPT](../DTr1_IVL_TS.EPSOS.TZ.OPT) [IVL_TS.CH.TZ](../DTr1_IVL_TS.CH.TZ) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** SXCM_TS

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not operator if null|dtr1‑1‑SXCM_TS|


