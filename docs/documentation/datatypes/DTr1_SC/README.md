---
permalink: /documentation/datatypes/DTr1_SC/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SC String with Codes


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SC** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@mediaType` |  optional  | cs|
|`@language` |  optional  | cs|
|`@compression` ( DF \| GZ \| Z \| ZL ) |  optional  | cs|
|`@integrityCheck` |  optional  | bin|
|`@integrityCheckAlgorithm` ( SHA-1 \| SHA-256 ) |  optional  | cs|
|`@representation` ( B64 \| TXT ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:reference` | 0 .. 1 | [TEL](../DTr1_TEL) |
|`hl7:thumbnail` | 0 .. 1 | [thumbnail](../DTr1_thumbnail) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** SC

Extends Rule(s): [ST](../DTr1_ST) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| code requires codeSystem|dtr1‑2‑SC|
|<error/>| codeSystemName only if codeSystem|dtr1‑3‑SC|
|<error/>| codeSystemVersion only if codeSystem|dtr1‑4‑SC|
|<error/>| displayName only if code|dtr1‑5‑SC|
|<error/>| no code if null|dtr1‑6‑SC|
|<error/>| no displayName if null|dtr1‑7‑SC|


