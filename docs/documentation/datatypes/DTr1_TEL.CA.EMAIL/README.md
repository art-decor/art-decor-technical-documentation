---
permalink: /documentation/datatypes/DTr1_TEL.CA.EMAIL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TEL.CA.EMAIL Telecommunication Address - Flavor Canadian Email

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TEL.CA.EMAIL** |<flag country="CA"/> ||

*TEL.EMAIL Email Address (CA constraints)*

*Allows e-mail addresses to be communicated*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TEL.CA.EMAIL

Extends Rule(s): [TEL](../DTr1_TEL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| useablePeriod property is not permitted|dtr1‑1‑TEL.CA.EMAIL|
|<error/>| @use is required|dtr1‑2‑TEL.CA.EMAIL|
|<error/>| @use may have up to three codes and must be drawn from 'EC', 'H', 'MC', 'PG', 'TMP', 'WP'|dtr1‑3‑TEL.CA.EMAIL|
|<error/>| @value has maximum length of 50 characters|dtr1‑4‑TEL.CA.EMAIL|
|<error/>| telecommunication scheme supported is "mailto:"|dtr1‑5‑TEL.CA.EMAIL|


