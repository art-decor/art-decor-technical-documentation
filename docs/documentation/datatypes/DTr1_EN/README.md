---
permalink: /documentation/datatypes/DTr1_EN/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# EN Entity Name


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **EN** ||[PN.NL](../DTr1_PN.NL) [PN.CA](../DTr1_PN.CA) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@nullFlavor` |  optional  | cs|
|`delimiter` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`family` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`given` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`prefix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`suffix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`validTime` | 0 .. 1 | [IVL_TS](../DTr1_IVL_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** EN

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| entity name shall not be null and have elements or text|dtr1‑1.1‑EN|
|<error/>| entity name shall not have both elements and text (no mixed content)|dtr1‑1.2‑EN|
|<error/>| entity name that is not null shall not be empty|dtr1‑1.3‑EN|
|<warning/>| empty name particles should not be present.|dtr1‑3‑EN|
|<warning/>| Use of quotes at the beginning of a name are probably not correct.|dtr1‑4‑EN|
|<error/>| enxp elements SHALL have distinct values in @qualifier|dtr1‑7‑EN|

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-001] *$name* SHALL NOT have nullFlavor, if there are other *$name* elements which are not null|dtr1‑5‑EN|
|<error/>| [RIM-002] *$name* (*$value*) SHALL be distinct|dtr1‑6‑EN|


