---
permalink: /documentation/datatypes/
description: PLEASE DO NOT EDIT THIS PAGE
---
# Datatypes

The following data types are supported by ART-DECOR in the template definition, schematron generation and validation.

::: tip

For the datatypes used in DECOR datasets and concepts, see: [DECOR datasets](/introduction/dataset/)

:::

## List

| Data Type / Variant (Flavor)                                 | flavor of | Realm | Description |
| :----------------------------------------------------------- | :-------: | :---- | :---------- |
|<tree level="0"/>[ANY](DTr1_ANY/) |  |  | ANY|
|<tree level="1"/>[AD](DTr1_AD/) |  |  | Address|
|<tree level="2"/>[AD.NL](DTr1_AD.NL/) | AD | <flag country="NL"/>  | Address - Flavor Dutch Address|
|<tree level="2"/>[AD.DE](DTr1_AD.DE/) | AD | <flag country="DE"/>  | Address - Flavor German Address|
|<tree level="2"/>[AD.EPSOS](DTr1_AD.EPSOS/) | AD | <flag country="EU"/>  | Address - Flavor epSOS Address|
|<tree level="2"/>[AD.IPS](DTr1_AD.IPS/) | AD | UV | Address - Flavor IPS Address|
|<tree level="2"/>[AD.CA.BASIC](DTr1_AD.CA.BASIC/) | AD | <flag country="CA"/>  | Address - Flavor Canadian Address - supported address parts (part types) are country, city, state, postalCode, addressLine and up to four delimiter - address parts and delimiter-separated text has each a maximum length of 80 characters|
|<tree level="3"/>[AD.CA](DTr1_AD.CA/) | AD | <flag country="CA"/>  | Address - Flavor Canadian Address - supported address parts (part types) are country, city, state, postalCode, addressLine and up to four delimiter - address parts and delimiter-separated text has each a maximum length of 80 characters|
|<tree level="1"/>[BIN](DTr1_BIN/) |  |  | Binary Data|
|<tree level="2"/>[ED](DTr1_ED/) |  |  | Encapsulated Data|
|<tree level="3"/>[ST](DTr1_ST/) |  |  | Character String|
|<tree level="4"/>[SC](DTr1_SC/) |  |  | String with Codes|
|<tree level="4"/>[ENXP](DTr1_ENXP/) |  |  | Entity Name Part|
|<tree level="4"/>[ADXP](DTr1_ADXP/) |  |  | Address Part|
|<tree level="3"/>[thumbnail](DTr1_thumbnail/) |  |  | Thumbnail|
|<tree level="1"/>[BL](DTr1_BL/) |  |  | Boolean|
|<tree level="1"/>[BN](DTr1_BN/) |  |  | Boolean Non Null|
|<tree level="1"/>[CD](DTr1_CD/) |  |  | Concept Descriptor<br/>(has strength)|
|<tree level="2"/>[CE](DTr1_CE/) |  |  | Coded with Equivalents<br/>(has strength)|
|<tree level="3"/>[CV](DTr1_CV/) |  |  | Coded Value<br/>(has strength)|
|<tree level="4"/>[CO](DTr1_CO/) |  |  | Coded Ordinal<br/>(has strength)|
|<tree level="5"/>[CO.EPSOS](DTr1_CO.EPSOS/) | CO | <flag country="EU"/>  | Coded Ordinal - Flavor epSOS<br/>(has strength)|
|<tree level="4"/>[PQR](DTr1_PQR/) |  |  | Physical Quantity Representation<br/>(has strength)|
|<tree level="4"/>[CV.EPSOS](DTr1_CV.EPSOS/) | CV | <flag country="EU"/>  | Coded Value - Flavor epSOS<br/>(has strength)|
|<tree level="4"/>[CV.IPS](DTr1_CV.IPS/) | CV | UV | Coded Value - Flavor IPS CV<br/>(has strength)|
|<tree level="3"/>[EIVL.event](DTr1_EIVL.event/) |  | UV | Event-Related Interval of Time<br/>(has strength)|
|<tree level="3"/>[CE.EPSOS](DTr1_CE.EPSOS/) | CE | <flag country="EU"/>  | Coded with Equivalents - Flavor epSOS<br/>(has strength)|
|<tree level="3"/>[CE.IPS](DTr1_CE.IPS/) | CE | UV | Coded with Equivalents - Flavor IPS CE<br/>(has strength)|
|<tree level="2"/>[CD.EPSOS](DTr1_CD.EPSOS/) | CD | <flag country="EU"/>  | Concept Descriptor - Flavor epSOS<br/>(has strength)|
|<tree level="2"/>[CD.IPS](DTr1_CD.IPS/) | CD | UV | Concept Descriptor - Flavor IPS CD<br/>(has strength)|
|<tree level="2"/>[CD.SDTC](DTr1_CD.SDTC/) | CD | UV | Concept Descriptor - Flavor Structured Documents Technical Committee<br/>(has strength)|
|<tree level="1"/>[CR](DTr1_CR/) |  |  | Concept Role<br/>(has strength)|
|<tree level="1"/>[CS](DTr1_CS/) |  |  | Coded Simple Value<br/>(has strength)|
|<tree level="2"/>[CS.LANG](DTr1_CS.LANG/) | CS | UV | Coded Simple Value - Flavor Language Codes<br/>(has strength)|
|<tree level="1"/>[EN](DTr1_EN/) |  |  | Entity Name|
|<tree level="2"/>[ON](DTr1_ON/) |  |  | Organization Name|
|<tree level="2"/>[PN](DTr1_PN/) |  |  | Person Name|
|<tree level="3"/>[PN.NL](DTr1_PN.NL/) | PN | <flag country="NL"/>  | Person Name - Flavor Dutch Person Name|
|<tree level="3"/>[PN.CA](DTr1_PN.CA/) | PN | <flag country="CA"/>  | Person Name - Flavor Canadian Person Name|
|<tree level="2"/>[TN](DTr1_TN/) |  |  | Trivial Name|
|<tree level="1"/>[II](DTr1_II/) |  |  | Instance Identifier|
|<tree level="2"/>[II.NL.AGB](DTr1_II.NL.AGB/) | II | <flag country="NL"/>  | Instance Identifier - Flavor Dutch AGB|
|<tree level="2"/>[II.NL.BIG](DTr1_II.NL.BIG/) | II | <flag country="NL"/>  | Instance Identifier - Flavor Dutch BIG|
|<tree level="2"/>[II.NL.BSN](DTr1_II.NL.BSN/) | II | <flag country="NL"/>  | Instance Identifier - Flavor Dutch BSN|
|<tree level="2"/>[II.NL.URA](DTr1_II.NL.URA/) | II | <flag country="NL"/>  | Instance Identifier - Flavor Dutch URA|
|<tree level="2"/>[II.NL.UZI](DTr1_II.NL.UZI/) | II | <flag country="NL"/>  | Instance Identifier - Flavor Dutch UZI|
|<tree level="2"/>[II.AT.DVR](DTr1_II.AT.DVR/) | II | <flag country="AT"/>  | Instance Identifier - Flavor Austrian DVR|
|<tree level="2"/>[II.AT.ATU](DTr1_II.AT.ATU/) | II | <flag country="AT"/>  | Instance Identifier - Flavor Austrian ATU|
|<tree level="2"/>[II.AT.BLZ](DTr1_II.AT.BLZ/) | II | <flag country="AT"/>  | Instance Identifier - Flavor Austrian Bankleitzahl|
|<tree level="2"/>[II.AT.KTONR](DTr1_II.AT.KTONR/) | II | <flag country="AT"/>  | Instance Identifier - Flavor Austrian Kontonummer|
|<tree level="2"/>[II.EPSOS](DTr1_II.EPSOS/) | II | <flag country="EU"/>  | Instance Identifier - Flavor epSOS|
|<tree level="1"/>[GLIST](DTr1_GLIST/) |  |  | Generated Sequence|
|<tree level="2"/>[GLIST_PQ](DTr1_GLIST_PQ/) |  |  | Sampled Sequence of Quantity|
|<tree level="2"/>[GLIST_TS](DTr1_GLIST_TS/) |  |  | Sampled Sequence of Timestamp|
|<tree level="1"/>[SLIST](DTr1_SLIST/) |  |  | Sampled Sequence|
|<tree level="2"/>[SLIST_PQ](DTr1_SLIST_PQ/) |  |  | Sampled Sequence of Quantity|
|<tree level="2"/>[SLIST_TS](DTr1_SLIST_TS/) |  |  | Sampled Sequence of Timestamp|
|<tree level="1"/>[list_int](DTr1_list_int/) |  |  | A sequence of raw digits for the sample values.|
|<tree level="1"/>[QTY](DTr1_QTY/) |  |  | Quantity|
|<tree level="2"/>[INT](DTr1_INT/) |  |  | Integer|
|<tree level="3"/>[IVXB_INT](DTr1_IVXB_INT/) |  |  | Interval Boundary of Integer|
|<tree level="3"/>[SXCM_INT](DTr1_SXCM_INT/) |  |  | Set Component of Integer|
|<tree level="4"/>[IVL_INT](DTr1_IVL_INT/) |  |  | Interval of Integer|
|<tree level="3"/>[INT.NONNEG](DTr1_INT.NONNEG/) | INT | UV | Integer - Flavor Interval of Integer, non-negative|
|<tree level="3"/>[INT.POS](DTr1_INT.POS/) | INT | UV | Integer - Flavor Interval of Integer, positive|
|<tree level="2"/>[hl7nl:INT](DTr1_hl7nl_INT/) |  |  | Integer|
|<tree level="2"/>[hl7nl:QSET_QTY](DTr1_hl7nl_QSET_QTY/) |  |  | |
|<tree level="3"/>[hl7nl:IVL_QTY](DTr1_hl7nl_IVL_QTY/) |  |  | Interval of Quantity|
|<tree level="2"/>[MO](DTr1_MO/) |  |  | Monetary Amount|
|<tree level="3"/>[IVXB_MO](DTr1_IVXB_MO/) |  |  | Interval Boundary of Monetary Amount|
|<tree level="3"/>[SXCM_MO](DTr1_SXCM_MO/) |  |  | Set Component of Monetary Amount|
|<tree level="4"/>[IVL_MO](DTr1_IVL_MO/) |  |  | Interval of Monetary Amount|
|<tree level="2"/>[PQ](DTr1_PQ/) |  |  | Physical Quantity|
|<tree level="3"/>[IVXB_PQ](DTr1_IVXB_PQ/) |  |  | Interval Boundary of Physical Quantity|
|<tree level="3"/>[RTO_PQ_PQ](DTr1_RTO_PQ_PQ/) |  |  | Ratio Physical Quantity / Physical Quantity|
|<tree level="3"/>[SXCM_PQ](DTr1_SXCM_PQ/) |  |  | Set Component of Physical Quantity|
|<tree level="4"/>[IVL_PQ](DTr1_IVL_PQ/) |  |  | Interval of Physical Quantity|
|<tree level="5"/>[BXIT_IVL_PQ](DTr1_BXIT_IVL_PQ/) |  |  | BAG of Interval of Physical Quantities|
|<tree level="2"/>[hl7nl:PQ](DTr1_hl7nl_PQ/) |  |  | Physical Quantity|
|<tree level="2"/>[REAL](DTr1_REAL/) |  |  | Real|
|<tree level="3"/>[IVXB_REAL](DTr1_IVXB_REAL/) |  |  | Interval Boundary of Real|
|<tree level="3"/>[SXCM_REAL](DTr1_SXCM_REAL/) |  |  | Set Component of Real|
|<tree level="4"/>[IVL_REAL](DTr1_IVL_REAL/) |  |  | Interval of Real|
|<tree level="3"/>[REAL.NONNEG](DTr1_REAL.NONNEG/) | REAL | UV | Real - Flavor Real, non-negative|
|<tree level="3"/>[REAL.POS](DTr1_REAL.POS/) | REAL | UV | Real - Flavor Real, positive|
|<tree level="2"/>[TS](DTr1_TS/) |  |  | Time Stamp|
|<tree level="3"/>[IVXB_TS](DTr1_IVXB_TS/) |  |  | Interval Boundary of Time Stamp|
|<tree level="3"/>[SXCM_TS](DTr1_SXCM_TS/) |  |  | Set Component of Time Stamp|
|<tree level="4"/>[IVL_TS](DTr1_IVL_TS/) |  |  | Interval of Time Stamp|
|<tree level="5"/>[IVL_TS.EPSOS.TZ](DTr1_IVL_TS.EPSOS.TZ/) | IVL_TS | <flag country="EU"/>  | Interval of Time Stamp - Flavor epSOS|
|<tree level="5"/>[IVL_TS.IPS.TZ](DTr1_IVL_TS.IPS.TZ/) | IVL_TS | UV | Interval of Time Stamp - Flavor IPS IVL TS TZ|
|<tree level="5"/>[IVL_TS.EPSOS.TZ.OPT](DTr1_IVL_TS.EPSOS.TZ.OPT/) | IVL_TS | <flag country="EU"/>  | Interval of Time Stamp - Flavor epSOS|
|<tree level="5"/>[IVL_TS.CH.TZ](DTr1_IVL_TS.CH.TZ/) | IVL_TS | <flag country="CH"/>  | Interval of Time Stamp - Flavor Timezone required|
|<tree level="4"/>[hl7nl:IVL_TS](DTr1_hl7nl_IVL_TS/) |  |  | Interval of Time Stamp|
|<tree level="4"/>[PIVL_TS](DTr1_PIVL_TS/) |  |  | Periodic Interval of Timezone|
|<tree level="4"/>[hl7nl:PIVL_TS](DTr1_hl7nl_PIVL_TS/) |  |  | Periodic Interval of Timezone - Datatypes Release 2 - Dutch extension import|
|<tree level="4"/>[EIVL_TS](DTr1_EIVL_TS/) |  |  | Event-related time interval|
|<tree level="4"/>[SXPR_TS](DTr1_SXPR_TS/) |  |  | Parenthetic Set Expression of Time Stamp|
|<tree level="3"/>[TS.DATE](DTr1_TS.DATE/) | TS | UV | Time Stamp - Flavor Date|
|<tree level="3"/>[TS.DATE.FULL](DTr1_TS.DATE.FULL/) | TS | UV | Time Stamp - Flavor Date|
|<tree level="3"/>[TS.DATE.MIN](DTr1_TS.DATE.MIN/) | TS | UV | Time Stamp - Flavor Date|
|<tree level="3"/>[TS.DATETIME.MIN](DTr1_TS.DATETIME.MIN/) | TS | UV | Time Stamp - Flavor Date|
|<tree level="3"/>[TS.DATETIMETZ.MIN](DTr1_TS.DATETIMETZ.MIN/) | TS | UV | Time Stamp - Flavor Date|
|<tree level="3"/>[TS.EPSOS.TZ](DTr1_TS.EPSOS.TZ/) | TS | <flag country="EU"/>  | Time Stamp - Flavor epSOS|
|<tree level="3"/>[TS.IPS.TZ](DTr1_TS.IPS.TZ/) | TS | UV | Time Stamp - Flavor epSOS|
|<tree level="3"/>[TS.EPSOS.TZ.OPT](DTr1_TS.EPSOS.TZ.OPT/) | TS | <flag country="EU"/>  | Time Stamp - Flavor epSOS|
|<tree level="3"/>[TS.CH.TZ](DTr1_TS.CH.TZ/) | TS | <flag country="CH"/>  | Time Stamp - Flavor Date|
|<tree level="3"/>[TS.AT.TZ](DTr1_TS.AT.TZ/) | TS | <flag country="AT"/>  | Time Stamp - Flavor Date|
|<tree level="3"/>[TS.AT.VAR](DTr1_TS.AT.VAR/) | TS | <flag country="AT"/>  | Time Stamp - Flavor Date|
|<tree level="2"/>[hl7nl:TS](DTr1_hl7nl_TS/) |  |  | Time Stamp|
|<tree level="2"/>[hl7nl:RTO](DTr1_hl7nl_RTO/) |  |  | Ratio Quantity / Quantity|
|<tree level="2"/>[RTO_QTY_QTY](DTr1_RTO_QTY_QTY/) |  |  | Ratio Quantity / Quantity|
|<tree level="3"/>[RTO](DTr1_RTO/) |  |  | Ratio Quantity / Quantity|
|<tree level="1"/>[SD.TEXT](DTr1_SD.TEXT/) |  |  | Structured Document Text|
|<tree level="1"/>[URL](DTr1_URL/) |  |  | Universal Resource Locator|
|<tree level="2"/>[TEL](DTr1_TEL/) |  |  | Telecommunication Address|
|<tree level="3"/>[TEL.AT](DTr1_TEL.AT/) | TEL | <flag country="AT"/>  | Telecommunication Address - Flavor Austria|
|<tree level="3"/>[TEL.EPSOS](DTr1_TEL.EPSOS/) | TEL | <flag country="EU"/>  | Telecommunication Address - Flavor epSOS|
|<tree level="3"/>[TEL.IPS](DTr1_TEL.IPS/) | TEL | UV | Telecommunication Address - Flavor IPS TEL|
|<tree level="3"/>[TEL.NL.EXTENDED](DTr1_TEL.NL.EXTENDED/) | TEL | <flag country="NL"/>  | Telecommunication Address - Flavor Dutch Extended|
|<tree level="3"/>[TEL.CA.EMAIL](DTr1_TEL.CA.EMAIL/) | TEL | <flag country="CA"/>  | Telecommunication Address - Flavor Canadian Email|
|<tree level="3"/>[TEL.CA.PHONE](DTr1_TEL.CA.PHONE/) | TEL | <flag country="CA"/>  | Telecommunication Address - Flavor Canadian Phone|
|<tree level="2"/>[URL.NL.EXTENDED](DTr1_URL.NL.EXTENDED/) | URL | <flag country="NL"/>  | Universal Resource Locator - Flavor Dutch Extended|



