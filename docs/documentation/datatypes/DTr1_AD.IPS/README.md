---
permalink: /documentation/datatypes/DTr1_AD.IPS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# AD.IPS Address - Flavor IPS Address

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **AD.IPS** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@isNotOrdered` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|
|`hl7:delimiter` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:country` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:state` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:county` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:city` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postalCode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumber` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumberNumeric` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:direction` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetName` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameBase` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:additionalLocator` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitID` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:careOf` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:censusTract` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationArea` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationQualifier` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryMode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryModeIdentifier` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:buildingNumberSuffix` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postBox` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:precinct` | 0 .. * | [ADXP](../DTr1_ADXP) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** AD.IPS

Extends Rule(s): [AD](../DTr1_AD) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| if addr is not null at least one sub element has to be provided|dtr1‑1‑AD.IPS|


