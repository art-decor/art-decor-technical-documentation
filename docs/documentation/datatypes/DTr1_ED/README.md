---
permalink: /documentation/datatypes/DTr1_ED/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# ED Encapsulated Data


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **ED** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@mediaType` |  optional  | cs|
|`@language` |  optional  | cs|
|`@compression` ( DF \| GZ \| Z \| ZL ) |  optional  | cs|
|`@integrityCheck` |  optional  | bin|
|`@integrityCheckAlgorithm` ( SHA-1 \| SHA-256 ) |  optional  | cs|
|`@representation` ( B64 \| TXT ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:reference` | 0 .. 1 | [TEL](../DTr1_TEL) |
|`hl7:thumbnail` | 0 .. 1 | [thumbnail](../DTr1_thumbnail) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** ED

Extends Rule(s): [BIN](../DTr1_BIN) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| integrityCheckAlgorithm required if integrityCheck|dtr1‑2‑ED|
|<error/>| thumbnails not null and reference|dtr1‑3‑ED|
|<error/>| thumbnails do not have thumbnails|dtr1‑4‑ED|
|<error/>| compression only on binary|dtr1‑5‑ED|
|<error/>| value implies mediaType is text/plain|dtr1‑6‑ED|
|<error/>| no history or updateMode|dtr1‑7‑ED|
|<error/>| no charset for value or xml|dtr1‑8‑ED|
|<error/>| no nested translations|dtr1‑9‑ED|
|<error/>| no mediaType if null|dtr1‑10‑ED|
|<error/>| no charset if null|dtr1‑11‑ED|
|<error/>| no language if null|dtr1‑12‑ED|
|<error/>| no compression if null|dtr1‑13‑ED|
|<error/>| no integrityCheck if null|dtr1‑14‑ED|
|<error/>| no integrityCheckAlgorithm if null|dtr1‑15‑ED|
|<error/>| no thumbnail if null|dtr1‑16‑ED|
|<error/>| no translation if null|dtr1‑17‑ED|
|<error/>| @language SHALL conform to RFC 3066. This usually has format sss?-CC with sss? for language code (ISO-639-1 / ISO-639-2) and optional CC for country code (conform ISO-3166 alpha-2)|dtr1‑18‑ED|


