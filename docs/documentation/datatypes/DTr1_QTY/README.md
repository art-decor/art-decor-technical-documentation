---
permalink: /documentation/datatypes/DTr1_QTY/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# QTY Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **QTY** ||[INT.NONNEG](../DTr1_INT.NONNEG) [INT.POS](../DTr1_INT.POS) [REAL.NONNEG](../DTr1_REAL.NONNEG) [REAL.POS](../DTr1_REAL.POS) [IVL_TS.EPSOS.TZ](../DTr1_IVL_TS.EPSOS.TZ) [IVL_TS.IPS.TZ](../DTr1_IVL_TS.IPS.TZ) [IVL_TS.EPSOS.TZ.OPT](../DTr1_IVL_TS.EPSOS.TZ.OPT) [IVL_TS.CH.TZ](../DTr1_IVL_TS.CH.TZ) [TS.DATE](../DTr1_TS.DATE) [TS.DATE.FULL](../DTr1_TS.DATE.FULL) [TS.DATE.MIN](../DTr1_TS.DATE.MIN) [TS.DATETIME.MIN](../DTr1_TS.DATETIME.MIN) [TS.DATETIMETZ.MIN](../DTr1_TS.DATETIMETZ.MIN) [TS.EPSOS.TZ](../DTr1_TS.EPSOS.TZ) [TS.IPS.TZ](../DTr1_TS.IPS.TZ) [TS.EPSOS.TZ.OPT](../DTr1_TS.EPSOS.TZ.OPT) [TS.CH.TZ](../DTr1_TS.CH.TZ) [TS.AT.TZ](../DTr1_TS.AT.TZ) [TS.AT.VAR](../DTr1_TS.AT.VAR) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@nullFlavor` |  optional  | cs|
|`@xsi:type` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** QTY

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| no uncertainty|dtr1‑1‑QTY|
|<error/>| no history or updateMode|dtr1‑2‑QTY|

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-001] *$name* SHALL NOT have nullFlavor, if there are other *$name* elements which are not null|dtr1‑3‑QTY|
|<error/>| [RIM-002] *$name* (@value&#061;*$value* @unit&#061;*$unit*) SHALL be distinct|dtr1‑4‑QTY|


