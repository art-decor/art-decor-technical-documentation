---
permalink: /documentation/datatypes/DTr1_TS.DATE/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.DATE Time Stamp - Flavor Date

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.DATE** |UV||

*constrains TS so that it may only contain a date value YYYYMMDD (or YYYYMM or YYYY)*

*def: let hasTimezone : Boolean = value.pos("+") > 0 or value.pos("-") > 0*

*inv "Date": not hasTimezone and value.size <= 8*


## Operationalization
May only contain a date value YYYYMMDD (or YYYYMM or YYYY)
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.DATE

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or date precision of time stamp shall be YYYYMMDD.|dtr1‑1‑TS.DATE|


