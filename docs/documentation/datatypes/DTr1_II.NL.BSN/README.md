---
permalink: /documentation/datatypes/DTr1_II.NL.BSN/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# II.NL.BSN Instance Identifier - Flavor Dutch BSN

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **II.NL.BSN** |<flag country="NL"/> ||

*II Flavor .NL The Netherlands .BSN Burgerservicenummer (BSN) Nederland, fixed OID 2.16.840.1.113883.2.4.6.3*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@root` |  optional  | oid|
|`@extension` |  optional  | st|
|`@assigningAuthorityName` |  optional  | st|
|`@displayable` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** II.NL.BSN

Extends Rule(s): [II](../DTr1_II) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| Burgerservicenummer @root MOET 2.16.840.1.113883.2.4.6.3 zijn indien niet null. Gevonden: "*@root*"|dtr1‑1‑II.NL.BSN|
|<error/>| Burgerservicenummer MOET 9 cijfers lang zijn, met voorloopnullen indien korter dan 9 cijfers. Gevonden: "*@extension*"|dtr1‑2‑II.NL.BSN|
|<error/>| Burgerservicenummer voldoet niet aan modulo 11 proef. Gevonden: "*@extension*"|dtr1‑3‑II.NL.BSN|


