---
permalink: /documentation/datatypes/DTr1_TS.EPSOS.TZ.OPT/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TS.EPSOS.TZ.OPT Time Stamp - Flavor epSOS

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TS.EPSOS.TZ.OPT** |<flag country="EU"/> ||

*SHALL be precise at least to the year, SHOULD be precise to the day, and MAY omit time zone.*


## Operationalization
Timezone optional
## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TS.EPSOS.TZ.OPT

Extends Rule(s): [TS](../DTr1_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| time SHALL be precise to at least the year|dtr1‑1‑TS.EPSOS.TZ.OPT|
|<warning/>| time SHOULD be precise to the day|dtr1‑2‑TS.EPSOS.TZ.OPT|


