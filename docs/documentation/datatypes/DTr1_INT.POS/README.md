---
permalink: /documentation/datatypes/DTr1_INT.POS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# INT.POS Integer - Flavor Interval of Integer, positive

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **INT.POS** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | int|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** INT.POS

Extends Rule(s): [INT](../DTr1_INT) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value &gt; 0|dtr1‑2‑INT.POS|


