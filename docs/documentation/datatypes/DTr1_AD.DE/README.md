---
permalink: /documentation/datatypes/DTr1_AD.DE/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# AD.DE Address - Flavor German Address

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **AD.DE** |<flag country="DE"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@isNotOrdered` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|
|`hl7:delimiter` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:country` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:state` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:county` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:city` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postalCode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumber` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:houseNumberNumeric` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:direction` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetName` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameBase` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:streetNameType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:additionalLocator` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitID` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:unitType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:careOf` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:censusTract` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryAddressLine` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationType` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationArea` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryInstallationQualifier` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryMode` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:deliveryModeIdentifier` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:buildingNumberSuffix` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:postBox` | 0 .. * | [ADXP](../DTr1_ADXP) |
|`hl7:precinct` | 0 .. * | [ADXP](../DTr1_ADXP) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** AD.DE

Extends Rule(s): [AD](../DTr1_AD) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| Deutsche Postleitzahlen in den Textknoten müssen im Format '99999' sein|dtr1‑1‑AD.DE|
|<error/>| In Deutschland sind bei Adressen zugelassen: delimiter, country, county, city, postalCode, houseNumber, buildingNumberSuffix, streetName, additionalLocator und useablePeriod. Gefunden '*$illegalAddressPart*'.|dtr1‑2‑AD.DE|
|<error/>| Nur postalCode, country und county können codiert werden. Gefunden '**[not(self::hl7:postalCode) and not(self::hl7:country) and not(self::hl7:county) and @code][1]/name()*'|dtr1‑3‑AD.DE|


