---
permalink: /documentation/datatypes/DTr1_TEL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TEL Telecommunication Address


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TEL** ||[TEL.AT](../DTr1_TEL.AT) [TEL.EPSOS](../DTr1_TEL.EPSOS) [TEL.IPS](../DTr1_TEL.IPS) [TEL.NL.EXTENDED](../DTr1_TEL.NL.EXTENDED) [TEL.CA.EMAIL](../DTr1_TEL.CA.EMAIL) [TEL.CA.PHONE](../DTr1_TEL.CA.PHONE) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:useablePeriod` | 0 .. * | [SXCM_TS](../DTr1_SXCM_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TEL

Extends Rule(s): [URL](../DTr1_URL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not null and useablePeriod|dtr1‑1‑TEL|


