---
permalink: /documentation/datatypes/DTr1_hl7nl_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# hl7nl:PQ Physical Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **hl7nl:PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@unit` |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7nl:translation` | 0 .. * | |


