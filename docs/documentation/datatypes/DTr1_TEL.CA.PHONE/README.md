---
permalink: /documentation/datatypes/DTr1_TEL.CA.PHONE/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TEL.CA.PHONE Telecommunication Address - Flavor Canadian Phone

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TEL.CA.PHONE** |<flag country="CA"/> ||

*TEL.PHONE Phone Number (CA constraints)*

*Allows Allows phone and fax numbers to be communicated*

*Additional guidance for DIR: to be used for immediate direct communications between providers (e.g. pharmacist to physician for potential medication interaction issues)*

*Note 1: No constraints have been applied to the content to restrict it to Canadian country codes*

*Note 2: No constraints have been applied to the format. Best practice is to always include country code and area code when known.*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TEL.CA.PHONE

Extends Rule(s): [TEL](../DTr1_TEL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| useablePeriod property is not permitted|dtr1‑1‑TEL.CA.PHONE|
|<error/>| @use is required|dtr1‑2‑TEL.CA.PHONE|
|<error/>| œuse may have up to three codes and must be drawn from 'EC', 'H', 'MC', 'PG', 'TMP', 'WP'|dtr1‑3‑TEL.CA.PHONE|
|<error/>| @value has maximum length of 40 characters|dtr1‑4‑TEL.CA.PHONE|
|<error/>| telecommunication scheme supported is "fax:" or "tel:"|dtr1‑5‑TEL.CA.PHONE|


