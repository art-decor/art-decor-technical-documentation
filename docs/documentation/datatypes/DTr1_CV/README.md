---
permalink: /documentation/datatypes/DTr1_CV/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CV Coded Value


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CV** ||[CO.EPSOS](../DTr1_CO.EPSOS) [CV.EPSOS](../DTr1_CV.EPSOS) [CV.IPS](../DTr1_CV.IPS) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |


