---
permalink: /documentation/datatypes/DTr1_IVXB_INT/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# IVXB_INT Interval Boundary of Integer


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **IVXB_INT** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | int|
|`@inclusive` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** IVXB_INT

Extends Rule(s): [INT](../DTr1_INT) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not inclusive if null|dtr1‑1‑IVXB_INT|


