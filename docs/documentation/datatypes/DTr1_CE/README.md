---
permalink: /documentation/datatypes/DTr1_CE/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CE Coded with Equivalents


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CE** ||[CO.EPSOS](../DTr1_CO.EPSOS) [CV.EPSOS](../DTr1_CV.EPSOS) [CV.IPS](../DTr1_CV.IPS) [CE.EPSOS](../DTr1_CE.EPSOS) [CE.IPS](../DTr1_CE.IPS) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |
|`hl7:translation` | 0 .. * | [CD](../DTr1_CD) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CE

Extends Rule(s): [CD](../DTr1_CD) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| cannot have qualifier|dtr1‑1‑CE|


