---
permalink: /documentation/datatypes/DTr1_PN/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# PN Person Name


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **PN** ||[PN.NL](../DTr1_PN.NL) [PN.CA](../DTr1_PN.CA) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@nullFlavor` |  optional  | cs|
|`delimiter` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`family` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`given` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`prefix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`suffix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`validTime` | 0 .. 1 | [IVL_TS](../DTr1_IVL_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** PN

Extends Rule(s): [EN](../DTr1_EN) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| Person names SHALL NOT contain a name part qualified with 'LS' (Legal status for organizations)|dtr1‑1‑PN|


