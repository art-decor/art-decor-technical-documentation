---
permalink: /documentation/datatypes/DTr1_hl7nl_IVL_QTY/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# hl7nl:IVL_QTY Interval of Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **hl7nl:IVL_QTY** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@xsi:type` |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`@lowClosed` |  optional  | bl|
|`@highClosed` |  optional  | bl|
|`hl7nl:low` | 0 .. 1 | |
|`hl7nl:high` | 0 .. 1 | |
|`hl7nl:width` | 0 .. 1 | |
|`hl7nl:any` | 0 .. 1 | |


