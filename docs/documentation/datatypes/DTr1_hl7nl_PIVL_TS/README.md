---
permalink: /documentation/datatypes/DTr1_hl7nl_PIVL_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# hl7nl:PIVL_TS Periodic Interval of Timezone - Datatypes Release 2 - Dutch extension import


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **hl7nl:PIVL_TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@alignment` ( CD \| CH \| CM \| CN \| CS \| CW \| CY \| D \| DM \| DW \| DY \| H \| HD \| J \| M \| MY \| N \| NH \| S \| SN \| W \| WY \| Y ) |  optional  | cs|
|`@isFlexible` |  optional  | bl|
|`@xsi:type` |  optional  | cs|
|`hl7nl:phase` | 0 .. 1 | |
|`hl7nl:period` | 0 .. 1 | |
|`hl7nl:frequency` | 0 .. 1 | |
|`hl7nl:count` | 0 .. 1 | |


