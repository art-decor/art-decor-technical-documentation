---
permalink: /documentation/datatypes/DTr1_list_int/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# list_int A sequence of raw digits for the sample values.


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **list_int** |||


## Registered Attributes and Child Elements
None.

