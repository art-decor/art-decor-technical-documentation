---
permalink: /documentation/datatypes/DTr1_SLIST_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SLIST_TS Sampled Sequence of Timestamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SLIST_TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`origin` | 1 .. 1 | [TS](../DTr1_TS) |
|`scale` | 1 .. 1 | [PQ](../DTr1_PQ) |
|`digits` | 1 .. 1 | [list_int](../DTr1_list_int) |


