---
permalink: /documentation/datatypes/DTr1_GLIST/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# GLIST Generated Sequence


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **GLIST** |||


## Registered Attributes and Child Elements
None.

