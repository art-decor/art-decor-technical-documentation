---
permalink: /documentation/datatypes/DTr1_thumbnail/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# thumbnail Thumbnail


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **thumbnail** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@mediaType` |  optional  | cs|
|`@language` |  optional  | cs|
|`@compression` ( DF \| GZ \| Z \| ZL ) |  optional  | cs|
|`@integrityCheck` |  optional  | bin|
|`@integrityCheckAlgorithm` ( SHA-1 \| SHA-256 ) |  optional  | cs|
|`@representation` ( B64 \| TXT ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:reference` | 0 .. 1 | [TEL](../DTr1_TEL) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** thumbnail

Extends Rule(s): [ED](../DTr1_ED) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| integrityCheckAlgorithm required if integrityCheck|dtr1‑2‑thumbnail|
|<error/>| no reference if null|dtr1‑3‑thumbnail|
|<error/>| thumbnails do not have thumbnails|dtr1‑4‑thumbnail|
|<error/>| compression only on binary|dtr1‑5‑thumbnail|
|<error/>| value implies mediaType is text/plain|dtr1‑6‑thumbnail|
|<error/>| no history or updateMode|dtr1‑7‑thumbnail|
|<error/>| no charset for value or xml|dtr1‑8‑thumbnail|
|<error/>| no mediaType if null|dtr1‑10‑thumbnail|
|<error/>| no charset if null|dtr1‑11‑thumbnail|
|<error/>| no language if null|dtr1‑12‑thumbnail|
|<error/>| no compression if null|dtr1‑13‑thumbnail|
|<error/>| no integrityCheck if null|dtr1‑14‑thumbnail|
|<error/>| no integrityCheckAlgorithm if null|dtr1‑15‑thumbnail|


