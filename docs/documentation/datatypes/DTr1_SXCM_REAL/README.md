---
permalink: /documentation/datatypes/DTr1_SXCM_REAL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SXCM_REAL Set Component of Real


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SXCM_REAL** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** SXCM_REAL

Extends Rule(s): [REAL](../DTr1_REAL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not inclusive if null|dtr1‑1‑SXCM_REAL|


