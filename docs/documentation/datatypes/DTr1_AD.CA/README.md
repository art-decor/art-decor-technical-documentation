---
permalink: /documentation/datatypes/DTr1_AD.CA/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# AD.CA Address - Flavor Canadian Address - supported address parts (part types) are country, city, state, postalCode, addressLine and up to four delimiter - address parts and delimiter-separated text has each a maximum length of 80 characters

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **AD.CA** |<flag country="CA"/> ||

*AD.CA Address (CA constraints)*

*Addresses are encoded with the street address sent as the value text of the address element, and additional address parts. Constraints for addresses are consistent with the requirements set out in the Pan Canadian Data Type Specification for the data type flavor AD.BASIC, with the exception of the fact that the pan Canadian Data Type specification pre-adopted coded state (province) and country elements from HL7’s release 2 Datatypes. Since CDA is fixed to release 1 datatypes, and the coded address parts are not available, the value for state and country address parts should be taken from the value sets assigned in the pan Canadian Datatype Specification.*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  | set_cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:delimiter` | 0 .. 4 | [ADXP](../DTr1_ADXP) |
|`hl7:country` | 0 .. 1 | [ADXP](../DTr1_ADXP) |
|`hl7:state` | 0 .. 1 | [ADXP](../DTr1_ADXP) |
|`hl7:city` | 0 .. 1 | [ADXP](../DTr1_ADXP) |
|`hl7:postalCode` | 0 .. 1 | [ADXP](../DTr1_ADXP) |
|`hl7:streetAddressLine` | 0 .. 1 | [ADXP](../DTr1_ADXP) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** AD.CA

Extends Rule(s): [AD.CA.BASIC](../DTr1_AD.CA.BASIC) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| @use may have up to three codes and must be drawn from 'H', 'PST', 'TMP', 'WP'|dtr1‑1‑AD.CA|
|<error/>| supported address parts (part types) are country, city, state, postalCode, streetAddressLine and up to four delimiter|dtr1‑2‑AD.CA|
|<error/>| address parts SHALL NOT contain @code|dtr1‑3‑AD.CA|


