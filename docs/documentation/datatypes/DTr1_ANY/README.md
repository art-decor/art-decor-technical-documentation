---
permalink: /documentation/datatypes/DTr1_ANY/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# ANY ANY


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **ANY** ||[AD.NL](../DTr1_AD.NL) [AD.DE](../DTr1_AD.DE) [AD.EPSOS](../DTr1_AD.EPSOS) [AD.IPS](../DTr1_AD.IPS) [AD.CA.BASIC](../DTr1_AD.CA.BASIC) [AD.CA](../DTr1_AD.CA) [CO.EPSOS](../DTr1_CO.EPSOS) [CV.EPSOS](../DTr1_CV.EPSOS) [CV.IPS](../DTr1_CV.IPS) [CE.EPSOS](../DTr1_CE.EPSOS) [CE.IPS](../DTr1_CE.IPS) [CD.EPSOS](../DTr1_CD.EPSOS) [CD.IPS](../DTr1_CD.IPS) [CD.SDTC](../DTr1_CD.SDTC) [CS.LANG](../DTr1_CS.LANG) [PN.NL](../DTr1_PN.NL) [PN.CA](../DTr1_PN.CA) [II.NL.AGB](../DTr1_II.NL.AGB) [II.NL.BIG](../DTr1_II.NL.BIG) [II.NL.BSN](../DTr1_II.NL.BSN) [II.NL.URA](../DTr1_II.NL.URA) [II.NL.UZI](../DTr1_II.NL.UZI) [II.AT.DVR](../DTr1_II.AT.DVR) [II.AT.ATU](../DTr1_II.AT.ATU) [II.AT.BLZ](../DTr1_II.AT.BLZ) [II.AT.KTONR](../DTr1_II.AT.KTONR) [II.EPSOS](../DTr1_II.EPSOS) [INT.NONNEG](../DTr1_INT.NONNEG) [INT.POS](../DTr1_INT.POS) [REAL.NONNEG](../DTr1_REAL.NONNEG) [REAL.POS](../DTr1_REAL.POS) [IVL_TS.EPSOS.TZ](../DTr1_IVL_TS.EPSOS.TZ) [IVL_TS.IPS.TZ](../DTr1_IVL_TS.IPS.TZ) [IVL_TS.EPSOS.TZ.OPT](../DTr1_IVL_TS.EPSOS.TZ.OPT) [IVL_TS.CH.TZ](../DTr1_IVL_TS.CH.TZ) [TS.DATE](../DTr1_TS.DATE) [TS.DATE.FULL](../DTr1_TS.DATE.FULL) [TS.DATE.MIN](../DTr1_TS.DATE.MIN) [TS.DATETIME.MIN](../DTr1_TS.DATETIME.MIN) [TS.DATETIMETZ.MIN](../DTr1_TS.DATETIMETZ.MIN) [TS.EPSOS.TZ](../DTr1_TS.EPSOS.TZ) [TS.IPS.TZ](../DTr1_TS.IPS.TZ) [TS.EPSOS.TZ.OPT](../DTr1_TS.EPSOS.TZ.OPT) [TS.CH.TZ](../DTr1_TS.CH.TZ) [TS.AT.TZ](../DTr1_TS.AT.TZ) [TS.AT.VAR](../DTr1_TS.AT.VAR) [TEL.AT](../DTr1_TEL.AT) [TEL.EPSOS](../DTr1_TEL.EPSOS) [TEL.IPS](../DTr1_TEL.IPS) [TEL.NL.EXTENDED](../DTr1_TEL.NL.EXTENDED) [TEL.CA.EMAIL](../DTr1_TEL.CA.EMAIL) [TEL.CA.PHONE](../DTr1_TEL.CA.PHONE) [URL.NL.EXTENDED](../DTr1_URL.NL.EXTENDED) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@nullFlavor` |  optional  | cs|
|`@xsi:type` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** ANY


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| if there is a nullFlavor, there shall be no text or other attribute or element, unless it's nullFlavor&#061;'OTH' or 'NA'          (@codeSystem, &lt;originalText&gt; or &lt;translation&gt; may have a value), or nullFlavor 'UNC' (@extension or &lt;originalText&gt; may have a value). Found: &lt;*name()**for $att in (@* except @nullFlavor) return concat(' ', name($att), '="', $att, '"')*&gt;*for $elm in * return concat('<', name($elm), '... />')*&lt;/*name()*&gt;|dtr1‑1‑ANY|


