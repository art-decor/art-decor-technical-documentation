---
permalink: /documentation/datatypes/DTr1_CV.IPS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CV.IPS Coded Value - Flavor IPS CV

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CV.IPS** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |
|`ips:designation` | 0 .. * | [ST](../DTr1_ST) |


