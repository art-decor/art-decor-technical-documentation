---
permalink: /documentation/datatypes/DTr1_IVXB_MO/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# IVXB_MO Interval Boundary of Monetary Amount


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **IVXB_MO** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@currency` |  optional  | cs|
|`@inclusive` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** IVXB_MO

Extends Rule(s): [MO](../DTr1_MO) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not inclusive if null|dtr1‑1‑IVXB_MO|


