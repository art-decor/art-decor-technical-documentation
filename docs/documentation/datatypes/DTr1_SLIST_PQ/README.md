---
permalink: /documentation/datatypes/DTr1_SLIST_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SLIST_PQ Sampled Sequence of Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SLIST_PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`origin` | 1 .. 1 | [PQ](../DTr1_PQ) |
|`scale` | 1 .. 1 | [PQ](../DTr1_PQ) |
|`digits` | 1 .. 1 | [list_int](../DTr1_list_int) |


