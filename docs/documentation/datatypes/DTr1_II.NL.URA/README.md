---
permalink: /documentation/datatypes/DTr1_II.NL.URA/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# II.NL.URA Instance Identifier - Flavor Dutch URA

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **II.NL.URA** |<flag country="NL"/> ||

*II Flavor .NL The Netherlands .URA Nederland, fixed OID 2.16.840.1.113883.2.4.6.3*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@root` |  optional  | oid|
|`@extension` |  optional  | st|
|`@assigningAuthorityName` |  optional  | st|
|`@displayable` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** II.NL.URA

Extends Rule(s): [II](../DTr1_II) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| UZI-registerabonneenummer @root MOET 2.16.528.1.1007.3.3 zijn indien niet null. Gevonden: "*@root*"|dtr1‑1‑II.NL.URA|
|<error/>| UZI-registerabonneenummer MOET 8 cijfers lang zijn. Gevonden: "*@extension*"|dtr1‑2‑II.NL.URA|


