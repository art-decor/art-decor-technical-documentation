---
permalink: /documentation/datatypes/DTr1_II.NL.UZI/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# II.NL.UZI Instance Identifier - Flavor Dutch UZI

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **II.NL.UZI** |<flag country="NL"/> ||

*II Flavor .NL The Netherlands .UZI Nederland, fixed OID 2.16.528.1.1007.3.1 (person) or 2.16.528.1.1007.3.2 (system)*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@root` |  optional  | oid|
|`@extension` |  optional  | st|
|`@assigningAuthorityName` |  optional  | st|
|`@displayable` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** II.NL.UZI

Extends Rule(s): [II](../DTr1_II) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| UZI-nummer @root MOET 2.16.528.1.1007.3.1 (Personen) of 2.16.528.1.1007.3.2 (Systemen) zijn indien niet null. Gevonden: "*@root*"|dtr1‑1‑II.NL.UZI|
|<error/>| UZI-nummer persoon MOET 9 cijfers lang zijn. Gevonden: "*@extension*"|dtr1‑2‑II.NL.UZI|
|<error/>| UZI-nummer systeem MOET 9 cijfers lang zijn. Gevonden: "*@extension*"|dtr1‑3‑II.NL.UZI|


