---
permalink: /documentation/datatypes/DTr1_BL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# BL Boolean


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **BL** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|


