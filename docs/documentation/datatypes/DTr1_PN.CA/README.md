---
permalink: /documentation/datatypes/DTr1_PN.CA/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# PN.CA Person Name - Flavor Canadian Person Name

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **PN.CA** |<flag country="CA"/> ||

*PN.CA Person Names (BC constraints)*

*Names are encoded as a sequence of Name Parts such as given name, family name, prefixes and suffixes. Constraints for names are consistent with the requirements set out in the Pan Canadian Data Type Specification for the data type flavor PN.BASIC, with some additional constraints described below.*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@nullFlavor` |  optional  | cs|
|`delimiter` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`family` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`given` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`prefix` | 0 .. * | [ENXP](../DTr1_ENXP) |
|`suffix` | 0 .. * | [ENXP](../DTr1_ENXP) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** PN.CA

Extends Rule(s): [PN](../DTr1_PN) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| given/@qualifier property is required|dtr1‑1‑PN.CA|
|<error/>| at present, the only value for name/given/@qualifier is IN (initial), indicating that the name part is just an initial|dtr1‑2‑PN.CA|
|<error/>| supported name parts (part types) are family, given, prefix, suffix|dtr1‑3‑PN.CA|
|<error/>| Exactly one [1..1] name/family part SHALL be present|dtr1‑4‑PN.CA|
|<error/>| One or more  [1..*] name/given parts SHALL be present|dtr1‑5‑PN.CA|
|<error/>| Zero or 1 [0..1] name/prefix name parts may be provided|dtr1‑6‑PN.CA|
|<error/>| Zero or 1 [0..1] name/suffix name parts may be provided|dtr1‑7‑PN.CA|
|<error/>| At most seven name parts may be present; since a family name part and at least one given name are mandatory, the cardinality for name parts is [2..7]|dtr1‑8‑PN.CA|
|<error/>| @use must be drawn from 'L', 'P', 'ASGN', 'C', 'HC'|dtr1‑9‑AD.CA|


