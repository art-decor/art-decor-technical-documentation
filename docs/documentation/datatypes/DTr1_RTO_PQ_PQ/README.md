---
permalink: /documentation/datatypes/DTr1_RTO_PQ_PQ/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# RTO_PQ_PQ Ratio Physical Quantity / Physical Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **RTO_PQ_PQ** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@nullFlavor` |  optional  | cs|
|`hl7:numerator` | 1 .. 1 | [PQ](../DTr1_PQ) |
|`hl7:denominator` | 1 .. 1 | [PQ](../DTr1_PQ) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** RTO_PQ_PQ

Extends Rule(s): [PQ](../DTr1_PQ) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| numerator or denominator required|dtr1‑1‑RTO_PQ_PQ|
|<error/>| no updateMode on numerator or denominator|dtr1‑2‑RTO_PQ_PQ|
|<error/>| no uncertainty|dtr1‑3‑RTO_PQ_PQ|
|<error/>| The denominator must not be zero.|dtr1‑4‑RTO_PQ_PQ|


