---
permalink: /documentation/datatypes/DTr1_SLIST/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SLIST Sampled Sequence


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SLIST** |||


## Registered Attributes and Child Elements
None.

