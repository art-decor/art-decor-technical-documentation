---
permalink: /documentation/datatypes/DTr1_REAL/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# REAL Real


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **REAL** ||[REAL.NONNEG](../DTr1_REAL.NONNEG) [REAL.POS](../DTr1_REAL.POS) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** REAL

Extends Rule(s): [QTY](../DTr1_QTY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value or child element in case of extension|dtr1‑1‑REAL|


