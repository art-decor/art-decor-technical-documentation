---
permalink: /documentation/datatypes/DTr1_CS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CS Coded Simple Value


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CS** ||[CS.LANG](../DTr1_CS.LANG) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CS

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| @code/@typeCode and @nullFlavor are mutually exclusive|dtr1‑1‑CS|
|<error/>| cannot have codeSystem|dtr1‑2‑CS|
|<error/>| cannot have codeSystemName|dtr1‑3‑CS|
|<error/>| cannot have codeSystemVersion|dtr1‑4‑CS|
|<error/>| cannot have displayName|dtr1‑5‑CS|
|<error/>| cannot have originalText|dtr1‑6‑CS|
|<error/>| cannot have qualifier|dtr1‑7‑CS|
|<error/>| cannot have translation|dtr1‑8‑CS|


