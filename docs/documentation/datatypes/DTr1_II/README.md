---
permalink: /documentation/datatypes/DTr1_II/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# II Instance Identifier


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **II** ||[II.NL.AGB](../DTr1_II.NL.AGB) [II.NL.BIG](../DTr1_II.NL.BIG) [II.NL.BSN](../DTr1_II.NL.BSN) [II.NL.URA](../DTr1_II.NL.URA) [II.NL.UZI](../DTr1_II.NL.UZI) [II.AT.DVR](../DTr1_II.AT.DVR) [II.AT.ATU](../DTr1_II.AT.ATU) [II.AT.BLZ](../DTr1_II.AT.BLZ) [II.AT.KTONR](../DTr1_II.AT.KTONR) [II.EPSOS](../DTr1_II.EPSOS) |


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@root` |  optional  | uid|
|`@extension` |  optional  | st|
|`@assigningAuthorityName` |  optional  | st|
|`@displayable` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** II

Extends Rule(s): [ANY](../DTr1_ANY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| @root is required if @nullFlavor is not present|dtr1‑1‑II|
|<error/>| @root may not be used if @nullFlavor is present|dtr1‑2‑II|
|<error/>| @identifierName may not be used if @nullFlavor is present|dtr1‑4‑II|
|<error/>| @displayable may not be used if @nullFlavor is present|dtr1‑5‑II|
|<error/>| @reliability may not be used if @nullFlavor is present|dtr1‑6‑II|
|<error/>| @assigningAuthorityName may not be used if @nullFlavor is present|dtr1‑7‑II|
|<warning/>| @root should not be longer than 128 characters. Found *string-length(@root)* characters. Please write a note to the authors of this rule if this is found to be not sufficient|dtr1‑8‑II|
|<warning/>| @extension should not be longer than 64 characters. Found *string-length(@extension)* characters. Please write a note to the authors of this rule if this is found to be not sufficient|dtr1‑9‑II|
|<error/>| @root SHALL be a syntactically correct OID or UUID.|dtr1‑10‑II|
|<error/>| @root SHALL NOT be a RUID. Identifiers in this scheme are only defined by balloted HL7 specifications. Local communities or systems must never use such reserved identifiers based on bilateral negotiations.|dtr1‑10a‑II|
|<info/>| Lower case UUID "*@root*" found in @root. UUIDs SHALL, under official HL7 V3 Datatypes Release 1 (and 2) rules, have upper case hexadecimal digits A-F. RFC 4122 and HL7 FHIR state lower case UUID display.|dtr1‑13‑II|

## Additional Strict Schematron Rules (SSR)
The following rules are defined in the strict mode of core schematrons.

|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| [RIM-002] *$name* SHALL be distinct|dtr1‑12‑II|


