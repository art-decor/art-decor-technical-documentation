---
permalink: /documentation/datatypes/DTr1_ENXP/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# ENXP Entity Name Part


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **ENXP** |||

*A character string token representing a part of a name. May have a type code signifying the role of the part in the whole entity name, and a qualifier code for more detail about the name part type. Typical name parts for person names are given names, and family names, titles, etc.*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@partType` |  optional  | cs|
|`@qualifier` |  optional  | set_cs|
|`@mediaType` |  optional  | cs|
|`@language` |  optional  | cs|
|`@compression` ( DF \| GZ \| Z \| ZL ) |  optional  | cs|
|`@integrityCheck` |  optional  | bin|
|`@integrityCheckAlgorithm` ( SHA-1 \| SHA-256 ) |  optional  | cs|
|`@representation` ( B64 \| TXT ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|
|`hl7:reference` | 0 .. 1 | [TEL](../DTr1_TEL) |
|`hl7:thumbnail` | 0 .. 1 | [thumbnail](../DTr1_thumbnail) |


