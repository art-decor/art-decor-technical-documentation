---
permalink: /documentation/datatypes/DTr1_EIVL_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# EIVL_TS Event-related time interval


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **EIVL_TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|
|`hl7:event` | 0 .. * | [EIVL.event](../DTr1_EIVL.event) |
|`hl7:offset` | 0 .. * | [IVL_PQ](../DTr1_IVL_PQ) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** EIVL_TS

Extends Rule(s): [SXCM_TS](../DTr1_SXCM_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| required attributes|dtr1‑1‑EIVL_TS|
|<error/>| no updateMode on EIVL attributes|dtr1‑2‑EIVL_TS|
|<error/>| null violation|dtr1‑3‑EIVL_TS|
|<error/>| cannot have codeSystem|dtr1‑4‑EIVL_TS|
|<error/>| cannot have codeSystemName|dtr1‑5‑EIVL_TS|
|<error/>| cannot have codeSystemVersion|dtr1‑6‑EIVL_TS|
|<error/>| cannot have displayName|dtr1‑7‑EIVL_TS|
|<error/>| cannot have originalText|dtr1‑8‑EIVL_TS|
|<error/>| cannot have qualifier|dtr1‑9‑EIVL_TS|
|<error/>| cannot have translation|dtr1‑10‑EIVL_TS|
|<error/>| offset null violation. Cannot have @nullFlavor and @value or child elements, or the other way around|dtr1‑11‑EIVL_TS|
|<error/>| offset null violation. Cannot have @nullFlavor and @value on any child elements|dtr1‑12‑EIVL_TS|
|<error/>| offset co-occurence violation. Cannot have @value and other child elements, or missing @value and child elements with data, or center element with other elements, or width element with both low and high elements|dtr1‑13‑EIVL_TS|
|<error/>| no updateMode on IVL attributes|dtr1‑14‑EIVL_TS|
|<error/>| value in offset/low/@value shall have same precision as offset/high/@value|dtr1‑15‑EIVL_TS|
|<error/>| width element: no unit without value|dtr1‑16‑EIVL_TS|
|<error/>| width element: no translation|dtr1‑17‑EIVL_TS|


