---
permalink: /documentation/datatypes/DTr1_CE.IPS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# CE.IPS Coded with Equivalents - Flavor IPS CE

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **CE.IPS** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@code` |  optional  | cs|
|`@codeSystem` |  optional  | oid|
|`@codeSystemName` |  optional  | st|
|`@codeSystemVersion` |  optional  | st|
|`@displayName` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:originalText` | 0 .. 1 | [ED](../DTr1_ED) |
|`hl7:translation` | 0 .. * | [CD](../DTr1_CD) |
|`ips:designation` | 0 .. * | [ST](../DTr1_ST) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** CE.IPS

Extends Rule(s): [CD.IPS](../DTr1_CD.IPS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| cannot have qualifier|dtr1‑1‑CE.IPS|


