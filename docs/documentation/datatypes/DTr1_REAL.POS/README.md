---
permalink: /documentation/datatypes/DTr1_REAL.POS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# REAL.POS Real - Flavor Real, positive

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **REAL.POS** |UV||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** REAL.POS

Extends Rule(s): [REAL](../DTr1_REAL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value &gt; 0|dtr1‑1‑REAL.POS|


