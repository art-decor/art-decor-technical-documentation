---
permalink: /documentation/datatypes/DTr1_SXPR_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SXPR_TS Parenthetic Set Expression of Time Stamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SXPR_TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|
|`hl7:comp` | 0 .. * | [SXCM_TS](../DTr1_SXCM_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** SXPR_TS

Extends Rule(s): [SXCM_TS](../DTr1_SXCM_TS) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or at least two components|dtr1‑1‑SXPR_TS|
|<error/>| null or at least two components|dtr1‑2‑SXPR_TS|
|<error/>| no updateMode on components or other subelements|dtr1‑3‑SXPR_TS|
|<error/>| null violation. Cannot have @nullFlavor and @value or child elements, or the other way around|dtr1‑4‑SXPR_TS|
|<error/>| co-occurence violation. Cannot have @value and other child elements, or missing @value and child elements with data, or center element with other elements, or width element with both low and high elements|dtr1‑5‑SXPR_TS|
|<error/>| comp/low/@value must have the same precision as comp/high/@value|dtr1‑6‑SXPR_TS|
|<error/>| width element: no unit without value|dtr1‑7‑SXPR_TS|
|<error/>| width element: no translation|dtr1‑8‑SXPR_TS|
|<error/>| null or value in period|dtr1‑9‑SXPR_TS|
|<error/>| no translation if null in period|dtr1‑10‑SXPR_TS|
|<error/>| null violation. Cannot have @nullFlavor and @value or child elements, or the other way around|dtr1‑11‑SXPR_TS|
|<error/>| co-occurence violation. Cannot have @value and other child elements, or missing @value and child elements with data, or center element with other elements, or width element with both low and high elements|dtr1‑12‑SXPR_TS|
|<error/>| comp/low/@value must have the same precision as comp/high/@value|dtr1‑13‑SXPR_TS|
|<error/>| value in period/@value SHALL be rounded to 4 decimals or less|dtr1‑14‑SXPR_TS|
|<error/>| width element: no unit without value|dtr1‑15‑SXPR_TS|
|<error/>| width element: no translation|dtr1‑16‑SXPR_TS|


