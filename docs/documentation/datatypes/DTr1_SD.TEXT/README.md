---
permalink: /documentation/datatypes/DTr1_SD.TEXT/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SD.TEXT Structured Document Text


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SD.TEXT** |||

*Structured Document Text (StrucDoc.Text), used in CDA Section.text*


## Registered Attributes and Child Elements
None.

