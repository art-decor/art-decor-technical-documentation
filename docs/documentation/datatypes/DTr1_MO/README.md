---
permalink: /documentation/datatypes/DTr1_MO/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# MO Monetary Amount


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **MO** |||

*Implementatiehandleiding HL7v3 Basiscomponenten, v2.2 - Datatype 1.0 MO - Monetary Amount*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | real|
|`@currency` |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** MO

Extends Rule(s): [QTY](../DTr1_QTY) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| null or value or child element in case of extension|dtr1‑1‑MO|


