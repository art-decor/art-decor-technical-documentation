---
permalink: /documentation/datatypes/DTr1_RTO/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# RTO Ratio Quantity / Quantity


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **RTO** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@nullFlavor` |  optional  | cs|
|`hl7:numerator` | 1 .. 1 | [QTY](../DTr1_QTY) |
|`hl7:denominator` | 1 .. 1 | [QTY](../DTr1_QTY) |


