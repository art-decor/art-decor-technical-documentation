---
permalink: /documentation/datatypes/DTr1_TEL.EPSOS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# TEL.EPSOS Telecommunication Address - Flavor epSOS

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **TEL.EPSOS** |<flag country="EU"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@use` |  optional  | set_cs|
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|
|`hl7:useablePeriod` | 0 .. * | [SXCM_TS](../DTr1_SXCM_TS) |

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** TEL.EPSOS

Extends Rule(s): [TEL](../DTr1_TEL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| Phone and fax numbers SHALL consist of an optional leading + for country code followed by digits 0-9. The only other allowable characters are parentheses (), hyphens - and/or dots. Pattern is '^\+?[0-9()\.-]+$'|dtr1‑TEL.EPSOS|
|<error/>| Phone and fax numbers SHALL have at least one dialing digit in the phone number after visual separators are removed.|dtr2‑TEL.EPSOS|


