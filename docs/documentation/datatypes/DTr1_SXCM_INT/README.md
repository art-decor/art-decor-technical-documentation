---
permalink: /documentation/datatypes/DTr1_SXCM_INT/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# SXCM_INT Set Component of Integer


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **SXCM_INT** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | int|
|`@operator` ( A \| E \| H \| I \| P ) |  optional  | cs|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** SXCM_INT

Extends Rule(s): [INT](../DTr1_INT) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| not operator if null|dtr1‑1‑SXCM_INT|


