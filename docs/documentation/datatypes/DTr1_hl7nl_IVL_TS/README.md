---
permalink: /documentation/datatypes/DTr1_hl7nl_IVL_TS/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# hl7nl:IVL_TS Interval of Time Stamp


| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **hl7nl:IVL_TS** |||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | ts|
|`@nullFlavor` |  optional  | cs|
|`hl7nl:low` | 0 .. 1 | |
|`hl7nl:width` | 0 .. 1 | |
|`hl7nl:high` | 0 .. 1 | |
|`hl7nl:any` | 0 .. 1 | |


