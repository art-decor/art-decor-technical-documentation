---
permalink: /documentation/datatypes/DTr1_URL.NL.EXTENDED/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# URL.NL.EXTENDED Universal Resource Locator - Flavor Dutch Extended

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **URL.NL.EXTENDED** |<flag country="NL"/> ||


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@value` |  optional  | st|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** URL.NL.EXTENDED

Extends Rule(s): [URL](../DTr1_URL) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with RFC 3966|dtr3‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with RFC 1738.|dtr4‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with RFC 2224.|dtr5‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with RFC 1738.|dtr6‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with RFC 1738|dtr7‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with RFC 1738.|dtr8‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with RFC 2368. Note that it is not allowed to carry extra headers|dtr9‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST start with "2.16.840.1.113883.2.4.6.6." prefixed with the numerical part|dtr10‑URL.NL.EXTENDED|
|<error/>| URIs with URL Scheme *$urlScheme* MUST comply with mllp://host:port/ where 'host' MAY be a name or IP and 'port' a named port or integer|dtr11‑URL.NL.EXTENDED|


