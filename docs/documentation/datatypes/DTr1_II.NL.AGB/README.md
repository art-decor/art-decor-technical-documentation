---
permalink: /documentation/datatypes/DTr1_II.NL.AGB/
description: PLEASE DO NOT EDIT THIS PAGE, IT IS GENERATED AND WILL BE OVERWRITTEN
---
# II.NL.AGB Instance Identifier - Flavor Dutch AGB

| [Datatype Release_1](../) | Realm | Flavors |
| :----------------- | :---- | :------ |
| **II.NL.AGB** |<flag country="NL"/> ||

*II Flavor .NL The Netherlands .AGB Nederland, fixed OID 2.16.840.1.113883.2.4.6.1*


## Registered Attributes and Child Elements
| @Attribute / Element | Cardinality | Datatype |
| :------------------- | :---------- | :------- |
|`@root` |  optional  | oid|
|`@extension` |  optional  | st|
|`@assigningAuthorityName` |  optional  | st|
|`@displayable` |  optional  | bl|
|`@nullFlavor` |  optional  | cs|

## Core Schematron Rules (CSR)
The following rules are defined in the base core schematrons (<error/> errors, <warning/> warnings and <info/> infos).

**Rule ID:** II.NL.AGB

Extends Rule(s): [II](../DTr1_II) 


|   Level    | Constraints | ID  |
| :--------: | :---------- | :-- |
|<error/>| AGB-code @root MOET 2.16.840.1.113883.2.4.6.1 zijn indien niet null. Gevonden: "*@root*"|dtr1‑1‑II.NL.AGB|
|<error/>| AGB-code moet 8 cijfers lang zijn. Gevonden: "*@extension*"|dtr1‑2‑II.NL.AGB|


