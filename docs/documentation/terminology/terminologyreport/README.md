---
permalink: /documentation/terminology/terminologyreport
---
# Terminology Report

Terminology plays an important role in the definition of standards in healthcare. Keeping track of referenced terminology by hand is a time consuming task and also prone to errors, this is where the Terminology Report function comes in.

The Terminology Report function scans a project and reports issues related to terminology (code systems) referenced in the project. This is especially useful to check if updates to code systems have consequences for a specific project.

The function will walk through datasets, transactions and Value Sets and check the following items in the project:

Datasets:

- Concept terminology associations
- Concept concept-list Value Set associations
- Concept concept-list-item Value Set item associations

Transactions:

- Concept terminology associations

Value Sets:

- Value Set items

::: note

Only items with status code ‘final’ and ‘draft’ items will be checked.

::: 

The report does not categorize messages as errors, warnings or informational messages but allows for filtering on individual message types.

It is possible to initiate a Terminology Report to check the terminologies used in a project. The report detects whether a coded concept in a Value Set has an illegal display name or is deprecated and should no longer be used.

Usually a Terminology Report check will be perfomed before creating a publication, as part of Preflighting publication and quality checks but could of course be run more often.

For projects with large Value Sets this can be a lengthy process.

## How to perform a Terminology Check

### What is performed?

The following checks are performed.

#### Value Sets

1. Value Sets with concepts/exceptions using a different displayName than the original codeSystem (All active names SNOMED, others usually have only one
2. valueSet with concepts/exceptions using deprecated codes (if code system has status information at all) 

#### Terminology Association

1. terminologyAssociation@code where
   - the association is based on dataset//concept and the @displayName differs from the codeSystem or if it is a local codeSystem available only in a local valueSet, then if it differs from the name in that local valueSet
   - the association is based on dataset//concept/conceptList/concept and the @displayName differs from the corresponding code in the valueSet that the conceptList is associated with, or if the conceptList doesn't have an association, then all local valueSets
2. If a terminologyAssociation connects to a dataset//concept then behavior matches a valueSet:
   - Get codeSystem names (e.g. SNOMED uses "http://[server]/terminology/snomed/getConcept/6797001")
   - If it can get at least one lower-case match, all is well
   - If there is none then for SNOMED the active *Fully Specified Name* (FSN) is returned or the only name in other cases (eg. LOINC) including status (if available)

If the terminologyAssociation is to a dataset//conceptList/concept:

- Search all local valueSets for the code+codeSystem
- If it can get at least one lower-case match, all is well
- If there is none, return all distinct matches from the valueSets

### Creating a terminology report

The check can be performed calling the Terminology Report service, offered in the Development Menu of a Project.

![image-20230523211516328](../../../img/image-20230523211516328.png)

A new terminology report can be created on the “Terminology Report” tab:

![image-20230523211530083](../../../img/image-20230523211530083.png)

The creation of a terminology report is implemented as a scheduled task so as not to block the application when processing a large project. When a new terminology report is created the request will be added to the processing queue:

![image-20230523211538319](../../../img/image-20230523211538319.png)

Depending on the size of the project processing can take a few seconds up to several minutes. The progress is reported in two stages:

![image-20230523211545952](../../../img/image-20230523211545952.png)

![image-20230523211551449](../../../img/image-20230523211551449.png)

When the processing has finished the report will be listed in the overview table:

![image-20230523211558562](../../../img/image-20230523211558562.png)

### Viewing the detailed terminology report

The details of the report can be viewed using the context menu at the end of the row:

![image-20230523211605883](../../../img/image-20230523211605883.png)

The context menu also allows for deleting an existing terminology report and for viewing the detailed report.

Depending on the size of the project it can take a few seconds to retrieve and show the report. By default none of the message types are selected for viewing so as not to clutter the report with messages that may not be relevant for the project or task at hand thus the headers of the expansion panels for datasets, transactions and Value Set show (0 of x) where x is the number of datasets, transaction or Value Sets in the project (with status code ‘draft’ or ‘final’).

![image-20230523211612152](../../../img/image-20230523211612152.png)

The dropdown list allows selecting message type for viewing. For a complete set of messages see below. It will only contain message types actually present in the report. Multiple message type can be selected this way.

<img src="../../../img/image-20230523211620631.png" alt="image-20230523211620631" style="zoom:67%;" />

The expansion panel marked ‘Overview’ lists the issues found in datasets, transactions and Value Sets along with the number of times they occur.

![image-20230523211631774](../../../img/image-20230523211631774.png)

Message types can also be selected or deselected for viewing by clicking on one of the rows in the overview. For instance, clicking on ‘Concept not found’ will add this message type to the selection and the expansion panel headers will show the number of datasets, transaction and Value Sets where this message occurs.

![image-20230523211639732](../../../img/image-20230523211639732.png)

### Dataset report

The expansion panel header shows the name of the dataset and its status code. For each concept the concept name and the report message text are always shown. The clicking on the name will open a new browser tab (or window depending on the browser settings) will be opened with the dataset and concept selected. If the concept has a concept list associated with it an icon will be present at the end of the row, clicking the icon will open a popup window with the concept list. If a concept shows a different message than the one selected for view, the selected message type occurs in the concept list. In the following example only ‘concept retired’ was selected as the message type:

![image-20230523211649558](../../../img/image-20230523211649558.png)

![image-20230523211658320](../../../img/image-20230523211658320.png)

Dependent on the type of message an ‘eye’ icon may be shown, clicking it will show message details. In this example the associated concept is from Snomed CT and clicking the ‘eye’ icon will show additional information with respect to the retired concept and suggest alternatives if available:

![image-20230523211704770](../../../img/image-20230523211704770.png)

### Value Set report

The expansion panel header shows the name of the Value Set and the status code. Clicking on the name will open the Value Set in a new browser tab (or window depending on the browser settings). Only Value Set members with the selected message type will be shown. For each Value Set member the code, display name and report message are displayed. Dependent on the type of message an ‘eye’ icon will be shown, clicking it will show message details.

![image-20230523211713704](../../../img/image-20230523211713704.png)

Popup window for ‘No matching designation’ message:

![image-20230523211721918](../../../img/image-20230523211721918.png)

### Terminology Report SNOMED expressions

The terminology report function offers basic support for checking **post-coordinated SNOMED expressions** covering the most common use cases. Examples are listed to demonstrate the feature.

::: example Concept refinement

83152002 |Oophorectomy|:

​    405815000 |Procedure device|= 122456005 |Laser device| 

:::

::: example Refinement with multiple attributes

71388002 |Procedure|:

    405815000 |Procedure device|= 122456005 |Laser device|,

​    260686004 |Method|= 129304002 |Excision - action|,

    405813007 |Procedure site - direct|= 15497006 |Ovarian structure| 

:::

::: example Refinement with attribute groups

71388002 |Procedure|:

​    {260686004 |Method|= 129304002 |Excision - action|, 

​     405813007 |Procedure site - direct|= 15497006 |Ovarian structure|}

​    {260686004 |Method|= 129304002 |Excision - action|, 

​     405813007 |Procedure site - direct|= 31435000 |Fallopian tube structure|} 

:::

::: example Reference set membership

^ 700043003 | Example problem list concepts reference set| 

:::

The expression dialog shows the expression and display name in the header and the parsed expression in the body:

<img src="../../../img/image-20230623191544414.png" alt="image-20230623191544414" style="zoom:60%;" />

## Listing of all possible messages

Legenda: <info/> Information, <warning/>  Warning, <error/> Error.

### Relevant in all cases

| Message                                                      | Type                | Description                                                  |
| ------------------------------------------------------------ | ------------------- | ------------------------------------------------------------ |
| OK                                                           | <info/>       | The concept, terminology association or Value Set item passed all tests. |
| Code system not found on server.                             | <warning/>             | The referenced code system could not be found on the server and as a result no validation of the referenced code was possible. |
| Concept not found in code system.                            | <error/>               | The referenced code could not be found in the referenced code system. This can be due to a typo in the code or leading or trailing spaces. |
| URI is not a valid code                                      | <error/>               | A code in a coded concept shall be just the code, not a complete URI. |
| Display name does not match any designation in concept.      | <error/>/<br /><warning/> | No matching designation could be found in the referenced coded concept. This is not necessarily an error and can be because the display name used in the project is in a language not present in the code system. The designations present in the code system can be shown to allow for manual validation. |
| Case of display name does not match designation case in concept. | <warning/>             | A matching designation could be found but the case is not exactly the same. This is a separate message from the one above to allow for filtering / ignoring this particular message. |
| Concept is in draft.                                         | <warning/>             | The referenced coded concept is still in draft.              |
| Concept is experimental.                                     | <warning/>             | The referenced coded concept is marked as experimental and may be cancelled in the future. |
| Concept is retired.                                          | <error/>/<br /><warning/> | The referenced coded concept is retired. If it concerns a concept from SNOMED-CT possible alternatives can be shown. |

### Relevant only to datasets

| Message                                                    | Type        | Description                                                  |
| ---------------------------------------------------------- | ----------- | ------------------------------------------------------------ |
| No terminology association defined.                        | <info/>     | No terminology association has been defined for this dataset concept. |
| No Value Set associated with concept list.                 | <info/>     | No Value Set has been associated with a concept list in a dataset concept. |
| No Value Set item associated with concept in concept list. | <info/>     | No Value Set item has been associated with a dataset concept in a concept list. |
| Value Set not found                                        | <error/>        | The Value Set associated with a concept list of a dataset concept could not be found. |

### Relevant only for SNOMED-CT post-coordinated expressions

| Message                                                      | Type        | Description                                                  |
| ------------------------------------------------------------ | ----------- | ------------------------------------------------------------ |
| Post-coordinated expression cannot be parsed                 | <info/>     | The function was unable to parse the post-coordinated expression. This could be due to an error in the expression itself of because the expression could not be handled by the function. |
| Equal sign is not allowed at start of a post-coordinated code. | <error/>        | When used as a code for a coded concept an expression must not begin with an equal sign. |
| Error in SNOMED-CT expression.                               | <error/>        | An error was found in the post-coordinated expression. This message occurs when an error was found relating to one of the concepts used in the expression. |
