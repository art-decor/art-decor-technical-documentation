---
permalink: /documentation/api-documentation/
---

# API-documentation

The ART-DECOR API contains RESTful services for DECOR and Terminology objects.

We use [swagger](https://swagger.io/) for the api-documentation. The link to the api-documentation is [https://io.art-decor.org/#/](https://io.art-decor.org/#/)

## How to use the api

For each endpoint the parameters, example query payload, responses and schema are described.

To use this endpoint you first click on the Try it out-button and fill in the parameters you want to add. Then click on Execute to get the reponse of it.
In this example we used GET/project to get a project list. 

![2022-07-13_11-01-53.png](../../img/2022-07-13_11-01-53.png)

## How to authenticate

Some of the api-endpoints requires an authentication first to have acces. To authenticate you have to create a new acces token.
To create a new accces token you use the endpoint POST/token en click on the Try it out-button. Fill in your username and password and then click on Execute.

![2022-07-13_10-06-57.png](../../img/2022-07-13_10-06-57.png)

In the responses you get a new token which you can copy and paste in the next step.

![2022-07-13_11-16-03.png](../../img/2022-07-13_11-16-03.png)

To authenticate you click on the Authorize-button. 

![2022-07-13_11-21-04.png](../../img/2022-07-13_11-21-04.png)

A new window will open where you can paste the token in the value field. And then click on Autorize. You can now also use the api endpoints which requires authentication.

![2022-07-13_11-22-54.png](../../img/2022-07-13_11-22-54.png)

## Postman configuration

You can also use the tool [Postman](https://www.postman.com/product/tools/) to have acces to and configure the api-endpoints. 

The website of Postman has a [learning Center](https://learning.postman.com/docs/getting-started/introduction/) where you are guided step by step on how to use Postman.

It is also possible to [export Postman data](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/#exporting-postman-data) as JSOn files. 







