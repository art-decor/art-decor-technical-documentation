---
permalink: /documentation/landing/
---
# The Landing Page

If you start ART-DECOR® the first you see is the Landing Page. The page offers you to choose a project to inspect or work with. 

![image-20210922174746304](../../img/image-20210922174746304-2325667.png)

## Menu Strip 

Section <ref1/>

## Detail Area and Top Strip 

The Detail Area is the right large part of the screen <ref2/>. It provides a top strip with the name of the currently selected project (if any). If the selected project supports multiple languagesthere is a project language switch available with flags to switch the language of the content (see also how to deal with [Multiple Languages](/documentation/languages)). 

 In addition there is a login button and a switch to change the User Interface Language.

## Project Choice

Section <ref3/>

Choose a project and start your endeavors.

Once a project is chosen you are forwarded automagically to the [Project Panel](/project).

## Software Summary

Section <ref4/>

## Services

Section <ref5/>

Please provide feedback if you like through the provided link.

## Server Summary

 Section <ref6/> summarizes some server properties informing about

- Cache freshness
- Overall statistics of artefacts 
- Configuration

### Cache freshness

The server cache for ART-DECOR artefacts is typically refreshed on a regular base. The "freshness" of the data in the cache is indicated as follows: 

- if cache data is up to 1440 minutes old (up to 24h) it is labeled as *dynamic*' (fresh) <StatusDot status="active" small/>
- if cache data is between 1440 and 2880 minutes old (up to 2 days) it is labeled as *static* <StatusDot status="pending" small/>
- if more than 2880 minutes / 2 days it is labeled as *withered*  <StatusDot status="retired" small/>

To refresh the cache manually see [Refresh cache](/administration//maintenance#refresh-cache).

### Overall statistics of artefacts

### Configuration
