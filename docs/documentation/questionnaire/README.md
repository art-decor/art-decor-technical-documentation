---
permalink: /documentation/questionnaire/
---

# Questionnaire

**Questionnaires** in ART-DECOR are versionable Objects that define forms with groups and enterable item fields that can subsequently be rendered as forms and populated with data. Although there is a proximity to **FHIR Questionnaires**, ART-DECOR Questionnaires are artifacts following the tool's principles. 

To document what ART-DECOR is supporting with regards to FHIR Questionnaires, we created an [Implementation Guide](https://ig.art-decor.pub/art-decor-questionnaires/) to define all features.

## Questionnaires and Responses

### Tree View

All Questionnaires of a Project are shown as usual in a Navigation Card to the left of the Questionnaires Panel (under Scenarios). All Questionnaires are listed alphabetically, associated Questionnaire Responses are shown as (always green) child items of the Questionnaire. Please note that Questionnaire Responses can be stored after populating a form, but ART-DECOR is a prtotyping tool and cannot be used for regular production Questionnaires.

<img src="../../img/image-20240731213642180.png" alt="image-20240731213642180" style="zoom:80%;" />

The Tree can be filtered, completely expanded or collapsed and the menu item offers to create a new Questionnaires (starting from scratch), to Transform an already defined Project Transaction into a Questionnaire.

<img src="../../img/image-20230210181412785.png" alt="image-20230210181412785" style="zoom:80%;" />

The Transform option opens a dialog to choose the Scenario Transaction Leaf to convert into an ART-DECOR Questionnaire. 

<img src="../../img/image-20240731213932417.png" alt="image-20240731213932417" style="zoom:67%;" />

After saving the request the transformation is done in the background and users on the Questionnaire panel are informed when the transformation is ready and the Questionnaire is shown in the tree.

::: note

If your project has not defined any FHIR services (see Project Overview - Services) then the following dialog invites you to defined such a FHIR service for your project.

<img src="../../img/image-20230210181425556.png" alt="image-20230210181425556" style="zoom:80%;" />

:::

### Questionnaires Details Card

The Details Card on the right shows the usual metadata for versionable objects. In addition there is an option to semantically annotate the whole questionnaire with one or more codes, see Card "Code (Questionnaire)", if they are not already promoted from the data element or scenario terminology association of the underlying concept. Furthermore, human readble tag names, see Tags, can be defined to the Questionnaire.

<img src="../../img/image-20240731214258593.png" alt="image-20240731214258593" style="zoom:80%;" />

A table reflecting the hierarchical list of the Items of the Questionnaire Definition. The list can be expanded or collapsed. Groups are shown with a folder icon, items are arranged with an icon indicating their Questionnaire item type.

<img src="../../img/image-20240731214215423.png" alt="image-20240731214215423" style="zoom:80%;" />

In Edit mode, items can be moved within the Questionnaire Tree to change the order of items using arrows on the right of the top bar. During edit, Text, Prefix, Definition, Cardinalities, Rendering Style, Answer Value Sets and Answer Options, Unit Options and more can be managed.

<img src="../../img/image-20240731214724923.png" alt="image-20240731214724923" style="zoom:80%;" />

Also structured Conditions can be created: a Question item must be chosen and a condition expression can be formed to define an "enable item" behavior, ie. whether an item in a rendered form is shown under certain conditions or not.

<img src="../../img/image-20240731220047272.png" alt="image-20240731220047272" style="zoom:67%;" />

### Questionnaire Item Types

Question Items have two levels of meaning. The first level distinguished between:

| **Item Type** | **Icon**            | **Explanation**                                              |
| :------------ | :------------------ | :----------------------------------------------------------- |
| Group         | <adimg     folder/> | An item with no direct answer but should have at least one child item. |
| Display       | <adimg     label/>  | Text for display that will not capture an answer or have child items |
| Question      | multiple            | An item that defines a specific answer to be captured, and which may have child items. The answer provided in the QuestionnaireResponse should be of the defined datatype. |

A Question can express expectations regarding the nature of the data when populating the form by means of Question **Item Types**.

| Item Type   | Icon                    | **Explanation**                                              |
| :---------- | ----------------------- | :----------------------------------------------------------- |
| Boolean     | <adimg     boolean/>    | Question with a yes/no answer                                |
| Decimal     | <adimg     decimal/>    | Question with is a real number answer                        |
| Integer     | <adimg     integer/>    | Question with an integer answer                              |
| Date        | <adimg     date/>       | Question with a date answer                                  |
| Date Time   | <adimg     datetime/>   | Question with a date and time answer                         |
| Time        | <adimg     time/>       | Question with a time (hour:minute:second) answer independent of date |
| String      | <adimg     string/>     | Question with a short (few words to short sentence) free-text entry answer |
| Text        | <adimg     text/>       | Question with a long (potentially multi-paragraph) free-text entry answer |
| Url         | <adimg     url/>        | Question with a URL (website, FTP site, etc.) answer         |
| Choice      | <adimg     choice/>     | Question with a Coding drawn from a list of possible answers (specified in either the answerOption property, or via the valueset referenced in the answerValueSet property) as an answer |
| Open Choice | <adimg     openchoice/> | Answer is a Coding drawn from a list of possible answers (as with the choice type) or a free-text entry in a string |
| Attachment  | <adimg     attachment/> | Question with binary content such as an image, PDF, etc. as an answer |
| Reference   | <adimg     reference/>  | Question with a reference to another resource (practitioner, organization, etc.) as an answer |
| Quantity    | <adimg     quantity/>   | Question with a combination of a numeric value and unit, potentially with a comparator (<, >, etc.) as an answer. |

Item text and other meta data can be edited accordingly.

<img src="../../img/image-20240731215305843.png" alt="image-20240731215305843" style="zoom:67%;" />

## Export/Download and Form Rendering

In the top bar there are three buttons offered, the usual raise-an-issue button, and especially for Questionnaires the Download and the Render button. 

<img src="../../img/image-20230620145425084.png" alt="image-20230620145425084" style="zoom:60%;" />

Clicking on the download button allows you to **export** the ART-DECOR Questionnaire as a **FHIR Questionnaire**. It opens a dialog that shows the available FHIR endpoints (version) of the Project and a format switch.

<img src="../../img/image-20230620145709908.png" alt="image-20230620145709908" style="zoom:60%;" />

The Render button opens a new window that shows the rendered Questionnaires. It uses the [LHC Forms](https://lhcforms.nlm.nih.gov) rendering machine from the Nation Library of Medicine. 

<img src="../../img/image-20230620145902029.png" alt="image-20230620145902029" style="zoom:67%;" />

You can also use the Render button to re-render already populated forms. You can even change the data and store the Questionnare Resposne again (overwriting the previous version).

<img src="../../img/image-20240731215707945.png" alt="image-20240731215707945" style="zoom:67%;" />

For a later Release we plan to offer the choice between different rendering machines.

## Transaction Items in Questionnaires

When transforming a Transaction with concept items into a Questionnaire, the concept properties are reflected in the Questionnaire.

Consider this example concept  as part of a *Vital Signs* Scenario.

<img src="../../img/image-20241119111857933.png" alt="image-20241119111857933" style="zoom:67%;" />

It contains a Name, Operationalization and a Description. When a Questionnaire is derived from this Transaction item the following table informs on how these items are reflected in automatically translated Questionnaires.

| **Dataset / Transaction Element**                            | **Example**                                                  | **If automatically translated into ART-DECOR Questionnaire** | If converted to FHIR Questionnaires                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Name                                                         | Pulse Regularity                                             | Questionnaire item text                                      | item.text                                                    |
| Operationalization if its content **does not contain a question mark** = is considered as help text or instruction | Place the tips of your first and second finger on the inside of the patient's wrist. Press gently against the pulse. Take your time to note any irregularities in strength or rhythm. If the pulse is regular and strong, **measure the pulse for 30 seconds**. | adding the original concept operationalization as Questionnaire item operationalization | converted as a subitem with item.text and itemControl helpButton |
| Operationalization if its content **does contain a question mark** = is considered as the real question | How regular is your pulse?                                   | item.text<br />The original concept name is added as Questionnaire item synonyms | item.text                                                    |
| Description                                                  | Pulse rhythm regularity                                      | -not at all-                                                 |                                                              |
| Comment                                                      | any text                                                     | -not at all-                                                 |                                                              |

