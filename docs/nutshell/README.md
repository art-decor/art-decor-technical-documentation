---
permalink: /nutshell
---
# What is ART-DECOR®?

::: tip ART-DECOR in four bullet points:

ART-DECOR is: 

<ref1/> TOOLING

...an **open-source open-source tool suite and collaboration platform** that aims on interoperability solutions in healthcare ....

<ref2/> TEAMWORK

...supporting **comprehensive collaboration of team members** within and between governance groups...

<ref3/> SHARING

... featuring a **cloud-based federated Building Block Repositories** (BBR)  for the creation and maintenance of Templates, Value Sets and other artefacts and thus foster re-use of specifications

<ref4/> PUBLISHING

... allowing **separation of concerns and different views** on a single documentation for different domain experts and common documentation done by multidisciplinary stakeholders of healthcare information exchange.

:::

The **Tooling** suite supports the creation and maintenance of data sets and scenarios to capture the medical expert needs, supports terminologies such as value sets to allow semantic annotations of the concepts and support HL7 templates, profiles and implementation guides to reflect technical representation of the data to be stored, exchanged or analyzed. It support FAIR Guiding Principles (Findability, Accessibility, Interoperability, and Reuse) for scientific data management and stewardship. **Teamwork** is central for this platform, smaller and larger teams can work collaboratively on artefacts in an sychronous or asychronous way. Best practics is **Sharing**, re-use of already published and shared interoperability art in own projects is fostered, once having own mature specifications they also can easly be shared with others. Finally, all good work is frozen and embraced in a **Publication**.

![image-20220630165306954](../img/image-20220630165306954.png)

## Collaboration platform

ART-DECOR is an open source collaboration platform for experts from medical, terminological and technical domains aiming on creation and maintenance of dataset with data element descriptions, use case scenarios, value sets and Health Level 7 (HL7) templates and profiles.

ART-DECOR allows to iteratively improve recorded data definitions and link together input from various experts with different background knowledge: caregivers, terminologist, modellers, analysts and interface/communication specialists.

### Caregivers  <adimg dataset/> <adimg scenario/>
<img src="../img/Wheelsv.png" alt="Wheelsv" style="zoom:20%; float:right;" />For caregivers, the tools mainly registers data elements and their collections as data sets with concepts, data types, allowable value ranges, identifications, codes, examples and business rules etc. In addition, use case specific Scenario's can be documented.

ART-DECOR typically identifies this area of interest with the <span style="background-color:#95C023; padding: 5px">green</span> color.

### Terminologists <adimg terminology/> <adimg identifier/>
From the base requirements, the caregivers documented, terminologist can add the appropriate codes, associate caregiver concepts with coded concepts and create and maintain the value sets to be used. Typically also namespaces, identifiers, URIs and OIDs are assigned.

ART-DECOR typically identifies this area of interest with the <span style="background-color:#4084BA; padding: 5px; color: white;">blue</span> color.

### Informaticians <adimg rules/> <adimg template/> <adimg profile/>
Modelers, analysts and interface / communication specialists add the technical representations as e.g. HL7 CDA templates, HL7 FHIR or IHE profiles. During that design phase and also in the following implementation and production phases the tool allows comprehensive validation and testing.

ART-DECOR typically identifies this area of interest with the <span style="background-color:#F0AF28; padding: 5px">orange</span> color.

### Project leads <adimg issue/>
During development and production phases stakeholders and team members of the governance group are supported by an artefact-aware issue and reporting management that enables effective change management of the artefacts.

ART-DECOR typically identifies this area of interest with the <span style="background-color:#EF5C50; padding: 5px; color: white;">red</span> color.

### Vendors and Interface Specialists <adimg implementation/>
Once a specification is ready for Implementation, ART-DECOR offers support to understand, implement and test the build.

ART-DECOR typically identifies this area of interest with the <span style="background-color:#A47874; padding: 5px; color: white;">bronze</span> color.

## What is in the name "DECOR"?

DECOR (Data Elements, Codes, OIDs and Rules) is a methodology to capture the data needs of caregivers in terms of datasets and scenarios and use it to generate various artefacts: documentation, value sets, XML instance validation, generation and processing support, and test tools etc.

The underlying DECOR data format is XML. The generation of HTML-, PDF- and Wiki-based documentation and XML-materials like validation and test environments is possible through a release and archive management function and transformations with stylesheets. The technical artefacts like templates and value sets can be used as input for test suites like IHE Gazelle ObjetcsChecker. Easy consistency checks across all artefacts can be achieved, CDA-based template definition can be verified to be standard conformant with the underlying CDA standard.

## What is in the name "ART"?
ART (Advanced Requirement Tooling) is the DECOR user interface to create and adapt DECOR files, and to generate artefacts from DECOR files.

+ ART is the backend, based on the eXist-db XML database.<br/><img src="../img/100px-Existdb-logo-2247834.gif" alt="Existdb-logo.gif" />
+ ART is also the frontend, until ART-DECOR 2.x, XQuery and Orbeon XForms is used.<br/><img src="../img/100px-Orbeon-logo-20210921201102531-2247884.png" alt="Orbeon-logo.png" />
+ ART-DECOR Release 3 is completely replacing the former UI platform Orbeon and uses Vue and Vuetify.<br/><img src="../img/30px-Vue-20210921201142188-2247904.png" alt="Vue.png" />

## Who benefits from ART-DECOR?

With ART-DECOR, experts with different backgrounds, such as healthcare professionals, terminologists, architects, designers and programmers together enter the necessary data, iteratively improve and connect those, all using the same underlying data store. ART-DECOR registers datasets, data types, value sets, identifications, codes, business rules, (HL7v3) templates and contains version management

## Use it for what?

ART-DECOR can be used by:
+ Regional and national networks for data exchange in healthcare.
+ Larger healthcare institutions or collaborations to improve the internal data exchanges.
+ Software vendors in healthcare looking for simpler ways to implement complex specifications.
+ IT service providers in healthcare such as ASPs.
+ All parties who facilitate complex data exchanges or data stores in healthcare.

## Use it for free!

ART DECOR is 100% free and:
+ Is the basis for capturing data definitions in healthcare.
+ Contains tools for governance and version management.
+ Supports various forms of publication.
+ Contains issue management to iteratively improve data definitions.
+ Enables and simplifies the creation and validation of examples and test scenarios.
+ Generates a web page on the fly to enter examples for each dataset.
+ Contains tools for conformance testing of participating parties.
+ Can be used by others as a basis for the construction of code generators, automatic questionnaires, user interfaces, conversions of data formats, database diagrams and much more.

## Used in practice!

ART DECOR is the base for specification, documentation and testing in more than 40 projects and used right now in Germany, Austria, Italy, Poland, Norway, Lithuania and the Netherlands, among other countries. National Infrastructure Programmes such as by Nictiz (the National Healthcare Standards Institute), RIVM (the Dutch National Public Health Authority), ELGA – Electronic Health Record Austria and eHealth Suisse in Switzerland, ART-DECOR® plays an important role. For more information about some of the projects see also our projects list page.

Take a look at
+ the demonstration #3 of ART-DECOR - Demo 3: Electrocardiogram Report as a Minimal CDA Document (switch to English language)
+ the DECOR site at Nictiz (in Dutch)
+ or download ART-DECOR from BitBucket.

A public ART-DECOR instance online is also available, which features a sandbox and demo environment. If you want to "play" with this project go to Sandbox: EKG Report CDA Document.

::: tip PLEASE NOTE
You need to log in before working in the sandbox. Use username *demo* and password *demodemo*. 

:::

::: warning

Please note that all changes to this project will be overwritten every day at 0300 Coordinated Universal Time UTC. We apologize to users who are in a time zone where this is in the middle of the day.
:::
