---
title: Legend
permalink: /legend
---
# Legend

ART-DECOR tries its best throughout the app to have a constistent look-and-feel and be supportive with icons and colors.

## ART-DECOR® Icons

ART-DECOR® uses icons to make orientation easier.

|                             Icon                             | Stakeholders                                                 |
| :----------------------------------------------------------: | ------------------------------------------------------------ |
|                 <adimg project/><br/>Project                 | All                                                          |
|                 <adimg dataset/><br/>Dataset                 | Caregivers and medical domain experts                        |
|               <adimg scenario/><br/>Scenarios                | Caregivers and medical domain experts                        |
| <adimg questionnaire/><br/>Questionnaires and Questionnaire Response | Caregivers and medical domain experts, Vendors and Interface Specialists |
| <adimg terminology/> <adimg identifier/><br />Terminology and Identifier | Terminologists                                               |
| <adimg template/><adimg profile/><br /> Template and Profile | Analysts, Modellers and Template/Profile Creators            |
|  <adimg issue/><br />Issue and Change Management, Reporting  | Project leads                                                |
|    <adimg implementationguide/><br />Implementation Guide    | Vendors and Interface Specialists                            |
| <adimg implementation/><br />Implementation Support, e.g. schematron | Vendors and Interface Specialists                            |

## ART-DECOR® Colors

ART-DECOR® uses colors to make orientation easier.

### Button color

Buttons that just change views and do not change any data are in green.

![image-20220927182340148](../img/image-20220927182340148.png)

Buttons that change data are typically in orange or even in red.

![image-20220927182312550](../img/image-20220927182312550.png)

### Status color

ART-DECOR uses colors thoughout the app to emphasize the status of the various artefacts. More information about the status machinery of corresponding artefacts [can be found here](../documentation/principles/#status-colors).

### Stakeholder color

We tend to color areas of the app for the differenz groups of stakeholders, along with "their" artefacts. More information about our addressed and distinct stakeholders and colors [can be found here](../nutshell/#collaboration-platform), some more background [here](../documentation/principles/#use-of-colors).

## Badges and Pills in this documentation

In this ART-DECOR documentation, some fetaures are used to help finding this quicker and easier.

Features in ART-DECOR Release 3 that are newly introduced with respect of ART-DECOR Release 2 functionality are marked as <new/> in this documentation.

Planned features in ART-DECOR Release 3 are marked <planned/> as such.

Features in ART-DECOR Release 2 that are no longer supported or managed differently in Release 3 are marked as <abandoned/>.

In Panel overwies and screenshots sometimes areas of interest are included and referenced in the text like this <ref1/>.