Introduction
============

This is a test scenario for ART-DECOR information standards. During the test an ART-DECOR project is recreated, based on the demo5 project from ART-DECOR examples (see see [[[1]](https://acceptance.art-decor.org/ad/#/demo5-/project/overview)] or [[[2]](https://bitbucket.org/art-decor/art-decor-backend/src/master/decor/data/examples/demo5/demo5-decor.xml)]).

To execute the scenario, you need at least two user accounts:

1.  A DBA user for creating the project

2.  A Project administrator for populating the project

3.  A general author account

Server preparation
==================

-   Create a restore instance: ./exist_stop_backup.sh --BackupLabel LABEL

    -   This way we can edit data in the database, and go back to a restore point before we changed data

-   Run updates:

    -   backend: make sure the latest API, etc are all installed

    -   frontend: update vue by running on the server/SSH:
        ```
        echo "update art-decor vue frontend" ; cd /opt/art-decor-vue/; ./vue_pull.sh --local
        ```
    
        Or see the documentation:
        `https://docs.art-decor.org/administration/setupmaintain/frontend3/#install-vue-frontend`

-   Enable/disable the right server functions. `https://docs.art-decor.org/administration/frontendfeatures/#disable-frontend-functionality`

Steps to perform for this scenario
==================================

User create
-----------

-   Add a user on the server, so that we can add it to projects later on.

As dba-user: add user `testuser{date}`. 

Enter values for all fields.

Groups: ada-user, decor, decor-admin, issues, editor, terminology

Save the user.

User edit
---------

Change email for just created user, save.

Change Organization, and save.

Change Description, and save.

Add a group 'debug', save.

Change password, and save.

Precondition
------------

Server has building block repositories configured. To configure those:

`https://acceptance.art-decor.org/ad/#/server-admin/art-settings`

![External repository servers](../img/configure-building-block-repos.png)

URL: `http://art-decor.org/decor/services/`

Via the edit-option → select, to select BBR projects: ad1bbr & ad2bbr

End result:

![External repository](../img/configure-building-block-repos2.png)

-   Refresh cache

`https://acceptance.art-decor.org/ad/#/server-admin/server-management`

![Refresh cash](../img/refresh-cash.png)


Start with clean setup
----------------------

Delete the project if it already exists.

This can be done *only* by having low level access with admin rights to the underlying eXist database: Delete the collection the project's data resides in. 

For this test project we want to remove the following collections:

/db/apps/decor/data/projects/test

/db/apps/decor/history/test

/db/apps/decor/releases/test

/db/apps/terminology-data/codesystem-stable-data/projects/test-

Run the following query (for example, in eXide, New XQuery, paste the query, then eval):  

```
xquery version "3.0";
try {
xmldb:remove('/db/apps/decor/data/projects/test'),
xmldb:remove('/db/apps/decor/history/test'),
xmldb:remove('/db/apps/decor/releases/test'),
xmldb:remove('/db/apps/terminology-data/codesystem-stable-data/projects/test-')
}
catch *
{ '' }
```

Create a project test
---------------------

Login as the DBA user. See [ART_maintenance_manual#Creating_a_project](https://docs.art-decor.org/index.php?title=ART_maintenance_manual#Creating_a_project) for further instructions.

| Property | Example |
| ------ | ----- |
| Name | Test: Vital Signs CDA Document |
| OID | 2.16.840.1.113883.3.1937.99.99.906 |
| prefix | test- |
| language | en-US |
| Experimental | yes |
| Copyright - Contributor | Add your or a random name  |
| Description | Test: Vital Signs Report Summary CDA Document |
| Authors | Add the project administrator as an author <br/> Test user 1 = normal project author (without decor-admin) <br/> Test user 3 = author, and project admin |

After clicking "Create Project" you return on an empty "new project" screen. No indication is given that the project was created successfully.

Edit project settings
---------------------

Login as project administrator and open the test project you just created.

`https://acceptance.art-decor.org/ad/#/test-/project/overview`

### Overview

1.  Add or change project description. Add the current date.


### Copyright

1.  Add a Contributer by clicking the +.

2.  Enter name and contact information.

3.  Enter the correct Copyright years.

4.  Add a logo from your computer by clicking on add and then browse.

5.  Enter contact information: add an entry, and select the type.

### Repositories

1.  Add building block repositories with prefixes 'ad1bbr-' & 'ad2bbr-' & 'ccda-' by clicking on the + next to 'Repositories'.

### Services

Add a FHIR service: Documentation: `https://docs.art-decor.org/documentation/project/#services-card`

-   Add a FHIR service by clicking Add:

![Add FHIR Service](../img/add-fhir.png)

Select Format as r4 and save:

![Add FHIR Service](../img/add-fhir2.png)

### Authors tab

1.  Add an author by clicking on the +. Add the user we created above to this project.

### Identifiers tab

For more information: [Project editor manual](https://docs.art-decor.org/index.php?title=ART_Project_Editor#Maintaining_a_local_OID_Registry).

1.  Add an other Id by clicking on the +.

2.  Fill in the Id (e.g. 1.2.3), Synonym, Language and some Display name

-   (No tests yet defined for this section.) Adding/editing Base ids: not yet included in the test. Documentation: `https://docs.art-decor.org/documentation/project/#base-ids`

### Governance Groups tab

For more information: [Dealing with governance groups](https://docs.art-decor.org/index.php?title=Dealing_with_Governance_Groups).

1.  Make the project member of a governance group (e.g. ART-DECOR) by clicking on the + to the left of the governance group name.

Datasets
--------

Login as project author. For this section, click on the Datasets menu (in the top menu bar).

### Basic dataset changes

Use the "Datasets" tab. Make some basic changes to the test dataset:

1.  Add a version label.

2.  Fill in a dataset description

3.  Name the dataset: Test dataset

4.  Save

### Add concepts

For more information check the [Dataset editor manual](https://docs.art-decor.org/documentation/dataset/).

Use the "Concepts Test dataset" tab. Recreate the concept "Person" (and underlying concepts) from the demo5 project dataset, see [[[3]](https://art-decor.org/ad/#/demo5-/project/overview)].

1.  Add the Person concept by clicking on the +

2.  Enter Description, Synonym, Source, Rationale, Operationalization, Comment.

3.  For each concept:

    1.  Select the right kind of type (group or item)

    2.  Add a name for the concept.

    3.  At 'Value' select the type

4.  Metadata for the first item concept.

    1.  For all metadata (Synonyms, Operationalization, ..) enter bogus values.

    2.  Save.

### Import LOINC panel

While in datasets: Import a LOINC panel (/ item bank)

1.  Make sure the user has selected the details of a dataset. For instance a dataset concept is selected

2.  Menu: Import LOINC panel

3.  Search for code: 62337-1

4.  Select: PROMIS item bank - 29 profile

5.  Save

While in datasets: Import a LOINC panel (/ item bank)

1.  Make sure the user has selected the details of a dataset. For instance a dataset concept is selected

2.  Menu: Import LOINC panel

3.  Search for code: 76805-1

4.  Select: PROMIS short form - physical function 8b ...

5.  Save

Check the preview screen to that:
Expected outcome: The second import will create inherited concept from the first item bank. 

Checks:

-   check that both LOINC panels are present as sets of concepts in the dataset

-   later we run a decor check to check for project health.

Scenarios
---------

For this section, click on the Scenarios menu (in the top menu bar).

### Add actors

Use the Actors tab.

1.  Add two actors by clicking on the +

2.  Person: actor1/actor1

3.  Organization: organization1/organization1

4.  Change type for organization to type device.

### Scenarios tab

Use the Scenarios tab.

1.  Add a scenario by clicking the + next to Scenarios.

    1.  Fill in a name and description for the new scenario.

    2.  Next.

    3.  Fill in a name for the new transaction group: group1

    4.  Fill in a type and name for the new transaction leaf: leaf1 (type = initial). Select the 2 actors.

    5.  Fill in a type and name for the new transaction leaf: leaf2 (type = back). Select the 2 actors.

    6.  Save.

2.  Change the second transaction:

    1.  Change the type to: *Registry*.

    2.  Remove the actor receiver.

For leaf1 transaction:

-   Source-dataset: edit and select our dataset.

-   Add Concepts to the created transaction. Add all concepts (0..1 R) we created in our dataset. Click save and then close the window to go back to the transaction.

-   edit the transaction. Change cardinality and conformance for last concept. Save.

-   edit the transaction. Select first concept. Change conformance to C. Add textual condition: description: condition1, 0..1, R. Ok. Save.

![Add condition](../img/scenario-condition.png)

-   edit the transaction. Select next concept. Change conformance to C. Add structured condition:

    -   question: select a concept,

    -   operator: exists;

    -   answertype: answerBoolean

    -   value: true

    -   Also add a textual condition, same as before

    -   save

-   Add a transaction leaf *Label: value:* schematronlabel1

    -   This prevents an error later: *Transaction with a representing template SHALL have a schematron label.*

Value Sets (in Terminology)
---------------------------

Terminology menu (in the top menu bar) and choose Value Sets.

For more information: [Value Set Editor manual](https://docs.art-decor.org/index.php?title=ART_Value_Set_Editor)

1.  Create a new valueset.

    1.  Click on the add/+ above the tree view on the left-hand side. This opens the browser.

    2.  Search for term *asthma*. Select 195967001 / asthma from SNOMED. Include: extensional

    3.  Search for term *acute*. Select LA18821-1 / acute from LOINC. Include: extensional

    4.  Select SNOMED. Search for term *eiwit*. Select 256443002. Include: intensional, code and all children

    5.  Select SNOMED. Search for term *eiwitarm*. Select 160673009. Include: intensional, all children only

    6.  Select SNOMED. Search for term *voorlichten*. Select 54070000 / postpartum voorlichten. Exclude: extensional

    7.  Select SNOMED. Search for term *cong ana*. Select 63565007

        Congenital anemia (disorder). Exclude: intensional, code and all children

    8.  Select SNOMED. Search for code 419891008. Select Record artifact (record artifact). Exclude: intensional, all children only

    9.  Add value set

    10. Enter display name for value set: valuesettest

    11. Save

2.  Expand the valueset just created. Save the expansion. Check that the save expansion is listed under Usage.   
    Expected number of codes: 5.  
    256443002 has one child code: 2 codes  
    160673009 has one child code: children only 1 code.  
    Is 5 codes in total.  

3.  1.  For the valueset: Fill in Version label

    2.  Add the valueset/description

4.  Add valueset

    -   Add value, scroll below to composition.

        1.  Add manual concept by clicking on the Add.

        2.  Concept: Fill in (Add another item in between codes)

            | Level | Type (L, A, S, D) | code (required) | Display name | Ordinal | CS |
            | ----- | ----------------- | --------------- | ------------ | ------- | -- |
            | 0 | L | 243324 | test-1 | 0 | 2.3.66.5 |
            | 0 | L | 243325 | test-2 | 1 | 2.3.66.5 |
            | 0 | A | 243326 | test-3 |   | 2.3.66.5 |
            | 1 | L | 243327 | test-4 | 3 | 2.3.66.5 |
            | 1 | A | 243328 | test-5 |   | 2.3.66.5 |
            | 2 | S | 243329 | test-6 |   | 2.3.66.5 |
            | 3 | L | 243330 | test-7 | 3 | 2.3.66.5 |

    -   Save the value set: *levels*

5.  Clone a valueset. Clone the just created valueset. Keep Id's = true. Expected: this should create a new version of the existing valueset (with datetime set to today)

6.  Clone a valueset. Clone the just created valueset. Keep Id's = false. Expected: this should create a new valueset in the valueset tree, with the same name.

7.  Create a reference

    1.  Precondition: repository reference to a bbr project is already present

    2.  Click on 'New reference' / "chain" icon above the tree view on the left-hand side

    3.  Search for a valueset, and select it to save it to this project

        1.  Search for 'patch'

        2.  Select 2.16.840.1.113883.1.11.14499 / PatchDrugForm (from bbr project ad2bbr).

8.  Clone the referenced VS PatchDrugForm, Keep ids = true.

9.  Clone the referenced VS PatchDrugForm, Keep ids = false. Expected: this should create a new valueset in the valueset tree, with the same name.

10. Create a reference to valueset 'Administrative Gender' from project ccda-

    1.  New reference

    2.  search: '*administrative*'

    3.  Select: 2.16.840.1.113883.1.11.1 Administrative Gender (HL7 V3)

11. Reference to VSs from the current project is not a desired option. No tests.

**Terminology associations**

1.  Create a terminology association between data concept, Person / Name and SNOMED concept

    1.  On terminology / associations

    2.  Select dataset, Person / Name

    3.  Concept Associations: Search. Terminology browser should open.

    4.  Search: '*name*'. Select 703503000 / Name from SNOMED.

    5.  Save.

    6.  Check: Go to dataset, Person / Name. Under terminology association should show the SNOMED code.

2.  Create a terminology association between data concept, Gender, and referenced valueset Administrative Gender (HL7 V3)

    1.  On terminology / associations

    2.  Select dataset, Person/Gender

    3.  Add Value Set Associations: Administrative Gender (HL7 V3), as dynamic.

    4.  Check: Go to dataset, Person / Gender. Under value set association should show codes from the valueset: Female, ..

3.  Create a Identifier association between data concept, National Patient Identifier, and id 1.2.3  

    1.  On terminology / associations

    2.  Select dataset, Person / National Patient Identifier

    3.  Identifier Associations: Create association, OID registries = true. Search for 'identi'. Select 2.16.840.1.113883.3.2562 / PIP. No tests yet defined for this section.: maybe find a better usecase here. Documentation is missing: `https://docs.art-decor.org/documentation/terminology/#terminology-associations-panel`

    4.  Check: Go to dataset, Person / National Patient Identifier. Should show: Identifier associations with PIP.

Templates
---------

For this section, click on the Templates menu (in the top menu bar) and choose the Templates menu entry.

For more information: `https://docs.art-decor.org/documentation/template/`

1.  Create template from scratch. New.

    1.  Create a couple elements and attributes. Save.

2.  Create a template. Click 'New from prototype' next to tree view. A new window opens.

    1.  Search 'CDA Act'

    2.  Filter on project: ad1bbr-

    3.  Select: 2.16.840.1.113883.10.12.301 / CDA Act

    4.  Edit template

    5.  Set hl7-subject: Conf to NP. Save the template

    6.  Re-edit the template

    7.  classCode, actions: unselect.

    8.  hl7:id, actions: unselect.

    9.  Save the template

    10. Re-edit the template. Check that both are shown as greyed out: classCode and hl7:id.

-   Scenarios: relate the selected template to our created transaction

Issues
------

For this section, click on the Issues menu (in the top menu bar).

For more information: `https://docs.art-decor.org/documentation/issue/`

On: `https://acceptance.art-decor.org/ad/#/test-/issues/issues`

1.  Add an issue by clicking on the right: New issue

    1.  Select the correct type (incident, change request, for future consideration, request for information/education): Incident

    2.  Fill in a title: testissue1

    3.  Select the desired priority (Lowest, Low, Normal, High, Highest): low

    4.  Fill in description: description1

    5.  Assign issue to an author: demo-project-admin

    6.  Save

    7.  Link to a dataset: Concerns: Our dataset.

    8.  Link to a dataset concept: Concerns: National patient identifier.

    9.  Concerns: Click National patient identifier, Go to. Check that in the dataset, this concept has *Issues (1)* shown.

Questionnaire
-------------

-   Form: Rules/Questionnaire: transform from transaction leaf1

-   Display the just transformed Questionnaire: Render

-   For item 1, select an answer from the dropdown, Save

-   Find the Questionnaire in the project index

Code system
-----------

-   Create code system

    -   Create code system from scratch

        -   Name/display/description: Organisation

        -   Codes: 1, 2, 3

        -   Add a designation, fully specified name on 1: code-1

        -   Save

    -   Clone just created CS. Keep ids = true. This will create a version of the CS.

    -   Edit the oldest version of just cloned CS.

        -   In the old version, *move* code 1 below code 2.

        -   Save. The newest CS should have the original order: 1, 2, 3.

    -   Create code system reference

        -   Search for: 'cod'. Select: CodingRationale / 2.16.840.1.113883.5.1074

        -   No tests yet defined for this section: Which bbr is this CS coming from? 

        -   No tests yet defined for this section: Find a better example from ad1/ad2bbr. 

-   Complete code system

    -   In terminology/valuesets. Create new VS. Down below: composition, Add.

    -   Complete code system.

    -   Select code system for context: Just created CS above, static.

    -   Add, Add value set: *includeCS*

    -   Expand: Should show the codes from CS.

        -   No tests yet defined for this section: for this example, the expand is empty. 

**FHIR profile**

-   (not included in 3.8.3 release, so for a later release) Options:

    -   New

    -   New from prototype

    -   New reference

    -   Clone

    -   Associate to transaction

    -   Associate to Questionnaire?

**Publication**

-   Log in as demo-project-admin

-   Check-decor on `https://acceptance.art-decor.org/ad/#/test-/project/development`

    -   Enable all options

    -   No tests yet defined for this section: CDA Act shows a error, Maybe use another template. 

-   Terminology-report on `https://acceptance.art-decor.org/ad/#/test-/project/development`

-   Create a new publication

    -   Intermediate version

-   Issues:

    -   Add a new event for the issue we created

    -   Change status from open → closed for the issue. (Now it should show up in the release notes).

-   Create a new publication

    -   Release

        -   In overview, set the publication location to: https://acceptance.art-decor.org/pub/

        -   Create a release: Add a Release label: version 1.0.0

    -   Development release

        -   To do: add documentation on this procedure.

        -   Find the development release at: `https://acceptance.art-decor.org/decor/services/RetrieveProject?prefix=test-&version=development&format=html&language=en-US&ui=en-US&mode=verbatim&download=false&force=false`

    -   Release with publication request without parameters and no filters

    -   Release with publication request

        -   with all parameters active

        -   Add filters

**Versioning**

**Dataset Versioning**

1.  Change the dataset

    1.  name to: *Test dataset 2*

    2.  status to: final (green bullet, top right)

    3.  status to: Deprecated (Blue bullet, top right)

Project index (To do) Add an instruction on how to test with this.

**Various findings from earlier (not part of our normal test)**

-   Demo5- shows Code system reference not found on: `https://acceptance.art-decor.org/ad/#/demo5-/terminology/codesystems/2.16.840.1.113883.5.1`

-   Frontend (for later) `https://acceptance.art-decor.org/ad/#/test-/project/overview` Overview shows 'Templates'. Expected something like Rules, to be able to click to profiles.
