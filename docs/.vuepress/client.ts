import { defineClientConfig } from '@vuepress/client'

import Layout from './theme/layouts/Layout.vue'
import NotFound from './theme/layouts/NotFound.vue'

import { 
  countertoken,
  counterurl
}
  from '../counter/info.js'

import axios from 'axios';
import { provide, ref } from 'vue'

axios.defaults.baseURL = counterurl

export default defineClientConfig({
  setup() {
    const visitCount = ref(0)
    provide('visitCount', visitCount)
  },

  enhance({ app, router }) {
    app.config.globalProperties.$visitCount = 0

    const token = countertoken
    const config = {
      url: '',
      method: 'get',
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    }

    router.beforeEach(async (to, from, next) => {
      next();

      if (to.path === from.path) {
        // Don´t make the call for subroutes
        return;
      }

      const page = to.path
      config.url = `/counter?token=${token}&page=${page}`
      // console.log('visitCount call: ', config.url)

      if (countertoken !== 'CTOKEN') {
        try {
          const res = await axios(config)
          app.config.globalProperties.$visitCount = res.data?.count || 0
          // console.log('visitCount: ', res.data?.count, ' page: ', to.path)
        } catch (error) {
          // console.log('visitCount error: ', error)
        }
      }
    })
  },

  layouts: {
    Layout,
    NotFound
  }
})
