import {
  defaultTheme
} from 'vuepress'
import {
  registerComponentsPlugin
} from '@vuepress/plugin-register-components'
import {
  containerPlugin
} from '@vuepress/plugin-container'
import {
  fullTextSearchPlugin
} from "vuepress-plugin-full-text-search2";
import {
  path
} from '@vuepress/utils'
import {
   transcludemd
} from './transcludemd.js'
 

export default {
  theme: defaultTheme({
    title: 'ART-DECOR®',
    description: 'The Technical Documentation of ART-DECOR®',
    port: 8082,
    logo: "/topmenu-ad-logo2.png",
    lastUpdated: true,
    lastUpdatedText: 'Last Update',
    colorModeSwitch: false,
    smoothScroll: true,
    navbar: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'Introduction',
        link: '/introduction/'
      },
      {
        text: 'User Manual',
        link: '/documentation/'
      },
      {
        text: 'Legend',
        link: '/legend/'
      },
      {
        text: 'ART-DECOR® Home',
        link: 'https://art-decor.org/'
      },
    ],
    sidebar: [
      {
        text: 'Introductions',
        link: '/introduction/',
        collapsible: true,
        collapsed: true,
        sidebarDepth: 3,
        children: [
          {
            text: 'What is ART-DECOR®?',
            link: '/nutshell/',
          },
          {
            text: 'Who we are!',
            link: '/adeg/',
          },
          {
            text: 'ART-DECOR® Open Tools',
            link: '/adot/',
          },
          {
            text: 'Interoperability',
            link: '/introduction/interoperability/',
          },
          {
            text: 'ART-DECOR® Feature Ensemble',
            link: '/features/',
          },
          {
            text: 'Project',
            link: '/introduction/project/',
          },
          {
            text: 'Dataset',
            link: '/introduction/dataset/',
          },
          {
            text: 'Scenario',
            link: '/introduction/scenario/',
          },
          {
            text: 'Questionnaire',
            link: '/introduction/questionnaire/',
          },
          {
            text: 'Terminology',
            link: '/introduction/terminology/',
          },
          {
            text: 'Value set',
            link: '/introduction/valueset/',
          },
          {
            text: 'Code system',
            link: '/introduction/codesystem/',
          },
          {
            text: 'Identifier',
            link: '/introduction/identifier/',
          },
          {
            text: 'Rules',
            link: '/introduction/rules/',
          },
          {
            text: 'Template',
            link: '/introduction/template/',
          },
          {
            text: 'Profile',
            link: '/introduction/profile/',
          },
          {
            text: 'Issue',
            link: '/introduction/issue/',
          },
          {
            text: 'Inheritance / Containment',
            link: '/introduction/inheritcontain/',
          },
          {
            text: 'Associations and Mappings',
            link: '/introduction/associations/',
          },
          {
            text: 'Implementation Guide',
            link: '/introduction/implementationguide/',
          },
          {
            text: 'Publication',
            link: '/introduction/publication/',
          },
          {
            text: 'HL7 CDA Core Principles',
            link: '/introduction/cda/',
          },
          {
            text: 'ART-DECOR® – Copyright',
            link: '/copyright/',
          },
          {
            text: 'ART-DECOR® – Licenses',
            link: '/licenses/',
          },
        ]
    },
    {
      text: 'Documentation',
      link: '/documentation/',
      collapsible: true,
      collapsed: false,
      sidebarDepth: 1,
      children: [
        {
          text: 'Principles of Use',
          link: '/documentation/principles/',
          collapsible: true,
        },
        {
          text: 'Starting with ART-DECOR®',
          link: '/documentation/starter/',
        },
        {
          text: 'The Landing Page',
          link: '/documentation/landing/',
        },
        {
          text: 'Project',
          link: '/documentation/project/',
        },
        {
          text: 'Dataset',
          link: '/documentation/dataset/',
        },
        {
          text: 'Scenario',
          link: '/documentation/scenario/',
        },
        {
          text: 'Terminology',
          link: '/documentation/terminology/',
        },
        {
          text: 'Value set',
          link: '/documentation/valueset/',
        },
        {
          text: 'Code system',
          link: '/documentation/codesystem/',
        },
        {
          text: 'Template',
          link: '/documentation/template/',
        },
        {
          text: 'Profile',
          link: '/documentation/profile/',
        },
        {
          text: 'Questionnaire',
          link: '/documentation/questionnaire/',
        },
        {
          text: 'Issue',
          link: '/documentation/issue/',
        },
        {
          text: 'Datatypes',
          link: '/documentation/datatypes/',
        },
        {
          text: 'Multiple Languages',
          link: '/documentation/languages/',
        },
        {
          text: 'Using Temple',
          link: '/documentation/temple/',
        },
        {
          text: 'Artefact Types',
          link: '/documentation/artefacts/',
        },
        {
          text: 'ART-DECOR® Repositories',
          link: '/documentation/repositories/',
        },
        {
          text: 'ART-DECOR® FHIR® Features',
          link: '/documentation/fhir/',
        },
        {
          text: 'ART-DECOR® LOINC® Panels',
          link: '/documentation/loinc-panels/',
        },
        {
          text: 'ART-DECOR® FHIR Server',
          link: '/documentation/fhirserver/',
        },
        {
          text: 'ART-DECOR® – Releases',
          link: '/documentation/releasenotes/',
        },
        {
          text: 'Publications about ART-DECOR®',
          link: '/documentation/papers/',
        },
        {
          text: 'Newsletters',
          link: '/documentation/newsletters/',
        },
      ]
      },
    {
      text: 'Administration',
      link: '/administration/',
      collapsible: true,
      collapsed: true,
      sidebarDepth: 1,
      children: [
        {
          text: 'System Administration',
          link: '/administration/',
        },
        {
          text: 'Frontend Features',
          link: '/administration/frontendfeatures/',
        },
        {
          text: 'Server Maintenance Periods',
          link: '/administration/maintenance/',
        },
        {
          text: 'Live on Air',
          link: '/administration/liveonair/',
        },
        {
          text: 'Setup and Maintenance',
          link: '/administration/setupmaintain/',
        },
        {
          text: 'Updating a Release',
          link: '/administration/releaseupdate/',
        },
        {
          text: 'Migration Support',
          link: '/administration/migration/',
        },
        {
          text: 'Documentation Helpers',
          link: '/administration/dochelper/',
        },
      ]
    },
    {
      text: 'Classic Release 2',
      link: '/release2/',
      collapsible: true,
      collapsed: true,
      sidebarDepth: 1,
      children: [
        {
          text: 'Orbeon/Tomcat Installation',
          link: '/release2/installation/',
        },
        {
          text: 'Migrating eXist-db 2.2 to Release 3',
          link: '/release2/migration/',
        },
        {
          text: 'Special Features',
          link: '/release2/specials/',
        },
      ]
    }
  
    ],
    extendsMarkdown: (md) => {
      // use more markdown-it plugins!
      // md.use(transcludemd)
      md.use(transcludemd)
    }
  }),
  plugins: [
    registerComponentsPlugin(
      {
        componentsDir: path.resolve(__dirname, './components'),
      },
    ),
    containerPlugin(
      {
        type: 'definition',
        before: info => `<div class="custom-container definition"><p class="title">${info ? `${info}` : 'DEFINITION'}</p>\n`,
        after: () => '</div>'
      }),
    containerPlugin(
      {
        type: 'note',
        before: info => `<div class="custom-container note"><p class="title">${info ? `${info}` : 'NOTE'}</p>\n`,
        after: () => '</div>'
      }),
    containerPlugin(
      {
        type: 'examples',
        before: info => `<div class="custom-container example"><p class="title">${info ? `${info}` : 'EXAMPLES'}</p>\n`,
        after: () => '</div>'
      }),
    containerPlugin(
      {
        type: 'example',
        before: info => `<div class="custom-container example"><p class="title">${info ? `${info}` : 'EXAMPLE'}</p>\n`,
         after: () => '</div>'
      }),
    containerPlugin(
      {
        type: 'example',
        before: info => `<div class="custom-container example"><p class="title">${info ? `${info}` : 'EXAMPLE'}</p>\n`,
         after: () => '</div>'
        }),
    containerPlugin(
      {
        type: 'constraint',
        before: info => `<div class="custom-container constraint"><p class="title">${info ? `${info}` : 'CONSTRAINT'}</p>\n`,
        after: () => '</div>'
        }),
    containerPlugin(
      {
        type: 'owl',
        before: info => `<div class="custom-container owl"><img src="https://docs.art-decor.org/mdi/owl.png" alt="Owl"/>`,
        after: () => '</div>'
      }),
    containerPlugin(
      {
        type: 'underconstruction',
        before: info => `<div class="custom-container underconstruction"><img src="https://docs.art-decor.org/mdi/hammer-wrench.png" alt="Wrench"/><span class="ctext">This part is under construction.<br/></span>`,
        after: () => '</div>'
      }
    ),
    fullTextSearchPlugin()
  ]
}