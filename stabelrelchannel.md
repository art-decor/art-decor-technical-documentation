

# Release Channel "stable"

::: definition

This Release channel contains the latest stable versions of everything. Stability is guaranteed. See Release Channel Development for information on the development releases.

:::

## ART Advanced Requirements Tooling

The ART web application is the main web application of ART-DECOR. It contains required logic for CRUD operations on DECOR files and various other related parts

2021-09-21 version 2.0.20
* Dataset
    * Fixed situation were a concept item that should have inherited could end up being contained
    * Restored searching by id
* Template
    * Fixed issue with retrieving templates from the live BBR server
    * Performance by using cache before doing http
* Template-mapping
    * Dataset tree did not do fallback to other language when active language has no name
* Value sets
    * Performance by using cache before doing http
* Users
    * Fixed filter for active/inactive
    * Fixed issue in selecting users based on their group membership
* Miscellaneous
    * Prevent error based on empty compiled/@on attribute in fixing permissions

2021-08-19 version 2.0.19
* '''<span style="color:red;">Security</span>'''
    * Add some safety. Cannot create users matching protected groups like dba and decor. Remove empty group matching new user before account creation (or it will fail)
    * Fix for [#154440](NCSC-NL) CVD report: cross-site-scripting-xss
* Dataset
    *  Fixed issue in inheritance
    * Fixed situation were a concept item that should have inherited could end up being contained
    * Restored searching by id
* Scenario
    * Make sure we show an available actor name/desc if the language version of choice is not present
    * Fix permissions error for guest access
* Template
    * Fixed issue with retrieving templates from the live BBR server
* Template-mapping
    * Improved performance by not refreshing associations list after create/delete
    * Don't save elementName or other non-core attributes on a template association
* Compilation
    * Improved retrieval of datasets if multiple versions exist and/or when inherits points to contains
* Misc
    * Fixed filter for active/inactive users in user admin
    * Don't add an empty valueSet if there is no association for it in fullDatasetTree
    * Improved support for projectVersion='' for getting the latest release
    * Add function to delete all but the newest three runtime compilations for all projects

2021-05-08 version 2.0.18
* Transaction
    * Fixed behavior for min/max when switching from/to NP
* Compilations
    * Upped version to 2.0.18. Rewritten compilation for performance and accuracy
    * Fixed a compilation error that could lead to duplicate datasets in the output
* Misc
    * Fixed an error processing li elements into markdown
    * Add fullDatasetTree url and ident. Efficiency in art:getNameForOID

2021-04-21 version 2.0.17
* FHIR
    * Fixed an issue when more than one hit (uri) is found for an OID
* CodeSystem
    * Add support for codeSystem.conceptList.codedConcept effectiveDate and expirationDate
    * Fixed an issue that would incorrectly serialize decor|codeSystem|valueSet nodes
    * Support codedConcept/desc
    * Add CodeSystem.language and concept.display
* Dataset
    * Convert client side filtering of similar results to server side filtering
    * Convert server side searching for concepts in current dataset to client side search
* Project
    * Add small tweak: only show releases dialoog when there are releases
    * Improved what comes into view when all is well in project development
    *  Allow selection of earlier release to base notes on when publishing a new release
* Template
    * nested dependencies shall not be more far away than 5 levels
* ValueSets
    * Add hint on IntensionalOperators
    * Updated order in the value set list to use the latest display name
* Generic
    * Improve naming of templates in filter.xml for partial publication
    * Add compilation of ids based on identifierAssociation
    * Make sure art-data itself has correct permissions
    * Add RBAC for project section of a project
    * Allow xs:documentation as xforms:hint in the absence of hints
    * Documented an exist-db5 specific problem for parseNode()
    * Improved returning json reliability in various places

2021-02-05 version 2.0.16
* Admin
    * Don't allow new project if lower-case() matches any existing lower-case() prefix
    * Fixed issue where template from cache did not get proper ns declarations
* Dataset
    * Allow status manipulation of a concept that contains another concept
    *  Fixed a UI bug causing a notion of max valueSet contents reached not to be displayed
    * Allow relationships to inherit/contains. Filter by project. Status icon
    * Add support for choosing concept baseId in creating a new dataset
    * Fixed a bug in defaultBaseId retrieval when multiple baseIds exist and new style default marking is used
    * Performance improvement upon switching datasets
    * Merge community info based on id and filter for non-matching flexibility
    * Allow for non-originals to be found in searching relationships
    * Improve concept/contains and relatipnship when ref != original
    * Improved properties for inherit/contains
* Generic
    * Implement/fix html to markdown conversion for description
    * New service for adding a new scenario based on a previous scenario but with new dataset version 
    * Consider @groups on refset applications in building the menu structure
    * Show DECOR locks in the list in green if they are not older than 24h
    * Fixed a problem in calling compileDecor without runtimeOnly boolean
* Issue
    * Condensed rendering of issue objects
    * Fixed technical error in function call that was not in use by Orbeon dependent code
    * Fix performance and meta set (only list relevant authors)
* MyCommunity
    * Added support for format parameter. Defaults to xforms invoking normal behavior with serialized descriptions. Other format values returns raw xhtml/xml descriptions
* Project
    * Don't write project/version/@statusCode = ''
    * Fixed an issue were statusCode on a version was not possible/visible
* Scenario
    * Don't allow max=0 + conformance=R, C or M
* Template
    * Namespace declarations in same order as other repos
* Terminology
    * Proper support for refsets in case of deprecation
* ValueSet
    * Fixed a problem in calling for valueSetList with version but without codeSystem
    * Allow intensional valueSets in view like << [    * Get SNOMED desc from preferred language if possible in editor
    * Add support for deprecation of completeCodeSystem and refsets 

2020-06-06 version 2.0.15
* Datasets
    * For dataset lists copy other dataset elements as well
    * Fixed issue in searching for concepts by id
* Issues
    * Improved readability/usability of object selection dialog for concepts
    * Fixed a problem where issues connected to multiple versions of the same object would return only one of those
* Project
    * Allow removal of project languages other than default language
    * Allow multi lingual edit of project descriptions
    * Support edit of older release notes for releases and versions
* Development
    * Add count for error|info|warning role in schematron
    * Now uses DECOR.sch instead of DECOR.xsd for Schematron rules (need DECOR-core update)
    * Added new datatype support for GLIST and SLIST
* Templates
* Support for getting transaction references when relevant
    * Added new datatype support for GLIST and SLIST

2020-04-28 version 2.0.14
* Development
    * Added option to check without compiling the project first, speeding getting results up significantly. Tradeoff is that some warnings about unresolved references are not to be avoided if they come from a BBR -- needs DECOR-core package 2.0.11 or up to work

2020-04-17 version 2.0.13
* Scenarios
    * Add two buttons on active groups that reads "All O" and "All R" with mouse over hints that explain the feature "All optional, except when conditional" and "All required, except when conditional" respectively.
* Issues
    * Add support for searching issues by last modified date
* Server settings
    * Fixed a problem in displaying a repository name when the last selected language was something other than available in the names
* General
    * Fixing ART permissions now includes cleaning up decor/tmp by leaving only the latest compilation
    * Fixed minor compilation problem processing valueSet/conceptList/*/desc | designation. Order was reversed

2020-04-11 version 2.0.12
* Datasets
    * Add break-lock functionality upon edit and status change
    * Fixed issue with findings datasets when searchString is empty
    * Support for new valueDomain/@type 'time'
    * Fixed id display for contain on inherit
    * Fixed searching for similar concepts upon changing the maximum number
    * Fix for misaligned ordinal column values without header
    * Fixed problem in context when no concept is found
* Scenarios
    * Add break-lock functionality upon edit and status change
    * Fixed an issue that would cause additional transaction concepts to appear (empty groups)
* Terminology
    * Fixed a problem that caused inability to see certain terminologyAssociation when the concept inherited from a containment
* Templates
    * Now closes the status change dialog if all is well and disables the buttons while waiting for that to happen
* Project
    * Introduce development tab containing a growing number of maintenance features
    * Implemented support for bulk removal of user locks
    * Add self-links to tabs
* Community
    * Fixed bug in creation of ADA community
* General
    * Links to Temple updated
    * Menu now display home before explore
    * User management including project authors now takes active status of user into account
    * Add function in fix-permissions that does cleanup of older ocmpilation in decor/tmp
    * Switching Orbeon version now also updates the forms accordingly
    * Fixed a bug in getFullConcept that would cause the dataset concept assoociations to be returned instead of the transaction concept associations when called for a transaction concept that overrides the dataset associations. This by accident also triggered a json related issue in RetrieveTransaction
    * Fixed serialized nodes in enhanced value sets. they are now parsed xml
    * Some performance rewrites
    * Consistency fix in Dutch. retired is 'obsoleet'. deprecated is 'verouderd'

2019-02-07 version 2.0.11
* Datasets
    * Fixed searching for similar concepts upon changing the maximum number
    * Fixed iddisplay for contain on inherit
* Templates
    * Now closes the status change dialog if all is well and disables the buttons while waiting for that to happen
* General
    * Links to new Temple (ART-DECOR XML Editor)

2019-12-24 version 2.0.10
* Datasets
    * Fix for misaligned ordinal column values without header
    * Fixed problem in context when no concept is found
* Scenarios
    * Fixed an issue where transactions/scenarios from other projects would surface if you selected a dataset from your project that was in use somewhere else.
* Value sets
    * Fixed serialized nodes in enhanced value sets. they are now parsed xml
* Project
    * Add self-links on tabs
    * Tweaks for the versions status dialog
* Community
    * Precaution against empty @ref attribute
    * Fixed bug in creation of ADA community
    * Markup changes in xform
* General
    * Add controller.xql (saves log warnings)

2019-10-16 version  2.0.9
* Codesystem-ids
    * CodeSystem Ids adds link to RetrieveOID
* Datasets
    * Allow adding concept in an empty dataset
    * Fixed exceptional situation where the target concept does not exist (anymore)
* Project
    * Fixed a bug that would could loss of identifierAssociations
    * Fixed a bug that would miss saving new defaults on existing baseIds as defaultBaseIds
* Terminology
    * Fixed problems in adding identifierAssociations from a repository concept to a concept that inherits from it in a different project
    * Add support for multilingual ClaML
* Transactions/Templates
    * Improved min/max checking. min always <= max
    * Improved attribute handling. Can no longer lower constraint compared to prototype
    * Simplified attribute retrieval for edit. No more @isOptional / @prohibited, but @conformance = NP/O/R. Template is saved with normal @isOptional / @prohibited as before
* ValueSets
    * Add support for multilingual ClaML

2019-09-29 version 2.0.8
* Compilation
    * When dataset is not from the same project as the transaction: get communities from other project as well
    * Fixed issue in iddisplay generation when the dataset is from a different project than the transaction
* Datasets
    * Allow manual relationship entry when the search option will not yield anything (usually multi server which is not supported today)
* Explorer
    * Add support for project selector to trim results in view
* MyCommunity
    * When community is not from the same project as the transaction: add @projectId and @ident onto <community/> element in the concepts
    * Improved community handling when some communities might be from a different project based on imported datasets
* Project
    * Reactivated development option in compiling a publication. This allows for test driving a release before actually committing it
* Project development
    * Add support for forced recompile (default='true') even if a compilation newer than the project exists. This is useful when a connected BBR was updated but the project was not
* Scenarios
    * Fixed a problem in adding new actors when none exist yet for the defaultBaseId
    * Fixed a problem in the retrieval of associations for transaction concepts
* Templates
    * Improved feedback on changing the status. Now displays a list of every affected template and the result
    * Improved rendering template names for new associations
* Terminology/Dataset
    * Add dataset ordinal support on conceptList/concept
* Value sets
    * Allow removal of references, e.g. for when the base name changed so you may re-add it
* General
    * Fixed a problem in getting associations for code bound to multiple parts of a concept
    * getFullDatasetTree now also returns the transaction/@type as @transactionType if called for a transaction
    * Fixed community prototype handling when the prototype is through ref
    * Moved internal RetrieveTransaction merge logic for communities to art:getFullDataset()
    * When dataset is not from the same project as the transaction: get communities from other project as well
    * When community is not from the same project as the transaction: add @projectId and @ident onto <community/> element in the concepts
    * When community is not from the same project as the transaction: fix for various empty @iddisplay attributes in the fullDataset
    * When transaction concept is missing conformance: don't write empty attribute.

2019-07-19 version 2.0.7
* Datasets
    * Performance update for datasets with lots of sibling concepts
* MyCommunity
    * Fixed a problem in language selection when clicking the paperclip for selflink
    * Fixed visual bug in the community editor for concepts with containment and with/without notes attached
    * Simplified concept tree selection based on datasets form
* Project
    * Prevent problems based on whitespace in project/reference/@url
    * Fixed an issue where clicking the "Add version/release" button would not yield the right UI for adding it
    * Reactivated the development release option in creating releases as it is useful in (granted only some) cases. Added explicit help text on what to expect
    * Add support for section=community to switch to the community tab
    * Now recreates the version-development collection if it exists so we don't have potential left over output from a previous test
    * For development mode, the compilation results are post populated with decor/@versionLabel and the project(version|release), so the verbatim copoy is the original project but the compilation are more representative for testing
    * Sort by displayName or by name if former is missing
    * Prevent casing problems and http/https problems in local BBR configs
* Compilations
    * For transactiondatasets xml, add @versionLabel only if available
    * For compilations xml, add @versionLabel only if available (would write empty if there wasn't one)
    * Fixed an issue that caused referred datasets from transactions not to be marked with ident/url as opposed to contained datasets
    * Add support for alternatives for <=? and >=?
    * Fixed problem in calculating shortNames when more than one preceding-sibling has the same name 
    * fullDataSet now compiles in any guest readable community so Retrievetransaction does not have to
    * fullConcept tries to complete terminologyAssociations with a displayName
    * Add compilation attributes to concept/relationship to point to the relevant concept
    * Add schematron checks specifically for compilation that check if relationships resolve
* General
    * Fixed a problem in retrieving a project when no language was provided and the project was not compiled using its project/@defaultLanguage
    * Fixed a problem in project retrieval when version was not xs:dateTime, but e.g. 'development'
    * Add function for retrieval of communities in releases
    * Fix permissions on DECOR objects for dba. This allows a dba to add a user to a project for example.
    * Workaround for Orbeon CSS bug where xf:output/xf:label leads to display problems. All these xf:label elements were hidden anyway so disabling them did not have any side effect
    * Force zebra stripe coloring when it is applied. This avoids conflicts with .not-selectable and repeat styles

2019-05-30 version 2.0.6
* Issues
    * Improved issue object rendering for transactions
    * Added explicit error situation for when the same object exists in multiple projects
    * Fixes rendering and linking issues with DECOR issues from different projects based on the object link. The link to the full issue now works, and labels display correctly
* Datasets
    * Improved similar concept search: only return active concepts (draft/pending/final). Allows post search filtering for origin project. Add result counter
    * Fixed a problem in iddisplay calculation for dataset/@id when the map does not contain this iddisplay yet
    * Fixed a problem in getFullConcept when the inherit leads to yet another inherit that somewhere down the line turns out to be a contains.
    * Fixed an issue in adding a title to a valueDomain/example when the title was not added before
* Project
    * Fixed an issue in displaying the @lastmodified for a project when the attribute is not present
* Scenarios
    * Fixed an issue in dynamically associating a dataset with a transaction
    * Fixed getting transaction concept context
    * Added feedback for when dataset retrieval fails
    * Fixed an issue for retrieval of transaction dataset trees where @maximumMultiplicity is ''
    * Implemented history read/write for scenarios/transactions. Note that currently all scenario/transaction history is written under UNK.xml and type UNK. This history is functionally lost
    * Fixed a bug when multiple defaultBaseIds exist
    * Resilience against malformed representingTemplate/concepts
* Templates
    * Performance
    * Avoid duplicate templateAssociation/concept listings, but if they do occur deduplicate before rendering them.
* ValueSets
    * Prevent empty @displayName upon expansion from codeSystem
* Miscellaneous
    * Performance (getNameForOid)
    * Fixed an issue that caused additional language items in concept definitions to disappear
    * Changed eXist-db 4 combined indexes to new-range indexes for stability reasons
    * Add option to upload/reindex OID Registries from the configuration form
    * Add support for newer OID Registries packages including imporved canonical URI retrieval
    * Reinstated inline dialog for status filtering in various forms because of an error that xi:include raises with Orbeon 2018
    * Admin: Added concept map artefact identifiers
    * Compilation: Correction on versionDate and compilationDate. Don't add subseconds/timezone
    * Compilation: Correction on @datasetId | @datasetEffectiveDate. Don't add when empty
    * Install: Explicitly set the Orbeon version setting. This facilitates configuring it in the art-settings later on. Otherwise the option will be empty

2019-03-12 version 2.0.5
* DECOR Explore
    * Fixed display order of results (this was random)
    * Added option to select which concept type you want to find (group/item)
* Datasets
    * Don't allow more than one element type per language on dataset (concepts) except for synonym and comment
    * Support for @canonicalUri
    * Allow @fractionDigits on decimals in saving concepts
* Governance Groups
    * Fixed a problem in the address-line-editor dropdown for addressLine/@type
* Transactions
    * Display ? for empty minimum/maximum multiplicity (unfinished transactions) and display exclamation mark next to display thereof
    * In representingTemplate tree views, mark every ancestor node leading to the culprits too, so you can still find them in larger datasets
    * Allow saving empty minimum/maximum multiplicity from the representingTemplate editor
    * To avoid not being able to fix the schema errors that the previous change causes, status changes are not allowed on any scenario/transaction while it contains unfinished business
    * Support for @canonicalUri
* Template-mappings
    * Resilience against duplicate language names in concepts
* Value Set
    * Performance upon usage calculation
    * Performance on value set retrieval
    * Support for valueSet/@experimental
    * Support for @canonicalUri
    * Added ordinal to view for dataset conceptList/concepts or valueSet/concepts and exceptions (if present)
    * Fixed a problem creating a valueset list when the project is compiled
* Issues
    * Fix issue with reading issue subscriptions
    * Reversed linkedartefactmissing logic which caused incorrect missing notions
* ART Settings
    * Adds Orbeon version dropdown
* Configuration
    * Slight feedback improvement on XForms update feature
    * Refresh symbols now properly green for status and orange for update
    * Now displays the currently configured Orbeon version
    * Now offers only 1 button for updating, which updates forms according to the currently configured version
* Compilation
    * Added linkedartefactmissing to the list of compilation attributes on template/relationship
    * Fixed writing linkedartefactmissing on relationship[@model](conceptId]). Only makes sense on relationship[    * Prevent error when dataset does not exist, or exists twice or more
* Other
    * Support for Orbeon 4.7 is now 2017. Support for other versions now goes to 2018
    * Restored status node icons under Orbeon 2017 and 2018
    * Don't apply any new Orbeon Server version upon install, just update to the current version or return status. Improved feedback status
    * ValueSet search will now find valuesets with matching @codeSystem based on (part of) its id
    * All searches will now find objects based on more than just the leaf node of the id: you may now use any trailing part of the id like 7.6.5
    * Now allows searching for concepts in releases.
    * Search performance improvements

2019-01-22 version 2.0.4
* CodeSystem
    * Updated getRawCodeSystem to a public function
    * Added publishingAuthority and purpose
* Template editor
    * Fixed a problem after saving a new template where the browser would detect unsaved changes upon reloading the template in edit mode
* ValueSet editor
    * Now uses any existing terminologyAssociations for creation of a valueSet proposal based on a conceptListId
* Compilation
    * Added support for ref box generation on template/relationship by adding the right set of properties
    * Add exrtra properties on datasetTree: transactionExpirationDate, transactionStatusCode, transactionVersionLabel
    * Defensive measure for duplicate template definitions
    * Fixed issue in outputting an error

2019-01-10 version 2.0.3
* CodeSystems
    * No longer offers "missing codeSystems". All this meant was that there was no codeSystem/@ref, but these do not really serve a purpose currently anyway. If and only if a project reuses DECOR codeSystems of another project, then a @ref would be useful. It is in no way a requirement
* Datasets
    * Restore focus on name field after searching
    * Searching will now start at three characters or more. (was 1)
    * Improved search by id, will now just get the matching concept without looking for inherits/contains from that
* Project
    * Fixed an issue that prevented adding a new language to a project
* Scenarios
    * Reversed display of role/name for actors on transactions
    * Mark template and dataset as references if they are
    * Updated dataset link to point to the right project if it is a reference.
* Development/Publications
    * Updated logic for compiling valueSet when there is no filter at all so it includes all valueSets in the project. Before it would skip all valueSets without a valid association somewhere. This was only true for check-decor and did not occur in releases or RetrieveProject
    * Fixed a problem compiling projects with concept/contains relations
    * Fixed an issue with merging xpaths into a fullDatasetTree
    * Fixed an issue with a generated transaction concept condition description when the incoming language is *
    * When you have a transaction based on a dataset from a different project: compile those in
    * Moved function for writing a compiled file into decor/tmp from RetrieveProject to compilation library/api
    * Development check will now use the same logic as RetrieveProject and not recompile when the project has not been touched since the previous compile 
    * Development check will now also render checkError and checkWarning. These were already returned but did not come to view. When the project is already being compiled when you call check-decor, you will get a warning for that and a request to try again later.
    * Development runtime compile will now use the same logic as RetrieveProject and not recompile when the project has not been touched since the previous compile, e.g. if you just did check-decor
    * decor-development.xhtml will now also render errors from along the way
    * Compilation process will now populate metadata for template/relationship facilitating more meaningful renderings in publications
* User management
    * Implemented search by lastlogin
    * Fixed technical error when searching based on multiple group memberships
* General
    * Explicitly close the login dialog after reload of the window. Necessary for FireFox
    * Disable empty install note
    * Various performance improvements
    * Fixed a problem in linking a concept to the concept it inherits from when that concept in turn inherits from/contains a concept that is in a different project.

2018-11-21 version 2.0.2
* Templates/StructureDefinitions
    * Deactivated StructureDefinition tab on templates form as it did not add too much value yet. It will return as soon as associations are implemented
    * Prevent saving empty template/context, but if you do, add schematron that tells you where it is rather than leave you guessing
    * Preload full templates list upon adding a new reference
* Project
    * Extended adding BBR functionality in project form. Now allows adding abitrary BBRs (like FHIR)
    * Improved pruning of BBR lines in saving a project

2018-11-14 version 2.0.1
* Issues
    * Fixed issue with object type transaction
* Templates
    * Fixed issue in loading templates form when no StructureDefinition definitions existed
* Terminology
    * Fixed a bug caused inability to add a new terminology association for a value set
* General
    * Technical fixes that remove the dependency on the FHIR servers being installed
    * Fixed context for project search 

2018-11-05 version 2.0.0
* FHIR support
    * FHIR DSTU2 and STU3 support for export of DECOR ValueSets and DECOR datasets and transactions as logical models. See [[FHIR_URIs](@template])]
    * Add any FHIR repository to your project, e.g. &lt;buildingBlockRepository url="https://stu3.simplifier.net/" ident="NictizSTU3-Zib2017" format="fhir"/&gt;
    * Declare that your project supports FHIR using a specific REST URI, e.g. &lt;restURI for="FHIR" format="3.0">http://art-decor.org/fhir/&lt;/restURI>
    * Connect FHIR StructureDefinitions to your project.
    * Map your dataset or transaction to FHIR StructureDefinitions (coming soon)
    * Declare project local canonicalUris for kids, like code systems (not preferred, preferred to in a server OID Registry
* Transactions
    * Concepts now support a local context that allow you to get more specific on the concept in the context of this transaction
    * Concepts now support terminology and identifier association overrides. This allows you to bind a concept to ICD for a quality registry in one context and SNOMED CT in a clinical context.
    * Fixed issue when cancelling edit in scenarios/transactions where locks would not be cleared
* Terminology
    * Added read/write support for transaction level terminology bindings
* CodeSystems
    * Viewer has been rewritten to be comparable to the ValueSets. Editing connects to Temple XML based editing
* Updated login procedure. Now does not leave the page anymore, and refreshes the page after login. This preserves the parameters in the URL
* Templates
    * Fixed display of labels on strength
    * Added preview of description text on accordeon header so you don't have to click them open to see.
* Datasets
    * Set focus on name field after edit saves the user a click every time
* Performance updates
* Tested to work with Orbeon 3.9, 2017 and 2018 (check new option in Configuration form), and eXist-db 2.2 and 4.4.0

2018-09-26 version 1.8.63
* Updated log format for performance. This requires a one-time-only log upgrade available from the configuration form post installation
* Updated save routines to write to the logging upon save and upon status update
* Small performance enhancements

2018-08-23 version 1.8.62

* Datasets
    * Fix for deInherit of a concept that inherits from a concepts that contains
    * Be more relaxed on multi language names/descs of the same language in datasets
* Scenarios
    * Button rearrange in the transaction editor
* Template-mapping
    * On template mappings show dataset concept status as well
* Terminology
    * Fix in valueSet search dialog
* General
    * Adds decor/tmp collection for RetrieveProject
    * Localization Dutch: Geldigheid was misunderstood. Changed into Ingangsdatum
    * Preparations for terminologyAssociation/@equivalence support, will not loose attribute if present
    * Support empty password on login (default for admin on local installs)
    * Be more resilient against group mismatches in saving user details
    * Performance updates

2018-07-04 version 1.8.61
* Datasets
    * New syntax added for "max X digits" in property/@fractionDigits using a trailing dot, so "3." means "max 3 digits"
    * Fix: when moving concepts in the dataset without editing them, all properties other than name and desc would go lost
    * Added links to documentation for valueDomain/@type
* Development
    * Fixed problem in selecting option for explicit includes
* Project
    * Improved addrLine editing. Fixed not being able to add first addrLine
* Scenarios
    * Fixes behavior when you update a representingTemplate link and have other edits. After updating the representingTemplate the Save/Cancel buttons would disappear
    * Set data-safe to true after successful save of a template connection
* Templates
    * Improved header rendering
* Template-editor
    * Add project namespaces to template for namespaced examples
    * Only show id-management dialog button when edit mode is 'edit' (i.e. not new or adapt)
    * Delete old templateAssociation elements before inserting new ones when the id is updated
* Template-ids
    * Add canonicalUri to view if present
    * Skip template/@ref if there are other templates found
    * Fix template/@ident rendering on initial template
    * Fixed problem in getting to the right template version
* ValueSet
    * Expand project codeSystem upon expansion
    * Fixed issue with usage retrieval for non-current valueSet versions on dynamic bindings
* ValueSet-editor
    * Added rudimentary exclusion editing support comparable to inclusion editing
    * Fixed a problem in inserting a second and further value set include. This broke after introduction of readonly intensions
    * Added rudimentary support for valueset intension editing. Allows add/delete/edit of intentions, but as of yet without codeSystem dialog support
* ValueSet-ids
    * Skip valueSet/@ref if there are other valueSet found
    * Fix valueSet/@ident rendering on initial valueSet
* Miscellaneous
    * Fixed issue that prevented language selection
    * Updates for new location of FHIR servers 1.0 (was: dstu2) and 3.0 (was: stu3)
    * Include @canonicalUri on terminology/codeSystem in results
    * Don't create empty desc element on conditions without text()
    * Fixed compilation bug in enhancedValueSets for language *. It would skip valueSet concept desc elements
    * Fixed order problem in ids when there are identifierAssociations defined
    * Compile 'foreign' templateAssociation/concept with url and ident to have provenance (especially when nifty when the original project contains errors and you had no idea where the concept came from)
    * Updated color for refvalue to something with more contrast, hence more readable (blueviolet)
    * Add extra properties on concept/inherit, concept/contains, template/staticAssociations/origConcept: dataset[| @versionLabel | @expirationDate](@statusCode) and specifically on template associations: concept[| @versionLabel | @expirationDate](@statusCode)
    * Fixed scoping issue in compiled projects that caused multiple copies of value sets to be returned

2018-03-30 version 1.8.60
* Project
    * Refresh list of artefacts when selection of governance group changes. Internal cleanup: remove leftover css
* Datasets
    * Fixed marker in dataset tree whether or a concept has a mapping. It responded only to @id instead of @id + @effectiveDate
* Templates
    * Fixed error in merging <text/> nodes
* Other
    * Menu: Removed DHD (Thesauri) entry from terminology-menu-template.
    * Menu: Added refsets_link menu option (NL) to terminology-menu-template.

2018-03-27 version 1.8.59
* Project
    * Run basic check-decor.xquery on compiled project rather than uncompiled project so we know if everything is resolvable
* Terminology
    * New: support for @strength on terminology bindings
* Datasets
    * Apply better dataset list sorting based on effectiveDate/versionLabel
    * Fixed problem in selecting/loading a different dataset version
    * Fixed problem in using the self link on a dataset
    * Improved feedback in case of inconsistency
    * Now saves identifierAssociations from repo too if applicable, just like terminologyAssociations
    * Fixed a problem where adding a comment on an inherited concept of type group would loose all children of that concept.
    * Fixed residual datasets/@maxcounter
    * Concept search corrections
* Transactions
    * New: transaction representingTemplate cloning into another transaction. In full, or just a concept (and children)
* Templates
    * Show all transactions pointing to this template, even if they are from other projects
* Template-editor
    * Better prototype merge for <choice>
    * No longer deletes choices/include in editor, but deactivates selection
    * Fixed issue for template adaptations that would cause the original template associations to be deleted
* Codesystems
    * Fixed namespace bug
* Valueset-editor
    * Added checks to avoid adding NullFlavor concepts instead of exceptions
    * Prevent saving generated valueSet/publishingAuthority
* Publications will now contain all relevant datasets regardless of original project. This has become relevant since the introduction of contained concepts. As of this version this also means the inclusion of all templateAssociations from anywhere, so contained concepts are also automagically associated with the templates in the project
* Publications and other compilations like for RetrieveTransaction will rewrite ValueSets that call NullFlavors as concept, to exception. NullFlavors are never concepts. E.g. <br/>&lt;concept code="OTH" codeSystem="2.16.840.1.113883.5.1008" ...&gt; will be rewritten as <br/>&lt;exception code="OTH" codeSystem="2.16.840.1.113883.5.1008" ...&gt; and <br/>&lt;include ref="2.16.840.1.113883.1.11.10609" ...&gt; will be rewritten as <br/>&lt;include ref="2.16.840.1.113883.1.11.10609" exception="true"...&gt;<br/>This also helps the schematron engine to assess valid values for code/codeSystem versus nullFlavor attributes.
* General
    * Various performance updates
    * Rewrote indexing for new-range when installed on eXist-db 4 or up
    * Added changelog to installer and dependency on eXist-db 2.2.0

2018-01-24 version 1.8.58
* Datasets
    * Fixed a problem where adding a comment on an inherited concept of type group would loose all children of that concept.
* CodeSystems
    * Performance fix for usage retrieval
* Project
    * Improved support for editing ids. Now includes id property support
    * Don't save empty project/copyright/addrLine
* ValueSets
    * Support read/write for valueSet/@canonicalUri. This attribute allows you to override the server dependent generated URI upon export als FHIR ValueSet
    * UI update for the value set editor
* Templates
    * Improved value set binding @strength support. From CNE/CWE to required/extensible/preferred/example
    * Allow for submission of issue for referenced templates in a project and then issue a note to consider promotion to originating BBR
* General
    * Compatibility update for the latest FHIR server packages (currently trunk only)
    * New index definitions are now only written when no index exists yet
    * Various performance updates

2018-01-12 version 1.8.57
* Project
    * Now creates full project backup in history when create a version (as opposed to a release)
    * No longer saves empty addrLine/@type. Slight visual enhancements for copyright/addrLine
* Datasets/Concepts
    * When the display name for a concept in a conceptlist does not exist win the requested, fall back to first available
    * Fixed a problem in linking from an inherit or relationship where the parameter language= occurred twice in the link
* Templates
    * Aligned edit in temple button behavior with template-editor
* ValueSets
    * Fixed a problem in getting valueset usage
    * Don't destroy (and not save) intensional value set definitions (even though they are not editable yet)
    * Include valueSet/publishingAuthority upon all valueSet[retrieval based on /decor/project/copyright[1](@id])

2017-11-22 version 1.8.56
* Datasets
    * Fixed a problem where setting concept status was done based on dataset status
    * Fixed a problem where the cancel button on a new dataset or concept issue did not work
    * Fixed a problem where moving an inherited group would yield an empty group after save
    * Fixed a problem that would cause duplicate ids in creating inherited groups
    * Fixed an issue when a templateAssociation was found in a different project, in getting the right project context
* Project development
    * Fixed a problem that caused inability to see previous results when not author but admin
    * Fixed setting for generating schematron with or without explicit includes
* ValueSet
    * Automatic copyright for SNOMED (part I for rendition get through api)
* General
    * Added a reverse lookup function that will take you from a FHIR URI to an OID if possible

2017-11-19 version 1.8.55
* Datasets
    * Updated datasets for large sets of datasets. Now allows for issues at dataset level. Dataset version now comes into view consistent with all other places where this is relevant. Because issues can now be recorded in two places, this has moved into a dialog (technical reason: the contents of the reusable fragment contain an @id, so the resuable fragment can only be called once per form). 
    * The multilingual dialog now displays the lastTranslated date, and marks when there are contents on the title of the folding accordeon.
    * Fixed a problem that caused inability to add new inherit/reference concept as after adding a new concept as containment
    * Fixed a problem in inserting new concepts into groups that caused duplication
* Template-viewer
    * Fixed iconizing referenced templates in the list
    * Fixed name display for class in the template list
    * Prevent spinner from spinning when selected tree item is a class/category instead of a template
* Template-editor
    * Fully renewed template example generation. Now allows for fully recursive and partial (from element[onwards) example generation regardless of whether the context is an in memory template (Temple / Template-editor) or any template based on @prefix/@id/@effectiveDate. At present Temple will only support examples for the current template.
* Project
    * Fixed a problem where additional properties on ids could get lost upon save
    * Removed initialization of dataset list as this was moved to the development form
* Project-develop
    * Now supports configuration for what to do in case of circular definitions. Default is 'continue' optionally does 'die' in which case the transformation is aborted 
* Scenario-editor
    * Fixed a problem adding a template to a transaction when the status of the transaction does not allow editing
* ValueSet-editor
    * Fixed a problem in saving intensional valueSets with include[@op](@id]) or exclude.
* General
    * Performance tweaks
    * Fixed a problem in serialization of single text() nodes. These got duplicate escaping e.g. &amp;amp;
    * Add attributes in compilation from original concept to inherit and contains so those can be used for linking
    * Improved project search algorithm for the project menu/search

2017-10-19 version 1.8.54
* Dataset editor
    * Fixed a bug in adding new concept under a group that would cause duplicate concepts
    * Deactivated inline dataset history creation and replaced that with the history API comparable to templates and valueSets
    * Fixed a problem that caused statusCodes not to be updated correctly upon dataset saves
    * Fixed a bug where new dataset group information would not be saved when the new group also contained new items/groups
* Terminology mapping
    * Better visuals for expirationDate. Now behaves like concept type D(deleted)
* Template editor
    * Fixed getting concept names on associations
* Issues
    * Fixed (potential) situations where multiple authors would be found where we only want 1. Pick first
* General
    * Added versionLabel to version drop downs for better visibility. https://sourceforge.net/p/artdecor/tickets/308/
    * Added sorting to concept associations so expired stuff comes last and codeSystems are all in the same order
    * Refresh dataset-list upon language switch in all relevant forms as it gets resorted according to the selected language
    * Menu project search will now search governance groups too. Clicking a governance group takes you to the governance group list of projects
    * Performance updates

2017-10-14 version 1.8.53
* General
    * Fixed a problem where the project searches woud not be shown because the language you are currently in is not available
* Dataset editor
    * Fixed https://sourceforge.net/p/artdecor/tickets/309/ where new dataset group information would not be saved when the new group also contained new items/groups
    * Better hint for max results input
    * Fixed a problem where the group/item switch would not work properly upon adding multiple groups as siblings
    * Deactivated inline dataset history creation as this is now handled externally through the history api
    * Fixed a problem that caused statusCodes not to be updated correctly upon dataset saves
    * Performance update in saving
* Issues
    * Fixed (potential) situations where multiple authors would be found. The first authoring user is picked
* Template editor
    * Fixed a problem in addTemplateElementAndAttributeIds
* Template viewer
    * Fixed a situation where multiple versions of the same template with different classifications would cause inability to load the templates form

2017-10-08 version 1.8.52
* General
    * Updated all links to self with language parameter
    * Searching now always is language aware
    * Forms with project content now support parameter language= to immediately go to the requested language
    * Fixed a bug in isRepository and isPrivate
    * Added isExperimental and setIsExperimental
    * Improved dataset list. Now keeps versions of datasets together and sort alphabetically. Dataset list is refreshed upon switching language so the sorting matches language.
* Scenarios
    * Fixed a bug in terminology and identifier associations retrieval
    * Support for parameter scenariotree=true|false. If false the scenario tree will be collapsed upon form initialization, else it is visible
* Terminology
    * Updates for concept[* Template editor
    * Fixed bug in saving art:placeholder examples https://sourceforge.net/p/artdecor/tickets/306/
* Performance updates

2017-10-01 version 1.8.51
* Issues
    * Allow adjustment of the max number of returned issue up to 500
* General
    * Performance

2017-09-25 version 1.8.50
* ART Settings
    * Allow configuring the default FHIR server through ART settings instead of hard coded
* Template viewer
    * Fixed usage calculation for templates in transactions when template is a ref

2017-09-18 version 1.8.49
* Project-development
    * Allow dba access too so he doesn't have to be an author everywhere
    * Now compiles issues too making sure that trackings/assignments are in the right order, adding performance to further processing
    * Now supports adding only templates specified in the template stack from the toplevel template stack as explicit includes rather than adding every single in scope template for the project.
* Project
    * Now checks the last time a project was checked upon creation of a new publication
    * Prevents saving duplicate buildingBlockRepositories

2017-09-15 version 1.8.48
* Issues
    * Fixed a problem where a users name was not retrieved correctly on new issues
* Project
    * Fixed https://sourceforge.net/p/artdecor/tickets/228/. DECOR Project now has extra tab 'ADA' if ADA is installed on the server and will list any apps related to the DECOR project with a link to the index
    * Fixed multi language name editing issue causing inability to save changes
* Project-development
    * Better calculation of the net result in schematron
* Template viewer
    * Updated height for templates and navigation to use maximum window height (thanks Jens Villadsen!)
* Template-editor
    * Fixed https://sourceforge.net/p/artdecor/tickets/299/ implementing improved element and attribute name checking
    * Fixed saving templateAssociations that were save with the wrong effectiveDate
    * Fixed template merge routine to support multiple seemingly similar example elements without type or caption
    * Fixed template merge routine so remaining prototype elements when the last current template element doesn't match are marked in too
* Project compilation (e.g. for publication)
    * Any compile will now also save an "all" version next to the language. This allows for multilingual retrieval later on, e.g. RetrieveTransaction?language=*. Before this would only work on the live version of a project and yield empty on any publication.
    * Fixed retrieval of project name/desc when language is '*' (all)
    * Enhanced art:getEnhancedValueSet for language. Now also takes designations into account.
    * Added some readability to camelcased names when those are converted into a shortName, they get an underscore just before the capital, leading to short_name.

2017-08-21 version 1.8.47
* Dataset concepts
    * Added better error feedback for duplicate concepts
    * Fixed links from concept relationships in read only views
    * Fixed a problem in the short name when multiple by the same name exist in the same context. Added postfix to every second occurence and thereafter with an underscore and the @id leaf node e.g. _2, _3 and so forth
    * Now allows for moving concept (groups) into any regular or inherited group at any level. For inherited groups you could only move into them at the first level.
* Project
    * When compiling dev releases: have schematron closed by default
    * Made rendering @elementId | @missingElementId conditional so we can leave it off in other error types.
    * Include compilation results from valueSets/templates in compilation of ids so we don't miss those
    * Now tells you whether a given development build was built with closed schematrons
    * Improvement of check decor logic and development tab layout
* Community
    * Don't show plus button for new associations unless there is none yet
    * Fix: now retrieves data for external prototypes too (think ADA)
* Templates
    * Fixed duplicate hits when multiple repositories point to other repositories containing a target template
    * German language tweak: Abbrechen instead of Annulieren - Marriages are annulled, Dialogs are cancelled
    * Fixes a problem in adding namespaces to 'foreign'  templates e.g. in compilation
    * Made deleting templateAssociations depedent on whether or not we found a template
    * Added permissions checking
    * Rename of get-template-usage.xq to get-decor-template-usage.xquery so it matches the other modules
    * Rename of parameter prefix to project to match the other modules
    * When you delete a template ref it now also deletes any attached and empty templateAssociations elements with that
    * Fixed the check for templateAssociations in template viewer. Now no longer let's you delete a template/@ref when templateAssociations exist
    * Added new functions for getting templateAssociations in art-decor.xqm so we don't repeat logic
    * Enhanced checking for permissions in deleting template/@ref
    * Improved behavior in template selection when transaction is in scope and target template has multiple versions
    * Improved behavior in template refresh when selected template has multiple versions and any older version is selected
    * Fixed a number of datatype issues. Now lists ANY (v3) or varies (v2.5) for new elements. In case of an illegal datatype on an element or attribute it now renders it correctly.
    * Fixed a problem that caused an inability to render include/constraint in ART-DECOR and DECOR-services. It did not affect publications
    * Added more properties of referred valueSets and templates so we can act on those downstream
    * Template API now generates the namespace nodes from the original context of the template, not the calling context (template/@ref) of the template
    * Schematron and fragment generator now generate top level schematrons that include the namespace declarations from the template hanging off the representingTemplate instead of just it's own. That may still not be good enough. Might need to globally harvest of every template because it might in turn point to something that defines yet another namespace...
    * Editor: Fixed a problem in with asserts and reports causing inability to access the @see reference
    * Editor: Now displays concept associations in the editor and deletes any that you delete the element or attribute for upon save. No more dangling associations. While the editor is open, you may reactivate an inadvertently deleted element of attribute at any time.
* Terminology
    * Fixed a problem in not setting the flexibility attribute for terminology associations causing associations to be made on "latest concept version"
    * Updated text on terminology-home page.
    * Fixed a bug in handling language on LOINC panels in the result set table causing inability to display localizations
    * Fixed a bug causing inability to remove a conceptList/concept terminology association
* OIDs
    * Fixed bug in searchOID function causing incorrect sorting order
* DECOR Admin
    * Fixed a problem that prevented adding more than 1 author.
    * Fixed a problem that prevented the Copyright type dropdown to work
* Installation
    * Fix development collection permissions for releases
    * Suppress error when the initial governance group link is not done by a dba

2017-06-13 version 1.8.46
*Project
    *Fixed problem in adding new users to projects (list was empty)
    *Fixed problem in searching users (Lucene index was missing)
*ART-DECOR User Management
    *Fixed lingering problem in handling groups with spaces in the name
    *Circumvented problem bug in framework for xxforms:tree in art-users.xhtml by switching appearance compact. Visually unattractive, but works
    *Fixed problem in filtering users based on group where you could add the same group multiple times
    *Fixed problem in finding the user groups after you searched for a particular name
*Template mapping
    *Now shows template/@displayName for mappings instead of @name
    *Now does not list template versions that are out of scope for the transaction

2017-05-19 version 1.8.45
* DECOR checks in DECOR development are cached
* Optional extra DECOR checks can be configured per project.

2017-05-16 version 1.8.44
* Templates viewer
    * Now allows for filtering by status comparable to concepts and valueSets. cancelled/retired/rejected are hidden by default
* Template-editor
    * Now contains a lot more aid in handling template/context * and <nowiki>    *</nowiki> using drop downs, hint/help and templateId creation if possible
* Terminology
    * Now has write/delete support for identifier associations allowing you to express that a certain concept is connected to one or more identification schemes like national patient ids, drivers licenses, social security numbers etc. The scheme must be available in one the OID Registries or in the project/BBR itself. For best search results, please also install the OID Registry package update.
    * Improved the valueSet and conceptList/concept association dialog by adding the concept name you are connecting to in the view. This improves a sense of context in this dialog
    * Fixed a problem causing inability to create/delete certain terminology associations caused by changes in version 1.8.43
    * Fixed a problem causing terminologyAssociations to be written without concept version when that would be applicable
* Value sets viewer
    * Now allows to go Temple for direct valueSet XML access with content completion. Useful for smaller tweaks, or adding large valueSets created offline

2017-05-12 version 1.8.43
* Project 
    * New: project administrators may now update @repository, @experimental, @private, @defaultLanguage in the project form. Each triggers a dialog that explains what you are about to do. @defaultLanguage maybe switched to any other project/name/@language if available. @repository once true cannot be set false. @repository cannot be set true while @experimental or @private is true.
* Datasets/Transactions
    * New: direct link to the DECOR services RetrieveDataSet and RetrieveTransaction from a given dataset and transaction, in list mode
    * New: read support for identifierAssociations so you can associate a concept of type identifier with one ore more identification schemes
* MyCommunity
    * When editing community info all data will be shown by default.
    * Empty data is discarded on save.
    * Edit form has column layout.
* Templates 
    * Improvement tweak for CONF display
    * Now displays template-id as-is not as readable version
    * Moved inclusion ref to the right where it fits, and gives a much more peaceful tree on the left without loosing focus/overview
    * Improvements in merging prototypes into templates
    ** Now supports merging immediate children of template
    ** Now supports merging defineVariable
    ** Fixes incorrect additional merge-ins from prototype
    * Fixed display of textarea.full-width
    * Fixed indenting for items under choices
* Template mapping
    * Fixed preselect in the mapping list so it selects the relevant mappings, not just the first
* Terminology mapping
    * Fixed a problem where adding new conceptList/cooncept to code associations was impossible,
* Value sets
    * New: history feature for value sets. Each save saves the previous version.
    * New: ID Management for value sets (same as for templates)
    * Fix: Prevent error when you search for something that exists and then for something that doesn't
* Admin only updates
    * New: can now search users by active status in the server settings
    * New: can now set FHIR server endpoint in the server settings. Note that the FHIR server itself is still in development
* Other
    * Performance: moved some logic from client side to server side. This should speed up working with datasets
    * Performance: rewrote some language related features
    * Fix: history collection is now created is missing when you fix permissions

2017-04-27 version 1.8.42
* Small fix with big impact for edit bug introduced after last update

2017-04-25 version 1.8.41
* Publications
    * Flipped the defaults for a number of publication parameters to match the defaults in DECOR2schematron and create-decor-version.xquery:
    ** switchCreateDocHTML0 to switchCreateDocHTML1
    ** switchCreateDocSVG0 to switchCreateDocSVG1
    ** useLocalAssets0 to useLocalAssets1
    ** useLocalLogos0 to useLocalLogos1
    ** useLatestDecorVersion0 to useLatestDecorVersion1
    ** switchCreateTreeTableHtml0 to switchCreateTreeTableHtml1
    ** Extra: defaultLanguage gets overridden with project/@defaultLanguage when value is not supported in project
    ** Extra: artdecordeeplinkprefix gets overridden with adserver:getserverURLArt() if not xs:anyURI
    * Fixed publication filter on/off behavior. Now resets to off when no more selections exist too and after clicking Reset.
    * Added Dutch localization to documentation of paramaters
    *Template viewer
    *Now displays in scope BBR templates even if no reference exists
    *Now allows cloning a template regardless of status
    *Now allows to go to temple from the edit-dialog
    *Now allows subscribe/unsubscribe from template issues
    *Now displays history if available
* Updated icon for stationary transactions
* Fixed a problem when you search for something that exists and then for something that doesn't
* Fixed a problem viewing valueSets when no value set is selected. The triggered a problem related to history
* Fixed a problem viewing valueSets and template from BBRs that have no reference yet but are in scope
* Fixed some permission problems in history
* Updated SNOMED CT license notice
* All editors: now returns any locks in case of NO PERMISSION
* Template-editor
    * Fixed a problem where it was not possible to select a repo valueSet to add to your project from the template-editor
    * Now retains strength from the prototype
    * No longer displays @strength when not(@datatype)
    * Saving now explicitly states every supported attribute on elements instead of saving everything except non-supported attributes. This allows more targeted saving like not saving @flexibility when not @contains|@ref|@valueSet, or not @valueSet and @code
    * Added a warning to the dialog that instructs you to update the templateId element after updating the template/@id when context/@id
    * Added a binding that disallows save until you have fixed a mismatching templateId element when context/@id
    * Various technical fixes

2017-04-06 version 1.8.40
* Installation: simplified language merging, performance gain
* Fix permissions upon initial save of governance group links
* Scenarios
    * Fixes a display bug in scenarios when there are no transaction connected to a dataset
    * Fixes a serious problem in id management causing duplicate entries when adding scenarios/transaction in when not all scenarios are in view. Now asks server for latest id and compares that to in client ids. Highest wins

2017-03-23 version 1.8.39
* Fixed permissions for mostly decor/releases which caused an inability to create new releases
* Better error handling and use of art-decor.xqm functions in compilations
* Terminology: fixed a problem where adding new conceptList/concept to code associations was impossible
* Scenarios/transactions: check if trigger contains a description at minimum. (will not be saved otherwise)

2017-03-14 version 1.8.38
* Datasets: fixed a problem in the inherit dialog where the selection in the dialog was not correctly applied.
* Datasets: fixes a deinherit problem when the object of inheritance is non local and has a coded/ordinal valueDomain
* Datasets: fixes a problem where dataset name and desc would not be editable if you switch to a supported language that was not populated before
* Project: added edit and save publication parameters for scenario based filters
* Templates: fixes a problem where if you did not populate assert|report @see or @flag before you would not be able to populate them later
* Templates: very first version of the object dependency query for the upcoming propagate to BBR feature
* Templates: fix for "exerr:ERROR Internal evaluation error: context is missing for node 3.9.23 ! [at line 1151, column 46"
* Templates: also update the templateAssociation element if any when updating the template/@id
* ValueSets: ID management for value sets (same as for templates)
* ValueSets: history also for value sets
* ValueSets: Improved dialog for selecting a valueSet. Order is now alphabetic
* Added iddisplay for transaction/dataset to getFullDatasetTree
* Updates for changes in the SNOMED-CT format
* Better permission handling in compilations
* Fixed a problem where order of dataset/@id | dataset/@effectiveDate suddenly became relevant.
* Fixed a compile problem that caused skipping of name elements with nested text() nodes (since name elements are flat this should never have occurred)

2017-02-02 version 1.8.37
* Template-editor
    * fixed a problem that could prevent saving template updates from the visual editor
* My Community
    * Fixed form initialization for datasets and transactions
    * Performance improvements
    * Small visual enhancements
* Template-mapping
    * Fixed form initialization for datasets
    * upon display of a dataset//conceptList/concept/@exception, show a readonly checkbox, not an input field
* ValueSets
    * fixed form initialization for non-existent valuesets

2017-01-30 version 1.8.36
* Templates: switched default for status recursion to "false". This SHALL be an explicitly chosen action
* Templates: in services that return template XML, now the namespace declarations are turned too
* Templates: when the original template displayName changes, references now follow this updated name
* Community: implemented support for datatype enum
* Community: fixed sort order when adding notes of different type to a concept

2017-01-20 version 1.8.35
* Scenarios: enhancement so you no longer have to go back to -all- in the list of datasets before being able to select a different focal dataset
* Fix for problem in saving new templates
* Fix for rare cases where history could be written for templates, while template wasn't actually saved

2017-01-18 version 1.8.34
* Datasets
    * Fixed behavior upon cancel after adding + inherit + deinherit in before save
* Scenarios
    * Added missing functionality to add localized properties through the globe button
* Templates
    * Fixed missing collection decor/history that caused inability to save template edits

2017-01-10 version 1.8.33
* Development
    * Moved development tab such as compilation from the Project page into a page of its own with its own menu entry under Project
    * Synchronized validation output layout with IHE Gazelle layout
    * Better layout for check decor
    * adding new layout of validation ouput to Life Runtime Compile LRC in XIS
    * adding at least @see on datatype core schematrons
* Publishing
    * Publishing now supports a dialog for setting the most important parameters that were defaulted before
    * Added support for partial publications through filters.xml for [[ADRAM](contains])]. Now you may publish just a particular set of transactions (with attached datasets/templates/value sets for example.
* Terminology
    * New: search in the value set dialog
    * Fixed: when adding new conceptList/concept terminology associations now the associated value sets are offered using the correct version(s)
    * Allow text even when condition/@conformance='NP'
* Transactions
    * When the transaction editor deletes a group it now marks underlying concepts too so after saving, the transaction viewer does not list group contents until you reload it. The bug was only a visual one.
    * Fixed summary in scenarios form that prevented display of errors when applicable
* Templates
    * Setting status now allows for recursion: you may recursively apply status and expiration on all templates hanging of the focal template. Because this is a very powerful feature you may first inspect the list of templates that would be affected by the update
    * Introducing api-decor-history, first functional: artifact history list for templates, triggered upon save-template
    * Fixed dissappearing gear icons when the contents of the table exceed the max table width
    * Now respects attributes order in template element and attribute except for @id which is written last.
* Value Sets
    * Replaced the XForms based rendering engine with the publishing rendering engine. ART and publications thus now support a unified view.
    * Great performance improvements in loading the tree of value sets.
* Visual improvements
    * Slight enhancement: the status buttons on dataset now explicitly mention "dataset".
    * Improved rendering of references to repository artifacts (value sets and templates)
    * Visual update in the titles of most DECOR pages
    * Moved project list under governance group to a separate tab
* Miscellaneous
    * Functionality improvement in project-ids editing. Now supports editing all languages at once and includes setting the type
    * Redesign of the decor-explore page
    * RetrieveConceptDiagram now supports filtering based on concept status. Datasets form new sends this info
    * Artifact searches now always returns shortest matches first
    * Added option to search for users where relevant, e.g. when adding a project or an author. Caveat: when you are dba the experience differs from a regular decor-admin. For the latter search works better. decor-admin searches (lucene) in user-info.xml based on username and displayname while a dba searches based on exists username only (glob style).
    * Implemented support for new buildingBlockRepository/@format (default: decor)
    * Enhancement for db permissions. Now sticky bit is set to the group on decor collections
    * Changed default value for tree views in publications to "true"  to use foldable tables where applicable
    * Fixed news ticker for html contents
    * Made a start with a bigger update to the project menu. Upon every retrieval of the project info, the project statistics for that user are updated with a last access time and a count. This way you can determine what the user likely wants to see, next time he logs on. Most frequently used, and most recently used
    * Various performance updates

2017-01-06 version 1.8.32
* Updates for LOINC 2.58

2016-12-02 version 1.8.31
* Fixes a bug that could cause duplicate template elementIds introduced in 1.8.30
* Fixes a problem where the Save (without close) button on the template editor would stay active after creating a version or adaptation

2016-11-30 version 1.8.30
* Important: closed security hole that could be used to exploit xis-accounts
* Important: fixed a problem when using the "New template" button that would cause template to be created under the default base id of the demo1- project. Affected projects on art-decor.org have already been patched
* Datasets
    * Fall back to first element with a value when it is not available for requested language on concept/name|desc and conceptList/concept/name
* Templates
    * Centralized and improved logic for adding ids on element and attributes
    * Fixed a problem that prohibited making a static valueSet binding in the editor
* Overall performance improvement

2016-11-22 version 1.8.29
* Realm-specific additions
    * Added support for Dutch labterminology
* Datasets
    * Fixed a problem where @exception markers were lost upon save
* Terminology
    * Added missing self-link like on all other forms
    * Added indenting to the valueSet/associations section
    * Added concept/@exception to the view

2016-11-10 version 1.8.28
* Datasets/Scenarios
    * Fixed behavior for objects in status pending. You may switch status, but not edit otherwise
    * Add shortcut to RetrieveTransaction service to allow view/download in table mode
* Issues
    * Authors can now edit the status/labels of trackings they authored (action is logged)
    * Adds a cancel button to editing a tracking/assignment description
    * Fixes the option to edit a tracking/assignment description when it was empty
* Projects
    * Added support for new property "type" on copyright holders. Default is 'author'
    * Added runtime compilation fixes so validation of examples is active. There are still issues, but all parts that work help.
* Templates
    * Add support for multi lingual editing instead of one language at a time
    * Can no longer edit templates with status pending. Temple was active
    * Added setting all statuses with respect for the template lifecycle
    * Do not offer a new reference in editing if one is already present
* Value sets
    * Fixed a bug where completeCodeSystems through included valueSets were not considered
    * Fix for language switching bug in value set concept designations
    * Fix for duplicate hits when projects point to each other
* General
    * Made concept/relationship a link in all places it appears just like in the dataset-viewer
    * Various performance improvements
    * Updated strings for German

2016-10-10 version 1.8.27
* Project
    * New: upon creation of a new project version|release you now get a summary of all changed issues since the previous version|release as part of your release info. You may review/edit that summary before saving.
    * Improved rendering of project compilation and validation results
    * Visual tweaks to BBR rendering
* Template mapping
    * Can no longer edit templates with status pending.
    * Added setting all statuses with respect for the template lifecycle
    * Added the visual template editor option. Only Temple was active
* Template editor
    * Fix: do not offer a new reference if one is already present
    * Avoid endless screen widths in template editor for templates with large comment lines in descriptions etc by at least restrict width to fix 2000px
    * Disable spellchecking of the browser (if supported) in the input field for element/attribute name
    * Improvements in retrieval of info under new attributes and elements
* Issues
    * Authors can now edit the description of tracking/assignments they authored (action is logged)
    * Decor-admin can now edit the description of tracking/assignments of anyone (action is logged)
    * Fixed rendering of id on referenced valueSets
* Value Sets
    * Add read/write support for concept and exception designations. This allows for multiple displayNames on codes based on language and/or type. Types: preferred, synonym, abbreviation. By default, but user can disable this, copies all known descriptions from SNOMED CT and LOINC into value set.
    * Fixed a bug where completeCodeSystems through included valueSets were not considered

2016-08-16 version 1.8.26
* Project: Get proper ADRAM status via httpclient again, show value set from repo if so. Rewritten logic for ADAWIB regarding templates and value sets to explicitly include them in wiki output even if they are not from this governance group
* Datasets:
    * Fixed initialization problem in selecting similar concepts to inherit from
    * Don't do an intermediate save upon getting concept for inherit. When something goes wrong there no cancel
* MyCommunity:
    * Added missing missing for concept/@exception causing this attribute to show up editable
    * Fixed label for prototypes code and enum
    * Fixed support for prototype code
* Datasets/Template/Value sets: Allow setting status to draft from pending
* Templates/Value sets: When searching on id without prefix, do not search just all non-private repositories, but instead search any project that defines it or references it.
* Codesystem editor: Fixed a bug that prevented deletion of a codeSystem/@ref
* Value set editor: 
    * Now searches in code of claml systems as well as descriptions
    * Fixed a problem in the creation of valueSet versions and adaptations
* Template editor: Fixed merge problems on templates based on prototype where multiple examples exists at the same level.
* Template mapping: 
    * Fixed a problem where an elementName would not available for non-local BBR templates
    * Dataset/transaction tree now only marks a concept as 'mapped' when either the full dataset in scope or when the concept is mapped in a template that is part of the selected transaction
* Template viewer: 
    * Fixed link to TemplateIndex from the template BBR dialog
    * No template usage determination any more in template xform (is done by RetrieveTemplate)
    * Don't assume that template designer is silent about flexibility dynamic when calculating used by/dependencies
* General: 
    * '''Updated hl7 index for performance''' Note that installation will take a long time if you have a large number of hl7 packages installed
    * DHD: removed old DHD menu entry

2016-07-13 version 1.8.25
* Datasets: fixed a problem where the similar items to inherit contains items ancestor-or-self compared to the context item
* Datasets/scenarios/terminology: improved usage views for concepts
* Concept usage no longer includes hits for transactions/templates connected to concepts that inherit from current concept
* Fixed a problem where transactions descriptions would not be editable
* Fixed problem where transaction/triggers were saved without @language attribute making them hidden after save for the viewer
* Template-editor: Now retrieves namespaced datatype if it is a supported datatype as such
* Template associations: improvement in original calculation
* Template associations: now resolves names of elements/attributes for referenced templates too
* Added missing style for transaction receive with status pending
* Scenario viewer: fix against special cases where multiple language attributes are found
* Allow removing of the last condition in the list so long as there remains one left.
* Fixed display of attribute/vocabulary/@code in template-editor

2016-07-02 version 1.8.24
* Improved getting description nodes for languages not found in the project. Now tries to find matching language based on language part of the code first before trying en-US
* Fixed problem where not all description nodes were serialized
* Fixed refresh problem and problem after setting status in template and template-mapping
* Fixed an issue in projects with multilingual dataset names
* Fixes an issue where you could select the top entry in the list of languages unless you reselected the already selected item or any other item in the list if available

2016-06-28 version 1.8.23
* Small tweak for LOINC 2.56 db format update, while supporting the older formats
* Fixed saving empty attributes in creation of new projects
* Fixed a problem that prevented adding new issues in template-mapping
* Now maximizes mapped element/attribute names to 40 characters after mapping too
* Fixed a problem that prevented the issue list from updating after adding a new issue in templates and template-mapping
* Template example generation
    * Fixed a problem when element with datatype PQ and attribute @value was specified
    * Made the xsi:type generation smarter by picking the parent element value when available
* Fixed mouse over for language flag in the menu
* Improved fix for viewing templates from projects that contain languages that are not part of the ART-DECOR core languages. Aligned publication engine and ART for i18n. Now both retrieve exact language if possible, main language (without region) if not, defaultLanguage if given and final fallback to en-US if all else fails.

2016-06-26 version 1.8.22
* Fixed problem in rare case in getting a dataset concept for edit where more than 1 comment for a given language exists
* Fix in getting template list: do not create @url|@ident. Just copy if present.
* Updated representingTemplate//condition check so any empty condition is ok regardless of @conformance='NP'
* Save-decor-template now supports lock retention
* Fixed problem in codesystem lists
* Updated view of template particle names to max 40 + … if applicable
* Added decor-templates2.xhtml and decor-template-mapping2.xhtml using the old template viewer for fallback

2016-06-17 version 1.8.21
* Fixed a bug that could occur when an HTTP redirect occurred on an external server call, e.g. In the retrieval of BBRs

2016-06-14 version 1.8.20
* Terminology
    * New: The DECOR valueSets now support a new attribute ''ordinal'' that allows you to specify ordinal values connected to codes. This supports assessment scales such as Glasgow Coma Scale and Apgar Score. Note that HL7 Datatypes Release 1 as found in CDA for example, does not support communication of this attribute. Its use is therefor mostly for documentation and/or import in systems.
    * Improved: The view of valueSets in all relevant places like Datasets, Terminology, Terminology-Mapping and Issues has been updated so levels are more clear
* Templates
    * Improved: Performance update in viewing templates anywhere in ART. To this end the rendering engine has been replaced with the engine that powers publications. Not only is this many factors faster than before, it is also guaranteed consistent
    * Improved: Templates now support folding of elements and includes. This significantly improves navigability
* Improved: Editor can now save-and-close/create-and-close as usual or just save/create without closing the window
    * New: Live Runtime Compilation. Project editors may, through the Project form, compile the project at any time during their development cycle to test their drive their definitions. See [for more information
* Template-mapping
    * New: You may now trim the template list to only those that belong to the selected transaction (if applicable)
    * New: Now supports parameter ''selector'' to initialize the form with a transaction as scope rather than a dataset
* Datasets
    * Improved: conceptList id algorithm is updated for versioned concepts
* Transactions
    * New: Concept conditions are now multilingual, text nodes are considered to be in the project default language for backward compatibility
* Compiled Datasets/Transactions
    * Improved: Added templateDisplayName and templateName to representingTemplate
    * Improved: Introduced implementation/@datatype
* General
    * Improved: updated the language/user parts of the menu slightly for better behavior on smaller screens
    * New: all relevant forms now support a ''self link''. This allows you to bookmark the exact context of where you are at a given point in time or to send a precise link to a third party. See [[Copy_artefact_deep_links]([Live_Runtime_Compile]])] for more information

2016-06-07 version 1.8.12
* Datasets: Fixed a problem in setting the lastTranslated value
* Fixed a problem in compiling projects with double hyphens or trailing hyphens in certains places
* Fixed a problem in compiling projects for projects that do not have the required <rules/> element

2016-04-21 version 1.8.11
* Improved performance in retrieving supported browser languages
* Concept search results are now sorted by name length, then requested prefix (if any) first, then datasetId. It is likely that you want your own projects results first. Original sorting was name length only
* Fixed a problem where the dataset-navigation could go empty after emptying the search input
* Added the option to set the project logo
* Added support for logos with spaces and other less usual characters
* Adds <switchCreateDocPDF0/> to the default set of parameters when compiling a version
* Fixed the project properties so you don't get problems in ids when you working in other tabs or the other way around
* New community ada will add user guest by default
* Fixed xpaths: <attribute name="root"/> generated xpath with <span>[in it. Now only does this when @value is present.
* getFullDataset: when multiple xpaths are found: add them all in a separate compartment
* getMinimumMultiplicity: when condition/@conformance='NP' exists, assume minimum 0 (applies to all places where cardinality is relevant)

2016-04-19 version 1.8.10
* Prevent access error for user guest to surface in the home form
* Fixed dataset form initialization problem when conceptEffectiveDate is empty but conceptId is not
* Made more visual that valueSet display is maximized to 20
* Logo tweak so the height is reserved before the logo is loaded asynchronously
* Check exist user display name when user-info.xml has nothing. This supports admin using eXist-db user management instead of ART-DECOR built in
* Fixed a bug in valueSet retrieval for display in datasets when multiple exist
* Fixed a bug where projects bound to a non-existent governance group would not surface
* Fixed a binding that caused inability to save a representingTemplate when @conformance='' and @minimuMultiplicity occurred somewhere
* Fixed a problem when editing a dataset concept with status pending
* Script changes, additions and adaptations in order to serve well for Instance Fragment Validation IFV and Live Runtime Compile LRC
* Performance gains in
    * Project form for non-admins
    * Forms that load project info
    * Forms that load the template tree
    * Forms that load the dataset tree
    * Forms that load the valueset tree

2016-03-31 version 1.8.9
* MyCommunity: moved save/cancel buttons into tabs to be easier to the eye
* MyCommunity: fixed behavior when prototypes come from reference
* Template list: fixed language problems on labels when the project language is not in the ART set
* Template API: Adds dataset name to the output so we have stuff to show in Template2html
* Scenarios/transactions: now allows keeps selected concept in viewer when opening the editor

2016-03-26 version 1.8.8
* Datasets: valueDomain/conceptList/concepts now support level and type
* Terminology/Value set editor: now supports creating a new value set from the conceptList as defined on the in scope concept. This sets up a new value set with as much data as possible (level/type/name), and terminology associations are created upon save
* Project: support for adding names in new languages.
* Project: enhanced support for creating an ADA community. If you choose the name ada, the new community is immediately set up with the right prototypes
* Community: added support for integer datatype (relevant for ADA)
* Scenarios/transactions: added support for adding/updating a top level template even after the transaction has been pronounced final
* Scenarios/transactions: now allows intermediate saving of representingTemplates
* Scenarios/transactions: now allows keeps selected concept in viewer when opening the editor
* Performance update and alignment in getting valuesets and templates
* Vast improvement in xpath generation accuracy

2016-03-23 version 1.8.7
* Performance update for xpath calculations
* Pre-populate new scenarios/transactions with minimal requirements

2016-03-17 version 1.8.6
* Improved dataset form initialization so when called for a concept the usage now loads as expected and the whole form loads faster
* Fixed locking of objects that have associated issues
* Fixed valueset list sorting. Groups by id not name. Now does proper sorting by displayName

2016-03-09 version 1.8.5
* Fixed showing a template if possible, even if there is no formal reference in the project (happens when you call a repo template that has associated templates of its own)
* Fixed a problem in adding a description on an item/desc element
* Fix for editing a community
* Refactored layout for uses/used by table of templates
* Fix for missing codeSystem in value sets when no name could be found
* Fixed a problem in updating scenario or transaction status
* Performance improvement when you update value set status
* Added template/value set id and sync'ed layout for viewer for both
* Fix for editing communities when multiple by the same name exist
* Fix for finding duplicate repository value sets when initial scope is the repository and the repository has buildingBlockRepositories configured.

2016-03-01 version 1.8.4
* Fixed a problem when a hand crafted template contains duplicate element/attribute ids. This would prevent any associations being returned
* Simplified code in template-mapping form for display of element/attribute names. It now just counts on generated data
* Fixed problem in finding templates/valuesets when they are connected through an intermediary repository. Example: valuesets or templates connected to a template that you refer to from your project.

2016-02-29 version 1.8.3
* Fixed a problem in saving a template as version or adaptation when the base template has template associations
* Fixed a problem with the "classified" template list when a template has multiple classifications. Templates now occur only under their first occurrence of classification

2016-02-23 version 1.8.2
* Performance: asynchronous loading of projects menu and terminology menu
* Performance: smarter loading of governance groups behind projects menu
* Performance: smarter loading of localization messages
* Fixed a problem in saving new governance groups that prevented adding new groups
* Fixed a problem in saving value sets

2016-02-23 version 1.8.1
* '''Important update'''. Fixed a problem where upon updating scenario or transaction properties, all transaction concepts would be lost for the whole scenario. This problem was introduced in 1.8.0. We have checked all projects on art-decor.org and decor.nictiz.nl, and have found 1 affected project. This project has been restored.

2016-02-12 version 1.8.0
* Upon inheriting in a dataset from a repository, now saves terminology bindings and value set references in the project too as needed.
* Brand new transaction/representingTemplate editor with all the features of the previous one, but now integrated into the scenarios form and with significant performance boost. Now also sports shortcut buttons on concept items for 1..1 R 1..1 M etc.
* Improved multi lingual name/description translations for project and concepts
* Project logo's are now supported top right in DECOR centric forms. Need to have the logo in decor/project/reference/@logo, and physically in the prefix-logos/ collection next to the decor file
* Can now update the id of a template. Relevant when template was created outside of ART-DECOR and needs a specific id
* New multi lingual LOINC integrated into terminology/value set editor
* Reimplemented lock management on artifacts with better feedback when someone else has it. Scenarios/transactions/representingTemplates now have lock management too. Locks are now in /db/art-data hence they now survive ART updates
* Better cruft prevention when things occasionally go south in saving a dataset
* Performance updates in many areas
* All buttons have a new fresher look

2016-01-08 version 1.6.1.5
* Fixed Exist-db 2.1 related problem when retrieving the project template list (IHE)

2016-01-05 version 1.6.14
* Performance update in user-settings, user-report, templates, value sets, project-versions, datasets, concepts
* Performance - hl7 index is pruned from element, and @message is added (for WSDLs)
* Fixed a problem when saving a template with an associated concept that is not actually associated anywhere
* Fixed a problem when saving a template where attribute ids where not properly taken into account
* Fixed a problem in adding fixed text in an HL7 V2.5 template
* Fixed a problem when compiling with an empty language or with a language that is not in the project name set
* Moved template example generation code into the template API
    * Added a switch based on template format and added a number of common hl72.5xml cases
    * If no format is given hl7v3xml1 is assumed
* Add rejected status to node CSS
* Fixed a bug where retrieval of a specific version of a valueset would also return other versions if they existed.
* Now preserves order in valueSet with regards to concepts|includes|exceptions when getting raw value set

2015-12-21 version 1.6.13
* Performance tweaks

2015-12-18 version 1.6.12
* Performance tweaks

2015-12-14 version 1.6.11
* Fixed a problem deleting the first transaction group in a scenario or the first transaction in a transaction group
* Now marks edited items in the tree in italic (datasets and scenarios)
* Project
    * Now loads latest release by default from the ProjectIndex link instead of live version. This potentially saves a lot of processing

2015-12-10 version 1.6.10
* More performance tweaks.
* Fixed a problem in retrieving a template from a release
* Fixed a problem in getting a value set name for display in various places like datasets
* Fixes a problem in the display of complex content for scenario/transaction triggers.
* Fixes a problem when an original concept is not found
* datasets
    * Don't offer inheritance from a ancestor of the current concept
    * Don't offer inheritance button when the only search results are the concept itself, or any of its ancestors or descendants

2015-12-01 version 1.6.9
* Performance tweaks. Rewritten a number of statements for efficiency.

2015-11-26 version 1.6.8
* Datasets
    * When an error message spans more than your screen, you may now press escape to close it.
    * Fixes a bug where multiple dataset names (languages) cause the dataset viewer not to load
* Datasets/template-mapping/community/issues
    * Now also searches in concept synonyms
* User management
    * Moved cancel/save up so they don't get lost under long lists
    * Added checks to prevent shooting yourself in the foot by adding someone as decor-admin/issues/editor but without decor group
    * Visual updates so the list of visible users is biggers and handles overflow better

2015-11-23 version 1.6.7
* Dataset editor
    * Now supports changing the maximum number of concepts that are retrieved when searching for similar concepts (default 50)
    * Now makes all dialogs a modal window since no dialog make sense to bypass
* Issues
    * Fixed parameter serclosed behavior on issues. The panels would not close
* Template-mapping
    * Fixed technical problem when project does not have a dataset yet
* General: adds anti robots header on all pages to avoid loosing performance on those while indexing live contents doesn't actually add anything

2015-11-11 version 1.6.6
* Transaction editor
    * Fixes a problem in detecting max vs min
    * Performance update

2015-11-03 version 1.6.5
* Project
    * Fixed a bug in adding namespaces. The default attribute was not set
* Issues
    * API now supports multiple ids to retrieve details for
* Scenarios
    * Now allows switching tabs upon load witch ?section=scenarios|actors where scenarios is the default unless you set param actorId=X
* Scenario editor
    * Implements error summary before being able to save
    * Does not allow non-grouping transactions without actors
    * Fixed a bug where a second actors element was added when you added a first new actor and <actors/> already existed

2015-10-29 version 1.6.4
* Transaction-editor
    * Now checks if minimum is <= maximum
    * Now checks if minimum is 0 or empty when Optional
    * Now checks if minimum is 1 or up when Mandatory
    * Now reset minimum to 0 when conformance is reset to Optional
    * Now implements error-summary for convenience in finding offending parts and disallows saving while errors are found
* Template-editor
    * Fix for inability to edit a template when the prototype has seemingly duplicate sibling elements. Extended the duplcate detection logic and enhanced the error output so it is easier to trace the offending elements.

2015-10-27 version 1.6.3
* New projects
    * Upon installation, the collection 'projects' is created under /db/apps/decor/data where all projects live. This causes there to be at least 1 collection for new projects under decor-admin. 
    * Decor-admin will alert you for installed projects (most notably DECOR-examples) so you do not inadvertently create your new projects there and then ''loose them upon updating the package''.
    * Implemented error-summary so you get a nice listing of things to fix before saving is possible
    * Implemented new user selection dialog where only decor-users may be selected for inclusion in a project
    * Added copyright to the list of items to set for new projects
* Project forms
    * Implemented error-summary so you get a nice listing of things to fix before saving is possible
    * Implemented new user selection dialog where only decor-users may be selected for inclusion in a project
    * Made editing project details and ids mutually exclusive. A change in ids locks the other tabs until cancel/save and vice versa.
* User settings
    * Implemented error-summary so you get a nice listing of things to fix before saving is possible
* Scenarios
    * May now delete transactions if there are >1 transactions for. status new or draft. Was only "new".

2015-10-20 version 1.6.2
* Improved concept search: now shortest match first and better performance
* Transaction-editor: don't offer recursive min/max settings on cancelled/deprecated/rejected concepts and do not add it when you click such an option on a parent with a normal status
* Datasets: don't allow deinherit when a parent has inherit
* Scenarios - IMPORTANT!
    * For new scenarios, no @effectiveDate was written
    * Under certain circumstances there was no valid @id on transactions (group|non groups) and the @id ends in a dot. This triggered a problem in creating new ids after that

* '''MITIGATION PROCEDURE'''
    * Update ART package to 1.6.2 or up
    * Go to the scenarios of your project, click Edit on any of them, click Save, no change is necessary. This adds missing @effectiveDates and replaces ids with unique new ones but cannot fix duplicates
    * Check if you already have a duplicate scenario in your project from the project form Development tab "Check DECOR" Look for something like <br/>''There was a problem processing this transaction (possible duplicate @id='X'/@effectiveDate='Y'?) - Actual error:<br/>/db/apps/art/modules/art-decor.xqm - java:org.exist.xquery.XPathException exerr:ERROR The actual return type does not match the sequence type declared in the function's signature: art:getTransaction(xs:string, xs:string?) element()?. Expected cardinality: zero or one, got 2''
    * If this concerns you please contact support

2015-09-22 version 1.6.1
* Template-editor
    * Fixes bug where attribute @extension was marked with datatype cs instead of st
* Project, Home, Language-support
    * Updates some minor visual anomalies
* General
    * Adds missing styles

2015-08-27 version 1.6.0
* Administrative rename for this release

* Projects
    * fixed reference to projectindex. Should be external resolvable link: decor-external-exist
    * fixed copyright editing
    * adds decor service Project Index to view
    * adds project @id to view
    * fixes logo upload for Orbeon 4.7
    * will no longer offer buildingBlockRepositories that points to itself. If you manually update a BBR reference so it points to itself, it is skipped in saving.

* All
    * Improved search performance
    * improved consistency of behavior around locking. will now show who and when everywhere when someone else has a lock
    * (decor-admin only) now allows every lock type to be viewed/removed from project and decor-admin forms
    * fix for missing projects in the DECOR menu when the govenernance group they belong to doesn't exist (anymore/yet) in the configured governance groups)
    * various performance updates by making better index use
    * corrected fall back onto username if full name is empty

* Datasets concepts general (in issues, scenarios, terminology)
    * improved display name for identifiers when they are not directly branched off a baseId
    * improved display of value domain properties
    * read support for concept/synonym, concept/relationship, concept/property, value domain concept/synonym

* Datasets
    * fixed bug that prevented adding new concepts when there is more than one base id to choose from
    * no longer saves dataset/relationship/node()
    * improved display of dataset/relationship comparable to concept/relationship
    * adds dataset/desc when missing
    * read/write support for dataset/relationship and dataset/property
    * read/write support for concept/synonym, concept/relationship, concept/property, value domain concept/synonym
    * visual indicator for inherited concepts in the tree so you do not need to view details for that
    * improved display name for identifiers when they are not directly branched off a baseId
    * improved readability of associated codes by indented their displayName
    * fixed problem in display of value domain concept descriptions
    * no longer returns the context concept when searching for similar items

* Dataset-editor
    * fixed missing existing synonyms on concepts. After save they would be lost
    * in saving items, only save concepts under a conceptList[@id](root='']</span>)
    * after deinherit get the right name for the relationship that replaces it
    * now offers a dialog for adding relationships on datasets and concepts

* Value sets
    * fixed a bug in id assignment when the first id of a certain baseId is created in a project
    * do fully specified name in SNOMED CT hierarchy instead of preferred term

* Terminology-associations
    * do fully specified name in SNOMED CT hierarchy instead of preferred term
    * fixed deleting terminology associations that lack certain information
    * updated the url logic upon adding a missing buildingBlockRepository to the project. Was localhost and now checks server services url

* Codesystems
    * removed a few non-functional editing functions as this way of editing codes is to replaced

* Scenarios
    * fixed behavior when you have added your very first actor
    * added SVG image of the current transaction group if it has at least a transaction of type Initial, is not currently being edited, and if the browser supports SVG. The button that goes to the same image has proven to be overlooked by most users, but is still there too.
    * fixed write support for scenario and transaction version labels
    * enabled write support for the version of a dataset in a transaction
    * fixed marking a scenario or a transaction as 'being edited'
    * visual indicator for inherited concepts in the tree so you do not need to view details for that

* Templates
    * after searching for repository templates select the first result saving a user click in most cases
    * improved view for deeper paths and long element names
    * improved view on used/used by overview
    * improved link to dataset concepts
    * fixed selector that would never display time in a timestamp in the version selector
    * fixed selection of elements that should not be selectable
    * fixed include card/conf override so it doesn't apply to elements only but also to choice and include
    * upon merging an existing template with its prototype don't add min/max 0..* on choices and includes if wasn't explicitly specified in the first place.
    * now returns datasetEffectiveDate on associations (important for versioning)
    * now returns dataset concept path for includes too
    * allow removal of broken template/@ref

* Template editor
    * datatypes for @use and @qualifier should be set_cs instead of st / cs respectively
    * can now correctly set/change datatypes for hl7v2.5xml
    * can now correctly add fixed text for hl7v2.5xml
    * can now correctly add vocabulary for hl7v2.5xml
    * can now search value sets in the dialog
    * can now add value set refs in the dialog
    * can now start the value set editor from the value set dialog for creating a new value set
    * fixed a problem in editing certain prototype based templates. The would not open at all due to merge problems
    * improved behavior in calculation of usage
    * improved performance and reliability when the cache doesn't have a template
    * fixed a problem where the choice cardinality was not visible for editing
    * fixed a problem in opening templates for edit that have a prototype with multiple asserts on the same level
    * editor now allows editing vocabulary/@displayName

* Issue editor
    * fixed view for value set objects. status/version label/contents were missing
    * worked around problem in Orbeon 4.7 where HTML select/label was displayed as-is (raw HTML) when the underlying result set changes. This happened for issue labels after searching and in new assignments
    * replaced fr:select1-button with xforms:select1 in new assignments. This fixes a caching issue where new assignment would appear to have 'remembered' your last setting but did not actually leading to mis-assignments
    * visual tweaks

* ART-settings
    * adds more info when caching of one or more BBRs fails

* Governance groups
    * fixes a client error when entering the form ([SF#242](https://sourceforge.net/p/artdecor/tickets/242/))

* Valueset-ids
    * fixes an HTML bug that caused improper rendering of <nowiki>[
* Governance groups
    * fixed unavailable language selector top right

* MyCommunity
    * fixed searching
    * added check on prototype/@type so it doesn't allow whitespace (that's what @label does)
    * improved behavior when entering that form without read permission
    * dataset tree no longer expands in full by default (user request)

* Configuration (admin only)
    * now show which BBRs failed to load when refreshing the BBR cache

* Orbeon 4.7
    * '''reversed the [[Browser_compatibility|Internet Explorer 11](</nowiki>ref<nowiki>]</nowiki>)] recommendation. Using Orbeon 4.7 one should remove the compatibility mode for ART-DECOR'''
    * all textareas now have incremental="true" because they do not get picked up otherwise
    * various UI tweaks due different defaults in Orbeon 4.7

2015-08-20 version 1.4.11
* See version 1.6.0

2015-07-26 version 1.4.10
* See version 1.6.0

2015-07-15 version 1.4.9
* See version 1.6.0

2015-07-12 version 1.4.8
* See version 1.6.0

2015-06-24 version 1.4.7
* See version 1.6.0

2015-06-17 version 1.4.6
* Datasets
    * Fixes a bug when adding inherit on the first concept in a dataset
    * Fixes a bug in inherit where the inherit/@ref is not handled
    * Fixes a performance problem in saving a dataset
* Value set editor / terminology
    * SNOMED CT: adds F, P, S before every term, not just in the main result table
    * SNOMED CT: adds support for display of preferred terms
* Decor-admin
    * Dataset and Dataset/concept on new projects are now created with status draft instead of new.
    * Dataset/concept is now created as item instead of group
* Terminology by default no longer offers cancelled|obsolete|deprecated concepts.
* Transaction editor by default no longer offers cancelled|obsolete|deprecated concepts. They are not hidden when added to the transaction

2015-06-10 version 1.4.5
* Can now delete terminology associations with incomplete contents
* Improved transaction cardinality for concept usage when there's a condition

2015-06-06 version 1.4.4
* Fixed problem where editing a concept with a conceptList that references another conceptList would lead to a "Exception in client-side code"

2015-05-12 version 1.4.3
* Scenarios now allows for sending parameter actorId to switch over to the tab for actors upon loading.
* Fixes scenarios group around dataset that prevents display when there's nothing to show.

2015-04-30 version 1.4.2
* Fix for a dataset form problem when clicking Edit on a concept that inherits AND has valueDomain of type 'code' or 'ordinal')

2015-04-02 version 1.4.1
* Performance updates in all DECOR related areas

2015-03-29 version 1.4.0
* New feature: governance groups. Read more in [with Governance Groups]([Dealing)]
* New feature: server ids. Read more in [* New feature: filter inactive concepts in dataset view
* New feature: import a connected value set in a dataset to allow adding custom names and descriptions
* New feature: view a connected value set in a dataset without leaving the page
* Enhancement: DECOR project form now allows editing the default namespace and add new ones
* Enhancement: associating templates with dataset items may now be done based on attributes. Elements was already supported.
* Enhancement: template editor and viewer now support element/@strength. Read more in [[DECOR-rules]([ART_maintenance_manual]])]
* Enhancements in template-editor: can now start from empty template instead of from a prototype, can now add new items above top level item, can now add attributes/elements other than the datatype supports (normally relevant for (CDA) extensions), example generation improved for templates without a single top level element, add/remove datatype declarations and more
* New feature: projects may now be marked experimental
* Performance improvements in various areas like searching, viewing, compiling, building releases etc.

2015-03-26 version 1.2.31
* Templates: relationships are now attributed to the correct source project/bbr
* Template editor:
    * Attributes from prototypes are now built comparably to the focus template attributes
    * Default for min/max is now '' so they are not added inadvertently after save
    * Doesn't instantiate conf/mand on choices anymore
    * All vocabulary is now instantiated the same way
    * Template-editor now loads even load when minimumMultiplicity is not a number (e.g. empty)
* Issues: do not apply any other filter when searching by id

2015-03-25 version 1.2.30
* Fixed a problem where templates with explicit classification/@type='notype' were skipped in the list
* Templates: attribute normalization now takes defensive approach instead of opportunistic approach. Only supported attributes are saved during normalization. This prevents new attibrutes from being rewritten the wrong way (e.g. @id)
* Templates: small performance optimization in retrieval of cached templates
* Templates: more reliable marking of missing valueSets in edit mode
* Templates: added retrieve/save for element/@strength (moved from vocabulary element)
* Templates: adds toplevel element around every existing and new examples without one upon edit so the result is well formed -- cannot save otherwise
* Fixes a path bug in reindex
* Fixed warning "org.xml.sax.SAXParseException: White spaces are required between publicId and systemId."

2015-03-23 version 1.2.29
* Bug fix in reindex.
* Fixed exist-db warnings: 'org.xml.sax.SAXParseException: White spaces are required between publicId and systemId'

2015-03-19 version 1.2.28
* Compilation for publication now includes value sets that are bound through templates but do not have a valueSet[in the project. This frequently happens in template[@ref](@ref]) situations.
* Bug fix: editing value sets now also offers editing completeCodeSystem

2015-03-16 version 1.2.27
* Issues:
    * Implemented configurable max result set for the issue list and set the max to be 75 rather than 50.
    * Did not make the max result set UI configurable (yet)
    * Made sorting by date descending rather than ascending
    * Made the default sorting column "date" rather than "id"
* Fixed rather severe issue in calculating value sets for getFullConcept [RetrieveTransaction / and compiled releases](impacts). If multiple versions exist, and @flexibility was not specified, the first of the result set was picked leading to the oldest. The function vs:getExpandedValueSetByRef will by design return *all* value sets without @flexibility. If you actually want the latest, you should supply 'dynamic'. Now when @flexibility is missing, 'dynamic' is substituted.

2015-03-11 version 1.2.26
* Internet Explorer 11 tweaks. Now points to compatibility mode for first aid, but this is really patch work

2015-03-06 version 1.2.25
* Templates: fixed problem in determining the correct status for included templates and vocabulary references.
* Templates: fixed problem in finding linked concepts for included templates

2015-03-06 version 1.2.24
* Fixed performance problems on the issue list.

2015-02-19 version 1.2.23
* Significant improvements in loading issues. Adds missing indexes and optimizes queries.

2015-02-04 version 1.2.22
* Template-editor
    * Adds root|value|representation to the list of default HL7 attributes
    * Fixes problem when adding a new attribute before changing selection. Now adds top in list as expected

2015-02-02 version 1.2.21
* Templates: after removing every element/attribute in a template, restore merge with prototype upon reopen for edit
* Value sets: fix a bug in usage calculation that prevented static binding in templates to be recognized when value set was newest version
* Value sets: fixed display bug where both localized and un-localized string for Static would pop up.
* Datasets: serialize inherited comments and concepList/concept descriptions too.

2015-01-26 version 1.2.20
* Templates: Fixed display bug in examples, assert, report
* Templates: Fixed display of RetrieveTemplate service
* HTML: replaced locally downloaded RetrieveTransaction with link

2015-01-20 version 1.2.19
* Fixes search for inherited items
* Fixed problem introduced when solving bug for parsing examples, but where the contents are not valid xml but just text which is legal.
* Fixed saving assert/report text
* Fixed retrieval of assert/report/let/defineVariable for view

2015-01-15 version 1.2.18
* Bugfix: datasets: fixed a bug that prevented display of a group to-be-inherited

2015-01-13 version 1.2.17
* Bugfix: parse template example as xml, not html
* Bugfix: save template report as report, not assert
* Bugfix: menu entry for reference sets now builds correctly

2015-01-13 version 1.2.16
* Menu entry for reference sets now builds correctly
* Value set api fixed for released versions in multiple languages
* Small governance groups fix for multiple languages
* Reversed order in decor menu. Governance groups first, then the rest

2014-12-22 version 1.2.15
* Concept search: Add name to search result when searching by id and target is inherited

2014-12-10 version 1.2.14
* Add new variable $get:strDecorServices to support DECOR services 1.2.3

2014-12-08 version 1.2.13
* Datasets: Removed Delete button from treeview
* Datasets: Fixed error when clicking a concept in the diagog with treeview of a similar group

2014-12-01 version 1.2.12
* Template viewer now has button "Set status to cancelled"
* Dataset description is now in an accordeon to keep focus on dataset contents
unified procedure. Having two caused maintenance issues. Having one solves that with little added complexity.
* Template editor: new templates now respect default base id better
* Template editor: Now retains element/@id if available which is useful for mode version/adapt
* Template editor: Fixes bug for attributes in building merged templates that caused @originalOpt to go missing
* Template editor: Adds missing support for conformance C
* Template editor: rolled creating templates (new/version/adapt) and saving templates into 1 unified procedure. Having two caused maintenance issues. Having one solves that with little added complexity.

2014-11-23 version 1.2.11
* Don't offer to add id's when the template is a referenced template
* Fixed creation of template associations on referenced templates [* Template editor: upon retrieval of prototypes check cache first [SF#207](SF#208])

2014-11-22 version 1.2.10
* Datasets/scenarios/terminology/home: performance updates by loading usage and issues asynchronously and displaying … while loading. This does not interfere with core functionality and lets the user switch context a lot faster.
* Templates that need to come from a different server no longer gives an error when retrieval fails. They just turn up empty
* Template prototype list now has templates listed on first classification only (was "all")
* Datasets: now saves inherit to original concept only. For the top level this was already covered by returning original concepts only as similar items, but for groups with inheritance at lower levels this check was bypassed. Now checks at every upon save
* Datasets: saving now keeps concept/@officialReleaseDate if present (ART doesn't support view or edit yet)
* RetrieveTransaction/RetrieveDataset shortName: replace '?' with 'q', names occur quitre often with and without '?' in datasets, i.e. 'Auto-immuun aandoening?' and 'Auto-immuun aandoening'
* art-decor.xqm: removed unused function art:prepareConceptForStore

2014-11-18 version 1.2.9
* Dataset editor: Fixes an eternal loop situation upon inheriting a group

2014-11-18 version 1.2.8
* ClaML based terminologies are now parsed better for their names
* Orbeon 4.x fixes: explicitly cast to xs:date or xs:dateTime, variable scoping problem fixed in menu builder, css updates, etc.
* Template editor: now respects the default base id for templates better

2014-11-17 version 1.2.7
* Template editor: Now retains element/@id if available which is useful for mode version/adapt
* Template editor: Fixes bug for attributes in building merged templates that caused @originalOpt to go missing
* Template editor: Adds missing support for conformance C

2014-11-11 version 1.2.6
* Improved attributes on concept-usage so the usage views have better underlying data.
* RetrieveTransaction: fix for useLocalAssets and the link on value sets

2014-11-10 version 1.2.5
* Updated all views where cardinality/conformance is shown. NP overrides cardinality, mandatory overrides required
* Updated Dutch translations for conformance Required and Mandatory
* Project now only lists status for releases, not for versions

2014-11-09 version 1.2.4
* Valueset viewer: improved sorting by checking displayName first and name second
* Valueset viewer: added error message when valueset-list retrieval fails
* Dataset viewer: no longer shows similar concepts after searching, but only in edit mode
* Dataset viewer: fixed finding the object by excluding issues
* Issues: fixed searching for objects after update in search-api
* Datasets now come up sorted on effectiveDate (descending) and newest is picked rather than latest/final (datasets, community, template-mapping, terminology)

2014-11-08 version 1.2.3
* Fixed ticket#204: https://sourceforge.net/p/artdecor/tickets/204/

2014-11-06 version 1.2.2
* Database: restored index on decor/core as it caused the template editor to break
* Template viewer: Fixed a bug in rendering >1 "text" elements that caused failure to display at all
* Value set viewer: usage now comes up reliably when asked for a specific version

2014-11-05 version 1.2.1
* Template editor removed check on max >= min as in some cases this would be triggered in error leaving the user without option to save.
* Performance updates. Split decor indexes into 3 parts (cache, data, releases).
* Expanded template retrieval now correctly resolves non-local includes
* Transaction editor: fixed logo and menu

2014-11-04 version 1.2.0
* Moved user-settings from home to a separate page. Click on your name after login to get there
* Improved display of concept usage when usage occurs in a dataset. Now includes the path to occurrences
* Dataset editor now includes support for de-referencing conceptLists while preserving terminology associations (referencing a conceptList was never supported through ART but is possible in the DECOR format)
* Dataset editor now implements the full status machine. Upon applying a new status you may apply that recursively to all child concepts with the same status.
* Dataset editor now only allows inheriting full groups and no longer lets you choose. Even after inheriting it is no longer possible to move other concepts into it as long as you do no deinherit the group.
* Scenario editor now implements the full status machine. Upon applying a new status you may apply that recursively to all child concepts with the same status.
* Scenario viewer/editor now allows filtering on dataset
* Transaction representing templates now have concept usage the concept details
* All template lists are now harmonized and display based on classification first and with proper status icons. Note that this may mean that a template occurs multiple times in the list if multiple classifications exist
* You may now refresh the list of value sets from terminology to create a new binding rather than refreshing the page
* Template editor no longer allows saving when you define an attribute twice
* Value set editor and Terminology association now support status on concepts that come from ClaML. ClaML doesn't support status directly, but Meta key name='statusCode' server that purpose in ART-DECOR. Current terminologies contain active concepts only, but given a use case this may now be different in new terminologies.
* Issues with status feedback are now by default in the initial list
* Extended language support / customization for administrators
    * Edits made in ART (language-support) are preserved between package updates. You can see which ones you have edited locally and revert to the default version
    * You may now add new languages
    * Language support adds support for menu items
    * ART server-settings now includes switch for custom menu file and adding your own logo
    * Updated [with explanation for these features
* Preparations for migration to latest Orbeon version. Type casting on null for xs:dateTime is now prevented

2014-10-20 version 1.0.36
* Dataset editor now allows inheriting from both local and repository concepts
* patched ART variables (/modules/art-decor-settings.xqm) used from XIS: 
    * Added string variable to CDA stylesheet

2014-10-16 version 1.0.35
* patched ART variables (/modules/art-decor-settings.xqm) used from XIS: 
    * Updated setting $get:strXisData to concat($get:root,'/xis-data') to align with all other *Data variables
    * Added former string under $get:strXisData as $get:strXisHelperConfig concat($get:strXisData,'/data')

2014-10-14 version 1.0.34
* Template editor: now supports adding elements/attributes "before" the selected item

2014-09-24 version 1.0.33
* Fixed dataset edit bug that could cause inadvertent deletion of concepts upon Cancel of editing

2014-09-23 version 1.0.32
* Fixed a bug in adding data element objects to an issue which caused ignore of selected dataset

2014-09-22 version 1.0.31
* Terminology adds refresh of value set list to get new value sets without refreshing the full page

2014-09-17 version 1.0.30
* Added missing function local:getOriginalConcept

2014-09-15 version 1.0.29
* Fixed a number of concept search problems in the search api. Total count was off, prefix not totally respected
* Added path of a concept in the dataset to the output of the search api function
* Added generated path of the concept to the search drop down of mycommunity/dataset/terminology forms
* Added generated path of the concept to the similar concepts on the datasets form
* Added generated path of the concept to the concepts on the issues form when adding a concept as object

2014-09-15 version 1.0.28
* Made display of concepts usage more useful when usage is in datasets by adding the path to the view in place of the dataset effective date.

2014-09-08 version 1.0.27
* Fixed language drop down for projects with one or more languages not in the ART package
* Interface tweaks in project creation (decor-admin)

2014-09-05 version 1.0.26
* Fixed bug when switching an existing concept from any other type to code or ordinal would not give you a concept list to expand

2014-09-05 version 1.0.25
* Fixed logo lookup. The logo may be retrieved dynamically upon page load (e.g. ClaML). The xsls however assumed that the logo would be hard coded in the form.
* Added styles for issue status

2014-08-28 version 1.0.24
* Bugfix in handling of issues labels when selected on issues form. Now any match is returned not just exact match

2014-08-27 version 1.0.23
* Upon saving an edited valueSet, don't save @projectPrefix

2014-08-27 version 1.0.22
* Adds status 'feedback' to the default list of selected statuses on issues form

## DECOR core files
DECOR-core contains definitions for the DECOR format in XSD, transformations that produce HTML/Schematron/Wiki etc.

2021-04-02 version 2.0.15

* DECOR.sch
    * Add check for unique codedConcepts in a codeSystem
* DECOR.xsd
    * Add CodeSystem/conceptList/codedConcept/@ordinal support
    * Add localizations in IntensionalOperators documentation
    * Allow include|exclude codeSystemVersion and codeSystemName
    * codedConcept needs optional version attributes

2020-12-02 version 2.0.15
* DECOR2html
    * Changing Courier to Courier family and added monspace
    * Fixed FHIR link generation when the version is not the default version
    * Fixed css bug where missing classes caused missing display hints 
    * Improved dataset conceptList/concept rendering when there are synonyms and/or ordinals
    *  Moved constraint show mechanism to the right place, eliminate duplicates when rendering
    * Render constraints under include under certain circumstances
* DECOR2schematron
    * Correction on datatype ST
    * Check for deprecated codes of value sets, issuing a warning
    * Start identifying individual assert/report rules with mnemonics for better parametrization later
* DECOR 2wiki
    * Changed link handling in WordPress publication engine
    * Changed copy mode of description node so that they don't spoil wiiki output by leading blanks on a new line; allow rendition on include->constraint in all situations
* All
    * Add display support for deprecation of completeCodeSystem and refsets

2020-10-20 version 2.0.14
* DECOR2schematron
    * Fixed default instance html problems causing duplicate name entries
    * Patch for failing namespace checks. Don't check for empty namespaces
* DECOR2html
    * Fixed a bug in rendering conditions on transaction concepts
* DECOR2wiki
    * Skip valueset without concepts in wiki creation
* DECOR.xsd
    * Improvements in German language

2020-08-05 version 2.0.13
* DECOR2html
    * Replaced %lt;tt> which is deprecated by corresponding HTML replacments, added new icon
* DECOR2schematron
    * Data Type support TS.AT.TZ (corrected) and TS.AT.VAR
    * Fixed a bug in predicate generation on hl7:code elements that would add <nowiki>[</nowiki>not(@nullFlavor)<nowiki>]([ART_maintenance_manual]])</nowiki> even when a predicate was preset
* DECOR2wiki
    * Implemented scenario export for ADAWIB
    * Extened wiki extracts incl extra language
* DECOR.xsd
    * Add missing compilation attribute @localInherit for dataset//relationship
* DECOR.sch
    * Fix false negative on nullFlavor usage

2020-06-06 version 2.0.12
* DECOR.sch
    * Many smaller and bigger fixes, readability improvements
* Datatypes
    * Add support for GLIST_TS, GLIST_PQ, SLIST_TS, SLIST_PQ datatypes for use in the template-editor and schematron publications
    * Corrected unit data type for IVXB_PQ
* DECOR2html
    * Use jQuery 1.11.3 from local assets folder if requested <useLocalAssets1/>
    * Don't show where path in templates unless explicitly set in the definition
    * Improved gathering usage details for templates. Now always includes transaction if relevant, not just for top level. Also improved performance
* General
    * Add local copies of jQuery 3.3.1 and 3.5.1

2020-04-28 version 2.0.11
* Add DECOR.sch as extraction of DECOR.xsd rules. Optimized for compiled vs uncompiled and project pointers vs BBR pointers (e.g. a conceptList)
* DECOR2html
    * Improved finding displayNames for OIDs

2020-04-20 version 2.0.10
* DECOR2html
    * Prevent missing/reporting names we already have calculated in the compile process

2020-04-14 version 2.0.9
* Support searching issues on/after date based on last modified date. Supports partial dates/datetimes and relative datetime (e.g. T-5D)
* DECOR2html
    * Improved reported features in Compile tab
* DECORschematron
    *Fixed a technical error when a transaction concept could not be resolved in the creation of defaultInstances

2020-04-11 version 2.0.8
* Activate FHIR R4 support
* Support for new valueDomain/@type 'time'
* Corrections for the confluencve ADAWIB support
* Datatype TEL.AT: allow TMP (upon request from Austrian authorities)
* Datatype II.NL.BSN: Extended documentation to say that zeroes should be prepended if necessary to get to 9 digits. Prevent potential for hard error if the @extension does not contain 9 digits
* Add support for hl7nl-IVL_QTY
* Add support for hl7nl-PIVL_TS
* Dutch update: Datasetelement > Datasetconcept
* Extended legend line for valueSets to include text on OTH + originalText
* Improved consistency check on terminologyAssociations vs template
* Don't warn/error for elements with @xsi:nil='true' in closed logic, as you may regard those as "not present" too.
* Don't warn for missing nullFlavor when template is not V3
* V2 IGs: Various fixes, Improved link creation, Only segments as needed for this scenario

2019-09-29 version 2.0.7
* DECOR.xsd
    * Lower warning to info level for nullFlavor behaviors based on valueSet/conformance
    * Raise warning to error level for name based references instead of id based references
    * Simplified error messaging around representingTemplate/concept with question marks in min or max. Reports once per transaction instead of per occurrence
* DECOR2html
    * Fixed link generation and FHIR retrieval of artifacts in development builds
* DECORmycommunity.xsd
    * Allow author name to be populated
* DECOR-communityADA.xml
    * More documentation on the types. New type @adaId to add an id on a concept (normally a group) that you may use to refer to from another concept (normally a concept[* Template2Example
    * Fixed problem for choice processing in overview mode: additional comma was inserted before the choice
    * Fixed problem for choice processing in overview mode: choice contents were not displayed

2019-07-18 version 2.0.6
* DECOR.xsd
    * Add compilation parts in relationship (name | desc for concepts)
    * Add compilation part for community in DatasetConcept
    * Add compilation attributes to concept/relationship to point to the relevant concept
    * Add schematron checks specifcally for compilation that check if relationships resolve
* DECOR2basics
    * For min/max functions. Force respectively 0 or * if empty
* DECOOR2html
    * Rendition include|exclude @ref added
* DECOR2schematron
    * II.EPSOS error message text changed (technical correction)
    * hl7nl:PQ value attribute always of type real

2019-05-30 version 2.0.5
* Add ISO 13582 OID (2012 and 2015) registry definitions
* DECOR.xsd
    * Updated Oid pattern to what was present in datatypes-base.xsd from HL7
    * Improved consistency checks and sqf on baseId || defaultBaseId
* DECOR2schematron
    * Fixed recursion issue in creation of html when the recursion was triggered from an inclusion
    * Fixed recursion issue in creation of default instances
    * Improved readability of two schematron messages
* DECOR2html
    * Improved listing of datasets so project local dataset are listed first and referenced datasets second. 
    * Avoid duplicate templateAssociation/concept listings, but if they do occur deduplicate before rendering them.
* DECOR2wiki
    * Add err namespace prefix declaration
* DECOR-optional-checks.sch
    * Improved context in issue reports
    * Improved validation in Singular inherits. No longer considers deprecated/cancelled/rejected concepts in counting

2019-03-12 version 2.0.4
* DECOR.xsd
    * Fixed schematron check on terminologyAssociation and identifierAssociation connected to concept/contains
    * Fixed default value false for new attribute experimental
* DECOR2hl7v2ig
    * Minor tweak for groups in message table without contents
* DECOR2html
    * Improved initial assertion text by applying an itemlabel based on the toplevel template
    * Added support for valueSet/@experimental, valueSet/@caseSensitive and codeSystem/@caseSensitive
* DECOR2schematron
    * Improved top level schematron title text
    * Enhanced default instance creation. XML now contains any concept available in the compilation. HTML now comes in two flavors: transaction specific where only transaction concepts are rendered and if the dataset has contains relationships also concepts that are referneced from different dataset as those might be relevant (not going to calculate if they are...). And an HTML that just lists all concepts connected to the instance tree.
    * Getting names for ids now makes much better use of compilation attributes by inspecting @iddisplay for @id and @refdisplay for @ref before trying on its own.
    * Improved datatype checking for elements called through containment relationships
* DTr1_ANY
    * Be namespace independent
* DTr1_IVL_TS
    * Improved certain low/high comparison with timezones
* DECOR2svg
    * Synced fixes from transaction-group-2-svg.xsl in decor/services/resources
* DECOR2wiki
    * Catch errors on template diagram generation

2019-01-22 version 2.0.3
* DECOR2html
    * Fixed a problem where the refbox was built for the wrong prefix
    * Make better use of compiled iddisplay present in compilation files
* DECOR2schematron
    * Fixed issue introduced in previous version that caused attribute/vocabulary definitions regardless of xsi:type to be processed as if it was xsi:type
    * Fixed the order inside decor-parameters.xml when it is initially written
* DECOR.xsd
    * Added compilation attributes for template/relationship
    * Changed AssertRole hint to info

2019-01-14 version 2.0.2
* DECOR2schematron
    * Fixed a problem in schematron generation for attribute xsi:type with vocabulary elements. xsi:type is always QName so simple code comparisons do not work
* DECOR2html
    * Fixed a problem introduced in the previous version where reference boxes could list the wrong project prefix

2019-01-09 version 2.0.1
* DECOR.xsd
    * Updated FhirObjectFormat values from dstu2 to 1.0 and from stu3 to 3.0
    * Improved error message for empty context/@path (they should not be saved anymore either)
    * Added dataset/@prefix in the CompilationAttributes section
    * Added concept/inherit/@originalPrefix as compilation attribute
* DECOR2html
    * Fix for technical failure to build when dupliucate names for a template or valueset exist
    * Open assert|report see reference in new tab/window
    * Check parent element for ident/url too. When redering live valueSets, that's where they are.
* DECOR2hl7v2ig
    * Fixed an issue with descriptions with mixed contents
* DECOR2schematron
    * Fixed closed logic for include/element[@contains](contains])). The contains first needs unpacking before handling
    * Fixes a problem for generating schematron when a valueset binding contains only exceptions
    * Fixes a problem in creation of phases in schematron when multiple template( version)s have the same @name
* Template2Example
    * Fix missing attributes on examples
    * Fixed support for example generation from the template editor where some parts might be selected and other might not.
    * Fixed example generation for @xsi:type
* Coreschematrons
    * Added XML schema defaults for EIVL_TS.event


2018-11-05 version 2.0.0
* DECOR.xsd
    * Improved checking of nullFlavor on value sets. Now detects nullFlavor attribute definition in addition to a value set
* coreschematrons
    * DTr1_ANY Add @unit='1' to the list of defaults to support, fix detection of @nullFlavor + empty elements

2018-09-29 version 1.8.48
* DECOR.xsd
    * Aligned codeSystem elements as available for valueSet (copyright, desc, publishingAuthority etc.)
    * Added transaction level concept context and bindings
    * Added valueSet support for designations on intensional statements (include | exclude)
* DECOR2html
    * Added viewport meta tag as per recommendation for more responsive pages
    * Moved inline onmouseover/onmouseout to CSS for cleaner html
    * Fixed bug in rendering template/valueSet ref lines on the identifiers tab
    * Beter reuse of variables avoiding duplicate calculation
    * Prevent dead links in the template header when the target concept does not exist
    * Prevent dead links on issue/objects
    * Fixed bug in opening/closing the transaction level concept section in rules.html
    * Added DECOR CodeSystem rendering capabilities
    * Added basic FHIR StructureDefinition rendering capabilities
    * Don't list a compile error when the name in the valueSet/@ref doesn't match any valueSet[match. Disjoint names are not a real issue.
* DECOR2hl7v2ig
    * More flexible widths based on em units
* GovernanceGroup2html
    * Add counters to tab headers
    * Make tables 100%

2018-08-23 version 1.8.47
* DECOR.xsd
    * Improved schematron check on NullFlavor
    * Add support for decor/@language = <nowiki>*</nowiki>
* DECOR2html
    * When valueSet or template is included through reference, render @statusCode as present instead of 'ref'
    * Fixed SF#318 "Terminology and rules pages updates"
    * Fixed "other versions of this valueSet" which was empty even though there were other versions
    * No longer evaluate Used by for Templates when extracting the Wiki. The lists in governance groups grow to 100s of entries for often reused templates and are useless in the wiki. Uses stays as is.
    * ValueSets sourceCodeSystem/@canonicalUri support
* DECOR2schematron
    * De-link of TEL and URL schematrons for TEL.AT and warnings instead of errors
    * Simplified and refixed checking attribute values, e.g. when mixed valueSet and code bindings exist
    * Skip checking @codeSystemVersion
    * Fixed handling missing @timeStampPrecision
    * Fix to prevent empty valueSets from causing illegal xpath expressions
* DECOR2wiki
    * Safety checks for singular properties
    * Show Versions only once in wiki
* General
    * Localization Dutch update: Geldigheid was misunderstood. Changed into Ingangsdatum

2018-07-04 version 1.8.46
* DECOR2docbook
    * Render Identifier lists also in landscape format in PDFs
* DECOR2schematron
    * Fixed bug introduced in previous version causing attributes to miss @-sign in xpaths
    * Fixed a bug that caused inability to retrieve the right namespace-uri for an attribute xsi:type declaration

2018-06-12 version 1.8.45
* DECOR.xsd
    *New syntax added for "max X digits" in property/@fractionDigits using a trailing dot, so "3." means "max 3 digits"
    * Don't info if templateAssociation/concept is missing and is compiled in: it is not a/our problem
    * When error if templateAssociation/concept is not connected to an existing elementId/elementPath add @ident to the message so we know what project authors to poke
    * Provenance attributes activated on templateAssociation/concept as these are compiled from anywhere.
    * Relaxed check on co-constraints in a valueSet from ERROR to WARNING. Also now only barks when nothing is specified at all. Just NullFlavors would be fine
* DECOR2html
    *Changes of layout Governance Group Lists, error corrections
    *Improved rendering of valueDomain/property. Now renders every property even if unlikely, but renders expected properties first. Does not render empty columns. For unexpected property types, the title is italic and has a mouseover text indicating unexpectedness.
    *Added diff rendering logic for future purposes
    *Added link to original artifact from valueSet[@ref](@id])
    *Fixed scripting issue in the bottom assets
    * Alignment between terminology and rules tabs. Versions of valueSets/templates are now both hierarchically under the latest version. Terminology tab already did this. ValueSets now also get the id next to the name, which makes them searchable. Templates now get the first part of the description if provided
    * Alignment of valueSet and template meta data effectiveDate/expirationDate and "other versions"
    * Now renders only 1 language for template constraints instead of all
    * Correct handling of version labels
    * Governance Groups
    ** Fixed statusCode in the header of templates and valuesets
    ** Alignment of reference to BBRs, based on GovernanceGroupList
    ** Alignment of index pages for datasets/valueset/templates
    ** Fixed i18n support on column names
    *DECOR2schematron
    *Changes in predicate generation:<br/>- When id or code element is found, there will no longer be checks for structural attributes too. This does not make the path more secure, but costs performance<br/>- When no id or code element is found, use as many structural attributes as defined and if there is exactly 1 element without @datatype, with @minimumMultiplicity > 0 and not @conformance = 'NP', use that too. This helps with sibling entry | entryRelationship | component etc. that only differ based on child element
    *Always generate phases when closed logic is active. This allows you to work with and without closed logic using the same set
    * Fixed a problem where an empty &lt;extends/> was written
    * Performance boost: don't duplicate assertions/reports that are also triggered through &lt;extends/>
    * Adds logic at the toplevel schematron that check whether or not the expected start of the content is in the instance and issues a warning if it is not
    * Fixed escaping of element/text in schematron
* DECOR2wiki
    * Add a tag, rewrite of get template in wiki export
* DTr1_ANY.sch
    * Allow nullFlavor UNC and originalText.

2018-03-27 version 1.8.44
* GovernanceGroupList
    * New: governance groups artefacts compiled into one view
* DECOR2schematron
    * Improved @xsi:type support. When required, don't allow omission
    * Slight improvements for intensional value sets
* DECOR2html
    * Rewrite of datasets page to support datasets from other projects. Relevant to support concept contains relationships.
    * Rewrite of templateAssociation rendering. Now only renders concepts that are truly part of the project. This is because compilation will get all templateAssociations from anywhere, to support datasets with contains relationship from potentially anywhere.
    * Now renders statusCode of the inherited/contained concept
    * Fixed rendering of dataset/properties folding
    * Fixed rendering of inline concept terminologyAssociations (relevant for contained stuff not in the terminology section)
    * Fixed a problem getting terminology associations for concepts that inherit from inheriting concepts
    * Now no longer opens a new tab for concept links that are in the same dataset as the target
* DECOR.xsd
    * Removed most default values. They interfere with schema-aware schematron rules for no good reason. flexibility and isMandatory now have no default value. This does not change anything in further processing
    * Added checks against adding NullFlavoras concept in ValueSet except the HL7 ValueSet for NullFlavor itself
    * Introducing DECOR concept maps as conceptAssociations
* DECOR-optional-checks.sch
    * Don't error/warn when ancestor-or-self is cancelled/deprecated/rejected
    * Fix for contained concepts

2018-02-21 version 1.8.43
* DECOR2schematron
    * Fixed a bug in generating schematron for attribute values with a single quote
    * Fixed a bug in generating schematron for mixed datatype projects (V2 / V3)
    * Added whitespace in | constructs for readability
* DECOR2html
    * Add terminologyAssociation/@strength support
* DECOR.xsd
    * Allow lower-case UUID if only because eXist-db generates those, but more because the UUID standard says that the display form of a UUID is lower-case
* Introducing ips:designation extension to code datatypes

2018-01-25 version 1.8.42
* New service: GovernanceGroup2List
* New html/xml output for DECOR2schematron: HL7 V2 IG + ConformanceProfile

2018-01-12 version 1.8.41
* DECOR2schematron
    * Dtr1_ANY fixed for other namespace than hl7

2018-01-11 version 1.8.40
* DECOR.xsd
    * Added @canonicalUri to dataset, transaction, valueSet, codeSystem, template, structureDefinition allowing an optional HL7 FHIR URIto be stored with a artefact
    * Improved binding support. From CNE/CWE to required, preferred, extensible, example
* Templates and TerminologyAssociation
    * Improved support for binding strength
* Templates
    * Added documentation link for datatypes
* DECOR2Schematron.xsl
    * Moved certain variables to basics, fixing wiki generation
    * Implemented Oxygen 19.1 XSD + SCH measures that prevent complaints about schema defaults
    * DTr1_II.NL.* Print offending attribute
    * Add DTr1 TS.DATETIMETZ.MIN
    * TEL.AT change of error to warning following request
    * Fixed problem with lower level nullFlavors e.g. <effectiveTime><low nullFlavor="UNK"/></effectiveTime> where the parent level was rejected
    * Now copies code systems to runtime environment
* DECOR-optional-checks.sch
    * Fixed situation where inherited group concepts were expected to have a valueDomain
* DECOR2html
    * Don't render valueSet/conceptList if there's nothing in it (happens when it only contained includes of valueSets with completeCodeSystems
* Template2Example
    * Prevent erroring out when @contains relationship defines the same attribute that the element that calls the containment also defines. (only generate attribute from element)

2017-11-20 version 1.8.39
* DECOR2schematron.xsl
    * Fixed errors in predicate generation when there is more than one id/code element to act on
    * Extended predicate generation to handle more options around choice
* DECOR2html.xsl
    * Fixed rendering of attributes as 1..1 F when then element is not actually mandatory.

2017-11-19 version 1.8.38
* Template2Example - fixed technical issue preventing example generation

2017-11-13 version 1.8.37
* DECOR2schematron
    * Fixed a problem generating schematron for top level templates without a top level element (e.g. for context id '*')
    * Fixed a problem generating schematron for elements with multiple discriminator id or code elements at the same level
    * Fixed a problem generating schematron for elements with multiple discriminator id or code elements at different levels
* DECOR2html
    * Fixed a problem causing empty groups to appear in transaction tables when no underlying concepts appear in the transaction

2017-10-23 version 1.8.36
* DECOR.xsd
    * Improved schematron checks for templateAssociations by checking the terminologyAssociations for the correct version of the concept.
* DECOR2schematron
    * Fixed a problem when a template specifies multiple required coded elements and no templateId
    * Export ART-DECOR code systems to runtime environment, too
    * Fixed a problem where a required (conformance R) element would be marked mandatory based on a required attribute @xsi:type
* Core schematrons
    * Add new flavor II.NL.BIG
    * Added see references to all core schematrons as per https://sourceforge.net/p/artdecor/tickets/285/
    * Refinement: now only flags duplicate element when the @use attributes match too. This allows sending duplicate names, addresses and telecommunication strings with different uses (EN, AD, URL)
    * DT-II: Corrected UUID pattern for Additional Strict Schematron Rules (SSR)
* DECOR2html
    * Add anchor/self-link to versions/releases on project.html so you may reference each directly
    * Improved handling for templateAssociation/concepts that cannot be found, especially in live rendering
    * Disclaimer textual update in Dutch
    * Fixed getting concept names on associations
    * Fix for art-decor.org domain and live/liveservices links
    * Fixed a problem where @isMandatory would be false when the include did not specify it, thereby overriding the @isMandatory status of an included element
    * Fixed links in template assocations. They linked to the dataset, not the concept

2017-10-01 version 1.8.35
* DECOR2Schematron
    * Major rewrite under the hood
    * Now tells you what elements were expecting in case of closed schematron error/warning instead of letting you guess
    * Improved handling of datatypes when the format is something other than hl7v3xml1
    * Add support for switchCreateSchematronWithExplicitIncludes in development compile
    * Handle intensional value set definitions properly in SCH engine (not yet fully supported for validation)
    * Protection against single quotes in vocabulary @displayName, @codeSystemName and @codeSystemVersion
    * Fixed transaction/@label to be transaction/normalize-space(@label) because due to lacking input protection some projects have leading/trailing spaces in there.
    * Updated to support all namespaces on any template as indeed one template is not enough especially when all includes regardless of use are present in the top level schematron
    * Now the compile process creates instance2schematron.xml like a normal publication would, allowing more applications on the development compile like creating test packages for XIS
    * Fixed closed logic bug when no child element come from an include/contains (e.g. just attributes.
    * Fixed a rare problem when a path contains a \ not followed by \ or $. This leads to an error in the multipleReplace template
* DECOR2html
    * Moved templateAssociation rendering for elements to directly underneath the element so you cannot confuse those with concepts associated with attributes of that same concept
    * Updated name generation to make use of doDescription which gets alternative names for the concept should the requested name not exist.
    * Fixed doDescription to select the first available name/desc if neither the requested language, or en-US is available
    * Generate correct status dots in case of template/@ref or valueSet/@ref
    * Converted every singular font-family: Verdana to font-fmaily: Verdana, Arial, sans-serif for proper browser fallback
    * Do not render any value set with more than configurable concept count fully and complete but truncate the publication rendering so that value sets do no longer exceed reasonable sizes.
    * Improved link management. Links to valuesets and templates now behave as natural as possible in publications/services/art-decor. I.e. whenever possible you do not leave the context you are in.
    * Added link to template/@id in HTML when it is a ref. Will take you to the template in ART-DECOR if available
    * Added link to template/relationship in HTML when it is a @template. Will take you to the template in the same publication if possible or to ART-DECOR if it is not. Takes @url and @ident into account
    * Fixed error on missing templates to include the notion they could be present but empty
* DECOR2wiki
    * Extending wiki extract for value sets status type pending
* DECOR.xsd
    * Updated location information in schematron for better pinpointing in the Project develop form
    * Made the nullFlavor check on vocabulary a little more intelligent. Now does not fire when attribute nullFlavor is prohibited
    * Fixed [SF Ticket #299](https://sourceforge.net/p/artdecor/tickets/299/) detecting illegal characters in attribute names
    * Added support for a new way of referencing another concept: concept containment 

2017-09-18 version 1.8.34
* Aids supporting adding only templates specified in the template stack from the toplevel template stack as explicit includes rather than adding every single in scope template for the project.
* Updated location information in schematron
* DECOR2schematron
    * Fixed problem in generating closed paths
    * Made the nullFlavor check on vocabulary a little more intelligent. Now does not fire when attribute nullFlavor is prohibited
    * Added hl7nl:TS to the supported set
    * Handle intensional value set definitions properly in SCH engine (not yet fully supported for validation)
* DECOR2html
    * Moved templateAssociation rendering for elements to directly underneath the element so you cannot confuse those with concepts associated with attributes of that same concept
    * Updated name generation to make use of doDescription which gets alternative names for the concept should the requested name not exist.
    * Fixed doDescription to select the first available name/desc if neither the requested language, or en-US is available
    * Improved datatype detection support when dealing with namespaces
    * Improved layout actor based tables in scenarios tab
    * Generate correct status dots
    * Do not render any value set with more than truncateConcpetListOuput (var) concetps fully and complete vbut truncate the publication rendition so that value sets do no longer exceed reasonable sizes.

2017-07-25 version 1.8.33
* DECOR.xsd
    * Allow ADARM conformant publication location URLs like http://hl7de.art-decor.org/index.php?prefix=pmp-
    * Corrected datatype for issue assignment to
    * Corrected datatype for project author id
    * Corrected OID pattern
    * Changed definitions for intentional value sets
    * Deactivated schematron on duplicate conceptList/@id on compiled projects
    * Added schematron checks on BBR templates. These should always have element/@ids and attribute/@ids to avoid inability to edit derived templates
    * Fixed datatype handling for both inside ART-DECOR as well as outside. Now correctly reports unsupported template formats and element|attribute datatypes within a supported format.
    * Updated decor/@compilationDate to include 'development' as valid value as documented.
    * Updated decor/versionDate to include 'development' as valid value as documented.
    * Warning against inactive datasets
    * No longer warn for each concept not being active when the dataset itself is not
    * Added project/version/@statusCode since ART-DECOR creating it anyway
* Datatypes
    * Added IPS datatypes: AD.IPS, CD.IPS, CE.IPS, CV.IPS, IVL_TS.IPS.TZ, TEL.IPS, TS.IPS.TZ
    * Improvements and fixes in coreschematrons and unit tests
    * EN: More targeted error for relationship between @nullFlavor, * and text()
* DECOR2html
    * Fixes a problem in rendering id / effectiveDate for transactions
    * labelize himbeereis: a new template caa to doLabelMiracle has been implemented for template <item> labels
    * New icon in assets: questionmark.png
    * Truncate long labels as 4...4 instead of 9... chars
    * Correction for exception rendering
    * Support for rendering intentional value sets
    * Adds the statusCode and a link to compilation errors for missing or empty templates if possible
* DECOR2schematron
    * Protection against single quotes in vocabulary @displayName, @codeSystemName and @codeSystemVersion
    * Updated to support all namespaces on any template as indeed one template is not enough especially when all includes regardless of use are present in the top level schematron
    * Template API now generates the namespace nodes from the original context of the template, not the calling context (template/@ref) of the template
    * Schematron and fragment generator now generate top level schematrons that include the namespace declarations from the template hanging off the representingTemplate instead of just it's own. That may still not be good enough. Might need to globally harvest of every template because it might in turn point to something that defines yet another namespace...
    * Now the compile process creates instance2schematron.xml like a normal publication would, allowing more applications on the development compile like creating test packages for XIS
    * Fixed closed logic bug when no child element come from an include/contains (e.g. just attributes.
    * Fixed a rare problem when a path contains a \ not followed by \ or $. This leads to an error in the multipleReplace template
* DECOR2wiki
    * Extending wiki extract for value sets status type pending
* Template2hgraph
    * Added necessary code to support hiergraphs
    * Update on hiergraphs for templates that handles multi type templates and recursive definitions properly
    * Correct handling for new icons in wiki rendering

2017-05-30 version 1.8.32
* Added CD.SDTC as supported datatype that includes @sdtc:valueSet and @sdtc:valueSetVersion

2017-05-13 version 1.8.31
* DECOR.xsd
    * Fixed transaction checks. most were not called due to overlapping contexts
    * Made version attributes on transaction required instead of optional
    * Added check on issue event order (descending by date)
    * Added fully specified name as a designation type (cf with SNOMED, FHIR etc)
    * Many schematron tweaks
* DECOR2schematron
    * Changed all document() calls to doc() -- relates to https://github.com/phax/ph-schematron/issues/42
    * Now generates @xsd on mappings based on root element. You may override these to set a different schema.
* DECOR2html
    * Critical fix: datasets.html was not rendered
    * Fixed problem with circular references in the HTML rendering of includes
    * Fixed problem with missing label on type of Used By for includes
    * Added Used By links for transactions
* RetrieveTemplateDiagram 
    * Added template hierarchical graph (hgraph) function for documentation of template hierarchy (similar to svg graph function) including an extract option for the wiki

2017-04-25 version 1.8.30
* DECOR.xsd
    *Improved choice min/max checking
    *Moved a number of unique checks from schematron to schema (performance).
    *Fixed for inherited concepts checks in compilations
    *Improved include min/max/conf/mandatory checks -- now includes a check for conformance / isMandatory. If the referred template has @conformance='NP', you cannot override that with @conformance[or @isMandatory[.='true'](not(.='NP')]). Likewise you cannot override @isMandatory='true' in the referred template with @conformance[= ('C', 'NP')](.) or @isMandatory='false'
    *Improved min/isMandatory checks when conformance='NP'
    *Added compilation attribute dataset/@iddisplay
* DECOR2html
    *Fixed valueset footers links to other format
    *Now creates folding for large (>200 characters) template descriptions leaving more room for viewing contents in forms such as template-associations
* DECOR2svg
    *Implemented fallback to the first name in all places where there is no defaultLanguage name
* Added localization to decor-parameter.xsd documentation (at least the ones visible in the project form dialog)

2017-04-02 version 1.8.29
* DECOR_Tr1.xsl: Added ANY support and fixed AD, AD.NL and IVL support
* DECOR.xsd: Corrected a number of schematron checks for versioned datasets/concepts
* Minor preparations for FHIR enabled projects

2017-03-26 version 1.8.28
* Changed license, see also https://art-decor.org/mediawiki/index.php?title=Overview_Licenses
* DECOR.xsd
    * Fixed schematron warning for deprecated/cancelled concepts in transactions
    * Added warning against duplicate baseId/@prefix values
    * Fixes a lingering problem with the context/@path definition. / and // are not valid xs:anyURI
    * Introducing purpose and copyright for templates and valueSets to fit Normative Version of  HL7 Templates Standard: Specification and Use of Reusable Information Constraint Templates, Release 1
    * Fix for compilation attributes unjustified errors.
    * Added support for language code without country/region on valueSet/conceptList/*/designation/@language. There and only there. This allows the import of additional SNOMED CT designations that do not have country/region codes by default.
    * Give a warning on context-less template definitions
    * Fix for SF#292: when a top level template (e.g. ClinicalDocument) does not have a template/content defined, assume path '//'
    * Relax error message for reference URL if there is no url
* DECOR2schematron
    * Now supports partial publications
    * Implemented support for templateId[which is the new thing for C-CDA-. If @extension is a required attribute then it will be used for validation
    * Updated support for mismatches between template/context/@id and the requested templateId(s). If a mismatch is found, then this is reported. For validation the actual templateId elements are used regardless of the template/@id
    * updated the whole stack for template recursion resilience. Distinguishing between nesting level and recursion level, handle that properly; max nesting is now 30 and max recursion is now 3.
    * Updated instance2schematron.xml logic. Now it doesn't just trust that the template called from representingTemplate actually is a template that defines a templateId, but instead ventures into it to find out. No also supports templateId[@root](@root][@extension])[constructs and even ventures up to 1 layer into the toplevel template in case of a choice and gets root elements and templateId from there. 
    * Updated instance2schematron.xml with version decription or release note to the manifest
    * Fixed a problem with defaultInstances when there is no dataset
    * Fixed a problem with defaultInstances for the link to the rules tab
* DECOR2html
    * Adds hints to the project properties just like in ART
    * Updated layout of the project tab to align better with ART and to add ProjectIndex to the list of properties
* coreschematrons
    * Added xs prefix declaration to coreschematron that use it
    * Updated RTO_PQ_PQ to support nullFlavor on denominator XOR nominator

2017-01-10 version 1.8.27
* Added support for partial publications using ADRAM. You may now explicitly publish one more more transactions for example. Associated datasets, templates and value sets are published automatically too
* Improved rendering of references to repository artifacts
* Now uses treetable rendering by default
* Various PDF publishing and printing improvements
* Introducing new parameter switchCreateSchematronClosedString; corrections on development xquery, closed accepted by Live Runtime Compile
* Cardinality check improvements
* Better layout for check decor
* adding new layout of validation ouput to Life Runtime Compile LRC in XIS
* adding new icons
* adding at least on @see to datatype core schematrons
* Add JSON, SQL and SVS links for ValueSets in HTML
* DECOR.xsd / DECOR-datatypes.xsd
    * Adjusted copyright years to support "20nn-" to mean "20nn to present". This saves the quite boring hassle of updating the copyrights year after year
    * Changed restriction type from xs:token to xs:string for MimeType, Oid, Uuid, OidChoiceList, NonEmptyString, CollapsedPackageId. With xs:token leading and trailing spaces are allowed which is unintended and leaves many typos hard to detect. This may lead to seemingly new errors in your project

2016-11-10 version 1.8.26
* DECOR.xsd
    * Adds ability to specify a type on project copyright holders. Default is 'author'
* Publications
    * Added versionLabel default to view on scenarios tab while hiding by default the column for model
    * Added dataset/@versionLabel to the view where relevant
    * Fixed initialization of collapsed contents
    * XHTML compliancy improvements

2016-10-10 version 1.8.25
* DECOR2schematron
    * Now includes closed logic for nested includes and choices
* DECOR2html
    * Applied foldable behavior to terminology tab
    * Terminology tab no longer renders as 1 huge page, but now is an index comparable to template with links to each valueSet
    * Applied foldable behavior to valueSets rendering
    * Aligned template and valueSet metadata rendering
* DECOR.xsd
    * buildingBlockRepository/@format now has proper values (default was and is 'decor')
    * buildingBlockRepository/@ident is now only required when format is 'decor'
    * valueSet/conceptList/concept and valueSet/conceptList/exception now support designation elements just like ids/id does
    * issue/tracking and issue/assignment now support multiple author elements and each author element may now carry @effectiveDate so you can track when an author changed something
    * Various enumeration values now have xforms labels

2016-07-11 version 1.8.24
* Added Dutch import of DTr2 PIVL_TS for frequency in dosage instructions

2016-06-26 version 1.8.23
* Fixed Content-Type encoding UTF-8 as utf-8
* Fixed a problem in generating schematron for datatypes in a different namespace
* Fixed display bug in processing attributes that appear as top level in a template
* Fixed a problem that would cause RetrieveTemplate to fail loading templates when collapsible=true on certain servers
* Added @language to the list of attributes to consider for merging of templates
* Added icons to assets
* Fixes a rendering problem where RetrieveTemplate would display duplicate attributes, constraints, and properties
* Corrected link for vocabulary href rendering in templates
* Show at least ids of non retrievable value sets

2016-06-23 version 1.8.22
* Fixed a problem that would cause RetrieveTemplate to fail loading templates when collapsible=true on certain servers

2016-06-17 version 1.8.21
* Fixed a problem where value sets where not linked in template HTML when called from ART

2016-06-14 version 1.8.20
* Major overhaul to support context switching between calls from ART and publications in the schematron and HTML engines
* New: datatype flavor BXIT_IVL_PQ
* New: datatype flavor TS.CH.TZ
* New: implemented valueSet/conceptList/(concept|exception)/@ordinal to support structured recording of ordinal values in for example assessment scales. HL7 Datatypes Release 1 does not support transmission. HL7 Datatypes Release 2 does. Whether or not HL7 supports it is less relevant than being able to record that information as meta. The alternative to date was to bury that information in the concept|exception description
* New: added support for multilingual condition elements in a transaction. Both text nodes and desc nodes are processed. Saving a transaction updates to new format
* New: SVG creation now supports stationary transactions
* Improved: HTML localizable title on status images
* Improved: HTML localizable title on transaction direction images
* Improved: nullFlavor handling in HTML views and schematron. Schematron will now accept any nullFlavor if valueSet doesn't define any and conformance is not mandatory, e.g. required/optional. If however the valueSet defines nullFlavor it these considered are the only valid nullFlavor, again unless mandatory. If mandatory no nullFlavors are allowed. Before all nullFlavors were accepted when not mandatory.
* Improved: SVG creation now more accurate finds Actor box width
* Under-the-hood: 
    * Consolidated filename creation for publication files
    * Consolidated link creation into 1 function that supports context switching (ART, publication)
    * Consolidated min/max/conformance determination for all places where applicable 
    * Performance updates in HTML creation
    * Improved label creation based on DECOR schema
* Update: jQuery treatable update from 3.0.1 to 3.2.0

2016-04-30 version 1.8.3
* Circumvented a problem when someone uses the default schematron.com SVRL/Text implementations. These contain a bug when processing a <name path="..."/> construct and thus will fail to process closed logic schematrons that use this. This is now circumvented by rewriting the statement to the equivalent <value-of select="name(..)"/>. The issue was brought forward through Tony Schaller related to [https://sourceforge.net/p/ehealthconnector/ eHealth Connector](@extension])

2016-04-10 version 1.8.2
* Coreschematrons: enhanced IVL* nullFlavor checking
* Extended ADA options to include overriding configured valueDomain/@type
* New parameters for DECOR2schematron
    * switchCreateTreeTableHtml - default 'false'. If 'true' enables foldable templates
    * Script changes, additions and adaptations in order to serve well for Instance Fragment Validation IFV and Live Runtime Compile LRC
    ** outputBaseUriPrefix - base output prefix if any, must end on "/" or empty on "relative" outputs
    ** scriptBaseUriPrefix - base uri to script (xsl) if any, must end on "/" or empty on "automatic" uri to scripts
* Schematron now includes new phases for more fine grained control. Includes separate phases for closed logic checking
* Template2html various xhtml fixes
* Various styling updates to support new features and tweaks to others.

2016-03-26 version 1.8.1
* DECOR.xsd added support dataset valueDomain concepts level and type
* Supported datatypes: added support for PN.NL
* Added support for folding in templates

2016-02-12 version 1.8.0
* Fixed a bug where IVL/width was declared with another datatype than the correct PQ datatype
* Publications/schematron engine
    * Improved namespace handling
    * Improved predicate generation for path without a templateId
    * Introduced parameter to do server call for dynamic value set bindings instead of a frozen local copy "bindingBehavior@valueSets='preserve'"
    * Introduced parameter for PDF generation "switchCreateDocPDF"
    * Improved performance of publications by rewriting the id generation
    * Improved closed path checking by not checking path //
    * Improved transaction group SVG generation by being more resilient against various combinations of senders/receivers

2015-08-27 version 1.6.0
* Administrative rename for this release

* DECOR2schematron
    * Major update for versioned datasets and dataset concepts
    * Tweaks in dataset overview view
    * Improved SVG / Wiki output
    * Improved schematron engine in case of circular references
    * Numerous smaller improvements
    * DTr1_CD: Disabled RIM-001 check from IHE as it was too broad for all use cases
    * DTr1_URL.NL.EXTENDED: Allow for x-hl7-applicatie with >= 1 digit instead of >1
    * Restyled zoomin/zoomout images

* DECORmycommunity.xsd
    * Allows common practise of HTML markup in association definitions
    * Allows a flexibility on referenced concepts (versioning reasons)

* DECOR.xsd
    * Implemented some SQF fixes for common problem
    * Relaxed schematron in a number of cases to allow focus on what really important. E.g. do not check under cancelled|deprecated items
    * Added all elements and attributes that are required to validate compiled DECOR files
    * Added dataset and dataset concept relationships and property elements
    * Added preparation for @effectiveDate versioned datasets and dataset concept (was: @id based)

2015-07-28 version 1.4.2
* See version 1.6.0

2015-04-04 version 1.4.1
* Copyright 2015. Either updated the year only or the full statement.
* DECOR2schematron
    * controlAct-wrapper.NL: Updated schematron error/warning text at request of VZVZ
    * DTr1_URL: Commented out the distinct check on telecom/@use which is simply a false assertion
    * Add HTML element/@strength support

2015-03-29 version 1.4.0
* Schematron engine enhancements
    * Enhanced attribute support to include rid and uid (oid, uuid, ruid, guid)
    * Enhanced support for template top level attribute, let, defineVariable, assert, report
    * Enhanced support for nested includes
* HL7 V2.5.xml support completed

2015-01-26 version 1.2.10
* Templates: Fixed display of RetrieveTemplate service
* HTML: replaced locally downloaded RetrieveTransaction with link

2015-01-18 version 1.2.9
* Fixed namespace problem preventing renderings of RetrieveTemplate service

2015-01-06 version 1.2.8
* coreschematron ANY: Allow datatype ANY with nullFlavor OTH and (new: additionally) NA to have originalText elements as a child
* DECOR.xsd/DECOR2html/DECOR2wiki: Now adds support for governance groups
* Changed default publishing format from HTML 4.0 to XHTML 1.1
* Various improvements in links in publication

2014-12-01 version 1.2.7
* DECOR.xsd schematron fixes for template associations on referenced templates.
* DECOR2html/Template2html: Fixed small problems in Template metadata display in html

2014-11-23 version 1.2.6
* DECOR.xsd schematron fixes for template associations on referenced templates.
* Added schematron check for template relationship @model + @type='DRIV'
* Fixed schematron when checking required attributes. These checks were not generated.
* Changed check for required attributes. No longer checks length >0. This duplicates the check based on datatype which is a waste of resources.
* Fixed somewhat misleading error text when a code doesn't match the defined value
* Fixed error in schematron creation for attributes of type 'ts'
* Updated Dutch translations for conformance Required and Mandatory
* Improved issues.html so links to details are more obvious
* Fixed bug in schematron engine that generated illegal expressions for checking datatypes oid and uuid
* Updated for German in DECOR.xsd and i18n file for HTML and schematron
* Improved rendering of examples in DECOR2html
* Improved rendering of contained template cardinality in DECOR2html
* HTML/Schematron predication improved for templates with >1 templateId. No longer assumes " or ", but now assumes " and ". Note that only templateId elements with @minimumMultiplicity > 0 were/are considered

2014-11-18 version 1.2.5
* Added schematron check for template relationship @model + @type='DRIV'
* Fixed schematron when checking required attributes. These checks were not generated.

2014-11-12 version 1.2.4
* Changed check for required attributes. No longer checks length >0. This duplicates the check based on datatype which is a waste of resources.
* Fixed somewhat misleading error text when a code doesn't match the defined value
* Fixed error in schematron creation for attributes of type 'ts'

2014-11-10 version 1.2.3
* Updated Dutch translations for conformance Required and Mandatory

2014-11-09 version 1.2.2
* Improved issues.html so links to details are more obvious

2014-11-08 version 1.2.1
* Fixed bug in schematron engine that generated illegal expressions for checking datatypes oid and uuid

2014-11-04 version 1.2.0
* DECOR definition updated
    * @code may now be 128 characters, was 64 but proved too small for SNOMED-CT expressions
    * @codeSystem may no longer be 'nullFlavor' but shall be OID
    * Templates DSTU update: statuses 'inactive' and 'update' are no longer supported
    * various schematron rules have been relaxed to only fire on places that are draft/new you may actually still fix them
* Schematron and HTML publication engine improvements for referenced templates, prohibited attributes, and codeSystemName
* Language updates for HTML / Schematron

2014-09-22 version 1.0.5
* Fixes a bug that prevented templateAssociations to be displayed in HTML publications

## DECOR example files
DECOR example files

2020-06-06 version 2.0.2
* Adjusted baseIds and defaultBaseIds according to recommendation [
2020-05-06 version 2.0.1
* Connect to DECOR.sch, not DECOR.xsd for schematron
* Add baseId and defaultBaseId for type MP
* Remove history in datasets
* Update name based valueSet and template references
* Add baseId/defaultBaseId MP

2018-11-05 version 2.0.0
* Added example for FHIR restURI and buildingBlockRepository in demo1-

2017-01-10 version 1.8.22
* A few corrections for our demo1
* Corrected example demo3 due to IHE Gazelle ObjectsChecker Base Standards Validation

2016-12-14 version 1.8.21
* Use new ART-DECOR logo for demos and sandboxes

2016-06-20 version 1.8.20
* Mostly administrative update to 1.8.20

2016-02-12 version 1.8.0
* Updated copyrights to 2016

2015-08-27 version 1.6.0
* Administrative rename for this release

* Added governance group link to "ART-DECOR"
* Fixed OIDs in demo1-

2015-03-29 version 1.4.0
* Activated the new @experimental flag

2014-11-04 version 1.2.0
* Administrative rename

2014-10-29 version 1.0.7
* Fixed missing conceptLists in the demo5 dataset

2014-10-10 version 1.0.6
* Fixed a problem in publishing demo5 (missing restURIs)
* Added missing name in en-US for the dataset in demo5
* Added missing terminology association in demo1
* Fixed English displayName on concept in demo1

## DECOR services
REST services for DECOR

2021-04-02 version 2.0.14
* OIDIndex
    * Allow search for OID with FHIR URI. Improve table view 
* ProjectIndex
    * Add FHIR link to CodeSystem if not FHIR 1.0 or 3.0
* Questionnaire
    * Fix in version retrieval
    * Rewritten for pure xquery, no xsl. This allows for access to xquery functions like getting a canonical for a uri.
    * Fixed minInclude and minLength 
* RetrieveCodeSystem
    * Add codedConcept/@statusCode support in CSV
    * Add support for codeSystem.conceptList.codedConcept effectiveDate and expirationDate
* RetrieveConceptDiagram
    * Introducing option "interactive" with default true to allow non-interaqctive extracts into a wiki or other cms where onClick isn't allowed
* RetrieveArtefacts4Wiki
    * Introduced hashOnly option for wiki services to better guess the need for refresh on wiki environments
* RetrieveOID
    * Improved rendering of references from OIDs
    * Improved rendering and FHIR integration
* RetrieveProject
    * Replace DECOR.xsd for schematron with DECOR.sch
* RetrieveTemplateDiagram
    * Added more documentation on extra parameters and output format, copied that to the wiki
* RetrieveTransaction
    * Fixed downloads for non-Safari browsers
    * Add support for deprecation of completeCodeSystem and refsets
* Transaction 2 SVG
    * Don't filter inactive transactions if group is inactive
* Miscellaneous
    * Replace custom json generation with fn:serialize()

2020-06-15 version 2.0.13
* RetrieveCode
    * Add links to results in RetrieveCode back into the relevant terminology/project parts. Add entry interface too
* [[FHIR_Questionnaire_support|RetrieveQuestionnaire]([DECOR-ids]])] (new)
    * Support translating facets to Questionnaire, 'Loading' indicator, some fixes, hook for other renderers
* RetrieveTransactionDiagram
    * Better identifiable IDs in SVG to better be able to canonicalize SVG images

2020-06-06 version 2.0.10
* RetrieveCode
    * Output alignment with TerminologyReport
* RetrieveQuestionnaireXml/Json
    * Basic HL7 FHIR Questionnaire functionality for transactions (experimental - to be moved to the FHIR servers soon)
* RetrieveTransaction
    * Fixed bug, json from /releases would not work since eXist passed a node pointer which made preceding-sibling true in cases.
* TerminologyReport
    * Implemented LOINC match on ShortName
    * Fixed bug in processing multiple hits

2020-05-07 version 2.0.9
* RetrieveCode
    * Now supports showing the origin project
* TerminologyReport
    * Skip LOINC Answer codes until we actually can 'see' them
    * Support indicator for translation availability in SNOMED CT
    * Improved terminology association reporting
* Transaction group SVG rendering
    * Fixed a problem for rendering transactions with multiple stationary actors
    * Fixed a problem for rendering inactive transactions. These are now skipped.
* All services
    * Use jQuery 1.11.3 from local assets folder on request

2020-04-17 version 2.0.8
* IssueIndex
    * Add support for searching issues by last modified date
* RetrieveProject
    * Fix intended behavior of mode=test which is to return the filters
* TerminologyReport
    * Performance: call functions directly without http
    * Usability: only display things with issues
    * Usability: improved feedback when concept not found
    * Usability: improved feedback when concept has been replaced by other concept(s) - only available for SNOMED CT and LOINC because they provide that content
    * Add type of concept for terminology assocaitions
    * Fixed a bug in claml processing
    * Add | ref | box when relevant for concept not from current project
    * Always display codesystem OID. If possible with name title
    * Added option to check SNOMED codes whether they are part of SNOMED International's Global Patient Set GPS
* General
    * Fixed behavior for server logo if that is an absolute path

2020-04-11 version 2.0.7
* ProjectIndex
    * Improved initial loading times: no longer load all artefacts from repositories when called for multiple (or no) prefixes
* RetrieveTransaction
    * Add concept counters to header of page
    * Support for new valueDomain/@type 'time'
    * Fixed use of ui param (language)
    * Add button for downloading table as Excel
    * Add help text for Download and Excel buttons to explain what they do
    * Fix concepts counter at the top for concepts in conceptLists
    * Improved rendering of Codes columns contents
    * Improved formatting of Terminology column according to order in concept
    * Improved formatting of Terminology column by applying indenting and formatting based on @level|@type
* RetrieveValueSet
    * IHE SVS export correct language handling

2019-09-29 version 2.0.6
* ProjectIndex
    * Add links to installed FHIR server versions that are called out in the project (restURI[= 'FHIR'](@for))
    * Merged XML | JSON columns into 1 DECOR column containing both xml and json links
* RetrieveTransction
    * Improved community handling
    * Add support for display of binding strength in the Value Set column

2019-07-18 version 2.0.5
* ProjectIndex
    * Fixed an issue that caused referred datasets from transactions not to be marked [bbr](ref|)
* RetrieveProject
    * Implemented new value 'xpaths' for parameter mode to support retrieval of transaction-xpath files (from compilations only)
    * Performance tweaks
* RetrieveTransaction
    * Performance tweaks for datatype labels
    * Performance tweaks for code displayNames
    * Now only does community if not already there (new in ART compiles)

2019-05-30 version 2.0.4
* IssueIndex
    * Fixed searching by label, status and type
    * Fixed label order
    * Fixed Dutch translation for priority
* RetrieveTemplateDiagram
    * Performance
* TerminologyReport
    * Performance
    * Add links to ART terminology/valuesets
    * Improved SNOMED CT reporting
    * Fixed issue in reporting false negatives in value sets
    * Fixed issue where column heading would not surface on value sets
    * Fixed issue where the originalDisplayName would not surface
    * Fixed issue where assocations on concept/contains would not surface properly
    * Fixed paths for resources in download

2019-03-12 version 2.0.3
* ProjectIndex
    * Fixed retrieval of compiled release files
    * For compiled projects add references for valuesets that were compiled in, but do not have a project reference.
    * Various fixed for compiled projects:
    ** All value sets are now displayed regardless of origin. This matches template behavior
    ** ref boxes now match the rest of ART repobox style
    ** Datasets can now behave as ref
    ** Datasets in compiled projects are now properly retrieved from compiled result so status codes etc. match what was published
    ** Counters match @id based
    ** Now displays first lange for objects if the language selector doesn't match any (like with *)
* RetrieveTransaction
    * Render deprecated codes properly
    * Display ? for empty minmum/maximum multiplicity (unfinished transactions)

2019-01-09 version 2.0.2
* RetrieveProject
    * Better tmp name so you can recognize at a glance which is the latest for a given project
    * New: exposure of the existing ignoreFilter option that allows by passing any filters.
    * Moved function for writing a compiled file into decor/tmp from RetrieveProject to compilation library/api -- need ART 2.0.3 or up
* RetrieveTransaction
    * Show transaction id if a transaction is rendered (was always dataset id before)
* RetrieveValueSet
    * Fixed typo in IHE SVS export
    * Language tweaks for SVS export format
* Generic
    * Performance tweaks

2018-11-14 version 2.0.1
* General
    * Technical fixes that remove the dependency on the FHIR servers being installed

2018-11-05 version 2.0.0
* ProjectIndex and RetrieveTransaction
    * "Latest version" is now "Live version" to better reflect what it is
    * Implements proper CSS classes for concept @type/@level
* RetrieveValueSet and RetrieveOID
    * Now connect to the FHIR servers for ValueSet and NamingSystem respectively
* RetrieveStructureDefinition
    * New service for rendering StructureDefinitions and mappings to them
* GovernanceGroupList
    * Added UI for selecting the governance group from a drop down if none is provided
* Statistics
    * zebra-table styling instead inline styling
    * more localization

2018-10-02 version 1.8.38
* RetrieveProject
    * Fixed a problem where retrieving a compilation a second time would include a <compile/> element

2018-09-29 version 1.8.37
* Rewrote CodeSystemIndex to be part of ProjectIndex, like ValueSetIndex, DataSetIndex and TransactionIndex
* Rewrote RetrieveCodeSystem as copy of RetrieveValueSet supporting the same export methods except for IHE SVS
* RetrieveProject is now more reliable if errors should occur

2018-08-23 version 1.8.36
* Dependency on ART 1.8.62 because of RetrieveProject
* GovernanceGroupList
    * More stability for getting governance group lists
* ProjectIndex
    * Fixed issue with absolute URLs for logos
    * Added json serialization link to datasets and transactions
    * Circumvent NullPointerException with new range (combined) indexes
    * A little resilience against @versionDate not being castable to xs:dateTime
    * Now, unless a specific version is requested, the initial selection always goes to the latest release in the requested language if available, otherwise the latest release, or there are no releases to the live version.
    * Updated TemplateIndex so you now see where compiled templates came from
* RetrieveArtifacts4Wiki
    * Add template associations to wiki extract
    * Alternative processing of wiki extract for governance groups
    * Re-write get template for wiki export
    * Serialize correctly handled on wiki extracts
* RetrieveProject
    * Use tmp directory to write compilation result in before trying return to user who may have experienced a timeout. Subsequent calls will detect if the previous call did not finish yet and will start polling every 30 seconds until it is, or happily return the previous result if the project itself did not change since compile, or start compiling otherwise. There's an option to force compilation, e.g. because a connected project was changed rather than the project itself
    * Performance updates
* RetrieveTransaction
    * Add link to documentation from valueDomain/@type
    * Added json serialization link to datasets and transactions
* TerminologyReport
    * Path corrections when you download
    * Better checks on preferred names for SNOMED CT. Now is language refset aware for Dutch
* General
    * Update to new-range indexes

2018-03-27 version 1.8.35
* New: GovernanceGroupList
    * New: governance groups artefacts compiled into one view
* ProjectIndex
    * Performance updates
    * Fix default server logo
    * New: value set index by codeSystem
* RetrieveProject
    * Fix CSS when download=true
* RetrieveTransaction
    * Slightly smaller @displayName patch. Don't hardcode NullFlavor as that will live in the valueSets as well (supposedly)
    * Improved performance, avoiding expensive getDisplayNameFromCodesystem hits when not needed. 
* RetrieveArtefacts4Wiki
    * Get templates 4 wiki through API for proper wiki display

2018-01-12 version 1.8.34
* OIDIndex
    * Fixed a problem where more than 1 search term would yield an error
* RetrieveArtefactForWiki
    * Better logic for experimental projects
* RetrieveTransaction
    * MS word layout transformation as MS word is a slacker on html
    * Extended RetrieveTransaction/Dataset to show mappings into templates (and profiles later)
    * Updated query parameters from doSerialized to serialized, from doRecursive to recursive, and from doSelectedOnly to selectedOnly
* RetrieveValueSet
    * Extending IHE SVS implementation to support ITI-60 like extracts
    * Expanded IHE SVS output format, corrected namespaces
* Template2Example
    * Fully renewed template example generation. Now allows for fully recursive and partial (from element[onwards) example generation regardless of whether the context is an in memory template (Temple / Template-editor) or any template based on @prefix/@id/@effectiveDate. At present Temple will only support examples for the current template.

2017-11-19 version 1.8.33
* ProjectIndex
    * Fixed a problem in switching projects when the current project has no versions yet
* RetrieveConceptDiagram
    * Transaction/dataset retrieval now is language aware
* RetrieveTransaction
    * Include dataset/transaction @versionLabel in title for better recognition
    * Fixed a problem where multiple FSNs exist for SNOMED CT (multilingual)
    * Made retrieval of displayNames language aware for SNOMED CT/LOINC/ClaML
    * Don't do template chain when we're not using it (html only). Huge performance boost for e.g. format xml
    * Introduce hidecolumns (b only for now) for hlist of datasets and sceanrios, defaults to hidecolumns=b
    * Added links to RetrieveTemplate from the mapping column
* RetrieveValueSet
    * Fixed a problem with codes and codeSystems for designations on exceptions
    * Fixed a problem in exporting the designations in CSV
    * Performance update
* Template2Example
    * Updated datasets for large sets of datasets. Now allows for issues at dataset level. Dataset version now comes into view consistent with all other places where this is relevant. Because issues can now be recorded in two places, this has moved into a dialog (technical reason: the contents of the reusable fragment contain an @id, so the resuable fragment can only be called once per form). 

2017-10-01 version 1.8.32
* All font-family Verdana now has more fallback to Arial and sans-serif
* RetrieveTransaction now supports concept[contains](@id])

2017-09-26 version 1.8.31
* ProjectIndex
    * Now supports hierarchical graph link for Templates just like ART does
    * Separated project language and ui language. UI language can now be set using a new parameter ui=X and defaults to the default server language
    * Fixed the version selector when multiple languages are available
* RetrieveTransaction
    * Various patches for supporting language=*, will default to project/@defaultLanguage when format = HTML
    * Separated UI language into new parameter ui=<lang>
    * Fix for multi valuedomain situations in list mode
    * Performance tweaks
* RetrieveTemplate
    * Properly initialize collapsed. Variable is boolean, not a string.
* RetrieveProject
    * Updated to support retrieval without filtering by language.
    * Updated so all other options are preserved when reloading the page based on a change in one of them
* RetrieveTemplateDiagram
    * More options for hiergraph from templates
* RetrieveValueSet
    * Multilingual value set export for SVS format
* RetrieveArtefacts4Wiki
    * Added necessary code to support hierarchical graphs
* Always include artifacts from idents of the own governance group

2017-05-12 version 1.8.30
* ProjectIndex
    * Now lists language in the list of project versions
* RetrieveTemplateDiagram
    * Added template hierarchical graph (hgraph) function for documentation of template hierarchy (similar to svg graph function) including an extract option for the wiki
    * Added format hlist as input for hierarchical graphical view on (CDA document level) templates
* RetrieveTransaction
    * Better layout for Retrieve Transaction/Dataset for hierarchical lists
    * Updated layout for simple lists
    * Added ID/versionLabel to title
    * Updated layout for simple lists
    * Updated layout for multiple value sets
    * Refixed selector behavior for hidden columns
    * Refixed community behavior
    * Fixed column selector behavior
    * Fixes for multiple community associations on same concept
    * Fixed inability to get community info
* RetrieveValueSet
    * Improved CSV export by exporting an extra line with the ValueSet vitals (displayName, id, effectiveDate) just above the column names so multiple value set exports are actually useful. Before you would have the column header once and all concepts would loose which value set they belonged to.
    * Send in decor like content including decor attributes and decor/project so DECOR2html can determine the right configuration attributes. E.g. for determining if this project is FHIR enabled
* RetrieveArtefacts4Wiki
    * Condensed wiki export format
    * Better handling of empty selection of decor to wiki
* SVG
    * Implemented fallback to the first name in all places where there is no defaultLanguage name

2017-02-02 version 1.8.29
* RetrieveValueSet now uses the stable transforms instead of the nightly build transforms

2017-01-10 version 1.8.28
* RetrieveTransaction
    * Patch in templ:getTemplateChain so it only returns new templates in the chain -- this severely impacts response times for compilation of larger projects
    * Patch in compile for inadvertent leading/trailing spaces in OIDs which was possible due to a problem in the DECOR.xsd definition. Now removed those spaces before trying to find a name
    * Now supports a download button
    * Fixed text display for transaction conditions
* RetrieveTransactionGroupDiagram (was: GetImage)
    * Renamed to RetrieveTransactionGroupDiagram to better reflect its purpose. Note that calls to GetImage will continue to work
    * Now supports statusCode based filtering
* RetrieveValueSet
    * Now supports necessary tweaks to render value sets for ART and has aligned metadata rendering with templates.
    * Added support for designations on concepts and exceptions in valueSets
    * Fixed download extension for json. (was .sql)
    * Extended support in json to include desc, completeCodeSystem, concept/desc and exception
    * Added FHIR ValueSet id, expansion.identifier
    * Added JSON, SQL, SQL, IHE SVS, FHIR XML (STU2) to the links in the header of HTML view.

2016-11-10 version 1.8.27
* RetrieveCode
    * Now checks codesystems as well as valuesets and provides more feedback
* OIDIndex
    * Fixed search behavior, especially by ID
* RetrieveOID
    * Added legend lines for abbreviations in use in the OID registry
* RetrieveTransaction
    * Rearrange of mapping column and hide by default
    * Improved rendering of multiple valueSet bindings
    * Reactivated explicit indents on download for Excel import
    * Fixed ProjectLogo service link for downloads
    * Performance bumps
* RetrieveTemplate
    * Switched the default value for html param collapsable to true()
    * RetrieveTemplate retrieve templates in the requested language if the requested language is part of the configured set in the project. Use project defaultLanguage otherwise.
* Statistics
    * Added links and special list filters for statistics
    * Added sort by id
* [(new)
    * Takes valueSet id/effectiveDate and code/codeSystem and returns concept/exception/completeSystem that matches. No return means does not match, if exception is returned when you expected concept or the other way around you might have an error to deal with.
* [[URIs#IssueIndex|IssueIndex]([URIs#ValidateCode|ValidateCode]])] (new)
    * Let's you search and list issues just like ART does. Goes back to ART for display, but allows XML retrieval too.
* Various
    * Fixed certain i18n strings

2016-09-04 version 1.8.26
* Fix for drawing stationary transactions on top on eachother when they occur in the same transaction group
* CompareArtefacts
    * Ignore development signatures in release tree if determining versions
* ProjectIndex/DataSetIndex/TransactionIndex now support a diagram column for live and releases
* RetrieveConcept
    * New service that basically leverages RetrieveTransaction to retrieve a dataset or transaction trimmed down to an arbitrary concept level. See [* RetrieveConceptDiagram 
    * now supports releases through the version parameter
* RetrieveOID
* Changed NamingSystem id algorithm for better consistency with ValueSet upon FHIR retrieval
* RetrieveProject
    * Adds language to filename comparable to regular result after release
* RetrieveTemplate
    * Sort OIDs in used/used by list
* RetrieveTemplateDiagram
    * Fix for recursive templates. Now diplays the first of a recursion tree in darker pink. Could be improved upon but at least doesn't crash
* RetrieveTransaction: 
    * Fixed order of Mapping column contents

2016-07-14 version 1.8.25
* Fixes a problem with paths to ProjectLogo, Template2StructureDefinition, and Statistics
* Fixes for projects in non-core-ART-languages. Aligned publication engine and ART for i18n. Now both retrieve exact language if possible, main language (without region) if not, defaultLanguage if given and final fallback to en-US if all else fails.
* RetrieveValueSet
    * Fixed download extension for json. (was .sql)
    * Extended support in json to include desc, completeCodeSystem, concept/desc and exception
    * Added FHIR XML (STU2) support as export format
    * Added JSON, SQL, SQL, IHE SVS, FHIR XML (STU2) to the links in the header of HTML view.

2016-06-27 version 1.8.24
* RetrieveArtefact4Wiki service: xquery memory limit set to 3000000 for large governance groups
* RetrieveTemplate: fix for non-rendering templates. This turned out to be a language issue when the calling language is not in the default set of 3
* RetrieveTransaction: Fixed problem in new mapping column
* RetrieveTemplateDiagram: Set default for effectiveDate 'dynamic' which gets the latest version
* RetrieveTemplateDiagram: Fixed error when there are multiple instances of the template nonetheless

2016-06-23 version 1.8.23
* Fixed a problem that would cause RetrieveTemplate to fail loading templates when collapsible=true on certain servers

2016-06-19 version 1.8.22
* Fixes a problem in getting contents when there's no template
* Fixes a path problem for a number of images on certain servers.

2016-06-17 version 1.8.21
* Fixed a bug that could occur when an HTTP redirect occurred on an external server call, e.g. In the retrieval of BBRs

2016-06-20 version 1.8.20
* ProjectLogo
    * New: Added parameter mode. See [[URIs#ProjectLogo]([URIs#RetrieveConcept|documentation]])]
* RetrieveTemplate
    * Major update to view. RetrieveTemplate, as does ART benefits from all updates in DECOR-core
* RetrieveTransaction
    * New: Adds a mapping column containing links from this transaction to templates (if any)
* RetrieveValueSet
    * New: support for ordinal valuesets
* General: improved link creation
* General: performance update through indexing
* General: consolidated almost all resources into DECOR-core package

2016-03-26 version 1.8.4
* ProjectIndex: added link to the live version from all artifacts
* RetrieveProject: improved language handling when the called language is not in the project (fall back to default language)
* RetrieveTemplate: added support for folding through the parameter doTreeTable=true (default is false)
* RetrieveTransaction: added support for download through parameter download=true (default is false)
* RetrieveTransaction: added visual disticntion between multiple valueSets in the codes columns if there are

2016-03-23 version 1.8.3
* GetImage: Display contents from inactive transactions with opacity 0.25, hence less visible

2016-03-14 version 1.8.2
* Added reports, through transform parameter, to RetrieveTransaction

2016-03-02 version 1.8.1
* Fixed a English typo relevant for RetrieveTransaction
* Fixed special case where ProjectIndex was called for a private project/repository and this was then missing from the dropdown
* RetrieveTransaction/RetrieveDataSet
    * Fixed default language when not given
    * Fixed rendering of descriptions from text to HTML

2016-02-12 version 1.8.0
* ProjectIndex
    * Experimental links to JSON/SQL representations of value sets
* RetrieveOID
    * Experimental link to retrieval as FHIR NamingSystem
* Statistics
    * New experimental service for getting some basic server statistics for DECOR projects
* Template2StructureDefinition
    * New experimental service for getting a Template ITS template, as FHIR StructureDefinition
* TerminologyReport
    * Support for multi lingual LOINC
    * Various small improvements

2016-01-05 version 1.6.7
* Performance updates in RetrieveTransaction, RetrieveValueSet, ProjectIndex
* ProjectLogo small fixes for various cases of missing logos and added support for full logo URLs

2015-12-18 version 1.6.6
* New service [to facilitate project logo retrieval
* Services ProjectIndex, RetrieveTransaction, RetrieveDataSet and RetrieveValueSet have been adjusted to call this function for the top right logo
* ProjectIndex looses two of the four columns that were essentially superfluous shortcuts to RetrieveTransaction. 
* RetrieveTransaction now has some minimal help text and sports an improved button layout
* Various performance tweaks

2015-12-15 version 1.6.5
* RetrieveTransaction no longer tries to 'calculate' how to reach the appropriate terminology service for a given code, but instead generates a fixed link to a new terminology redirection service that does the "dirty work".
* RedirectTerminology supports SNOMED CT, LOINC, ClaML and local systems. The first 3 go to the appropriate browser and preload the requested concept. The local codes are handed to RetrieveCode that has now been implemented for formats xml, csv and html

2015-11-04 version 1.6.4
* RetrieveValueSet
    * Fixed a bug when multiple valueSet names were retrieved

2015-11-03 version 1.6.3
* Preliminary checkin of new services RetrieveIssue and IssueIndex that only do XML for now

2015-10-17 version 1.6.2
* TerminologyReport
    * Fix for "exerr:ERROR err:XQDY0025: element has more than one attribute" in some projects
* GetImage
    * Fix for error when there are multiple receivers and/or senders for a given transaction

2015-09-22 version 1.6.1
* RetrieveDataSet/RetrieveTransaction
    * Fix for unfiltered so it works at all levels in the hierarchy
    * Fix for unfiltered so it never filters for transactions. Things either are or are not part of the transaction. They do not have an association status there.

2015-08-27 version 1.6.0
* All services now properly display HTML when called with https. CSS links now match protocol
* RetrieveValueSet
    * CSV no longer contains 'dynamic' flexibility for concepts and exceptions
    * All formats now have a sensible name when you need to save them as file
    * Added column Exception that contains true if line concerns an exception, and false otherwise

2015-07-26 version 1.4.2
* RetrieveTransaction
    * Performance tweak by skipping name for OID calculation when already done
    * jQuery from https instead of http

2015-06-24 version 1.4.1
* GetImage
    * Now supports parameter inline=true that makes it return plain SVG instead of HTML
    * Multiple improvements related to various combinations of multiple actors/transactions
* ProjectIndex
    * Versions of a project are now sorted in descending order
    * Datasets name sorting is fixed
    * Improved transaction list by only listing those associated with a dataset
* RetrieveTransaction
    * Adds support for value domain concept synonyms in the column Codes
    * Adds support for all value domain properties (was: unit only)
    * Improved reliability of display names for identifiers when they are not branched directly from a baseId
    * Performance improvements
* General
    * CSS updates for h1, h2, h3

2015-03-29 version 1.4.0
* All services have rewritten localization under the hood
* TerminologyReport no longer fails when terminology calls do

2015-01-26 version 1.2.6
* Templates: Fixed display bug in examples, assert, report
* Templates: Fixed display of RetrieveTemplate service
* HTML: replaced locally downloaded RetrieveTransaction with link

2015-01-13 version 1.2.5
* RetrieveTransaction fixed links to valuesets. Prefix was missing and needs full URL when downloaded locally

2015-01-06 version 1.2.4
* RetrieveTransaction: Fixed meta header for charset and media type
* RetrieveTransaction: Fixed missing expiration date in cookie
* RetrieveTemplate: Fixes, tweaks and syncs for Retrieve Template service
* Retrievetemplate: Now with format raw to get template unexpanded
* ProjectIndex: Removed Xpath intermediate format from ProjectIndex.xquery.
* Sort ProjectIndex/transactions on dataset effectiveDate.

2014-12-10 version 1.2.3
* ProjectIndex/DatSetIndex/TransactionIndex/TemplateIndex: Fixes logo behavior in cases of multiple logos, no logos, or no visible logos. depends on ART package version 1.2.14 for variable $get:strDecorServices

2014-12-01 version 1.2.2
* RetrieveTransaction: Fixed display of Codes column when derived from a valueSet. Now includes the level hierarchy and visual distinction between <span style="font-style:italic;opacity:0.5;">Abstract</span>, <span style="font-style:italic;">Specializable</span>, <span style="font-style:italic;opacity:0.5;text-decoration:line-through;">Deprecated</span> and Leaf
* All services: fixed path problem on CSS/JS/img when you retrieve for local (DECOR2schematron) but without useLocalAssets=true
* Fixed link on templates in ProjectIndex
* Fixed display of referenced templates in ProjectIndex
* Added html link on templates in ProjectIndex
* Transaction group diagram now has the model on the message arrows it displays

2014-11-05 version 1.2.1
* Added try/catch around displayName retrieval for codes. Returns no displayName in case of error. This is usually due to missing codesystems or local codesystems

2014-11-04 version 1.2.0
* RetrieveDataSet / RetrieveTransaction updates
    * includes both concept terminology and concept value terminology, improved display of value set bindings
    * allows moving columns in any particular order
    * allows client side searching where non matching contents are hidden

Version 1.0.2

Read more information, or download DECOR-services-1.0.2.xar.

## ART-DECOR System Services
ART-DECOR System Services provides resources related to ART-DECOR system services like ADANS (automatic notifier system) and ADRAM (release and release manager)

2018-11-05 version 2.0.0
* ADRAM
    * Fixed issue with l10n when project default language and user preferred language did not match
* ADRESH
    * More efficient. Prepared for FHIR BBRs

2018-08-23 version 1.8.24
* ADRAM
    * Do not return language='*'
* ADANS
    * Adds an unsubscribe link to /user-settings towards at the end of the email
    * Now tries to send the email in the preferred ART language for the user.
    * Enhanced rendering of issue labels in email
* ADRESH
    * Up the output size limit

2017-01-10 version 1.8.23
* Allow for partial publications through filters.xml for ADRAM
* Added user demo to sandbox upon refresh
* Use new ART-DECOR logo for demos and sandboxes

2016-11-30 version 1.8.22
* Refresh of cache data now uses appropriate server function for trusted server retrieval

2016-06-17 version 1.8.21
* Fixed a bug that could occur when an HTTP redirect occurred on an external server call, e.g. In the retrieval of BBRs

2016-06-14 version 1.8.20
* Improved: More robust HTTP handling in cache creation
* Improved: Results are now sorted for overview at a glance

2016-02-12 version 1.8.0
* Updated copyright to 2016

2015-08-27 version 1.6.0
* Administrative rename for this release

2015-06-24 version 1.4.7
* Now informs designated project members when a new release has been published through ADANS (automatic notifier)

2015-03-29 version 1.4.0
* Administrative rename for this release

2014-12-22 version 1.2.1
* Fixed markup (mail clients do not read header css)

2014-11-04 version 1.2.0
* [[ADANS]([URIs#ProjectLogo|ProjectLogo]])] improved notification text
* ADANS notifications now includes labels when applicable
* ADANS no longer sends notifications to the last editor of the issue as he already knows
* ADANS
* (for admins) ADANS now calls all strings from the ART package
* (for admins) ADANS improved debugging/testing options

Version 1.0.3

Read more information, or download ART-DECOR-system-services-1.0.3.xar.

## HL7 FHIR DSTU2 server
HL7 FHIR DSTU2 server for export of DECOR artifacts into FHIR. Supported: ValueSet, Dataset, Transaction. See [
2020-04-11 version 2.0.4
* Add search.mode to Bundles of type searchset
* Add function in fix-permissions that does cleanup of everythin older than a week in _audit and _snapshot

2020-02-15 version 2.0.3
* Improved retrieval of CodeSystems and ValueSets by canonical. Now supports @canonicalUri
* Fixed a major problem that caused query params to be omitted after redirect
* Improved semver detection. Now handles 2 level versions e.g. 1.0 and returns 1.0.0
* Fix a problem in getting a published version of a dataset for logical model generation
* Fixed link generation and FHIR retrieval of artifacts in development builds
* Fixed various corner cases for NamingSystem
* Disable returning StructureDefinition for the version of FHIR in meta
* Fixed bug in location of ValueSet.compose 

2019-05-30 version 2.0.2
* Improved the mapping-bundle and template-its operations
* Updated for the improved getCanonicalForOID/getOIDForCanonical functions in ART
* Fixed Bundle returns

2018-11-20 version 2.0.1
* Fixed an issue when retrieving as logical model from a publication. This would become a mix of published and current definitions

2018-11-05 version 2.0.0
* Initial public version after roughly four years or testing under two public servers by multiple parties

## HL7 FHIR STU3 server
HL7 FHIR STU3 server for export of DECOR artifacts into FHIR. Supported: ValueSet, Dataset, Transaction. See [[FHIR_URIs]([FHIR_URIs]])]

2021-01-19 version 2.0.6
* CodeSystem
    * Rewrote canonicalUri generation so it matches systems in ValueSet
    * Fix designation.use codes
    * Get expanded CodeSystem to include publishing details
    * Now exports empty CodeSystem with as much metadata as possible if the system is not found based on its OID
    * Fixes for codeSystems without explicit canonical
* NamingSystem
    * Improved rendering and FHIR integration
* StructureDefinition
    * Fixed sdf-9 invariant issue in logical models
* ValueSet
    * Prevent leading/trailing in display names

2020-10-02 version 2.0.5
* General
    * Add function in fix-permissions that does cleanup of everythin older than a week in _audit and _snapshot
    * Return the correct uri relative to the version of FHIR
* ValueSet
    * Don't add designation/use to a ValueSet concept unless there is a designation type to translate

2020-04-11 version 2.0.4
* Add search.mode to Bundles of type searchset
* Add function in fix-permissions that does cleanup of everythin older than a week in _audit and _snapshot

2020-02-15 version 2.0.3
* StructureDefinition
    * Disable returning StructureDefinition for the version of FHIR in meta
    * Fix a problem in getting a published version of a dataset for logical model generation
    * Large update for the dataset/transaction to FHIR logical model handling. canonicalUrl on DECOR object is now repected, fixed bug in translation extension datatypes, improved semver handling, improved handling of Dutch HCIMs specifically, improved condition handling (written into comment for manual processing after export)
    * Improved example parsing for Quantity. Now converts comma to dot to conform
* CodeSystem
    * Support CodeSystem?url=
* ValueSet
    * Support ValueSet?url=
    * Add compose in expansion
* NamingSystem
    * Fixed various corner cases for NamingSystem
    * Improved NamingSystem retrieval when multiple hits exist
* General
    * Fixed a major problem that caused query params to be omitted after redirect
    * Improved semver detection. Now handles 2 level versions e.g. 1.0 and returns 1.0.0

2019-05-30 version 2.0.2
* Improved the mapping-bundle and template-its operations
* Updated for the improved getCanonicalForOID/getOIDForCanonical functions in ART
* Fixed Bundle returns
* Add CodeSystem support

2018-11-20 version 2.0.1
* Fixed an issue when retrieving as logical model from a publication. This would become a mix of published and current definitions

2018-11-05 version 2.0.0
* Initial public version after roughly two years or testing under two public servers by multiple parties

## HL7 FHIR R4 server
HL7 FHIR R4 server for export of DECOR artifacts into FHIR. Supported: ValueSet, Dataset, Transaction. See [
2021-01-19 version 2.0.8
* CodeSystem
    * Rewrote canonicalUri generation so it matches systems in ValueSet
    * Fix designation.use codes
    * Get expanded CodeSystem to include publishing details
    * Now exports empty CodeSystem with as much metadata as possible if the system is not found based on its OID
    * Fixes for codeSystems without explicit canonical
* NamingSystem
    * Improved rendering and FHIR integration
* StructureDefinition
    * Fixed sdf-9 invariant issue in logical models
* ValueSet
    * Prevent leading/trailing in display names

2020-10-02 version 2.0.7
* General
    * Add function in fix-permissions that does cleanup of everythin older than a week in _audit and _snapshot
    * Return the correct uri relative to the version of FHIR
* ValueSet
    * Don't add designation/use to a ValueSet concept unless there is a designation type to translate

2020-04-11 version 2.0.6
* Add search.mode to Bundles of type searchset
* Add function in fix-permissions that does cleanup of everythin older than a week in _audit and _snapshot

2020-02-15 version 2.0.5
* StructureDefinition
    * Large update for the dataset/transaction to FHIR logical model handling. canonicalUrl on DECOR object is now repected, fixed bug in translation extension datatypes, improved semver handling, improved handling of Dutch HCIMs specifically, improved condition handling (written into comment for manual processing after export)
* CodeSystem
    * Support CodeSystem?url=
* General
    * Fixed a major problem that caused query params to be omitted after redirect

2020-02-13 version 2.0.4
* Introducing FHIR R4 server. Does what FHIR STU3 does and adds CodeSystem for export

## Terminology Applications

2021-04-12 version 2.0.7
* ICA-editor: Fix typo in terminology/ica/modules/set-ci-object-status.xquery

2020-04-10 version 2.0.5
* SNOMED CT: Added query for creating association and attribute value refsets
* SNOMED CT: Added support for multi language associations, refset filters and toplevel filters. Requires SNOMED package 20.3.4 (NL) or 20.4.0 (International)
* SNOMED CT: Add space between "DEPRECATED" and the term desc
* Added ordered refset html view.

2020-03-25 version 2.0.4
* SNOMED CT: added legend to browser help explaining icons. Changed search to start at three keystrokes. Added translation to hover in concept view

2020-02-06 version 2.0.3
* SNOMED CT Adapted stylesheet to hide GMDN code in mapping due to licensing issues
* SNOMED CT: updated stylesheet, new order for language refset display
* Add support for multilingual ClaML
* Nictiz Demo: Minor changes to layout

2019-09-28 version 2.0.2
* Initial version for testing and demonstration of demo

2019-03-30 version 2.0.1
* Added Nictiz terminology demo files to stable branch

2018-11-06 version 2.0.0
* Administrative rename

2017-06-15 version 1.8.31
* SNOMED-CT: Added services for retrieving refset list and refsets.

2017-05-18 version 1.8.30
* Support for LOINC Panels

2017-01-06 version 1.8.28
* Updates for LOINC 2.58
* Performance, and better accuracy in searching by LOINC id
* SNOMED-CT: fixed errors with display of Dutch FSN.
* SNOMED-CT: Removed references to extension and refsets from query.
* Terminology SNOMED: removed xqueries related to SNOMED NL extension.
* Terminology SNOMED: removed xqueries related to SNOMED NL extension.
* Terminology SNOMED: removed all API functions related to SNOMED NL extension and all xqueries that called them.
* Terminology, DHD: Update group by clauses to new syntax

2016-10-25 version 1.8.26
* Installer no longer fails when you do not have SNOMED Extension management on your server

2016-10-25 version 1.8.25
* Installer no longer fails when you do not have SNOMED Extension management on your server

2016-10-23 version 1.8.25
* updated stylesheet for refset display
* Changed DHD menu to Thesauri, added refsetId parameter to SNOMED-CT form
* reformulated refsetlist query to also use 2.2 index.
* Removed all DHD artifacts from terminology package. Removed diagnosis registration demo.
* Removed edit functions from refset editor.

2016-07-18 version 1.8.24
* Performance LOINC and improvements on accuracy. Need to install LOINC 2.56.3 or above as well...

2016-07-16 version 1.8.23
* Performance bump for LOINC searches by a factor 3
* Added new version searchLOINC services called searchLOINC-v2 which accepts parameter "string" containing the search string. This way we do not run into trouble when you need to search for special characters like "Hla-A <nowiki>[</nowiki>Type<nowiki>]([FHIR_URIs]])</nowiki>"

2016-07-06 version 1.8.22
* Fixed a problem in the LOINC detail viewer where right column values would sometimes display contents of the left column

2016-06-28 version 1.8.21
* Small search tweak in LOINC API to support new db format, while supporting the previous format introduced with LOINC 2.54

2016-06-14 version 1.8.20
* Performance update for LOINC. Please install Terminology-data/LOINC package 2.54.1 after installing this update this update will not work with the older packages. Please note that installing this LOINC data package will take considerable time to install with perceived down time to up to 45 minutes.

2016-02-12 version 1.8.0
* LOINC
    * New LOINC browser with full support for the multilingual data files
    * By default takes the user language as language but allows switching to any of the LOINC supported languages
    * Searches in both main table and language table
    * New column legend in the browser replaces the question marks with info. This enhances overview.
    * Displays the localization when available and a little flag if a column holds localized data
* Fix: when a concept has more than 1 mapping, they are now displayed separately 
* Extra: now also allows you to link to a concept that the current concept is replacing (reverse mapping)
* Extra: comments for a MAP_TO are now visible as mouse over on the link
* ClaML
    * Fixed total count display

2015-11-23 version 1.6.1
* SNOMED CT
* Fixed getDescription behavior
* Fixes search behavior by returning shortest match first
* Rewrote SNOMED-CT search with refset filter for faster response
* SNOMED CT Editor
    * Fixed bug with permissions, added nrc permission to update query.
    * Fix for missing relationship descriptions and SnowOwl id's where there should have been NL extension id's.
    * Synchronised stable with trunk and current status on production server
    * Updates for ICD10 group support and NRC attributes

2015-08-27 version 1.6.0
* Administrative rename for this release
* Fix for SNOMED CT search behavior regression. Now returns shortest matches first again
* Fix for SnowOwl imports for SNOMED-CT NL extension
* Fix for creating RF2 full release of SNOMED-CT NL extension
* Added SNOMED-CT definitions to explorer
* Report of deprecated SNOMED-CT concepts in Reference sets
* DHD editor support for DBC groups
* DHD editor ICD10 mapping priority fixes

2015-07-01 version 1.4.2
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-05-12 version 1.4.1
* DHD editor support for DHD 3.0 datatypes

2015-03-29 version 1.4.0
* LOINC browser rewritten to support more localization (UI), pagination, improved sorting and more
* Fix: CLaML hierarchy expansion fixed
* Lot of performance and stability improvements under the hood

2015-03-23 version 1.2.4
* Bug fix in reindex

2015-01-26 version 1.2.3
* updated icd-10-nl classificationId used in ICA-editor
* Changed form and query to handle grouped relationships (get-snomed-extension-concept.xquery and snomed-extension.xhtml)
* Bug fix: in getting refset list with lots of empty entries
* Added search in extension
* Bug fix: enables the addition of extension concepts to refsets
* Bug fix: creation of descriptions in snomed extension
* All links in license now have http:// prefix. Removed superfluous groups/tables
* Bug fix: setting relationship status

2015-01-08 version 1.2.2
* Fixed bug with display of draft concepts

2014-11-18 version 1.2.1
* ClaML based terminologies are now parsed better for their names

2014-11-04 version 1.2.0
* ClaML based terminologies now support status through Meta key name='statusCode' through View and Search
* Numerous SNOMED-CT management and publication improvements

2014-09-29 version 1.0.8
* Fixed bug creating 'undotted' ICD10 code.
* Added daily system job for activating and retiring of objects.
* Fixed bug preventing selection of DBC for AGB if retired DBC for this AGB is already present.
* Changed query and controller to work with Snomed extension.

2014-09-23 version 1.0.7
* DHD: 
    * removed check on expirationDate
    * Removed ICD10 sorting by @no. 
    * Changes for datamodel 3.0
* SNOMED CT: Fixed display of double preferred terms and missing preferred terms.

2014-09-17 version 1.0.6
* Various SNOMED CT and DHD Thesaurus Editor fixes

2014-09-10 version 1.0.5
* Fixed bug with id search for NL extension concepts without preferred term. (SNOMED CT)

2014-08-27 version 1.0.4
* Upon install only write index files and reindex when they are different
* Improved collection creation when path does not exist yet (we were missing /db/system/config/db/apps/terminology-data)

## XIS Test information system
Client and server test information system

2020-04-11 version 2.0.9
* Implement saving account selection upon change in the test-accounts

2020-03-26 version 2.0.8
* Performance improvements in saving tests
* Fixed selection of last visited account on messages and test-accounts forms

2020-03-14 version 2.0.7
* Fixed behavior when account messages are removed but testseries still contains a copy. Upon revalidation this copy is written back into the messages folder and is thus again available for normal viewing/validation/rendering
* Improved test-accounts
    * Now displays very clearly if and which errors exist
    * Better load times by loading user list async
    * Leave more room for accounts by moving URL one row up
    * Fallback to username when full name is unavailable.
* Improved message rendering
    * Add support for code with qualifier
    * Don't add  "Unit" / "Units" in case of missing PQ.unit
    * Add support for IVL_TS with only a width
* General
    * Don't save @filecount / @lastmodified

2019-07-30 version 2.0.6
* Add support for indexing ClinicalDocument/organizer elements
* Account selection improvement
* Allow better selection of message types

2019-07-16 version 2.0.5
* Support for searching messages/files based on "older than date" and/or "root element"
* Support for deletion of files based on "older than date" and/or "root element"
* Support for manual entry of a page number, instead of clicking one-by-one
* Message: Fixed inability to retrieve the base validation upon adding a testmessage to a testseries which was not validated yet using the base package
* Test Accounts: Add file count for test accounts to view
* Major changes in index: 
    * file/meta/file/@name is now file/meta/file/@filename
    * file/meta/file/@root is now file/meta/file/@rootelement
    * file/meta/file/message/@root is now - file/meta/file/message/@rootelement
    * This affects the eXist-db index too. Please update manually by running the code below and then by replacing the index definition as found in pre-install.xql, and reindex

<syntaxhighlight lang="python" line="line">
let $docs           := collection('/db/apps/xis-data/accounts')/file/meta

let $update         :=
    for $f in $docs/file[    return (
        update insert attribute filename {$f/@name} into $f,
        update delete $f/@name,
        update insert attribute rootelement {$f/@root} into $f,
        update delete $f/@root
    )
let $update         :=
    for $f in $docs/file/message[not(@rootelement)](not(@filename)])
    return (
        update insert attribute rootelement {$f/@root} into $f,
        update delete $f/@root
    )
</syntaxhighlight>

2019-06-21 version 2.0.4
* Prevent potential bug for account without packages (only after fresh install)
* Add index on file/@filename, file/@rootelement, message/@rootelement
* Fixed getting core validation upon associating a message with a test
* Remove unused form tests (included in messages)
* Add download option for files

2019-05-30 version 2.0.3
* Messages in HTML
    * Improved rendering of II with @nullFlavor + @extension
    * Fixed issue with references to # and .
    * Fix for @codeSystem rendering

2019-03-12 version 2.0.2
* Messages HTML
    * Improved CDA section/text display
    * Improved ED.reference display for internal references
    * Improved display for Prescription and SubstanceAdministration elements
    * Externalized datatype switch display template
* Test Accounts
    * Performance. Only update permissions if new upon save of accounts
* Test Suites
    * Sort by id
    * Fix for failure to load the account tests

2019-01-22 version 2.0.1
* Messages
    * Fixed issue that caused inability to download/render tests
    * Improved test status when generic tests are not ok
    * Improved display of test message meta data
    * Validation: only grab mapping line for schema if element nam(space) matches too next to templateId
    * Smarter send template retrieval
    * Fixed issue when package contains multiple templates of the same kind
* Files
    * Index hack for pseudo webservices based on POCD_IN000040NL

2018-11-05 version 2.0.0
* Worked around namespace issue in eXist-db 2.2 and up
* Compatible with Orbeon 2018 (workaround for namespace bug)
* Now saves only new/changed accounts saving lots of performance issues.
* HTML rendering
    * Fixed rendering of PQ when there are no units and the @value is below 1
    * Improved rendering of codes by added the displayName to view

2018-10-06 version 1.8.35
* Restored the hack workaround for the namespace issue in eXist-db that kills namespace declarations for @xsi:type value. Turns out that upon transform:transform() for schematron it does this once more, unless we save to disk first. The best solution would be to write every message in its own file, but that would require a much bigger rewrite. The smaller solution is to write a temp file in the account just before validation and cleanup that up right after.

2018-10-03 version 1.8.34
* Workaround for https://github.com/eXist-db/exist/issues/2182
* Fixed a problem when input file has leading comment
* Small visual patches
* Improved workings of ?wsdl by redirecting to the file instead of immediate retrieval.
* Various improvements in HTML rendering of messages
* Performance improvements

2018-09-18 version 1.8.33
* Messages
    * Fixed problem where deleting any messages or files after the first one without explicitly changing index would result in deletion of the first in the list
    * Major update for the default HTML viewer to support more constructs in a better way
    * Now renders a button for each viewer where applicable

2018-03-27 version 1.8.32
* Messages
    * Fixed problem with service selection in query/message send tabs [    * Allow /schematron/ for schematrons. First /schematron_svrl/, then /schematron_xslt/, then /schematron/, final default /schematron_svrl/
    * Bug fixed in endpoint retrieval
    * Bug fix: Testsuiteid did not contain testsuiteid
    * Fixed a problem in the HTML button on Tests
* Installer
    * Performance updates. Note: writes new index and triggers reindex!!

2017-10-01 version 1.8.31
* Messages
    * Now lists the creation date of a file, which is relevant compared to when it was validated
    * Close testsuites by default
    * Adds details of a selected testsuite/scenario/file to view.
    * Now uses the saved copy of a message to validate against instead of going back to the file for test scenarios. Before it would be able to revalidate if the file was deleted. Still missing default schema(tron) though. First check file as uploaded, fallback to previously saved file if that fails
    * Fixed an edge case that caused the wrong package to be selected in a multi package account where the input is CDA and the default package did not have the right schema
* Test accounts
    * Fixed problem in deleting account data
    * Don't keep delete account button active when the selected account is no longer in sight
    * Select first visible account when you change selections
    * Moved deleting account data into xforms-part.xml so as to refer to it from both messages.xhtml and test-accounts.xhtml giving them on par functionality
* Validate single instance
    * Validate single instance now strips namespaces in output to make it more readable
    * Fixed a problem where instances without text node woud not be recognized
    * Fixed a problem for instances that start with a UTF-8 Byte Order Marker (BOM)
* General
    * Fix for error when account doesn't exist
    * Small performance updates
    * All font-family Verdana now has more fallback to Arial and sans-serif
    * Refresh of interaction list
    * Fixed a problem that prevented authorization when you download a message from the tests
    * Fixed bug in deleting resources (permissions issue)
    * Fixed bug in deleting resources (spaces in filenames issue)

2017-06-14 version 1.8.30
* Fixed package selection when multiple are available and the default package doesn't have it. Before it would pick the first after the default, regardless of whether or not that package actually contains useful stuff
* Fixed a number of performance issues on accounts with a large (> 76k) number of messages
    * Can now empty account data within reasonable timeframe. Drops table and recreates instead one-by-one
    * Can now load message within a more reasonable timeframe but could still do with improvements
    * Changed the load proces of the form. Now loads message/file list after the form is on screen. The file list is actually not load until you click the tab for the first time or trigger it after sending a message

2017-05-12 version 1.8.29
* New: templateId based schema selection using the same type of mapping rules that already existed for schematron selection. This feature allows you to override the default selection based on root element.
* New: viewer selection. Now allows you to specify alternative (xslt based) viewers for rendering your messages

2017-04-27 version 1.8.28
* Fixed permission errors when calling for stuff that needs permission, now credentials are requested before failing (RFC 2617)

2017-03-16 version 1.8.27
* Synchronized multiple patches from trunk to stable to support multiple hl7-materials in testaccount
* Don't allow set default on resources while they are empty
* Don't allow adding the same resources again
* Don't allow removing default resources.
* Updated namespace URL
* Fix for HL7 namespaced elements
* Also remove elements with xsi:type != urn:hl7-org:v3
* Improved indexing for non-interactions
* Don't hide error like permissions
* Enhanced errors from save-file-upload.xquery
* Added frond-end service UploadMessage for save-file-upload.xquery
* Friendlified upload error for unsupported file types.
* Fixes the case of the dissappearing drop downs on Query/Files tabs
* Show XSD info too
* Tell whether or not resources are default for the account
* Check if requested HL7 resources are still part of the account the validation is performed for.
* Various performance and visual improvements.
* Implemented test selection
* Fix for single resources accounts
* Upon initial validation matches first package containing XSDs for the message and uses SCHs from there too
* Message based resource preferences
* Added @mutkod for displaying mutation code to prescription viewer

2017-03-15 version 1.8.26
* Added additional elements into display

2017-01-10 version 1.8.25
* Eradicated login requirement as xis-webservice
* Improved SOAP error when webservice is not active
* Various visual updates to Live Runtime Compile
* Removed unused functions and added index on collection XIS

2016-11-10 version 1.8.24
* Messages
    * Updated textual validate/refresh buttons with images
    * Fix for missing schematron errors from test_xslt
* Test-accounts
    * Added account last modified time to view. It was thusfar only visible as hint on the account delete button
    * Removed constraint that prevented saving account changes for no reason
    * Made default package checkmark readonly for single package accounts
    * Updated logic when switching default for multi package accounts
* Single validation
    * Catch role warn
    * Fix some of the unexplained extras in the text node of an error
* Tests
    * Fixed problem in getting result for test without file
* Performance bumps

2016-10-25 version 1.8.23
* Added a schema for test-accounts
* Fixed a bug in validating tests when multiple resources are connected and the the tests is not connected to the default resources
* Implemented direct revalidate / delete buttons without dialog in between to facilitate 'bulk' actions

2016-09-02 version 1.8.22
* Fixed a problem where unmarked schematron issues would not surface
* Fixed a problem where info/information schematron issues would not surface if no errors/warnings were present
* Performance improvements in loading accounts
* Don't fail on missing testsuites
* Don't fail on missing base file upon revalidation
* Reset offset if above total count
* Improved indexing of CDA like payload content
* Sort query/message lists
* Fix sorting by root-name/root in selecting messages dialog
* Support input in sending application id

2016-08-11 version 1.8.21
* Fixed a problem where the generics are not retrieved using the correct resources. Only relevant to multi-resources accounts.
* Fixed a problem where the resources are not sent in requesting a query to be sent. Only relevant to multi-resources accounts.
* Fixed a bug when an empty SOAP:Body is encountered
* Now finds WSDLs in all configured packages, not just the default package
* Fixed a problem where the file selector would disappear when you searched for something that wasn't found

2016-06-14 version 1.8.20
* New Implemented searching by file name across tabs in Messages form

2016-05-04 version 1.8.3
* Messages: can now add files to test based on filename
* Testsuites: now copies &lt;name/> element from HL7 materials too

2016-04-03 version 1.8.2
* No longer allows adding a new account until the current is deemed valid and saved
* Initialization of accounts immediate updates error checks

2016-03-26 version 1.8.1
* Improved indexing of SOAP:Envelope by adding the contained root element to the name
* Test-suite creation/updating now works as expected
* Updated: integrated tests support in messages form to have everything together. Legacy tests form still works but is now deprecated
* New: support for test-account deletion
* Improved performance of various parts
* Smaller UI tweaks

2016-02-12 version 1.8.0
* Fixed approval buttons in tests form

2015-12-05 version 1.6.6
* Performance update: save generated HTML with message and try that before regenerating
* Performance update in building HTML
* Performance update in getting message templates in account
* Prevent unnecessary error upon failure to save last selected account

2015-12-08 version 1.6.5
* Add close="true" to messages so you may escape them if the close button is out of reach
* Now renders RTO_QTY_QTY properly
* Fixed a problem where receiver info was present but unavailable in the view
* Prevent problems from handycrafted situation where a person has >1 lastSelected account
* Fixed a problem in indexing a XIS file with non-XML Base64 contents.

2015-11-05 version 1.6.4
* Test-accounts
    * Show all accounts for dba. Show only accounts you are a user of otherwise

2015-10-30 version 1.6.3
* Fixed sender properties applicationId / UZI-system id
* Rearranged messages form a little for clarity and added sender properties to view

2015-09-30 version 1.6.2
* Fix for inability to create zips under special circumstances where comments/processing-instructions precediing the root element were saved too

2015-09-25 version 1.6.1
* Messages
    * Fixed permissions for uploading and webservice
    * Fixed root element detection
    * Added root info column in file tab

2015-08-27 version 1.6.0
* Test accounts
    * now cannot edit accounts when not editor (some parts were left world writable in client but saving would yield an error)
    * now allow for multi language display names on role-code and saves display name too.
    * Can now filter accounts based on XML materials which is particularly useful when you install updates
    * Implemented error summary so you can see at a glance what needs attention before saving
    * Now allows account ZIP download, reindexing and fixing permissions (after manual database work)
    * Now allows attaching more than 1 XML package to an account with one being the default
* Complete rewrite of the messages section
    * New pre calculated indexing mechanism gives enormous performance
    * Loads paginated message lists 50 at a time
    * More reliable permission management upon upload/SOAP of new messages
    * Can now switch between configured packages. Validation results are saved so switching back and forth does not constitute revalidation. Manual revalidation is still an option. This allows impact assessment in going from 1 package to the other
* Improved account ZIP generation logic
* Tests
    * Now allows overriding results by an admin after assessing how serious reported problems and warnings are
* Numerous tweaks for performance and visuals

2015-07-26 version 1.4.4
* Fixed couple of literal ampersands in messages
* Added more interaction names for display

2015-07-09 version 1.4.3
* Fixed problem that would occur when uploading files under Orbeon 4.7

2015-05-20 version 1.4.2
* Now supports any casing for SOAPAction HTTP header. Header value is still case sensitive

2015-04-15 version 1.4.1
* Activated a selector for the SOAP end point so you know which ones are available while still allowing manual override.

2015-03-29 version 1.4.0
* Improved file and message retrieval performance and reliability
* Improved message when no schematron is found

2015-03-19 version 1.2.15
* No longer displays unjustified errors/warnings on codes in HTML display of messages.
* HTML now has a proper title and an improved main body title

2015-03-03 version 1.2.14
* Now supports retrieval of query param config from an HL7 package with fallback to the current config in the form. For more information, see: [[XIS_maintenance#Configuring_query_parameters|XIS_maintenance/Configuring_query_parameters](2])]
* Refresh query parameter list upon switching accounts
* Permissions improvement for account art-decor
* Only show send query/send message tab when there is send query or send message content available
* Indexes compatible with eXist-db >= 2.2
* Removed exclusion of elements with @negationInd='true' from test. Made comments out of <error> nodes, to always yield valid such files
* Remove whitespace and escaped hex chars from filenames before saving an upload

2015-02-19 version 1.2.13
* Made SOAPAction interpretation more lenient. Now optionally accepts both leading and leading single and double quotes.
* Fixed bug in SOAP:Fault responses based on $account
* Disabled controller cache as this prevents subsequent calls to the same Webservice URL when you missed the SOAPAction URL the first time.
* Added query parameters for specific queries
* Replaced all fixed body elements for additional query parameters with flexible repeaters. Due to this change you loose localization of query parameter names, but gain a flexible interface to responds to the meta model in the xforms instance. The only missing now is externalizing that instance and dynamically loading it, rather than fixed data. Adds solution for repeating parameters (AND) as well as repeating paramater values (OR). 
* Fix for missing root and extension in query parameters

2015-02-12 version 1.2.12
* Fix for deleting files with spaces in name 

2015-01-19 version 1.2.11
* Fixed deletion problem in testseries

2015-01-10 version 1.2.10
* Fix for not recognizing availability of a SOAPAction header in reception of incoming traffic
* Fix for missing namespace declaration in parameters on queries

2015-01-06 version 1.2.9
* Rename various functions to facilitate fix-permissions

2015-01-06 version 1.2.8
* Rewrote account data deletion so everything goes through delete-account-data.xquery
* Account data deletion on Messages form now goes through a dialog as protection
* Account data deletion now supports full account data delete in addition to deletion on a per file basis.

2014-12-22 version 1.2.7
* Support conditional adding of query parameters (only when valued.
* Support parameter mostRecentDispenseForEachRxIndicator on QURX_IN990111NL

2014-12-20 version 1.2.6
* Messages: add QURX_IN990101NL02 as query
* Messages: Allow application-id and organization-id override when sending files (useful mostly for ART-DECOR account)
* Install: Fixes xis accounts too if possible.
* Messages: Return accounts for guest too when logged in

2014-12-04 version 1.2.5
* api-permissions.xqm: copied/adapted local:setArtQueryPermissions to local:setXisQueryPermissions to set permissions based on xquery name 

2014-12-04 version 1.2.4
* Files are now all listed and not only if they contain messages we know
* Added button to view contents of a file

2014-11-18 version 1.2.3
* Orbeon 4.x fixes: explicitly cast to xs:date or xs:dateTime

2014-11-11 version 1.2.2
* HTML viewer for messaging: Adds B64 rendering for hl7:text elements comparable to hl7:value
* Retrieving and Rendering messages: Fix for retrieval of files with special characters.

2014-11-06 version 1.2.1
* Messages form: fixed problem getting file meta data

2014-11-04 version 1.2.0
* Added option to bulk download messages, ticket #202

2014-10-16 version 1.0.15
* Circumvention of a bug in the path generator function that causes validation/rendering problems for messages in a file with similar siblings of the message

2014-10-16 version 1.0.14
* Updated validation logic to support multiple messages per file each with their own validation status.
* Improved error trapping for more screen guidance after message validation
* HTML version of messages (Dutch only), now has Base64 support when encountered
* Messages and tests no longer show messages inline, but feature a button for a separate window. This significantly boosts performance and prevents interference with data tables on the same forms.
* (for admins) The Test-accounts setting "Show XML" is no longer useful and is ignored. It will be removed at some point
* Bug fixed that caused empty testseries.xml in test accounts after pressing save in Test Accounts
* Improved reliability of HTML display of messages
* Various smaller fixes

2014-10-16 version 1.0.13
* Updated list of supported queries
* Added interactions and patient 999911624
* Updated resources vocab

2014-10-02 version 1.0.12
* Fix for display of Contact with multiple components and differing sequenceNumbers

2014-10-01 version 1.0.11
* When rendering a message in HTML, start rendering from first interaction or ClinicalDocument

2014-09-18 version 1.0.10
* Fixed initialization for the messages tab dropdown menus

2014-09-10 version 1.0.9
* Updated list of supported queries

2014-09-05 version 1.0.8
* Fixed rare situation where file-list retrieval would fail because of a missing messages collection under a test account

## OID Registry tools
OID Registry tools. OID Registries are a basic building block for other parts of ART-DECOR.

2020-04-11 version 2.0.2
* Fixed a problem in index definition for fulltext index
* Workaround for Orbeon CSS bug where xf:output/xf:label leads to display problems. All these xf:label elements were hidden anyway so disabling them did not have any side effect

2019-05-30 version 2.0.1
* Update to the latest version of the ISO 13582 standard for OID Registries
* Preparation for OID editing support

2018-11-05 version 2.0.0
* Administrative rename for this release

2017-05-15 version 1.8.2
* Add name for the lucene index (potential for long install time as reindex occurs)

2016-08-16 version 1.8.1
* Removed duplicate new-range index. The old range index performs better and more reliable

2016-02-12 version 1.8.0
* Added indexing support for FHIR System URIs

2015-08-27 version 1.6.0
* Administrative rename for this release

2015-03-29 version 1.4.0
* Administrative rename for this release

2014-11-04 version 1.2.0
* Administrative rename

Version 1.0.3

## ADA - ART DECOR Applications
ADA is a toolkit for building applications from DECOR specifications. It is useful for rapid prototyping, test-driving functional specifications and as a starting point for serious development.

2020-04-11 version 2.0.5
* Support for new valueDomain/@type 'time'
* Add instance description to index of projects (can activate/deactivate)
* Don't include concepts that have max 0 in releases
* Fix duplicate hits causing functional corruption to data upon retrieval/save
* Fix permissions problems in ada-data/db
* Add empty value for NullFlavor to prevent validation-error in XForms
* Fix for empty attributes that hindered effective html generation
* Fixed missing @value when @datatype has @value
* Improved VariableVagueDateTime for {00:00:00} variable time

2019-09-29 version 2.0.4
* ada2release.xsl
    * Add support for multiple valueSets by merging them into 1
    * Add support for view level override of language
    * Add support for potential of multiple names in definition file
    * Allow for concept/@following-ref and concept/@preceding-ref to add concepts following or precding another concept instead of just into
* ada2xforms.xsl

2019-07-30 version 2.0.3
    * Add support for changing the instance id. All ada has been updated to support having the same id in multiple projects
* ada-xml.xqm
    * Now retrieves code/codeSystem from localId or vice versa.

2019-07-18 version 2.0.2
* Add support for @datatype attribute, added for supporting concept/valueDomain[This introduces elements in ADA with all attribute types active in new-xml, but we only want relevant combinations
* Add support for alternatives for <=? and >=? in shortName routine
* Allow description in addition to title on instances
* Add global parameter overrideContainsAsIdentifier to add valuedomain identifier where only contains exists

2019-05-30 version 2.0.1
* Add support for adaextension element that may occur on any element using arbitrary contents that should be left alone during normal ADA processing.
* Add support for @ordinal so a user may enter ordinal values in his instance. 
* Improved adaxml:removeEmptyValues by checking if elements contains anything useful (non-empty attributes or elements). This also suppresses empty adaextension elements
* Improved XML output by omitting unnecessary namespace declarations.
* Improved support for (multiple) valueDomain properties on various types
* Fallback to string if there's no valueDomain.
* Fixed logic for adding @value based on @code/@codeSystem or the other way around
* Don't delete groups when there's a concept with @nullFlavor in forms
* Fixed issue with HTML generation where the parent def was not carried over correctly
* overrideDatatypeDateTimeAsString now supports date too
* Fixed an issue where nesting of multiple concepts was rejected
* Fixed a problem in generating forms for concepts with fractionDigits property

2018-11-05 version 2.0.0
* Fixed call for overrideDatatypeDateTimeAsString
* Don't produce global error part in xform. They are normally empty and yield an issue for Orbeon 2018
* Fixed the missing part of the fixes
* If $debug is false, add inspector based on exist-db group debug
* Fix for when the instance has nullFlavor and new xml has an additional attribute with data. This should be added in empty
* Sort drop down for new instances in index
* Add param options to projects for global overrides
* Add empty @root upon retrieving data from db instead of adding it with a default value that the original data did not have

2018-08-21 version 1.8.25
* Index files now offer optional column Id
* Index: don't show Edit button on index if username doesn't match owner
* Index: default collapse sections
* Index: check if user has ada-user group permissions before offering editor functionality
* Add support for @fractionDigits, new syntax for "max X digits" in property/@fractionDigits using a trailing dot, so "3." means "max 3 digits"
* Fix support for duration/count bindings
* Enhacned support for timeStampPrecision
* Change schema type for timestampPrecision Y! and YM! to xs:string as those can never be a date
* Change schema type for type='datetime' and timestampPrecision YMD! to union of xs:date and xs:dateTime
* Added @nullFlavor, @codeSystemName, @codeSystemVersion support
* Fix for inability to save data after initial save of new
* Adds a "Remove empty elements and groups button", which is also triggered when starting a new instance, so you only worry about the stuff you want to use
* Added lastModified date to the application index
* Now sorts by owner and descending lastModified
* New: Adds first stab at identifier (@root) support
* Improved handling when multiple valueDomains exist
* Fix: index-admin.xquery sometimes wrongly used shortName instead of form name
* Fixed the helper function addConceptId, and improved getting of spec file.

2017-10-01 version 1.8.24
* Project data forms
    * Fixed saving code|codeSystem|displayName without @value
    * Fixed problem with addCode causing inability to save data
    * Added some readability to camelcased names when those are converted into a shortName, they get an underscore just before the capital, leading to short_name.
    * A little more room for units in the interface (4em to 6em)
    * save-data now returns the data after save so you get access to the id that was assigned
    * adaxml:saveAdaXml now always returns id of data it saved
* Project indexes
    * Include sorting by short name for multi transaction type applications
    * Prevent illegible error when id is not provided
* Admin index
    * Added sorting based on creator and lastModifier
    * Adds selector even when you are in 1 app so you may switch more easily
    * Adds sorting by short name like in newer form indexes
    * Added id to the title of validation results
    * Fixed problem when index-admin is called without arguments
* General
    * Don't return last-update-date unless $summary
    * Get internal modification date instead of db modification date
    * Better preservation of data in existing instances when the dataset changes names. Check @conceptId based matches too in merging the saved data with the new data
    * Fixed https://sourceforge.net/p/artdecor/tickets/228/. DECOR Project now has extra tab 'ADA' if ADA is installed on the server and will list any apps related to the DECOR project with a link to the index
* Core XSLs
    * General
    ** Support for @unit even when not property
    ** Add code / codeSystem / displayName to "new" instance when there are no predefined codes. Apparently you then need free form entry support. Added free form entry support in the form
    * Release generation
    ** Support for multiple valueSets in the release as long as they only define completeCodeSystems
    * XML Schema generation handles valueDomain identifier and boolean better
    * XForms generation
    ** Deactivate New, Edit and Duplicate buttons when not logged in
    ** Always add accordeons for concept groups. They are closed by default unless they have @initial = 'open'
    ** Adds accordeons on data based on short name. If there is one type of short name, no accordeons will be generated, but when you have multiple apps, you get accordeons (open by default) and Collapse/Expand All buttons.
    ** improved binding for boolean and now only allows Unknown when conformance is not mandatory
    ** Adds a delete button to the index if the item is created by logged in user
    ** Fixed a problem causing new instances to be saved upon every Save as ong as the user kept the "new" window open
    ** Adds codeSystem in the newxml if there is only one.

2017-08-04 version 1.8.23
* New: Adds "Save draft" button that does not close the form
* New: Now sorts the files in the index by owner and date, newer first
* New: Support for properties on duration
* Fix: Reverted revert of xs:decimal to constraint to circumvent a problem in Orbeon
* Fix: Adds all missing attributes upon opening data for edit. This fixes a problem where adding @root later was not possible.

2017-05-12 version 1.8.22
* New: Adds first stab at identifier (@root) support
* Improved handling when multiple valueDomains exist
* Fix: index-admin.xquery sometimes wrongly used shortName instead of form name
* Fixed the helper function addConceptId, and improved getting of spec file.

2017-01-10 version 1.8.21
* Improved support for conditional concepts. These could be marked @minimumMultiplicity > 0 which would trigger a warning in combination with notPresentWhen, and lead to problem downstream in the schemas, new xml and xforms
* Generates both .tmpl and normal versions of repo and expath-pkg. This allows building packages with either eXide or Ant

2016-04-14 version 1.8.20
* Improved support for datatypes such as boolean and decimal
* Support for nested tabs
* Added a switch for errorSummary
* Improved routing on /ada/

2016-04-06 version 1.8.2
* Major rewrites to support multi transaction applications while remaining backward compatible

2016-02-12 version 1.8.1
* Performance improvements, especially in generated indexes
* Improved internationalization
* Rolled out 'duplicate' logic which creates new ADA record based on existing one
* Various bugfixes, none critical
* Better datetime and decimal support
* completeCodeSystem support

2016-02-12 version 1.8.0
* All forms texts are now localizable and ADA now appears in the language-support form for admins
* Added a backup facility which auto-backups data on a new install of an ada app
* Numerous smaller stability fixes and UI tweaks

2015-12-23 version 1.7.3
* Support for saving drafts (incomplete records)
* Coverage check (which concepts are touched in ADA data)
* Various bugfixes and UI improvements

2015-11-11 version 1.7.2
* Guest has read access
* Support for prototypes in creation of new records
* Language support on app generation
* ADA specs can be made in ADA myCommunity
* Support for ordinal
* Logger added to track bugs
* Added dashboard 
* Added index-admin with debugging and validation tools
* Various minor UI improvements in generated apps 

2015-09-03 version 1.6.1
* Bug fixed on permissions in ada-data/db

2015-09-03 version 1.6.0
* Now supports creating of xar packages to create installable projects
* Project data is retained upon project package updates.
* Various improvements in the UI

2015-03-29 version 1.4.0
* The RESTful interface accepts application/json and application/xml for GET and POST. JSON will be converted to and from XML.
* Added support for FHIR-style RESTful retrieval.
* Concepts of type code, which have no conceptList or valueSet, will generate a text input instead of a select. This allows the user to manually enter a code. Useful for medication etc., when DECOR does not provide all choices.
* ADA XML will now contain @code/@codeSystem for exceptions (was: only for concepts).
* Changed license to GNU Affero GPL.
* Form now shows error summary after submission if not valid.
* Adds support for conditional 'NP' concepts.

2014-11-04 version 1.2.0
* Administrative rename

2014-09-29 version 0.8.2
* Better hints on errors.
* Better configuration for locations of exist etc.
* Check that empty strings are really empty.
* More modular location configuration in conf.xml.
* A separate ADA login (for non-ART systems).
* Cleaning of @hidden elements. Fixed bug, now code/codeSystem are saved.
* Proper handling of insert/delete of x..* concepts, regardless of nesting depth. Also x..* concept items are now properly supported.
* Fixed bug in post-install

## ART-DECOR XML Editor
DECOR Temple Editor is the advanced version of the default version contained in ART. It allows direct manipulation of the underlying definition in XML. It supports content completion, and validation of the definition before saving.

2020-04-11 version 2.2.4
* Support for new valueDomain/@type 'time'
* Fix in inspector popup after save
* Fix in DECOR help link

2020-02-13 version 2.2.3
* Small fixes to button behavior
* Inspector to bottom, not right

2020-02-12 version 2.2.2
* Fixed creation of versions and adaptations for artefacts. Now includes parameter mode in the call
* Fixed calling the correct version of an artefact. Now includes parameter effectiveDate in the call
* Fixed behavior of Close on last tab
* Now shows Loading... while loading an artefact
* Search window is now on bottom by default
* Fixed activation of Edit button. Only active when applicable

2020-02-12 version 2.2.1
* Fixed issue in getting valueSet nodes upon clone. These would not be cloned
* Improved predictability of order of attributes across different calls @id, @name, @displayName, @effectiveDate, @statusCode, other
* Updated menu option (F11) to "Full Screen (Esc for exit)" 

2020-02-07 version 2.2.0
* Introducing Temple II
    * slick new interface with maximal editing space, unobtrusive buttons
    * multi-tab editor: this makes opening new templates from within Temple superfast
    * async retrieval of autocompletes, Temple II starts in fractions of seconds instead up to and > 10 seconds
    * Inspector dockable right, below, hidden
    * Search LOINC and Snomed from within Temple. Select, drag&drop to template/valueSet
* Add missing support for hl7nl:IVL_QTY and hl7nl:INT

2019-05-30 version 2.0.3
* Rename to ART-DECOR XML Editor
* Add support for valueSet/@experimental
* Add support for codeSystem/@experimental and codeSystem/@caseSensitive

2019-03-12 version 2.0.2
* Saving templates
    * If there's a new @id, replace it in templateId/@root, attribute/@value of hl7:templateId and element/@id as well
    * Also replace up @elementId

2019-01-22 version 2.0.1
* Template
    * Correction in enumeration AssertRole. Changed Hint to Info
    * Fixed issue when multiple template versions exist
    * Added support for element constraint.
* ValueSet
    * Fixed issue where temple:valueSetShortName could try to return more than 1 string

2018-11-05 version 2.0.0
* Fixed disabling/enabling Save button

2018-10-06 version 1.8.33
* Added CodeSystem editing capabilities to Temple and activated the Temple edit button in the CodeSystem form

2018-05-16 version 1.8.32
* Fixed save bug
* Removed jQuery dependency

2018-01-14 version 1.8.30
* Enhanced template @strength support
* Fixed various valueSet schema elements and attributes
* Added valueSet @canonicalUri support
* Added intensional valueSet support

2018-01-12 version 1.8.29
* Templates/ValueSets
    * Fixed a problem templates/valueSets with multiple versions

2017-11-19 version 1.8.28
* Templates
    * Fully renewed template example generation. Now allows for fully recursive and partial (from element[@id](@type='complex'].) onwards) example generation regardless of whether the context is an in memory template (Temple / Template-editor) or any template based on @prefix/@id/@effectiveDate. At present Temple will only support examples for the current template.
    * Fixed dataset concept tree in the drop down so it now follows dataset hierarchy
* Templates / ValueSets
    * Now is language aware in search

2017-10-16 version 1.8.27
* Fixed a problem in using CTRL+Space for content completion
* Fixed a problem in loading datasets when there is only one
* Fixed a problem where the language was not suppied to the dataset concept retrieval so it used defaultLanguage

2017-10-01 version 1.8.26
* Save button now disables when there's nothing to save leaving you with better indication of whether you still need saving
* Fixed problem in generating json when there is an empty codeSystem attribute in the project
* Enhanced Temple so it can now switch between datasets to associate concepts from without reloading
* Performance update: dataset concepts are not loaded at startup, but only when you ask for it after starting Temple. Most updates to templates do not involve associations

2017-05-12 version 1.8.25
* New feature: Temple is now suitable as valueSet editor. Obviously you do not have code-from-codeSystem selection feature (yet?), but content completion including named @codeSystem and include/@ref is featured
* Added codeSystem to supported inspectables
* Various improvements in art-decor.xqm usage eg for getting projects/status change/valueSet preparation.

2017-04-27 version 1.8.24
*Fixed temple:conceptShortName so it detects empty name elements too and makes better use of existing functions
*Fixed read-only permissions
*Fixed logo height
*Now sets an initial id / effectivedate on clones
*Fix for templateId when template is a new version.

2016-11-10 version 1.8.23
* Small rewrite for group by to align better with XQuery standard
* Added missing template classification types
* Added datatype hl7nl:PIVL_TS
* Now supports id/name/flex retrieval for template[and valueSet[@ref](@ref]) too
* Fixes a problem where templateAssociations were not saved when there was no templateAssociation element yet

2016-07-02 version 1.8.22
* Fixes a validation/save problem introduced in version 1.8.21

2016-06-28 version 1.8.21
* Fixes a problem in validating/saving templates with prefixed datatype values
* Fixes a problem in including/containing/binding templates/valueSets that are referenced from a BBR
* Fixes validation problem when a binding occurs on an attribute
* Upgrade to CodeMirror 5.16.0

2016-06-14 version 1.8.20
* New: Example generation. Temple now supports the same example generation feature that ART does. Generating an example using a button returns it in the Inspector. Adjust it until you are satisfied, and copy it into your main window pane as replacement example or add it to your other examples. Example generation will trigger validation first so you know you have a valid template before generating examples from it.
* Improved: Validation will now also check namespace declarations. This is highly useful when your templates use namespaces other than the HL7 version 3 (CDA) namespace. You still set namespaces in the ART Project form
* Improved: Content completion now supports example @type
* Improved: Validation now also supports associations with an attribute

2016-02-12 version 1.8.0
* updated copyright to 2016

2015-11-25 version 1.6.4
* Fixed missing quotes in autocomplete for id, codeSystem

2015-11-24 version 1.6.3
* Add support for element/@strength and vocabulary/@domain
* Don't write @conformance when @isMandatory='true' in expansion of shortcut cc

2015-11-21 version 1.6.2
* Small enhancement of the error output when Temple fails validation
* Fix for inability to validate/save when content has HTML special characters

2015-11-14 version 1.6.1<br/>
Temple patches:
Fix: Temple outputs message when no concept or name is found for concept/@ref.
Return type xs;string? for temple:conceptShortName to make temple more robust against pathological decor files.
Added check on duplicate element id.
2015-10-30 version 1.6.0<br/>
The DECOR Temple Editor has had extensive testing and received many new features in this major overhaul. These include:
* Supports references to templates and value sets based on id rather than name. Dealing with name based references can lead to ambiguity once you start referencing to other projects that may have artifacts with the same name as references in our project. For this reason the HL7 Template ITS was updated to favor id based references over name based references.<br/>Dealing with id based references however is hard when looking at plain XML but Temple makes this task as easy as name based references with auto completion and inspection (see next)
* Supports an inspector that allows you to navigate from any id to the artifact it points to
* Does Template ITS-aware syntax coloring improving the readability by miles
* Has full screen support (F11) for bigger templates
* Allows you to bookmark items you would like quick return to
* Allows indenting (pretty printing) template content
* Supports inline validation feature of your template
* Adds a Help button that takes you to the [page in this wiki
* Major styling update to integrate better visually with the rest of ART-DECOR

2015-03-29 version 0.9.7
* Now support being called to create a new template
* Adds a close button and deactivating edit buttons when no lock available
* Add content completion for assert/report

2015-02-04 version 0.8.6
* Permissions for modules from admin:dba to admin:decor

2015-01-21 version 0.8.5
* Fixed bug with &amp;lt; in asserts.
* Add content completion for assert/report

2014-11-23 version 0.8.4
* Give a proposed default value for id and effectiveDate in new template.
* Made editor global variable for access by all functions. 
* First shot a template-from-concept generator. Code still in flux. Intent: select a concept, apply a stylesheet from xslt collection to make draft template. Simply add stylesheet to /xslt to change behavior.

2014-11-04 version 0.8.3
* Updated code mirror
* Added incubator functions for rapid creation of observation templates from a concept

## HL7: CDA Release 2 XML materials
CDA Release 2 files. XML Schema, XSL, instances

2019-10-23 version 1.4.10
* Avoid install errors
* Updated for legalAuthenticator signatures
* Improved ICPC-1-NL display

2019-01-22 version 1.4.9
* Synced with official version 4.0.1 but updated for XSLT2. Saxon does not like the exslt:node-set() function

2018-10-08 version 1.4.8
* CDA.xsl 4.0.0 as officially published at HL7 International

2018-08-21 version 1.4.7
* CDA.xsl 4.0.0 beta 5 from HL7 International
* Added examples 

2017-12-15 version 1.4.3
* Reimplemented security fix from Rick Geimer, Lantana, that does full checking on narrative table components
* Fixed capitals in iframe properties WIDTH and HEIGHT
* Removed non-latins from filenames to make easier URLs
* Fixed a problem where more than 1 search term would yield an error
* Extended the font-families from just Verdana to include Arial and sans-serif
* Removed l10n duplicate keys 
* Copyright update in code
* Removed unused css file

2016-02-12 version 1.4.2
* Updated AD datatype handling

2015-03-29 version 1.4.0
* Fixed HTML 4 as XHTML 1.0 and fixed compliancy problems

2014-11-04 version 1.2.0
* cda.xsl updated. See [https://decor.nictiz.nl/CDA/xsl/documentation/cda.html#dS_cda.xsl full documentation]([Temple]]) or [download the materials](https://decor.nictiz.nl/CDA/)

2014-10-20 version 1.0.3
Fixed support for recordTarget/patient extensions deceasedInd/deceasedTime and multipleBirthInd/multipleBirthOrderNumber. They were inadvertently called in the HL7 namespace.
2014-10-13 version 1.0.2
Fixed @ID by adding it to whatever in scope element (div, sup, sub, a, etc.) as @id. To this end the separate calls were replaced by a central call in mode 'handleSectionTextAttributes'.
2014-10-02 version 1.0.1
Fixed linkHtml support and links to CDA.xsd in the example files

## HL7: demo5 - vital signs XML materials
demo5 XML-materials

Version 0.1.0

Read more information, or download hl7_demo5-0.1.0.xar.

## Terminology: ATC Data
ATC Data

2015-07-01 version 0.6.5
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 0.6.4
Indexes compatible with eXist-db >= 2.2

Version 0.6.2

Read more information, or download terminology-atc-data-0.6.2.xar.

## Terminology: HL7 Data
HL7 Data - generated from RIM 2.40.7 / CoreMIF DEFN=UV=VO=1175-20120802 (HL7 Normative Edition 2013) with only active code systems.

2015-07-01 version 1175-20120802.2
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 1175-20120802.1
Indexes compatible with eXist-db >= 2.2

Version 1175-20120802

Read more information, or download terminology-hl7-data-1175-20120802.xar.

## Terminology: ICD-10 DE Data
ICD-10 DE Data package

2015-07-01 version 1.1.3
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 1.1.2
Indexes compatible with eXist-db >= 2.2

Version 1.1.1

Read more information, or download terminology-icd10-de-data-1.1.1.xar.

## Terminology: ICD-10 NL Data
ICD-10 NL Data package

2017-10-19 version 2.0.4
* Optimized searches by including relatedTerm
* Optimized display for Rubrics by including the display name of the rubric if available

2016-06-13 version 2.0.3
* Updated version after update from RIVM that includes Zika

2015-07-01 version 2.0.2
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 2.0.1
Indexes compatible with eXist-db >= 2.2

2015-01-12 version 2.0.0
* Upgraded ICD10 NL to 2014 version.

2014-10-29 version 1.1.2
* Added agreed upon OID 2.16.840.1.113883.6.3.2

## Terminology: ICD-10 US Data
ICD-10 US Data package

2015-07-01 version 1.1.3
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 1.1.2
Indexes compatible with eXist-db >= 2.2

Version 1.1.1

Read more information, or download terminology-icd10-us-data-1.1.1.xar.

## Terminology: ICF NL Data
ICF NL data package

2015-07-01 version 0.6.6
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 0.6.5
Indexes compatible with eXist-db >= 2.2

Version 0.6.4

Read more information, or download terminology-icf-nl-data-0.6.4.xar.

## Terminology: ICPC-1-NL Data
ICPC-1-NL data package

2015-07-01 version 2013.2
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 2013.1
Indexes compatible with eXist-db >= 2.2

Version 2013

Read more information, or download terminology-icpc-1-nl-data-2013.xar.

## Terminology: ISO 9999 Data
ISO 9999 Data

2015-02-16 version 0.2.1
Indexes compatible with eXist-db >= 2.2

Version 0.2

Read more information, or download terminology-iso9999-data-0.2.xar.

## Terminology: LOINC data
LOINC data package

2017-03-23 version 2.58.3
* More fixed indexes

2017-02-28 version 2.58.2
* Added panels.
* Fixed LOINC search issues in index.

2017-01-06 version 2.58.0
* Updated for LOINC 2.58

2016-07-18 version 2.56.3
* Performance LOINC and improvements on accuracy. Depends on Terminology 1.8.24

2016-07-14 version 2.56.2
* Bugfix due to inconsistency in Linguistic Variants. These contain column TIME_ASPECT instead of TIME_ASPCT like the main db has. Mitigated by converting TIME_ASPECT into TIME_ASPCT.

2016-07-03 version 2.56.1
* Performance. Do not merge duplicate @loinc-num attributes into result
* Performance in LOINC searching

2016-06-28 version 2.56
* Update to LOINC 2.56
* NOTE: due to a newer more compact db layout, please also update ART and Terminology Applications packages

2016-06-14 version 2.54.1
* Performance update. Rearranged the package for speed. Please update Terminology Applications package in conjunction with this update, and please note that the installation time is considerable (up to 45 minutes of perceived down time)

2016-02-12 version 2.54
* Updated to LOINC 2.54 with all linguistic variants

2015-02-25 version 2.50.3
* Updated for license after talking to Daniel Vreeman
* Indexes compatible with eXist-db >= 2.2

2015-01-31 version 2.50.2
* Added @loinc_num as elem name='LOINC_NUM' to smooth searching (saves if/then)
* Added all properties into lucene index

2015-01-25 version 2.50.1
* Now includes all columns and map_to info. Compatible with existing Terminology packages

2014-12-22 version 2.50.0
* LOINC 2.50

2014-10-14 version 2.48.1
* LOINC 2.48

## Terminology: RadLex Data
RadLex

2015-07-01 version 3.9.5
* Updated all ClaML terminologies to support index on @conceptId
* Updated all form to support searching in description (default), code, or both

2015-02-16 version 3.9.4
Indexes compatible with eXist-db >= 2.2

Version 3.9.3

Read more information, or download terminology-RadLex-data-3.9.3.xar.

## Terminology: SNOMED CT Data
Snomed CT Data

2015-02-16 version 14.1.1
Indexes compatible with eXist-db >= 2.2

Version 14.1

Read more information, or download terminology-snomed-data-14.1.xar.
